var cluster = require('cluster');
var os = require('os');
cluster.schedulingPolicy = cluster.SCHED_RR;
// var { memdb, dbs } = require('cluster-memdb');

// async function masterinit() {
//     const userdb = memdb('user', 'id')
//     const lovedb = memdb('love', 'key');
//     // create "user" key and write those 2 users to "user"
//     await userdb.save([
//         { id: 1, name: 'Mike', gender: 'male', love: ['bread'] },
//         { id: 2, name: 'Louis', gender: 'female', love: ['beef', 'bread'] },
//     ])

//     // clean "love" key and write new datas
//     await lovedb.replace([
//         { key: 'bread', price: '3.00' },
//         { key: 'beef', price: '10.00' }
//     ])

//     var dd = await userdb.getAll() // [{id:1, ...}, {id:2, ...}]

//     await lovedb.getByKeys(['bread']) // [{key:'bread', price:'3.00'}]

//     await userdb.find({ name: 'Mike' }) // [{id:1, ...}]

// }
if (cluster.isMaster) {

    // masterinit();
    // Count the machine's CPUs
    var cpuCount = os.cpus().length;
    // var cpuCount = 3;

    cluster.on('message', (worker, msg, handle) => {
        // console.log(`cluster:master:from:worker:${worker.id} : ${msg.type}`);
        for (const id in cluster.workers) {
            // Here we notify each worker of the updated value
            if (cluster.workers[id].id != worker.id) {
                cluster.workers[id].send(msg);
            }
        }
    });

    // Create a worker for each CPU
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
    // Listen for dying workers
    cluster.on('exit', function() {
        cluster.fork();
    });
} else {
    process.on('message', (msg) => {

        if (msg.type == 'online_user_lists') {
            online_user_lists = msg.online_user_lists;
        } 
        else if (msg.type == 'all_users') {
            all_users = msg.all_users;
        }
        else if (msg.type == 'all_devices') {
            all_devices = msg.all_devices;
        } 
        else if (msg.type == 'all_company') {
            all_company = msg.all_company;
            // console.log(`cluster:worker:${msg.type}:wid:${cluster.worker.id}:from master:${Object.keys(all_company).length}`);
        } else if (msg.type == 'all_teams') {
            all_teams = msg.all_teams;
        } else if (msg.type == 'all_bunit') {
            all_bunit = msg.all_bunit;
        } else if (msg.type == 'voip_conv_store') {
            voip_conv_store = msg.voip_conv_store;
            // console.log(`cluster:worker:${msg.type}:wid:${cluster.worker.id}:from master:${Object.keys(voip_conv_store).length}`);
        } else if (msg.type == 'voip_busy_user') {
            voip_busy_user = msg.voip_busy_user;
        } else if (msg.type == 'voip_busy_conv') {
            voip_busy_conv = msg.voip_busy_conv;
        } else if (msg.type == 'voip_timer_ring') {
            voip_timer_ring = msg.voip_timer_ring;
            
        } else if (msg.type == 'voip_timer_clear') {
            if (String(msg.pid) == String(process.pid)) {
                // console.log('RingInterval:del:worker:', process.pid, msg.conv_id, msg.user_id)
                if(voip_timer_ring[msg.conv_id] && voip_timer_ring[msg.conv_id][msg.user_id]){
                    delete voip_timer_ring[msg.conv_id][msg.user_id];
                    if (process.send) process.send({ type: 'voip_timer_ring', voip_timer_ring });

                }
                if(voip_timer_process[msg.conv_id] && voip_timer_process[msg.conv_id][msg.user_id]){
                    clearTimeout(voip_timer_process[msg.conv_id][msg.user_id]['timer']);
                    delete voip_timer_process[msg.conv_id][msg.user_id];
                }
            }
        }
    });

    require('./bin/www');
    // console.log('cluster',process.pid)
}