const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");
// var {models} = require('../config/db/express-cassandra');

function validateLoginInput(data) {
    let errors = {};
    data.email = !isEmpty(data.email) ? data.email : "";
    data.password = !isEmpty(data.password) ? data.password : "";
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    }
    if (Validator.isEmpty(data.password)) {
        errors.password = "Password field is required.";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateRegisterInput(data) {
    let errors = {};
    data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.password = !isEmpty(data.password) ? data.password : "";
    data.password2 = !isEmpty(data.password2) ? data.password2 : "";
    if (Validator.isEmpty(data.firstname)) {
        errors.firstname = "Fullname field is required.";
    }
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    }
    if (Validator.isEmpty(data.password)) {
        errors.password = "Password field is required.";
    }
    if (Validator.isEmpty(data.password2)) {
        errors.password2 = "Confirm password field is required.";
    }
    if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
        errors.password = "Password must be at least 6 characters.";
    }
    if (!Validator.equals(data.password, data.password2)) {
        errors.password2 = "Passwords must match.";
    }
	if (!isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateMainGet(data) {
	// let errors = {};
	// let query = {};
	// if(isUuid(data.user_id)){
	// 	query.id = models.uuidFromString(data.user_id);
	// }
	// else if(!isEmpty(data.user_id)){
	// 	errors.user_id = "User id is invalid.";
	// }
	
	// if(isUuid(data.company_id)){
	// 	query.company_id = models.timeuuidFromString(data.company_id);
	// }
	// else if(!isEmpty(data.company_id)){
	// 	errors.company_id = "Company id is invalid.";
	// }
	
	// if(!isEmpty(data.firstname)){
	// 	query.firstname = data.firstname;
	// }
	// if(!isEmpty(data.email)){
	// 	query.email = data.email;
	// }
	// if(!isNaN(data.is_active)){
	// 	query.is_active = data.is_active;
	// }
	// if(!isNaN(data.is_delete)){
	// 	query.is_delete = data.is_delete;
	// }
	
	// if(!isEmpty(data) && isEmpty(query)){
	// 	errors.params = "Invalid params.";
	// }
    
    // return {
	// 	query,
    //     errors,
    //     isValid: isEmpty(errors)
    // };
}

function validateUpdateUserInput(data) {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    if (Validator.isEmpty(data.name)) {
        errors.name = "Name field is required";
    }
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateOpenThread(data) {
    let errors = {};
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.last_msg_id = !isEmpty(data.last_msg_id) ? data.last_msg_id : "";
    
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	// if (data.last_msg_id != "" && !isUuid(data.last_msg_id)) {
    //     errors.last_msg_id = "Last msg id is invalid.";
    // }
	

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateDateArg(data) {
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.delete_type = !isEmpty(data.delete_type) ? data.delete_type : "";
    data.is_reply_msg = !isEmpty(data.is_reply_msg) ? data.is_reply_msg : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
    if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (Validator.isEmpty(data.delete_type)) {
        errors.last_msg_id = "Delete type is required.";
    }
	if (Validator.isEmpty(data.is_reply_msg)) {
        errors.is_reply_msg = "Message type is required.";
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateForwardeArg(data) {
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.is_reply_msg = !isEmpty(data.is_reply_msg) ? data.is_reply_msg : "";
    data.conversation_lists = !isEmpty(data.conversation_lists) ? data.conversation_lists : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (Validator.isEmpty(data.is_reply_msg)) {
        errors.is_reply_msg = "Message type is required.";
    }
	if (data.conversation_lists && data.conversation_lists.length == 0) {
        errors.conversation_lists = "Message type is required.";
    }
	if (data.conversation_lists && Array.isArray(data.conversation_lists)) {
        (data.conversation_lists).forEach(function(v, k){
            if(!isUuid(v))
                errors.conversation_list_item = "Conversation id is invalid.";
        });
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateFileShareArg(data) {
    let errors = {};
    data.sender_img = !isEmpty(data.sender_img) ? data.sender_img : "";
    data.conversation_lists = !isEmpty(data.conversation_lists) ? data.conversation_lists : [];
    data.file_lists = !isEmpty(data.file_lists) ? data.file_lists : [];
    
    if (Validator.isEmpty(data.sender_img)) {
        errors.sender_img = "Sender img is required.";
    }
	if (data.conversation_lists && data.conversation_lists.length == 0) {
        errors.conversation_lists = "Conversation lists is required.";
    }
	if (data.conversation_lists && Array.isArray(data.conversation_lists)) {
        (data.conversation_lists).forEach(function(v, k){
            if(!isUuid(v))
                errors.conversation_list_item = "Conversation id is invalid.";
        });
    }
	if (data.file_lists && data.file_lists.length == 0) {
        errors.file_lists = "File lists is required.";
    }
	if (data.file_lists && Array.isArray(data.file_lists)) {
        (data.file_lists).forEach(function(v, k){
            if(!isUuid(v))
                errors.file_lists_item = "File id is invalid.";
        });
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateEditArg(data) {
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    // data.new_msg_body = !isEmpty(data.new_msg_body) ? data.new_msg_body : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (data.new_msg_body !== undefined && Validator.isEmpty(data.new_msg_body)) {
        errors.new_msg_body = "New message body is required.";
    }
    if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function valdateSecretUserArg(data) {
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.add_secret_user = !isEmpty(data.add_secret_user) ? data.add_secret_user : [];
    data.remove_secret_user = !isEmpty(data.remove_secret_user) ? data.remove_secret_user : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
    if(data.add_secret_user.length == 0 && data.remove_secret_user.length == 0){
        errors.msg_id = "Either add_secret_user or remove_secret_user is required.";
    }
    if (data.add_secret_user.length > 0) {
        (data.add_secret_user).forEach(function(v, k){
            if(!isUuid(v))
                errors.add_secret_user_item = "Add User id is invalid.";
        });
    }
    if (data.remove_secret_user.length > 0) {
        (data.remove_secret_user).forEach(function(v, k){
            if(!isUuid(v))
                errors.remove_secret_user_item = "Remove User id is invalid.";
        });
    }
	
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

module.exports = {
	validateLoginInput,
	validateRegisterInput,
	valdateMainGet,
	validateUpdateUserInput,
    valdateOpenThread,
    valdateDateArg,
    valdateForwardeArg,
    valdateFileShareArg,
    valdateEditArg,
    valdateSecretUserArg
};