const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");

function validateCreateTeamInput(data) {
    // console.log(data);
    let errors = {};
    data.title = !isEmpty(data.title) ? data.title : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
    if (Validator.isEmpty(data.title)) {
        errors.title = "Team title field is required.";
    }
	if (data.participants.length == 0) {
        errors.participants = "Participants are not array or empty.";
    }
    if(Array.isArray(data.participants)){
        (data.participants).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.participants_item = "Participants id is invalid.";
            }
        });
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

module.exports = {
	validateCreateTeamInput
};