const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");

function validateMsgInput(data) {
    // console.log(data);
    let errors = {};
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.sender = !isEmpty(data.sender) ? data.sender : "";
    data.msg_body = !isEmpty(data.msg_body) ? data.msg_body : "";
    data.is_reply_msg = !isEmpty(data.is_reply_msg) ? data.is_reply_msg : "no";
    data.reply_for_msgid = !isEmpty(data.reply_for_msgid) ? data.reply_for_msgid : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
	if (Validator.isEmpty(data.msg_body)) {
        errors.msg_body = "Message body field is required.";
    }
	if (!isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.sender)) {
        errors.sender = "Sender id is invalid.";
    }
	if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	if (data.is_reply_msg == 'yes') {
	    if (!isUuid(data.reply_for_msgid)) {
            errors.reply_for_msgid = "Reply msg id is invalid.";
        }
    }
	if (data.msg_type !== undefined && data.msg_type == 'checklist') {
	    if (! Array.isArray(data.list_items)) {
            errors.list_items = "List item are not array.";
        }else{
            (data.list_items).forEach(function(v, k){
                v.title = !isEmpty(v.title) ? v.title : "";
                if(Validator.isEmpty(v.title)){
                    errors.item_title = "Item title field is required.";
                }
            });
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateEmojiArg(data) {
    // console.log(data);
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.emoji = !isEmpty(data.emoji) ? data.emoji : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
    if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (Validator.isEmpty(data.emoji)) {
        errors.emoji = "Emoji field is required.";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateFlagArg(data) {
    // console.log(data);
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.is_add = !isEmpty(data.is_add) ? data.is_add : "";
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (Validator.isEmpty(data.is_add)) {
        errors.is_add = "Is add field is required.";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateReplyMsgInput(data) {
    // console.log(data);
    let errors = {};
    data.reply_for_msgid = !isEmpty(data.reply_for_msgid) ? data.reply_for_msgid : "";
    data.is_reply_msg = !isEmpty(data.is_reply_msg) ? data.is_reply_msg : "";
    
	if (Validator.isEmpty(data.is_reply_msg)) {
        errors.is_reply_msg = "Message body field is required.";
    }
	if (data.company_id && !isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
	if (data.conversation_id && !isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (data.sender && !isUuid(data.sender)) {
        errors.sender = "Sender id is invalid.";
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateMsgTaskArg(data) {
    // console.log(143, data);
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	if (data.msg_type != 'task') {
        errors.msg_type = "Message type invalid.";
    }
    if (! Array.isArray(data.list_items)) {
        errors.list_items = "List item are not array.";
    }else{
        (data.list_items).forEach(function(v, k){
            v.title = !isEmpty(v.title) ? v.title : "";
            if(Validator.isEmpty(v.title)){
                errors.item_title = "Item title field is required.";
            }
        });
    }

    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateTaskArg(data) {
    // console.log(186, data);
    let errors = {};
    data.msg_body = !isEmpty(data.msg_body) ? data.msg_body : "";
    data.msg_type = !isEmpty(data.msg_type) ? data.msg_type : "";
    data.participants = Array.isArray(data.participants) ? data.participants : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (!isUuid(data.msg_id)) {
        errors.msg_id = "Message id is invalid.";
    }
	if (data.participants && data.participants.length == 0) {
        errors.participants = "Participants array required.";
    }
	if (data.participants && Array.isArray(data.participants)) {
        (data.participants).forEach(function(v, k){
            if(!isUuid(v))
                errors.participant_list_item = "Participant id is invalid.";
        });
    }
	if (data.msg_body == "") {
        errors.msg_body = "Task title is required.";
    }
    if (data.msg_type.indexOf('task') == -1) {
        errors.msg_type = "Message type invalid.";
    }
    
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateSeenArg(data) {
    // console.log(186, data);
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_ids = !isEmpty(data.msg_ids) ? data.msg_ids : [];
    
	if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	if (data.msg_ids && data.msg_ids.length == 0) {
        errors.msg_ids = "Msg ids array required.";
    }
	if (data.msg_ids && Array.isArray(data.msg_ids)) {
        (data.msg_ids).forEach(function(v, k){
            if(!isUuid(v))
                errors.msg_id_list_item = "Msg id is invalid.";
        });
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}


module.exports = {
	validateMsgInput,
    validateEmojiArg,
    validateFlagArg,
    validateMsgTaskArg,
    validateSeenArg,
    validateTaskArg
};