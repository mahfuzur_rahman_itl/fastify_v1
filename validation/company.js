const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");
// var {models} = require('../config/db/express-cassandra');

function validateCompanyInput(data) {
    let errors = {};
    data.company_name = !isEmpty(data.company_name) ? data.company_name : "";
    data.domain_name = !isEmpty(data.domain_name) ? data.domain_name : "";
    if (Validator.isEmpty(data.company_name)) {
        errors.company_name = "Company Name field is required.";
    }
    if (Validator.isEmpty(data.domain_name)) {
        errors.domain_name = "Domain Name field is required.";
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateGet(data) {
    let errors = {};
	let query = {};
	if(isUuid(data.company_id)){
		query.company_id = data.company_id;
	}
	else if(!isEmpty(data.company_id)){
		errors.company_id = "Company id is invalid.";
	}
	
	if(!isEmpty(data.company_name)){
		query.company_name = data.company_name;
	}
    if(!isEmpty(data) && isEmpty(query)){
		errors.params = "Invalid params.";
	}
    return {
		query,
        errors,
        isValid: isEmpty(errors)
    };
}

function validateUpdateInput(data) {
    let errors = {};
	let query = {};
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.company_name = !isEmpty(data.company_name) ? data.company_name : "";

	if(isUuid(data.company_id)){
		query.company_id = data.company_id;
	}
	else {
		errors.company_id = "Company id is invalid.";
	}
    if (Validator.isEmpty(data.company_name)) {
        errors.company_name = "company_name field is required.";
    }
    
    return {
		query,
        errors,
        isValid: isEmpty(errors)
    };
}

module.exports = {
	validateCompanyInput,
	validateGet,
    validateUpdateInput
};