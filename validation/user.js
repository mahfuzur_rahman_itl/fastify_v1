const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");
// var {models} = require('../config/db/express-cassandra');

function validateLoginInput(data) {
    let errors = {};
    data.email = !isEmpty(data.email) ? data.email : "";
    data.password = !isEmpty(data.password) ? data.password : "";
    data.device_id = !isEmpty(data.device_id) ? data.device_id : "";
    
	if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    } 
	
    if (Validator.isEmpty(data.password)) {
        errors.password = "Password field is required.";
    }
    if (Validator.isEmpty(data.device_id)) {
        errors.device_id = "Device id field is required.";
    }
	if (data.company_id && !isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateSwitchAccount(data) {
    let errors = {};
    data.email = !isEmpty(data.email) ? data.email : "";
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.device_id = !isEmpty(data.device_id) ? data.device_id : "";
    
	if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    } 
	
    if (Validator.isEmpty(data.device_id)) {
        errors.device_id = "Device id field is required.";
    }
	if (!isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateRegisterInput(data) {
    let errors = {};
    data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
    data.lastname = !isEmpty(data.lastname) ? data.lastname : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    if (Validator.isEmpty(data.firstname)) {
        errors.firstname = "First name field is required.";
    }
    if (Validator.isEmpty(data.lastname)) {
        errors.lastname = "Last name field is required.";
    }
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    }
    if (!isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateUpdateUser(data) {
    let errors = {};
    data.firstname = !isEmpty(data.firstname) ? data.firstname : "";
    data.img = !isEmpty(data.img) ? data.img : "";
    if (Validator.isEmpty(data.firstname)) {
        errors.firstname = "Fullname field is required.";
    }
    if (Validator.isEmpty(data.img)) {
        errors.img = "Image field is required.";
    }
    // should be add more data in future 
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateNewPassInput(data) {
    let errors = {};
    data.email = !isEmpty(data.email) ? data.email : "";
    data.password = !isEmpty(data.password) ? data.password : "";
    data.password2 = !isEmpty(data.password2) ? data.password2 : "";
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required.";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid.";
    }
    if (Validator.isEmpty(data.password)) {
        errors.password = "Password field is required.";
    }
    if (Validator.isEmpty(data.password2)) {
        errors.password2 = "Confirm password field is required.";
    }
    if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
        errors.password = "Password must be at least 6 characters.";
    }
    if (!Validator.equals(data.password, data.password2)) {
        errors.password2 = "New Password and Confirm Password does not match.";
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateGet(data) {
	let errors = {};
	let query = {};
	if(isUuid(data.user_id)){
		query.id = data.user_id;
	}
	else if(!isEmpty(data.user_id)){
		errors.user_id = "User id is invalid.";
	}
	
	if(isUuid(data.company_id)){
		query.company_id = data.company_id;
	}
	else if(!isEmpty(data.company_id)){
		errors.company_id = "Company id is invalid.";
	}
	
	if(!isEmpty(data.firstname)){
		query.firstname = data.firstname;
	}
	if(!isEmpty(data.email)){
		query.email = data.email;
	}
	if(!isNaN(data.is_active)){
		query.is_active = data.is_active;
	}
	if(!isNaN(data.is_delete)){
		query.is_delete = data.is_delete;
	}
	
	if(!isEmpty(data) && isEmpty(query)){
		errors.params = "Invalid params.";
	}
    
    return {
		query,
        errors,
        isValid: isEmpty(errors)
    };
}

function validateUpdateUserInput(data) {
    let errors = {};
    data.name = !isEmpty(data.name) ? data.name : "";
    data.email = !isEmpty(data.email) ? data.email : "";
    if (Validator.isEmpty(data.name)) {
        errors.name = "Name field is required";
    }
    if (Validator.isEmpty(data.email)) {
        errors.email = "Email field is required";
    } else if (!Validator.isEmail(data.email)) {
        errors.email = "Email is invalid";
    }
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateTeammateInput(data) {
    let errors = {};
    data.user_id = !isEmpty(data.user_id) ? data.user_id : "";
    data.name = !isEmpty(data.name) ? data.name : "";
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.company_name = !isEmpty(data.company_name) ? data.company_name : "";
    data.teammate_data = !isEmpty(data.teammate_data) ? data.teammate_data : [];
    if (Validator.isEmpty(data.user_id)) {
        errors.user_id = "User ID field is required";
    }
    if (! isUuid(data.user_id)) {
        errors.user_id = "User ID is invalid";
    }
    if (Validator.isEmpty(data.name)) {
        errors.name = "Name field is required";
    }
    if (Validator.isEmpty(data.company_id)) {
        errors.company_id = "Company ID field is required";
    }
    if (! isUuid(data.company_id)) {
        errors.company_id = "Company ID is invalid";
    }
    if (Validator.isEmpty(data.company_name)) {
        errors.company_name = "Company name field is required";
    }
    if (data.teammate_data.length == 0) {
        errors.teammate_data = "teammate_data are not array or empty.";
    }
    if(Array.isArray(data.teammate_data)){
        var email_uniq = [];
        (data.teammate_data).forEach(function(v, k){
            if(!Validator.isEmail(v.email)){
                errors.teammate_data_item = "Email is invalid.";
            }
            if(email_uniq.indexOf(v) == -1)
                email_uniq.push(v);
            else
                errors.teammate_data = 'teammate_data are not uniq';
        });
    }
    if (data.teammate_data.length > 10) {
        errors.teammate_data = 'Limit over. Max 10 email use.';
    }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

module.exports = {
	validateLoginInput,
    validateSwitchAccount,
	validateRegisterInput,
    validateNewPassInput,
	validateGet,
	validateUpdateUserInput,
    validateUpdateUser,
    validateTeammateInput
};