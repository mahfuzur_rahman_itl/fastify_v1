const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");

function validateCreateTagInput(data) {
    // console.log(data);
    let errors = {};
    data.title = !isEmpty(data.title) ? data.title : "";
    data.mention_users = !isEmpty(data.mention_users) ? data.mention_users : [];
    data.tagged_by = !isEmpty(data.tagged_by) ? data.tagged_by : "";
    data.tag_type = !isEmpty(data.tag_type) ? data.tag_type : "";
    data.tag_color = !isEmpty(data.tag_color) ? data.tag_color : "";
    data.shared_tag = !isEmpty(data.shared_tag) ? data.shared_tag : "";
    data.team_list = !isEmpty(data.team_list) ? data.team_list : "";
    
	if (Validator.isEmpty(data.title)) {
        errors.title = "Tag title field is required.";
    }
	if (Validator.isEmpty(data.tag_type)) {
        errors.tag_type = "Tag type field is required.";
    }
	if (Validator.isEmpty(data.tag_color)) {
        errors.tag_color = "Tag color field is required.";
    }
	if (data.shared_tag && Validator.isEmpty(data.shared_tag)) {
        errors.shared_tag = "Shared Tag field is required.";
    }
	if (data.team_list && !Array.isArray(data.team_list)) {
        errors.team_list = "Team list are not array.";
    }
	if(Array.isArray(data.team_list)){
        (data.team_list).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.team_list_item = "Team id is invalid.";
            }
        });
    }
    if (data.mention_users && !Array.isArray(data.mention_users)) {
        errors.mention_users = "Mention users are not array.";
    }
    if(Array.isArray(data.mention_users)){
        (data.mention_users).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.mention_users_item = "Mention user id is invalid.";
            }
        });
    }
    
	if (!isUuid(data.tagged_by)) {
        errors.tagged_by = "Tagged by id is invalid.";
    }
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateAddRemoveMsgTag(data) {
    // console.log(data);
    let errors = {};
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    data.msg_id = !isEmpty(data.msg_id) ? data.msg_id : "";
    data.file_id = !isEmpty(data.file_id) ? data.file_id : "";
    data.newtag = !isEmpty(data.newtag) ? data.newtag : [];
    data.removetag = !isEmpty(data.removetag) ? data.removetag : "";
    // data.tag_list_with_user = !isEmpty(data.tag_list_with_user) ? data.tag_list_with_user : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    
    if (!isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
    if (!isUuid(data.msg_id)) {
        errors.msg_id = "msg id is invalid.";
    }
    if (!isUuid(data.file_id)) {
        errors.file_id = "File id is invalid.";
    }
	if (data.participants && !Array.isArray(data.participants)) {
        errors.participants = "Participants are not array.";
    }
	if(Array.isArray(data.participants)){
        (data.participants).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.participants_item = "Participants id is invalid.";
            }
        });
    }
    if (data.removetag && !Array.isArray(data.removetag)) {
        errors.removetag = "Remove tag list are not array.";
    }
	if(Array.isArray(data.removetag)){
        (data.removetag).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.removetag_item = "Remove tag id is invalid.";
            }
        });
    }
    if (data.newtag && !Array.isArray(data.newtag)) {
        errors.newtag = "New Tag list are not array.";
    }
    if(Array.isArray(data.newtag)){
        (data.newtag).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.newtag_item = "New tag id is invalid.";
            }
        });
    }
    // if (data.tag_list_with_user != "") {
    //     data.tag_list_with_user = JSON.parse(data.tag_list_with_user);
    //     if(Array.isArray(data.tag_list_with_user)){
    //         (data.tag_list_with_user).forEach(function(v, k){
    //             for (let [key, value] of Object.entries(v)) {
    //                 if(!isUuid(key) || !isUuid(value)){
    //                     errors.tag_list_with_user_kv = "Tag list with user key or value is invalid.";
    //                 }
    //             }   
    //         });
    //     } else {
    //         errors.tag_list_with_user_json = "Tag list with user is not json array.";
    //     }
    // }else{
    //     errors.tag_list_with_user = "Tag list with user is required.";
    // }
    
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

function validateFavouriteUnfavouriteTagInput(data) {
    // console.log(data);
    let errors = {};
    data.favo_data = !isEmpty(data.favo_data) ? data.favo_data : [];
    
    if (data.favo_data && !Array.isArray(data.favo_data)) {
        errors.favo_data = "Favourite data are not array.";
    }
    if(Array.isArray(data.favo_data)){
        (data.favo_data).forEach(function(v, k){
            if(v && !isUuid(v.tag_id)){
                errors.favo_data_item = "Favourite data tag_id is invalid.";
            }
            if(v.status && (v.status < 1 || v.status > 2)){
                errors.favo_data_item = "Favourite data status is invalid.";
            }
        });
    }
    
	
    return {
        errors,
        isValid: isEmpty(errors)
    };
}

module.exports = {
	validateCreateTagInput, validateAddRemoveMsgTag, validateFavouriteUnfavouriteTagInput
};