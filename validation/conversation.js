const Validator = require("validator");
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");

function validateCreateRoomInput(data) {
    // console.log(data);
    let errors = {};
    data.title = !isEmpty(data.title) ? data.title : "";
    data.participants = !isEmpty(data.participants) ? data.participants : [];
    data.participants_admin  = !isEmpty(data.participants_admin ) ? data.participants_admin  : [];
    data.created_by = !isEmpty(data.created_by) ? data.created_by : "";
    data.company_id = !isEmpty(data.company_id) ? data.company_id : "";
    data.team_id = !isEmpty(data.team_id) ? data.team_id : "";
    data.b_unit_id = !isEmpty(data.b_unit_id) ? data.b_unit_id : "";
    data.conversation_id = !isEmpty(data.conversation_id) ? data.conversation_id : "";
    
	if (Validator.isEmpty(data.title)) {
        errors.title = "Conversation title field is required.";
    }
	if (data.participants && !Array.isArray(data.participants)) {
        errors.participants = "Participants are not array.";
    }
    if(Array.isArray(data.participants)){
        (data.participants).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.participants_item = "Participant id is invalid.";
            }
        });
    }
	if (data.participants_admin && !Array.isArray(data.participants_admin)) {
        errors.participants_admin = "Participants admin are not array.";
    }
    if(Array.isArray(data.participants_admin)){
        (data.participants_admin).forEach(function(v, k){
            if(v && !isUuid(v)){
                errors.participants_admin = "Participant id is invalid.";
            }
        });
    }
	if (!isUuid(data.created_by)) {
        errors.created_by = "Created by id is invalid.";
    }
	if (!isUuid(data.company_id)) {
        errors.company_id = "Company id is invalid.";
    }
	if (data.team_id && !isUuid(data.team_id)) {
        errors.team_id = "Team id/ team_id is invalid.";
    }
	if (data.b_unit_id && !isUuid(data.b_unit_id)) {
        errors.b_unit_id = "Business unit id is invalid.";
    }
	if (data.conversation_id && !isUuid(data.conversation_id)) {
        errors.conversation_id = "Conversation id is invalid.";
    }
	

    return {
        errors,
        isValid: isEmpty(errors)
    };
}


module.exports = {
	validateCreateRoomInput
};