const { MongoClient } = require('mongodb');
const fs = require('fs');
const cachePath = 'cache.json';

const uri = process.env.MONGO_URL; // Replace with your MongoDB connection string
const dbName = "freeli_live_v3"; // Replace with your database name
const collectionName = "msal_cache"; // Collection name for storing the cache

let client;

const connectToMongo = async () => {
  if (!client) {
    client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    await client.connect();
  }
  return client.db(dbName).collection(collectionName);
};

// const beforeCacheAccess = async (cacheContext) => {
//   console.log('beforeCacheAccess', cacheContext, cacheContext.account.homeAccountId);
//   cacheContext.tokenCache.deserialize(fs.readFile(cachePath, "utf-8", (err, d) => {
//     console.log("errror", err, d)

//   }));
//   // const collection = await connectToMongo();
//   // const cacheDoc = await collection.findOne({ id: "msal_cache" });
//   // if (cacheDoc) {
//   //   cacheContext.tokenCache.deserialize(cacheDoc.cache);
//   // }
// };

// const afterCacheAccess = async (cacheContext) => {
//   console.log('afterCacheAccess', cacheContext);
//   if (cacheContext.cacheHasChanged) {
//     fs.writeFile(cachePath, cacheContext.tokenCache.serialize(), (err) => {
//       console.log("errror", err)
//     });
//   }

//   // if (cacheContext.cacheHasChanged) {
//   //   const collection = await connectToMongo();
//   //   const cacheData = cacheContext.tokenCache.serialize();
//   //   await collection.updateOne(
//   //     { id: "msal_cache" },
//   //     { $set: { cache: cacheData } },
//   //     { upsert: true }
//   //   );
//   // }
// };

const beforeCacheAccess = async (cacheContext) => {
  console.log('beforeCacheAccess', cacheContext);

  try {
    const data = await fs.readFile(cachePath, 'utf-8');
    console.log('Cache data read:', data);
    cacheContext.tokenCache.deserialize(data);
  } catch (err) {
    if (err.code === 'ENOENT') {
      console.log('Cache file not found, initializing new cache');
    } else {
      console.error('Error reading cache file:', err);
    }
  }
};

const afterCacheAccess = async (cacheContext) => {
  console.log('afterCacheAccess', cacheContext);
  if (cacheContext.cacheHasChanged) {
    try {
      await fs.writeFile(cachePath, cacheContext.tokenCache.serialize());
      console.log('Cache data written');
    } catch (err) {
      console.error('Error writing cache file:', err);
    }
  }
};

const cachePlugin = {
  beforeCacheAccess,
  afterCacheAccess,
};

module.exports = cachePlugin;
