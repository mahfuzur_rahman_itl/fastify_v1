/**
 * A plugin that provide encapsulated routes
 * @param {FastifyInstance} fastify encapsulated fastify instance
 * @param {Object} options plugin options, refer to https://fastify.dev/docs/latest/Reference/Plugins/#plugin-options
 */
async function routes (fastify, options) {
    // const Conversation = require('../mongo-models/conversation');
    var Conversations = fastify.mongo.db.collection('conversations')
    fastify.get('/', async (request, reply) => {
        try{
            var convs = await Conversations.find({conversation_id: 'cce83797-b863-4dcc-b220-f090e2020247'}); // Execute the query
            console.log('Number of conversations:', convs.length);

        }catch(e){
            console.log(13, e)
        }
      return { conversations: convs }
    })
}
  
module.exports = routes