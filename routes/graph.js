// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.

var graph = require('@microsoft/microsoft-graph-client');
require('isomorphic-fetch');

module.exports = {
  getUserDetails: async function(msalClient, userId, dbuser) {
    const client = getAuthenticatedClient(msalClient, userId, dbuser);

    const user = await client
      .api('/me')
      .select('displayName,mail,mailboxSettings,userPrincipalName')
      .get();
    return user;
  },

  // <GetCalendarViewSnippet>
  getCalendarView: async function(msalClient, userId, start, end, timeZone, dbuser) {
    const client = getAuthenticatedClient(msalClient, userId , dbuser);

    return client
      .api('/me/calendarview')
      // Add Prefer header to get back times in user's timezone
      .header('Prefer', `outlook.timezone="${timeZone}"`)
      // Add the begin and end of the calendar window
      .query({
        startDateTime: encodeURIComponent(start),
        endDateTime: encodeURIComponent(end)
      })
      // Get just the properties used by the app
      .select('subject,organizer,start,end,body,isAllDay')
      // Order by start time
      .orderby('start/dateTime')
      // Get at most 50 results
      .top(50)
      .get();
  },
  // </GetCalendarViewSnippet>
  // <CreateEventSnippet>
  createEvent: async function(msalClient, userId, formData, dbuser) {
    const client = getAuthenticatedClient(msalClient, userId, dbuser);
    return await client.api('/me/events').post(formData);
  },
  // </CreateEventSnippet>
  updateEvent: async function(msalClient, userId, event_id, formData, dbuser) {
    const client = getAuthenticatedClient(msalClient, userId, dbuser);
    return await client.api('/me/events/'+event_id).update(formData);
  },
  deleteEvent: async function(msalClient, userId, event_id, dbuser) {
    const client = getAuthenticatedClient(msalClient, userId, dbuser);
    return await client.api('/me/events/'+event_id).delete();
  },

};

function getAuthenticatedClient(msalClient, userId, dbuser) {
  if (!msalClient || !userId) {
    throw new Error(
      `Invalid MSAL state. Client: ${msalClient ? 'present' : 'missing'}, User ID: ${userId ? 'present' : 'missing'}`);
  }

  // Initialize Graph client
  const client = graph.Client.init({
    // Implement an auth provider that gets a token
    // from the app's MSAL instance
    authProvider: async (done) => {
      try {
        let account=null;
        // const account = await msalClient.getAccount({ homeAccountId: userId })
        account = await msalClient.getTokenCache().getAccountByHomeId(userId);
        // if(!account) account = dbuser.outlookAccount;
        console.log('outlook:account:token:',account);
       

        if (account) {
          // Attempt to get the token silently
          // This method uses the token cache and
          // refreshes expired tokens as needed
          let redirectUri = (new URL('/v2/outlook/callback', new URL(process.env.API_SERVER_URL).origin)).href;
          const scopes = process.env.OAUTH_SCOPES || 'https://graph.microsoft.com/.default';
          const response = await msalClient.acquireTokenSilent({
            scopes: scopes.split(','),
            redirectUri: redirectUri,
            account: account
          });

          // First param to callback is the error,
          // Set to null in success case
          done(null, response.accessToken);
        }
      } catch (err) {
        console.log(JSON.stringify(err, Object.getOwnPropertyNames(err)));
        done(err, null);
      }
    }
  });

  return client;
}
