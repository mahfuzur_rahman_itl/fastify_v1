const express = require('express');
const router = express.Router();
const isUuid = require('uuid-validate');
const moment = require('moment');
const jwt = require('jsonwebtoken');
const { google } = require('googleapis');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
// var Calendar = require('mongoose').model('calendar')
const Calendar = require('../../mongo-models/calendar');
var Users = require('mongoose').model('user');
var { xmpp_send_server } = require('../../v2_utils/voip_util');

// Route to initiate Google OAuth2 flow
router.get('/', (req, res) => {
    const userId = req.query.userId;
    const url = oauth2Client.generateAuthUrl({
        access_type: 'offline', // Request offline access to receive a refresh token
        scope: [
            'https://www.googleapis.com/auth/calendar.readonly', // Scope for read-only access to the calendar
            'https://www.googleapis.com/auth/calendar.events' // Scope for write access to calendar events
        ],
        state: userId // Pass the user ID as state parameter
    });
    // Redirect the user to Google's OAuth 2.0 server
    res.redirect(url);
});

// Route to handle the OAuth2 callback
router.get('/redirect', async (req, res) => {
    // Extract the code from the query parameter
    const code = req.query?.code || null;
    if (!code) return res.send('Authentication Failed')
    const userId = req.query.state;
    const { tokens } = await oauth2Client.getToken(code);

    if (tokens.refresh_token) {
        await Users.updateOne({ id: userId }, { googleTokens: tokens, google_id: code });
    } else {
        await Users.updateOne({ id: userId }, { google_id: code });
        var user = await Users.findOne({ id: userId });
        // If no refresh token is provided, only update the access token
        if (user.googleTokens) {
            user.googleTokens.access_token = tokens.access_token;
            user.googleTokens.expiry_date = tokens.expiry_date;
            await Users.updateOne({ id: userId }, { googleTokens: user.googleTokens });
        } else {
            // If there are no previous tokens, save the new tokens as they are
            await Users.updateOne({ id: userId }, { googleTokens: tokens });
        }
    }
    xmpp_send_server(userId, 'calendar_google', {google_id: code}, req);

    res.send('Google Authentication successful! You can now close this window.');
});

router.get('/google_token', (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    Users.findOne({ id: decode.id }, function (e, user) {
        if (e) return res.status(400).json({ error: 'Find error', message: e });
        res.status(200).json({ google_id: user.google_id || null });
    });
});
router.get('/google_events', async (req, res) => {
    try {
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        var user = await Users.findOne({ id: decode.id }).lean();
        if (!user || !user.googleTokens) { console.log('User or Google token not found.'); return; }
        // let code = user.google_id;
        let event_list = [];
        // Check if the access token is expired
        // const now = Date.now();
        // if (user.googleToken.expiry_date < now) {

        //         // Refresh the access token if it's expired
        //         await refreshToken(user);

        // oauth2Client.getToken(code, async (err, tokens) => {
        // if (err) {
        //     // Handle error if token exchange fails
        //     console.error('Couldn\'t get token', err);
        //     res.send('Error');
        //     return;
        // }
        // Set the credentials for the Google API client
        oauth2Client.setCredentials(user.googleTokens);
        let newToken = await oauth2Client.refreshAccessToken();
        oauth2Client.setCredentials(newToken.credentials);
        const calendar = google.calendar({ version: 'v3', auth: oauth2Client });

        const calendarListResponse = await calendar.calendarList.list();
        const calendars = calendarListResponse.data.items;
        if (calendars.length) {
            // let calendar_list = calendars[0];
            // Iterate over each calendar
            for (const cal of calendars) {
                // if(!cal.primary) continue;
                console.log(`Fetching events from calendar: ${cal.summary}`);
                const response = await calendar.events.list({
                    calendarId: cal.id,
                    timeMin: (new Date(moment().startOf('month'))).toISOString(),
                    maxResults: 100, // Adjust this value as needed
                    singleEvents: true,
                    orderBy: 'startTime',
                });
                const events = response.data.items;
                if (events.length) {
                    event_list.push(...response.data?.items?.map(v => ({
                        _id: String(v.id),
                        calendarId: cal.id,
                        title: v.summary,
                        start: new Date(v.start.dateTime || v.start.date),
                        allDay: v.start.date ? true : false,
                        ...((v?.end?.dateTime || v?.end?.date) && { end: new Date(v.end.dateTime || v.end.date) }),
                        description: v.description,
                        type: "google",
                    })));
                } else {
                    console.log(`No upcoming events found in ${cal.summary}.`);
                }
            }


        }
        res.status(200).json({ events: event_list });
    } catch (error) {
        console.log('Error refreshing token:', error);
        return res.status(500).send('Failed to refresh token');
    }

});
router.get('/google_calendars', (req, res) => {
    // Create a Google Calendar API client
    const calendar = google.calendar({ version: 'v3', auth: oauth2Client });
    // List all calendars
    calendar.calendarList.list({}, (err, response) => {
        if (err) {
            // Handle error if the API request fails
            console.error('Error fetching calendars', err);
            res.end('Error!');
            return;
        }
        // Send the list of calendars as JSON
        const calendars = response.data.items;
        res.json(calendars);
    });
});
router.post('/google_create', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    var user = await Users.findOne({ id: decode.id }).lean();
    if (!user || !user.googleTokens) { console.log('User or Google token not found.'); return; }
    oauth2Client.setCredentials(user.googleTokens);
    let newToken = await oauth2Client.refreshAccessToken();
    oauth2Client.setCredentials(newToken.credentials);

    const calendar = google.calendar({ version: 'v3', auth: oauth2Client });
    let timeZone = req.body.timeZone || "Greenwich Standard Time";
    let createObj = {
        summary: req.body.title,
    };

    if (req.body.description) {
        createObj.description = req.body.description;
    }
    let start_time = req.body.allDay ? moment.tz(req.body.start, timeZone).startOf('day').format() : moment.tz(req.body.start, timeZone).format();
    let end_time = req.body.allDay ? moment.tz(req.body.end, timeZone).endOf('day').add(1, 'seconds').format() : moment.tz(req.body.end, timeZone).format();
    if (req.body.start) {
        if (req.body.allDay) {
            createObj.start = {
                date: start_time.split('T')[0], // Updated start time
                timeZone: timeZone,
            }
        } else {
            createObj.start = {
                dateTime: start_time, // Updated start time
                timeZone: timeZone,
            }
        }
    }
    if (req.body.end) {
        if (req.body.allDay) {
            createObj.end = {
                date: end_time.split('T')[0], // Updated start time
                timeZone: timeZone,
            }

        } else {
            createObj.end = {
                dateTime: end_time, // Updated start time
                timeZone: timeZone,
            }
        }
    }
    // recurrence setup
    let recurs = [];
    if (req.body.recur === "2") {
        createObj.recurrence = ['RRULE:FREQ=DAILY'];
    }
    if (req.body.recur === "3") {
        createObj.recurrence = ['RRULE:FREQ=WEEKLY'];
    }
    if (req.body.recur === "4") {
        createObj.recurrence = ['RRULE:FREQ=MONTHLY'];
    }
    if (req.body.recur === "5") {
        createObj.recurrence = ['RRULE:FREQ=YEARLY'];
    }
    if (req.body.recur === "6" && req.body.recurrenceData) {
        if (req.body.recurrenceData.repeatUnit === "day" && req.body.recurrenceData.repeatInterval) {
            createObj.recurrence = [`RRULE:FREQ=DAILY;INTERVAL=${req.body.recurrenceData.repeatInterval}`];
        }
        if (req.body.recurrenceData.repeatUnit === "week" && req.body.recurrenceData.repeatInterval) {
            createObj.recurrence = [`RRULE:FREQ=WEEKLY;INTERVAL=${req.body.recurrenceData.repeatInterval}`];
        }
        if (req.body.recurrenceData.repeatUnit === "month" && req.body.recurrenceData.repeatInterval) {
            createObj.recurrence = [`RRULE:FREQ=MONTHLY;INTERVAL=${req.body.recurrenceData.repeatInterval}`];
        }
        if (req.body.recurrenceData.repeatUnit === "year" && req.body.recurrenceData.repeatInterval) {
            createObj.recurrence = [`RRULE:FREQ=YEARLY;INTERVAL=${req.body.recurrenceData.repeatInterval}`];
        }

        if (req.body.recurrenceData.endOption === "after" && req.body.recurrenceData.occurrences && createObj.recurrence.length) {
            createObj.recurrence = createObj.recurrence[0] + ';COUNT=' + req.body.recurrenceData.occurrences;
        }
        if (req.body.recurrenceData.endOption === "on" && req.body.recurrenceData.endDate && createObj.recurrence.length) {
            const untilDate = moment.tz(req.body.recurrenceData.endDate, timeZone).format('YYYYMMDD[T]HHmmss[Z]'); // Format as YYYYMMDDTHHMMSSZ in UTC
            createObj.recurrence = createObj.recurrence[0] + ';UNTIL=' + untilDate;
        }


    }

    calendar.events.insert({
        calendarId: 'primary', // Use 'primary' for the primary calendar of the authenticated user
        resource: createObj,
    }, (err, response) => {
        if (err) {
            console.error('Error inserting event:', err);
            return;
        }
        console.log('Event created: %s', response.data);
        res.status(200).json({ status: true });
    });
});
router.post('/google_update', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    var user = await Users.findOne({ id: decode.id }).lean();
    if (!user || !user.googleTokens) { console.log('User or Google token not found.'); return; }
    oauth2Client.setCredentials(user.googleTokens);
    let newToken = await oauth2Client.refreshAccessToken();
    oauth2Client.setCredentials(newToken.credentials);
    // Create a Google Calendar API client
    const calendar = google.calendar({ version: 'v3', auth: oauth2Client });
    let timeZone = req.body.timeZone || "Greenwich Standard Time";
    let updateObj = {
        summary: req.body.title,
    };
    if (req.body.description) {
        updateObj.description = req.body.description;
    }
    let start_time = req.body.allDay ? moment.tz(req.body.start, timeZone).startOf('day').format() : moment.tz(req.body.start, timeZone).format();
    let end_time = req.body.allDay ? moment.tz(req.body.end, timeZone).endOf('day').add(1, 'seconds').format() : moment.tz(req.body.end, timeZone).format();
    if (req.body.start) {
        if (req.body.allDay) {
            updateObj.start = {
                date: start_time.split('T')[0], // Updated start time
                timeZone: timeZone,
            }

        } else {
            updateObj.start = {
                dateTime: start_time, // Updated start time
                timeZone: timeZone,
            }

        }

    }

    if (req.body.end) {
        if (req.body.allDay) {
            updateObj.end = {
                date: end_time.split('T')[0], // Updated start time
                timeZone: timeZone,
            }

        } else {
            updateObj.end = {
                dateTime: end_time, // Updated start time
                timeZone: timeZone,
            }
        }
    }

    calendar.events.update({
        calendarId: 'primary',
        eventId: req.body._id,
        requestBody: updateObj,
    }, (err, response) => {
        if (err) {
            console.error('Error updating event:', err);
            return;
        }
        console.log('Event updated:', response.data);
        res.status(200).json({ status: true, data: response.data });
    });
});

router.post('/google_delete', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    var user = await Users.findOne({ id: decode.id }).lean();
    if (!user || !user.googleTokens) { console.log('User or Google token not found.'); return; }
    oauth2Client.setCredentials(user.googleTokens);
    let newToken = await oauth2Client.refreshAccessToken();
    oauth2Client.setCredentials(newToken.credentials);
    // Create a Google Calendar API client
    const calendar = google.calendar({ version: 'v3', auth: oauth2Client });
    const response = await calendar.events.delete({
        calendarId: 'primary', // Use 'primary' for the primary calendar or specify the calendar ID
        eventId: req.body._id
    });
    console.log('Event deleted successfully');
    res.status(200).json({ status: true, data: response.data });

});
// system
router.get('/list', async (req, res) => {
    try {
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        let events = await Calendar.find({ company_id: decode.company_id }).sort({ start_date: 1 }).lean();
        let user = await Users.findOne({ id: decode.id }).select('google_id outlook_id').lean();
        return res.status(200).json({
            user: user,
            events: events ? events.map(v => ({
                _id: String(v._id),
                title: v.title,
                start: new Date(v.start_date),
                end: new Date(v.end_date),
                description: v.description,
                type: "freeli",
                eventColor: 'blue'
            })) : []
        });


    } catch (e) {
        return res.status(400).json({ error: 'delete unit Error 2', message: e });
    }
});

router.post('/insert', async (req, res) => {
    try {
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        let data = {};
        data.company_id = decode.company_id;
        data.created_by = decode.id;

        if (req.body.title) data.title = req.body.title;
        if (req.body.start) data.start_date = req.body.start;
        if (req.body.end) data.end_date = req.body.end;
        if (req.body.description) data.description = req.body.description;

        let newEvent = new Calendar(data);
        let event = await newEvent.save();
        return res.status(200).json({ event });


    } catch (e) {
        return res.status(400).json({ error: 'delete unit Error 2', message: e });
    }
});

router.post('/update', async (req, res) => {
    try {
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);

        let data = {};
        if (req.body.title) data.title = req.body.title;
        if (req.body.start) data.start_date = req.body.start;
        if (req.body.end) data.end_date = req.body.end;
        if (req.body.description) data.description = req.body.description;

        let newData = await Calendar.findOneAndUpdate({ _id: new Object(req.body._id) }, data, {
            returnDocument: 'after'  // Return the updated document
        });

        return res.status(200).json({ newData });


    } catch (e) {
        return res.status(400).json({ error: 'delete unit Error 2', message: e });
    }
});

router.post('/delete', async (req, res) => {
    try {
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        let status = await Calendar.deleteOne({ _id: new Object(req.body._id) })

        return res.status(200).json({ status });


    } catch (e) {
        return res.status(400).json({ error: 'delete unit Error 2', message: e });
    }
});





module.exports = router;
