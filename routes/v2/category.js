const isUuid = require('uuid-validate');
const jwt = require('jsonwebtoken');

var {validateCreateTeamInput} = require('../../validation/team');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var {count_category, get_bunit} = require('../../v2_utils/business_unit');
var { get_user } = require('../../v2_utils/user');
var { xmpp_send_server } = require('../../v2_utils/voip_util');
// var {models} = require('../../config/db/express-cassandra');
var BusinessUnit = require('mongoose').model('business_unit')
var Conversation = require('mongoose').model('conversation')
const { v4: uuidv4 } = require('uuid');

/**
 * Get team list
 */
async function routes (fastify, options) {
    fastify.get('/get_lists', async (req, res) => {
        console.log(17);
        const token = extractToken(req);
        var decode = await token_verify_decode(token);
        try {
            var all_users = await get_user({company_id: decode.company_id});
            return res.status(200).send({ 
                success: true, 
                category: await get_bunit(decode.company_id, all_users)
            });
        } catch (e) {
            return res.status(400).json({ error: 'Get room category error ', message: e });
        }
    });
    
    /*
    Create or update team
    */
    fastify.post('/create_update', (req, res) => {
        if (! req.body.title) {
            return res.status(400).json({ error: 'Validation error', message: "Title field required." });
        }
        try{
            const token = extractToken(req);
            let decode = jwt.verify(token, process.env.SECRET);
            console.log(36, req.body);
    
            if(req.body.unit_id){
                try{
                    BusinessUnit.updateOne({unit_id: req.body.unit_id, company_id: decode.company_id}, {unit_name: req.body.title}, async function(err,result){
                        if(err) console.log(err);
                        
                        all_bunit[decode.company_id] = await get_bunit(decode.company_id);
                        return res.status(200).json({status:true, data: all_bunit[decode.company_id] });
                    });
                }catch(e){
                    return res.status(400).json({ error: e });
                }
            }else{
                var data = {
                    company_id: decode.company_id,
                    unit_id: uuidv4(),
                    created_by: decode.id,
                    unit_name: req.body.title,
                    user_id: decode.id
                };
    
                new BusinessUnit(data).save().then(async ()=>{
                    all_bunit[decode.company_id] = await get_bunit(decode.company_id);
                    return res.status(200).json({status:true, data: all_bunit[decode.company_id] });
                }).catch((err)=>{
                    return res.status(400).json({ error: err.name, message: err.message });
                });
            }
        }catch(e){
            return res.status(400).json({ error: 'create update category Error 2', message: e });
        }
    });
    
    /*
    Delete team
    */
    fastify.post('/delete', (req, res) => {
        if (!isUuid(req.body.unit_id)) {
                return res.status(400).json({ error: 'Validation error', message: "Unit id invalid" });
        }
        try{
            const token = extractToken(req);
            let decode = jwt.verify(token, process.env.SECRET);
            // console.log(93, req.body);
            Conversation.find({b_unit_id: req.body.unit_id},  function(err, convs){
                if(err) return res.status(400).json({ error: 'Find error', message: err });
                if(convs.length == 0){
                    BusinessUnit.deleteOne({unit_id: req.body.unit_id, company_id: decode.company_id}, async function(error){
                        if(error) return res.status(400).json({ error: 'Find error', message: err });
                        else {
                            all_bunit[decode.company_id] = await get_bunit(decode.company_id);
                            return res.status(200).json({ status: true, message: 'Category delete successfully', unit_id: req.body.unit_id });
                        }
                    })
                }
                else{
                    // console.log(convs);
                    return res.status(400).json({ status: true, message: 'Category already used', unit_id: req.body.unit_id });
                }
            });
        }catch(e){
            return res.status(400).json({ error: 'delete unit Error 2', message: e });
        }
    });
}



module.exports = routes;
