const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const isUuid = require('uuid-validate');
const { v4: uuidv4 } = require('uuid');

var {validateCreateTagInput, validateAddRemoveMsgTag, validateFavouriteUnfavouriteTagInput} = require('../../validation/tag');
var {extractToken, token_verify_decode} = require('../../v2_utils/jwt_helper');
var {get_message, get_tags, get_tags2, get_conversation} = require('../../v2_utils/message');
var {get_one_tag, update_tag_count, findAFile, get_company_untag_id} = require('../../v2_utils/tag');
var { get_user_obj } = require('../../v2_utils/user');
var { xmpp_send_server, xmpp_send_broadcast } = require('../../v2_utils/voip_util');
var { znTagMsgUpdate } = require('../../v2_utils/zookeeper');
// var {models} = require('../../config/db/express-cassandra');
const Tag = require('../../mongo-models/tag');
const File = require('../../mongo-models/file');
const Messages = require('../../mongo-models/message');
const Conversation = require('../../mongo-models/conversation');

// function load_untag(){
//     return new Promise((resolve, reject)=>{
//         Tag.find({ title: 'UNTAGGED FILES' }, function(e, untags){
//             if(e) reject(e);
//             for(let i=0; i<untags.length; i++){
//                 all_untags[untags[i].company_id] = untags[i];
//                 // console.log(`company ${untags[i].company_id} loded`)
//             }
//         });
//     });
// } 
// setTimeout(load_untag, 2000);

/**
 * Get tag list
 * tag_id = query string/ if send then that tag details are send
 * conversation_id
 */
router.get('/get_tag_lists', async (req, res) => {
    const token = extractToken(req);
    var decode = await token_verify_decode(token);
    try{
        if(req.query.tag_id){
            var result = await get_one_tag(req.query.tag_id);
            File.find({tag_list: {$in: req.query.tag_id}}, async function(e, files){
                if(e) return res.status(400).json({ error: 'Files find error', message: e });
                if(files.length>0){
                    var c = [];
                    var all_users = await get_user_obj({company_id: decode.company_id});
                    files.forEach(function(v, k){
                        v.conversation_name = "";
                        v.user_name = "";
                        if(isUuid(v.conversation_id.toString()) && c.indexOf(v.conversation_id) == -1) 
                            c.push(v.conversation_id);
                        if(isUuid(v.user_id.toString())) {}
                            v.user_name = all_users[v.user_id.toString()].fullname;
                    });
                    var convs = await get_conversation({conversation_id: c, user_id: decode.id});
                    convs.forEach(function(v, k){
                        for(let n=0; n<files.length; n++){
                            if(files[n].conversation_id.toString() === v.conversation_id.toString()){
                                files[n].conversation_name = v.title;
                                break;
                            }
                        }
                    });
                    return res.status(200).json({tag: result, files});
                
                }
                else{
                    return res.status(200).json({tag: result, files: []});
                }
            });
        }else{
            let arg = {id: decode.id, company_id: decode.company_id, from: req.query.type ? req.query.type : 'all'};
            if(isUuid(req.query.conversation_id))
                arg.conversation_id = req.query.conversation_id;
            if(isUuid(req.query.team_id))
                arg.team_id = req.query.team_id;
            if(req.query.upload_type) arg.upload_type = req.query.upload_type;
            var result = await get_tags(arg);
            return res.status(200).json(result);
        }
    }catch(e){
        console.log(e)
        return res.status(400).json(e);
    }
});

router.get('/others_tag_details', async (req, res) => {
    // Read the contents of the directory

    const token = extractToken(req);
    var decode = await token_verify_decode(token);
    
    if(! isUuid(req.query.tag_id) && !isUuid(req.query.conversation_id))
        return res.status(200).json({ status: false, message: 'No data found', other_tag_details: [req.query.tag_id], conversation_id: req.query.conversation_id });

    let tagids = [req.query.tag_id];
    let query = {tag_list: {$in: req.query.tag_id}, participants: {$in: decode.id}};
    if(isUuid(req.query.conversation_id)){
        query.conversation_id = req.query.conversation_id;
    }
    let filelist = await File.find(query);

    for (let i=0; i<filelist.length; i++) {
        for (let j=0; j<filelist[i].tag_list.length; j++) {
            if (tagids.indexOf(filelist[i].tag_list[j]) === -1) {
                tagids.push(filelist[i].tag_list[j]);
            }
        }
    }
    
    let other_tag_details = await Tag.find({tag_id: {$in: tagids}}).select('_id tag_id title tag_type tag_color');

    return res.status(200).json({ status: true, other_tag_details });
    

    // try {
    //     const tag_id = req.query.tag_id.replace(/['"]/g, '');


    //     // const taglist = await Tag.find();
    //     // const filelist = await File.find();


    //     File.aggregate([
    //         {
    //             $match: {
    //                 // company_id: decode.company_id, 
    //                 // participants: decode.id,  // Filter documents where participants array contains the targetUserId
    //                 tag_list: tag_id, // Filter documents where tags array contains the targetTagId
    //             },
    //         },
    //         {
    //             $sort: {
    //                 tag_list: -1, // Sort by presence of the targetTagId (descending order)
    //                 //  participants: -1, // Sort by presence of the targetUserId (descending order)
    //             },
    //         },
    //     ])
    //         .exec(async (err, filelist) => {
    //             if (err) {
    //                 console.error('Error:', err);
    //             } else {
    //                 console.log('Sorted File List:', filelist);

    //                 const other_tag = [];

    //                 console.log(97, filelist.length);

    //                 if (filelist.length > 0) {
    //                     for (let i = 0; i < filelist.length; i++) {
    //                         // console.log(193,filelist[i].tag_list+" "+i);
    //                         if (filelist[i].tag_list !== null) {
    //                             if (filelist[i].tag_list.includes(tag_id)) {
    //                                 other_tag.push(...filelist[i].tag_list);
    //                                 // console.log(193,filelist[i].originalname+" "+i);
    //                             } else {
    //                                 // console.log(194,filelist[i].originalname+" "+i);

    //                             }
    //                         }
    //                     }
    //                 } else {
    //                     return res.status(200).json({ status: false, message: "file list empty" });
    //                 }

    //                 if (other_tag.length === 0) {
    //                     return res.status(200).json({ status: false, message: "No tag found in file" });
    //                 }

    //                 //console.log(109,other_tag);
    //                 const uniqueIdArray = [...new Set(other_tag)];


    //                 console.log(111, uniqueIdArray);
    //                 const other_tag_details = [];

    //                 for (let i = 0; i < uniqueIdArray.length; i++) {
    //                     //console.log(114, uniqueIdArray[i]);
    //                     const tag_u_id = uniqueIdArray[i];
    //                     // const tag_detalis = await Tag.findById(tag_id);

    //                     const tag = await Tag.findOne({ tag_id: tag_u_id });

    //                     let total_file = 0;
    //                     for (let j = 0; j < filelist.length; j++) {

    //                         if (filelist[j].tag_list !== null) {

    //                             if (filelist[j].tag_list.includes(uniqueIdArray[i])) {
    //                                 total_file = total_file + 1;
    //                             }
    //                         }

    //                     }
    //                     //  console.log(215,uniqueIdArray[i]+" "+total_file);
    //                     if (tag !== null) {

    //                         other_tag_details.push({ files: total_file, ...JSON.parse(JSON.stringify(tag)) });
    //                     }

    //                 }

    //                 //  console.log(242,other_tag_details.length);
    //                 if (other_tag_details.length === 0) {
    //                     return res.status(200).json({ status: false, message: 'Tag not found' });
    //                 } else {
    //                     return res.status(200).json({ status: true, message: 'success', others_tag: other_tag_details });
    //                 }

    //             }
    //         });

    // } catch (e) {
    //     console.log(e);
    //     return res.status(400).json(e);
    // }


});

/**
 * this route use for get tag list counter
 * conversation_id = optional
 * used_by = null/ me/ others
 */
router.get('/get_tag_list_counter', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    try{
        let data = {id: decode.id, company_id: decode.company_id};
        if(isUuid(req.query.conversation_id))
            data.conversation_id = req.query.conversation_id;
        if(req.query.start_date)
            data.start_date = req.query.start_date;
        if(req.query.end_date)
            data.end_date = req.query.end_date;
        if(req.query.used_by === 'me' || req.query.used_by === 'others')
            data.used_by = req.query.used_by;
        
        // console.log(149, data);
        var result = await get_tags2(data);
        // console.log(151, result.result.length);
        // models.instance.Tagcount.stream(query, {raw: true, allow_filtering: true}, function(reader){
        //     var row;
        //     while (row = reader.readRow()) {
        //         tag[row.tag_id] = tag[row.tag_id] ? tag[row.tag_id] + 1 : 1;
        //     }
        // }, function(e){
        //     if(e) console.log(160, e);
        //     else {
        //         for(let i=0; i<all_tag.result.length; i++){
        //             console.log(163, all_tag.result[i].title);
        //             all_tag.result[i].counter = tag[all_tag.result[i].tag_id] ? tag[all_tag.result[i].tag_id] : 0;
        //             delete all_tag.result[i].use_count;
        //             delete all_tag.result[i].connected_user;
        //             delete all_tag.result[i].user_use_count;
        //             delete all_tag.result[i].my_use_count;
        //             if(all_tag.result[i].counter == 0 && all_tag.result[i].tag_type == 'public'){
        //                 console.log("O open for personal tag"); 
        //                 continue;
        //             }
        //             result.push(all_tag.result[i]);
        //         }
        //         console.log(174, all_tag.result.length);
        //         return res.status(200).json(result);
        //     }
        // });
        return res.status(200).json({result: result.result});
    }catch(e){
        return res.status(400).json(e);
    }
});

/**
 * this router use for create or update tag
 * 
 * tag_id = required for update a tag
 * title
 * tagged_by
 * tag_type = public/ private
 * tag_color
 * team_list = array
 * mention_users = array
 */


router.post('/create_new_tag', async (req, res) => {
    const { errors, isValid } = validateCreateTagInput(req.body);
    if (!isValid) {
		    return res.status(400).json({ error: 'Validation error', message: errors });
    }
    try{
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        // console.log(21, req.body);
        if(req.body.title.toLowerCase() === 'untagged files' || req.body.title.toLowerCase() === 'personal files' || req.body.title.toLowerCase() === '#TASK')
            return res.status(400).json({ error: 'Validation error', message: 'You can not use this tag title. It is reserved by system.' });
        var use_count = 0;
        var old_tag = {};
        if(isUuid(req.body.tag_id)){
            try{
                old_tag = await get_one_tag(req.body.tag_id);
                use_count = old_tag.use_count;
                // tag created by checking
                // if(old_tag.tagged_by.toString() !== decode.id)
                //     return res.status(400).json({ error: 'Tag edit error', message: "This tag not created by you." });
            }catch(e){
                return res.status(400).json({ error: 'Tag find error', message: e });
            }
        }
        var alltags = await get_tags(decode);
        if(req.body.tag_type === 'public'){
            for(let i = 0; i<alltags['public_tag'].length; i++){
                if(alltags['public_tag'][i].title.toLowerCase() == req.body.title.toLowerCase()){
                    if(! isUuid(req.body.tag_id) || (isUuid(req.body.tag_id) && alltags['public_tag'][i].tag_id !== req.body.tag_id)){
                        return res.status(400).json({ error: 'Tag duplicate error', message: "Tag name already exists." });
                    }
                }
            }
        }
        else if(req.body.tag_type === 'private'){
            for(let i = 0; i<alltags['private_tag'].length; i++){
                if(alltags['private_tag'][i].title.toLowerCase() == req.body.title.toLowerCase()){
                    if(! isUuid(req.body.tag_id) || (isUuid(req.body.tag_id) && alltags['private_tag'][i].tag_id !== req.body.tag_id)){
                        return res.status(400).json({ error: 'Tag duplicate error', message: "Tag name already exists." });
                    }
                }
            }
        }
        var newtag = {
            tag_id          : isUuid(req.body.tag_id) ? req.body.tag_id :uuidv4(),
            tagged_by       : req.body.tagged_by,
            title           : req.body.title.toString(),
            type            : 'tag',
            tag_type        : req.body.tag_type,
            tag_color       : req.body.tag_color ? req.body.tag_color : '#023d67',
            company_id      : decode.company_id.toString(),
            use_count       : use_count
        };
        
        newtag.mention_users = Array.isArray(req.body.mention_users) ? req.body.mention_users : isUuid(req.body.tag_id) ? old_tag.mention_users : [];
        if(req.body.team_list !== undefined){
            if(Array.isArray(req.body.team_list))
                newtag.team_list = req.body.team_list;
            else
                newtag.team_list = [];
        }
        else if(isUuid(req.body.tag_id))
            newtag.team_list = old_tag.team_list;
        else
            newtag.team_list = [];

        newtag.shared_tag = req.body.shared_tag ? req.body.shared_tag : isUuid(req.body.tag_id) ? old_tag.shared_tag : "";
        if(isUuid(req.body.tag_id) && !_.isEmpty(old_tag)){
            newtag.conversation_ids = old_tag.conversation_ids;
            newtag.connected_user_ids = old_tag.connected_user_ids;
            newtag.connected_user = old_tag.connected_user;
            newtag.user_use_count = old_tag.user_use_count;
            newtag.my_use_count = old_tag.my_use_count;
            newtag.favourite = old_tag.favourite;
            newtag.update_by = decode.id;
            newtag.updated_at = new Date();
            
            await Tag.findOneAndUpdate({_id: old_tag._id}, newtag);
            newtag._id = old_tag._id;
            newtag.conversation_ids = "";
            newtag.connected_user_ids = "";
            newtag.connected_user = [];
            newtag.team_list_name = [];
            if(newtag.team_list.length>0){
                for(let i=0; i<newtag.team_list.length; i++){
                    for(let j=0; j<all_teams[decode.company_id].length; j++){
                        if(all_teams[decode.company_id][j].team_id.toString() === newtag.team_list[i]){
                            newtag.team_list_name.push(all_teams[decode.company_id][j].team_title);
                            break;
                        }
                    }
                }
            }else if(newtag.team_list.length === 0){
                newtag.team_list_name.push("All Teams");
            }
            xmpp_send_broadcast("create_new_tag", {status:true, data:newtag});
            return res.status(200).json({status:true, data:newtag});
        }else{
            new Tag(newtag).save().then(()=>{
                newtag.created_by_name = "";
                if(all_users[decode.company_id].hasOwnProperty(req.body.tagged_by))
                newtag.created_by_name = all_users[decode.company_id][req.body.tagged_by].firstname + ' ' + all_users[decode.company_id][req.body.tagged_by].lastname;
                newtag.team_list = Array.isArray(newtag.team_list) ? newtag.team_list : [];
                newtag.team_list_name = [];
                newtag.favourite = [];
                if(newtag.team_list.length>0){
                    for(let i=0; i<newtag.team_list.length; i++){
                        for(let j=0; j<all_teams[decode.company_id].length; j++){
                            if(all_teams[decode.company_id][j].team_id.toString() === newtag.team_list[i]){
                                newtag.team_list_name.push(all_teams[decode.company_id][j].team_title);
                                break;
                            }
                        }
                    }
                }
                // console.log(226, newtag)
                xmpp_send_broadcast("create_new_tag", {status:true, data:newtag});
                return res.status(200).json({status:true, data:newtag});
            }).catch((err)=>{
                return res.status(400).json({ error: err.name, message: err.message });
            });
        }
    }catch(e){
        console.log(e)
        return res.status(400).json({ error: 'create_new_tag Error 2', message: e });
    }
});

/**
 * this router is used for delte an unused tag
 * 
 * tag_id
 */
router.delete('/delete', (req, res) => {
    if (! isUuid(req.body.tag_id)) {
        return res.status(400).json({ error: 'Validation error', message: "tag id invalid" });
    }
    try{
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        // console.log(106, req.body);
        var query = {tag_id:req.body.tag_id, company_id: decode.company_id};
        Tag.findOne(query, function(err, tag){
            if(err) return res.status(400).json({ error: 'Tag find Error', message: err });
            if(tag){
                File.find({tag_list: {$in: [req.body.tag_id]}}, function(e, fileinfo){
                    if(e) return res.status(400).json({ error: 'Files tag find Error', message: e });
                    if(fileinfo.length > 0){
                        return res.status(400).json({ error: 'Files tag use', message: "Tag already used" });
                    }
                    Tag.deleteOne(query, function(error){
                        if(error) return res.status(400).json({ error: 'Tag delete error', message: error });
                        xmpp_send_broadcast("tag_delete", tag);
                        return res.status(200).json(tag);
                    });
                });
            }else{
                return res.status(400).json({ error: 'Error', message: "No tag found" });
            }
        });
    }catch(e){
        return res.status(400).json({ error: 'delete tag error', message: e });
    }
});


function check_msg_has_any_other_file_with_same_tag(req, file){
    //Check have any other file in this message
    // models.instance.File.find({company_id: file.company_id, msg_id: file.msg_id}, {raw: true, allow_filtering: true}, function(e, of){
    //     if(e) console.log(313, e);
    //     var tag_list_all = [];
    //     for(let i=0; i<of.length; i++){
    //         of[i].tag_list = Array.isArray(of[i].tag_list) ? of[i].tag_list : [];
    //         tag_list_all = [...tag_list_all, ...of[i].tag_list];
    //     }
    //     tag_list_all = [...new Set(tag_list_all)]; //remove duplicate
    //     console.log(319, tag_list_all);
    //     //update message tag
    //     if(req.body.removetag.length === 1 && tag_list_all.indexOf(req.body.removetag[0]) === -1){
    //         models.instance.Messages.findOne({conversation_id: file.conversation_id, msg_id: file.msg_id}, {raw: true, allow_filtering: true}, function(ee, msgdata){
    //             if(ee) console.log(324, ee);
    //             msgdata.has_tag_text = Array.isArray(msgdata.has_tag_text) ? msgdata.has_tag_text : [];
    //             if(msgdata.has_tag_text.length > 0 && msgdata.has_tag_text.indexOf(req.body.removetag[0]) > -1){
    //                 models.instance.Messages.update({conversation_id: file.conversation_id, msg_id: file.msg_id}, {has_tag_text: tag_list_all}, function(er){
    //                     if(er) console.log(326, er);
    //                     else {
    //                         console.log(321, "msg update");
    //                         setTimeout(function(){
    //                             req.body.type = 'remove';
    //                             req.body.file_id = req.body.conversation_id;
    //                             (req.body.participants).forEach(function(v, k){
    //                                 xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
    //                             });
    //                         }, 2000);                                        
    //                     }
    //                 });
    //             }
    //         });
    //     }
    // });
}

/*
Add or update tag into file
*/
router.post('/add_remove_tag_into_file', async (req, res) => {
    const { errors, isValid } = validateAddRemoveMsgTag(req.body);
    if (!isValid) {
        return res.status(400).json({ error: 'Validation error', message: errors });
    }
    try{
        // console.log(80, req.body);
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        var query = {id: String(req.body.file_id), company_id: decode.company_id };
        var tag_list_with_user = [];
        req.body.newtag = Array.isArray(req.body.newtag) ? req.body.newtag : [];
        req.body.removetag = Array.isArray(req.body.removetag) ? req.body.removetag : [];
        // console.log(303, req.body);
        try{
            var fileinfo = await findAFile({company_id: decode.company_id.toString(), file_id: req.body.file_id});
            
            if(fileinfo.tag_list_with_user !== null){
                tag_list_with_user = JSON.parse(fileinfo.tag_list_with_user);
            }
        }catch(e){
            console.log(293, e);
        }
        // return res.status(200).json({ status: true });
        if(req.body.newtag.length > 0 && req.body.removetag.length > 0){
            // console.log(315);
            tag_list_with_user = Array.isArray(tag_list_with_user) ? tag_list_with_user : [];
            for(let i=0; i<req.body.newtag.length; i++){
                tag_list_with_user.push({"tag_id": req.body.newtag[i], "created_by": decode.id});
            }
            
            var fileinfo = await File.findOneAndUpdate(query, { $push: { tag_list: { $each: req.body.newtag } } , tag_list_with_user: JSON.stringify(tag_list_with_user)},{new: true});

            await update_tag_count({tag_id: req.body.newtag, company_id: decode.company_id, val: 1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids: [req.body.file_id], recursive: false });

            // var fileinfo = await findAFile({company_id: decode.company_id, file_id: req.body.file_id});
            try{
                if(fileinfo.tag_list_with_user !== null){
                    var db_tag_list_with_user = JSON.parse(fileinfo.tag_list_with_user);
                    // console.log(291, db_tag_list_with_user, req.body.removetag);
                    for(let i = 0; i<db_tag_list_with_user.length; i++){
                        if(req.body.removetag.indexOf(db_tag_list_with_user[i].tag_id) > -1 && db_tag_list_with_user[i].created_by === decode.id){
                            db_tag_list_with_user.splice(i, 1);
                            break;
                        }
                    }
                    tag_list_with_user = JSON.stringify(db_tag_list_with_user);
                }
            }catch(e){
                console.log(293, e);
            }
            
            File.updateOne(query, { $pull: { tag_list: {$in: req.body.removetag} }, tag_list_with_user},async function(err2,result2){
                if(err2){
                    return res.status(400).json({ error: 'Update error2', message: err2 });
                }else{
                    await update_tag_count({tag_id: req.body.removetag, company_id: decode.company_id, val: -1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids: [req.body.file_id], recursive: false});
                    // resolve({status:true});
                    // io.emit("add_remove_tag_into_msg", req.body);
                    req.body.type = 'add_remove';
                    (req.body.participants).forEach(function(v, k){
                        for(let r=0; r<req.body.newtag_tag_data.length; r++){
                            req.body.newtag_tag_data[r].favourite = ! Array.isArray(req.body.newtag_tag_data[r].favourite) ? [] : req.body.newtag_tag_data[r].favourite;
                            req.body.newtag_tag_data[r].connected_user_ids = "";
                            req.body.newtag_tag_data[r].conversation_ids = "";
                            req.body.newtag_tag_data[r].favourite = req.body.newtag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                        }
                        for(let r=0; r<req.body.removetag_tag_data.length; r++){
                            req.body.removetag_tag_data[r].favourite = ! Array.isArray(req.body.removetag_tag_data[r].favourite) ? [] : req.body.removetag_tag_data[r].favourite;
                            req.body.removetag_tag_data[r].connected_user_ids = "";
                            req.body.removetag_tag_data[r].conversation_ids = "";
                            req.body.removetag_tag_data[r].favourite = req.body.removetag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                        }
                        xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                    });
                    var mainmsg = (await get_message({ convid: req.body.conversation_id, msgid: req.body.msg_id, company_id: decode.company_id, is_reply: req.body.is_reply, limit: 1, user_id: decode.id, type: 'one_msg' })).msgs;

                    znTagMsgUpdate({...req.body, msg: mainmsg[0]});
                    return res.status(200).json({ status: true });
                }
            });
        
        }else{
            if(req.body.newtag.length > 0){
                tag_list_with_user = [];
                for(let i=0; i<req.body.newtag.length; i++){
                    // console.log(360, {"tag_id": req.body.newtag[i], "created_by": decode.id});
                    tag_list_with_user.push({"tag_id": req.body.newtag[i], "created_by": decode.id});
                }
                
                let updatedata = {tag_list_with_user: JSON.stringify(tag_list_with_user), has_tag: 'tag'};
                if(Array.isArray(fileinfo.tag_list)){
                    updatedata.$push = { tag_list: { $each: req.body.newtag } };
                }else{
                    updatedata.tag_list = req.body.newtag;
                }
                await File.updateOne(query, updatedata);
                await update_tag_count({tag_id: req.body.newtag, company_id: decode.company_id, val: 1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids: [req.body.file_id], recursive: false});

                req.body.type = 'add';
                // io.emit("add_remove_tag_into_msg", req.body);
                (req.body.participants).forEach(function(v, k){
                    for(let r=0; r<req.body.newtag_tag_data.length; r++){
                        req.body.newtag_tag_data[r].favourite = ! Array.isArray(req.body.newtag_tag_data[r].favourite) ? [] : req.body.newtag_tag_data[r].favourite;
                        req.body.newtag_tag_data[r].connected_user_ids = "";
                            req.body.newtag_tag_data[r].conversation_ids = "";
                        req.body.newtag_tag_data[r].favourite = req.body.newtag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                    }
                    // for(let r=0; r<req.body.removetag_tag_data.length; r++){
                    //     req.body.removetag_tag_data[r].favourite = ! Array.isArray(req.body.removetag_tag_data[r].favourite) ? [] : req.body.removetag_tag_data[r].favourite;
                    //     req.body.removetag_tag_data[r].connected_user_ids = "";
                    //     req.body.removetag_tag_data[r].conversation_ids = "";
                    //     req.body.removetag_tag_data[r].favourite = req.body.removetag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                    // }
                    xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                });

                var mainmsg = (await get_message({ convid: req.body.conversation_id, msgid: req.body.msg_id, company_id: decode.company_id, is_reply: req.body.is_reply, limit: 1, user_id: decode.id, type: 'one_msg' })).msgs;

                znTagMsgUpdate({...req.body, msg: mainmsg[0]});

                return res.status(200).json({ status: true });
                    
            }
            if(req.body.removetag.length > 0){
                tag_list_with_user = [];
                try{
                    var fileinfo = await findAFile({company_id: decode.company_id, file_id: req.body.file_id});
                    if(fileinfo.tag_list_with_user !== null){
                        var db_tag_list_with_user = JSON.parse(fileinfo.tag_list_with_user);
                        // console.log(341, db_tag_list_with_user, req.body.removetag);
                        for(let i = 0; i<db_tag_list_with_user.length; i++){
                            if(req.body.removetag.indexOf(db_tag_list_with_user[i].tag_id) > -1 && db_tag_list_with_user[i].created_by === decode.id){
                                db_tag_list_with_user.splice(i, 1);
                                break;
                            }
                        }
                        tag_list_with_user = JSON.stringify(db_tag_list_with_user);
                    }
                }catch(e){
                    console.log(351, e);
                }
                
                let updatedata = {tag_list_with_user};
                if(Array.isArray(fileinfo.tag_list)){
                    updatedata.$pull = { tag_list: { $in: req.body.removetag } };
                }
                File.updateOne(query, updatedata).then(async function(result) {
                   
                        await update_tag_count({tag_id: req.body.removetag, company_id: decode.company_id, val: -1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids: [req.body.file_id], recursive: false});
                        File.findOne(query, function(fe, file){
                            file.tag_list = Array.isArray(file.tag_list) ? file.tag_list : [];
                            //Check have any other file in this message
                            File.findOne({company_id: file.company_id, msg_id: file.msg_id}, function(of){
                                //if(e) console.log(313, e);
                                var tag_list_all = [];
                                for(let i=0; i<of.length; i++){
                                    of[i].tag_list = Array.isArray(of[i].tag_list) ? of[i].tag_list : [];
                                    tag_list_all = [...tag_list_all, ...of[i].tag_list];
                                }
                                tag_list_all = [...new Set(tag_list_all)]; //remove duplicate
                                // console.log(319, tag_list_all);
                                //update message tag
                                if(req.body.removetag.length === 1 && tag_list_all.indexOf(req.body.removetag[0]) === -1){
                                    Messages.findOne({conversation_id: file.conversation_id, msg_id: file.msg_id}, function(msgdata){
                                        //if(ee) console.log(324, ee);
                                        msgdata.has_tag_text = Array.isArray(msgdata.has_tag_text) ? msgdata.has_tag_text : [];
                                        if(msgdata.has_tag_text.length > 0 && msgdata.has_tag_text.indexOf(req.body.removetag[0]) > -1){
                                            Messages.updateOne({conversation_id: file.conversation_id, msg_id: file.msg_id}, {has_tag_text: tag_list_all}, function(er){
                                                if(er) console.log(326, er);
                                                else {
                                                    // console.log(321, "msg update");
                                                    setTimeout(function(){
                                                        req.body.type = 'remove';
                                                        req.body.file_id = req.body.conversation_id;
                                                        (req.body.participants).forEach(function(v, k){
                                                            for(let r=0; r<req.body.newtag_tag_data.length; r++){
                                                                req.body.newtag_tag_data[r].favourite = ! Array.isArray(req.body.newtag_tag_data[r].favourite) ? [] : req.body.newtag_tag_data[r].favourite;
                                                                req.body.newtag_tag_data[r].connected_user_ids = "";
                                                                req.body.newtag_tag_data[r].conversation_ids = "";
                                                                req.body.newtag_tag_data[r].favourite = req.body.newtag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                                                            }
                                                            for(let r=0; r<req.body.removetag_tag_data.length; r++){
                                                                req.body.removetag_tag_data[r].favourite = ! Array.isArray(req.body.removetag_tag_data[r].favourite) ? [] : req.body.removetag_tag_data[r].favourite;
                                                                req.body.removetag_tag_data[r].connected_user_ids = "";
                                                                req.body.removetag_tag_data[r].conversation_ids = "";
                                                                req.body.removetag_tag_data[r].favourite = req.body.removetag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                                                            }
                                                            xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                                                        });
                                                    }, 2000);                                        
                                                }
                                            });
                                        }
                                    });
                                }
                            });

                            if(file.tag_list.length == 0){
                                File.updateOne(query,{has_tag: 'null'}, function(err,result){
                                    if(err){
                                        return res.status(400).json({ error: 'Update error5', message: err });
                                    }
                                });
                            }
                        });
                        req.body.type = 'remove';
                        // io.emit("add_remove_tag_into_msg", req.body);
                        (req.body.participants).forEach(function(v, k){
                            // for(let r=0; r<req.body.newtag_tag_data.length; r++){
                            //     req.body.newtag_tag_data[r].favourite = ! Array.isArray(req.body.newtag_tag_data[r].favourite) ? [] : req.body.newtag_tag_data[r].favourite;
                            //     req.body.newtag_tag_data[r].connected_user_ids = "";
                            //     req.body.newtag_tag_data[r].conversation_ids = "";
                            //     req.body.newtag_tag_data[r].favourite = req.body.newtag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                            // }
                            for(let r=0; r<req.body.removetag_tag_data.length; r++){
                                req.body.removetag_tag_data[r].favourite = ! Array.isArray(req.body.removetag_tag_data[r].favourite) ? [] : req.body.removetag_tag_data[r].favourite;
                                req.body.removetag_tag_data[r].connected_user_ids = "";
                                req.body.removetag_tag_data[r].conversation_ids = "";
                                req.body.removetag_tag_data[r].favourite = req.body.removetag_tag_data[r].favourite.indexOf(v) > -1 ? [v] : [];
                            }
                            xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                        });

                        var mainmsg = (await get_message({ convid: req.body.conversation_id, msgid: req.body.msg_id, company_id: decode.company_id, is_reply: req.body.is_reply, limit: 1, user_id: decode.id, type: 'one_msg' })).msgs;

                        znTagMsgUpdate({...req.body, msg: mainmsg[0]});

                        return res.status(200).json({ status: true });
                    
                }).catch((e)=>{
                    return res.status(400).json({ error: 'Update error4', message: e }); 
                });
            }
        }
    }catch(e){
        console.log(457, e);
        return res.status(400).json({ error: 'add_remove_tag_into_msg Error 2', message: e });
    }
});

/*
Add or update tag into message
*/
router.post('/add_remove_tag_into_msg', async (req, res) => {
    req.body.file_id = req.body.conversation_id;
    const { errors, isValid } = validateAddRemoveMsgTag(req.body);
    if (!isValid) {
        return res.status(400).json({ error: 'Validation error', message: errors });
    }
    try{
        // console.log(338, req.body);
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        var mquery = {msg_id: req.body.msg_id, conversation_id: req.body.conversation_id};
        var queries = [];
        var file_ids = [];
        File.find(mquery, async function(err,result){
            if(err){
                return res.status(400).json({ error: 'Find error1', message: err });
            }else{
                if(result.length>0){
                    if(req.body.newtag.length > 0 && req.body.removetag.length > 0){
                        await Messages.updateOne(mquery, {$push: {has_tag_text: {$each: req.body.newtag}}});
                        await Messages.updateOne(mquery, {$pullAll: {has_tag_text: req.body.removetag}});
                        for(let i=0; i<result.length; i++){
                            file_ids.push(result[i].id.toString());
                            var fquery = {_id : result[i]._id};
                            if(Array.isArray(result[i].tag_list))
                                await File.updateOne(fquery, {has_tag: 'tag', $push: {tag_list: {$each: req.body.newtag}}});
                            else
                                await File.updateOne(fquery, {has_tag: 'tag', tag_list: req.body.newtag});

                            await File.updateOne(fquery, {$pullAll: {tag_list: req.body.removetag}});
                        }
                        await update_tag_count({tag_id: req.body.newtag, company_id: decode.company_id, val: 1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids});
                        await update_tag_count({tag_id: req.body.removetag, company_id: decode.company_id, val: -1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids});
                        req.body.type = 'add_remove';
                        (req.body.participants).forEach(function(v, k){
                            xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                        });
                    }
                    else{
                        if(req.body.newtag.length > 0){
                            await Messages.updateOne(mquery, {$push: {has_tag_text: {$each: req.body.newtag}}});
                            for(let i=0; i<result.length; i++){
                                file_ids.push(result[i].id.toString());
                                var fquery = {_id : result[i]._id};
                                if(Array.isArray(result[i].tag_list))
                                    await File.updateOne(fquery, {has_tag: 'tag', $push: {tag_list: {$each: req.body.newtag}}});
                                else
                                    await File.updateOne(fquery, {has_tag: 'tag', tag_list: req.body.newtag});
                            }
                            
                            await update_tag_count({tag_id: req.body.newtag, company_id: decode.company_id, val: 1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids});
                            req.body.type = 'add';
                            // io.emit("add_remove_tag_into_msg", req.body);
                            (req.body.participants).forEach(function(v, k){
                                xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                            });
                        }
                        if(req.body.removetag.length > 0){
                            await Messages.updateOne(mquery, {$pullAll: {has_tag_text: req.body.removetag}});
                            for(let i=0; i<result.length; i++){
                                file_ids.push(result[i].id.toString());
                                result[i].tag_list = Array.isArray(result[i].tag_list) ? result[i].tag_list : [];
                                var has_tag = result[i].tag_list.length <= req.body.removetag.length ? null : 'tag';
                                var fquery = {_id : result[i]._id};
                                let a = await File.updateOne(fquery, {has_tag: has_tag, $pullAll: {tag_list: req.body.removetag}});
                                console.log(636, a);
                            }
    
                            await update_tag_count({tag_id: req.body.removetag, company_id: decode.company_id, val: -1, conversation_id: req.body.conversation_id, msg_id: req.body.msg_id, participants: req.body.participants, user_id: decode.id, file_ids});
                            req.body.type = 'remove';
                            // io.emit("add_remove_tag_into_msg", req.body);
                            (req.body.participants).forEach(function(v, k){
                                xmpp_send_server(v, 'add_remove_tag_into_msg', req.body,req);
                            });
                        }
                    }
                }
                
                var mainmsg = (await get_message({ convid: req.body.conversation_id, msgid: req.body.msg_id, company_id: decode.company_id, is_reply: req.body.is_reply, limit: 1, user_id: decode.id, type: 'one_msg' })).msgs;

                znTagMsgUpdate({...req.body, msg: mainmsg[0]});

                return res.status(200).json({ message: "Done" });
            }
        });
    }catch(e){
        return res.status(400).json({ error: 'add_remove_tag_into_msg Error 2', message: e });
    }
});

/**
 * this router use for favourite/ unfavorite a tag
 * 
 * favo_data = [{
 *      tag_id = required for update a tag,
 *      status = 1 / 2 (1 = favourite, 2 = unfavorite)
 *      }]
 */
 router.patch('/favourite_unfavourite', async (req, res) => {
    const { errors, isValid } = validateFavouriteUnfavouriteTagInput(req.body);
    if (!isValid) {
		    return res.status(400).json({ error: 'Validation error', message: errors });
    }
    try{
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        // console.log(564, req.body);
        var queries = [];
        for(let i=0; i<req.body.favo_data.length; i++){
            var favourite = {};
            if(Number(req.body.favo_data[i].status) === 1){
                // favourite = {$add: [decode.id]};
                await Tag.updateOne({tag_id: String(req.body.favo_data[i].tag_id), company_id: decode.company_id}, { '$push': { favourite : decode.id } });
            }else{
                // favourite = {$remove: [decode.id]};
                await Tag.updateOne({tag_id: String(req.body.favo_data[i].tag_id), company_id: decode.company_id}, { '$pull': { favourite : decode.id } });
            }
            
           
        }
        // if(queries.length>0){
            // models.doBatch(queries, function(err){
                // if(err) throw err;
                // else{
                    console.log("Favourite Tag update");
                    xmpp_send_server(decode.id, 'tag_favourite_unfavourite', req.body,req);
                    return res.status(200).json(req.body);
                // }
            // });
        // }
    }catch(e){
        return res.status(400).json({ error: 'create_new_tag Error 2', message: e });
    }
});

router.post('/get_rooms_or_tags', async (req, res) => {
    const token = extractToken(req);
    var decode = await token_verify_decode(token);
    
    try {
        if (req.body.type === 'room') {
            if (req.body.tag_ids === null || req.body.tag_ids === undefined || req.body.tag_ids.length === 0) {
                return res.status(200).json({ status: false, message: 'tag id array required!' })
            }
            let query = {participants: decode.id};
            if(req.body.tag_ids.indexOf('untag') > -1){
                req.body.tag_ids.splice(req.body.tag_ids.indexOf('untag'), 1);
                query.$or = [{ tag_list: { $all: req.body.tag_ids } }, { tag_list: null} ];
            }
            else
                query.tag_list = { $all: req.body.tag_ids };

            // console.log(872, query);
            File.find(query).then(async (file) => {
                    const conversationIds = [];
                    file.forEach(tag => {
                        const cid = tag.conversation_id;
                        conversationIds.push(cid);
                    });

                    const uniqueIdArray = [...new Set(conversationIds)];
                    const cid_details = await get_conversation({conversation_id: uniqueIdArray, user_id: decode.id});
                    return res.status(200).json({ status: true, conversation_list: cid_details })
            }).catch((err)=>{
                console.error('Error:', err);
            })

        } else if (req.body.type === 'tag') {

            // find taglist
            if (req.body.conversation_ids == null || req.body.conversation_ids == undefined ||req.body.conversation_ids.length == 0) {
                return res.status(400).json({ status: false, message: 'conversation id required!' });
            }
            
            const tag_list_data = []
            File.find({conversation_id: { $in: req.body.conversation_ids }}).then(async (files) => {
                    // 'files' now contains an array of documents with the specified conversation ID
                    //console.log('Matching Documents:', files);


                    console.log('File length:', files.length);
                    for (let i = 0; files.length > i; i++) {
                        // console.log('Matching Documents:', files[i].tag_list);
                        if (files[i].tag_list !== null) {
                            tag_list_data.push(...files[i].tag_list)
                        }
                    }

                    const uniqueIdArray = [...new Set(tag_list_data)];

                    const tag_details = [];
                    for (let i = 0; i < uniqueIdArray.length; i++) {
                        //console.log(114, uniqueIdArray[i]);
                        const tag_u_id = uniqueIdArray[i];
                        const tag = await Tag.findOne({ tag_id: tag_u_id });
                        if (tag !== null) {
                            tag_details.push(tag);
                        }

                    }

                    return res.status(200).json({ status: true, tag_list: tag_details })
            }).catch((err)=>{
                console.error('Error:', err);
            });


        } else {
            return res.status(400).json({ status: false, message: 'type should be room or tag!'});
        }

    } catch (e) {
        console.log(e);
        return res.status(400).json(e);
    }


});

module.exports = router;
