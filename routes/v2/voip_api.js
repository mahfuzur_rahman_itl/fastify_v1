const express = require('express');
const router = express.Router();
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");
const short = require('short-uuid');
const jwt = require('jsonwebtoken');
const passport = require('passport');
var CryptoJS = require("crypto-js");
// var { models } = require('../../config/db/express-cassandra');
var { set_message, get_message, update_conversation_for_last_msg, fnln } = require('../../v2_utils/message');
const { validateMsgInput } = require('../../validation/message');
var { get_user_obj } = require('../../v2_utils/user');
var { count_company_users, update_company, billing_address } = require('../../v2_utils/company');
var { sg_email } = require('../../v2_utils/sg_email');
const Conversation = require('../../mongo-models/conversation');
const Users = require('../../mongo-models/user');
var Messages = require('../../mongo-models/message');
var Company = require('../../mongo-models/company');
var Payment = require('../../mongo-models/payment');
var MessagesEmojis = require('../../mongo-models/message_emoji');
var File = require('../../mongo-models/file');
var BusinessUnit = require('../../mongo-models/business_unit');
var Team = require('../../mongo-models/team');
const Tag = require('../../mongo-models/tag');

var moment = require('moment');
const fs = require("fs");
const { v4: uuidv4 } = require('uuid');
var {
    convertMS,
    update_conversation,
    delRingInterval,
    send_msg_firebase,
    updateJitsiMemberStatus,

    add2buffer,
    getConvUsers,
    addUserBusy,
    deleteUserBusy,
    hangup_conv_exe,
    get_one_msg,
    call_ringing_loop,
    checkRingStatus,
    isRealString,
    sendCallMsgDB,
    xmpp_send_server,
    xmpp_send_broadcast,
    deleteRingIndex,
    create_account_xmpp,
    register_firebase_token,
    xmppUserAdd,
    xmppUserRemove,
    xmpp_send_room
} = require('../../v2_utils/voip_util');
var { extractToken } = require('../../v2_utils/jwt_helper');
const { jsPDF } = require("jspdf");
const { resolve } = require('path');
// process.env.CRYPTO_SECRET = 'D1583ED51EEB8E58F2D3317F4839A';

// ============ strophe ======================================================
// var server = '192.168.0.108:5222';
// var jid = 'admin';
// var password = 'admin';

// // Requirements
// var strophe = require("node-strophe").Strophe;
// var Strophe = strophe.Strophe;

// // Set-up the connection
// var BOSH_SERVICE = 'http://' + server + '/ws';
// var connection = new Strophe.Connection(BOSH_SERVICE);

// // Log XMPP
// connection.rawInput = connection.rawOutput = console.log;

// // Connect
// connection.connect(jid, password, (status) => {
//     switch (status) {
//         case Strophe.Status.CONNECTED:
//             console.log('YEAH! Connection established.')
//             break
//         case Strophe.Status.DISCONNECTED:
//             console.log('Oh No... Connection disconnected.')
//             break
//     }
// });

//  var m = $msg({to: 'test1@192.168.0.108', from: 'sujon@192.168.0.108', type: 'chat'}).c("body").t(body);     
//    // custom data
//   //  m.up().c("data", {xmlns: 'my-custom-data-ns', field1: field1, field2: field2});
//    connection.send(m);
// =========================================================

// ==================== XMPP API ================================================================
router.post('/xmpp_online_user', async function (req, res, next) {
    if (req.body.user_id) {
        var xmpp_user = (req.body.user_id + '$$$' + req.body.token);
        xmppUserAdd(xmpp_user, req, true);

        res.status(200).json({ status: true });
    }
});

router.post('/xmpp_register_user', async function (req, res, next) {
    if (req.body.user_id && req.body.token) {
        var user_id = req.body.user_id;
        var xmpp_token = req.body.token;
        // xmpp register =================================================================
        var xmpp_user = (user_id + '$$$' + xmpp_token);
        var status = await create_account_xmpp(xmpp_user);
        if (status) {
            xmppUserAdd(xmpp_user, req, true);
        }
        var query = { participants: user_id };
        Conversation.find(query, ['conversation_id']).then(function (list) {
            var conv_list = []
            list.map((v) => {
                conv_list.push({ 'jid': `${String(v.conversation_id)}@conference.${xmpp_domain}`, 'nick': xmpp_user })

            })
            res.status(200).json({ status, xmpp_user: xmpp_user, conv_list: conv_list });
        });
        // models.instance.Conversation.find({ participants: { $contains: user_id } }, { select: ['conversation_id'], raw: true, allow_filtering: true }, function (ce, list) {
        //     var conv_list = []
        //     list.map((v) => {
        //         conv_list.push({ 'jid': `${String(v.conversation_id)}@conference.${xmpp_domain}`, 'nick': xmpp_user })

        //     })
        //     res.status(200).json({ status, xmpp_user: xmpp_user, conv_list: conv_list });
        // });


        // firebase register ============================================================
        // if (req.body.device) var device = req.body.device;
        // else var device = 'android';

        // if (device == 'android') {
        //     var firebase_token = req.body.token;
        //     await new Promise((resolve, reject) => {
        //         models.instance.Users.find({ 'fcm_id': { '$contains': device + '@@@' + firebase_token } }, { allow_filtering: true }, async function (error, user_fcm) {
        //             if (user_fcm && user_fcm.length) {
        //                 user_fcm.forEach((fcm, index, array) => {
        //                     models.instance.Users.update({ id: fcm.id }, { fcm_id: { '$remove': [device + '@@@' + firebase_token] } }, update_if_exists, async function (err) {
        //                         if (err) console.trace(err);
        //                         resolve();
        //                     });
        //                 })
        //             } else {
        //                 resolve();
        //             }
        //         });
        //     });

        //     models.instance.Users.update({ id: models.uuidFromString(user_id) }, { fcm_id: { '$add': [device + '@@@' + firebase_token] } }, update_if_exists, async function (err) {
        //         if (err) console.trace(err);
        //     });
        // }
    } else {
        res.status(200).json({ status: false });
    }
});

router.post('/xmpp_close_user', async function (req, res, next) {

    // if (req.body.user_id && req.body.token) {
    //     var user_id = req.body.user_id;
    //     var xmpp_token = req.body.token;
    //     var xmpp_user = (user_id + '$$$' + xmpp_token);

    //     xmppUserRemove(user_id, xmpp_user, req);

    //     if (req.body.firebase_token) {
    //         var firebase_token = req.body.firebase_token;
    //     } else {
    //         var firebase_token = req.body.token;
    //     }
    //     if (req.body.device) var device = req.body.device;
    //     else var device = 'android';

    //     models.instance.Users.update({ id: models.uuidFromString(req.body.user_id) }, { fcm_id: { '$remove': [device + '@@@' + firebase_token] } },
    //         update_if_exists, async function (err) {
    //             if (err) console.trace(err);
    //             res.status(200).json({ status: true });

    //         });

    // } else {
    //     res.status(200).json({ status: false });
    // }


});

router.post("/register_firebase_token", async (req, res) => { // for web
    if (req.body.firebase_token) {
        if (req.body.device) var device = req.body.device;
        else var device = 'android';
        var firebase_token = req.body.firebase_token;
        var user_id = req.body.user_id;

        await register_firebase_token(user_id, device, firebase_token);
        res.status(201).json({ 'status': 'true' });
    } else {
        res.status(201).json({ 'status': 'false' });
    }

});


router.post('/user_id_list', async function (req, res, next) {

    // models.instance.Users.find({ email: req.body.email }, { raw: true, allow_filtering: true }, async function (error, users) {
    //     if (users && users.length) {
    //         var user_list = users.map((v) => {
    //             return v;
    //             // return { id: String(v.id), name: v.firstname };
    //         })
    //         res.status(200).json({ user_list });

    //     }
    // });



});


router.post("/subscribe2", (req, res) => {
    // Send 201 - resource created
    res.status(201).json({ 'restart_time': restart_time });
});

function updateCallMemberList(conv_id, user_id, data, req) {
    if (voip_conv_store.hasOwnProperty(conv_id)) {
        let ring_data = voip_conv_store[conv_id];
        if (ring_data.participants_all.indexOf(user_id) === -1) ring_data.participants_all.push(user_id);

        data.active_participants = voip_conv_store[conv_id].active_participants;
        data.participants_status = ring_data.participants_status;
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

        ring_data.participants_all.forEach(uid => {
            xmpp_send_server(uid, 'jitsi_ring_status', data, req);
        });

    }

}
function getJwtPayload(user_id, conv_id) {
    if (voip_conv_store[conv_id]) {
        var conv_data = voip_conv_store[conv_id];
        var company_id = conv_data.company_id;
        // var user = conv_data.user_info.filter((v)=> v.id == user_id)
        var user = all_users[company_id][user_id];
        var user_avatar = user ? user.img : 'https://wfss001.freeli.io/profile-pic/Photos/img.png';
        var user_name = user ? (user.firstname + (user.lastname ? ' ' + user.lastname : '')) : 'Guest User';
        var user_email = user ? user.email : 'guest@guest.com';

        if (conv_data.conversation_type == "personal" && conv_data.arr_participants.length == 2) {
            var callee_id = conv_data.arr_participants.join().replace(user_id, '').replace(',', '');
            var callee_user = all_users[company_id][callee_id];
            // var callee_user = conv_data.user_info.filter((v)=> v.id == callee_id);
            var callee_name = callee_user ? (callee_user.firstname + (callee_user.lastname ? ' ' + callee_user.lastname : '')) : '';
            var callee_avatar = callee_user ? callee_user.img : 'https://wfss001.freeli.io/profile-pic/Photos/img.png';
        } else {
            var callee_id = user_id;
            var callee_name = conv_data.convname;
            var callee_avatar = user_avatar;
        }
        return {
            "context": {
                "user": {
                    "id": user_id,
                    "name": user_name,
                    "email": user_email,
                    "affiliation": conv_data.participants_all.indexOf(user_id) > -1 ? "owner" : "member",
                    "avatar": user_avatar,
                    "lobby_bypass": conv_data.participants_all.indexOf(user_id) > -1,
                },
                "callee": {
                    id: callee_id,
                    name: callee_name,
                    avatar: callee_avatar
                }
            },
            "aud": "jitsi", "iss": "jitsi", "sub": "*", "room": "*",
        };

    } else {
        return false;
    }


}
// ==================== JITSI API =============================================================
router.post('/jitsi_running_status', async function (req, res, next) {
    var data = req.body;
    let busy_conv = voip_busy_conv[data.conversation_id] ? true : false;
    let user_status = voip_busy_user[data.user_id] ? voip_busy_user[data.user_id] : false;
    let user_busy = voip_busy_user.hasOwnProperty(data.user_id);
    var ring_index = voip_conv_store[data.conversation_id] && voip_conv_store[data.conversation_id].ring_index_list[data.user_id];
    let plan_upgrade = false;
    if (voip_conv_store[data.conversation_id] && voip_conv_store[data.conversation_id].plan_name == 'starter' && voip_conv_store[data.conversation_id].active_participants.length >= 100) {
        plan_upgrade = true;
    }
    res.json({
        voip_busy_conv,
        user_status,
        busy_conv,
        ring_index,
        plan_upgrade,
        user_busy
    });


});
router.post('/jitsi_ring_calling', async function (req, res, next) { // store calling data here..
    const token = extractToken(req);
    if (!token) return;
    let decode = jwt.verify(token, process.env.SECRET);
    var data = req.body;
    data.init_time = Date.now();
    data.company_id = data.company_id ? data.company_id: decode.company_id;

    var conv_id = data.conversation_id;
    // data.plan_name = all_company[data.company_id].plan_name;
    data.caller_id = data.init_id = data.root_id = data.user_id;
    data.busy_conv = voip_busy_conv[data.conversation_id] ? true : false;
    data.plan_upgrade = false;
    // if (voip_conv_store[data.conversation_id] && voip_conv_store[data.conversation_id].plan_name == 'starter' && voip_conv_store[data.conversation_id].active_participants.length >= 100) {
    //     data.plan_upgrade = true;
    //     return res.json({ status: false, msg: 'plan_upgrade' });
    // }

    if (data.conversation_type == 'personal') {
        data.receiver_id = data.arr_participants.filter((v) => { return v != data.user_id });
        data.merge_status = voip_busy_user.hasOwnProperty(data.receiver_id);
        if (checkRingStatus(data.receiver_id) == true) {
            return res.json({ status: false, msg: 'Receiver is busy!', merge_status: data.merge_status, busy_conv: data.busy_conv });
        }
    } else {
        data.merge_status = false;
    }

    if (voip_busy_user.hasOwnProperty(data.user_id)) {
        let status = false;
        let msg = 'You are busy!';
        if (voip_busy_user[data.user_id].indexOf(conv_id) > -1) {
            status = true;
            msg = 'join'; // switch
        }
        let payload = getJwtPayload(data.user_id, data.conversation_id);
        jwt.sign(payload, "jitsi", { algorithm: 'HS256' }, async (err, jwt_token) => {
            return res.json({ status: status, msg: msg, merge_status: data.merge_status, busy_conv: data.busy_conv, jwt_token });
        });
    } else {
        if (data.busy_conv) {
            let payload = getJwtPayload(data.user_id, data.conversation_id);
            let jwt_token = await jwt.sign(payload, "jitsi", { algorithm: 'HS256' });
            return res.json({ status: true, msg: 'join', merge_status: data.merge_status, busy_conv: data.busy_conv, jwt_token });
        }
        else if (data.conversation_type == 'group') {
            if (data.arr_participants.length == 0) {
                return res.json({ status: false, msg: 'list', merge_status: data.merge_status, busy_conv: data.busy_conv });
            }
        }
        if (typeof data.arr_participants === 'string' && data.arr_participants != "false") data.arr_participants = JSON.parse(data.arr_participants);
        if (typeof data.participants_all === 'string' && data.participants_all != "false") data.participants_all = JSON.parse(data.participants_all);
        // if (typeof data.participants_admin === 'string' && data.participants_admin != "false") data.participants_admin = JSON.parse(data.participants_admin);
        if (typeof data.conversation_type != 'undefined') data.conversation_type = data.conversation_type.toLowerCase();
        if (!data.call_option) data.call_option = 'ring';
        if (!data.arr_participants) data.arr_participants = [];
        if (data.arr_participants && data.arr_participants.indexOf(data.user_id) === -1) data.arr_participants.push(data.user_id);
        // data.call_option = data.arr_participants.length > 1 ? 'ring' : 'window';
        data.participants_admin = [data.user_id];
        data.set_calltype = 'audio';
        // data.user_info = await Users.find({company_id: data.company_id});
        // var caller_info = data.user_info.filter((v)=> v.id == data.user_id);
        // data.caller_img = data.user_img = process.env.FILE_SERVER + 'profile-pic/Photos/' + caller_info[0].img;
        data.caller_img = data.user_img = all_users[data.company_id][data.user_id].img;
        data.caller_email = data.user_email = all_users[data.company_id][data.user_id].email;
        data.caller_fullname = data.user_fullname = (all_users[data.company_id][data.user_id].firstname + ' ' + (all_users[data.company_id][data.user_id].lastname || '')).trim();
        data.msg_body = CryptoJS.AES.encrypt('<p>Call in progress...</p>', process.env.CRYPTO_SECRET).toString();
        data.admin_users = [data.root_id];
        data.room_name = data.conversation_id ? data.conversation_id.replace(/-/g, "") : 'workfreeli';
        data.connected = false;
        data.ring_status = false;
        data.database_update = false;
        data.start_time = 0;
        data.call_duration = "";
        data.ring_index_list = {};
        data.device_list = {};
        if (!data.device_list[data.user_id]) data.device_list[data.user_id] = [];
        data.device_list[data.user_id].push(decode.device_id);
        // console.log('voip:::::::::::::jitsi__ring_calling', data.init_time, data.device_list[data.user_id]);
        data.conv_history = [];
        data.file_server = process.env.FILE_SERVER;
        data.participants_status = {};
        if (data.call_option != 'window') {
            data.participants_status[data.user_id] = 'Calling...';
            // console.log('part_status', data.user_id, data.participants_status[data.user_id]);
            // data.arr_participants.forEach((v) => { data.participants_status[v] = 'Calling...'; }); 

        }
        var old_participant_list = [];
        if (voip_conv_store[conv_id] && voip_conv_store[conv_id].active_participants && voip_conv_store[conv_id].active_participants.length) {
            old_participant_list = voip_conv_store[conv_id].active_participants;
        }

        delete voip_conv_store[conv_id];
        // calling data set ========
        voip_conv_store[conv_id] = data;
        if (old_participant_list.length) voip_conv_store[conv_id].active_participants = old_participant_list;
        else voip_conv_store[conv_id].active_participants = [];
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

        // timer set ========================================================
        voip_timer_ring[conv_id] = {};
        voip_timer_process[conv_id] = {};
        if (process.send) process.send({ type: 'voip_timer_ring', voip_timer_ring: voip_timer_ring });

        // ring =====================
        if (data.call_option == 'ring') {
            // console.log('RingInterval:set:this:', process.pid, conv_id, conv_id);
            voip_timer_ring[conv_id][conv_id] = { pid: process.pid };
            if (process.send) process.send({ type: 'voip_timer_ring', voip_timer_ring: voip_timer_ring });
            voip_timer_process[conv_id][conv_id] = {
                timer: setTimeout(() => {
                    hangup_conv_exe(conv_id, -1, false, req);
                    // console.log('RingInterval:timeout:conv:', process.pid, conv_id, conv_id);
                }, CALL_RING_TIME)
            }
        }
        voip_busy_conv[conv_id] = 'data.msgid';
        if (process.send) process.send({ type: 'voip_busy_conv', voip_busy_conv });
        for (const [i, v] of data.participants_all.entries()) {
            if (voip_conv_store.hasOwnProperty(conv_id)) {
                xmpp_send_server(v, 'jitsi_busy_status', {
                    conversation_id: conv_id,
                    user_busy: voip_busy_user.hasOwnProperty(v),
                    voip_busy_conv: voip_busy_conv,
                    busy_conv_list: Object.keys(voip_busy_conv),
                }, req);
            }
        }
        let payload = getJwtPayload(data.user_id, data.conversation_id);
        jwt.sign(payload, "jitsi", { algorithm: 'HS256' }, async (err, jwt_token) => {
            res.json({ status: true, msg: 'ring', merge_status: data.merge_status, busy_conv: data.busy_conv, jwt_token });
        });

        var res_msg = await sendCallMsgDB(data.user_id, data.sender_img, data.user_fullname, data.conversation_id, data.msg_body, '', data.set_calltype, 0, data.arr_participants, data.conference_id, true, 'called', null, '', data.company_id);
        if (res_msg.status) {
            // console.log('voip:::::::::::mongo:save::::::::::', Date.now() - data.init_time);
            data.msgid = String(res_msg.res);
            voip_conv_store[conv_id].msg_db = res_msg.msg;
            voip_conv_store[conv_id].msgid = data.msgid;


            // var lastmsg = await get_message({ convid: data.conversation_id, msgid: data.msgid, limit: 1, is_reply: 'no', user_id: data.user_id });
            var lastmsg = await get_message({ convid: data.conversation_id, company_id: data.company_id, msgid: data.msgid, limit: 1, is_reply: 'no', user_id: data.user_id, type: 'one_msg' });

            for (const [i, v] of data.participants_all.entries()) {
                xmpp_send_server(v, 'new_message', lastmsg.msgs[0], req);
                // send_msg_firebase(v, 'new_message', lastmsg[0]);
                // xmpp_send_server(v, 'jitsi_conv_start', {
                //     status: true,
                //     conversation_id: conv_id,
                //     msgid: data.msgid,
                //     call_duration: 0
                // }, req);
            }
            update_conversation_for_last_msg(lastmsg.msgs[0]);

            if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
        } else {
            console.error(res_msg);
            res.json({ status: false, msg: 'database' });
        }
    }
});
router.post('/jitsi_ring_users', function (req, res, next) {
    var data = req.body;
    var conv_id = data.conversation_id;
    if (voip_conv_store.hasOwnProperty(conv_id) && voip_conv_store[conv_id].database_update == false) {

        if (voip_conv_store[conv_id].plan_name == 'starter' && voip_conv_store[conv_id].active_participants.length >= 100) {
            return res.json({ status: false });
        }

        if (voip_conv_store[conv_id].active_participants.indexOf(data.user_id) === -1) voip_conv_store[conv_id].active_participants.push(data.user_id);
        if (voip_conv_store[conv_id].arr_participants.indexOf(data.user_id) === -1) voip_conv_store[conv_id].arr_participants.push(data.user_id);

        // updateJitsiMemberStatus(conv_id, receiver_id, 'accept');
        voip_conv_store[conv_id].participants_status[data.user_id] = 'In Call';
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

        let payload = getJwtPayload(data.user_id, data.conversation_id);
        if (payload) {
            jwt.sign(payload, "jitsi", { algorithm: 'HS256' }, async (err, jwt_token) => {
                res.json({
                    status: true,
                    jwt_token,
                    callee_avatar: payload.context.callee.avatar,
                    // users: Object.values(voip_conv_store[conv_id].user_info),
                    users: Object.values(all_users[voip_conv_store[conv_id].company_id]),
                    online_user_lists: Object.keys(online_user_lists),
                    voip_conv: voip_conv_store[conv_id],
                    msg_db: voip_conv_store[conv_id].msg_db,
                    conv_history: voip_conv_store[conv_id].conv_history.length,
                    active_participants: voip_conv_store[conv_id].active_participants,
                    participants_status: voip_conv_store[conv_id].participants_status
                });

                if (voip_conv_store[conv_id].ring_status == false) {
                    voip_conv_store[conv_id].ring_status = true;
                    if (voip_conv_store[conv_id].call_option == 'ring') {
                        for (const v of voip_conv_store[conv_id].arr_participants) {
                            if (v != voip_conv_store[conv_id].init_id) {
                                call_ringing_loop(conv_id, v, {
                                    conversation_id: conv_id,
                                    ring_index: conv_id + '_' + voip_conv_store[conv_id].init_id + '_' + v,
                                    receiver_id: v,
                                    user_id: voip_conv_store[conv_id].init_id,
                                    user_fullname: voip_conv_store[conv_id].caller_fullname,
                                    user_img: voip_conv_store[conv_id].caller_img,
                                    conversation_type: voip_conv_store[conv_id].conversation_type,
                                    convname: voip_conv_store[conv_id].convname,
                                    user_role: 'participant',

                                    room_name: voip_conv_store[conv_id].room_name,
                                    CLIENT_BASE_URL: process.env.CLIENT_BASE_URL,
                                    root_id: voip_conv_store[conv_id].root_id,
                                    company_id: voip_conv_store[conv_id].company_id,
                                    file_server: process.env.FILE_SERVER,
                                    origin: req.headers.origin,
                                    // expire_unix: Date.now() + 60000
                                    // msg_type: 'call'

                                }, req);
                            }
                            addUserBusy(conv_id, v, req);
                        };
                    }
                }
                updateCallMemberList(conv_id, data.user_id, data, req);
            });
        } else {
            res.json({ status: false });

        }


    } else {
        if (!voip_pending_store[conv_id]) voip_pending_store[conv_id] = new Set();
        voip_pending_store[conv_id].add(data.user_id);
        res.json({ status: false });

    }

});
router.post('/jitsi_call_accept', async function (req, res, next) {
    var data = req.body;
    var conv_id = data.conversation_id;
    if (voip_conv_store.hasOwnProperty(conv_id)) {
        var ring_data = voip_conv_store[conv_id];
        if (!ring_data.device_list[data.user_id]) ring_data.device_list[data.user_id] = [];
        ring_data.connected = true;
        if (ring_data.start_time == 0) ring_data.start_time = Date.now();
        data.call_duration = convertMS(Math.abs(Date.now() - ring_data.start_time));
        var recv_id = data.user_id;
        var caller_id = ring_data.user_id;
        if (ring_data.active_participants.indexOf(recv_id) === -1) ring_data.active_participants.push(recv_id);

        delRingInterval(conv_id, conv_id);
        delRingInterval(conv_id, recv_id);
        updateJitsiMemberStatus(conv_id, recv_id, 'accept', -3, req);

        const token = extractToken(req);
        if (token) {
            var decode = jwt.verify(token, process.env.SECRET);
            ring_data.device_list[data.user_id].push(decode.device_id);
        }
        // console.log('voip::::::::::::jitsi_call__accept', Date.now() - voip_conv_store[conv_id].init_time, ring_data.device_list[data.user_id]);

        if (data.device_type == 'web') xmpp_send_server(recv_id, 'jitsi_send_accept', data, req); // receiver
        else xmpp_send_server(recv_id, 'jitsi_send_accept', data, req, { 'filter': data.token }); // receiver

        xmpp_send_server(caller_id, 'jitsi_user_accept', data, req); // sender

        send_msg_firebase(recv_id, 'jitsi_send_accept', data, req);
        send_msg_firebase(caller_id, 'jitsi_user_accept', data, req);

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
        let payload = getJwtPayload(data.user_id, data.conversation_id);
        jwt.sign(payload, "jitsi", { algorithm: 'HS256' }, async (err, jwt_token) => {
            res.json({ status: true, jwt_token });
        });

    } else {
        res.json({ status: false });

    }
});
router.post('/jitsi_call_hangup', async function (req, res, next) { // step.3 = user call end + browser close
    const token = extractToken(req);
    var decode = token ? jwt.verify(token, process.env.SECRET) : null;
    var data = req.body;

    if (!data.hold_status) data.hold_status = false;
    if (!data.switch_status) data.switch_status = false;
    var conv_id = data.conversation_id;
    if (voip_conv_store.hasOwnProperty(conv_id)) {
        deleteRingIndex(conv_id, data.user_id);
        var ring_data = voip_conv_store[conv_id];
        if (!ring_data.device_list[data.user_id]) ring_data.device_list[data.user_id] = [];
        // var xmpp_user = data.user_id + '$$$' + data.token ? data.token : decode.device_id;
        let idx = ring_data.device_list[data.user_id].indexOf(data.token ? data.token : decode.device_id);
        if (idx > -1) { ring_data.device_list[data.user_id].splice(idx, 1); }

        ring_data.hangup_name = data.user_fullname != undefined ? data.user_fullname : '';
        data.msg_code = data.msg_code != undefined ? data.msg_code : -2;

        delRingInterval(conv_id, data.user_id);

        // let active_participants = 0;
        // for (let ps in ring_data.participants_status) {
        //     if (ring_data.participants_status[ps] == 'In Call') active_participants++;
        // }
        // if (ring_data.device_list[data.user_id].length == 0 && ring_data.active_participants.indexOf(data.user_id) > -1) {
        // if (data.hold_status == false) 
        deleteUserBusy(conv_id, data.user_id, req);
        updateJitsiMemberStatus(conv_id, data.user_id, 'hangup', data.msg_code, req);
        // }
        // console.log(`voip::::::::hangup:${Date.now() - voip_conv_store[conv_id].init_time}, total_active:${ring_data.active_participants},total_admin:${ring_data.admin_users},user_devices: ${ring_data.device_list[data.user_id]}`);
        if (ring_data.call_option == 'ring') {
            if ((ring_data.active_participants.length <= 1 || ring_data.admin_users.length == 0) && ring_data.device_list[data.user_id].length == 0) {
                hangup_conv_exe(conv_id, data.msg_code, data.hold_status, req);

            } else {
                var msg = {
                    ring_index: ring_data.ring_index_list[data.user_id],
                    conversation_id: conv_id,
                    msgid: ring_data.msgid,
                    user_id: data.user_id,
                    origin: req.headers.origin
                };
                xmpp_send_server(data.user_id, 'jitsi_send_hangup', msg, req, { 'equal': data.token });
                send_msg_firebase(data.user_id, 'jitsi_send_hangup', msg, req);

            }
        } else {
            if ((ring_data.active_participants.length < 1 || ring_data.admin_users.length == 0) && ring_data.device_list[data.user_id].length == 0) {
                hangup_conv_exe(conv_id, data.msg_code, data.hold_status, req);
            } else {
                var msg = {
                    ring_index: ring_data.ring_index_list[data.user_id],
                    conversation_id: conv_id,
                    msgid: ring_data.msgid,
                    user_id: data.user_id,
                    origin: req.headers.origin
                };
                xmpp_send_server(data.user_id, 'jitsi_send_hangup', msg, req, { 'equal': data.token });
                send_msg_firebase(data.user_id, 'jitsi_send_hangup', msg, req);

            }

        }

        // if (ring_data.active_participants.length <= 1 && ring_data.connected == false) {
        //     hangup_conv_exe(conv_id, data.msg_code, data.hold_status, req);
        // } else {
        //     if (ring_data.user_id == data.user_id) {
        //         hangup_conv_exe(conv_id, data.msg_code, data.hold_status, req);
        //     } else {
        //         // updateJitsiMemberStatus(conv_id, data.user_id, 'hangup');
        //         var msg = {
        //             ring_index: ring_data.ring_index_list[data.user_id],
        //             conversation_id: conv_id,
        //             msgid: ring_data.msgid,
        //             user_id: data.user_id,
        //             origin: req.headers.origin
        //         };
        //         xmpp_send_server(data.user_id, 'jitsi_send_hangup', msg, req, { 'equal': data.token });
        //         send_msg_firebase(data.user_id, 'jitsi_send_hangup', msg, req);
        //     }
        // }



    }
    if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

    res.json({ status: true });
});
router.post('/jitsi_call_hold', async function (req, res, next) { // step.5
    var data = req.body;
    // console.log('voip::jitsi_call__merge', data);
    var conv_id_new = data.conversation_id;
    var conv_id_old = data.conv_id_old;

    if (voip_conv_store.hasOwnProperty(conv_id_new)) {
        var ring_data = voip_conv_store[conv_id_new];
        var ring_old = voip_conv_store[conv_id_old];
        var send_id = data.merge_id;
        var recv_id = data.user_id;
        xmpp_send_server(recv_id, 'jitsi_send_accept', data, req, { 'filter': data.token }); // receiver
        xmpp_send_server(send_id, 'jitsi_user_accept', data, req);
        if (data.mute_status) {
            // ring_old.arr_participants.forEach(function(v, k) {
            xmpp_send_server(recv_id, 'jitsi_send_mute', { conversation_id: conv_id_new, conv_id_old: conv_id_old }, req);
            // });

        }

        if (ring_data.active_participants.indexOf(send_id) === -1) ring_data.active_participants.push(send_id);

        ring_data.conv_history.push(conv_id_old);
        ring_old.conv_history.push(conv_id_new);
        xmpp_send_server(recv_id, 'jitsi_send_history', {
            conversation_id: conv_id_new,
            conv_id_old: conv_id_old,
            conv_history: voip_conv_store[conv_id_new].conv_history.length
        }, req);

        if (ring_data.participants_all.indexOf(send_id) === -1) ring_data.participants_all.push(send_id);

        // delRingTimerConv(conv_id_new);
        delRingInterval(conv_id_new, conv_id_new);
        delRingInterval(conv_id_new, recv_id);
        // updateJitsiMemberStatus(conv_id_new, send_id, 'accept');

        xmpp_send_server(recv_id, 'jitsi_open_hold', { conversation_id: conv_id_new, conv_id_old: conv_id_old }, req);

        // var msg = {
        //     ring_index: data.ring_index,
        //     conversation_id: data.conv_id_old,
        //     user_id: recv_id
        // };

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

        res.json({ status: true });

    } else {
        res.json({ status: false });

    }
});
router.post('/jitsi_call_merge', async function (req, res, next) { // step.5
    const token = extractToken(req);
    if (!token) return;
    let decode = jwt.verify(token, process.env.SECRET);
    var data = req.body;
    data.user_fullname = (decode.firstname + ' ' + decode.lastname).trim();
    data.user_email = decode.email;
    // console.log('voip::jitsi_call__merge', data);
    var conv_id_now = req.body.conversation_id; // current
    var conv_id_old = req.body.conv_id_old; // incoming

    if (voip_conv_store.hasOwnProperty(conv_id_now)) {
        var ring_data = voip_conv_store[conv_id_now];
        var recv_id = req.body.user_id;
        var caller_id = req.body.merge_id;
        if (ring_data.active_participants.indexOf(recv_id) === -1) ring_data.active_participants.push(recv_id);
        if (ring_data.participants_all.indexOf(recv_id) === -1) ring_data.participants_all.push(recv_id);

        delRingInterval(conv_id_old, conv_id_old);
        delRingInterval(conv_id_old, caller_id);
        // updateJitsiMemberStatus(conv_id_now, recv_id, 'accept');

        xmpp_send_server(caller_id, 'jitsi_send_merge', {
            conversation_id: conv_id_now,
            conv_id_old: conv_id_old,
            hold_status: true
        }, req);
        xmpp_send_server(recv_id, 'jitsi_send_accept', data, req, { 'filter': data.token }); // receiver

        send_msg_firebase(recv_id, 'jitsi_send_accept', data, req);

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

        res.json({ status: true });

    } else {
        res.json({ status: false });

    }
});
router.post('/jitsi_add_admin', function (req, res, next) {
    var conv_id = req.body.conversation_id;
    var ring_data = voip_conv_store[req.body.conversation_id];
    if (ring_data && ring_data.database_update == false && ring_data.admin_users.indexOf(req.body.user_id) === -1) {
        ring_data.admin_users.push(req.body.user_id);
    }
    if (voip_pending_store.hasOwnProperty(conv_id)) {
        for (const [i, v] of voip_pending_store[conv_id].entries()) {
            xmpp_send_server(v, 'jitsi_guest_join', {
                conversation_id: conv_id,
                user_busy: voip_busy_user.hasOwnProperty(v),
                voip_busy_conv: voip_busy_conv,
                busy_conv_list: Object.keys(voip_busy_conv),
            }, req);
        }
        delete voip_pending_store[conv_id];
    }
    res.json({ status: true });


})
router.post('/jitsi_add_member', async function (req, res, next) { // step.4
    var data = req.body;
    data.rootImg = data.user_img;
    data.user_fullname = data.sender_name;
    data.call_option = 'ring';
    data.conversation_type = 'addparticipant';
    data.join_who = 'initiator';
    data.reg_status = 'webcam';
    if (typeof data.arr_participants === 'string') data.arr_participants = JSON.parse(data.arr_participants);
    var conv_id = data.conversation_id;
    var sender_id = data.user_id;
    var receiver_id = data.member_id;
    data.ring_type = 'new';
    if (!voip_conv_store[conv_id]) return;
    if (voip_conv_store[conv_id].plan_name == 'starter' && voip_conv_store[conv_id].active_participants.length >= 100) {
        return res.json({ status: 'upgrade' });
    }

    call_ringing_loop(conv_id, receiver_id, {
        conversation_id: conv_id,
        ring_index: conv_id + '_' + sender_id + '_' + receiver_id,
        user_fullname: data.user_fullname,
        user_img: data.user_img,
        conversation_type: data.conversation_type,
        convname: data.convname,
        user_role: 'participant',
        user_id: sender_id,
        receiver_id: receiver_id,
        file_server: process.env.FILE_SERVER,
        room_name: voip_conv_store[conv_id].room_name,
        company_id: voip_conv_store[conv_id].company_id,
        CLIENT_BASE_URL: process.env.CLIENT_BASE_URL,
        root_id: voip_conv_store[conv_id].root_id,
        expire_unix: Date.now() + 60000
        // msg_type: 'call'
        // origin: req.headers.origin

    }, req);
    addUserBusy(conv_id, receiver_id, req); // set & add receiver to busy
    voip_conv_store[conv_id].participants_status[receiver_id] = 'Calling...';
    // console.log('part_status', receiver_id, voip_conv_store[conv_id].participants_status[receiver_id]);

    updateCallMemberList(conv_id, receiver_id, data, req);

    if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
    res.json({ status: 'send' });
    // }
});
router.post('/jitsi_merge_hold', async function (req, res, next) {
    var data = req.body;
    var conv_id_now = data.conversation_id;
    if (voip_conv_store.hasOwnProperty(conv_id_now)) {
        var ring_data = voip_conv_store[conv_id_now];
        var active_now = ring_data.active_participants;

        if (voip_busy_user[data.user_id] && voip_busy_user[data.user_id].length) {
            voip_busy_user[data.user_id].forEach(function (conv_id_old, k) {
                if (conv_id_now == conv_id_old) return
                if (voip_conv_store.hasOwnProperty(conv_id_old)) {
                    let ring_old = (voip_conv_store[conv_id_old]);
                    let active_old = ring_old.active_participants;
                    let intersection = active_now.filter(x => active_old.includes(x)); // common
                    let difference = active_now
                        .filter(x => !active_old.includes(x))
                        .concat(active_old.filter(x => !active_now.includes(x)));

                    intersection.forEach(function (v, k) {
                        xmpp_send_server(v, 'jitsi_send_hangup', { conversation_id: conv_id_old, hold_status: true }, req);
                        send_msg_firebase(v, 'jitsi_send_hangup', { conversation_id: conv_id_old, hold_status: true }, req);
                    });
                    difference.forEach(function (v, k) {
                        xmpp_send_server(v, 'jitsi_send_merge', {
                            conversation_id: conv_id_now,
                            conv_id_old: conv_id_old,
                            hold_status: true
                        }, req);
                    });
                    res.json({ status: true });


                } else {
                    res.json({ status: false });
                }

            });

        } else {
            res.json({ status: false });
        }

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
    } else {
        res.json({ status: false });
    }
});
router.post('/get_call_userlist', function (req, res, next) {
    var data = req.body;
    // if (!isUuid(data.user_id)) return;
    let busy_conv = voip_busy_conv[data.conversation_id] ? true : false;

    if (voip_busy_user.hasOwnProperty(data.user_id)) {
        res.json({ status: true, msg: 'busy' });
    } else {
        getConvUsers(data, async (userdata, user_error) => {
            if (userdata.status) {
                res.json({ status: true, msg: 'list', all_users: userdata.users });
            } else {
                res.json({ status: false });
            }

        });

    }

});
router.post('/get_jitsi_members', async function (req, res, next) {
    var data = req.body;
    if (voip_conv_store.hasOwnProperty(data.conversation_id)) {
        let members = voip_conv_store[data.conversation_id].active_participants;
        res.json({ status: true, members: members });
    } else {
        res.json({ status: false, members: [] });
    }
});



// ================= Messaging API ======================================================
router.post('/send_message', function (req, res, next) {
    // console.log(`db_unix:msg_api: ${Date.now()}`);
    const token = extractToken(req);
    if (token) {
        var decode = jwt.verify(token, process.env.SECRET);
    }
    var data = req.body;
    const { errors, isValid } = validateMsgInput(data);
    if (!isValid) {
        res.json({ error: 'Validation error', message: errors });
    } else {
        set_message(data, (result) => {
            if (result.status) {
                // xmpp_send_room(decode.xmpp_user + '@' + xmpp_domain, data.conversation_id + '@conference.' + xmpp_domain, 'new_message', result.msg, req);

                (data.participants).forEach(function (v, k) {
                    // io.to(v).emit('new_message', result.msg);
                    xmpp_send_server(v, 'new_message', result.msg, req);
                    if (data.sender != v) {
                        // result.msg.group = req.body.group;
                        // result.msg.title = req.body.title;
                        send_msg_firebase(v, 'new_message', result.msg, req);
                    }

                });
                res.json({ status: true, message: 'Message send', data: result.msg });
            } else {
                res.json({ error: 'Error', message: result.err });
            }
        });
    }
});
router.post('/send_reply_msg', function (req, res, next) {
    var data = req.body;
    // console.log('send_reply_msg_data', data);
    const { errors, isValid } = validateMsgInput(data);
    if (!isValid) {
        res.json({ error: 'Validation error', message: errors });
    } else {
        set_message(data, (result) => {
            if (result.status) {
                (data.participants).forEach(function (v, k) {
                    // io.to(v).emit('new_reply_message', result.msg);
                    xmpp_send_server(v, 'new_reply_message', result.msg, req);
                    if (data.sender != v) {
                        send_msg_firebase(v, 'new_reply_message', result.msg, req);
                    }
                });
                res.json({ status: true, message: 'Reply Message send', data: result.msg });
            } else {
                res.json({ error: 'Error', message: result.err });
            }
        });
    }
});
router.post('/typing', function (req, res, next) {
    var data = req.body;
    // console.log(96, data);
    for (let to of data.participants) {
        if (to !== data.sender) {
            xmpp_send_server(to, 'someone_typing', data, req);

        }
        // io.to(to).emit('someone_typing', data);
    }
    res.status(201).json({ 'status': 'ok' });
});

// router.post('/get_call_history_old', passport.authenticate('jwt', { session: false }), async (req, res) => {
//     console.log(1299, req.body);
//     const token = extractToken(req);
//     if (token) {
//         let decode = jwt.verify(token, process.env.SECRET);
//         models.instance.Conversation.find({ participants: { $contains: decode.id.toString() }, status: 'active' }, { raw: true, allow_filtering: true }, async function (error, convs) {
//             console.log(convs);
//             var conv_arr = [];
//             var conv_data = {};
//             for (let conv of convs) {
//                 conv_arr.push(conv.conversation_id);
//                 conv_data[conv.conversation_id.toString()] = conv;
//             }


//             var query = {
//                 conversation_id: { '$in': conv_arr },
//                 // msg_type: 'call' 
//             };
//             var page_obj = { fetchSize: 20, raw: true, allow_filtering: true };
//             if (req.body.pageState) page_obj.pageState = req.body.pageState;
//             var history = [];
//             (function searchMsgTable() {
//                 models.instance.Messages.eachRow(query, page_obj, function (n, msg) {
//                     if (msg.msg_type == 'call') {
//                         let cv = conv_data[msg.conversation_id.toString()];
//                         if (cv) {
//                             if (cv.single == "yes" && cv.conversation_id.toString() != decode.id.toString()) {
//                                 var fid = cv.participants.join().replace(decode.id.toString(), '').replace(',', '');
//                                 msg.title = all_users[decode.company_id][fid].firstname + " " + all_users[decode.company_id][fid].lastname;
//                                 msg.conv_img = all_users[decode.company_id][fid].img;
//                                 msg.friend_id = fid;
//                                 msg.fnln = all_users[decode.company_id][fid].fnln;
//                                 msg.conversation_type = "personal";
//                                 msg.group = "no";
//                             } else {
//                                 msg.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + cv.conv_img;
//                                 msg.conversation_type = "group";
//                                 msg.group = "yes";
//                             }
//                             history.push(msg);
//                         }
//                     }

//                 }, async function (err, result) {
//                     if (err) return res.status(400).json({ error: 'get_call_history error1', message: err });
//                     page_obj.pageState = result.pageState;

//                     if (history.length < 20 && page_obj.pageState != null) {
//                         page_obj.fetchSize *= 2;
//                         searchMsgTable();
//                     } else {
//                         return res.status(200).json({ history: history, pageState: result.pageState });

//                     }


//                 });
//             })();




//         })
//         // try {


//         // } catch (e) {
//         //     return res.status(404).json({ error: 'get_call_history error2', message: e });
//         // }
//     }
// });

// router.post('/get_call_history', passport.authenticate('jwt', { session: false }), async (req, res) => {
//     console.log('get_call_history:req:', req.body);
//     const token = extractToken(req);
//     if (token) {
//         let decode = jwt.verify(token, process.env.SECRET);
//         Conversation.find({ participants: decode.id.toString(), status: 'active' }, ['conversation_id'], function (e, convs) {
//         // models.instance.Conversation.find({ participants: { $contains: decode.id.toString() }, status: 'active' }, { raw: true, allow_filtering: true }, async function (error, convs) {
//             var conv_arr = [];
//             var conv_data = {};
//             for (let conv of convs) {
//                 // console.log('get_call_history1', conv.conversation_id.toString())
//                 conv_arr.push(conv.conversation_id);
//                 conv_data[conv.conversation_id.toString()] = conv;
//             }

//             var query = {
//                 call_participants: { $contains: decode.id.toString() },
//                 company_id: decode.company_id,
//                 // $orderby: { '$desc': 'msg_id' },
//                 // conversation_id: { '$in': conv_arr },
//                 // $groupby: ['conversation_id']
//                 // msg_type: 'call' 
//             };
//             var page_obj = { fetchSize: 20, raw: true, allow_filtering: true, materialized_view: 'messages_by_call' };
//             if (req.body.pageState) page_obj.pageState = req.body.pageState;
//             var history = [];
//             (function searchMsgTable() {
//                 models.instance.Messages.eachRow(query, page_obj, function (n, msg) {
//                     if (msg.msg_type == 'call') {
//                         let cv = conv_data[msg.conversation_id.toString()];
//                         if (cv) {
//                             if (cv.single == "yes" && cv.conversation_id.toString() != decode.id.toString()) {
//                                 var fid = cv.participants.join().replace(decode.id.toString(), '').replace(',', '');
//                                 msg.title = all_users[decode.company_id][fid].firstname + " " + all_users[decode.company_id][fid].lastname;
//                                 msg.conv_img = all_users[decode.company_id][fid].img;
//                                 msg.friend_id = fid;
//                                 msg.fnln = all_users[decode.company_id][fid].fnln;
//                                 msg.conversation_type = "personal";
//                                 msg.group = "no";
//                             } else {
//                                 msg.title = cv.title;
//                                 msg.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + cv.conv_img;
//                                 msg.conversation_type = "group";
//                                 msg.group = "yes";
//                             }
//                             history.push(msg);
//                         }
//                     }

//                 }, async function (err, result) {
//                     if (err) return res.status(400).json({ error: 'get_call_history error1', message: err });
//                     page_obj.pageState = result.pageState;

//                     if (history.length < 20 && page_obj.pageState != null) {
//                         // page_obj.fetchSize *= 2;
//                         searchMsgTable();
//                     } else {
//                         for (his of history) {
//                             console.log('get_call_history:data', his.conversation_id.toString(), his.created_at)
//                         }
//                         return res.status(200).json({ history: history, pageState: result.pageState });

//                     }


//                 });
//             })();




//         })
//         // try {


//         // } catch (e) {
//         //     return res.status(404).json({ error: 'get_call_history error2', message: e });
//         // }
//     }
// });
// ==========================================
router.post('/call_history_group', passport.authenticate('jwt', { session: false }), async (req, res) => {
    // console.log('call_history_group:req:', req.body);
    const token = extractToken(req);
    if (token) {
        let decode = jwt.verify(token, process.env.SECRET);
        var user_id = req.body.user_id ? req.body.user_id : decode.id.toString();
        var company_id = req.body.company_id ? req.body.company_id : decode.company_id;
        let convs = await Conversation.find({ participants: user_id, status: 'active' }).lean();
        // models.instance.Conversation.find({ participants: { $contains: decode.id.toString() }, status: 'active' }, { raw: true, allow_filtering: true }, async function (error, convs) {
        // if (error) return res.status(400).json({ error: 'call_history_group error1', message: error });
        var history = [];
        var conv_arr = [];
        var conv_data = {};
        for (let conv of convs) {
            conv_arr.push(conv.conversation_id);
            conv_data[conv.conversation_id.toString()] = conv;
        }

        var msgs = await Messages.aggregate([
            { $match: { call_participants: user_id, company_id: company_id } },
            { $group: { _id: '$conversation_id', "doc": { "$first": "$$ROOT" } } },
            { "$replaceRoot": { "newRoot": "$doc" } }
        ]);

        for (let msg of msgs) {
            let cv = conv_data[msg.conversation_id.toString()];
            if (!cv) continue
            msg.participants = cv.participants;
            msg.participants_details = [];
            for (let i = 0; i < msg.participants.length; i++) {
                let this_user = await get_user_obj(msg.participants[i]);
                msg.participants_details.push({
                    id: msg.participants[i],
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    email: this_user.email,
                    img: this_user.img,
                    fnln: this_user.fnln,
                    role: this_user.role,
                    createdat: this_user.createdat,
                    is_active: this_user.is_active

                });
            }
            if (cv.group == "no" && cv.conversation_id.toString() != user_id) {
                var fid = cv.participants.join().replace(user_id, '').replace(',', '');
                msg.title = all_users && all_users[company_id] && all_users[company_id][fid] && all_users[company_id][fid].firstname ? all_users[company_id][fid].firstname + " " + all_users[company_id][fid].lastname : '';
                msg.conv_img = all_users[company_id] && all_users[company_id][fid] ? all_users[company_id][fid].img : '';
                msg.friend_id = fid;
                msg.fnln = all_users[company_id] && all_users[company_id][fid] ? all_users[company_id][fid].fnln : '';
                msg.conversation_type = "personal";
                msg.group = "no";

            } else {
                msg.title = cv.title;
                msg.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + cv.conv_img;
                msg.conversation_type = "group";
                msg.group = "yes";
            }
            history.push(msg);
        }
        history.sort(function (a, b) { return new Date(b.created_at) - new Date(a.created_at) });
        return res.status(200).json({ history_group: history });

    }
});
router.post('/call_history_detail', passport.authenticate('jwt', { session: false }), async (req, res) => {
    // console.log('get_call_history:req:', req.body);
    // const token = extractToken(req);
    // if (token) {
    //     let decode = jwt.verify(token, process.env.SECRET);
    //     let cv = await Conversation.find({ conversation_id: req.body.conversation_id, status: 'active' }).lean();

    //     var query = {
    //         call_participants: { $contains: decode.id.toString() },
    //         company_id: decode.company_id,
    //         conversation_id: models.uuidFromString(req.body.conversation_id)
    //     };

    //     models.instance.Conversation.findOne({
    //         conversation_id: models.uuidFromString(req.body.conversation_id),
    //         status: 'active'
    //     }, { raw: true, allow_filtering: true }, async function (error, cv) {
    //         // if (!cv) return res.status(400).json({ error: 'call_history_detail error1', message: error });

    //         var page_obj = { fetchSize: 20, raw: true, allow_filtering: true };
    //         if (req.body.pageState) page_obj.pageState = req.body.pageState;
    //         var history = [];
    //         (function searchMsgTable() {
    //             models.instance.Messages.eachRow(query, page_obj, function (n, msg) {
    //                 if (msg.msg_type == 'call') {
    //                     if (cv.single == "yes" && cv.conversation_id.toString() != decode.id.toString()) {
    //                         var fid = cv.participants.join().replace(decode.id.toString(), '').replace(',', '');
    //                         msg.title = all_users[decode.company_id][fid].firstname + " " + all_users[decode.company_id][fid].lastname;
    //                         msg.conv_img = all_users[decode.company_id][fid].img;
    //                         msg.friend_id = fid;
    //                         msg.fnln = all_users[decode.company_id][fid].fnln;
    //                         msg.conversation_type = "personal";
    //                         msg.group = "no";
    //                     } else {
    //                         msg.title = cv.title;
    //                         msg.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + cv.conv_img;
    //                         msg.conversation_type = "group";
    //                         msg.group = "yes";
    //                     }
    //                     history.push(msg);
    //                 }

    //             }, async function (err, result) {
    //                 if (err) return res.status(400).json({ error: 'get_call_history error1', message: err });
    //                 page_obj.pageState = result.pageState;

    //                 if (history.length < 20 && page_obj.pageState != null) {
    //                     // page_obj.fetchSize *= 2;
    //                     searchMsgTable();
    //                 } else {
    //                     for (his of history) {
    //                         console.log('get_call_history:data', his.conversation_id.toString(), his.created_at)
    //                     }
    //                     return res.status(200).json({ history: history, pageState: result.pageState });

    //                 }


    //             });
    //         })();




    //     })
    //     // try {


    //     // } catch (e) {
    //     //     return res.status(404).json({ error: 'get_call_history error2', message: e });
    //     // }
    // }
});


// var user_diff = req.body.choose_plan.user - company_info.plan_user_limit;
// let per_month = moment().daysInMonth();
// let per_day = (4 / per_month);
// let days_left = moment().endOf('month').diff(moment(), 'days');
// let new_charge = days_left * per_day;
// if (user_diff > 0) {
//     amount = subscription.plan.amount / 100 + user_diff * new_charge;
// } else if (user_diff < 0) {
//     amount = subscription.plan.amount / 100 - user_diff * new_charge;
// }
async function addCardCustomer(customer_id, token_additional, origResolve, origReject) {
    return new Promise(async (resolve, reject) => {
        resolve = (origResolve) ? origResolve : resolve
        reject = (origReject) ? origReject : reject
        // var current_index = 0;
        try {
            // var token_loop = [...token_additional];
            while (token_additional.length) {
                // token_additional.splice(i--, 1);
                var card = token_additional.shift()
                var source = await stripe.customers.createSource(customer_id, { source: card.id });
                // console.log('Stripe:card:added:', card.id);

            }
            resolve();

        } catch (error) {
            console.error('Stripe:card:failed', error);
            // token_additional.splice(current_index, 1);
            if (token_additional.length) {
                addCardCustomer(customer_id, token_additional, resolve, reject)
            } else {
                var customer = await stripe.customers.retrieve(customer_id);
                if (customer.default_source) {
                    resolve();
                } else {
                    reject({ message: error.message });

                }


            }

            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    })

}
// =============================================================================================
// 2.1. STRIPE: CARD: CREATE TOKENS: ======================================================
// var primary_token = null;
// var token_additional = [];

// for (let [i, card_info] of payment_info.entries()) {
//     if (card_info.primaryCardCheckBox) {
//         payment_info.unshift(payment_info.splice(i, 1)[0]);
//     }
// }
// for (let card_info of payment_info) {
//     var expdate = new Date(card_info.billingExpirationDate);
//     var card_number = card_info.billingCardNumber.replace(/\s/g, '');
//     token_additional.push(await stripe.tokens.create({
//         card: {
//             name: card_info.billingCardHolderName,
//             number: card_number,
//             exp_month: expdate.getMonth() + 1,
//             exp_year: expdate.getFullYear(),
//             cvc: card_info.billingSecurityCode,
//             address_city: card_info.cardAddressCity,
//             address_state: card_info.cardAddressProvince,
//             address_zip: card_info.cardAddressZip,
//             address_line1: card_info.cardAddressStreet
//         }
//     }));
// }
// if (!token_additional) return res.status(200).json({ status: false, error: 'No primary card' });
// 2.2. STRIPE: CUSTOMER + SOURCE: CREATE / UPDATE =============================================
// 6. charge::generate =============================================
// var charge = await stripe.charges.create({
//     amount: amount * 100,
//     // source: token.id,
//     currency: 'usd',
//     customer: customer_id,
//     shipping: {
//         "address": {
//             line1: req.body.address.street_address,
//             postal_code: req.body.address.zip,
//             city: req.body.address.province,
//             state: req.body.address.province,
//             country: req.body.address.province,

//         },
//         "name": req.body.payment_info.billingCardHolderName,
//         // "phone": null
//     }
// });

router.post('/stripe_make_payment', async function (req, res) {
    const jwt_token = extractToken(req);
    var decode = jwt.verify(jwt_token, process.env.SECRET);
    try {
        // var payment_info = req.body.payment_info;
        // var plan_storage_limit = String(req.body.choose_plan.storage);
        // var baseURL = req.body.baseURL ? req.body.baseURL : process.env.CLIENT_BASE_URL;
        var plan_name = 'essential';
        var paymentMethod = null;
        var email_arr = [];

        var email_invoice = req.body.email_invoice ? req.body.email_invoice : req.body.address.email;
        if (email_invoice.includes(',')) {
            email_arr = email_invoice.split(',');
        } else if (email_invoice.includes(';')) {
            email_arr = email_invoice.split(';');
        } else {
            email_arr.push(email_invoice);
        }
        var total_amount = req.body.total_amount;
        var payment_method = req.body.payment_method;
        var additional_storage = String(req.body.additional_storage);

        var old_user = all_company[decode.company_id].plan_user_limit;
        var old_storage = all_company[decode.company_id].plan_storage_limit;

        var new_user = req.body.new_user ? String(req.body.new_user) : String(req.body.choose_plan.user);
        var new_storage = req.body.new_storage ? String(req.body.new_storage) : String(req.body.plan_storage_limit);

        if (process.env.API_SERVER_URL && !process.env.API_SERVER_URL.includes('localhost')) {
            var webhookEndpointList = await stripe.webhookEndpoints.list();
            var hook_filter = [];
            let hook_url = String(new URL('/webhook', process.env.API_SERVER_URL));

            if (webhookEndpointList && webhookEndpointList.data && webhookEndpointList.data.length) {
                var hook_filter = webhookEndpointList.data.filter(async (hook) => {
                    if (hook.url !== hook_url) {
                        const deleted = await stripe.webhookEndpoints.del(hook.id);
                        // console.log('stripe:webhook:del:', deleted);
                    }
                    let status = hook.url == hook_url;
                    return status
                });
            }
            if (hook_filter.length == 0) {

                var webhookEndpoint = await stripe.webhookEndpoints.create({
                    url: hook_url,
                    enabled_events: ['*'],
                });
            }
        }

        // =========================================================================================================
        // 1.1. CASSANDRA: GET: CUSTOMER_ID +  =============================================
        var customer_id = await new Promise((resolve, reject) => {
            Users.findOne({ id: String(decode.id) }, function (error, user) {
                if (user) { resolve(user.customer_id) } else reject({ message: 'User not found!' })
            });
        });
        var company_info = await new Promise((resolve, reject) => {
            Company.findOne({ company_id: String(decode.company_id) }, function (error, company) {
                if (company) { resolve(company) } else reject({ message: 'Company not found!' })
            });
        });

        // try {
        if (customer_id) {
            // const prev_cards = await stripe.customers.listSources(customer_id, { object: 'card', limit: 100 });
            // for (let card of prev_cards.data) {
            //     var deleted = await stripe.customers.deleteSource(customer_id, card.id);
            // }
            // const paymentMethods = await stripe.customers.listPaymentMethods(
            //     customer_id,
            //     {type: 'card'}
            //   );

            const paymentMethods = await stripe.paymentMethods.list({
                customer: customer_id,
                type: 'card',
            });
            if (paymentMethods && paymentMethods.data && paymentMethods.data.length) {
                // if (payment_method) {
                // paymentMethod = await stripe.paymentMethods.retrieve(payment_method);
                paymentMethod = paymentMethods.data[0];
                payment_method = paymentMethod.id;

                var cusObj = {
                    email: email_arr[0],
                    // name: decode.firstname + ' ' + decode.lastname,
                    name: paymentMethod.billing_details.name,
                    description: decode.company_name,
                    address: {
                        line1: paymentMethod.billing_details.address.line1,
                        line2: decode.company_name,
                        postal_code: paymentMethod.billing_details.address.postal_code,
                        city: paymentMethod.billing_details.address.city,
                        state: paymentMethod.billing_details.address.state,
                        country: paymentMethod.billing_details.address.country,
                    },
                    metadata: {
                        plan_user_limit: new_user,
                        plan_storage_limit: new_storage,
                        email_invoice: email_invoice
                    }
                };
                cusObj.invoice_settings = {
                    "default_payment_method": payment_method,
                }
                var customer = await stripe.customers.update(customer_id, cusObj);
            }
            // }

        }

        // ================================================================
        if (company_info.subscription_id && company_info.product_id) {
            total_amount = Math.trunc(total_amount);
            var price = await stripe.prices.create({
                unit_amount: total_amount * 100,
                currency: 'usd',
                recurring: { interval: 'month' },
                product: company_info.product_id,
            });
            var subscription = await stripe.subscriptions.retrieve(company_info.subscription_id);
            // console.log('stripe:subscription:get:', JSON.stringify(subscription));
            // UPDATE SUBSCRIPTION ====================================================
            subscription = await stripe.subscriptions.update(company_info.subscription_id, {
                default_payment_method: payment_method,
                // cancel_at_period_end: false,
                // proration_behavior: 'none',
                // pending_invoice_item_interval: {interval: 'month'},
                items: [{
                    id: subscription.items.data[0].id,
                    price: price.id,
                }]
            });
            // console.log('stripe:subscription:update:', JSON.stringify(subscription));
            // UPCOMING INVOICE ==========================================================
            // var invoice = await stripe.invoices.retrieveUpcoming({
            //     // subscription_prorate: true,
            //     customer: customer_id,
            //     subscription: subscription.id,

            //     subscription_items: [{
            //         id: subscription.items.data[0].id,
            //         price: price.id, // Switch to new price
            //     }],
            //     subscription_proration_date: Math.floor(Date.now() / 1000),
            // });
            // console.log('stripe:invoice', invoice);
        } else {
            // CREATE PRODUCT ====================================================
            var product = await stripe.products.create({ name: 'Essential' });
            // console.log('stripe:product:create:', product);
            // CREATE PRICE ========================================================
            var price = await stripe.prices.create({
                unit_amount: total_amount * 100,
                currency: 'usd',
                recurring: {
                    interval: 'month',
                },
                product: product.id,
            });
            // console.log('stripe:price:create:', price);

            // CREATE SUBSCRIPTION ========================================================
            var subscription = await stripe.subscriptions.create({
                trial_period_days: 30,
                customer: customer_id,
                items: [{ price: price.id }],
                default_payment_method: payment_method
                // application_fee_percent: 10,
                // trial_end: Math.floor(Date.now() / 1000) + 30 * 86400,
                // pending_invoice_item_interval: { interval: 'month' }
            });
            // console.log('stripe:subscription:create:', JSON.stringify(subscription));
            await new Promise((resolve, reject) => {
                Company.updateOne({ company_id: String(decode.company_id) }, {
                    subscription_id: subscription.id,
                    product_id: product.id
                }, async function (err) {
                    if (err) reject({ message: 'Company Update failed' });
                    else resolve();
                });
            });

            // var invoice = await stripe.invoices.retrieveUpcoming({
            //     customer: customer_id,
            // });

        }

        // 1. company::: update =============================================
        await update_company({
            plan_name: plan_name,
            plan_user_limit: new_user,
            plan_storage_limit: new_storage,
            company_id: decode.company_id,
        });
        all_company[decode.company_id].plan_name = plan_name;
        all_company[decode.company_id].plan_user_limit = new_user;
        all_company[decode.company_id].plan_storage_limit = new_storage;


        // 7. payment::generate =============================================
        // var db_pay_id = models.timeuuid();
        // console.log('subscription Successful', db_pay_id.toString(), subscription, price, invoice);
        customer = await stripe.customers.retrieve(customer_id);
        // var default_card = await stripe.customers.retrieveSource(customer_id, customer.default_source); // ?????

        await Payment.insertMany([{
            id: uuidv4(),
            company_id: decode.company_id,
            card_holder_name: decode.firstname + decode.lastname, // ???
            card_no: paymentMethod ? paymentMethod.card.last4 : '', // ????
            transection_id: subscription.id,
            payment_by: decode.id,
            created_by: decode.id,
            amount: String(total_amount),
            plan_name: plan_name,
            plan_user_limit: new_user,
            plan_storage_limit: new_storage,
            customer_id: customer_id,
            additional_storage: additional_storage
        }]);


        const invoice_last = await stripe.invoices.retrieve(subscription.latest_invoice);

        var dateString = '';
        if (subscription && subscription.current_period_end) {
            dateString = moment.unix(subscription.current_period_end).format("LL");
        }

        if (company_info.subscription_id) {
            for (let email of email_arr) {
                var eHtml = `Hi ${decode.firstname},<br><br>
                Your subscription plan has successfully been changed for your Workfreeli account ${decode.company_name}.<br><br>
                Please find below the plan change details:<br><br>
                <b>Your new subscription plan (currently active):</b><br>
                <b>Essential Plan</b><br>
                Monthly \$${total_amount} to be charged every month starting from ${dateString}<br>
                Payment method: Credit Card (last 4 digits: ${paymentMethod.card.last4})<br>
                This plan includes ${new_user} users and ${new_storage} GB of storage.<br><br><br>
                <b>Your previous subscription plan:</b><br>
                <b>Essential Plan</b><br>
                This plan includes ${old_user} users and ${old_storage} GB of storage.<br><br><br>

                Your first invoice/receipt for the new subscription plan is attached herewith. To upgrade or cancel your subscription or to buy additional storage, please go to Admin Panel -> Subscription & Billing. Admin Panel can only be accessed from an administrator’s account.<br><br>
                <a clicktracking="off" href="${invoice_last.invoice_pdf}">Download invoice and receipt from here</a><br><br>
                Regards,<br><br>
                <img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br>
                WORKFREELI Team<br>
                W: workfreeli.com<br>
                E: support@workfreeli.com<br>
                `;
                sg_email.send({
                    to: email,
                    // to: 'sujon.xpress@gmail.com',
                    // bcc: 'mahfuzak08@gmail.com',
                    subject: 'Your Workfreeli subscription plan has been changed!',
                    text: 'CHANGE OF SUBSCRIPTION PLAN',
                    html: eHtml,

                }, (result) => {
                    if (result.msg == 'success') {

                    } else {
                        console.log('email sending error 1 ', result);
                        // return res.status(200).json({ status: false, error: 'Email Sending failed' });
                    }
                });


            }
            return res.status(200).json({
                status: true,
                amount: total_amount,
                email: email_arr.join(),
                subscription_id: subscription.id,
                customer_id: customer.id
            });



        } else {

            for (let email of email_arr) {
                var eHtml = `Hi ${decode.firstname},<br><br>
                Thank you for subscribing to the following Workfreeli plan. We hope you and your team will enjoy using Workfreeli to connect and collaborate with each other.<br><br>
                Essential Plan<br>
                Account Name: ${decode.company_name} workspace<br>
                Subscribed with email ID: ${email}<br>
                Subscription fee: \$${total_amount} to be charged every month starting from ${dateString}<br>
                Payment method: Credit Card (last 4 digits: ${paymentMethod.card.last4})<br>
                This plan includes ${new_user} users and ${new_storage} GB of storage.<br><br><br>
                By subscribing, you have authorized us to charge you the subscription fee automatically until cancelled. Please keep this invoice for your records.<br><br>
                Your first invoice/receipt is attached herewith. To upgrade or cancel your subscription or to buy additional storage, please go to Admin Panel -> Subscription & Billing. Admin Panel can only be accessed from an administrator’s account.<br><br>
                <a clicktracking="off" href="${invoice_last.invoice_pdf}">Download invoice and receipt from here</a><br><br>
                Regards,<br><br>
                <img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br>
                WORKFREELI Team<br>
                W: workfreeli.com<br>
                E: support@workfreeli.com<br>
                `;
                sg_email.send({
                    to: email,
                    // to: 'sujon.xpress@gmail.com',
                    // bcc: 'mahfuzak08@gmail.com',
                    subject: 'Your new SUBSCRIPTION PLAN is now active!',
                    text: 'Plan Upgraded!',
                    html: eHtml,
                    // attachments: [{
                    //     content: fs.readFileSync(file_name).toString("base64"),
                    //     filename: "attachment.pdf",
                    //     type: "application/pdf",
                    //     disposition: "attachment"
                    // }]
                }, (result) => {
                    if (result.msg == 'success') {

                    } else {
                        console.log('email sending error 1 ', result);
                        // res.status(200).json({ status: false, error: 'Email Sending failed' });
                    }
                });
            }
            res.status(200).json({
                status: true,
                amount: total_amount,
                email: email_arr.join(),
                subscription_id: subscription.id,
                customer_id: customer.id
            });

        }

    } catch (error) {
        console.log(error);

        Users.updateOne({ id: String(decode.id) }, {
            customer_id: null,
        }, async function (err) {
            if (err) console.trace(err);
        });

        Company.updateOne({
            company_id: String(decode.company_id)
        }, { subscription_id: null, product_id: null }, async function (err) {
            if (err) console.trace(err);
        });
        res.status(200).json({ status: false, error: error.message });
    }
});

router.post('/stripe_get_card', async function (req, res) {
    var cardObj = [];
    var setupIntents = null;
    var customer_id = null;
    var Stripe_Publishable_Key = process.env.Stripe_Publishable_Key ? process.env.Stripe_Publishable_Key : '';

    try {
        const jwt_token = extractToken(req);
        var decode = jwt.verify(jwt_token, process.env.SECRET);

        customer_id = await new Promise((resolve, reject) => {
            Users.findOne({ id: String(decode.id) }, function (error, user) {
                if (user) {
                    resolve(user.customer_id);
                } else {
                    reject();
                }
            });
        });

        if (customer_id) {
            var customer = await stripe.customers.retrieve(customer_id);
            // const cards = await stripe.customers.listSources(customer_id, { object: 'card', limit: 100 });

            const paymentMethods = await stripe.customers.listPaymentMethods(
                customer_id, { type: 'card' }
            );

            for (let pay of paymentMethods.data) {
                cardObj.push({
                    billingCardID: pay.id,
                    billingCardNumber: pay.card.last4,
                    billingCardHolderName: pay.billing_details.name,
                    billingExpirationDate: new Date(pay.card.exp_year, pay.card.exp_month - 1),
                    billingSecurityCode: "",
                    // primaryCardCheckBox: card.id == customer.default_source ? true : false,
                    primaryCardCheckBox: true,
                    cardAddressCity: pay.billing_details.address.city,
                    cardAddressProvince: pay.billing_details.address.state,
                    cardAddressZip: pay.billing_details.address.postal_code,
                    cardAddressStreet: pay.billing_details.address.line1
                });
            }
        } else {
            var customer = await stripe.customers.create({
                // source: primary_token.id,
                email: decode.email,
                // name: primary_token.card.name,
                name: decode.firstname + ' ' + decode.lastname,
                // address: {
                //     line1: req.body.address.street_address,
                //     postal_code: req.body.address.zip,
                //     city: req.body.address.province,
                //     state: req.body.address.province,
                //     country: req.body.address.province,
                // }
            });
            customer_id = customer.id;
            await new Promise((resolve, reject) => {
                Users.updateOne({ id: String(decode.id) }, { customer_id: customer_id }, async function (err) {
                    if (err) reject({ message: 'User update failed' });
                    else resolve();
                });
            });


        }
        const ephemeralKey = await stripe.ephemeralKeys.create(
            { customer: customer_id },
            { apiVersion: '2022-08-01' }
        );

        var setupIntents = await stripe.setupIntents.create({
            customer: customer_id,
            // receipt_email: decode.email,
            usage: 'off_session',
            payment_method_types: ['card'],
            // currency: 'usd',
        })

        // client_secret = setupIntents.client_secret;

        res.json({ status: true, cardObj, setupIntents, client_secret: setupIntents.client_secret, Stripe_Publishable_Key, ephemeralKey: ephemeralKey.secret });

    } catch (err) {
        res.json({ status: false, cardObj, setupIntents, Stripe_Publishable_Key, err });

        Users.updateOne({ id: String(decode.id) }, {
            customer_id: null,

        }, async function (err) {
            if (err) console.trace(err);
        });

        Company.updateOne({
            company_id: String(String(decode.company_id))
        }, { subscription_id: null, product_id: null }, async function (err) {
            if (err) console.trace(err);
        });
    }



    // var cusObj = {};
    // cusObj.billingProvince = customer.address.city;
    // // cusObj.billingCountry = customer.address.country;
    // cusObj.billingStreetAddress = customer.address.line1;
    // cusObj.billingZipCode = customer.address.postal_code;
    // // cusObj.billingState = customer.address.state;

    // cusObj.billingFirstName = customer.name;
    // cusObj.billingLaststName = customer.name;

    // cusObj.default_source = customer.default_source;




});

router.post('/stripe_del_subscription', async function (req, res) {
    try {
        const jwt_token = extractToken(req);
        let decode = jwt.verify(jwt_token, process.env.SECRET);

        var company_info = await new Promise((resolve, reject) => {
            Company.findOne({ company_id: String(decode.company_id) }, function (error, company) {
                if (company) { resolve(company) } else reject()
            });
        });
        if (company_info.subscription_id) {
            await new Promise((resolve, reject) => {
                Company.updateOne({
                    company_id: String(decode.company_id)
                }, { subscription_id: null, product_id: null, plan_id: null }, async function (err) {
                    if (err) reject(err);
                    else resolve();
                });
            });
            // await new Promise((resolve, reject) => {
            //     models.instance.Users.update({
            //         customer_id: models.timeuuidFromString(String(decode.company_id))
            //     }, { subscription_id: null, product_id: null,plan_id: null }, update_if_exists, async function(err) {
            //         if (err) reject(err);
            //         else resolve();
            //     });
            // });

            const deleted = await stripe.subscriptions.del(company_info.subscription_id);


            res.json(true);
        } else {
            res.json(false);
        }

    } catch (e) {
        console.log(e);
        res.json(false);
    }

});

router.post('/getCallingUrl', function (req, res, next) {
    let d1 = Date.now();
    if (req.body.type == 'user') {
        Users.findOne({ short_id: req.body.short_id }, async function (error, user) {
            // console.log('getCallingUrl:user:', Date.now() - d1);
            if (user) {
                let user_id = user.id.toString();
                let url = "https://wfvs001.freeli.io/" + user_id.replace(/-/g, "");

                // let url = new URL( "/call/"+ convs.conversation_id.toString(),req.headers.origin);
                return res.json({
                    url: url,
                    participants: [user_id],
                    conversation_id: user_id,
                    is_running: (voip_conv_store.hasOwnProperty(user_id) && voip_conv_store[user_id].database_update == false),
                    // title: convs.title,
                    conversation_type: 'personal',
                    company_id: user.company_id
                });
            } else {
                return res.json({ url: false });
            }
        });

    } else {
        Conversation.findOne({ short_id: req.body.short_id }, async function (error, convs) {
            // console.log('getCallingUrl:conv:', Date.now() - d1);
            if (convs) {
                let conv_id = convs.conversation_id.toString();
                let url = "https://wfvs001.freeli.io/" + conv_id.replace(/-/g, "");
                // let url = new URL( "/call/"+ conv_id,req.headers.origin);
                return res.json({
                    url: url,
                    participants: convs.participants,
                    conversation_id: conv_id,
                    is_running: (voip_conv_store.hasOwnProperty(conv_id) && voip_conv_store[conv_id].database_update == false),
                    title: convs.title,
                    conversation_type: convs.group == 'yes' ? 'group' : 'personal',
                    company_id: convs.company_id
                });
            } else {
                return res.json({ url: false });
            }
        });

    }

});
router.post('/setCallingUrl', function (req, res, next) {
    if (req.body.type == 'group') {
        var query = {
            conversation_id: req.body.conversation_id,
            company_id: req.body.company_id
        }
        if (req.body.action == 'get') {
            Conversation.findOne(query, function (error, conv) {
                if (conv && conv.short_id) {
                    return res.json({ status: true, short_id: conv.short_id });
                } else {
                    let short_id_new = short.generate();
                    Conversation.update(query, { short_id: short_id_new }, function (err) {
                        if (err) {
                            console.log(err);
                            return res.json({ status: false });
                        } else {
                            return res.json({ status: true, short_id: short_id_new });
                        }
                    });
                }
            });

        } else if (req.body.action == 'reset') {
            let short_id_new = short.generate();
            Conversation.updateOne(query, { short_id: short_id_new }, function (err) {
                if (err) {
                    console.log(err);
                    return res.json({ status: false });
                } else {
                    return res.json({ status: true, short_id: short_id_new });
                }
            });

        }


    } else if (req.body.type == 'user') {
        var query = { id: req.body.user_id };
        if (req.body.action == 'get') {

            Users.findOne(query, function (error, user) {
                if (user && user.short_id) {
                    return res.json({ status: true, short_id: user.short_id });
                } else {
                    let short_id_new = short.generate();
                    Users.updateOne(query, { short_id: short_id_new }, function (err) {
                        if (err) {
                            console.log(err);
                            return res.json({ status: false });
                        } else {
                            all_users[req.body.company_id][req.body.user_id].short_id = short_id_new;
                            return res.json({ status: true, short_id: short_id_new });
                        }
                    });
                }
            });

        } else if (req.body.action == 'reset') {
            let short_id_new = short.generate();
            Users.updateOne(query, { short_id: short_id_new }, function (err) {
                if (err) {
                    console.log(err);
                    return res.json({ status: false });
                } else {
                    all_users[req.body.company_id][req.body.user_id].short_id = short_id_new;
                    return res.json({ status: true, short_id: short_id_new });
                }
            });

        }

    }

});

// router.get('/secret', async(req, res) => {
//     const paymentIntent = await stripe.paymentIntents.create({
//         amount: 1099,
//         currency: 'eur',
//         automatic_payment_methods: { enabled: true },
//     });
//     res.json({ client_secret: paymentIntent.client_secret });
// });


// select * from messages where call_participants contains('896c2fc4-deb9-4dcf-b64f-9b59c349ec54') and company_id='f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0' GROUP BY conversation_id ALLOW FILTERING;
function parseMuteSafely(str) {
    if (str === null || str === 'null') return [];
    try {
        if (str.startsWith('[') && str.endsWith(']')) {
            return JSON.parse(str);
        } else {
            return JSON.parse('['+ str +']');
        }
    } catch (e) {
        console.log("Error in JSON parse");
        if (typeof str == 'string') return [str];
        else return [];
    }
}
router.get('/get_all_emoji', function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let user_id = decode.id.toString();

    MessagesEmojis.find({ user_id: user_id }, function (err, list) {
        if (err) res.status(400).json({ status: false, err: err });
        else {
            res.status(201).json({ 'status': true, data: list });
        }
    });

});
router.get('/get_all_message', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let user_id = decode.id.toString();
    let query = {
        $or: [
            { participants: { $in: user_id } },
            { sender: user_id }
        ],
        // $and: [{archive: 'yes'}]
    };
    let list = await Messages.find(query).sort({ created_at: -1 });
    res.status(201).json({ 'status': true, data: list });

});
router.get('/get_all_file', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let user_id = decode.id.toString();
    let query = {
        $or: [
            { participants: { $in: user_id } },
        ]
    };
    let list = await File.find(query);
    res.status(201).json({ 'status': true, data: list });
});
router.get('/get_all_team', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let user_id = decode.id.toString();
    let query = {
        $or: [
            { participants: { $in: user_id } },
        ]
    };
    let list = await Team.find(query);
    res.status(201).json({ 'status': true, data: list });
});
router.get('/get_all_business_unit', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let company_id = decode.company_id.toString();
    let query = { company_id: company_id };
    let list = await BusinessUnit.find(query);
    res.status(201).json({ 'status': true, data: list });
});
router.get('/get_all_tag', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let company_id = decode.company_id.toString();
    let query = { company_id: company_id };
    let list = await Tag.find(query);
    res.status(201).json({ 'status': true, data: list });
});
router.get('/get_all_conversation', async function (req, res, next) {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let user_id = decode.id.toString();

    var query = { participants: { $in: user_id } };
    let convs = await Conversation.find(query);
    let conversations = [];
    
    if (convs.length > 0) {
        if (convs.length === 1 && convs[0].tag_list.length) {
            let taginfo = await get_all_tags(convs[0].tag_list);
            convs[0].tag_list_details = [];
            for (let t of taginfo) {
                convs[0].tag_list_details.push({
                    tag_id: t.tag_id,
                    tagged_by: t.tagged_by,
                    title: t.title,
                    tag_type: t.tag_type,
                    tag_color: t.tag_color
                });
            }
        }

        for (let key in convs) {
            let conv = convs[key];
            if (conv.has_mute && conv.has_mute.indexOf(user_id) > -1) {
                for (let m of conv.mute) {
                    let mute_data = parseMuteSafely(m);
                    for (let n = 0; n < mute_data.length; n++) {
                        if (mute_data[n].mute_by == user_id) {
                            conv.mute = [mute_data[n]];
                        }
                    }
                }
            } else {
                conv.mute = [];
            }

            conv.friend_id = "";
            conv.conv_is_active = 1;
            conv.close_for = conv.close_for.indexOf('yes') > -1 ? 'yes' : 'no';
            conv.temp_user = [];
            // Direct conversation
            if (conv.group === 'no') {
                if (conv.conversation_id === user_id)
                    var fid = conv.conversation_id;
                else
                    var fid = conv.participants.join().replace(user_id, '').replace(',', '');
                let this_user = await get_user_obj(fid);
                if (this_user.firstname !== '' && this_user.firstname !== undefined) {
                    conv.title = this_user.fullname ? this_user.fullname : (this_user.firstname + ' ' + this_user.lastname).trim();
                    conv.conv_img = this_user.img;
                    conv.friend_id = fid;
                    conv.fnln = fnln(this_user);
                    conv.conv_is_active = this_user.is_active;
                }
                else continue;
            }
            // Group conversation
            else {
                conv.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + conv.conv_img;
            }
            conversations.push(conv);
        }
    }
    res.status(201).json({ 'status': true, conv_list: conversations });

});

module.exports = router;