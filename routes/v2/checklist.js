const express = require('express');
const router = express.Router();
const {isValidJson, validateJSON} = require('../../v2_utils/common');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { set_checklist, set_checklist_item, delete_checklist_item } = require('../../v2_utils/task_manage');
const Task = require('../../mongo-models/task');
const Users = require('../../mongo-models/user');
var { save_notification, get_notification, get_notification_count } = require('../../v2_utils/notification');
var { xmpp_send_server, send_msg_firebase } = require('../../v2_utils/voip_util');

async function get_user_obj(id) {
    return new Promise((resolve, reject) => {
        for (let value of Object.entries(all_users)) {
            for (let v of value) {
                if (typeof v === 'object' && v.hasOwnProperty(id)) {
                    return resolve(v[id]);
                }
            }
        }
        if (cache_user) {
            return resolve({
                id: id,
                firstname: "",
                lastname: "",
                fullname: "",
                img: "",
                dept: "",
                designation: "",
                email: "",
                phone: "",
                access: [],
                company_id: "",
                company_name: "",
                conference_id: "",
                role: "",
                email_otp: "",
                is_active: 0,
                mute_all: "",
                mute: "",
                login_total: 0,
                fnln: "",
                createdat: ""
            });
        };
        // console.log('get_user_obj',id)
        Users.findOne({ id: String(id) }, function (error, user) {
            if (user) {
                return resolve({
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    fullname: user.firstname + ' ' + user.lastname,
                    img: process.env.FILE_SERVER + user.img,
                    dept: user.dept,
                    designation: user.designation,
                    email: user.email,
                    phone: Array.isArray(user.phone) ? user.phone.join("") : "",
                    access: user.access,
                    company_id: user.company_id,
                    company_name: user.company,
                    conference_id: user.conference_id,
                    role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
                    email_otp: user.email_otp,
                    is_active: user.is_active,
                    mute_all: user.mute_all,
                    mute: user.mute ? JSON.parse(user.mute) : '',
                    login_total: user.login_total,
                    fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
                    device: user.device ? user.device : [],
                    short_id: user.short_id,
                    createdat: user.createdat ? user.createdat : new Date(),
                    fcm_id: user.fcm_id
                });

            } else {
                return resolve({
                    id: id,
                    firstname: "",
                    lastname: "",
                    fullname: "",
                    img: "",
                    dept: "",
                    designation: "",
                    email: "",
                    phone: "",
                    access: [],
                    company_id: "",
                    company_name: "",
                    conference_id: "",
                    role: "",
                    email_otp: "",
                    is_active: 0,
                    mute_all: "",
                    mute: "",
                    login_total: 0,
                    fnln: "",
                    createdat: ""
                });
            }

        });

    })
    // var result = {};
}

/**
 * Create checklist
 * 
 */
router.post('/save', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let check_body_data = isValidJson(JSON.stringify(req.body));
    const requiredKeys = ["checklist", "items"];
    let missingKeys = validateJSON(req.body, requiredKeys);
    if (missingKeys.length === 0) {
        if(check_body_data){
            try{
                req.body.company_id = decode.company_id;
                req.body.created_by = decode.id;
                let p = await set_checklist({...req.body});
                if(req.body.participants && req.body.checklist?.task_id){
                    let data = {};
                    data.view_checklist = [...req.body.participants.filter((v)=> v != decode.id)];
                    data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
                    var task = await Task.findOneAndUpdate({ _id: req.body.checklist.task_id }, data, {new: true});
                    var uobj = await get_user_obj(decode.id);
                    save_notification({
                        company_id: decode.company_id,
                        receiver_id: task.participants,
                        title: `Checklist has been created.`,
                        type: "Task",
                        body: JSON.stringify(task),
                        user_id: task.created_by,
                        user_name: uobj.firstname + ' ' + uobj.lastname,
                        user_img: uobj.img,
                        task_id: task._id,
                        tab: "property"
                    }, req);
                    if(p.items?.length){
                        (data.view_update).forEach(async function(v, k){
                            xmpp_send_server(v.toString(), 'new_checklist', p.items[0].toObject(), req);
                        });

                    }
                    

                }
                return res.status(200).json({success: true, checklist: p.checklist, items: p.items});
            }catch(error){
                console.log('Unexpected error:', error.message);
                return res.status(400).json({success: false, error});
            }
        }else{
            return res.status(400).json({success: false, msg: "Data is not valid"}) 
        }
    } else {
        return res.status(400).json({success:false, error:`The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    }
});

/**
 * Create checklist item or 
 * Update
 * 
 */
router.post('/item_save', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let check_body_data = isValidJson(JSON.stringify(req.body));
    const requiredKeys = ["task_id", "checklist_id", "item_title"];
    let missingKeys = validateJSON(req.body, requiredKeys);
    if (missingKeys.length === 0) {
        if(check_body_data){
            try{
                req.body.company_id = decode.company_id;
                req.body.created_by = decode.id;
                let p = await set_checklist_item({...req.body});
                if(req.body.participants){
                    let data = {};
                    data.view_checklist = [...req.body.participants.filter((v)=> v != decode.id)];
                    data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
                    var task = await Task.findOneAndUpdate({ _id: req.body.task_id }, data, {new: true});
                    var uobj = await get_user_obj(decode.id);
                    save_notification({
                        company_id: decode.company_id,
                        receiver_id: task.participants,
                        title: `checklist has been updated.`,
                        type: "Task",
                        body: JSON.stringify(task),
                        user_id: task.created_by,
                        user_name: uobj.firstname + ' ' + uobj.lastname,
                        user_img: uobj.img,
                        task_id: task._id,
                        tab: "property"
                    }, req);

                    if(p.is_new){
                        (data.view_update).forEach(async function(v, k){
                            xmpp_send_server(v.toString(), 'new_checklist', p.item.toObject(), req);
                        });
                    }
                }
                
                return res.status(200).json(p);
            }catch(error){
                console.log('Unexpected error:', error.message);
                return res.status(400).json({success: false, error});
            }
        }else{
            return res.status(400).json({success: false, msg: "Data is not valid"}) 
        }
    } else {
        return res.status(400).json({success:false, error:`The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    }
});

/**
 * Delete checklist item 
 * _id = item id
 * 
 */
router.delete('/delete_item', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    try{
        req.query.created_by = decode.id;
        let p = await delete_checklist_item(req.query);
        if(p.success)
            return res.status(200).json(p);
        else throw p;
    }catch(error){
        console.log('Unexpected error:', error.message);
        return res.status(400).json({success: false, error});
    }
});

module.exports = router;