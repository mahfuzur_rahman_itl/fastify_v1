const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const _ = require('lodash');
var moment = require('moment');
const isUuid = require('uuid-validate');

var {extractToken, token_verify_decode} = require('../../v2_utils/jwt_helper');
var {get_user_obj} = require('../../v2_utils/user');
var {get_team} = require('../../v2_utils/team');
var {get_bunit} = require('../../v2_utils/business_unit');
var { get_message } = require('../../v2_utils/message');
var { xmpp_send_server } = require('../../v2_utils/voip_util');

// var {models} = require('../../config/db/express-cassandra');
const { get_conversation } = require('../../v2_utils/conversation');
const Messages = require('../../mongo-models/message');
const Notifications = require('../../mongo-models/notification');

/**
 * Get notification list
 */
router.post('/get_notifications', async (req, res) => {
  // console.log(14, req.body);
  const token = extractToken(req);
  var decode = await token_verify_decode(token);
  try{
    var notifications = [];
    var notiids = [];

    const PAGE_SIZE = 20;
    const page = Number(req.body.page) || 1;

    req.body.read_status = req.body.read_status ? req.body.read_status : 'no';
    var query = {receiver_id: decode.id};
    if(req.body.read_status === 'archive'){
      query.read_status = 'archive';
    }else{
      query.read_status = {$ne : 'archive'};
    }
    
    const count_total = [{$match: query}, {$count: "total"}];
    const dataAggregate = [
        {$match: query}, 
        { $sort: { created_at: -1 } },
        { $skip: (page - 1) * PAGE_SIZE },
        { $limit: PAGE_SIZE }
    ];
    // console.log(50, dataAggregate);
    const [countResult, notis] = await Promise.all([
      Notifications.aggregate(count_total),
      Notifications.aggregate(dataAggregate)
    ]);
    var total = countResult[0] ? countResult[0].total : 0;
    var totalPages = Math.ceil(total / PAGE_SIZE);
        
    for(let v of notis){
      v.fnln = all_users[decode.company_id][v.created_by_id].fnln;
      v.created_by_email = all_users[decode.company_id][v.created_by_id].email;
      if(v.read_status === 'no') 
        notiids.push(v._id);
      notifications.push(v);
    }

    if(notiids.length > 0){
      Notifications.updateMany({receiver_id: decode.id, _id: { $in: notiids }}, {read_status: 'yes'}, function(error){
        if(error) console.log('Notification read status not update ', error);
        xmpp_send_server(decode.id, 'read_status_notification', {read: notiids.length}, req);
      });
    }
    
    // var pageState = result.pageState;
    return res.status(200).json({ notifications, pagination: {
        page: parseInt(page),
        totalPages,
        total
    } });
  }catch(e){
    return res.status(400).json(e);
  }
});

/**
 * Go to notification
 */
router.post('/goto', async (req, res) => {
  // console.log("41 goto", req.body);
  const token = extractToken(req);
  var decode = await token_verify_decode(token);
  var msgs = [];
  try{
    var conv = await get_conversation({user_id: decode.id, company_id: decode.company_id, conversation_id: req.body.conversation_id});
    if(! conv){
      return res.status(404).json({ error: 'Find error', message: 'No conversation found' });
    }
    
    if(req.body.msg_id && isUuid(req.body.msg_id)){
      // msgs = await get_message(req.body.conversation_id, req.body.msg_id, 1, '', '', 'no');
      Messages.findOne({conversation_id: req.body.conversation_id, msg_id: req.body.msg_id}, async function(e, m){
        if(e || !m){
          console.log(175, e); 
          return res.status(200).json({details: conv[0], msgs: [], error: "No message found"}); 
        }
        else{
          if(m.is_reply_msg === 'yes')
            msgs = await get_message({convid: req.body.conversation_id, msgid: m.reply_for_msgid.toString(), limit: 1, is_reply: 'no', user_id: decode.id, company_id: decode.company_id });
          else
            msgs = await get_message({convid: req.body.conversation_id, msgid: req.body.msg_id, limit: 1, is_reply: 'no', user_id: decode.id, company_id: decode.company_id });
          return res.status(200).json({details: conv[0], msgs});
        }
      });
    }
    else{
      return res.status(200).json({details: conv[0], msgs});
    }
  }catch(e){
    return res.status(400).json(e);
  }
});

/**
 * Delete notification
 * id = notification id
 */
router.post('/delete', (req, res) => {
  // console.log("117 delete", req.body)
  // const token = extractToken(req);
  // let decode = jwt.verify(token, process.env.SECRET);
  // try{
  //   var query = {receiver_id: decode.id};
  //   var nids = [];
  //   if(req.body.id && Array.isArray(req.body.id)){
  //     req.body.id.forEach(function(v, k){
  //       if(isUuid(v)){
  //         nids.push(models.timeuuidFromString(v));
  //       }
  //     });
  //   }
  //   if(nids.length>0){
  //     query.notification_id = { '$in': nids };
  //   }
  //   else
  //     return res.status(404).json({ error: 'Delete error', message: 'No id found' });
      
  //     models.instance.Notifications.update(query, {read_status: 'archive'}, function(error){
  //       if (error) return res.status(404).json({ error: 'Delete error', message: error });
        
  //       return res.status(200).json({status: true, message: 'Success', data: req.body});
  //     });
  // }catch(e){
  //   return res.status(400).json(e);
  // }
});


module.exports = router;
