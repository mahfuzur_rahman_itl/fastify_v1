const express = require('express');
const router = express.Router();
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { get_project, set_project } = require('../../v2_utils/task_manage');

/**
 * Get all project
 * _id
 * project_title
 */
router.get('/get_all', async(req, res)=>{
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    if(! req.query.company_id) 
        req.query.company_id = decode.company_id;
    const p = await get_project(req.query);
    if(p.success)
        res.status(200).json(p);
    else
        res.status(400).json(p);
});

router.post('/save', async (req, res) => {
    try{
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        req.body.created_by = decode.id;
        req.body.company_id = decode.company_id;
        let p = await set_project(req.body);
        if(p.success)
            res.status(200).json(p);
        else 
            res.status(400).json(p);
    }catch(e){
        res.status(400).json({success: false, error: e});
    }
});

module.exports = router;