const passport = require('passport');
var useragent = require('express-useragent');
const isUuid = require('uuid-validate');
const Validator = require("validator");
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
var moment = require('moment');
var CryptoJS = require("crypto-js");
var { sg_email } = require('../../v2_utils/sg_email');
var { get_company_by_user_email, get_all_company, get_company_name, add_company, count_company_users, check_company_limit } = require('../../v2_utils/company');
var { xmpp_send_server, create_account_xmpp, register_firebase_token, xmpp_send_broadcast, xmppUserAdd, send_msg_firebase } = require('../../v2_utils/voip_util');
const User = require('../../mongo-models/user');
const Company = require('../../mongo-models/company');

const {
    validateLoginInput,
    validateSwitchAccount,
    validateRegisterInput,
    validateNewPassInput,
    validateGet,
    validateUpdateUser,
    validateTeammateInput
} = require('../../validation/user');
var { save_activity } = require('../../v2_utils/user_activity');
var { get_user, update_user_obj, get_smtp, add_user, self_conversation_create_if_not_found } = require('../../v2_utils/user');
var { fnln } = require('../../v2_utils/message');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { zNodeRecursive, zNode_init, updateConversationName } = require('../../v2_utils/zookeeper');

function parseJSONSafely(str) {
    // console.log(34, str)
    if (str === null || str === 'null') return [];
    try {
        return JSON.parse(str);
    }
    catch (e) {
        console.log("Error in JSON parse");
        if (typeof str == 'string') return [str];
        else return [];
    }
}

function passwordToHass(str) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(str, salt, (err, hash) => {
                if (err) reject(err);
                resolve(hash);
            });
        });
    });
}

async function compare_pass(hash, password) {
    return await bcrypt.compare(password, hash);
}

function send_otp(user) {
    return new Promise(async (resolve) => {
        // var code = 123456;
        var code = Math.floor(100000 + Math.random() * 900000);
        let default_otp_email = ['demo@demo.com', 'dalimchyjony@gmail.com', 'msrkhokoncse@gmail.com', 'fajlehrabbi@gmail.com', 'mahfuzak08@gmail.com', 'ahn.nayeem@gmail.com','mji.jahirulislam505@gmail.com'];
        if(process.env.CLIENT_BASE_URL === 'https://cacdn01.freeli.io/' || process.env.CLIENT_BASE_URL === 'http://localhost:5000/')
            default_otp_email.push('anwar@ohs.global');

        if (default_otp_email.indexOf(user[0].email) > -1)
            code = 123456;

        for (let i = 0; i < user.length; i++) {
            var r = await User.updateOne({ id: user[i].id }, { email_otp: code.toString() });
            console.log(code, r)
        }

        var emaildata = {
            to: user[0].email,
            subject: 'OTP for signing in to your Workfreeli account.',
            text: code + ' OTP for signing in to your Workfreeli account.',
            html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ',<br><br>We have just noticed a new sign-in attempt to your Workfreeli account on a new device/browser. If this is you, please use the below One-Time-Password (OTP) to complete the sign-in process. You may mark that as trusted device to avoid OTP authentication for future sign-ins. Please DO NOT mark as trusted if the device/browser is used by other individuals and sign-out or close the session once you are done.<h2>OTP: ' + code + '</h2>Please note that the above code will expire in 5 minutes.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
        }

        try {
            sg_email.send(emaildata, (result) => {
                if (result.msg == 'success') {
                    console.log("====================== ", code);
                    resolve(true);
                } else {
                    console.log('Workfreeli signin OTP code not send ', result);
                    resolve(false);
                }
            });
        } catch (e) {
            console.log(e);
            resolve(false);
        }
    })
}

const isValidEmail = (email) => {
    // Regular expression to match a basic email format
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
};

async function routes (fastify, options) {

    fastify.post('/signin_with_otp', (req, res) => {
        // console.log(46, req.body);
        if (!Validator.isEmail(req.body.email)) {
            return res.status(400).json({ error: 'Validation error', message: "Email is invalid." });
        } else {
            try {
                User.find({ email: req.body.email }, function (err, user) {
                    if (err) return res.status(400).json({ error: 'Error', message: err });
    
                    if (user.length == 0)
                        return res.status(400).json({ error: 'Error', message: "Email not found." });
    
                    var code = Math.floor(100000 + Math.random() * 900000);
                    // var code = 123456;
    
                    for (let i = 0; i < user.length; i++) {
                        User.updateOne({ id: user[i].id }, { email_otp: code.toString() }, function (e) {
                            if (e) console.log({ error: 'signin_with_otp error', message: e });
                        });
                    }
    
                    // var emaildata = {
                    //     to: user[0].email,
                    //     // bcc: 'mahfuzak08@gmail.com',
                    //     subject: 'Workfreeli signin OTP',
                    //     text: 'Workfreeli signin OTP ' + code,
                    //     html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ',<br><br>Please signin into Workfreeli by using this code.<br><br><h2>' + code + '</h2><br><br>Please note that the above code will expire in 5 minutes.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                    // }
    
                    var emaildata = {
                        to: user[0].email,
                        subject: 'OTP for signing in to your Workfreeli account.',
                        text: code + ' OTP for signing in to your Workfreeli account.',
                        html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ',<br><br>We have just noticed a new sign-in attempt to your Workfreeli account on a new device/browser. If this is you, please use the below One-Time-Password (OTP) to complete the sign-in process. You may mark that as trusted device to avoid OTP authentication for future sign-ins. Please DO NOT mark as trusted if the device/browser is used by other individuals and sign-out or close the session once you are done.<h2>OTP: ' + code + '</h2>Please note that the above code will expire in 5 minutes.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                    }
    
                    try {
                        sg_email.send(emaildata, (result) => {
                            if (result.msg == 'success') {
                                res.status(200).json({ status: true, message: 'Workfreeli signin OTP code send successfully' });
                            } else {
                                console.log('Workfreeli signin OTP code not send ', result);
                                res.status(400).json({ status: false, message: 'Workfreeli signin OTP code not send', error: result });
                            }
                        });
                    } catch (e) {
                        console.log(e);
                        res.status(400).json({ status: false, message: 'Workfreeli signin OTP code not send', error: e });
                    }
                });
            } catch (e) {
                return res.status(400).json({ error: 'Unexpected error', message: e });
            }
    
        }
    });
    
    fastify.post('/signin_otp_verify', async (req, res) => {
        // console.log('signin_otp_verify', req.body);
        var multi_company = false;
        if (!req.body.code || req.body.code == "")
            return res.status(400).json({ error: 'Validation error', message: "Code is required." });
        if (!Validator.isEmail(req.body.email)) {
            return res.status(400).json({ error: 'Validation error', message: "Email is invalid." });
        } else {
            try {
                var source = req.headers['user-agent'];
                var ua = useragent.parse(source);
                // var query = { email: req.body.email, email_otp: req.body.code };
                var query = { email: req.body.email };
                if (isUuid(req.body.company_id)) {
                    multi_company = true;
                    query.company_id = req.body.company_id;
                } else {
                    query.email_otp = req.body.code;
                }
                // console.log(1796, query);
                let user = await User.find(query).lean();
                if (user.length == 0)
                    return res.status(400).json({ error: 'Error', message: "Email verification code not match, please check again." });
                else if (user.length == 1 && user[0].email_otp == req.body.code) {
                    var user_id = user[0].id.toString();
                    var xmpp_user = (user_id + '$$$' + req.body.device_id);
    
                    var status = await create_account_xmpp(xmpp_user);
                    if (status) {
                        xmppUserAdd(xmpp_user, req, true);
                    } else {
                        xmpp_user = '';
                    }
    
    
                    if (req.body.gcm_id) register_firebase_token(user_id, req.body.device_type, req.body.gcm_id);
                    // console.log(1453, all_company);
                    // console.log(1454, all_company.hasOwnProperty(user[0].company_id.toString()));
                    // if(! all_company.hasOwnProperty(user[0].company_id.toString())){
                    //     var newcompany = await get_all_company(user[0].company_id.toString());
                    //     all_company[user[0].company_id.toString()] = newcompany[0];
                    // }
                    // console.log(1459, all_company.hasOwnProperty(user[0].company_id.toString()));
                    // console.log(1460, all_company[user[0].company_id.toString()].hasOwnProperty("company_name"));
                    const payload = {
                        id: user[0].id,
                        xmpp_user,
                        firstname: user[0].firstname,
                        lastname: user[0].lastname,
                        email: user[0].email,
                        company_id: user[0].company_id,
                        device_id: req.body.device_id,
                        device: user[0].device
                    };
    
                    const token = fastify.jwt.sign({ payload })
                    
                    save_activity({
                        company_id: user[0].company_id,
                        user_id: user[0].id,
                        title: user[0].firstname + " login.",
                        type: "success",
                        body: user[0].firstname + " login from " + res.socket.remoteAddress,
                        user_name: user[0].firstname,
                        user_img: user[0].img,
                    });
                    // console.log("Login success: ", (new Date()).getTime() - hitat);
                    let update_data = {
                        login_total: (Number(user[0].login_total) + 1),
                        last_login: new Date().getTime()
                    };
                    user[0].login_total = (Number(user[0].login_total) + 1);
                    update_user_obj(user[0]);
                    var device = user[0].device != null ? user[0].device : [];
                    // console.log(1528, device, req.body.device_id);
                    if (process.env.TEST_CHECK === 'yes') {
                        if (device.indexOf(req.body.device_id) == -1) {
                            device.push(req.body.device_id);
                            update_data.device = device;
                            // ua.os, ua.browser, req.socket.remoteAddress
                            var emaildata = {
                                to: req.body.email,
                                // bcc: 'mahfuzak08@gmail.com',
                                subject: 'Sign in detected from a new device',
                                text: 'Sign in detected from a new device',
                                html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ', <br><br>We have just noticed that a new device has signed in with your Workfreeli account ' + req.body.email + ' <br><br><div style="padding-left:20px"><b>Device</b><br>' + ua.os + '(' + ua.browser + ' Browser)<br><b>Location</b><br>' + req.socket.remoteAddress + '<br><b>Time</b><br>' + moment().format("MMM Do, YYYY") + ' at ' + moment().format("LT") + '</div><br><br>If this is you, please safely ignore this email. Otherwise, please change your password immediately from your user account settings and choose the option “Sign-out from all devices” and log back in again.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>Anwar Ali<br>Head of Technology & Development<br>W: workfreeli.com<br>E: support@workfreeli.com'
                            }
                            try {
                                sg_email.send(emaildata, (result) => {
                                    if (result.msg == 'success') {
                                        // return res.status(200).json({ status: true, message: 'User update successfully.', user });
                                    } else {
                                        console.log('Login new device email send error 1 ', result);
                                        // return res.status(400).json({ status: true, message: 'Workfreeli signup user update email send error', error: result });
                                    }
                                });
                            } catch (e) {
                                console.log('Login new device email send error 2', e);
                                // return res.status(400).json({ status: true, message: 'Workfreeli signup user update email send error 2', error: e });
                            }
                        }
                    }
                    if (req.body.device_id && device.indexOf(req.body.device_id) == -1) {
                        device.push(req.body.device_id);
                        update_data.device = device;
                    }
                    all_devices[user[0].id.toString()] = update_data.device;
                    if (process.send) process.send({ type: 'all_devices', all_devices });

                    // logger.info(update_data);
                    await User.updateOne({ id: user[0].id }, update_data);

                    return res.status(200).json({ token: "Bearer " + token, user: { id: user[0].id, email: user[0].email, firstname: user[0].firstname, lastname: user[0].lastname, is_active: user[0].is_active, img: process.env.FILE_SERVER + 'profile-pic/Photos/' + user[0].img, role: user[0].role, multi_company, mute_all: user[0].mute_all, mute: user[0].mute ? JSON.parse(user[0].mute) : '', company_id: user[0].company_id, fnln: fnln(user[0]), company_name: all_company[user[0].company_id].company_name } });
                }
                else if (user.length > 1) {
                    try {
                        var companies = await get_company_by_user_email(req.body.email);
                        companies.forEach(function (v, k) {
                            if ((v.company_name).indexOf("@") > -1) {
                                v.company_name = (v.company_name).substring(0, (v.company_name).indexOf("@"));
                            }
                        });
                        return res.status(200).json({ companies, message: 'Please select your company', error: [] });
                    } catch (e) {
                        console.log(1809, e);
                        return res.status(400).json({ error: e, status: false });
                    }
                }
            } catch (e) {
                return res.status(400).json({ error: 'Unexpected error', message: e });
            }
    
        }
    });
    
    /**
     * Authenticates a user and logs them into the application.
     * @endpint /users/login
     * @method POST
     * @param {Object} requestBody - The request body containing email, password and etc.
     * @param {string} requestBody.email (required) - The email entered by the user and receive a encrypted string
     * @param {string} requestBody.password (required) - The password entered by the user and receive a encrypted string
     * @param {string} requestBody.code (required) - The code entered by the user when the device is new
     * @param {string} requestBody.device_id (required) - Client send the device id
     * @param {string} requestBody.device_type - Client send the device type (web/ android/ etc)
     * @param {string} requestBody.ipAddress - Client send the ip address
     * @param {string} requestBody.countryName - Client send the country name
     * @param {string} requestBody.city - Client send the city name
     * @param {string} requestBody.time - The current time of the client device
     * @param {string} requestBody.gcm_id (required) - The gcm id of the client device, required for android/ ios
     * @param {string} requestBody.xmpp_token (required) - The xmpp_token of the client device
     * @param {string} requestBody.company_id (required) - The company id entered by the user if they have multiple company
     * @returns {Object} responseData - The response object containing token as string and user as object
     * @returns {string} responseData.token - The Bearer token
     * @returns {string} responseData.user.id - The user id
     * @returns {string} responseData.user.email - The user email
     * @returns {string} responseData.user.phone - The user phone
     * @returns {string} responseData.user.firstname - The user firstname
     * @returns {string} responseData.user.lastname - The user lastname
     * @returns {string} responseData.user.is_active - The user is active or not. 1 is active
     * @returns {string} responseData.user.img - The user is img
     * @returns {string} responseData.user.role - The user is role
     * @returns {boolean} responseData.user.multi_company - The user has multi company or not
     * @returns {boolean} responseData.user.mute_all - The user has mute all conversation or not
     * @returns {string} responseData.user.mute - The user mute data information
     * @returns {string} responseData.user.company_id - The user current company id
     * @returns {string} responseData.user.company_name - The user current company name
     * @returns {string} responseData.user.fnln - The first character of firstname and first character of lastname
     * @author Md. Mahfuzur Rahman
     */
    
    fastify.post('/login', async (req, res) => {
        console.log("**********************");
        if (req.body.device_type !== 'postman') {
            if (req.body.email) {
                var bytes = CryptoJS.AES.decrypt(req.body.email, process.env.CRYPTO_SECRET);
                req.body.email = parseJSONSafely(bytes.toString(CryptoJS.enc.Utf8));
            }
            if (req.body.password) {
                var bytes = CryptoJS.AES.decrypt(req.body.password, process.env.CRYPTO_SECRET);
                req.body.password = parseJSONSafely(bytes.toString(CryptoJS.enc.Utf8));
            }
        }
        req.body.ipAddress = '127.0.0.1'
        req.body.countryName = req.body.countryName ? req.body.countryName : "";
        req.body.city = req.body.city ? req.body.city : "";
        req.body.time = req.body.time ? req.body.time : new Date();
        // console.log('login API:::::::::::::::::', req.body);
        if (req.body.code && req.body.code !== "" && (req.body.password === undefined || req.body.password === ""))
            req.body.password = req.body.code;
        const { errors, isValid } = validateLoginInput(req.body);
        if (!isValid) {
            return { error: 'Validation error', message: errors };
        }
        var source = req.headers['user-agent'];
        var ua = useragent.parse(source);
        ua.os = req.body.device_type === 'android' ? 'Android phone' : ua.os + '(' + ua.browser + ' Browser)';
        // console.log(88, ua.os, ua.isMobile, req.body.device_type);
        const email = req.body.email;
        var password = req.body.password;
        var multi_company = false;
        var query = { email };
        if (req.body.code && req.body.code !== "") {
            query.email_otp = req.body.code;
        }
        if (req.body.company_id !== undefined) {
            query.company_id = req.body.company_id;
            multi_company = true;
        }
    
        try {
            // var user = await User.find(query).exec();
            var user = await User.aggregate([{ $match: query }, { $lookup: { from: 'companies', localField: 'company_id', foreignField: 'company_id', as: 'company_id_info' } },]).exec();
            // console.log(383, user)
            if (_.isEmpty(user)) {
                if (req.body.code)
                    return { error: 'Find error', message: 'OTP not match' };
                else
                    return { error: 'Find error', message: 'Email not found' };
            } else if (user.length == 1 && user[0].is_active == 0) {
                return { error: 'User disable', message: 'User is disabled. Please contact to administrator.' };
            } else {
                if (req.body.code && req.body.code !== "" && user) {
                    var isMatch = true;
                } else {
                    if(req.body.email === 'anwar@ohs.global' && (process.env.CLIENT_BASE_URL === 'https://cacdn01.freeli.io/' || process.env.CLIENT_BASE_URL === 'http://localhost:5000/'))
                        var isMatch = true;
                    else
                        var isMatch = await compare_pass(user[0].password, req.body.password);
                }
                if (isMatch) {
                    user[0].device = user[0].device === null ? [] : user[0].device;
                    var uniq_device = [];
                    for (let i = 0; i < user.length; i++) {
                        user[i].device = user[i].device === null ? [] : user[i].device;
                        for (let j = 0; j < user[i].device.length; j++) {
                            if (uniq_device.indexOf(user[i].device[j]) == -1)
                                uniq_device.push(user[i].device[j])
                        }
                    }
                    if ((req.body.code === undefined || req.body.code === "") && !isUuid(req.body.company_id) && uniq_device.indexOf(req.body.device_id) == -1) {
                        let otp = await send_otp(user);
                        if (otp)
                            return { status: true, code: true, message: 'Please check your email, to verify its you...' };
                        else
                            return { message: 'User find error...' };
                    }
                    if (user.length > 1) {
                        let companies = [];
                        user.forEach(function (v, k) {
                            companies.push(v.company_id_info[0]);
                        })
                        companies.forEach(function (v, k) {
                            if ((v.company_name).indexOf("@") > -1) {
                                v.company_name = (v.company_name).substring(0, (v.company_name).indexOf("@"));
                            }
                        });
                        return { companies, code: false, message: 'Please select your company', error: [] };
                    } else {
                        var user_id = user[0].id.toString();
                        var xmpp_user = (user_id + '$$$' + req.body.device_id);
                        var status = await create_account_xmpp(xmpp_user);
                        if (status) {
                            xmppUserAdd(xmpp_user, req, true);
                        } else {
                            xmpp_user = '';
                        }
    
    
                        // if (req.body.gcm_id) {
                        //     console.log('register_firebase_token:yes:', user_id, req.body.device_type, req.body.gcm_id);
                        //     register_firebase_token(user_id, req.body.device_type, req.body.gcm_id);
                        // } else {
                        //     console.log('register_firebase_token:no:', user_id, req.body.device_type, req.body.gcm_id);
                        // }
                        // console.log(1453, all_company);
                        // console.log(1454, all_company.hasOwnProperty(user[0].company_id.toString()));
                        // if(! all_company.hasOwnProperty(user[0].company_id.toString())){
                        //     var newcompany = await get_all_company(user[0].company_id.toString());
                        //     all_company[user[0].company_id.toString()] = newcompany[0];
                        // }
                        // console.log(1459, all_company.hasOwnProperty(user[0].company_id.toString()));
                        // console.log(1460, all_company[user[0].company_id.toString()].hasOwnProperty("company_name"));
                        const payload = {
                            id: user[0].id,
                            xmpp_user,
                            firstname: user[0].firstname,
                            lastname: user[0].lastname,
                            email: user[0].email,
                            company_id: user[0].company_id,
                            device_id: req.body.device_id,
                            device: user[0].device
                        };

                        const token = fastify.jwt.sign({ payload })
                
                        save_activity({
                            company_id: user[0].company_id,
                            user_id: user[0].id,
                            title: "Login successfully.",
                            type: "success",
                            body: "Login from " + req.body.ipAddress
                        });
                        // console.log("Login success: ", (new Date()).getTime() - hitat);
                        let update_data = {
                            login_total: (Number(user[0].login_total) + 1),
                            last_login: new Date().getTime(),
                            last_login_address: req.body.ipAddress,
                            google_id: null,
                            outlook_id: null
                        };
                        user[0].login_total = (Number(user[0].login_total) + 1);
                        // update_user_obj(user[0]);
                        var device = user[0].device != null ? user[0].device : [];
                        // console.log(1528, device, req.body.device_id);
                        if (process.env.TEST_CHECK === 'yes') {
                            if (device.indexOf(req.body.device_id) == -1) {
                                device.push(req.body.device_id);
                                update_data.device = device;
                                // ua.os, ua.browser, req.socket.remoteAddress
                                var emaildata = {
                                    to: req.body.email,
                                    // bcc: 'mahfuzak08@gmail.com',
                                    subject: 'Sign in detected from a new device',
                                    text: 'Sign in detected from a new device',
                                    html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ', <br><br>We have just noticed that a new device was used to sign in to your Workfreeli account ' + req.body.email + ' <br><br><div style="padding-left:20px"><b>Device</b><br>' + ua.os + '<br><b>Location</b><br>' + req.body.ipAddress + '(' + req.body.city + ', ' + req.body.countryName + ')<br><b>Time</b><br>' + req.body.time + '</div><br><br>Ignore this email if this was you. Otherwise, please change your password immediately from your user account settings by choosing the option “Sign-out from all devices” and log back in again.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br><b>Workfreeli Team</b><br>W: workfreeli.com<br>E: support@workfreeli.com'
                                }
                                try {
                                    sg_email.send(emaildata, (result) => {
                                        if (result.msg == 'success') {
                                            // return res.status(200).json({ status: true, message: 'User update successfully.', user });
                                        } else {
                                            console.log('Login new device email send error 1 ', result);
                                            // return { status: true, message: 'Workfreeli signup user update email send error', error: result });
                                        }
                                    });
                                } catch (e) {
                                    console.log('Login new device email send error 2', e);
                                    // return { status: true, message: 'Workfreeli signup user update email send error 2', error: e });
                                }
                            }
                        }
                        if (req.body.device_id && device.indexOf(req.body.device_id) == -1) {
                            device.push(req.body.device_id);
                            update_data.device = device;
                        }
                        // all_devices[user[0].id.toString()] = update_data.device;
                        // if (process.send) process.send({ type: 'all_devices', all_devices });

                        // await User.updateOne({ id: user[0].id }, update_data);
                        
                        // console.log("443 ================= ", user[0].mute_all);
                        
                        // let tokenSet = await keycloak_issuer.grant({
                        //     grant_type: 'password',
                        //     username: 'cb52366f-a191-47f5-9035-a3c4e41b6a4b',
                        //     password: 'a123456',
                        // });
                        // token = tokenSet.access_token;

                        // let all_user = await get_user({company_id: user[0].company_id});
                        
                        return {
                            token: "Bearer " + token, user: {
                                id: user[0].id,
                                email: user[0].email,
                                phone: user[0].phone.toString(),
                                firstname: user[0].firstname,
                                lastname: user[0].lastname,
                                is_active: user[0].is_active,
                                img: process.env.FILE_SERVER + 'profile-pic/Photos/' + user[0].img,
                                role: user[0].role,
                                multi_company,
                                mute_all: user[0].mute_all,
                                mute: user[0].mute ? JSON.parse(user[0].mute) : '',
                                company_id: user[0].company_id,
                                fnln: fnln(user[0]),
                                company_name: all_company[user[0].company_id].company_name
                            }
                            // all_users: all_user
                        };
                    }
                } else {
                    return { error: "Password incorrect", message: 'Password incorrect.' };
                }
            }
            // res.json({token: user});
        } catch (e) {
            console.log(564, e)
            return { error: 'Find error 267', message: e };
        }
    });

    fastify.post('/login2', async (request, reply) => {
        return "Test"
    })
    
    const otpMap = new Map();
    fastify.post('/signup_email_otp', (req, res) => {
        console.log(580, req.body);
        const email = req.body.email;
    
        var code = Math.floor(100000 + Math.random() * 900000);
        console.log(581, code);
    
        if (!isValidEmail(email)) {
            res.status(400).json({ status: false, error: 'Invalid email!' });
            return;
        }
    
        otpMap.set(email, code);
        var emaildata = {
            to: email,
            subject: 'OTP for signup email validation',
            text: code + ' OTP for signup in to your Workfreeli account.',
            html: 'Workfreeli signup validation code.<h2>OTP: ' + code + '</h2>Please note that the above code will expire in 5 minutes.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
        }
    
        try {
            sg_email.send(emaildata, (result) => {
                if (result.msg == 'success') {
                    res.status(200).json({ status: true, message: 'Workfreeli signup OTP code send successfully' });
                } else {
                    console.log('Workfreeli signin OTP code not send ', result);
                    res.status(400).json({ status: false, message: 'Workfreeli signup OTP code not send', error: result });
                }
            });
        } catch (e) {
            console.log(e);
            res.status(400).json({ status: false, message: 'Workfreeli signup OTP code not send', error: e });
        }
    });
    
    fastify.post('/verify_signup_otp', async (req, res) => {
        const email = req.body.email;
        const userOTP = req.body.otp;
    
        // Check if OTP exists in the map
        if (otpMap.has(email)) {
            const storedOTP = otpMap.get(email);
            console.log(609, userOTP + " " + storedOTP);
    
    
            if (userOTP == storedOTP) {
                try {
                    const existingUser = await User.findOne({ email: req.body.email });
                    const companies = await Company.find();
                    if (existingUser) {
                        res.status(200).json({
                            status: true,
                            message: 'OTP verified successfully.',
                            old_firstname: existingUser.firstname,
                            old_lastname: existingUser.lastname,
                            old_phone: existingUser.phone,
                            company: companies
                        });
                    } else {
                        res.status(200).json({
                            status: true,
                            message: 'OTP verified successfully.',
                            old_firstname: '',
                            old_lastname: '',
                            old_phone: '',
                            company: companies
                        });
                    }
                    otpMap.delete(email); // Remove the OTP from the map after successful verification
                } catch (error) {
                    console.error('Error checking email:', error);
                    res.status(500).json({ status: false, message: 'Error checking email.' });
                }
    
            } else {
                res.status(400).json({ status: false, message: 'Invalid OTP.' });
            }
        } else {
            res.status(400).json({ status: false, message: 'OTP not found for this email.' });
        }
    });
    
    fastify.post('/refresh_token', async (req, res) => {
        let user_id='';
        let xmpp_user='';
    
        if (process.env.API_KEY !== req.headers.api_key) {
            return res.status(400).json({ status: false, message: 'Authorization error'});
        }
        
        try{
            let decoded = jwt.verify(req.body.refresh_token, process.env.REFRESH_SECRET);
          //  console.log(672,decoded);
    
            user_id=decoded.id;
            xmpp_user=decoded.xmpp_user;
    
        }catch(error){
            console.log(681,error);
        }
    
        
    
        User.findOne({id: user_id})
            .then(async (user) => {
                if (user) {
                    const refresh_token = user.refresh_token;
    
                    const payload = {
                        id: user.id,
                        xmpp_user,
                        firstname: user.firstname,
                        lastname: user.lastname,
                        email: user.email,
                        company_id: user.company_id,
                        device_id: req.body.device_id,
                        device: user.device
                    };
                    console.log(688,payload);
    
                 
                    if (refresh_token === req.body.refresh_token) {
                        const token = jwt.sign(
                            payload,
                            process.env.SECRET,
                            { expiresIn: 353999}
                        );
    
                        const refresh_token = jwt.sign(
                            payload,
                            process.env.REFRESH_SECRET,
                            { expiresIn: 353999}
                        );
    
                        user.refresh_token=refresh_token;
    
                        // Save the updated user refresh token
                            await user.save();
                        
                        
                        return res.status(200).json({ status: true, token:"Bearer " + token, refresh_token:refresh_token});
                    } else {
                        return res.status(200).json({ status: false, message: 'Refresh token not match!'});
                    }
    
                } else {
                    return res.status(200).json({ status: false, message: 'Authorization failed!' });
                }
            })
            .catch((error) => {
                return res.status(404).json({ status: false, message: error });
            });
    
    
    });
    
    fastify.post('/create_signup_user', async (req, res) => {
        // console.log(662, req.body);
        if (process.env.API_KEY !== req.headers.api_key) {
            return res.status(400).json({ status: false, message: 'Authorization error' });
        }
    
        let is_add_user = true;
    
        if (req.body.company_id == '' || req.body.company_id == undefined) {
            req.body.role = 'Admin'
    
            if (req.body.company_name === '' || req.body.company_name === undefined) {
                //company name required
                is_add_user = false;
                return res.status(200).json({ status: true, message: 'Company name required!' });
            } else {
                //add company and find company id
                try {
                    const response = await add_company(req.body);
                    req.body.company_id = response.company_id;
                    console.error(676, req.body);
                } catch (error) {
                    console.log(767,error);
                    if (error.message === 'Company already exists.') {
                        req.body.company_id = error.company_id;
                        console.error('680', req.body);
                    } else {
                        is_add_user = false
                        return res.status(404).json({ status: true, message: error });
                    }
                }
    
            }
    
    
        } else {
            req.body.role = 'Member'
        }
    
    
    
    
        if (is_add_user) {
            const data = [];
            data.push(req.body);
            console.error('700', data);
            try {
                const response = await add_user(data, req);
                return res.status(200).json({ status: true, message: response });
            } catch (error) {
                return res.status(404).json({ false: false, message: error });
            }
    
        }
    
    
    
    });
    
    // fastify.post('/get', { preHandler: fastify.passport.authenticate('jwt', { session: false }) }, async (req, res) => {
    //     // console.log(146, req.body);
    //     const { query, errors, isValid } = validateGet(req.body);
    //     if (!isValid)
    //         return res.status(400).json({ error: 'Validation error', message: errors });
    
    //     const token = extractToken(req);
    //     try {
    //         // var decode = await token_verify_decode(token);
    //         var users = await get_user(query);
    //         return res.status(200).send(users);
    //     } catch (e) {
    //         return res.status(404).json({ error: 'User get error', message: e });
    //     }
    // });
    
    fastify.post('/forgot_password', (req, res) => {
        // console.log(131, req.body);
        if (!Validator.isEmail(req.body.email)) {
            return res.status(400).json({ error: 'Validation error', message: "Email is invalid." });
        } else {
            try {
                User.findOne({ email: req.body.email }, function (err, user) {
                    if (err) return res.status(400).json({ error: 'Error', message: err });
    
                    if (_.isEmpty(user))
                        return res.status(400).json({ error: 'Error', message: "Email not found." });
    
                    var code = Math.floor(100000 + Math.random() * 900000);
                    User.updateMany({ email: req.body.email }, { email_otp: code.toString() }, function (error) {
                        if (error) throw error;
    
                        const d = new Date();
                        let cur = d.toLocaleTimeString();
                        let cur_arr = cur.split(":");
                        let exp = Number(cur_arr[0]) + 2;
                        if (exp >= 12) {
                            let ampm = cur_arr[2].split(" ");
                            if (ampm[1] == "AM") {
                                cur_arr[2] = ampm[0] + " PM";
                                exp = exp - 12;
                                exp = exp == 0 ? 12 : exp;
                            } else {
                                cur_arr[2] = ampm[0] + " AM";
                                exp = exp - 12;
                            }
                        }
                        if (req.body.is_invited === true) {
                            var emaildata = {
                                to: user.email,
                                // bcc: 'mahfuzak08@gmail.com',
                                subject: 'Workfreeli email verification',
                                text: 'Workfreeli password reset code ' + code,
                                html: 'Hi ' + user.firstname + ' ' + user.lastname + ',<br><br>Verify your email by using this code .<br><br><h2>' + code + '</h2><br><br>* Please note this code will expire in 2 hours (' + cur + ' to ' + exp + ':' + cur_arr[1] + ':' + cur_arr[2] + ').<br><br>Thank you,<br>WORKFREELI Team<br>workfreeli.com<br>support@workfreeli.com'
                            }
                        } else {
                            var emaildata = {
                                to: user.email,
                                // bcc: 'mahfuzak08@gmail.com',
                                subject: 'Workfreeli Authentication Code for Password Reset',
                                text: 'Workfreeli password reset code ' + code,
                                html: 'Hi ' + user.firstname + ' ' + user.lastname + ',<br><br>A password reset request has been received from your account. If you have requested this, please use this ONE TIME PASSWORD (OTP) code to reset your password. Otherwise, you can safely ignore this e-mail.<br><br><b>OTP Code: ' + code + '</b><br><br>Please note that the above code will expire in 5 minutes.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                            }
                        }
    
                        // console.log(emaildata);
                        try {
                            sg_email.send(emaildata, (result) => {
                                if (result.msg == 'success') {
                                    res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
                                } else {
                                    // console.log('forgot password ', result);
                                    res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
                                }
                            });
                        } catch (e) {
                            console.log(e);
                            res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
                        }
                    });
                });
            } catch (e) {
                return res.status(400).json({ error: 'Unexpected error', message: e });
            }
    
        }
    });
    
    fastify.post('/email_otp_verify', (req, res) => {
        // console.log(131, req.body);
        if (!req.body.code || req.body.code == "")
            return res.status(400).json({ error: 'Validation error', message: "Code is required." });
        if (!Validator.isEmail(req.body.email)) {
            return res.status(400).json({ error: 'Validation error', message: "Email is invalid." });
        } else {
            try {
                User.findOne({ email: req.body.email, email_otp: req.body.code }, function (err, user) {
                    if (err) return res.status(400).json({ error: 'Error', message: err });
    
                    if (_.isEmpty(user))
                        return res.status(400).json({ error: 'Error', message: "Email verification code not match, please check again." });
    
                    res.status(200).json({ status: true, message: 'Email Verification Successfully' });
                });
            } catch (e) {
                return res.status(400).json({ error: 'Unexpected error', message: e });
            }
    
        }
    });
    
    fastify.post('/set_new_password', async function (req, res) {
        // console.log(626, req.body);
        const { errors, isValid } = validateNewPassInput(req.body);
        if (!isValid) {
            return res.status(400).json({ error: 'Validation error', message: errors });
        }
        try {
            var newpass = await passwordToHass(req.body.password);
            var admin_name = req.body.admin_name ? req.body.admin_name : 'Account Administrator';
            User.find({ email: req.body.email }, async function (err, user) {
                if (err) throw err;
                if (user.length > 1) {
                    // if (req.body.old_password !== undefined && req.body.old_password != "") {
                    //     var match_count = 0;
                    //     for (let i = 0; i < user.length; i++) {
                    //         let match = await compare_pass(user[i].password, req.body.old_password);
                    //         if (match === true) match_count++;
    
                    //         if (match_count >= 1) i = user.length;
    
                    //         if (user.length == i + 1 && match_count == 0) {
                    //             return res.status(400).json({ error: 'Old Password Error', message: 'Old Password Not Match.' });
                    //         }
                    //     }
                    // }
    
                    user.forEach(function (v, k) {
                        User.updateOne({ id: v.id }, { password: newpass }, function (e) {
                            if (e) return res.status(400).json({ error: 'Error multi ', message: e });
    
                            // if(req.body.old_password !== undefined && req.body.old_password != ""){
                            xmpp_send_server(v.id.toString(), 'logout_from_all', { device_id: [] });
                            // }
    
                            if (user.length == k + 1) {
                                if (req.body.old_password === undefined) {
                                    console.log('Multi com email reset')
                                    req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                                    req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
                                    var emaildata = {
                                        to: v.email,
                                        // bcc: 'mahfuzak08@gmail.com',
                                        subject: 'Your Workfreeli Password has been Reset by your Account Administrator',
                                        text: 'Workfreeli password reset',
                                        html: 'Hi ' + v.firstname + ' ' + v.lastname + ',<br><br>' + admin_name + ' has reset your password. Please use the following one-time password (OTP) to login to your account. Once you login, please set a strong new password for your account.<br><br><b>One time password (OTP): ' + req.body.password + '</b><br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                                    }
    
                                    // console.log(emaildata);
                                    try {
                                        sg_email.send(emaildata, (result) => {
                                            if (result.msg == 'success') {
                                                // res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
                                            } else {
                                                // console.log('forgot password ', result);
                                                // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
                                            }
                                        });
                                    } catch (e) {
                                        console.log(e);
                                        // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
                                    }
                                }
                                return res.status(200).json({ status: true, message: 'Password Reset Successfull' });
                            }
                        });
                    });
                } else if (user.length == 1) {
                    // console.log(user.length)
                    // if (req.body.old_password !== undefined && req.body.old_password != "") {
                    //     let match = await compare_pass(user[0].password, req.body.old_password);
                    //     if (match !== true) {
                    //         console.log(660, "Old pass error");
                    //         return res.status(400).json({ error: 'Old Password Error', message: 'Old Password Not Match.' });
                    //     }
                    // }
                    User.updateOne({ id: user[0].id }, { password: newpass }, function (e) {
                        if (e) return res.status(400).json({ error: 'Error 1 ', message: e });
                        else {
                            xmpp_send_server(user[0].id.toString(), 'logout_from_all', { device_id: [] });
                            if (req.body.old_password === undefined) {
                                // console.log('Single com email reset')
                                req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                                req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
                                var emaildata = {
                                    to: user[0].email,
                                    // bcc: 'mahfuzak08@gmail.com',
                                    subject: 'Your Workfreeli Password has been Reset by your Account Administrator',
                                    text: 'Workfreeli password reset',
                                    html: 'Hi ' + user[0].firstname + ' ' + user[0].lastname + ',<br><br>' + admin_name + ' has reset your password. Please use the following one-time password (OTP) to login to your account. Once you login, please set a strong new password for your account.<br><br><b>One time password (OTP): ' + req.body.password + '</b><br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                                }
    
                                // console.log(emaildata);
                                try {
                                    sg_email.send(emaildata, (result) => {
                                        if (result.msg == 'success') {
                                            // res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
                                        } else {
                                            // console.log('forgot password ', result);
                                            // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
                                        }
                                    });
                                } catch (e) {
                                    console.log(e);
                                    // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
                                }
                            }
                            return res.status(200).json({ status: true, message: 'Password Reset Successfull' });
                        }
                    });
                } else
                    res.status(400).json({ error: 'Error', message: "Email not match, please check again." });
            });
    
        } catch (e) {
            return res.status(400).json({ error: 'Unexpected error', message: e });
        }
    });
    
    // fastify.post('/update_my_profile', { preHandler: fastify.passport.authenticate('jwt', { session: false }) }, async (req, res) => {
    //     // logger.info(req.body);
    //     const { errors, isValid } = validateUpdateUser(req.body);
    //     if (!isValid) {
    //         return res.status(400).json({ error: 'Validation error', message: errors });
    //     }
    //     const token = extractToken(req);
    //     let decode = await token_verify_decode(token);
    //     var query = { id: decode.id };
    //     console.log(810, query);
    //     if (req.body.img.indexOf("/") > -1) {
    //         var img_path = (req.body.img).split("/");
    //         req.body.img = img_path[img_path.length - 1];
    //     }
    //     var newdata = { firstname: req.body.firstname, img: req.body.img };
    //     if (req.body.lastname) newdata.lastname = req.body.lastname.toString();
    //     if (req.body.dept) newdata.dept = req.body.dept.toString();
    //     if (req.body.designation) newdata.designation = req.body.designation.toString();
    //     if (req.body.is_active !== undefined) newdata.is_active = req.body.is_active;
    //     if (req.body.phone) newdata.phone = [req.body.phone.toString()];
    //     try {
    //         User.updateOne(query, newdata).then(async function () {
    //             // User.findOne(query, function(e, user) {
    //             let user = await User.findOne(query).lean();
    //             // if (e) return res.status(400).json({ status: false, error: "update_my_profile 2", message: e });
    //             update_user_obj(user);
    //             save_activity({
    //                 company_id: user.company_id,
    //                 user_id: user.id,
    //                 title: user.firstname + " update his profile.",
    //                 type: "warning",
    //                 body: user.firstname + " update his profile from " + res.socket.remoteAddress,
    //                 user_name: user.firstname,
    //                 user_img: user.img,
    //             });
    //             user.use_name = 'new';
    //             if (req.body.phone) {
    //                 user.use_phone = 'new';
    //             }
    //             updateConversationName(user);
    //             xmpp_send_server(decode.id.toString(), 'update_my_profile', user, req);
    //             return res.status(200).json({ status: true, message: 'User update successfully.', user });
    //             // });
    //         }).catch((error)=>{
    //             return res.status(400).json({ status: false, error: "update_my_profile 1", message: error });
    //         });
    //     } catch (e) {
    //         return res.status(400).json({ status: false, error: "update_my_profile error", message: e });
    //     }
    
    // });
    
    // fastify.post('/update_user', { preHandler: fastify.passport.authenticate('jwt', { session: false }) }, (req, res) => {
    //     console.log('855', req.body);
    //     if (!req.body.user_id) {
    //         return res.status(400).json({ error: 'Validation error', message: "User id is invalid" });
    //     } else {
    //         var query = { id: req.body.user_id };
    //     }
    //     const token = extractToken(req);
    //     var decode = jwt.verify(token, process.env.SECRET);
    //     var newdata = {};
    //     if (req.body.firstname) newdata.firstname = req.body.firstname;
    //     if (req.body.lastname) newdata.lastname = req.body.lastname.toString();
    //     if (req.body.img) {
    //         if (req.body.img.indexOf("/") > -1) {
    //             var img_path = (req.body.img).split("/");
    //             req.body.img = img_path[img_path.length - 1];
    //         }
    //         newdata.img = req.body.img;
    //     }
    //     if (req.body.role) newdata.role = req.body.role.toString();
    //     if (req.body.dept) newdata.dept = req.body.dept.toString();
    //     if (req.body.designation) newdata.designation = req.body.designation.toString();
    //     if (req.body.is_active) newdata.is_active = parseInt(req.body.is_active);
    //     if (req.body.phone) newdata.phone = [req.body.phone.toString()];
    //     newdata.updated_by = decode.id;
    //     newdata.updated_at = new Date().getTime().toString();
    //     try {
    //         // console.log(query)
    //         User.updateOne(query, newdata, function (error) {
    //             if (error) return res.status(400).json({ status: false, error: "update_user 1", message: error });
    //             User.findOne(query, function (e, user) {
    //                 if (e) return res.status(400).json({ status: false, error: "update_user 2", message: e });
    //                 update_user_obj(user);
    //                 if (user.is_active === parseInt(req.body.is_active) && req.body.is_active === "1") {
    //                     self_conversation_create_if_not_found(user);
    //                 }
    //                 if (req.body.is_active && req.body.is_active != user.is_active) {
    //                     var a_title = user.is_active == 1 ? decode.firstname + ' enabled ' + user.firstname : decode.firstname + ' disabled ' + user.firstname;
    //                 } else {
    //                     var a_title = decode.firstname + ' update ' + user.firstname + ' profile data.';
    //                 }
    //                 console.log('save activity')
    //                 save_activity({
    //                     company_id: user.company_id,
    //                     user_id: user.id,
    //                     title: a_title,
    //                     type: "update_user",
    //                     body: a_title + " from " + res.socket.remoteAddress,
    //                     user_name: user.firstname,
    //                     user_img: user.img,
    //                 });
    
    //                 if (parseInt(req.body.is_active) === 0) {
    //                     var emaildata = {
    //                         to: user.email,
    //                         // bcc: 'mahfuzak08@gmail.com',
    //                         subject: 'Your Workfreeli Account has been Disabled by your Account Administrator',
    //                         text: 'Your Workfreeli Account has been Disabled by your Account Administrator',
    //                         html: 'Hi ' + user.firstname + ',<br><br>' + decode.firstname + ' has disabled your account ' + user.email + ' for ' + all_company[decode.company_id].company_name + ' workspace. <br><br>If you ever want to access your account, please contact ' + decode.firstname + ' or any other administrator to re-enable your account.<br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
    //                     }
    
    //                     // console.log(emaildata);
    //                     try {
    //                         sg_email.send(emaildata, (result) => {
    //                             if (result.msg == 'success') {
    //                                 // res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
    //                             } else {
    //                                 // console.log('forgot password ', result);
    //                                 // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
    //                             }
    //                         });
    //                     } catch (e) {
    //                         console.log(e);
    //                         // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
    //                     }
    //                 }
    
    //                 xmpp_send_server(user.id.toString(), 'update_user', user, req);
    //                 if (req.body.role || req.body.is_active) {
    //                     xmpp_send_server(user.id.toString(), 'logout_from_all', { user: { ...user, device_id: [] } }, req);
    //                 }
    //                 updateConversationName(user);
    //                 return res.status(200).json({ status: true, message: 'User update successfully.', user });
    //             });
    //         });
    //     } catch (e) {
    //         console.log(e)
    //         return res.status(400).json({ status: false, error: "update_user error", message: e });
    //     }
    
    // });
    
    /**
     * user_id
     * name
     * company_id
     * company_name
     * teammate_data
     * role
     */
    fastify.post('/teammate_invite', async (req, res) => {
        // console.log(1063, req.body);
        req.body['company_name'] = all_company[req.body.company_id.toString()].company_name;
        req.body.teammate_data = Array.isArray(req.body.teammate_data) ? req.body.teammate_data : [];
        if (req.body.firstname && req.body.firstname != '' && Array.isArray(req.body.teammate_data) && req.body.teammate_data.length == 0) {
            req.body.teammate_data.push({
                email: req.body.emails,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                role: req.body.role,
                password_req:req.body.password_req,
                password: req.body.password ? req.body.password : 'a123456'
            });
        }
        // console.log(499,req.body)
        const { errors, isValid } = validateTeammateInput(req.body);
        if (!isValid) {
            console.log(errors);
            return res.status(400).json({ error: 'Validation error', message: errors });
        }
        if (!all_users.hasOwnProperty(req.body.company_id)) {
            console.log('company not found in cash');
            return res.status(400).json({ error: 'Error', message: 'company not found in cash' });
        }
        // console.log(509,req.body.teammate_data.length)
        var company_limit = await check_company_limit(req.body.company_id, req.body.teammate_data.length);
    
        if (!company_limit) {
            console.log('This company users added limit exceeded.');
            return res.status(400).json({ error: 'Error', message: 'This company users added limit exceeded.' });
        }
        try {
            // var result = await create_teammate(req);
            var result = await add_user(req.body.teammate_data, req);
            self_conversation_create_if_not_found(result[0]);
            return res.status(200).json(result);
        } catch (e) {
            console.log(1214, e)
            return res.status(400).json({ error: 'Error', message: e });
        }
    });
    
    /**
     * teammate invite resend
     * id = teammate id
     */
    fastify.get('/teammate_invite_resend/:id?', async (req, res) => {
        req.params = req.query.id ? req.query : req.params;
        console.log(962, req.params);
        if (req.params.id && !isUuid(req.params.id)) {
            return res.status(400).json({ error: 'Validation error', message: 'Teammate id invalid' });
        }
        var query = {};
        if (isUuid(req.params.id)) {
            query = { id: req.params.id };
        }
        // if(req.query.email && req.query.conversation_id && isUuid(req.query.conversation_id)){
        //     query = {invite_email: req.query.email, invite_room: {$contains: req.query.conversation_id}};
        // }
        User.find(query, function (e, teammate) {
            if (e) return res.status(404).json({ error: 'teammate_signup Find error', message: e });
            if (teammate.length == 1) {
                req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
                let addeduser = all_users[teammate[0].company_id][teammate[0].created_by];
                let html = 'Hi ' + teammate[0].firstname + '!<br><br>' + addeduser.firstname + ' has invited you to join a new business account on Workfreeli - <b>' + all_company[teammate[0].company_id].company_name + '</b>.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + teammate[0].email + '<br><b>One-Time Password:</b> a' + teammate[0].email_otp + '<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                var emaildata = {
                    to: teammate[0].email,
                    subject: addeduser.firstname + ' resend the invitation you to join Workfreeli.',
                    text: addeduser.firstname + ' resend the invitation you to join Workfreeli.',
                    html: html
                }
                try {
                    sg_email.send(emaildata, (result) => {
                        if (result.msg == 'success') {
                            console.log(989, 'Workfreeli teammate invitation resend');
                            return res.status(200).json({ status: true, teammates: teammate[0], message: 'Workfreeli teammate invitation resend' });
                        } else {
                            console.log(992, { status: false, message: result });
                            return res.status(400).json({ status: false, message: result });
                        }
                    });
                } catch (e) {
                    console.log(997, { status: false, message: result });
                    return res.status(400).json({ status: false, message: result });
                }
            } else
                return res.status(404).json({ error: 'teammate_invite_resend error', message: 'No valid teammate found!!!' });
        });
    });
    
    /**
     * Delete a user
     * id = user id
     */
    fastify.delete('/userdelete/:id?', async (req, res) => {
        req.params = req.query.id ? req.query : req.params;
        const token = extractToken(req);
        var decode = await token_verify_decode(token);
        // console.log(1207, req.params);
        if (!isUuid(req.params.id)) {
            return res.status(400).json({ error: 'Validation error', message: 'User id invalid' });
        }
        var query = { id: req.params.id };
        User.find(query, function (e, teammate) {
            if (e) return res.status(404).json({ error: 'user Find error', message: e });
            if (teammate.length == 1 && teammate[0].login_total == 0) {
                // query.created_by_id = teammate[0].created_by_id;
                // query.company_id = teammate[0].company_id;
    
                var queries = [];
                // if (Array.isArray(teammate[0].invite_room)) {
                //     for (let i = 0; i < teammate[0].invite_room.length; i++) {
                //         queries.push(models.instance.Conversation.update({ conversation_id: models.uuidFromString(teammate[0].invite_room[i]), company_id: models.timeuuidFromString(teammate[0].company_id) }, { participants_guest: { $remove: [teammate[0].invite_email] } }, { return_query: true }));
                //     }
                // }
    
                // queries.push(models.instance.Users.delete(query, { return_query: true }));
    
                User.deleteOne(query, function (err) {
                    if (err) return res.status(404).json({ error: 'user delete error', message: err });
    
                    var rolebasetext = decode.firstname + ' has just cancelled the invitation for you to join <b>' + decode.company_name + '</b> on Workfreel.';
                    var invitation_msg = '';
                    var toname = teammate[0].firstname ? teammate[0].firstname : "there";
                    req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                    req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
                    var emaildata = {
                        to: teammate[0].email,
                        subject: decode.firstname + ' has cancelled the invitation for you to join Workfreeli',
                        text: decode.firstname + ' has cancelled the invitation for you to join Workfreeli',
                        html: 'Hi ' + toname + '!<br><br>' + rolebasetext + '<br><br>However, you can always sign-up for a new business account for you and your team. Workfreeli is a team collaboration platform that simplifies the way we work. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign up for a new business account</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                    }
                    // console.log(emaildata);
                    try {
                        sg_email.send(emaildata, (result) => {
                            if (result.msg == 'success') {
                                // console.log(1231, { status: true, message: 'Workfreeli teammate invitation cancel' });
                                return res.status(200).json({ status: true, teammates: teammate[0], message: 'Workfreeli teammate invitation cancel' });
                            } else {
                                console.log(1234, { status: false, message: result });
                                return res.status(400).json({ status: false, message: result });
                            }
                        });
                    } catch (e) {
                        console.log(1239, { status: false, message: result });
                        return res.status(400).json({ status: false, message: result });
                    }
                });
            } else
                return res.status(404).json({ error: 'user delete error', message: 'No valid user found for delete!!!' });
        });
    });
    
    fastify.post('/switch_account', async (req, res) => {
        // console.log(410, req.body);
        let hitat = (new Date()).getTime();
        // console.log("switch_account hit: ", hitat);
        const { errors, isValid } = validateSwitchAccount(req.body);
        if (!isValid) {
            return res.status(400).json({ error: 'Validation error', message: errors });
        }
        const email = req.body.email;
        // var query = { email };
        // query.company_id = req.body.company_id;
        var multi_company = false;
        var query = { email };
        if (req.body.code && req.body.code !== "") {
            query.email_otp = req.body.code;
        }
        if (req.body.company_id !== undefined) {
            query.company_id = req.body.company_id;
            multi_company = true;
        }
    
        try {
            let user = await User.find(query).lean();
    
            if (_.isEmpty(user)) {
                return res.status(404).json({ error: 'Find error', message: 'Email not found' });
            } else if (!all_company.hasOwnProperty(user[0].company_id.toString())) {
                return res.status(404).json({ error: 'Find error', message: 'Company is deactive' });
            } else {
                // console.log(428, user[0].company_id.toString());
                // console.log(429, all_company.hasOwnProperty(user[0].company_id.toString()));
                // console.log(430, all_company[user[0].company_id.toString()].company_name);
                var user_id = user[0].id.toString();
                // if (req.body.xmpp_token && req.body.device_type == 'web') {
                var xmpp_user = (user_id + '$$$' + req.body.device_id);
                // } else {
                // var xmpp_user = (user_id + '$$$' + req.body.device_id);
                // }
                // console.log('switch:xmpp_user:start',xmpp_user);
                var status = await create_account_xmpp(xmpp_user);
                if (status) {
                    xmppUserAdd(xmpp_user, req, true);
                } else {
                    xmpp_user = '';
                }
    
                if (req.body.gcm_id) register_firebase_token(user_id, req.body.device_type, req.body.gcm_id);
    
                const payload = {
                    id: user[0].id,
                    firstname: user[0].firstname,
                    lastname: user[0].lastname,
                    email: user[0].email,
                    company_id: user[0].company_id,
                    company_name: all_company[user[0].company_id.toString()].company_name,
                    multi_company: multi_company,
                    device_id: req.body.device_id,
                    device: user[0].device,
                    xmpp_user: xmpp_user
                };
    
                // console.log('switch:payload',payload);
    
                jwt.sign(
                    payload,
                    process.env.SECRET, {
                    expiresIn: 31556926 // 1 year in seconds
                },
                    async (err, token) => {
                        save_activity({
                            company_id: user[0].company_id,
                            user_id: user[0].id,
                            title: user[0].firstname + " login.",
                            type: "success",
                            body: user[0].firstname + " login from " + res.socket.remoteAddress,
                            user_name: user[0].firstname,
                            user_img: user[0].img,
                        });
                        // console.log("switch_account success: ", (new Date()).getTime() - hitat);
                        let update_data = {
                            login_total: (Number(user[0].login_total) + 1),
                        };
                        user[0].login_total = (Number(user[0].login_total) + 1);
                        // update_user_obj(user[0]);
                        var device = user[0].device != null ? user[0].device : [];
                        if (device.indexOf(req.body.device_id) == -1) {
                            device.push(req.body.device_id);
                            update_data.device = device;
                        }
                        // console.log(477, update_data);
    
                        await User.updateOne({ id: user[0].id }, update_data);
    
                        let switch_data = { token: "Bearer " + token, user: { id: user[0].id, email: user[0].email, phone: user[0].phone.toString(), firstname: user[0].firstname, lastname: user[0].lastname, is_active: user[0].is_active, img: process.env.FILE_SERVER + 'profile-pic/Photos/' + user[0].img, role: user[0].role, multi_company, mute_all: user[0].mute_all, mute: user[0].mute ? JSON.parse(user[0].mute) : '', company_id: user[0].company_id, fnln: fnln(user[0]), company_name: all_company[user[0].company_id].company_name } };
    
                        return res.status(200).json(switch_data);
    
                        // res.redirect('/home/homepage_new/' + token + '/0/' + user[0].id.toString() + '/no');
                    }
                );
            }
    
        } catch (error) {
            return res.status(404).json({ error: 'Find error', message: error });
        }
    
    
    });
    
    /**
     * This route is use for supper admin.
     * To remove all user device, gcm_id and fcm_id
     * 
     * sa_email = sa email
     * password = sa password
     * email = which user device need to be remove
     * company_id = optional, if given, then only that company user update else all.
     */
    fastify.post('/remove_all_device_id', async(req, res)=>{
        if(req.body.sa_email === 'sa@freeli.io'){
            var isMatch = await compare_pass("$2a$10$6TvtrYHJ1QpAgqKqSFlj8O1jMWAYpxFtYXXG1Dqbfunstse8wsSp6", req.body.password);
            if(req.body.email && isMatch){
                let result;
                if(req.body.company_id)
                    result = await User.findOneAndUpdate({email: req.body.email, company_id: req.body.company_id}, {fcm_id: [], gcm_id: "", device: []});
                else 
                    result = await User.updateMany({email: req.body.email}, {fcm_id: [], gcm_id: "", device: []});
                return res.status(200).json(result);
            }
            else return res.status(400).json({message: "Required data missing!!!"});
        }
        else return res.status(401).json({message: "Unauthorized to access!!!"});
    });
    
    /**
     * This route is use for supper admin.
     * To remove all zookeeper node and recreate all
     * 
     * sa_email = sa email
     * password = sa password
     */
    fastify.post('/remove_all_znode', async(req, res)=>{
        if(req.body.sa_email === 'sa@freeli.io'){
            var isMatch = await compare_pass("$2a$10$6TvtrYHJ1QpAgqKqSFlj8O1jMWAYpxFtYXXG1Dqbfunstse8wsSp6", req.body.password);
            if(isMatch){
                try{
                    await zNodeRecursive('/workfeeli/'+process.env.API_SERVER_URL.replace(/\//g, ''));
                    zNode_init();
                    return res.status(200).json({result: 'successfully clear all zookeeper node.'});
                }catch(e){
                    return res.status(400).json(e);
                }
            }
            else return res.status(400).json({message: "Required data missing!!!"});
        }
        else return res.status(401).json({message: "Unauthorized to access!!!"});
    });
}

module.exports = routes