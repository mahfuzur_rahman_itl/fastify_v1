const express = require('express');
const router = express.Router();
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var {get_keywords, set_keywords} = require('../../v2_utils/task_manage');

router.get('/get_all', async(req, res)=>{
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    if(! req.query.company_id)
        req.query.company_id = decode.company_id;
    let k = await get_keywords(req.query);
    if(k.success)
        res.status(200).json(k);
    else
        res.status(400).json(k);
});

router.post('/save', async (req, res) => {
    try{
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        req.body.created_by = decode.id;
        req.body.company_id = decode.company_id;
        let k = await set_keywords(req.body);
        if(k.success)
            res.status(200).json(k);
        else 
            res.status(400).json(k);
    }catch(e){
        res.status(400).json({success: false, error: e});
    }
});

module.exports = router;