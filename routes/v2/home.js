const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const _ = require('lodash');
const isUuid = require('uuid-validate');
var moment = require('moment');
const short = require('short-uuid');
var CryptoJS = require("crypto-js");

const OpenAI = require("openai");
const openai = new OpenAI({ apiKey: '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' });

const Conversation = require('../../mongo-models/conversation');
const Users = require('../../mongo-models/user');
const File = require('../../mongo-models/file');
const Messages = require('../../mongo-models/message');
const Link = require('../../mongo-models/link');

var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { get_user, get_user_obj } = require('../../v2_utils/user');
var { get_all_tags } = require('../../v2_utils/tag');
var { get_team } = require('../../v2_utils/team');
var { get_bunit } = require('../../v2_utils/business_unit');
var { save_activity } = require('../../v2_utils/user_activity');
var { get_all_company } = require('../../v2_utils/company');
var { my_conv_obj } = require('../../v2_utils/myconv');
var {setZK, getZK, checkZNode, znMsgUpdate, zNodeDelete, znReadMsgUpdateCounter, delayFun,znConNodeUpdate} = require('../../v2_utils/zookeeper');

var path = '/workfeeli/' + process.env.API_SERVER_URL.replace(/\//g, '');

var {
    get_conversation,
    fnln,
    get_message,
    get_rep_message,
    get_tags2,
    get_filter_msg,
    clear_conversation,
    mute_conversation,
    set_msg_status_read,
    count_unread_reply,
    read_all_reply,
    count_unread_reply_for_other_acc
} = require('../../v2_utils/message');
// var { models } = require('../../config/db/express-cassandra');
var { valdateOpenThread } = require('../../validation/home');
var {
    xmpp_send_server,
    xmpp_send_broadcast,
    send_msg_firebase,
    xmppUserRemove,
    xmppUserAdd
} = require('../../v2_utils/voip_util');
const { mode } = require('crypto-js');
const { vary } = require('express/lib/response');

function parseJSONSafely(str) {
    // console.log(45, str)
    if(str === null || str === 'null') return [];
    try {
       return JSON.parse(str);
    }
    catch (e) {
       console.log("Error in JSON parse");
       if(typeof str == 'string') return [str];
       else return [];
    }
}
async function routes (fastify, options) {

    /**
     * remove_device = default "yes"
     */
    fastify.post('/logout', async(req, res) => {
        const token = extractToken(req);
        // console.log("Logout hit ", req.body);
        try {
            var decode = await token_verify_decode(token);
            // console.log(51, decode);
            if(req.body.remove_device === 'no')
                var update_data = {};
            else
                var update_data = { $pullAll: {device: [decode.device_id, req.body.token] } };
    
            if (req.body.user_id && req.body.token) {
                // console.log(77);
                var user_id = req.body.user_id;
                var xmpp_token = req.body.token;
                var xmpp_user = (user_id + '$$$' + xmpp_token);
    
                xmppUserRemove(user_id, xmpp_user, req);
    
                if (req.body.firebase_token) {
                    var firebase_token = req.body.firebase_token;
                } else {
                    var firebase_token = req.body.token;
                }
                if (req.body.device) var device = req.body.device;
                else var device = 'android';
    
                update_data.fcm_id = { $pull: device + '@@@' + firebase_token };
            }
            try{
                var user = await Users.findOne({ id: decode.id }).lean();
                oauth2Client.setCredentials(user.googleTokens);
                const google_token = oauth2Client.credentials.access_token;
                if (google_token) {
                    await oauth2Client.revokeToken(google_token);
                    oauth2Client.setCredentials(null);
                }
    
            }catch(error){
                Users.updateOne({ id: decode.id }, { googleTokens: null, google_id: null });
            }
            
            
            Users.updateOne({ id: decode.id }, update_data).then(function(){
                return res.status(200).json({ status: true, message: "logout successfully." });
            }).catch((e)=>{
                console.log("logout error ", e);
                return res.status(400).json({ e: 'logout error', message: e });
            })
               
        } catch (e) {
            console.log(103, e);
            return res.status(400).json({ error: 'Token error', message: e });
        }
    });
    
    fastify.post('/remove_other_device', async(req, res) => {
        const token = extractToken(req);
        try {
            let decode = await token_verify_decode(token);
            var firebase_id = null;
            // if(req.body.device_type && req.body.firebase_token){
            //     firebase_id = req.body.device_type + '@@@' + req.body.firebase_token;
            // } 
            
            // xmpp_send_server(decode.id, 'logout_from_all', { device_id: decode.device_id, still_login: req.body.trust === 'yes' ? true : false });
            // await send_msg_firebase(decode.id, 'logout_from_all', { firebase_id: firebase_id, still_login: req.body.trust === 'yes' ? true : false }, req);
            // // { device: [decode.device_id], fcm_id: [decode.xmpp_user] }
            // Users.find({email: decode.email}, async function(e, u){
            //     if(e) console.log(112, e);
            //     if(u.length>0){
            //         var did = null;
            //         var fcm_id = null;
                    
            //         if(req.body.trust==="yes"){
            //             did = [decode.device_id];
            //             if(req.body.device_type && req.body.firebase_token) fcm_id = [firebase_id];
            //             xmppUserRemove(decode.id, 'except', req);
            //         }else{
            //             xmppUserRemove(decode.id, 'all', req);
            //         }
            //         await Users.updateMany({ email: decode.email }, { device: did, fcm_id: fcm_id });
    
            //         console.log(124, "logout from all");
            //         return res.status(200).json({ status: true, message: "All other devices removed from your info." });
            //     }
            // });
        } catch (e) {
            return res.status(400).json({ error: 'Token error', message: e });
        }
    });
    
    /**
     * get conversation history and load old message
     * token	        = token
     * type             = conversation / msgs
     * conversation_id  = conversation_id
     * page             = page no [default 1]
     */
    fastify.get('/room', async (req, res) => {
        const token = extractToken(req);
        console.log("Room API Start");
        req.params = req.query.type ? req.query : req.params;
        // req.params = Object.keys(req.params).length !== 0 ? req.params : req.query;
        try{
            if (token) {
                var decode = await token_verify_decode(token);
                var keyspace = all_company[decode.company_id].keyspace;
                console.log("keyspace", keyspace);
                var pin_convs = [];
                var unpin_convs = [];
                var active_conv = {};
                active_conv['details'] = {};
                active_conv['msgs'] = [];
                if (req.params.type === 'msgs' && isUuid(req.params.conversation_id)) {
                    if(Number(req.params.page) > 1){
                        active_conv = await get_message({ 
                            type: req.params.type,
                            convid: req.params.conversation_id, 
                            page: req.params.page, 
                            limit: 20, 
                            is_reply: 'no', 
                            user_id: decode.id, 
                            company_id: decode.company_id
                        });
                    }else{
                        let con = `${path}/active_conv/${decode.id}`;
                        let hasNode = await checkZNode(con);
                        if(! hasNode) await setZK(con, null);
                        con = `${path}/active_conv/${decode.id}/${req.params.conversation_id}`;
                        hasNode = await checkZNode(con);
                        if(! hasNode) {
                            active_conv = await get_message({ 
                                type: req.params.type,
                                convid: req.params.conversation_id, 
                                page: req.params.page, 
                                limit: 20, 
                                is_reply: 'no', 
                                user_id: decode.id, 
                                company_id: decode.company_id
                            });
                            await setZK(con, active_conv);
                        }
                        else{
                            active_conv = await getZK(con);
                            console.log('active conv from znode');
                        }
                    }
                    
                    if(active_conv['msgs'].length>0){
                        set_msg_status_read({ msgs: active_conv['msgs'], user_id: decode.id.toString() }, req);
                    }
                    /* if(!active_conv['details'].hasOwnProperty('conversation_id')){
                        return res.status(400).send({
                            success: false,
                            message: 'this conversation is not listed',
                            //body: e
                        });
    
                    } */
                    
                    if(Object.keys(active_conv['details']).length === 0){
                        console.log("get conversation list not");
                        return res.status(400).send({
                            success: false,
                            message: 'this conversation is not listed',
                            //body: e
                        });
                        
                    }
                    return res.status(200).send({
                        success: true,
                        message: 'success',
                        active_conv,
                        restart_time
                    });
                } else if(req.params.type === 'conversation') {
                    console.log("Reload page and get conversation list only", decode.id);
                    let con = `${path}/connect/${decode.id}`;
                    let hasConnect = await checkZNode(con);
                    if(! hasConnect){
                        var convs = await get_conversation({user_id: decode.id, select: ['last_msg', 'title', 'conv_img', 'group', 'team_id', 'team_id_name', 'b_unit_id', 'b_unit_id_name', 'participants', 'participants_details','participants_admin', 'last_msg_time', 'archive', 'pin', 'has_mute', 'mute', 'close_for','tag_list','created_by']});
                        // var convs = await get_conversation({user_id: decode.id});
                        await setZK(con, convs);
                    }
                    else{
                        var convs = await getZK(con);
                        console.log('199 from zNode');
                    }
                    for(let key in convs){
                        if(convs[key].pin.indexOf(decode.id)>-1) pin_convs.push(convs[key]);
                        else unpin_convs.push(convs[key]);
                    }
                    return res.status(200).send({
                        success: true,
                        message: 'success',
                        token: req.params.token,
                        pin_convs, 
                        unpin_convs,
                        restart_time
                    });
                    
                }
                else{
                    return res.status(200).send({
                        success: true,
                        message: 'success',
                        token: req.params.token,
                        pin_convs: [], 
                        unpin_convs: [],
                        restart_time: ""
                    });
                    
                }
            }
            else throw new Error({"message": "Token missing"});
        }catch(e){
            return res.status(400).send({
                success: false,
                message: 'error',
                body: e
            });
        }
    });
    
    
    /**
     * This route is used for open a message reply history
     * conversation_id
     * msg_id = main msg id
     * page = need for more reply msg
     * 
     * select * from messages where reply_for_msgid = 2 and is_reply_msg = 'yes';
     */
    fastify.get('/open_thread', async(req, res) => {
        req.params = req.query.msg_id ? req.query : req.params;
        const { errors, isValid } = valdateOpenThread(req.params);
        if (!isValid) {
            return res.status(400).json({ error: 'Validation error', message: errors });
        }
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            
            let rnr = `${path}/reply_msgs/${req.params.msg_id}`; // reply node root = rnr
            let hasNode = await checkZNode(rnr);
            let result = {};
            if(! hasNode){ 
                result = await get_rep_message({msg_id: req.params.msg_id, user_id: decode.id});
                await setZK(rnr, null);
                await setZK(`${rnr}/conversation`, result.conversation);
                await setZK(`${rnr}/main_msg`, result.main_msg);
                await setZK(`${rnr}/reply_msgs`, result.reply_msgs.length > 0 ? result.reply_msgs : null);
            }
            else {
                result['conversation'] = await getZK(`${rnr}/conversation`);
                result['main_msg'] = await getZK(`${rnr}/main_msg`);
                result['reply_msgs'] = await getZK(`${rnr}/reply_msgs`);
                console.log('from reply znode')
            }
            if(result.reply_msgs.length > 0){
                set_msg_status_read({ msgs: result.reply_msgs, user_id: decode.id.toString() }, req);
            }
            return res.status(200).json({ status: true, result });
        } catch (e) {
            return res.status(400).json({ error: 'Error', message: e });
        }
    });
    
    /**
     * This route is used for 'pin/ unpin' a conversation
     * conversation_id
     * action = pin/ unpin
     */
    fastify.post('/pin_unpin', async(req, res) => {
        const token = extractToken(req);
        console.log('pin_unpin_success_check1',token)
        if (token) {
            var decode = await token_verify_decode(token);
            console.log('pin_unpin_success_check2',decode)
    
            if (req.body.conversation_id === undefined || !isUuid(req.body.conversation_id)) {
                return res.status(400).json({ error: 'Validation error', message: "Conversation id invalid" });
            }
            try {
                var con = `${path}/connect/${decode.id}`;
                var convs = await getZK(con);
                console.log('pin_unpin_success_check3')
                var details;
                if (req.body.user_id) xmpp_send_server(req.body.user_id, 'pin_unpin', req.body);
                if (req.body.action == 'pin') {
                    Conversation.updateOne({ conversation_id: req.body.conversation_id }, { '$push': { pin: decode.id } }, async function(err) {
                        if (err) return res.status(400).json({ error: err.name, message: err.message });
                        if(convs){
                            for(let key in convs){
                                if(convs[key].conversation_id === req.body.conversation_id){
                                    convs[key].pin = Array.isArray(convs[key].pin) ? convs[key].pin : [];
                                    convs[key].pin.push(decode.id);
                                    details = convs[key];
                                }
                            }
                            if(await zNodeDelete(con)){
                                await delayFun(100);
                                await setZK(con, convs);
                            }
        
                            con = `${path}/active_conv/${decode.id}/${req.body.conversation_id}`;
                            let hasNode = await checkZNode(con);
                            if(hasNode) {
                                let active_conv = await getZK(con);
                                active_conv['details'] = details;
                                if(await zNodeDelete(con)){
                                    await delayFun(100);
                                    await setZK(con, active_conv);
                                }
                            }
    
                        }
                        
                        console.log('pin_unpin_success_check2',req.body);
                        return res.status(200).json({ status: true, message: 'Conversation pin successfully.' });
                    });
                } else {
                    Conversation.updateOne({ conversation_id: req.body.conversation_id }, { '$pull': { pin : decode.id } }, async function(err) {
                        if (err) return res.status(400).json({ error: err.name, message: err.message });
                        if(convs){
                            for(let key in convs){
                                if(convs[key].conversation_id === req.body.conversation_id){
                                    convs[key].pin.splice(convs[key].pin.indexOf(decode.id), 1);
                                    details = convs[key];
                                }
                            }
                            if(await zNodeDelete(con)){
                                await delayFun(100);
                                await setZK(con, convs);
                            }
        
                            con = `${path}/active_conv/${decode.id}/${req.body.conversation_id}`;
                            let hasNode = await checkZNode(con);
                            if(hasNode) {
                                let active_conv = await getZK(con);
                                active_conv['details'] = details;
                                if(await zNodeDelete(con)){
                                    await delayFun(100);
                                    await setZK(con, active_conv);
                                }
                            }
                        }
                        
                        return res.status(200).json({ status: true, message: 'Conversation unpin successfully.' });
                    });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Pin unpin error ', message: e });
            }
        } else {
            return res.status(400).json({ error: 'pin unpin error ', message: "Token missing." });
        }
    });
    
    /**
     * Filter message
     * name = all_reply/ call/ flag/ checklist/ media/ unread/ private/ task/ link/ msg_title/ str
     * conversation_id
     * str = String [only required for name=str. msg_body search korar somoy aita lagbe]
     * type = old/ new/ one
     */
    fastify.get('/filter', async(req, res) => {
        req.params = req.query.name ? req.query : req.params;
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            var result = [];
            try {
                // console.log(356, req.params);
                if (req.params.name !== undefined && req.params.name != '') {
                    req.params.user_id = decode.id.toString();
                    req.params.company_id = decode.company_id.toString();
                    req.params.limit = 20;
                    result = await get_filter_msg(req.params);
                    // console.log(438, result.msgs.length);
                    if (result.msgs.length > 0) {
                        result.msgs.reverse();
                        if (req.params.name == 'unread')
                            set_msg_status_read({ msgs: result.msgs, user_id: decode.id.toString() }, req);
                    }
                    return res.status(200).json({ status: true, result: result, message: "success" });
                } else
                    return res.status(400).json({ error: 'Validation error', message: "Conversation id invalid" });
            } catch (e) {
                return res.status(404).json({ error: 'Unwanted error1', message: e });
            }
        }
    });
    
    /**
     * this route is used for clear conversation
     * conversation_id
     * delete_text = yes/no
     * delete_checklist = yes/no
     * delete_flag = yes/no
     * delete_media = yes/no
     */
    fastify.post('/clear_conversation', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                if (!isUuid(req.body.conversation_id)) throw { error: 'Validation error', message: "Conversation id invalid" };
                req.body.user_id = decode.id.toString();
                clear_conversation(req.body, function(result) {
                    if (result.status) return res.status(200).json({ status: true, message: "Clear conversation successfully." });
                    else throw result;
                });
            } catch (e) {
                return res.status(404).json({ error: 'clear_conversation error1', message: e });
            }
        }
    });
    
    /**
     * this route used for mute conversations
     * conversation_id = '682089b2-35ee-47a3-bc23-e22e5176d8ef'
     * mute_duration = '30M'
     * mute_start_time = 'Mon, Aug 30, 2021 5:07 PM'
     * mute_end_time = 'Mon, Aug 30, 2021 5:37 PM'
     * mute_day = null
     * mute_timezone = '+06:00'
     * type = add / update/ delete
     */
    fastify.post('/mute_conversation', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                if (!isUuid(req.body.conversation_id)) throw { error: 'Validation error', message: "Conversation id invalid" };
                req.body.user_id = decode.id.toString();
                req.body.company_id = decode.company_id.toString();
               // console.log(994,req);
    
                mute_conversation(req.body, function(result) {
                    if (result.status) {
                        result.message = req.body.type + " mute conversation successfully.";
                        result.type = req.body.type;
                        result.conversation_id = req.body.conversation_id;
                        // io.to(decode.id.toString()).emit("mute_conversation", result);
                        xmpp_send_server(decode.id.toString(), 'mute_conversation', result, req);
                        //console.log(475,result);
                        //console.log(476,req.body);
    
                        znConNodeUpdate({uid: decode.id.toString(), type: 'mute_conversation', conv: req.body.conversation_id, mute_data: req.body });
                        return res.status(200).json(result);
                    } else throw result;
                });
            } catch (e) {
                return res.status(404).json({ error: 'mute_conversation error1', message: e });
            }
        }
    });
    
    /**
     * this route used for mute all conversations 
     * user_id = '896c2fc4-deb9-4dcf-b64f-9b59c349ec54'
     */
    fastify.post('/mute_conversation_all', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            // var decode = await token_verify_decode(token);
            var data = req.body;
            // console.log("******************************", req.body);
            try {
                if (!isUuid(data.user_id)) throw { error: 'Validation error', message: "Conversation id invalid" };
    
                var query = { id: data.user_id };
                var mute_data = '';
                if (data.mute_all) {
                    var nowUnix = moment(data.mute_start_time + ' ' + data.mute_timezone, 'llll Z').unix();
                    var endUnix = moment(data.mute_end_time + ' ' + data.mute_timezone, 'llll Z').unix();
                    mute_data = JSON.stringify({
                        // conversation_id: data.conversation_id,
                        mute_by: data.user_id,
                        mute_duration: data.mute_duration,
                        mute_start_time: data.mute_start_time,
                        mute_end_time: data.mute_end_time,
                        mute_day: data.mute_day,
                        mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                        mute_unix_end: endUnix ? endUnix.toString() : "0",
                        mute_timezone: data.mute_timezone ? data.mute_timezone : '',
                    });
                    Users.updateOne(query, { mute_all: data.mute_all, mute: mute_data.toString()})
                    .then(result => {
                        data.status = true;
                        data.mute_data = JSON.parse(mute_data);
                        data.message = "Mute conversation successfully.";
                        xmpp_send_server(data.user_id, 'mute_conversation_all', data, req);
                        // send_msg_firebase(decode.id, 'logout_from_all', { device_id: decode.device_id }, req);
    
                        return res.status(200).json(data); })
                    .catch(error => {
                        console.log("mute conversation all:error ", error);
                        return res.status(404).json({ error: 'mute conversation all error', message: error });
                    });
                } else {
                    Users.updateOne(query, {mute_all: data.mute_all, mute: mute_data })
                    .then(result => {
                        data.status = true;
                        data.mute_data = null;
                        data.message = "Unmute conversation successfully.";
                        xmpp_send_server(data.user_id, 'mute_conversation_all', data, req);
                       // znConNodeUpdate({uid: v, type: 'room_archive', conv: c[0], archive: req.query.archive });
                        // send_msg_firebase(decode.id, 'logout_from_all', { device_id: decode.device_id }, req);
    
                        return res.status(200).json(data); })
                    .catch(error => {
                        console.log("mute conversation all:error ", error);
                        return res.status(404).json({ error: 'mute conversation all error', message: error });
                    });
                    // =====================
                }
                // ==========================================================
    
                all_users[data.company_id][data.user_id]['mute_all'] = data.mute_all;
                all_users[data.company_id][data.user_id]['mute'] = mute_data ? JSON.parse(mute_data) : '';
                if (process.send) process.send({ type: 'all_users', all_users: all_users });
            } catch (e) {
                return res.status(404).json({ error: 'mute_conversation error1', message: e });
            }
        }
    });
    /**
     * this route use for total unread reply view and
     * if use type conversation_and_total then,
     * show left side bar conversation wise total unread reply.
     * 
     * and if use conversation_id, then give only that conversation total unread reply
     * 
     * 
     * type = conversation_and_total/ total
     * conversation_id
     */
    fastify.post('/total_unread_reply', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                if (isUuid(req.body.conversation_id))
                    var data = { conversation_id: req.body.conversation_id, type: req.body.type, user_id: decode.id.toString() };
                else if (req.body.type && req.body.type != '')
                    var data = { type: req.body.type, user_id: decode.id.toString() };
                else
                    var data = { user_id: decode.id.toString() };
                try {
                    let con = `${path}/total_unread_reply/${decode.id}`;
                    let hasNode = await checkZNode(con);
                    if(! hasNode) {
                        var result = await count_unread_reply(data);
                        await setZK(con, result);
                    }else{
                        console.log('from znode');
                        var result = await getZK(con);
                    }
                    
                    var conversations_arr = [];
                    for(let i in result.conversations){
                        conversations_arr.push(result.conversations[i]);
                    }
                    result.conversations_arr = conversations_arr;
                    return res.status(200).json({
                        status: true,
                        result
                    });
                } catch (e) {
                    return res.status(404).json({ error: 'total_unread_reply error 1219', message: e });
                }
            } catch (e) {
                return res.status(404).json({ error: 'total_unread_reply error1', message: e });
            }
        }
    });
    
    /**
     * view one conversation all unread reply msgs
     * conversation id
     */
    fastify.get('/get_reply_msg', async(req, res) => {
        req.params = req.query.id ? req.query : req.params;
        // console.log(560, req.params);
        const token = extractToken(req);
        // console.log(561, token);
        if (token) {
            var decode = await token_verify_decode(token);
            // console.log(414, decode);
            try {
                req.params.user_id = decode.id.toString();
                req.params.company_id = decode.company_id.toString();
                req.params.conversation_id = req.params.id;
                req.params.name = 'all_reply';
                // req.params.page_size = 'unlimited';
                
                var con = `${path}/get_reply_msg/${decode.id}`;
                var hasNode = await checkZNode(con);
                if(! hasNode) await setZK(con, null);
    
                con = con + `/${req.params.conversation_id}`;
                hasNode = await checkZNode(con);
                if(Number(req.params.page) > 1 || ! hasNode) {
                    var [msgs, details] = await Promise.all([
                        get_filter_msg(req.params),
                        get_conversation({conversation_id:req.params.conversation_id, user_id: decode.id})
                    ]);
                    if(Number(req.params.page) < 2){
                        await setZK(con, null);
                        await setZK(con+'/msg', msgs);
                        await setZK(con+'/details', details);
                    }
                }else{
                    var msgs = await getZK(con+'/msg');
                    var details = await getZK(con+'/details');
                    console.log('from znode 573');
                }
    
                if(req.params.unread_reply === 'yes'){
                    let newmsgs = [];
                    // let data = { conversation_id: req.params.id, user_id: decode.id.toString() };
                    // var result = await count_unread_reply(data);
                    // if(result.total_unread_reply > 0){
                        for(let i=0; i<msgs['msgs'].length; i++){
                            if(msgs['msgs'][i].unread_reply > 0)
                                newmsgs.push(msgs['msgs'][i]);
                        }
                        msgs['msgs'] = newmsgs.length > 0 ? newmsgs : msgs['msgs'];
                    // }
                }
                return res.status(200).json({success: true, message: 'success', active_conv: { msgs: msgs.msgs, pagination: msgs.pagination, details: details[0] }});
            } catch (e) {
                return res.status(404).json({ error: 'get_reply_msg error1', message: e });
            }
        }
    });
    
    
    /**
     * this route use for read all reply
     * 
     * conversation_id
     */
    fastify.post('/read_all_reply', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                if (isUuid(req.body.conversation_id))
                    var data = { conversation_id: req.body.conversation_id, type: req.body.type, user_id: decode.id.toString() };
                else
                    var data = { user_id: decode.id.toString() };
    
                read_all_reply(data, req, function(reply) {
                    if (reply.status)
                        return res.status(200).json(reply);
                    else
                        return res.status(400).json({ error: 'read_all_reply error1', message: reply });
                });
            } catch (e) {
                return res.status(404).json({ error: 'read_all_reply error2', message: e });
            }
        }
    });
    
    fastify.post('/read_all', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                var queries = {};
                if (isUuid(req.body.conversation_id))
                    var q = { conversation_id: req.body.conversation_id, is_reply_msg: 'no', msg_status: { $in: [decode.id] } };
                else
                    var q = { msg_status: { $in: [decode.id] } };
                Messages.find(q, async function(e, ms) {
                    if (e) return res.status(400).json({ error: 'read_all error', message: e });
                    for (let i = 0; i < ms.length; i++){
                        if(! queries.hasOwnProperty(ms[i].conversation_id)) 
                            queries[ms[i].conversation_id] = [];
                        queries[ms[i].conversation_id].push(ms[i].msg_id);
                    }
                    let error = false;
                    let total_update = 0;
                    for (let convid in queries) {
                        total_update += queries[convid].length;
                        Messages.updateMany({conversation_id: convid, msg_id: {$in: queries[convid]}}, { msg_status: { $pull: decode.id } }, function(e){
                            if(e) {
                                error = true;
                                console.log(575, e);
                            }
                        });
                        console.log(755, total_update);
                        await znReadMsgUpdateCounter({user_id: decode.id, conversation_id: convid, is_reply_msg: 'no', root_msg_id: '', read: queries[convid].length, reply_mention:0});
                    }
                    setTimeout(function() {
                        xmpp_send_server(decode.id, 'read_status_msg', { conversation_id: req.body.conversation_id, is_reply_msg: 'no', root_msg_id: '', read: total_update }, req);
                    }, 2000);
                    console.log(761, total_update);
                    return res.status(200).json({success: error, queries, read: total_update, message: "All msg status set read"});
                        // queries.push(models.instance.Messages.update({ msg_id: ms[i].msg_id, conversation_id: ms[i].conversation_id }, { msg_status: { $remove: [decode.id] } }, { return_query: true }));
                    // if (queries.length > 0) {
                    //     models.doBatch(queries, function(err) {
                    //         if (err) return res.status(400).json({ error: 'read_all error2', message: err });
                    //         setTimeout(function() {
                    //             xmpp_send_server(decode.id, 'read_status_msg', { conversation_id: req.body.conversation_id, is_reply_msg: 'no', root_msg_id: '', read: queries.length }, req);
                    //         }, 2000);
                    //         return res.status(200).json({ message: "All msg status set read" });
                    //     });
                    // } else {
                    //     return res.status(200).json({ message: "Done" });
                    // }
                });
            } catch (e) {
                console.log(777, e);
                return res.status(404).json({ error: 'read_all error0', message: e });
            }
        }
    });
    
    /**
     * this route use for file gallery
     * 
     * conversation_id = conversation_id / all_files
     * file_type = star/ delete
     */
    function count_file(arg) {
        // return new Promise((resolve, reject) => {
        //     var q = "SELECT COUNT(*) FROM file WHERE" + arg + " ALLOW FILTERING ;";
        //     models.instance.File.execute_query(q, {}, function(e, file) {
        //         if (e) reject(e);
        //         if (file.rows[0].count)
        //             resolve(file.rows);
        //         else
        //             resolve(0);
        //     });
        // });
    }
    
    /**
     * this route use for file gallery
     * 
     * conversation_id = conversation id/ conversation id array / all_files
     * conversation_ids = array of conversation ids
     * file_type = all/ image/ video/ audio/ application/ star/ tag/ delete
     * file_sub_type = 
     * tag_id = all/ tag id
     * tag_operator = or/ and [default and]
     * from = YYYY-MM-DD
     * to = YYYY-MM-DD
     * limit = default 20
     * tab = tag/ file
     */
    fastify.post('/get_file_gallery', async(req, res) => {
        // console.log(1714, req.body);
        const token = extractToken(req);
        if (token) {
            let decode = await token_verify_decode(token);
            const PAGE_SIZE = 20;
            const page = Number(req.body.page) || 1;
            // console.log(643, req.body);
            try {
                if(req.body.tab === 'tag'){
                    const tags = await get_tags2({id: decode.id, company_id: decode.company_id, conversation_id: req.body.conversation_id});
                    return res.status(200).json({ tags: tags.result });
                }
                else if(req.body.tab === 'file'){
                    req.body.conversation_id = req.body.conversation_ids && Array.isArray(req.body.conversation_ids) && req.body.conversation_ids.length > 0 ? req.body.conversation_ids : req.body.conversation_id;
                    // req.body.conversation_id = req.body.conversation_id === decode.id ? 'its_you' : req.body.conversation_id;
                    var query = { is_delete: 0 };
                    if (isUuid(req.body.conversation_id))
                        query.conversation_id = req.body.conversation_id;
                    else if (req.body.conversation_id === 'all_files' || req.body.conversation_id === 'recent') {
                        query.participants = { $in: [decode.id] };
                    } else if (req.body.conversation_id === 'other_files') {
                        query.participants = { $nin: decode.id };
                    } else if(Array.isArray(req.body.conversation_id)){
                        query.conversation_id = {$in: req.body.conversation_id};
                    }
                    else
                        query.user_id = decode.id;
                    
                    if (req.body.file_type && req.body.file_type == 'delete') {
                        query.is_delete = 1;
                    } else if (req.body.file_type && req.body.file_type != 'all')
                        query.file_category = req.body.file_type;
    
                    if (req.body.file_sub_type && req.body.file_sub_type === 'tag') {
                        if(all_untags[decode.company_id].tag_id && all_untags[decode.company_id].tag_id === req.body.tag_id[0]){
                            // query.tag_list = [];
                            query.$and = [{$or: [{tag_list: []}, {tag_list: null}, {tag_list: 'null'}]}];
                            query.participants = { $in: [decode.id] };
                        }else{
                            if(Array.isArray(req.body.tag_id) && req.body.tag_id.length>0){
                                // query.tag_list = req.body.tag_operator === 'or' ? {$in: req.body.tag_id } : {$all: req.body.tag_id };
                                let tag1 = req.body.tag_id.splice(0, 1);
                                if(req.body.tag_id.length == 0){
                                    query.tag_list = {$in: tag1 };
                                }else{
                                    if(req.body.tag_operator === 'or'){
                                        query.$and = [
                                            {
                                              "tag_list": {
                                                $elemMatch: {
                                                  $all: tag1
                                                }
                                              }
                                            },
                                            {
                                              "tag_list": {
                                                $elemMatch: {
                                                  $in: req.body.tag_id
                                                }
                                              }
                                            }
                                        ];
                                    }else{
                                        query.tag_list = {$all: [...tag1, ...req.body.tag_id] };
                                    }
                                }
                                query.has_tag = req.body.file_sub_type;
                            }
                            else{
                                query.participants = { $in: [decode.id] };
                            }
                        }
                        if (!isUuid(req.body.conversation_id)) {
                            delete query.user_id;
                        }
                    } else if (req.body.file_sub_type && req.body.file_sub_type === 'star') {
                        query.star = { $in: [decode.id] };
                        // delete query.conversation_id;
                        // delete query.user_id;
                        // delete query.participants;
                    } else if(req.body.file_sub_type && req.body.file_sub_type === 'share'){
                        query.user_id = decode.id;
                        query.url_short_id = {$ne: ""};
                        delete query.participants;
                    } 
    
                    if(isUuid(req.body.uploaded_by)){
                        delete query.participants;
                        delete query.has_tag;
                        delete query.star;
                        req.body.file_sub_type = "";
                        query.user_id = req.body.uploaded_by;
                    }
                    
                    if (req.body.file_name && req.body.file_name != ''){
                        query.originalname = new RegExp(req.body.file_name, 'i');
                    }
                    
                    //YYYY-MM-DD
                    if (req.body.from) {
                        query.created_at = {};
                        query.created_at.$gte = new Date(req.body.from + ' 00:00:01+0000');
    
                        if (req.body.to) {
                            query.created_at.$lte = new Date(req.body.to + ' 23:59:59+0000');
                        }
                    }
                    
    
                    query.has_delete = { $nin: [decode.id.toString()] };
    
                    query.$or = [ 
                        {
                            $and: [
                                { is_secret: true }, 
                                { secret_user: { $in: [decode.id] }} 
                            ]
                        },
                        { is_secret: false }
                    ];
    
                    if(req.body.file_sub_type && req.body.file_sub_type === 'ref'){
                        query.referenceId = { "$exists": true, "$ne" : "" };
                        query.company_id = decode.company_id;
                        // query.has_tag = "tag";
                        query.participants = { $in: [decode.id] };
                        
                        delete query.user_id;
                        delete query.other_user;
                        
                        
                    }
                    if(req.body.file_name){
                        delete query.originalname;
                        query.$or = [ 
                            {referenceId: new RegExp(req.body.file_name, 'i')},
                            {originalname: new RegExp(req.body.file_name, 'i')},
                        ];
                    }
    
                    
                    // console.log(724, query);
    
                    
                    
                    // console.log(1486, tags.result.length);
                    // if (req.body.limit === 0)
                    //     return res.status(200).json({ files, tags: this_conv_tag, pageState: null });
                    // else
                    //     var limit = req.body.limit ? req.body.limit : 20;
                    
                    // var page_obj = { fetchSize: limit, raw: true, allow_filtering: true };
                    // if (req.body.pageState) page_obj.pageState = req.body.pageState;
                    let copyofquery = {...query};
                    // console.log(912, query.file_category);
                    delete copyofquery.file_category;
                    // console.log(914, copyofquery.file_category);
                    const countTotal = [
                                            {$match: copyofquery}, 
                                            {$group: {
                                                _id: "$file_category",
                                                count: { $sum: 1 }
                                            }}
                                        ];
                    // console.log(922, query.file_category);
                    const getConversation = [
                                            {$match: query}, 
                                            {$group: {
                                                _id: '$conversation_id'
                                            }},
                                            {$project: {
                                                conversation_id: '$_id',
                                                _id: 0
                                            }}
                                        ];
                                        // console. f log(740, getConversation);
                    // const tagLists = [
                    //                         {$match: query}, 
                    //                         {$group: {
                    //                             _id: '$tag_list'
                    //                         }},
                    //                         {$project: {
                    //                             tag_list: '$_id',
                    //                             _id: 0
                    //                         }}
                    //                     ];
                    // console.log('file_hub_query',query);                    
                    const dataAggregate = [
                                            {$match: query}, 
                                            {$lookup:{ 
                                                from: 'tags', 
                                                localField: 'tag_list', 
                                                foreignField:'tag_id',
                                                as:'tag_list_details'
                                            }},
                                            { $sort: { created_at: -1 } },
                                            { $skip: (page - 1) * PAGE_SIZE },
                                            { $limit: PAGE_SIZE },
                                            {
                                                $project: {
                                                    company_id: 1,
                                                    id: 1,
                                                    acl: 1,
                                                    bucket: 1,
                                                    conversation_id: 1,
                                                    created_at: 1,
                                                    file_category: 1,
                                                    file_size: 1,
                                                    file_type: 1,
                                                    has_delete: 1,
                                                    has_tag: 1,
                                                    is_delete: 1,
                                                    is_secret: 1,
                                                    key: 1,
                                                    location: 1,
                                                    main_msg_id: 1,
                                                    mention_user: 1,
                                                    msg_id: 1,
                                                    originalname: 1,
                                                    other_user: 1,
                                                    participants: 1,
                                                    referenceId: 1,
                                                    reference_type: 1,
                                                    root_conv_id: 1,
                                                    secret_user: 1,
                                                    star: 1,
                                                    tag_list: 1,
                                                    tag_list_with_user: 1,
                                                    url_short_id: 1,
                                                    user_id: 1,
                                                    user_name: 1,
                                                    "tag_list_details.company_id": 1,
                                                    "tag_list_details.tag_id": 1,
                                                    "tag_list_details.shared_tag": 1,
                                                    "tag_list_details.tag_color": 1,
                                                    "tag_list_details.tag_type": 1,
                                                    "tag_list_details.tagged_by": 1,
                                                    "tag_list_details.title": 1,
                                                    "tag_list_details.type": 1,
                                                }
                                            }
                                        ];
                    // console.log(1021, JSON.stringify(dataAggregate));
                    const [countResult, get_Conversation, files] = await Promise.all([
                        File.aggregate(countTotal),
                        File.aggregate(getConversation),
                        // Files.aggregate(tagLists),
                        File.aggregate(dataAggregate)
                    ]);
                    var convids = [];
                    var conversation_name = [];
                    // console.log(838, get_Conversation.length);
                    for(let i=0; i<get_Conversation.length; i++){
                        if(convids.indexOf(get_Conversation[i].conversation_id) === -1)
                            convids.push(get_Conversation[i].conversation_id);
                    }
                    // console.log(843, convids.length);
                    let convinfo = await get_conversation({conversation_id: convids, user_id: decode.id});
                    // console.log(845, convinfo.length);
                    for(let j=0; j<files.length; j++){
                        files[j].conversation_title = "";
                        files[j].conversation_img = "";
                        files[j].location = process.env.FILE_SERVER + files[j].location;
                        files[j].uploaded_by = all_users[decode.company_id][files[j].user_id].firstname + " " + all_users[decode.company_id][files[j].user_id].lastname;
                    }
                    for(let i=0; i<convinfo.length; i++){
                        conversation_name.push({
                            conversation_id: convinfo[i].conversation_id,
                            title: convinfo[i].title,
                            conv_img: convinfo[i].conv_img,
                        });
                        for(let j=0; j<files.length; j++){
                            if(files[j].conversation_id === convinfo[i].conversation_id){
                                files[j].conversation_title = convinfo[i].title;
                                files[j].conversation_img = convinfo[i].conv_img;
                            }
                        }
                    }
                    convids = [];
                    convinfo = [];
                    var summary = {
                                        total: 0, 
                                        image: 0, 
                                        audio: 0, 
                                        video: 0, 
                                        other: 0,
                                        voice: 0,
                                        share: 0,
                                        conversation: conversation_name,
                                        tags: []
                                    };
                    // for(let i=0; i<tag_Lists.length; i++){
                    //     // console.log(813, tag_Lists[i].tag_list);
                    //     for(let j=0; j<tag_Lists[i].tag_list.length; j++){
                    //         if(summary.tags.indexOf(tag_Lists[i].tag_list[j]) == -1){
                    //             summary.tags.push(tag_Lists[i].tag_list[j])}
                    //     }
                    // }
                    
                    // for(let i=0; i<files.length; i++){
                    //     if(files[i].tag_list_details.favourite.indexOf(decode.id)>-1) files[i].tag_list_details.favourite = [decode.id];
                    //     else files[i].tag_list_details.favourite = [];
                    // }
                    // tags = await get_tags2({id: decode.id, company_id: decode.company_id, tag_ids: summary.tags});
                    
                    for(let i=0; i<countResult.length; i++){
                        if(countResult[i]._id == 'image') summary.image += countResult[i].count;
                        if(countResult[i]._id == 'audio') summary.audio += countResult[i].count;
                        if(countResult[i]._id == 'video') summary.video += countResult[i].count;
                        if(countResult[i]._id == 'docs') summary.other += countResult[i].count;
                        if(countResult[i]._id == 'voice') summary.voice += countResult[i].count;
                        summary.total += countResult[i].count;
                    }
                    const totalPages = Math.ceil(summary.total / PAGE_SIZE);
                    return res.status(200).json({ files, summary, pagination: {
                        page: parseInt(page),
                        totalPages,
                        total: summary.total
                    } });
                    
                }
            } catch (e) {
                return res.status(404).json({ error: 'get_file_gallery error2', message: e });
            }
        }
    });
    
    /**
     * this route use for show other company counter
     * 
     * email
     */
    fastify.post('/other_account_counter', (req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try {
                Users.find({ email: req.body.email }, async function(error, users) {
                    if (error) return res.status(404).json({ error: 'other_account_counter error1', message: error });
                    if (users.length > 1) {
                        var all_company = await get_all_company();
                        let promises = [];
                        for (let i = 0; i < users.length; i++) {
                            // if (users[i].id.toString() != decode.id.toString()) {
                            var data = { user_id: users[i].id.toString(), img: process.env.FILE_SERVER + 'profile-pic/Photos/' + users[i].img, role: users[i].role };
                            all_company.forEach(function(v, k) {
                                if (v.company_id.toString() == users[i].company_id.toString()) {
                                    data.company_id = users[i].company_id.toString();
                                    data.company_name = v.company_name;
    
                                    data.total_urmsg = 0;
                                    data.total_urreply = 0;
                                    data.notification = 0;
                                }
                            });
                            // console.log(805, data);
                            // promises.push(await count_unread_reply_for_other_acc(data));
                            promises.push(data);
    
                            // }
                        }
                        return res.status(200).json({ status: true, total_counter: promises });
                    } else {
                        return res.status(200).json({ message: 'No other account found.' });
                    }
                });
            } catch (e) {
                return res.status(404).json({ error: 'other_account_counter error2', message: e });
            }
        }
    });
    
    fastify.post('/another_account_counter', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try {
                var data = { 
                    user_id: req.body.user_id, 
                    img: all_users[req.body.company_id][req.body.user_id].img, 
                    role: all_users[req.body.company_id][req.body.user_id].role, 
                    company_id: req.body.company_id,
                    company_name: all_company[req.body.company_id].company_name
                };
                var result = await count_unread_reply_for_other_acc(data);
                return res.status(200).json({ status: true, total_counter: result });
            } catch (e) {
                return res.status(404).json({ error: 'another_account_counter error2', message: e });
            }
        }
    });
    
    /**
     * file share link create
     * file_id
     * expire in sec
     * participants
     */
    fastify.patch('/file_share_link_create', async(req, res) => {
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        if (isUuid(req.body.file_id)) {
            let query = { id: String(req.body.file_id) };
            var file = await File.findOne(query).lean();
            if (file) {
                if (file.user_id.toString() === decode.id) {
                    let short_id = short.generate();
                    // if (req.body.expire && parseInt(req.body.expire) > 0)
                    //     var opt = { ttl: parseInt(req.body.expire), if_exists: true };
                    // else
                    //     var opt = { if_exists: true };
    
                    await File.updateOne({ id: file.id }, { url_short_id: short_id });
    
                    // models.instance.File.update({ id: file.id, company_id: decode.company_id }, { url_short_id: short_id }, opt, function(error) {
                        // if (error) return res.status(400).json({ error: "Update error", message: error });
    
                        file.url_short_id = short_id;
                        try{
                            var tlwu = JSON.parse(file.tag_list_with_user);
                        }catch(e){
                            var tlwu = [];
                        }
                        file.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
                        req.body.participants.forEach(function(v, k) {
                            xmpp_send_server(v, 'file_share_link_create', file, req);
                            send_msg_firebase(v, 'file_share_link_create', file, req);
                        });
    
                        var mainmsg = (await get_message({ convid: file.conversation_id, msgid: file.msg_id, company_id: file.company_id, type: 'one_msg', limit: 1, user_id: decode.id })).msgs;
    
                        await znMsgUpdate(mainmsg[0], file.participants, 'msg_update_edit');
    
                        return res.status(200).send(file);
                    // });
                } else {
                    return res.status(400).send({ error: "Error", message: "You are not the file owner." });
                }
            } else {
                return res.status(200).send({ error: "Not found", message: "No file found by this file id" });
            }
    
            // models.instance.File.findOne(query, { raw: true, allow_filtering: true }, function(e, file) {
                // if (e) return res.status(400).json({ error: 'Find error', message: e });
                
                // return res.status(200).send(companies);
            // });
        } else {
            return res.status(400).json({ error: "Validation error", message: 'Invalid file id' });
        }
    });
    
    /**
     * remove file share link
     * file_id
     * participants
     */
    fastify.patch('/file_share_link_remove', async(req, res) => {
        const token = extractToken(req);
        let decode = await token_verify_decode(token);
        if (isUuid(req.body.file_id)) {
            let query = { id: req.body.file_id };
            File.findOne(query, function(e, file) {
                if (e) return res.status(400).json({ error: 'Find error', message: e });
                if (file) {
                    if (file.user_id.toString() === decode.id) {
                        File.updateOne({ _id: file._id }, { url_short_id: "" }, async function(error) {
                            if (error) return res.status(400).json({ error: "Update error", message: error });
    
                            file.url_short_id = "";
                            try{
                                var tlwu = JSON.parse(file.tag_list_with_user);
                            }catch(e){
                                var tlwu = [];
                            }
                            file.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
                            req.body.participants.forEach(function(v, k) {
                                xmpp_send_server(v, 'file_share_link_remove', file, req);
                                send_msg_firebase(v, 'file_share_link_remove', file, req);
                            });
    
                            var mainmsg = (await get_message({ convid: file.conversation_id, msgid: file.msg_id, company_id: file.company_id, type: 'one_msg', limit: 1, user_id: decode.id })).msgs;
    
                            await znMsgUpdate(mainmsg[0], file.participants, 'msg_update_edit');
    
                            return res.status(200).send(file);
                        });
                    } else {
                        return res.status(400).send({ error: "Error", message: "You are not the file owner." });
                    }
                } else {
                    return res.status(200).send({ error: "Not found", message: "No file found by this file id" });
                }
                // return res.status(200).send(companies);
            }).lean();
        } else {
            return res.status(400).json({ error: "Validation error", message: 'Invalid file id' });
        }
    });
    
    /**
     * share file view
     * url_short_id
     */
    fastify.get('/file_share_link_view', async (req, res) => {
        // console.log(1517, req.query);
        let query = { url_short_id: req.query.url, is_delete: 0 };
        var file = await File.findOne(query);
        if (file) {
            file.fullpath = process.env.FILE_SERVER + file.location;
            try{
                var tlwu = JSON.parse(file.tag_list_with_user);
            }catch(e){
                var tlwu = [];
            }
            file.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
            return res.status(200).send({fullpath: process.env.FILE_SERVER + file.location});
        } else {
            return res.status(200).send({ error: "Not found", message: "Invalid URL" });
        }
    
        // models.instance.File.findOne(query, { raw: true, allow_filtering: true }, function(e, file) {
        //     if (e) return res.status(400).json({ error: 'Find error', message: e });
            
        // });
    });
    
    /**
     * this route use for all flag
     * 
     * conversation_id
     * msg_id = for load more msg
     * type = old (for load more msg)
     */
    function get_convs_lists(arg) {
        return new Promise(async(resolve, reject) => {
            arg.page_size = 'unlimited';
            var msgs = await get_filter_msg(arg);
            // console.log(1106, msgs.msgs.length);
            if (msgs.msgs.length > 0) {
                var convs = [];
                var conv_id = [];
                for (let i = 0; i < msgs.msgs.length; i++) {
                    // console.log(1884, msgs.msgs[i].conversation_id.toString());
                    if (conv_id.indexOf(msgs.msgs[i].conversation_id.toString()) === -1) {
                        convs.push({ total: 1, msgs: [msgs.msgs[i]], conversation_id: msgs.msgs[i].conversation_id });
                        conv_id.push(msgs.msgs[i].conversation_id.toString());
                    } else {
                        for (let k = 0; k < convs.length; k++) {
                            if (convs[k].conversation_id.toString() === msgs.msgs[i].conversation_id.toString()) {
                                convs[k].total = convs[k].total + 1;
                                convs[k].msgs.push(msgs.msgs[i]);
                            }
                        }
                    }
                }
                // console.log(1124, convs.length)
                
                var query = { conversation_id: conv_id, user_id: arg.user_id };
                if (arg.name === 'link') query.user_id = arg.user_id;
                var conversation = await get_conversation(query);
                conversation = conversation === undefined ? [] : conversation;
                for (let n = 0; n < conversation.length; n++) {
                    convs.forEach(function(v, k) {
                        if (v.conversation_id.toString() === conversation[n].conversation_id.toString()) {
                            conversation[n].total = v.total;
                            conversation[n].msgs = v.msgs;
                        }
                    });
                }
                // console.log(1144, conversation.length);
                resolve(conversation);
            } else
                resolve([]);
    
        });
    }
    
    /**
     * type = has_flag
     * conversation_id
     * page
     */
    fastify.get('/all_flag', async(req, res) => {
        console.log(1583, req.query);
        const token = extractToken(req);
        if (token) {
            let decode = await token_verify_decode(token);
            try {
                if (req.query.type === 'has_flag') {
                    const countTotal = [
                        {$match: {has_flagged: {$in: [decode.id]}}}, 
                        {$count: "total"}
                    ];
                    const result = await Messages.aggregate(countTotal);
                    return res.status(200).json({ has_flag: result[0].total > 0, total: result[0].total });
                } else {
                    var result = {};
                    result.list_of_conversations = [];
                    result.msgs = [];
                    result.pagination = {};
                    var arg = {
                        name: 'flag',
                        user_id: decode.id,
                        company_id: decode.company_id,
                        limit: 20
                    };
                    let con = `${path}/all_flag/${decode.id}`;
                    let hasNode = await checkZNode(con);
                    if(! hasNode) await setZK(con, null);
                    con = `${path}/all_flag/${decode.id}/${req.query.conversation_id}`;
                    hasNode = await checkZNode(con);
                    
                    if (req.query.conversation_id === undefined) {
                        con = `${path}/all_flag/${decode.id}/list_of_conversations`;
                        hasNode = await checkZNode(con);
                        if(! hasNode) {
                            arg.limit = 'all';
                            // console.log(1171, arg);
                            result.list_of_conversations = await get_convs_lists(arg);
                            await setZK(con, result.list_of_conversations);
                        }else{
                            result.list_of_conversations = await getZK(con);
                        }
    
                        if (result.list_of_conversations && result.list_of_conversations.length > 0)
                            req.query.conversation_id = result.list_of_conversations[0].conversation_id;
                    }
                    else if (isUuid(req.query.conversation_id)) {
                        arg.conversation_id = req.query.conversation_id;
    
                        con = `${path}/all_flag/${decode.id}/${req.query.conversation_id}`;
                        hasNode = await checkZNode(con);
                        let allfm = {};
                        if(! hasNode) {
                            result.list_of_conversations = await get_conversation(arg);
                            allfm = await get_filter_msg(arg);
                            await setZK(con, allfm);                        
                        }else{
                            allfm = await getZK(con);
                        }
                        result.msgs = allfm.msgs;
                        result.pagination = allfm.pagination;
                        allfm = {};
                    }
                    if(result.msgs && result.msgs.length > 0) result.msgs = result.msgs.reverse();
    
                    return res.status(200).json(result);
                }
            } catch (e) {
                return res.status(400).json({ error: 'all_flag error2', message: e });
            }
        }
    });
    
    /**
     * conversation_id
     * page
     */
    fastify.get('/all_private_msgs', async(req, res) => {
        // console.log(1207, req.query);
        const token = extractToken(req);
        if (token) {
            let decode = await token_verify_decode(token);
            try {
                var result = {};
                result.list_of_conversations = [];
                result.msgs = [];
                result.pagination = {};
                var arg = {
                    name: 'private',
                    user_id: decode.id,
                    company_id: decode.company_id,
                    limit: 20
                };
    
                let con = `${path}/all_private_msgs/${decode.id}`;
                let hasNode = await checkZNode(con);
                if(! hasNode) await setZK(con, null);
                con = `${path}/all_private_msgs/${decode.id}/${req.query.conversation_id}`;
                hasNode = await checkZNode(con);
                
                if (req.query.conversation_id === undefined) {
                    con = `${path}/all_private_msgs/${decode.id}/list_of_conversations`;
                    hasNode = await checkZNode(con);
                    if(! hasNode) {
                        arg.limit = 'all';
                        result.list_of_conversations = await get_convs_lists(arg);
                        await setZK(con, result.list_of_conversations);
                    }else{
                        result.list_of_conversations = await getZK(con);
                    }
                    
                    // console.log(1224, arg);
                    if (result.list_of_conversations && result.list_of_conversations.length > 0)
                        req.query.conversation_id = result.list_of_conversations[0].conversation_id;
                }
                else if (isUuid(req.query.conversation_id)) {
                    arg.conversation_id = req.query.conversation_id;
    
                    con = `${path}/all_private_msgs/${decode.id}/${req.query.conversation_id}`;
                    hasNode = await checkZNode(con);
                    let allpm = {};
                    if(! hasNode) {
                        result.list_of_conversations = await get_conversation(arg);
                        allpm = await get_filter_msg(arg);
                        await setZK(con, allpm);                        
                    }else{
                        allpm = await getZK(con);
                    }
                    result.msgs = allpm.msgs;
                    result.pagination = allpm.pagination;
                    allpm = {};
                }
                if(result.msgs && result.msgs.length > 0) result.msgs = result.msgs.reverse();
    
                return res.status(200).json(result);
            } catch (e) {
                return res.status(400).json({ error: 'all_private error2', message: e });
            }
        }
    });
    
    fastify.get('/all_link_msgs', async(req, res) => {
        // console.log(1968, req.query);
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try {
                var result = {};
                result.list_of_conversations = [];
                result.msgs = [];
                var arg = {
                    name: 'link',
                    user_id: decode.id,
                    company_id: decode.company_id,
                    limit: 20
                };
                if (req.query.conversation_id === undefined) {
                    arg.limit = 'all';
                    // console.log(1984, arg);
                    result.list_of_conversations = await get_convs_lists(arg);
                    if (result.list_of_conversations && result.list_of_conversations.length > 0)
                        req.query.conversation_id = result.list_of_conversations[0].conversation_id;
                }
                else if (isUuid(req.query.conversation_id)) {
                    arg.conversation_id = req.query.conversation_id;
                    // console.log(1991, arg);
                    result.list_of_conversations = await get_conversation(arg);
                    result.msgs = await get_filter_msg(arg);
                }
                if(result.msgs && result.msgs.length > 0) result.msgs = result.msgs.reverse();
    
                return res.status(200).json(result);
            } catch (e) {
                return res.status(400).json({ error: 'all_private error2', message: e });
            }
        }
    });
    
    fastify.get('/all_archive', async(req, res) => {
        // console.log(2180, req.query);
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try {
                var result = {};
                result.list_of_conversations = [];
                let convs = await get_conversation({user_id: decode.id, archive: 'yes'});
                // Conversation.find({ participants: { $in: [decode.id.toString()] }, archive: 'yes' }, { raw: true, allow_filtering: true }, function(error, convs) {
                //     if (error) return res.status(404).json({ error: 'Find error', message: error });
    
                //     convs = convs === undefined ? [] : convs;
                //     if (all_bunit.hasOwnProperty(decode.company_id))
                //         var all_bunits = Object.values(all_bunit[decode.company_id]);
                //     else
                //         var all_bunits = [];
                    
                //     for (let n = 0; n < convs.length; n++) {
                //         convs[n].friend_id = "";
                //         convs[n].team_id_name = "";
                //         convs[n].close_for = convs[n].close_for === null ? 'no' : 'yes';
                //         convs[n].temp_user = convs[n].temp_user === null ? [] : convs[n].temp_user;
                //         convs[n].conv_is_active = 1;
                //         convs[n].b_unit_id_name = "";
                //         convs[n].fnln = "";
    
                //         /* Manipulate the convs data */
                //         if (convs[n].single == "yes" && convs[n].conversation_id.toString() != decode.id.toString()) {
                //             /*Single convs data manipulation */
                //             var fid = convs[n].participants.join().replace(decode.id.toString(), '').replace(',', '');
                //             if (isUuid(fid) && all_users[decode.company_id].hasOwnProperty(fid)) {
                //                 convs[n].title = all_users[decode.company_id][fid].firstname + " " + all_users[decode.company_id][fid].lastname;
                //                 convs[n].conv_img = all_users[decode.company_id][fid].img;
                //                 convs[n].friend_id = fid;
                //                 convs[n].fnln = all_users[decode.company_id][fid].fnln;
                //                 convs[n].conv_is_active = all_users[decode.company_id][fid].is_active;
                //             }
                //         } else {
                //             /*Group convs data manipulation */
                //             if (convs[n].conversation_id.toString() != decode.id.toString())
                //                 convs[n].conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + convs[n].conv_img;
                //             _.each(all_teams[decode.company_id], function(v, k) {
                //                 if (v.team_id.toString() == convs[n].team_id) {
                //                     convs[n].team_id_name = v.team_title;
                //                     return false;
                //                 }
                //             });
    
                //             _.each(all_bunits, function(v, k) {
                //                 if (v.unit_id && convs[n].b_unit_id) {
                //                     if (v.unit_id.toString() == convs[n].b_unit_id) {
                //                         convs[n].b_unit_id_name = v.unit_name;
                //                         return false;
                //                     }
                //                 }
                //             });
                //         }
    
                //         if (convs[n].pin === null) convs[n].pin = [];
                //         if (convs[n].tag_list === null) convs[n].tag_list = [];
                //         if (convs[n].participants_admin == null) convs[n].participants_admin = [];
                //         //mute convs
                //         if (convs[n].has_mute == null) convs[n].has_mute = [];
                //         if (convs[n].mute == null) convs[n].mute = [];
                //         else {
                //             var perconvmute = [];
                //             _.each(convs[n].mute, function(v, k) {
                //                 perconvmute.push(JSON.parse(v));
                //             });
                //             convs[n].mute = perconvmute;
                //         }
    
                //         convs.forEach(function(v, k) {
                //             if (v.conversation_id.toString() === convs[n].conversation_id.toString()) {
                //                 convs[n].total = v.total;
                //                 convs[n].msgs = v.msgs;
                //             }
                //         })
                //     }
                    // console.log(2263, convs.length);
                    return res.status(200).json(convs);
                // });
            } catch (e) {
                return res.status(400).json({ error: 'all_private error2', message: e });
            }
        }
    });
    
    /**
     * from = YYYY-MM-DD
     * to = YYYY-MM-DD
     * conversation_ids = array
     * user_ids = array
     * limit
     * page
     * sort_by = title/ url/ user_id/ created_at
     * sort_style = -1/ 1
     */
    fastify.post('/hub_all_link_msgs', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = await token_verify_decode(token);
            try {
                var convs = await get_conversation({conversation_id: req.body.conversation_ids, user_id: decode.id});
                
                // console.log(2469, convs.length);
                if(convs.length>0){
                    var convids = [];
                    var conv_obj = {};
                    for(let n=0; n<convs.length; n++){ 
                        convids.push(convs[n].conversation_id.toString()); 
                        conv_obj[convs[n].conversation_id.toString()] = convs[n];
                    }
                    // var limit = req.body.limit && Number(req.body.limit)>0 ? req.body.limit : 20;
                    var query = {
                        conversation_id: { $in: convids },
                        company_id: decode.company_id,
                        is_delete: '0'
                    };
                    if (req.body.from) {
                        query.created_at = {};
                        req.body.from = new Date(req.body.from).setHours(0, 0, 0, 0);
                        query.created_at = { $gte: new Date(req.body.from) };
        
                        if (req.body.to) {
                            req.body.to = new Date(req.body.to).setHours(23, 59, 59, 999);
                            query.created_at = { $gte: new Date(req.body.from), $lte: new Date(req.body.to) };
                        }
                    }
                    query.has_hide = { $nin: [decode.id] };
                    query.has_delete = { $nin: [decode.id] };
                    if(req.body.user_ids && req.body.user_ids.length > 0){
                        query.user_id = {$in: req.body.user_ids};
                    }
                    if (req.body.url && req.body.url != ''){
                        var str = req.body.url.toLowerCase();
                        query.$or = [
                            {title: new RegExp(str, 'i')},
                            {url: new RegExp(str, 'i')}
                        ]
                    }
                    
                    switch(req.body.sort_by){
                        case 'title':
                            var sort = { $sort: { 'title': req.body.sort_style } };
                            break;
                        case 'url':
                            var sort = { $sort: { 'url': req.body.sort_style } };
                            break;
                        case 'user_id':
                            var sort = { $sort: { 'user_id': req.body.sort_style } };
                            break;
                        case 'created_at':
                            var sort = { $sort: { 'created_at': req.body.sort_style } };
                            break;
                        default:
                            var sort = { $sort: { 'created_at': -1 } };
                            break;
                    }
    
                    const PAGE_SIZE = 20;
                    const page = Number(req.body.page) || 1;
    
                    const count_total = [{$match: query}, {$count: "total"}];
                    const all_conversation_id_in_link = [{$match: query}, {$group: {
                                                                _id: "$conversation_id"
                                                            }
                                                        }];
                    const all_uploaded_by_in_link = [{$match: query}, {$group: {
                                                                _id: "$user_id"
                                                            }
                                                        }];
                    const dataAggregate = [
                                            {$match: query}, 
                                            sort,
                                            { $skip: (page - 1) * PAGE_SIZE },
                                            { $limit: PAGE_SIZE }
                                        ];
                    // console.log(1664, JSON.stringify(query));
                    var [links, countResult] = await Promise.all([
                        Link.aggregate(dataAggregate),
                        Link.aggregate(count_total)
                    ]);
    
                    var conversation_id_in_link, uploaded_by_in_link = [];
                    if(page <= 1){
                        [conversation_id_in_link, uploaded_by_in_link] = await Promise.all([
                            Link.aggregate(all_conversation_id_in_link),
                            Link.aggregate(all_uploaded_by_in_link)
                        ]);
                        
                        conversation_id_in_link = conversation_id_in_link.map((e)=>{return e._id});
                        uploaded_by_in_link = uploaded_by_in_link.map((e)=>{return e._id});
                        console.log(1676, conversation_id_in_link, uploaded_by_in_link);
                    }
                    var total = countResult[0] ? countResult[0].total : 0;
                    var totalPages = Math.ceil(total / PAGE_SIZE);
                    
                    for(let i=0; i<links.length; i++){
                        links[i].conversation_title = conv_obj[links[i].conversation_id].title;
                        links[i].uploaded_by = all_users[decode.company_id][links[i].user_id].fullname;
    
                        if(!_.isEmpty(links[i].title)){
                            var have_my_title = false;
                            var sender_title = false;
                            var old_title = parseJSONSafely(links[i].title);
                            for (let p = 0; p < old_title.length; p++) {
                                // sender title
                                if(old_title[p].user_id == links[i].user_id && have_my_title === false){
                                    links[i].title = old_title[p].title;
                                    sender_title = true;
                                    if (old_title[p].user_id == decode.id){
                                        have_my_title = true;
                                    }
                                }
                                // custom title
                                if (old_title[p].user_id == decode.id && have_my_title === false){
                                    links[i].title = old_title[p].title;
                                    have_my_title = true;
                                }
    
                            }
                            // console.log(2624, have_my_title, sender_title, links[i].title);
                            links[i].title = have_my_title === false && sender_title === false ? "" : links[i].title;
                        }else{
                            links[i].title = "";
                        }
                    }
    
                    return res.status(200).json({links, conversation_id_in_link, uploaded_by_in_link, pagination: {
                        page: parseInt(page),
                        totalPages,
                        total
                    }});
                }
                else{
                    // console.log(2672, moment().format());
                    return res.status(200).json({links: [], pageState: null});
                }
            } catch (e) {
                return res.status(400).json({ error: 'hub_all_link_msgs error', message: e });
            }
        }
    });
    
    function get_link_details(id){
        return new Promise((resolve)=>{
            Link.find({url_id: String(id)}, function(e, l){
                if(e) {console.log(2585, e); resolve([]);}
                else if(l.length) resolve(l[0]);
                else resolve([]);
            });
        });
    }
    
    /**
     * url_ids = array
     * delete_type = only_link/ link_msg [default = only_link]
     */
    fastify.post('/delete_link', async(req, res) => {
        // console.log(2587, req.body);
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try{
                // input validation
                if(! req.body.delete_type) return res.status(400).json({ error: 'Validation error', message: 'delete_type not defined' });
                if(req.body.url_ids && Array.isArray(req.body.url_ids)){
                    for(let i=0; i<req.body.url_ids.length; i++){
                        if(! isUuid(req.body.url_ids[i])) 
                            return res.status(400).json({ error: 'Validation error', message: 'url_ids item invalid' });
                    }
                }else{
                    return res.status(400).json({ error: 'Validation error', message: 'url_ids not found or not array' });
                }
                // input validation end
    
                var queries = [];
                var return_data = [];
                var convids = [];
                for(let i=0; i<req.body.url_ids.length; i++){
                    var link_info = await get_link_details(req.body.url_ids[i]);
                    if(link_info.user_id === decode.id){
                        await Link.updateOne({_id: link_info._id}, {is_delete: "1"});
                        link_info.is_delete = 1;
                        return_data.push(link_info);
                        convids.push(String(link_info.conversation_id));
    
                        if(req.body.delete_type === 'link_msg'){
                            var mtq = {conversation_id: String(link_info.conversation_id), msg_id: String(link_info.msg_id)};
                            await new Promise((resolve)=>{
                                try{
                                    Messages.find(mtq, async function(err, ms){
                                        if(err) { 
                                            console.log(2622, err); 
                                            // return res.status(400).json({error: "Find error", message: me});
                                            resolve();
                                        }
                                        else if(ms.length == 1){
                                            try{
                                                var bytes = CryptoJS.AES.decrypt(ms[0].msg_body, process.env.CRYPTO_SECRET);
                                                let msgbody = bytes.toString(CryptoJS.enc.Utf8);
                                                if (msgbody.indexOf('"') == 0)
                                                    msgbody = (msgbody).substring(1, msgbody.length - 1);
                                                var msg_body = msgbody.replace(/<\/?[^>]+(>|$)/g, "").trim();
                                            }catch(e){
                                                var msg_body = ms[0].msg_body;
                                            }
                                            // console.log(1705, msg_body, link_info.url);
                                            link_info.url = (link_info.url).replace(/<\/?[^>]+(>|$)/g, "").trim();
                                            if(msg_body === link_info.url || msg_body === link_info.url+"/"){
                                                await Messages.updateOne(mtq, { $push : { has_hide : decode.id, has_delete : decode.id }});
                                            }
                                            else{
                                                msg_body = msg_body.replace(link_info.url, '[Link Deleted]');
                                                msg_body = CryptoJS.AES.encrypt(JSON.stringify(msg_body), process.env.CRYPTO_SECRET).toString();
                                                var msg_text = msg_body.replace(link_info.url, '');
                                                await Messages.updateOne(mtq, {msg_body: msg_body, msg_text: msg_text, msg_type: ms[0].msg_type.replace('link', '')});
                                            }
                                            let thismsg = await get_message({ convid: ms[0].conversation_id, company_id: ms[0].company_id, msgid: ms[0].msg_id, limit: 1, is_reply: ms[0].is_reply_msg, user_id: decode.id, type: 'one_msg' });
                                            await znMsgUpdate(thismsg.msgs[0], thismsg.details.participants, 'msg_update_delete');
                                        }
                                        else{
                                            console.log(1715, ms);
                                        }
                                        resolve();
                                    }).lean();
                                } catch(e){
                                    console.log(2653, e);
                                }
                            })
                        }
                    }
                    else{
                        return res.status(400).json({ error: 'delete_link error', message: "Sorry!!! You are not the link owner." });
                    }
                }
                // console.log(1729, convids);
                if(convids.length>0){
                    var allconv = await get_conversation({conversation_id: {$in: convids}, user_id: decode.id});
                    // var allconv = await get_my_all_conversations({conversation_id: {$in: convids}}, decode);
                    var allp = [decode.id];
                    if(allconv.length){
                        for(let i=0; i<allconv.length; i++){
                            for(let j=0; j<allconv[i].participants.length; j++){
                                if(allp.indexOf(allconv[i].participants[j]) == -1){
                                    allp.push(allconv[i].participants[j]);
                                }
                            }
                        }
                    }
                    let linkdata = {return_data};
                    for(let i=0; i<allp.length; i++)
                        xmpp_send_server(allp[i], 'delete_link', linkdata, req);
                }
                return res.status(200).json({ message: "Link deleted", return_data });
            } catch (e) {
                return res.status(400).json({ error: 'delete_link error', message: e });
            }
        }
    });
    
    /**
     * url_ids = array
     * delete_type = only_link/ link_msg [default = only_link]
     */
    fastify.post('/workai', async(req, res) => {
        console.log(1933, req.body);
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            try {
                // Make a request to the OpenAI API
                const response = await openai.chat.completions.create({
                    messages: [{ role: "system", content: req.body.qns }],
                    model: "gpt-3.5-turbo",
                });
              
                // Extract the generated text from the API response
                const generatedText = response.choices[0];
                
                // Send the generated text back as the response
                // console.log(generatedText)
                res.status(200).json({answer: generatedText.message.content });
            } catch (error) {
                console.error(error);
                res.status(400).json({answer: JSON.stringify({ status: 500, error: 'Internal server error', details: error }) });
            }
        }
    });
}

module.exports = routes;