const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const passport = require('passport');
const isUuid = require('uuid-validate');
var moment = require('moment');
var CryptoJS = require("crypto-js");

var { set_message, get_conversation } = require('../../v2_utils/message');
var { validateCreateRoomInput } = require('../../validation/conversation');
var { get_user, get_user_obj, add_user, self_conversation_create_if_not_found } = require('../../v2_utils/user');
var { get_team } = require('../../v2_utils/team');
const logger = require('../../logger');
var { get_bunit } = require('../../v2_utils/business_unit');
var { has_conversation, has_this_title_conversation, check_send_invite_email } = require('../../v2_utils/conversation');
var { save_notification } = require('../../v2_utils/notification');
var { check_company_limit } = require('../../v2_utils/company');
var { update_tag_count } = require('../../v2_utils/tag');
// var { create_teammate } = require('../../utils/teammate');
// var { models } = require('../../config/db/express-cassandra');
const Conversation = require('../../mongo-models/conversation');
const File = require('../../mongo-models/file');
const { v4: uuidv4 } = require('uuid');

var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { znConNodeUpdate } = require('../../v2_utils/zookeeper');
var {
    xmpp_send_server,
    xmpp_send_broadcast,
    send_msg_firebase
} = require('../../v2_utils/voip_util');
var { sg_email } = require('../../v2_utils/sg_email');
var { get_team_admin } = require('../../v2_utils/team');

// process.env.CRYPTO_SECRET = 'D1583ED51EEB8E58F2D3317F4839A';

function get_conversation_all_files(id) {
    return new Promise((resolve) => {
        File.find({ conversation_id: id }).then(function (e, f) {
            if (f.length)
                resolve(f);
            else
                resolve([]);
        }).catch((e) => {
            console.log(e); resolve([]);
        })
    });
}

async function routes (fastify, options) {
    /*
    Create new conversation
    */
    fastify.post('/create_room', async (req, res) => {
        const { errors, isValid } = validateCreateRoomInput(req.body);
        const token = extractToken(req);
        if (!isValid) {
            return res.status(400).send({ error: 'Validation error', message: errors });
        }
        try {
            var decode = await token_verify_decode(token);
            console.log(31, req.body);
            var convid = uuidv4();
            if (req.body.group && req.body.group === 'yes') {
                var httc = await has_this_title_conversation(req.body.title);
                if (httc.status === true && httc.conversation) {
                    return res.status(400).send({ error: 'Duplicate error', message: 'Conversation title already exists.' });
                }
    
                if (req.body.participants_guest && Array.isArray(req.body.participants_guest) && req.body.participants_guest.length > 0) {
                    req.body.user_id = decode.id;
                    req.body.emails = req.body.participants_guest;
                    req.body.name = decode.firstname + " " + decode.lastname;
                    req.body.company_name = decode.company_name;
                    req.body.role = "Guest";
                    req.body.convid = convid;
                    for (let i = 0; i < req.body.participants_guest.length; i++) {
                        // let code = 123456;
                        let code = Math.floor(100000 + Math.random() * 900000);
                        req.body.participants_guest[i].code = code;
                    }
                    try {
                        var company_limit = await check_company_limit(req.body.company_id, req.body.participants_guest.length);
                        if (!company_limit) {
                            console.log('This company users added limit exceeded.');
                            return res.status(400).send({ error: 'Error', message: 'This company users added limit exceeded.' });
                        }
                        var result = await add_user(req.body.participants_guest, req);
                        for (let i = 0; i < result.length; i++) {
                            self_conversation_create_if_not_found(result[i]);
                            req.body.participants.push(result[i].id.toString());
                        }
                    }
                    catch (e) {
                        console.log(51, e);
                        return res.status(400).send({ error: 'Error', message: e });
                    }
                }
            }
    
            if (req.body.single && req.body.single == 'yes') {
                var hc = await has_conversation(req.body);
                if (hc.status == true && hc.conversation) {
                    return res.status(200).send({ status: true, message: 'Conversation already exists.', conversation: hc.conversation });
                }
            }
    
    
            // console.log(64, req.body);
            // if(Array.isArray(req.body.participants_guest) && req.body.participants_guest.length > 0 && isUuid(req.body.participants_guest[0].toString())){
            //     for(let i=0; i<req.body.participants_guest.length; i++)
            //         req.body.participants_guest[i] = req.body.participants_guest[i].toString();
            // }
            // else req.body.participants_guest = [];
    
            req.body.participants_guest = Array.isArray(req.body.participants_guest) ? req.body.participants_guest : [];
            let pg = [];
            for (let i = 0; i < req.body.participants_guest.length; i++) {
                pg.push(JSON.stringify(req.body.participants_guest[i]));
            }
            if (pg.length > 0)
                req.body.participants_guest = pg;
            // console.log(94, req.body.participants_guest);
            let team_admin = [];
            // if(req.body.team_id)
            //     team_admin = get_team_admin(req.body.company_id, req.body.team_id);
    
            console.log('team_admin:get:::', team_admin);
            req.body.participants = [...new Set([...req.body.participants, ...team_admin])];
            const conv = new Conversation({
                conversation_id: convid,
                created_by: req.body.created_by,
                sender_id: req.body.created_by,
                participants: req.body.participants,
                participants_admin: [...new Set([...req.body.participants_admin, ...team_admin])],
                participants_guest: req.body.participants_guest,
                title: req.body.title,
                conv_img: req.body.conv_img ? req.body.conv_img : 'feelix.jpg',
                privacy: req.body.privacy ? req.body.privacy : 'public',
                single: req.body.single ? req.body.single : 'yes',
                group: req.body.group ? req.body.group : 'no',
                company_id: req.body.company_id,
                team_id: req.body.team_id ? req.body.team_id : '',
                b_unit_id: req.body.b_unit_id ? req.body.b_unit_id : '',
                tag_list: Array.isArray(req.body.tag_list) ? req.body.tag_list : [],
                topic_type: req.body.is_block && req.body.is_block === 'Block Room' ? req.body.is_block : null
            });
    
            // console.log(112, conv);
            conv.save().then(async () => {
                var newconv = await get_conversation({ conversation_id: convid, user_id: decode.id });
                save_notification({
                    company_id: req.body.company_id,
                    receiver_id: req.body.participants,
                    title: "New conversation '" + req.body.title + "' created",
                    type: "conversation",
                    body: JSON.stringify(newconv[0]),
                    user_id: req.body.created_by,
                    user_name: decode.firstname + ' ' + decode.lastname,
                    user_img: decode.img
                }, req);
    
                req.body.name = decode.firstname + " " + decode.lastname;
                req.body.company_name = decode.company_name;
                check_send_invite_email(req);
                // io broadcast to all that a new room created
                for (let v of req.body.participants) {
                    if (newconv[0].group === 'no') {
                        let fid = newconv[0].participants.join().replace(v, '').replace(',', '');
                        console.log(166, v);
                        let tp = all_users[req.body.company_id][fid];
                        newconv[0].title = tp.firstname + ' ' + tp.lastname;
                        newconv[0].fnln = tp.fnln;
                        console.log(170, fid, newconv[0].title);
                    }
                    xmpp_send_server(v, 'new_room', newconv[0], req);
                    await znConNodeUpdate({ uid: v, type: 'new_room', conv: newconv[0] });
                    // if(process.env.TEST_CHECK === 'yes'){
                    //     if(v !== req.body.created_by){
                    //         var uinfo = get_user_obj(v);
                    //         var emaildata = {
                    //             to: uinfo.email,
                    //             // bcc: 'mahfuzak08@gmail.com',
                    //             subject: 'New room invitation',
                    //             text: 'New room invitation',
                    //             html: 'Hi ' + uinfo.firstname + ',<br><br>' + uobj.firstname + ' created and invited to join a new room "'+ req.body.title +'" in Workfreeli <b>'+ decode.company_name +'</b> company.<br><br>To catch up on what you’ve missed in Workfreeli, please click sign-in button below. If for any reason, you are unable to log in, please use “Forget Password” option on Sign-in Page to receive one time password (OTP) to login and reset your password.<br><br><a clicktracking=off href="'+ req.headers.origin +'" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                    //         }
    
                    //         // console.log(emaildata);
                    //         try {
                    //             sg_email.send(emaildata, (result) => {
                    //                 if (result.msg == 'success') {
                    //                     // res.status(200).send({ status: true, message: 'Workfreeli password reset code send successfully' });
                    //                 } else {
                    //                     console.log('create_room ', result);
                    //                     // res.status(200).send({ status: false, message: 'Workfreeli password reset but email not send', error: result });
                    //                 }
                    //             });
                    //         } catch (e) {
                    //             console.log(e);
                    //             // res.status(200).send({ status: false, message: 'Workfreeli password reset but email not send', error: e });
                    //         }
                    //     }
                    // }
                }
                if (newconv[0].group === 'no') {
                    let fid = newconv[0].participants.join().replace(decode.id, '').replace(',', '');
                    let tp = all_users[req.body.company_id][fid];
                    newconv[0].title = tp.firstname + ' ' + tp.lastname;
                    newconv[0].fnln = tp.fnln;
                }
    
                return res.status(200).send({ status: true, message: 'Conversation added successfully.', conversation: newconv[0] });
            }).catch((err) => {
                return res.status(400).send({ error: 'Find error', message: err });
            });
    
        } catch (e) {
            console.log(190, e);
            return res.status(400).send({ error: 'Create conversation Error', message: e });
        }
    });
    
    /**
     * Update conversation except participants
     * participants will update separately
     * 
     */
    fastify.post('/update_room', async (req, res) => {
        // console.log(112, req.body);
        const { errors, isValid } = validateCreateRoomInput(req.body);
        if (!isValid) {
            return res.status(400).send({ error: 'Validation error', message: errors });
        }
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            if (req.body.old_participants && !Array.isArray(req.body.old_participants)) {
                return res.status(400).send({ error: 'Validation error', message: "Old Participants are not array." });
            }
            if (!isUuid(req.body.conversation_id)) {
                return res.status(400).send({ error: 'Validation error', message: 'Conversation id missing.' });
            }
    
            if (req.body.group && req.body.group === 'yes') {
                var httc = await has_this_title_conversation(req.body.title);
                if (httc.status === true && httc.conversation && httc.conversation.conversation_id.toString() !== req.body.conversation_id) {
                    return res.status(400).send({ error: 'Duplicate error', message: 'Conversation title already exists.' });
                }
    
                if (req.body.participants_guest && Array.isArray(req.body.participants_guest) && req.body.participants_guest.length > 0) {
                    req.body.user_id = req.body.created_by;
                    req.body.emails = req.body.participants_guest;
                    req.body.name = decode.firstname + " " + decode.lastname;
                    req.body.company_name = decode.company_name;
                    req.body.role = "Guest";
                    req.body.convid = req.body.conversation_id;
                    for (let i = 0; i < req.body.participants_guest.length; i++) {
                        // let code = 123456;
                        let code = Math.floor(100000 + Math.random() * 900000);
                        req.body.participants_guest[i].code = code;
                    }
                    try {
                        var company_limit = await check_company_limit(req.body.company_id, req.body.participants_guest.length);
                        if (!company_limit) {
                            console.log('This company users added limit exceeded.');
                            return res.status(400).send({ error: 'Error', message: 'This company users added limit exceeded.' });
                        }
                        var result = await add_user(req.body.participants_guest, req);
                        for (let i = 0; i < result.length; i++) {
                            self_conversation_create_if_not_found(result[i]);
                            req.body.participants.push(result[i].id.toString());
                        }
                    }
                    catch (e) {
                        console.log(260, e);
                        return res.status(400).send({ error: 'Error', message: e });
                    }
                    // try{
                    //     var result = await create_teammate(req);
                    //     // req.body.participants_guest = result.teammate_uid;
                    // }
                    // catch(e){
                    //     console.log(51, e);
                    //     return res.status(400).send({ error: 'Error', message: e });
                    // }
                }
            }
    
            // if(Array.isArray(req.body.participants_guest) && req.body.participants_guest.length > 0 && isUuid(req.body.participants_guest[0].toString())){
            //     for(let i=0; i<req.body.participants_guest.length; i++)
            //         req.body.participants_guest[i] = req.body.participants_guest[i].toString();
            // }
            // else req.body.participants_guest = [];
            req.body.participants_guest = Array.isArray(req.body.participants_guest) ? req.body.participants_guest : [];
            let pg = [];
            for (let i = 0; i < req.body.participants_guest.length; i++) {
                pg.push(JSON.stringify(req.body.participants_guest[i]));
            }
            if (pg.length > 0)
                req.body.participants_guest = pg;
    
            let team_admin = [];
            // if(req.body.team_id)
            //     team_admin = get_team_admin(req.body.company_id, req.body.team_id);
            req.body.participants = [...new Set([...req.body.participants, ...team_admin])];
    
            var conv = {
                created_by: req.body.created_by,
                sender_id: req.body.created_by,
                participants: req.body.participants,
                participants_admin: [...new Set([...req.body.participants_admin, ...team_admin])],
                participants_guest: req.body.participants_guest,
                title: req.body.title,
                conv_img: req.body.conv_img ? req.body.conv_img : 'feelix.jpg',
                privacy: req.body.privacy ? req.body.privacy : 'public',
                team_id: req.body.team_id ? req.body.team_id : '',
                b_unit_id: req.body.b_unit_id ? req.body.b_unit_id : '',
                tag_list: Array.isArray(req.body.tag_list) ? req.body.tag_list : []
            };
            var pardiff = [];
            for (let i = 0; i < req.body.participants.length; i++) {
                if (req.body.old_participants.indexOf(req.body.participants[i]) === -1)
                    pardiff.push(req.body.participants[i]);
            }
            // console.log(318, pardiff);
            var needfileupdate = req.body.old_participants.length !== req.body.participants.length ? true : pardiff.length > 0 ? true : false;
            // console.log(319, needfileupdate);
            var query = { conversation_id: req.body.conversation_id, company_id: req.body.company_id };
            Conversation.updateOne(query, conv).then(async function () {
                conv.conversation_id = req.body.conversation_id;
                conv.company_id = req.body.company_id;
                var update_conv = await get_conversation({ conversation_id: req.body.conversation_id, user_id: decode.id });
                save_notification({
                    company_id: req.body.company_id,
                    receiver_id: req.body.participants,
                    title: req.body.title + " conversation updated.",
                    type: "conversation",
                    body: JSON.stringify(conv),
                    user_id: decode.id,
                    user_name: decode.firstname + ' ' + decode.lastname,
                    user_img: decode.img
                }, req);
    
                update_conv[0].participants_admin = update_conv[0].participants_admin === null ? [] : update_conv[0].participants_admin;
                // console.log(332, update_conv[0].participants_guest);
                // console.log(333, Array.isArray(update_conv[0].participants_guest));
                update_conv[0].participants_guest = Array.isArray(update_conv[0].participants_guest) ? update_conv[0].participants_guest : [];
                // console.log(update_conv[0].participants_guest);
                try {
                    var pg = [];
                    for (let i = 0; i < update_conv[0].participants_guest.length; i++) {
                        update_conv[0].participants_guest[i] = JSON.parse(update_conv[0].participants_guest[i]);
                        // if(update_conv[0].participants_guest[i].email !== req.body.email)
                        update_conv[0].participants_guest[i].role = 'Guest';
                        pg.push(update_conv[0].participants_guest[i]);
                    }
                    update_conv[0].participants_guest = pg;
                } catch (e) {
                    // console.log("732 participants guest only email ",e);
                    update_conv[0].participants_guest = update_conv[0].participants_guest;
                }
    
                // console.log("267 file table need to be update ", needfileupdate);
                var files = [];
                var queries = [];
                if (needfileupdate)
                    files = await get_conversation_all_files(req.body.conversation_id);
    
                req.body.name = decode.firstname + " " + decode.lastname;
                req.body.company_name = decode.company_name;
                // check_send_invite_email(req);
                // (req.body.participants).forEach(async function(v, k) {
                for (let k = 0; k < req.body.participants.length; k++) {
                    if ((req.body.old_participants).indexOf(req.body.participants[k]) == -1) { // add new member
                        if (files.length > 0) {
                            var fileids = [];
                            for (let i = 0; i < files.length; i++) {
                                fileids.push(files[i]._id);
                                // queries.push(models.instance.File.update({company_id: files[i].company_id, id: files[i].id}, {participants: { $add: [req.body.participants[k]]}, other_user: { $add: [req.body.participants[k]]} }, { return_query: true }));
    
                                // console.log(450, {return_query: true, tag_id: files[i].tag_list,  company_id: files[i].company_id.toString(), val: 1, participants: [req.body.participants[k]], conversation_id: files[i].conversation_id.toString(),  msg_id: files[i].msg_id.toString(), user_id: files[i].user_id.toString(), queries: [], file_ids: [],  recursive: false });
                                let tag_return_query = await update_tag_count({
                                    // return_query: true,
                                    tag_id: files[i].tag_list,
                                    company_id: files[i].company_id.toString(),
                                    val: 1,
                                    participants: [req.body.participants[k]],
                                    conversation_id: files[i].conversation_id.toString(),
                                    msg_id: files[i].msg_id.toString(),
                                    user_id: files[i].user_id.toString(),
                                    queries: [],
                                    file_ids: [],
                                    recursive: false
                                });
                                // if(tag_return_query && tag_return_query.length > 0)
                                //     queries = [...queries, ...tag_return_query];
                            }
                            if (fileids.length > 0) {
                                let r = await File.updateMany({ _id: { $in: fileids } }, { $push: { participants: req.body.participants[k] } })
                                console.log(r.matchedCount, r.modifiedCount);
                            }
                        }
                        // if any old perticipants add then temp user remove
                        update_conv[0].temp_user = (update_conv[0].temp_user).filter(x => { return x !== req.body.participants[k] })
                        Conversation.updateOne(query, { $pull: { temp_user: req.body.participants[k] } });
    
                        xmpp_send_server(req.body.participants[k], 'new_room', update_conv[0], req);
                        znConNodeUpdate({ uid: req.body.participants[k], type: 'new_room', conv: update_conv[0] });
                    }
                    else // update room
                        xmpp_send_server(req.body.participants[k], 'update_room', update_conv[0], req);
                    znConNodeUpdate({ uid: req.body.participants[k], type: 'update_room', conv: update_conv[0] });
                }
    
                // remove participants
                (req.body.old_participants).forEach(async function (v, k) {
                    if ((req.body.participants).indexOf(v) == -1) {
                        let this_user = await get_user_obj(v);
                        // console.log(407, this_user);
                        // if(this_user.role === 'Guest'){
                        //     models.instance.Conversation.find({ participants: { $contains: this_user.id.toString() } }, {raw: true, allow_filtering: true}, function(ce, cc){
                        //         console.log("453 ====================== ",cc, cc.length);
                        //         if(cc.length == 0){
                        //             xmpp_send_server(v, 'logout_from_all', { device_id: [] });
                        //         }
                        //     });
                        //     // if(process.env.TEST_CHECK === 'yes'){
                        //         req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                        //         req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length-1) : req.headers.origin;
                        //         req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length-1) : req.headers.origin;
                        //         var emaildata = {
                        //             to: this_user.email,
                        //             // bcc: 'mahfuzak08@gmail.com',
                        //             subject: decode.firstname +' has cancelled your invitation to join a Workfreeli room',
                        //             text: decode.firstname +' has cancelled your invitation to join a Workfreeli room',
                        //             html: 'Hi ' + this_user.firstname + ',<br><br>' + decode.firstname + ' has cancelled your guest invitation to "'+ req.body.title +'" room in Workfreeli.<br><br>However, you can always sign-up for a new business account for yourself and your team. Workfreeli is a team collaboration platform that simplifies the way we work. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br><a clicktracking=off href="'+ req.headers.origin +'signup" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign up for a new business account</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                        //         }
    
                        //         // console.log(emaildata);
                        //         try {
                        //             sg_email.send(emaildata, (result) => {
                        //                 if (result.msg == 'success') {
                        //                     // res.status(200).send({ status: true, message: 'Workfreeli password reset code send successfully' });
                        //                 } else {
                        //                     console.log('create_room ', result);
                        //                     // res.status(200).send({ status: false, message: 'Workfreeli password reset but email not send', error: result });
                        //                 }
                        //             });
                        //         } catch (e) {
                        //             console.log(e);
                        //             // res.status(200).send({ status: false, message: 'Workfreeli password reset but email not send', error: e });
                        //         }
    
                        //     // }
                        // }
                        // if any perticipants remove, he/she add into temp user
                        update_conv[0].temp_user = [...v, ...update_conv[0].temp_user];
                        Conversation.updateOne(query, { $push: { temp_user: v } });
    
                        xmpp_send_server(v, 'kick_out', update_conv[0], req);
                        znConNodeUpdate({ uid: v, type: 'kick_out', conv: update_conv[0] });
    
                        if (files && files.length > 0) {
                            for (let i = 0; i < files.length; i++) {
                                if (files[i].user_id.toString() !== v) {
                                    let tag_return_query = await update_tag_count({
                                        // return_query: true,
                                        tag_id: files[i].tag_list,
                                        company_id: files[i].company_id.toString(),
                                        val: -1,
                                        participants: [v],
                                        conversation_id: files[i].conversation_id.toString(),
                                        msg_id: files[i].msg_id.toString(),
                                        user_id: files[i].user_id.toString(),
                                        queries: [],
                                        file_ids: [],
                                        recursive: false
                                    });
                                    let r = await File.updateOne({ company_id: files[i].company_id, id: files[i].id }, { $pull: { participants: v, other_user: v } });
                                    console.log(r.matchedCount, r.modifiedCount);
                                }
                            }
                        }
                    }
                });
                return res.status(200).send({ status: true, message: 'Conversation update successfully.', conversation: update_conv[0] });
            }).catch((err) => {
                return res.status(400).send({ error: 'Update error', message: err });
            });
        } catch (e) {
            return res.status(400).send({ error: 'Update conversation Error', message: e });
        }
    });
    fastify.post('/update_system_room', async (req, res) => {
        console.log(497, req.body);
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            // console.log(501, decode.company_id);
            var conv ={}
            if(req.body.title === 'Everyone Channel'){
                var conv = {
                    system_conversation_active: req.body.is_active,
                    system_conversatio_send_sms:req.body.team_system_conversation_off_sms,
                    participants_admin:req.body.participants_admin
                };
            }else{
                var conv = {
                    system_conversation_active: req.body.is_active,
                    system_conversatio_send_sms:req.body.team_system_conversation_off_sms
                   // participants_admin:re
                };
            }
            
            var con_id = "";
            //var participants = [];
            // console.log(507, conv);
            if (req.body.title === 'Everyone Channel') {
                console.log(509, con_id);
                let con_e = await Conversation.findOne({ company_id: decode.company_id, title: 'Everyone Channel' })
                con_id = con_e.conversation_id;
                // console.log(512, con_id);
                // participants = con_e.participants;
            } else {
                console.log(515, decode.company_id + 'team_' + req.body.team_id);
                let con_e = await Conversation.findOne({ company_id: decode.company_id, system_conversation: 'team_' + req.body.team_id })
                con_id = con_e.conversation_id;
                // console.log(513, con_id);
                // participants = con_e.participants;
    
            }
            var query = { conversation_id: con_id, company_id: decode.company_id };
            Conversation.updateOne(query, conv).then(async function () {
                var update_conv = await get_conversation({ conversation_id: con_id, user_id: decode.id });
                save_notification({
                    company_id: decode.company_id,
                    receiver_id: req.body.participants,
                    title: req.body.title + " conversation updated.",
                    type: "conversation",
                    body: JSON.stringify(conv),
                    user_id: decode.id,
                    user_name: decode.firstname + ' ' + decode.lastname,
                    user_img: decode.img
                }, req);
                for (let k = 0; k < req.body.participants.length; k++) {
                    xmpp_send_server(req.body.participants[k], 'update_room', update_conv[0], req);
                    znConNodeUpdate({ uid: req.body.participants[k], type: 'update_room', conv: update_conv[0] });
                }
    
                return res.status(200).send({ status: true, message: 'Conversation update successfully.', conversation: update_conv[0] });
            }).catch((err) => {
                return res.status(400).send({ error: 'Update error', message: err });
            });
        } catch (e) {
            return res.status(400).send({ error: 'Update conversation Error', message: e });
        }
    });
    
    fastify.get('/get_room_create_data', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            try {
                if (decode.role === 'Admin') {
                    var all_users = await get_user({ company_id: decode.company_id });
                    return res.status(200).send({
                        success: true,
                        all_users,
                        all_teams: await get_team(decode, all_users),
                        all_bunits: await get_bunit(decode.company_id, all_users)
                    });
                }
                else {
                    // var all_users = await get_user({company_id: decode.company_id});
                    let teams = await get_team(decode);
                    var teammate = [];
                    let tmids = [];
                    for (let i = 0; i < teams.length; i++) {
                        // console.log(455, teams[i].participants_details);
                        for (let j = 0; j < teams[i].participants_details.length; j++) {
                            // console.log(457, teams[i].participants_details[j]);
                            try {
                                if (tmids.indexOf(teams[i].participants_details[j].id) === -1) {
                                    teammate.push(teams[i].participants_details[j]);
                                    tmids.push(teams[i].participants_details[j].id);
                                }
                            } catch (e) {
                                console.log(569, e);
                            }
                        }
                    }
                    return res.status(200).send({
                        success: true,
                        all_users: teammate,
                        all_teams: teams,
                        all_bunits: await get_bunit(decode.company_id, all_users)
                    });
                }
            } catch (e) {
                console.log(581, e)
                return res.status(400).send({ error: 'Get room create data Error 2', message: e });
            }
        } else {
            console.log(585)
            return res.status(400).send({ error: 'Get room create data Error 3', message: "Token missing." });
        }
    });
    
    
    fastify.post('/leave_room', async (req, res) => {
        // const token = extractToken(req);
        // if (token) {
        //     let decode = jwt.verify(token, process.env.SECRET);
        //     try {
        //         if (!isUuid(req.body.conversation_id)) throw { error: 'Validation error', message: "Conversation id invalid" };
    
        //         var query = { conversation_id: models.uuidFromString(req.body.conversation_id), company_id: models.timeuuidFromString(decode.company_id) };
        //         models.instance.Conversation.update(query, { participants: { $remove: [decode.id.toString()] }, temp_user: { $remove: [decode.id.toString()] } }, update_if_exists, function(err) {
        //             if (err) return res.status(400).send({ error: 'Leave room Error 1', message: err });
    
        //             // models.instance.Conversation.findOne(query, { raw: true }, function(err, conv) {
        //             //     if (err) return res.status(400).send({ error: 'Leave room Error 11', message: err });
    
        //             //     var uobj = get_user_obj(decode.id);
        //             //     save_notification({
        //             //         company_id: conv.company_id,
        //             //         receiver_id: conv.participants,
        //             //         title: uobj.firstname + " " + uobj.lastname + " leave from '" + conv.title + "' conversation.",
        //             //         type: "conversation",
        //             //         body: JSON.stringify(conv),
        //             //         user_id: decode.id,
        //             //         user_name: uobj.firstname + " " + uobj.lastname,
        //             //         user_img: uobj.img
        //             //     }, req);
    
        //                 // (conv.participants).forEach(function(v, k) {
        //                 //     xmpp_send_server(v, 'leave_room', { conversation_id: req.body.conversation_id, company_id: decode.company_id, participants: conv.participants }, req);
        //                 // });
        //                 xmpp_send_server(decode.id, 'leave_room', { conversation_id: req.body.conversation_id, company_id: decode.company_id }, req);
    
        //                 return res.status(200).send({ success: true, result: { conversation_id: req.body.conversation_id, company_id: decode.company_id } });
        //             // });
        //         });
        //     } catch (e) {
        //         return res.status(400).send({ error: 'Leave room Error 2', message: e });
        //     }
        // } else {
        //     return res.status(400).send({ error: 'Leave room Error 3', message: "Token missing." });
        // }
    });
    
    fastify.post('/delete_room', async (req, res) => {
        // const token = extractToken(req);
        // if (token) {
        //     let decode = jwt.verify(token, process.env.SECRET);
        //     try {
        //         if (!isUuid(req.body.conversation_id)) throw { error: 'Validation error', message: "Conversation id invalid" };
    
        //         var query = { conversation_id: models.uuidFromString(req.body.conversation_id), company_id: models.timeuuidFromString(decode.company_id) };
        //         models.instance.Conversation.findOne(query, { raw: true }, function(e, conv) {
        //             if (e) return res.status(400).send({ error: 'Delete room Error 0', message: e });
        //             if (conv) {
        //                 if (conv.created_by.toString() != decode.id.toString())
        //                     return res.status(400).send({ error: 'Delete room Error 11', message: "You are not create this room." });
        //                 models.instance.Conversation.update(query, { status: 'inactive' }, update_if_exists, function(err) {
        //                     if (err) return res.status(400).send({ error: 'Delete room Error 1', message: err });
    
        //                     io.emit("delete_room", { conversation_id: req.body.conversation_id, company_id: decode.company_id });
        //                     return res.status(200).send({ success: true, result: { conversation_id: req.body.conversation_id, company_id: decode.company_id } });
        //                 });
        //             } else {
        //                 return res.status(400).send({ error: 'Delete room Error 12', message: 'No conversation found.' });
        //             }
        //         });
    
        //     } catch (e) {
        //         return res.status(400).send({ error: 'Delete room Error 2', message: e });
        //     }
        // } else {
        //     return res.status(400).send({ error: 'Delete room Error 3', message: "Token missing." });
        // }
    });
    
    /**
     * This route user for archive/ un-archive a room
     * conversation_id
     * archive = yes/ no
     */
    fastify.patch('/room_archive', async (req, res) => {
        console.log(456, req.query);
        if (!isUuid(req.query.conversation_id)) {
            return res.status(400).send({ error: 'Validation error', message: "Conversation ID missing..." });
        }
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            var query = { conversation_id: req.query.conversation_id };
            Conversation.find(query).then(function (c) {
                if (c.length == 1) {
                    if (c[0].participants_admin.indexOf(decode.id) > -1) {
                        if (c[0].group === 'yes') {
                            Conversation.updateOne({ conversation_id: c[0].conversation_id, company_id: c[0].company_id }, { archive: req.query.archive }).then(function () {
                                var str_msg_body = req.query.archive === 'yes' ? 'This room has been archived ' : 'This room has been restored ';
                                str_msg_body += 'on ' + moment().format("MMM Do, YYYY");
                                var body = CryptoJS.AES.encrypt(JSON.stringify(str_msg_body), process.env.CRYPTO_SECRET).toString();
                                var arg = {
                                    conversation_id: req.query.conversation_id,
                                    company_id: decode.company_id,
                                    sender: decode.id,
                                    sender_img: decode.img,
                                    msg_body: body,
                                    participants: c[0].participants,
                                    is_reply_msg: 'no',
                                    msg_type: 'text log_notify',
                                    CLIENT_BASE_URL: process.env.API_SERVER_IP
                                }
                                try {
                                    set_message(arg, function (result) {
                                        c[0].archive = req.query.archive;
                                        c[0].participants.forEach(function (v, k) {
                                            xmpp_send_server(v, 'new_message', result.msg, req);
                                            send_msg_firebase(v, 'new_message', result.msg, req);
                                            xmpp_send_server(v, 'room_archive', req.query, req);
                                            znConNodeUpdate({ uid: v, type: 'room_archive', conv: c[0], archive: req.query.archive });
                                            console.log(v);
                                        });
                                    });
                                    var str_noti_body = "'" + c[0].title + "'";
                                    str_noti_body += req.query.archive === 'yes' ? ' room has been archived' : ' room has been restored';
                                    str_noti_body += ' by ' + decode.firstname + ' ' + decode.lastname;
                                    str_noti_body += ' on ' + moment().format("MMM Do, YYYY") + ' at ' + moment().format("LT");
                                    save_notification({
                                        company_id: decode.company_id,
                                        receiver_id: c[0].participants,
                                        title: str_noti_body,
                                        type: "conversation",
                                        body: JSON.stringify(arg),
                                        user_id: decode.id,
                                        user_name: decode.firstname + ' ' + decode.lastname,
                                        user_img: 'img.png'
                                    }, req);
                                } catch (e) {
                                    console.log(e);
                                }
    
                                return res.status(200).send({ success: true, data: req.query });
                            }).catch((err) => {
                                return res.status(400).send({ error: 'Update error', message: err });
                            })
                        }
                        else {
                            return res.status(400).send({ error: 'Error', message: 'This conversation is not a room' });
                        }
                    }
                    else {
                        return res.status(400).send({ error: 'Error', message: 'You are not an admin of this conversation.' });
                    }
                }
                else {
                    return res.status(400).send({ error: 'Error', message: 'No conversation found.' });
                }
            }).catch((e) => {
                return res.status(400).send({ error: 'Find error', message: e });
            });
        } catch (e) {
            return res.status(400).send({ error: 'Update conversation Error', message: e });
        }
    });
    
    /**
     * This route user for close/ re-open a room
     * conversation_id
     * close_room = yes/ no
     */
    fastify.patch('/close_room', async (req, res) => {
        console.log(502, req.query);
        if (!isUuid(req.query.conversation_id)) {
            return res.status(400).send({ error: 'Validation error', message: "Conversation ID missing..." });
        }
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            var query = { conversation_id: req.query.conversation_id };
            Conversation.find(query).then(function (c) {
                if (c.length == 1) {
                    if (c[0].participants_admin.indexOf(decode.id) > -1) {
                        if (c[0].group === 'yes') {
                            Conversation.updateOne({ conversation_id: c[0].conversation_id, company_id: c[0].company_id }, { $set: { close_for: req.query.close_room === 'yes' ? 'yes' : 'no' } }).then(function () {
    
                                var str_msg_body = req.query.close_room === 'yes' ? 'This room has been locked ' : 'This room has been unlocked ';//May 22nd, 2022
                                str_msg_body += 'on ' + moment().format("MMM Do, YYYY");
                                var body = CryptoJS.AES.encrypt(JSON.stringify(str_msg_body), process.env.CRYPTO_SECRET).toString();
                                var arg = {
                                    conversation_id: req.query.conversation_id,
                                    company_id: decode.company_id,
                                    sender: decode.id,
                                    sender_img: decode.img,
                                    msg_body: body,
                                    participants: c[0].participants,
                                    is_reply_msg: 'no',
                                    msg_type: 'text log_notify',
                                    CLIENT_BASE_URL: process.env.API_SERVER_IP
                                }
                                try {
                                    set_message(arg, function (result) {
                                        c[0].participants.forEach(function (v, k) {
                                            xmpp_send_server(v, 'new_message', result.msg, req);
                                            send_msg_firebase(v, 'new_message', result.msg, req);
                                            xmpp_send_server(v, 'close_room', req.query, req);
                                            znConNodeUpdate({ uid: v, type: 'close_room', conv: c[0], close_room: req.query.close_room });
                                        });
                                    });
                                    var str_noti_body = "'" + c[0].title + "'";
                                    str_noti_body += req.query.close_room === 'yes' ? ' room has been locked' : ' room has been unlocked';
                                    str_noti_body += ' by ' + decode.firstname + ' ' + decode.lastname;
                                    str_noti_body += ' on ' + moment().format("MMM Do, YYYY") + ' at ' + moment().format("LT");
                                    save_notification({
                                        company_id: decode.company_id,
                                        receiver_id: c[0].participants,
                                        title: str_noti_body,
                                        type: "conversation",
                                        body: JSON.stringify(arg),
                                        user_id: decode.id,
                                        user_name: decode.firstname + ' ' + decode.lastname,
                                        user_img: 'img.png'
                                    }, req);
                                } catch (e) {
                                    console.log(e);
                                }
    
                                return res.status(200).send({ success: true, data: req.query });
                            }).catch((e) => {
    
                                console.log(e);
                                return res.status(400).send({ error: 'Update error', message: e });
    
                            });
                        }
                        else {
                            return res.status(400).send({ error: 'Error', message: 'This conversation is not a room' });
                        }
                    }
                    else {
                        return res.status(400).send({ error: 'Error', message: 'You are not an admin of this conversation.' });
                    }
                }
                else {
                    return res.status(400).send({ error: 'Error', message: 'No conversation found.' });
                }
            }).catch((e) => {
                return res.status(400).send({ error: 'Find error', message: e });
            });
        } catch (e) {
            return res.status(400).send({ error: 'Update conversation Error', message: e });
        }
    });
    
    /**
     * This route user for close/ re-open a room
     * conversation_id
     * close_room = yes/ no
     */
    fastify.get('/archive_count', async (req, res) => {
        console.log(631);
        try {
            const token = extractToken(req);
            var decode = await token_verify_decode(token);
            Conversation.countDocuments({ participants: { $in: [decode.id.toString()] }, archive: 'yes' }, function (e, count) {
                if (e) return res.status(400).send({ error: 'Find error', message: e });
    
                return res.status(200).send({ success: true, archive: count });
            });
        } catch (e) {
            return res.status(400).send({ error: 'archive_count Error', message: e });
        }
    });
    
    /**
     * This api use for "Update conversation admin" from Supper Admin panel
     * conversation_id
     * user_id = as admin id
     * 
     */
    fastify.post('/update_admin', async (req, res) => {
        console.log(884, req.body);
        // try {
        //     const token = extractToken(req);
        //     let decode = jwt.verify(token, process.env.SECRET);
        //     if (! isUuid(req.body.user_id)) {
        //         return res.status(400).send({ error: 'Validation error', message: "Admin id is missing." });
        //     }
        //     if (! isUuid(req.body.conversation_id)) {
        //         return res.status(400).send({ error: 'Validation error', message: 'Conversation id missing.' });
        //     }
    
        //     var conv = { participants: { '$add': [req.body.user_id]}, participants_admin: { '$add': [req.body.user_id]} };
        //     var query = { conversation_id: models.uuidFromString(req.body.conversation_id), company_id: models.timeuuidFromString(decode.company_id) };
        //     models.instance.Conversation.update(query, conv, update_if_exists, function(err) {
        //         if (err) return res.status(400).send({ error: 'Update error', message: err });
        //         return res.status(200).send({ status: true, message: 'Conversation update successfully.' });
        //     });
        // } catch (e) {
        //     return res.status(400).send({ error: 'Update conversation Error', message: e });
        // }
    });
}

module.exports = routes;