const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
var ObjectId = require('mongoose').Types.ObjectId;
const _ = require('lodash');
const isUuid = require('uuid-validate');
var CryptoJS = require("crypto-js");
const Team = require('../../mongo-models/team');
const Task = require('../../mongo-models/task');
const Task_discussion = require('../../mongo-models/task_discussion');
const File = require('../../mongo-models/file');
// const TaskFiles = require('../../mongo-models/task_files');
const Checklist_item = require('../../mongo-models/checklist_item');
const Review = require('../../mongo-models/task_review');
const Messages = require('../../mongo-models/message');
const Users = require('../../mongo-models/user');
const { isValidJson, validateJSON } = require('../../v2_utils/common')
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { get_unread_tasks, get_project, set_project, get_keywords, set_keywords, set_task_into_keyword, set_task_into_review, remove_task_review, item_to_checklist } = require('../../v2_utils/task_manage');
var { save_notification, get_notification, get_notification_count } = require('../../v2_utils/notification');
var { set_message, get_message, update_conversation_for_last_msg, fnln } = require('../../v2_utils/message');
var { xmpp_send_server, send_msg_firebase } = require('../../v2_utils/voip_util');
// const task_discussion = require('../../mongo-models/task_discussion');
const Notification = require('../../mongo-models/notification');
const { v4: uuidv4 } = require('uuid');
var Tag = require('../../mongo-models/tag');
const TaskCustom = require('../../mongo-models/task_custom');
var { set_message, get_conversation } = require('../../v2_utils/message');


// process.env.CRYPTO_SECRET = 'D1583ED51EEB8E58F2D3317F4839A';

async function get_user_obj(id) {
    return new Promise((resolve, reject) => {
        for (let value of Object.entries(all_users)) {
            for (let v of value) {
                if (typeof v === 'object' && v.hasOwnProperty(id)) {
                    return resolve(v[id]);
                }
            }
        }
        if (cache_user) {
            return resolve({
                id: id,
                firstname: "",
                lastname: "",
                fullname: "",
                img: "",
                dept: "",
                designation: "",
                email: "",
                phone: "",
                access: [],
                company_id: "",
                company_name: "",
                conference_id: "",
                role: "",
                email_otp: "",
                is_active: 0,
                mute_all: "",
                mute: "",
                login_total: 0,
                fnln: "",
                createdat: ""
            });
        };
        // console.log('get_user_obj',id)
        Users.findOne({ id: String(id) }, function (error, user) {
            if (user) {
                return resolve({
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    fullname: user.firstname + ' ' + user.lastname,
                    img: process.env.FILE_SERVER + user.img,
                    dept: user.dept,
                    designation: user.designation,
                    email: user.email,
                    phone: Array.isArray(user.phone) ? user.phone.join("") : "",
                    access: user.access,
                    company_id: user.company_id,
                    company_name: user.company,
                    conference_id: user.conference_id,
                    role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
                    email_otp: user.email_otp,
                    is_active: user.is_active,
                    mute_all: user.mute_all,
                    mute: user.mute ? JSON.parse(user.mute) : '',
                    login_total: user.login_total,
                    fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
                    device: user.device ? user.device : [],
                    short_id: user.short_id,
                    createdat: user.createdat ? user.createdat : new Date(),
                    fcm_id: user.fcm_id
                });

            } else {
                return resolve({
                    id: id,
                    firstname: "",
                    lastname: "",
                    fullname: "",
                    img: "",
                    dept: "",
                    designation: "",
                    email: "",
                    phone: "",
                    access: [],
                    company_id: "",
                    company_name: "",
                    conference_id: "",
                    role: "",
                    email_otp: "",
                    is_active: 0,
                    mute_all: "",
                    mute: "",
                    login_total: 0,
                    fnln: "",
                    createdat: ""
                });
            }

        });

    })
    // var result = {};
}

// router.get('/insert_status', async (req, res) => {
    // const token = extractToken(req);
    // let decode = await token_verify_decode(token);

    

    // return res.status(200).json({ success: true, reply_msgs_old });
// });


/**
 * Create new task
 * and update full task document 
 * 
 */
router.post('/convert', async (req, res) => {
    var reply_msgs_old = [];
    if (req.body.msg_id) {
        let reply_msgs = await Messages.find({ is_reply_msg: 'yes', reply_for_msgid: req.body.msg_id });
        if (reply_msgs.length) {

            reply_msgs.forEach((v) => {
                let item = new Messages(v)
                item["task_id"] = "";
                item.msg_body = CryptoJS.AES.encrypt(item.msg_body, process.env.CRYPTO_SECRET).toString();
                reply_msgs_old.push(item);
            });
        }

    }

    return res.status(200).json({ success: true, reply_msgs_old });
});
router.post('/save', async (req, res) => {
    console.log(149, req.body);
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    // let check_body_data = isValidJson(JSON.stringify(req.body));
    // const requiredKeys = ["task_title"];
    // let missingKeys = validateJSON(req.body, requiredKeys);
    // console.log(155, req.body);
    // if (missingKeys.length === 0) {
        // if (check_body_data) {
            // get project id
            if (!req.body.project_id) {
                let p = await get_project({ project_title: 'Not Defined', company_id: decode.company_id });
                if (p.success) {
                    if (p.projects && p.projects.length == 1) {
                        var project_id = p.projects[0]._id;
                        var project_title = p.projects[0].project_title;
                    }
                    else {
                        let p = await set_project({ project_title: 'Not Defined', created_by: decode.id, company_id: decode.company_id });
                        if (p.success) {
                            var project_id = p.project._id; 
                            var project_title = 'Not Defined';
                        }
                    }
                }
                else {
                    return res.status(400).json({ success: false, error: `The get_project api is failed` });
                    // delete data.project_title;
                    // delete data.project_id;
                }
            }

            if (Array.isArray(req.body)) {
                var newTasks = []
                for (let t = 0; t < req.body.length; t++) {
                    req.body[t].view_status = [decode.id];
                    if (Array.isArray(req.body[t].assign_to) && req.body[t].assign_to.length > 0) {
                        req.body[t].view_status = [...req.body[t].view_status, ...req.body[t].assign_to];
                    }
                    let data = {
                        ...req.body[t],
                        created_by: decode.id,
                        company_id: decode.company_id,
                        project_id: project_id ? project_id : "",
                        project_title: project_title ? project_title : "",
                        flag : [],
                        last_updated_at: new Date()
                    };

                    try {
                        if (data.temp_id && data.temp_id !== "") {
                            data._id = new ObjectId(data.temp_id);
                            delete data.temp_id;
                        }
                        if (!Array.isArray(req.body[t].participants)) req.body[t].participants = [decode.id];

                        var msg_id = uuidv4();
                        data.msg_id = msg_id;
                        let newTask = new Task(data);

                        await newTask.save();
                        if (req.body[t].key_words) {
                            if (Array.isArray(req.body[t].key_words) && req.body[t].key_words.length > 0) {
                                for (let i = 0; i < req.body[t].key_words.length; i++) {
                                    let k = await set_keywords({ keywords_title: req.body[t].key_words[i], created_by: decode.id, company_id: decode.company_id });
                                    if (k.success)
                                        console.log(`${k.task_keywords.keywords_title} ${k.msg !== 'Already exists' ? 'save successfully for future use.' : 'old keywords'}`);
                                    else {
                                        console.log(`${k.task_keywords.keywords_title} not saved. Error ${k}`);
                                    }
                                }
                            }
                            await set_task_into_keyword({ task_keywords: req.body[t].key_words, task_id: req.body[t]._id });
                        }
                        let tasks = newTask.toObject();
                        tasks.progress = tasks.progress ? tasks.progress : 0;
                        tasks.assign_to_details = [];
                        tasks.observers_details = [];
                        tasks.participants_details = [];
                        tasks.created_by_details = {};
                        tasks.flag = Array.isArray(tasks.flag) ? tasks.flag : [];
                        tasks.hour_breakdown = Array.isArray(tasks.hour_breakdown) ? tasks.hour_breakdown : [];
                        tasks.cost_breakdown = Array.isArray(tasks.cost_breakdown) ? tasks.cost_breakdown : [];
                        tasks.notes = tasks.has_note && tasks.has_note.hasOwnProperty(decode.id) ? tasks.has_note[decode.id] : "";
                        if(tasks.description_by){
                            let this_user = await get_user_obj(tasks.description_by);
                            tasks.description_by = {
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            };
        
                        }
                        let this_user = await get_user_obj(tasks.created_by);
                        tasks.created_by_details = {
                            id: this_user.id,
                            email: this_user.email,
                            firstname: this_user.firstname,
                            lastname: this_user.lastname,
                            is_active: this_user.is_active,
                            img: this_user.img,
                            role: this_user.role,
                            fnln: this_user.fnln,
                            createdat: this_user.createdat
                        };
                        for (let j = 0; j < tasks.assign_to.length; j++) {
                            if (!tasks.assign_to || tasks.assign_to.length == 0) {
                                if (assign_to.indexOf(tasks.assign_to[j]) === -1)
                                    assign_to.push(tasks.assign_to[j]);
                            }
                            let this_user = await get_user_obj(tasks.assign_to[j]);
                            tasks.assign_to_details.push({
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            })
                        }
                        for (let j = 0; j < tasks.participants.length; j++) {
                            let this_user = await get_user_obj(tasks.participants[j]);
                            tasks.participants_details.push({
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            })
                        }
                        for (let j = 0; j < tasks.observers.length; j++) {
                            let this_user = await get_user_obj(tasks.observers[j]);
                            tasks.observers_details.push({
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            })
                        }
                        
                        (req.body[t].assign_to).forEach(async function(v, k){
                            xmpp_send_server(v.toString(), 'new_task', tasks, req);
                        });
                        var uobj = await get_user_obj(newTask.created_by);
                        save_notification({
                            company_id: decode.company_id,
                            receiver_id: data.participants,
                            title: `Task titled '${newTask.task_title}' created.`,
                            type: "Task",
                            body: JSON.stringify(newTask),
                            user_id: newTask.created_by,
                            user_name: uobj.firstname + ' ' + uobj.lastname,
                            user_img: uobj.img,
                            task_id: newTask._id,
                            tab: "global",
                            decode_id: decode.id
                        }, req);

                        newTasks.push(newTask);
                    } catch (error) {
                        if (error instanceof mongoose.Error.StrictModeError) {
                            console.log('Validation error:', error.message);
                        } else {
                            console.log('Unexpected error:', error.message);
                        }
                        return res.status(400).json({ success: false, error });
                    }

                }

                
                return res.status(200).json({ success: true, task: newTasks, msg: "Create Tasks Successfully." });


            } else {
                req.body.view_status = [decode.id];
                if (Array.isArray(req.body.assign_to) && req.body.assign_to.length > 0) {
                    req.body.view_status = [...req.body.view_status, ...req.body.assign_to];
                }
                let data = {
                    ...req.body,
                    created_by: decode.id,
                    company_id: decode.company_id,
                    project_id: project_id ? project_id : "",
                    project_title: project_title ? project_title : ""
                };

                try {
                    if (data.temp_id && data.temp_id !== "") {
                        data._id = new ObjectId(data.temp_id);
                        delete data.temp_id;
                    }
                    if (!Array.isArray(req.body.participants)) req.body.participants = [decode.id];

                    var msg_id = uuidv4();
                    data.msg_id = msg_id;
                    let newTask = new Task(data);

                    await newTask.save();
                    if (req.body.key_words) {
                        if (Array.isArray(req.body.key_words) && req.body.key_words.length > 0) {
                            for (let i = 0; i < req.body.key_words.length; i++) {
                                let k = await set_keywords({ keywords_title: req.body.key_words[i], created_by: decode.id, company_id: decode.company_id });
                                if (k.success)
                                    console.log(`${k.task_keywords.keywords_title} ${k.msg !== 'Already exists' ? 'save successfully for future use.' : 'old keywords'}`);
                                else {
                                    console.log(`${k.task_keywords.keywords_title} not saved. Error ${k}`);
                                }
                            }
                        }
                        await set_task_into_keyword({ task_keywords: req.body.key_words, task_id: req.body._id });
                    }
                    var uobj = await get_user_obj(newTask.created_by);
                    save_notification({
                        company_id: decode.company_id,
                        receiver_id: data.participants,
                        title: `Task titled '${newTask.task_title}' created.`,
                        type: "Task",
                        body: JSON.stringify(newTask),
                        user_id: newTask.created_by,
                        user_name: uobj.firstname + ' ' + uobj.lastname,
                        user_img: uobj.img,
                        task_id: req.body._id,
                        tab: "global",
                        decode_id: decode.id
                    }, req);

                    // if (req.body._id) {
                    //     delete data.created_by;
                    //     data.last_updated_at = new Date();
                    //     let task = await Task.findOneAndUpdate({ _id: req.body._id }, data);
                    //     data.created_by = task.created_by;
                    //     data._id = req.body._id;
                    //     task = data;
                    //     // console.log(task.created_by); 
                    //     if(req.body.key_words){
                    //         await set_task_into_keyword({ task_keywords: req.body.key_words, task_id: req.body._id });
                    //     }else{

                    //     }

                    //     if (req.body.status === 'Completed') {
                    //         let created_for = task.review ? [...task.observers, task.created_by] : [task.created_by];
                    //         await Task.findOneAndUpdate({_id: req.body._id}, {review_status: created_for});
                    //         for(let i=0; i<created_for.length; i++){
                    //             let review = set_task_into_review({
                    //                 task_id: req.body._id,
                    //                 project_id: data.project_id,
                    //                 created_by: decode.id,
                    //                 created_for: created_for[i], 
                    //                 review_title: data.task_title,
                    //                 company_id: data.company_id
                    //             });
                    //         }
                    //         // console.log(186, review);
                    //     } else {
                    //         let review = remove_task_review({ task_id: req.body._id });
                    //         // console.log(189, review);
                    //     }
                    //     if (items.length > 0)
                    //         await item_to_checklist(item_to_checklist_data);

                    //     var uobj = await get_user_obj(task.created_by);
                    //     save_notification({
                    //         company_id: decode.company_id,
                    //         receiver_id: req.body.participants,
                    //         title: `Task titled '${task.task_title}' updated`,
                    //         type: "Task",
                    //         body: JSON.stringify(task),
                    //         user_id: task.created_by,
                    //         user_name: uobj.firstname + ' ' + uobj.lastname,
                    //         user_img: uobj.img,
                    //         task_id: req.body._id,
                    //     }, req);
                    //     return res.status(200).json({ success: true, task: data, msg: "Task Update Successfully." });
                    // }
                    // {


                    // let item_to_checklist_data = {
                    //     company_id: decode.company_id,
                    //     created_by: decode.id,
                    //     task_id: req.body._id ? req.body._id : newTask._id,
                    //     task_title: data.task_title,
                    //     items
                    // }

                    // else{

                    // }
                    // if (req.body.status === 'Completed') {
                    //     let created_for = newTask.review ? [...newTask.observers, newTask.created_by] : [newTask.created_by];
                    //     await Task.findOneAndUpdate({_id: newTask._id}, {review_status: created_for});
                    //     for(let i=0; i<created_for.length; i++){
                    //         let review = set_task_into_review({
                    //             task_id: newTask._id,
                    //             project_id: data.project_id,
                    //             created_by: decode.id,
                    //             created_for: created_for[i], 
                    //             review_title: data.task_title,
                    //             company_id: data.company_id
                    //         });
                    //     }
                    //     // console.log(186, review);
                    // } else {
                    //     let review = remove_task_review({ task_id: newTask._id });
                    //     // console.log(189, review);
                    // }
                    // ============= Convert ===========
                    // if (DiscussionList.length) {
                    //     var discussion_old = [];
                    //     DiscussionList.forEach((v) => {
                    //         if (!v.task_id) {
                    //             delete v._id;
                    //             let item = new Messages(v)
                    //             item["task_id"] = newTask._id;
                    //             var bytes = CryptoJS.AES.decrypt(item.msg_body, process.env.CRYPTO_SECRET);
                    //             item.msg_body = bytes.toString(CryptoJS.enc.Utf8);
                    //             discussion_old.push(item);
                    //         }
                    //     });
                    //     let tt = await Messages.insertMany(discussion_old);
                    // }

                    // if (items.length > 0)
                    //     await item_to_checklist(item_to_checklist_data);



                    // set_message({
                    //     "msg_id": msg_id,
                    //     "conversation_id": data.conversation_id,
                    //     "company_id": data.company_id,
                    //     "sender": decode.id,
                    //     "sender_img": decode.img,
                    //     "sender_name": decode.firstname +" " + decode.lastname,
                    //     "msg_body": CryptoJS.AES.encrypt(data.task_title, process.env.CRYPTO_SECRET).toString(),
                    //     "participants": data.participants,
                    //     // "topic_type": "task",
                    //     "newtag": [],
                    //     "newtag_tag_data": [],
                    //     "created_at" : new Date(),
                    //     "secret_user": data.participants,
                    //     "taskid": data._id
                    //     // "referenceId" : msg.referenceId,
                    //     // "reference_type": msg.reference_type

                    //     // "group": props.logindata.active_conv.details.group,
                    //     // "title": props.logindata.active_conv.details.title
                    // }, (result) => {
                    //     // if (result.status) {
                    //     //     (data.participants).forEach(function (v, k) {
                    //     //         xmpp_send_server(v, 'new_message', result.msg, req);
                    //     //         if (data.sender != v) {
                    //     //             send_msg_firebase(v, 'new_message', result.msg, req);
                    //     //         }
                    //     //     });
                    //     // }
                    // });

                    return res.status(200).json({ success: true, task: newTask, msg: "Create Task Successfully." });
                    // }
                } catch (error) {
                    if (error instanceof mongoose.Error.StrictModeError) {
                        console.log('Validation error:', error.message);
                    } else {
                        console.log('Unexpected error:', error.message);
                    }
                    return res.status(400).json({ success: false, error });
                }

            }



            // let DiscussionList = [];
            // if (!req.body.start_date) req.body.start_date = new Date();
            // if (req.body.end_date === undefined || req.body.end_date === "") {
            // req.body.end_date = new Date();
            // }
            // else if(req.body.end_date !== undefined || req.body.end_date !== ""){
            //     if(req.body.due_time && req.body.due_time !== ""){
            //         let h = new Date(req.body.due_time).getHours();
            //         let m = new Date(req.body.due_time).getMinutes();
            //         req.body.end_date = new Date(new Date(req.body.end_date).setHours(h, m, 59, 999));
            //     }
            // }
            // if (req.body.newchecklistitem) {
            //     items = req.body.newchecklistitem;
            //     delete req.body.newchecklistitem;
            // }
            // if (req.body.DiscussionList) {
            //     DiscussionList = req.body.DiscussionList;
            //     delete req.body.DiscussionList;
            // }
            // var save_type;
            // if(req.body.save_type){
            //     save_type = req.body.save_type;
            //     delete req.body.save_type;
            // }

            // if(Array.isArray(req.body.observers) && req.body.observers.length>0){
            //     req.body.view_status = [...req.body.view_status, ...req.body.observers];
            // }


            // hour_breakdown = [{fdate: '', tdate: '', forecasted_hours: '', actual_hours: '', note: '', status: ''}]
            // if(Array.isArray(req.body.hour_breakdown) && req.body.hour_breakdown.length>0){
            //     for(let i=0; i<req.body.hour_breakdown.length; i++){
            //         let obj = req.body.hour_breakdown[i];

            //         if (!obj.hasOwnProperty('fdate') || !obj.hasOwnProperty('tdate') || !obj.hasOwnProperty('forecasted_hours') || !obj.hasOwnProperty('actual_hours') || !obj.hasOwnProperty('note') || !obj.hasOwnProperty('status')) {
            //             return res.status(400).json({ success: false, error: `The hour_breakdown object is incorrect required keys` });
            //         }
            //     }
            // }
            // cost_breakdown = [{cost_title: '', forecasted_cost: '', actual_cost: ''}]
            // if(Array.isArray(req.body.cost_breakdown) && req.body.cost_breakdown.length>0){
            //     for(let i=0; i<req.body.cost_breakdown.length; i++){
            //         let obj = req.body.cost_breakdown[i];

            //         if (!obj.hasOwnProperty('cost_title') || !obj.hasOwnProperty('forecasted_cost') || !obj.hasOwnProperty('actual_cost')) {
            //             return res.status(400).json({ success: false, error: `The cost_breakdown object is incorrect required keys` });
            //         }
            //         if( !obj.hasOwnProperty('_id')){
            //             obj._id = new ObjectId();
            //         }
            //     }
            // }

            // if(req.body.flag !== undefined){
            //     delete data.flag;
            //     if(req.body._id){
            //         if(req.body.flag === false) {
            //             console.log("8888888888888888888888888888888888");
            //             data.$pull = {flag: decode.id};
            //         }
            //         if(req.body.flag === true) {
            //             data.$push = {flag : decode.id};
            //         }
            //     }else{
            //         if(req.body.flag === false) data.flag = [];
            //         if(req.body.flag === true) data.flag = [decode.id];
            //     }
            // }

            // project block
            // if (req.body.project_id && req.body.project_title) {
            //     // no action required
            // }
            // else if (req.body.project_title) {
            //     let p = await set_project({ project_title: req.body.project_title, created_by: decode.id, company_id: decode.company_id });
            //     if (p.success)
            //         data.project_id = p.projects[0]._id;
            //     else {
            //         delete data.project_title;
            //         delete data.project_id;
            //     }
            // }
            // Get the Untitled Project id

            // project block end

            // keyword block

            // keyword block end


        // } else {
        //     return res.status(400).json({ success: false, msg: "Data is not valid" })
        // }
    // } else {
    //     return res.status(400).json({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}` });
    // }
});

/**
 * Create new task
 * and update full task document 
 * 
 */
router.patch('/update', async (req, res) => {
    //  console.log(407, req.body);
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let check_body_data = isValidJson(JSON.stringify(req.body));
    if (check_body_data) {
        let data = {};
        if (req.body.start_date) data.start_date = new Date(req.body.start_date);
        else data.start_date = "";

        if ("end_date" in req.body) data.end_date = req.body.end_date ? new Date(req.body.end_date) : "";
        else data.end_date = "";

        if ("due_time" in req.body)
            data.due_time = req.body.due_time ? new Date(req.body.due_time) : "";
        if ('progress' in req.body) 
            data.progress = req.body.progress;
        if (req.body.status) {
            data.status = req.body.status = req.body.status;
        }
        if ("notes" in req.body){
            data.notes = req.body.notes;
            data["$set"] = { ["has_note."+decode.id]: req.body.notes };
        }
        if (req.body.review !== undefined)
            data.review = req.body.review;
        
        if ('description' in req.body){
            data.description = req.body.description;
            data.description_by = decode.id;
            data.description_at = Date.now()
        }
        if (req.body.forecasted_cost)
            data.forecasted_cost = req.body.forecasted_cost;
        if (req.body.actual_cost)
            data.actual_cost = req.body.actual_cost;
        if (req.body.cost_variance)
            data.cost_variance = req.body.cost_variance;
        if (req.body.forecasted_hours)
            data.forecasted_hours = req.body.forecasted_hours;
        if (req.body.actual_hours)
            data.actual_hours = req.body.actual_hours;
        if (req.body.hours_variance)
            data.hours_variance = req.body.hours_variance;
        if (req.body.repeat_task)
            data.repeat_task = req.body.repeat_task;
        if (req.body.priority)
            data.priority = req.body.priority;
        if (req.body.is_archive)
            data.is_archive = req.body.is_archive;
        if (req.body.flag === true)
            data.$push = { flag: decode.id };
        if (req.body.flag === false) {
            // console.log("999999999999999999999999999999999");
            data.$pull = { flag: decode.id };
        }
        if (req.body.project_id) {
            data.project_id = req.body.project_id;
        }
        if (req.body.project_title) {
            // req.body.project_title = req.body.project_title.toLowerCase();
            let p = await get_project({ project_title: req.body.project_title, company_id: decode.company_id });
            if (p.success) {
                if (p.projects && p.projects.length == 1) {
                    data.project_id = String(p.projects[0]._id);
                    data.project_title = p.projects[0].project_title;
                }
                else {
                    let p = await set_project({ project_title: req.body.project_title, created_by: decode.id, company_id: decode.company_id });
                    if (p.success) {
                        data.project_id = String(p.project._id);
                        data.project_title = p.project.project_title;
                    }
                }
            }
            else {
                return res.status(400).json({ success: false, error: `The get_project api is failed` });
                // delete data.project_title;
                // delete data.project_id;
            }
        }
        if (Array.isArray(req.body.assign_to) && req.body.assign_to.length > 0) {
            if (!req.body.view_status) req.body.view_status = [decode.id];
            req.body.view_status = [...req.body.view_status, ...req.body.assign_to];
            data.assign_to = req.body.assign_to;
        }
        else data.assign_to = [];
        if (Array.isArray(req.body.observers) && req.body.observers.length > 0) {
            if (!req.body.view_status) req.body.view_status = [decode.id];
            req.body.view_status = [...req.body.view_status, ...req.body.observers];
            data.observers = req.body.observers;
        } else data.observers = [];

        var notification_title = `Task titled '${req.body.task_title}' ${req.body.save_type != "auto" ? req.body.save_type || '' +' ' : '' }updated`;
        if (req.body.save_type === "cost"){
            data.view_cost = [...req.body.participants.filter((v)=> v != decode.id)];
            data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
            notification_title = `Costs have been updated`;
        } 
        else if (req.body.save_type === "hour"){
            data.view_hour = [...req.body.participants.filter((v)=> v != decode.id)];
            data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
            notification_title = `Hours have been updated`;
        } 
        else if (req.body.save_type === "description"){
            data.view_description = [...req.body.participants.filter((v)=> v != decode.id)];
            data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
            notification_title = `Description has been updated`;
        }
        else if (req.body.save_type === "status"){ notification_title = `Status has been changed`;}
        else if (req.body.save_type === "duedate"){ notification_title = `Due date has been changed`;}
        else if(req.body.save_type === "startdate"){ notification_title = `Start date has been changed`;}
        else if(req.body.save_type === "progress"){notification_title = `Progress has been changed`;}
        else if(req.body.save_type === "assignee") {notification_title = `Assignee has been changed`;}
        else if(req.body.save_type === "observer") {notification_title = `Observers have been changed`;}
        else if(req.body.save_type === "note") {notification_title = `Notes have been updated`;}
        else if(req.body.save_type === "tasktitle") {notification_title = `Task title has been updated`;}
        else if(req.body.save_type === "keyword") {notification_title = `Keywords have been updated`;}
        else if(req.body.save_type === "project_title") {notification_title = `Project title has been updated`;}
        
        // hour_breakdown = [{fdate: '', tdate: '', hours: '', note: '', status: ''}]
        if (Array.isArray(req.body.hour_breakdown) && req.body.hour_breakdown.length > 0) {
            for (let i = 0; i < req.body.hour_breakdown.length; i++) {
                let obj = req.body.hour_breakdown[i];

                if (!obj.hasOwnProperty('fdate') || !obj.hasOwnProperty('forecasted_hours')) {
                    return res.status(400).json({ success: false, error: `The hour_breakdown object is incorrect required keys` });
                }
            }
            data.hour_breakdown = req.body.hour_breakdown;
        }
        // cost_breakdown = [{cost_title: '', forecasted_cost: '', actual_cost: ''}]
        if (Array.isArray(req.body.cost_breakdown) && req.body.cost_breakdown.length > 0) {
            for (let i = 0; i < req.body.cost_breakdown.length; i++) {
                let obj = req.body.cost_breakdown[i];

                if (!obj.hasOwnProperty('cost_title') || !obj.hasOwnProperty('forecasted_cost') || !obj.hasOwnProperty('actual_cost')) {
                    return res.status(400).json({ success: false, error: `The cost_breakdown object is incorrect required keys` });
                }
                if (!obj.hasOwnProperty('_id')) {
                    obj._id = new ObjectId();
                }
            }
            data.cost_breakdown = req.body.cost_breakdown;
        }

        // if (req.body.conversation_id) 
        data.conversation_id = req.body.conversation_id;
        if (req.body.conversation_name) data.conversation_name = req.body.conversation_name;
        if (req.body.task_title) data.task_title = req.body.task_title;
        if (Array.isArray(req.body.participants)) data.participants = req.body.participants;
        if (Array.isArray(req.body.key_words)) data.key_words = req.body.key_words;
        // keyword block
        if (Array.isArray(req.body.key_words) && req.body.key_words.length > 0) {
            for (let i = 0; i < req.body.key_words.length; i++) {
                let k = await set_keywords({ keywords_title: req.body.key_words[i], created_by: decode.id, company_id: decode.company_id });
                if (k.success)
                    console.log(`${k.task_keywords.keywords_title} ${k.msg !== 'Already exists' ? 'save successfully for future use.' : 'old keywords'}`);
                else {
                    console.log(`${k.task_keywords.keywords_title} not saved. Error ${k}`);
                }
            }
        }
        // keyword block end
        data.last_updated_at = new Date();
        // data.view_update = [...req.body.participants.filter((v)=> v != decode.id)];
        // console.log(175, data);
        if("owned_by" in req.body){
            data.owned_by = req.body.owned_by;
        }
        if("owned_status" in req.body){
            data.owned_status = req.body.owned_status;
            if(data.owned_status){
                data.owned_at = Date.now();
            }
        }
        
        let task = await Task.findOneAndUpdate({ _id: req.body._id }, data, {new: true});
        task = task.toObject();
        if (data.observers) task.observers = data.observers;
        if (data.assign_to) task.assign_to = data.assign_to;
        if (data.review) task.review = data.review;
        // console.log(177, task);
        if (req.body.status === 'Completed') {
            let created_for = task.review ? [...task.observers, task.created_by] : [task.created_by];
            await Task.findOneAndUpdate({ _id: req.body._id }, { review_status: created_for });
            for (let i = 0; i < created_for.length; i++) {
                let review = set_task_into_review({
                    task_id: task._id,
                    project_id: task.project_id,
                    created_by: decode.id,
                    created_for: created_for[i],
                    review_title: data.task_title,
                    company_id: task.company_id
                });
            }
            // console.log(186, review);
        } else {
            let review = remove_task_review({ task_id: task._id.toString() });
            // console.log(189, review);
        }
        if (req.body.key_words && Array.isArray(req.body.key_words) && req.body.key_words.length > 0) {
            await set_task_into_keyword({ task_keywords: req.body.key_words, task_id: req.body._id });
        }

        var uobj = await get_user_obj(task.created_by);
        var noti_data = await save_notification({
            company_id: decode.company_id,
            receiver_id: task.participants,
            title: notification_title,
            type: "Task",
            body: JSON.stringify(data),
            user_id: task.created_by,
            user_name: uobj.firstname + ' ' + uobj.lastname,
            user_img: uobj.img,
            task_id: task._id,
            tab: "property",
            decode_id: decode.id
        }, req);

        data.assign_to_details = [];
        if (data.assign_to && Array.isArray(data.assign_to)) {
            for (let j = 0; j < data.assign_to.length; j++) {
                let this_user = await get_user_obj(data.assign_to[j]);
                data.assign_to_details.push({
                    id: this_user.id,
                    email: this_user.email,
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    is_active: this_user.is_active,
                    img: this_user.img,
                    role: this_user.role,
                    fnln: this_user.fnln,
                    createdat: this_user.createdat
                })
            }

        }
        data.observers_details = [];
        if (data.observers && Array.isArray(data.observers)) {
            for (let j = 0; j < data.observers.length; j++) {
                let this_user = await get_user_obj(data.observers[j]);
                data.observers_details.push({
                    id: this_user.id,
                    email: this_user.email,
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    is_active: this_user.is_active,
                    img: this_user.img,
                    role: this_user.role,
                    fnln: this_user.fnln,
                    createdat: this_user.createdat
                })
            }

        }
        data.participants_details = [];
        if (data.participants && Array.isArray(data.participants)) {
            for (let j = 0; j < data.participants.length; j++) {
                let this_user = await get_user_obj(data.participants[j]);
                data.participants_details.push({
                    id: this_user.id,
                    email: this_user.email,
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    is_active: this_user.is_active,
                    img: this_user.img,
                    role: this_user.role,
                    fnln: this_user.fnln,
                    createdat: this_user.createdat
                })
            }

        }
        if(data.description_by){
            let this_user = await get_user_obj(data.description_by);
            data.description_by = {
                id: this_user.id,
                email: this_user.email,
                firstname: this_user.firstname,
                lastname: this_user.lastname,
                is_active: this_user.is_active,
                img: this_user.img,
                role: this_user.role,
                fnln: this_user.fnln,
                createdat: this_user.createdat
            };
        }
        if(data.created_by){
            data.created_by_details = {};
            let this_user = await get_user_obj(data.created_by);
            data.created_by_details = {
                id: this_user.id,
                email: this_user.email,
                firstname: this_user.firstname,
                lastname: this_user.lastname,
                is_active: this_user.is_active,
                img: this_user.img,
                role: this_user.role,
                fnln: this_user.fnln,
                createdat: this_user.createdat
            };
        }
        task.assign_to_details = data.assign_to_details;
        task.observers_details = data.observers_details;
        task.participants_details = data.participants_details;
        task.description_by = data.description_by;
        task.created_by_details = data.created_by_details;
        delete task.notes;

        let xmppArr = [decode.id];
        if(Array.isArray(data.assign_to) && data.assign_to?.length) xmppArr.push(...data.assign_to);
        if(Array.isArray(data.observers) && data.observers?.length) xmppArr.push(...data.observers);
        if(Array.isArray(data.owned_by) && data.owned_by?.length) xmppArr.push(...data.owned_by);
        
        if(xmppArr.length){
            Array.from(new Set([...xmppArr])).forEach(async function(v, k){
                xmpp_send_server(v.toString(), 'new_task', task, req);
            });
        }
        
        return res.status(200).json({ success: true, task: data, noti_data: noti_data, msg: "Task Update Successfully." });
    } else {
        return res.status(400).json({ success: false, msg: "Data is not valid" })
    }
});

/**
 * This route use for get all tasks
 * 
 * _id = optional
 * page = optional
 * this_week
 * this_month
 * start_date
 * end_date
 * status = Unassigned ...
 * task_title
 * conversation_id
 * key_words
 * assign_to
 * observers
 * from = workload/ all(default)
 * missing = yes
 */
router.post('/get_all', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    var PAGE_SIZE = 20;
    var page = Number(req.body.page) || 1;
    let query = { has_delete : { $nin: [decode.id.toString()] }};
    let tasks = [];
    let total = 0;
    let totalPages = 1;
    let assign_to = [];
    let notification = [];
    // if(req.body.created_by)
    //     req.body.created_by = [decode.id];
    // req.body.assign_to = ['3e6b810a-3da3-41a7-9d89-a2996beba220'];
    // console.log(414, req.body);
    try {
        if (req.body._id && req.body._id !== undefined && req.body._id !== '') {
            tasks = await Task.find({ _id: req.body._id }).sort({last_updated_at:-1}).populate({
                path: 'checklist_id',
                model: 'task_checklist'
            }).lean();
            if (tasks.length === 1 && tasks[0].checklist_id && tasks[0].checklist_id._id) {
                let items = await Checklist_item.find({ checklist_id: tasks[0].checklist_id._id }).lean();
                for (let i = 0; i < items.length; i++) {
                    let uobj = await get_user_obj(items[i].created_by);
                    items[i].created_by_name = uobj.firstname + ' ' + uobj.lastname;
                }
                tasks[0].checklist_id.items = items;
            }
            // else{
            //     tasks[0].checklist_id = {};
            //     tasks[0].checklist_id.items = [];
            // }
            tasks[0].discussion = [];
            tasks[0].files = [];
            let tag_ids = [];
            var taginfo = [];

            let td = await Messages.find({ task_id: req.body._id, msg_type: new RegExp('discussion', 'i') }).lean();
            let fd = await File.find({ task_id: req.body._id, is_delete: 0 }).lean();
            if (fd.length > 0) {
                tasks[0].files = fd;
                _.each(tasks[0].files, function (fvv, fkk) {
                    // if (fv == fvv.location) {
                    // fvv.location = process.env.FILE_SERVER + fvv.location;
                    fvv.tag_list = Array.isArray(fvv.tag_list) ? fvv.tag_list : [];
                    fvv.star = Array.isArray(fvv.star) ? fvv.star : [];
                    fvv.url_short_id = fvv.url_short_id === null ? "" : fvv.url_short_id;
                    fvv.secret_user = Array.isArray(fvv.secret_user) ? fvv.secret_user : [];
                    try {
                        var tlwu = JSON.parse(fvv.tag_list_with_user);
                    } catch (e) {
                        var tlwu = [];
                    }
                    fvv.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
                    // td[i].all_attachment.push(fvv);
                    if (fvv.tag_list.length > 0) {
                        for (let j = 0; j < fvv.tag_list.length; j++)
                            if (tag_ids.indexOf(fvv.tag_list[j]) === -1)
                                tag_ids.push(fvv.tag_list[j]);
                    }
                    return false;
                    // }
                });

                // if(tasks[0].cost_breakdown && tasks[0].cost_breakdown.length){
                //     tasks[0].cost_breakdown.map((v)=>{
                //         v.files = fd.find((f)=> f.cost_id == v._id) || [];
                //     });
                // }

            }
            if (tag_ids.length > 0) {
                taginfo = await Tag.find({ tag_id: { $in: tag_ids } }, 'tag_id tagged_by title company_id type shared_tag visibility tag_type tag_color team_list created_at update_at').lean();
            }
            if (td.length > 0) {
                for (let i = 0; i < td.length; i++) {
                    // td[i].msg_body = CryptoJS.AES.encrypt(td[i].msg_body, process.env.CRYPTO_SECRET).toString();
                    td[i].all_attachment = [];
                    let att_list = [...td[i].attch_imgfile, ...td[i].attch_audiofile, ...td[i].attch_videofile, ...td[i].attch_otherfile];
                    for (let fv of att_list) {
                        // td[i].all_attachment.push({location: process.env.FILE_SERVER + fv});
                        _.each(tasks[0].files, function (fvv, fkk) {
                            if (fv == fvv.location) {
                                fvv.location = process.env.FILE_SERVER + fvv.location;
                                fvv.tag_list = Array.isArray(fvv.tag_list) ? fvv.tag_list : [];
                                fvv.tag_list_details = [];
                                if (fvv.tag_list.length > 0) {
                                    for (let j = 0; j < fvv.tag_list.length; j++) {
                                        for (let k = 0; k < taginfo.length; k++) {
                                            if (taginfo[k].tag_id === fvv.tag_list[j])
                                                fvv.tag_list_details.push(taginfo[k]);
                                        }
                                    }
                                }

                                fvv.star = Array.isArray(fvv.star) ? fvv.star : [];
                                fvv.url_short_id = fvv.url_short_id === null ? "" : fvv.url_short_id;
                                fvv.secret_user = Array.isArray(fvv.secret_user) ? fvv.secret_user : [];
                                try {
                                    var tlwu = JSON.parse(fvv.tag_list_with_user);
                                } catch (e) {
                                    var tlwu = [];
                                }
                                fvv.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
                                td[i].all_attachment.push(fvv);


                                return false;
                            }
                        });
                    }

                }

                tasks[0].discussion = td;
            }
            if (tasks.length === 1) {
                tasks[0].notification = await get_notification({ tab: 'property', user_id: decode.id, company_id: decode.company_id, task_id: req.body._id, page: req.query.page || 1 });
                let p = await get_project({ company_id: decode.company_id });
                if (p.success && p.projects.length) {
                    tasks[0].projects_list = p.projects.map(p=> ({id: String(p._id), title: p.project_title }));
                }else{
                    tasks[0].projects_list = [];
                }
            }
            
            if(!tasks[0].view_cost?.includes(decode.id) && 
            !tasks[0].view_hour?.includes(decode.id) && 
            !tasks[0].view_description?.includes(decode.id) &&
            // !tasks[0].view_note?.includes(decode.id) &&
            !tasks[0].view_checklist?.includes(decode.id)
            ){
                await Task.updateOne({ _id: new ObjectId(req.body._id) }, { $pull: { view_update: decode.id } });
            }
            // if(tag_ids.length>0){
            //     let taginfo = await Tag.find({tag_id: {$in: tag_ids}}, 'tag_id tagged_by title company_id type shared_tag visibility tag_type tag_color team_list created_at update_at').lean();

            //     for(let file of tasks[0].files){
            //         if(msg.all_attachment.length>0){
            //             for(let i=0; i<msg.all_attachment.length; i++){
            //                 // msg.all_attachment[i].tag_list_details = [];
            //                 // msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
            //                 if(msg.all_attachment[i].tag_list.length>0){
            //                     for(let j=0; j<msg.all_attachment[i].tag_list.length; j++){
            //                         for(let k=0; k<taginfo.length; k++){
            //                             if(taginfo[k].tag_id === msg.all_attachment[i].tag_list[j])
            //                                 msg.all_attachment[i].tag_list_details.push(taginfo[k]);
            //                         }
            //                     }
            //                 }
            //             }
            //         }
            //     }
            // }
        }
        else {
            const isEmpty = Object.values(req.body).every(x => x === null || x === '' || x === 0 || x.length === 0);
            // if(isEmpty){
            setTimeout(function () {
                console.log("777777777777777777777777");
                Task.updateMany({ view_status: { $in: [decode.id] } }, { $pull: { view_status: decode.id } }, { multi: true })
                    .then(result => { console.log(508, 'task update done') })
                    // .then(result => {console.log(508, 'task update done', result)})
                    .catch(error => { console.log(509, error); });
            }, 3000);
            // }
            req.body.created_by = Array.isArray(req.body.created_by) ? req.body.created_by : [];
            req.body.assign_to = Array.isArray(req.body.assign_to) ? req.body.assign_to : [];
            req.body.observers = Array.isArray(req.body.observers) ? req.body.observers : [];

            if (req.body.this_week === 'yes') {
                let curr = new Date; // get current date
                let first = curr.getDate() - curr.getDay();
                let last = first + 6;

                req.body.start_date = new Date(curr.setDate(first)).toUTCString();
                req.body.end_date = new Date(curr.setDate(curr.getDate() + 6)).toUTCString();
            }
            else if (req.body.this_month === 'yes') {
                let curr = new Date();
                req.body.start_date = new Date(curr.getFullYear(), curr.getMonth(), 1);
                req.body.end_date = new Date(curr.getFullYear(), curr.getMonth() + 1, 0);
            }
            if (req.body.start_date) {
                req.body.start_date = new Date(req.body.start_date).setHours(00, 00, 00, 000);
                query.start_date = { $gte: new Date(req.body.start_date) };
            }
            if (req.body.end_date) {
                req.body.end_date = new Date(req.body.end_date).setHours(23, 59, 59, 999);
                query.end_date = { $lte: new Date(req.body.end_date) };
            }
            if (Array.isArray(req.body.flag) && req.body.flag.indexOf(decode.id) > -1)
                query.flag = { $in: [decode.id] };

            if (req.body.status) {
                if (req.body.status === 'OverDue') {
                    query.status = { $ne: 'Completed' };
                    // req.body.end_date = new Date().setHours(23, 59, 59, 999);
                    req.body.end_date = Date.now();
                    query.end_date = { $lte: new Date(req.body.end_date) };
                }
                else if (req.body.status === 'incomplete') {
                    query.status = { $ne: 'Completed' };
                }
                else if (req.body.status === 'complete') {
                    query.status = { $eq: 'Completed' };
                }
                else if (req.body.status === 'flagged') {
                    query.flag = { $in: [decode.id] };
                }
                else if (req.body.status === 'deleted') {
                    query.has_delete = { $in: [decode.id.toString()] };
                }
                else if (req.body.status === 'Unassigned') {
                    // req.body.created_by = [decode.id];
                    query.assign_to = [];
                }
                else {
                    query.status = req.body.status;
                }
            }
            if (req.body.task_title) {
                query.task_title = new RegExp(req.body.task_title, 'i');
            }
            if (Array.isArray(req.body.conversation_id)) {
                query.conversation_id = { $in: req.body.conversation_id };
            }
            if (Array.isArray(req.body.key_words)) {
                query.key_words = { $all: req.body.key_words };
            }
            if (req.body.progress) {
                query.progress = req.body.progress
            }
            if (req.body.from === 'workload') {
                PAGE_SIZE = 1000;
                page = 1;
                if (req.body.assign_to.length > 0) {
                    query.created_by = decode.id;
                    query.assign_to = { $in: req.body.assign_to };
                }
                else if (req.body.created_by) {
                    query.created_by = { $in: req.body.created_by };
                }
                else
                    query.created_by = decode.id;
            }
            /*
            due to logic change, this code off
            else if (req.body.assign_to.length>1) {
                query.$and = [
                        { created_by: {$in: req.body.assign_to }},
                        { assign_to: { $in: [decode.id] } }
                    ];
            }
            else if (req.body.observers.length>1) {
                query.$and = [
                        { created_by: {$in: req.body.observers }},
                        { observers: { $in: [decode.id] } }
                    ];
            }
            else if (req.body.created_by.length>0) {
                query.created_by = {$in: req.body.created_by};
            }
            else if (req.body.observers.length==1) {
                query.observers = {$in: req.body.observers};
            }
            else if (req.body.assign_to.length==1) {
                query.assign_to = {$in: req.body.assign_to };
            }
            */
            else if (req.body.assign_to.length > 0) {
                query.assign_to = { $in: req.body.assign_to };
                // query.created_by = decode.id;
            }
            else if (req.body.created_by.length > 0 && req.body.missing !== 'yes') {
                query.created_by = { $in: req.body.created_by };
            }
            else if (req.body.observers.length > 0) {
                query.observers = { $in: req.body.observers };
            }

            else if (req.body.missing === 'yes') {
                query.$and = [
                    { created_by: { $in: req.body.created_by } },
                    {
                        "$or": [
                            { "end_date": null },
                            { "forecasted_hours": null },
                            { "forecasted_hours": 0 }
                        ]
                    }
                ];
                // { $and: [
                //     { assign_to: { $in: [decode.id] } }, 
                //     {$or: [
                //         {end_date: null},
                //         {$or: [{ forecasted_hours: null }, { forecasted_hours: { $eq: 0, $type: 'number' } }]}
                //     ]}
                // ]
                // },
                // { $and: [
                //     { observers: { $in: [decode.id] } },
                //     {$or: [
                //         {end_date: null},
                //         {$or: [{ forecasted_hours: null }, { forecasted_hours: { $eq: 0, $type: 'number' } }]}
                //     ]}
                // ]}
            }
            else {
                // if (Array.isArray(req.body.assign_to)) {
                //     req.body.assign_to.push(decode.id);
                //     query.$or = [{ created_by: decode.id }, { assign_to: { $in: req.body.assign_to } }];
                // }
                // else
                query.$or = [{ created_by: decode.id }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }, { owned_by: { $in: [decode.id] } } ];
                // query.has_delete = { $nin: [decode.id.toString()] }
            }

            // console.log(236, JSON.stringify(query));
            const dataQuery = [
                { $match: query },
                { $sort: { created_at: -1 } }
            ];
            // if (req.body.export !== 'yes') {
            //     dataQuery.push({ $skip: (page - 1) * PAGE_SIZE });
            //     dataQuery.push({ $limit: PAGE_SIZE });
            // }
            tasks = await Task.aggregate(dataQuery);
        }

        var custom_data = await TaskCustom.aggregate([
            { $match: { 
                custom_type: "Status",
                company_id: decode.company_id} 
            }
        ]);

        if (tasks && tasks.length > 0) {
            if (!req.body._id) {
                const count_total = [{ $match: query }, { $count: "total" }];
                var countResult = await Task.aggregate(count_total);
                total = countResult[0] ? countResult[0].total : 0;
                totalPages = Math.ceil(total / PAGE_SIZE);
            }
            for (let i = 0; i < tasks.length; i++) {
                tasks[i].progress = tasks[i].progress ? tasks[i].progress : 0;
                tasks[i].assign_to_details = [];
                tasks[i].observers_details = [];
                tasks[i].participants_details = [];
                tasks[i].owned_by_details = [];
                tasks[i].created_by_details = {};
                
                tasks[i].flag = Array.isArray(tasks[i].flag) ? tasks[i].flag : [];
                tasks[i].hour_breakdown = Array.isArray(tasks[i].hour_breakdown) ? tasks[i].hour_breakdown : [];
                tasks[i].cost_breakdown = Array.isArray(tasks[i].cost_breakdown) ? tasks[i].cost_breakdown : [];
                tasks[i].notes = tasks[i].has_note && tasks[i].has_note.hasOwnProperty(decode.id) ? tasks[i].has_note[decode.id] : "";
                
                let this_user = await get_user_obj(tasks[i].created_by);
                tasks[i].created_by_details = {
                    id: this_user.id,
                    email: this_user.email,
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    is_active: this_user.is_active,
                    img: this_user.img,
                    role: this_user.role,
                    fnln: this_user.fnln,
                    createdat: this_user.createdat
                };

                if(tasks[i].description_by){
                    let this_user2 = await get_user_obj(tasks[i].description_by);
                    tasks[i].description_by = {
                        id: this_user2.id,
                        email: this_user2.email,
                        firstname: this_user2.firstname,
                        lastname: this_user2.lastname,
                        is_active: this_user2.is_active,
                        img: this_user2.img,
                        role: this_user2.role,
                        fnln: this_user2.fnln,
                        createdat: this_user2.createdat
                    };

                }
                
                for (let j = 0; j < tasks[i].assign_to.length; j++) {
                    if(Array.isArray(tasks[i].assign_to[j]) && tasks[i].assign_to[j][0]){
                        tasks[i].assign_to[j] = tasks[i].assign_to[j][0];
                    } 

                    if (!req.body.assign_to || req.body.assign_to.length == 0) {
                        if (assign_to.indexOf(tasks[i].assign_to[j]) === -1) 
                            assign_to.push(tasks[i].assign_to[j]);
                    }
                    let this_user = await get_user_obj(tasks[i].assign_to[j]);
                    tasks[i].assign_to_details.push({
                        id: this_user.id,
                        email: this_user.email,
                        firstname: this_user.firstname,
                        lastname: this_user.lastname,
                        is_active: this_user.is_active,
                        img: this_user.img,
                        role: this_user.role,
                        fnln: this_user.fnln,
                        createdat: this_user.createdat
                    })
                }
                for (let j = 0; j < tasks[i].participants.length; j++) {
                    let this_user = await get_user_obj(tasks[i].participants[j]);
                    tasks[i].participants_details.push({
                        id: this_user.id,
                        email: this_user.email,
                        firstname: this_user.firstname,
                        lastname: this_user.lastname,
                        is_active: this_user.is_active,
                        img: this_user.img,
                        role: this_user.role,
                        fnln: this_user.fnln,
                        createdat: this_user.createdat
                    })
                }
                for (let j = 0; j < tasks[i].observers.length; j++) {
                    let this_user = await get_user_obj(tasks[i].observers[j]);
                    tasks[i].observers_details.push({
                        id: this_user.id,
                        email: this_user.email,
                        firstname: this_user.firstname,
                        lastname: this_user.lastname,
                        is_active: this_user.is_active,
                        img: this_user.img,
                        role: this_user.role,
                        fnln: this_user.fnln,
                        createdat: this_user.createdat
                    })
                }
                if(tasks[i].owned_by && Array.isArray(tasks[i].owned_by)){
                    for (let j = 0; j < tasks[i].owned_by.length; j++) {
                        let this_user = await get_user_obj(tasks[i].owned_by[j]);
                        tasks[i].owned_by_details.push({
                            id: this_user.id,
                            email: this_user.email,
                            firstname: this_user.firstname,
                            lastname: this_user.lastname,
                            is_active: this_user.is_active,
                            img: this_user.img,
                            role: this_user.role,
                            fnln: this_user.fnln,
                            createdat: this_user.createdat
                        })
                    }
                }
            }
            res.status(200).json({
                success: true, tasks, custom_data: custom_data || [], notification, assign_to, msg: 'Success', pagination: {
                    page: parseInt(page),
                    totalPages,
                    total
                }
            });
        }
        else {

            res.status(200).json({
                success: false, tasks: [], notification, pagination: {
                    page: parseInt(page),
                    totalPages,
                    total
                }, msg: 'Sorry! No task found!'
            });
        }
    } catch (e) {
        res.status(400).json({ success: false, error: e });
    }
});

router.get('/get_unread', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);

    let unread_tasks = await get_unread_tasks(decode);
    let unread_notification = await get_notification_count(decode);

    res.status(200).json({ unread_tasks, unread_notification });

});

router.get('/get_team_transfer', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let participant = [];
    if(req.query.conv_id){
        var conv = await get_conversation({ conversation_id: req.query.conv_id, user_id: decode.id });
        if(conv && conv.length){
            if(conv[0].group == 'yes'){
                var allteams = await Team.find({team_id: conv[0].team_id}).lean();
                if(allteams && allteams.length) participant = allteams[0].participants;

            }else{
                var allteams = await Team.find({participants: { $in: [decode.id] }}).lean();
                if(allteams && allteams.length){
                    allteams.map(v => {
                        participant.push(...v.participants);
                    })
                }
                

            }
            
        }
    }

    res.status(200).json({  participants: Array.from(new Set([...participant.filter(f=> f != decode.id)])) });

});

router.get('/get_unread_discussion', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    var unread1 = [];
    var unread2 = [];
    var unread3 = [];
    var task_list = [];
    
    var query1 = { msg_status: { $in: [decode.id] }, msg_type: new RegExp('discussion', 'i'), has_delete: { $nin: [decode.id.toString()] } };
    let unread_disc = await Messages.find(query1, { task_id: 1,msg_id:1, _id: 0 }).lean();
    if (unread_disc) unread1 = unread_disc.map((v) => String(v.task_id));

    var query2 = { has_delete: { $nin: [decode.id.toString()] }  };
    query2.$or = [{ created_by: decode.id }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }];
    let list = await Task.find(query2, { _id: 1,view_update:1 }).lean();
    if (list) task_list = list.map((v) => String(v._id));
    if (list) unread2 = list.filter((v)=> v.view_update && v.view_update.includes(decode.id)).map((v) => String(v._id));
    
    if(unread1.length) unread1 = Array.from(new Set([...unread1])).filter((v)=> task_list.includes(v));

    let query3 = { company_id: decode.company_id, type: 'Task', read_status: 'no', receiver_id: decode.id };
    const countNotification = await Notification.find(query3, { _id: 0, task_id: 1 }).lean();
    if (countNotification) unread3 = countNotification.map((v) => String(v.task_id));
    
    let unread_tasks = new Set([...unread1, ...unread2]);
    let uu = Array.from(unread_tasks);
    let uu2 = uu.filter((v) => v != "undefined");
    // let uu2 = uu.filter((v) => v != "undefined");

    res.status(200).json({ unread: uu2, unread_discussion: unread1, unread_cost_hour: unread2, unread_notification: unread3.filter((v) => v != "undefined") });

});

router.get('/get_project_list', async (req, res) => {
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let projects_list = [];
    let p = await get_project({ company_id: decode.company_id });
    if (p.success && p.projects && p.projects.length) {
        projects_list = p.projects.map(p=> ({id: String(p._id), projectTitle: p.project_title }));
    }
    
    res.status(200).json({ projects_list: projects_list || [] });

});



/**
 * This route use for task dashboard
 * year = 2023
 * month = 0-11
 * members = optional
 */
router.post('/get_task_dashboard', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let teams = await Team.find({ created_by: decode.id, company_id: decode.company_id }).lean();
    let event = {};
    var query = { has_delete: { $nin: [decode.id.toString()] } };

    /**
     * due to logic change, this code block is commented. 
     * in this block, when multiple user select, the created by set to the multiple user,
     * but assign to and observers are always me (me = who loged in now, that is decode data)
     * 
     * and when single user select, all task show 
     * where i connected (i = who loged in now, that is decode data)
     */
    /*
    let members = [decode.id];
    if (Array.isArray(req.body.members) && req.body.members.length > 0) {
        members = [...members, ...(req.body.members)];
    }
    if(members.length>1){
        query.$or = [
            {$and: [
                { created_by: {$in: members }},
                { assign_to: { $in: [decode.id] } }
            ]}, 
            {$and: [
                { created_by: {$in: members }},
                { observers: { $in: [decode.id] } }
            ]}, 
            { created_by: decode.id }
        ];
    }else{
        query.$or = [{ created_by: { $in: members } }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }];
    }
    */

    /**
     * Now logic is- 
     * member list other then me, that is, fully that user dashboard.
     * because member list are my team member. so I can see my team member all task 
     * 
     * and if multiple member selected, then marge all the data.
     */
    let members = [];
    if (Array.isArray(req.body.members) && req.body.members.length > 0) {
        members = [...req.body.members];
        query.$or = [{ created_by: { $in: members } }, { assign_to: { $in: members } }, { observers: { $in: members } }];
    } else {
        members = [decode.id];
        query.$or = [{ created_by: decode.id }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }];
    }

    if (req.body.month && req.body.year) {
        req.body.start_date = new Date(req.body.year, req.body.month, 1);
        req.body.end_date = new Date(req.body.year, req.body.month + 1, 0);
    }
    if (req.body.start_date) {
        req.body.start_date = new Date(req.body.start_date).setHours(00, 00, 00, 000);
        query.start_date = { $gte: new Date(req.body.start_date) };
    }
    if (req.body.end_date) {
        req.body.end_date = new Date(req.body.end_date).setHours(23, 59, 59, 999);
        query.end_date = { $lte: new Date(req.body.end_date) };
    }

    let curr = new Date();
    let this_month_start_date = new Date(curr.getFullYear(), curr.getMonth(), 1);
    let this_month_end_date = new Date(curr.getFullYear(), curr.getMonth() + 1, 0);

    try {
        // console.log(698, query);
        var result = await Task.find(query).lean();
        let k = await get_keywords({ company_id: decode.company_id });
        let missing = 0;
        let summary = { overdue: 0, inprogress: 0, onhold: 0, notstarted: 0, unassigned: 0, completed: 0 };
        let assigned_summary = { overdue: 0, inprogress: 0, onhold: 0, notstarted: 0, unassigned: 0, completed: 0 };
        let created_summary = { overdue: 0, inprogress: 0, onhold: 0, notstarted: 0, unassigned: 0, completed: 0 };
        let observing_summary = { overdue: 0, inprogress: 0, onhold: 0, notstarted: 0, unassigned: 0, completed: 0 };
        let room = [];
        let keywords = [];
        let _keywords = [];
        let this_month_over_due = 0;
        let participants = [decode.id];
        let participants_details = [];
        let unread_notification = await get_notification_count(decode);
        let unread_tasks = await get_unread_tasks(decode);

        for (let i = 0; i < teams.length; i++) {
            for (let j = 0; j < teams[i].participants.length; j++) {
                if (participants.indexOf(teams[i].participants[j]) === -1) {
                    participants.push(teams[i].participants[j])
                }
            }
        }

        if (result && result.length > 0) {
            // let priority_status = [];
            let today = Date.now();
            // let today = new Date().setHours(23, 59, 59, 999);
            let curr = new Date();
            let first_day_of_month = new Date(curr.getFullYear(), curr.getMonth(), 1).getTime();
            for (let i = 0; i < result.length; i++) {
                if (result[i].due_time && result[i].end_date) {
                    let h = new Date(result[i].due_time).getHours();
                    let m = new Date(result[i].due_time).getMinutes();
                    result[i].end_date = new Date(result[i].end_date).setHours(h, m, 59, 999);
                    // console.log(827, result[i].end_date);
                }
                // if (Array.isArray(req.body.members) && req.body.members.length > 0){
                //     let cflag = false; 
                //     let aflag = false; 
                //     let oflag = false;
                //     // cflag = members.indexOf(result[i].created_by) > -1;
                //     if(result[i].assign_to.length>0)
                //         aflag = req.body.members.some(e => (result[i].assign_to).includes(e))
                //     if(result[i].observers.length>0)
                //         oflag = req.body.members.some(e => (result[i].observers).includes(e))
                //     if(aflag === false && oflag === false)
                //         continue;
                // }


                if (result[i].key_words.length > 0) {
                    _keywords = [..._keywords, ...result[i].key_words];
                }

                // Due to logic change, this code is off.
                // if(participants.indexOf(result[i].created_by) === -1) participants.push(result[i].created_by);
                // for(let ai = 0; ai<result[i].assign_to.length; ai++)
                //     if(participants.indexOf(result[i].assign_to[ai]) === -1) participants.push(result[i].assign_to[ai]);
                // for(let ai = 0; ai<result[i].observers.length; ai++)
                //     if(participants.indexOf(result[i].observers[ai]) === -1) participants.push(result[i].observers[ai]);

                // Event block
                if (result[i].start_date && result[i].end_date && ((result[i].start_date.getTime() > this_month_start_date) &&
                    (result[i].start_date.getTime() < this_month_end_date)) ||
                    ((result[i].end_date > this_month_start_date) &&
                        (result[i].end_date < this_month_end_date))) {

                    if (result[i].start_date && (result[i].start_date.getTime() > this_month_start_date) && (result[i].start_date.getTime() < this_month_end_date)) {
                        let dd = new Date(result[i].start_date);
                        let d = dd.getDate();
                        let key = dd.getFullYear() + '-' + (dd.getMonth() + 1) + "-" + d;
                        if (!event.hasOwnProperty(key)) event[key] = [];

                        event[key].push({
                            day: d,
                            name: result[i].task_title,
                            event_status: 'started',
                            task_status: result[i].status
                        })
                    }

                    if ((result[i].end_date > this_month_start_date) && (result[i].end_date < this_month_end_date)) {
                        let dd = new Date(result[i].end_date);
                        let d = dd.getDate();
                        let key = dd.getFullYear() + '-' + (dd.getMonth() + 1) + "-" + d;
                        if (!event.hasOwnProperty(key)) event[key] = [];

                        event[key].push({
                            day: d,
                            name: result[i].task_title,
                            event_status: result[i].end_date && result[i].end_date < today && result[i].status !== 'Completed' ? 'overdue' : 'ended',
                            task_status: result[i].status
                        })
                    }
                }
                // End Event block
                result[i].forecasted_hours = result[i].forecasted_hours ? result[i].forecasted_hours : 0;

                if (result[i].end_date && result[i].end_date > first_day_of_month && result[i].end_date < today) this_month_over_due++;
                if (result[i].end_date && result[i].end_date < today && result[i].status !== 'Completed') summary.overdue++;
                if (result[i].status === 'In Progress') summary.inprogress++;
                if (result[i].status === 'On Hold') summary.onhold++;
                if (result[i].status === 'Not Started') summary.notstarted++;
                if (result[i].status === 'Completed') summary.completed++;
                if (result[i].assign_to.length === 0) summary.unassigned++;
                // console.log(882, !result[i].end_date);
                // console.log(882, result[i].forecasted_hours);
                // console.log(882, !result[i].end_date || result[i].forecasted_hours <= 0);
                if (members.some(e => (result[i].created_by).includes(e)) && (!result[i].end_date || result[i].forecasted_hours === null || result[i].forecasted_hours <= 0)) missing++;

                if (result[i].assign_to.length > 0) {
                    if (members.some(e => (result[i].assign_to).includes(e))) {
                        // if(result[i].assign_to.indexOf(decode.id)>-1){
                        if (result[i].end_date && result[i].end_date < today && result[i].status !== 'Completed') {
                            assigned_summary.overdue++;
                        }
                        if (result[i].status === 'In Progress') assigned_summary.inprogress++;
                        if (result[i].status === 'On Hold') assigned_summary.onhold++;
                        if (result[i].status === 'Not Started') assigned_summary.notstarted++;
                        if (result[i].status === 'Completed') assigned_summary.completed++;
                        if (result[i].assign_to.length === 0) assigned_summary.unassigned++;
                    }
                }
                if (result[i].observers.length > 0) {
                    if (members.some(e => (result[i].observers).includes(e))) {
                        // if(result[i].observers.indexOf(decode.id)>-1){
                        if (result[i].end_date && result[i].end_date < today && result[i].status !== 'Completed') observing_summary.overdue++;
                        if (result[i].status === 'In Progress') observing_summary.inprogress++;
                        if (result[i].status === 'On Hold') observing_summary.onhold++;
                        if (result[i].status === 'Not Started') observing_summary.notstarted++;
                        if (result[i].status === 'Completed') observing_summary.completed++;
                        if (result[i].assign_to.length === 0) observing_summary.unassigned++;
                    }
                }

                if (members.indexOf(result[i].created_by) > -1) {
                    if (result[i].end_date && result[i].end_date < today && result[i].status !== 'Completed') {
                        created_summary.overdue++;
                    }
                    if (result[i].status === 'In Progress') created_summary.inprogress++;
                    if (result[i].status === 'On Hold') created_summary.onhold++;
                    if (result[i].status === 'Not Started') created_summary.notstarted++;
                    if (result[i].status === 'Completed') created_summary.completed++;
                    if (result[i].assign_to.length === 0) created_summary.unassigned++;
                }


                let flag = false;
                result[i].end_date = new Date(result[i].end_date);
                for (let j = 0; j < room.length; j++) {
                    if (room[j].conversation_id === result[i].conversation_id) {
                        room[j].notask++;
                        let this_user = await get_user_obj(result[i].created_by);
                        room[j].tasks.push({
                            task_title: result[i].task_title,
                            start_date: result[i].start_date,
                            end_date: result[i].end_date,
                            progress: result[i].progress,
                            status: result[i].status,
                            assign_to: result[i].assign_to,
                            observers: result[i].observers,
                            priority: result[i].priority,
                            forecasted_hours: result[i].forecasted_hours,
                            created_at: result[i].created_at,
                            created_by: result[i].created_by,
                            created_by_details: {
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            },
                            my_roll: result[i].created_by === decode.id ? 'Assignor' : result[i].observers.indexOf(decode.id) > -1 ? 'Observer' : 'Assigned to me'
                        });
                        flag = true;
                    }
                }
                if (flag === false) {
                    let this_user = await get_user_obj(result[i].created_by);
                    room.push({
                        conversation_id: result[i].conversation_id,
                        conversation_name: result[i].conversation_name,
                        notask: 1,
                        tasks: [{
                            task_title: result[i].task_title,
                            start_date: result[i].start_date,
                            end_date: result[i].end_date,
                            progress: result[i].progress,
                            status: result[i].status,
                            assign_to: result[i].assign_to,
                            observers: result[i].observers,
                            priority: result[i].priority,
                            forecasted_hours: result[i].forecasted_hours,
                            created_at: result[i].created_at,
                            created_by: result[i].created_by,
                            created_by_details: {
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            },
                            my_roll: result[i].created_by === decode.id ? 'Assignor' : result[i].observers.indexOf(decode.id) > -1 ? 'Observer' : 'Assigned to me'
                        }]
                    });
                }
            }

            // _keywords = _.uniq(keywords);
            // console.log(921, event);
            let kwu = [];
            for (let j = 0; j < _keywords.length; j++) {
                if (kwu.indexOf(_keywords[j]) > -1) {
                    for (let n = 0; n < keywords.length; n++) {
                        if (keywords[n].keywords_title === _keywords[j]) {
                            keywords[n].total_tasks++;
                            break;
                        }
                    }
                }
                else {
                    for (let i = 0; i < k.task_keywords.length; i++) {
                        if (_keywords[j] === k.task_keywords[i].keywords_title) {
                            kwu.push(_keywords[j]);
                            keywords.push({
                                _id: k.task_keywords[i]._id,
                                keywords_title: k.task_keywords[i].keywords_title,
                                total_tasks: 1,
                            });
                            break;
                        }
                    }
                }
            }

            for (let i = 0; i < participants.length; i++) {
                let this_user = await get_user_obj(participants[i]);
                if (this_user.firstname !== '' || this_user.fnln !== '') {
                    participants_details.push({
                        id: this_user.id,
                        firstname: this_user.firstname,
                        lastname: this_user.lastname,
                        fnln: this_user.fnln,
                        img: this_user.img,
                    });
                }
            }

            res.status(200).json({
                success: true,
                summary,
                assigned_summary,
                created_summary,
                observing_summary,
                keywords,
                missing,
                this_month_over_due,
                total_overdue: summary.overdue,
                total_tasks: result.length,
                total_completed: summary.completed,
                unread_notification,
                unread_tasks,
                event,
                participants_details,
                participants,
                room
            });
        }
        else
            res.status(200).json({ success: true, summary, assigned_summary, created_summary, observing_summary, keywords, missing, this_month_over_due, total_overdue: summary.overdue, total_tasks: result.length, total_completed: summary.completed, unread_notification, unread_tasks, room });
    } catch (e) {
        res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
    }
});

/**
 * this route use for delete a task
 * only your created task you can delete
 * 
 * _id = required
 */
router.delete('/delete_task', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    const requiredKeys = ["_id"];
    let missingKeys = validateJSON(req.query, requiredKeys);
    if (missingKeys.length !== 0)
        return res.status(400).json({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}` });
    try {
        // await Task.updateOne({ _id: req.query._id, created_by: decode.id })
        await Task.updateOne({ _id: req.query._id, created_by: decode.id }, { $push: { has_delete: decode.id } });
        await Messages.updateMany({ task_id: req.query._id }, { $push: { has_delete: decode.id } }, { multi: true } );
        res.status(200).json({ success: true, msg: 'Task delete successfully' });

        var task = await Task.findOne({ _id: req.query._id }).lean();
        var uobj = await get_user_obj(decode.id);
        save_notification({
            company_id: decode.company_id,
            receiver_id: task.participants,
            title: `Task titled '${task.task_title}' deleted`,
            type: "Task",
            body: JSON.stringify(task),
            user_id: decode.id,
            user_name: uobj.firstname + ' ' + uobj.lastname,
            user_img: uobj.img,
            task_id: req.query._id,
            tab: "global",
            decode_id: decode.id
        }, req);
            
        // Messages.findOneAndDelete({ task_id: req.query._id }, function (err, delDoc) {
        //     console.log(err);
        //     // if (err)
        //         // res.status(400).json({ success: false, error: err, msg: 'Delete error' });

        // });
    } catch (e) {
        res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
    }
});

router.delete('/restore_task', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    const requiredKeys = ["_id"];
    let missingKeys = validateJSON(req.query, requiredKeys);
    if (missingKeys.length !== 0)
        return res.status(400).json({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}` });
    try {
        await Task.updateOne({ _id: req.query._id, created_by: decode.id }, { $pull: { has_delete: decode.id } });
        await Messages.updateOne({ task_id: req.query._id }, { $pull: { has_delete: decode.id } })
        res.status(200).json({ success: true, msg: 'Task restored successfully' });

    } catch (e) {
        res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
    }
});

/**
 * This route use for task review tab
 * 
 */
// router.get('/get_task_review', async (req, res) => {
//     const token = extractToken(req);
//     let decode = await token_verify_decode(token);
//     var query = {};
//     query.created_for = decode.id;
//     try {
//         var result = await Review.find(query).lean();
//         var incomplete = await Task.find({status: {$ne: 'Completed'}, $or: [{ created_by: decode.id }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }]});

//         let summary = {
//             five: 0, four: 0, three: 0, two: 0, one: 0, total_review: 0, efficiency_score: 0
//         }
//         let task_ids = [];
//         for (let i = 0; i < result.length; i++) {
//             if(task_ids.indexOf(result[i].task_id)===-1) task_ids.push(result[i].task_id);
//         }
//         let user_review = await Review.find({task_id: {$in : task_ids}}).populate({
//             path: 'task_id',
//             model: 'task'
//         }).lean();
//         let review = [];
//         let review_list = {};
//         for (let i = 0; i < user_review.length; i++) {
//             let uobj = get_user_obj(user_review[i].created_by);
//             user_review[i].history = Array.isArray(user_review[i].history) ? user_review[i].history : [];
//             user_review[i].created_by_name = uobj.fullname;
//             if(! review_list.hasOwnProperty(user_review[i].task_id._id)) {
//                 review_list[user_review[i].task_id._id] = {five: 0, four: 0, three: 0, two: 0, one: 0, nor: 0, history: '', details: []};
//                 review.push(user_review[i]);
//             }
//             if(user_review[i].created_for === decode.id){
//                 review_list[user_review[i].task_id._id].history = user_review[i].history;
//             }
//             uobj = get_user_obj(user_review[i].created_for);
//             review_list[user_review[i].task_id._id].details.push({review_id: user_review[i]._id, created_for: user_review[i].created_for, created_for_name: uobj.fullname, created_at: user_review[i].last_updated_at, rate: user_review[i].rate, review_body: user_review[i].review_body, history: user_review[i].history});
//             switch (user_review[i].rate) {
//                 // case 0: summary.zero ++; break;
//                 case 1: 
//                     review_list[user_review[i].task_id._id].one++; 
//                     summary.one++; break;
//                 case 2: 
//                     review_list[user_review[i].task_id._id].two++; 
//                     summary.two++; break;
//                 case 3: 
//                     review_list[user_review[i].task_id._id].three++; 
//                     summary.three++; break;
//                 case 4: 
//                     review_list[user_review[i].task_id._id].four++; 
//                     summary.four++; break;
//                 case 5: 
//                     review_list[user_review[i].task_id._id].five++; 
//                     summary.five++; break;
//             }
//             if (user_review[i].rate > 0) {
//                 review_list[user_review[i].task_id._id].nor++; 
//                 summary.total_review++;
//             }
//         }
//         for (let i = 0; i < review.length; i++) {
//             review[i].rate = Math.floor(parseInt((review_list[review[i].task_id._id].five * 5 +
//                              review_list[review[i].task_id._id].four * 4 +
//                              review_list[review[i].task_id._id].three * 3 +
//                              review_list[review[i].task_id._id].two * 2 +
//                              review_list[review[i].task_id._id].one * 1) / review_list[review[i].task_id._id].nor)) || 0;
//             review[i].history = review_list[review[i].task_id._id].history;
//             review[i].details = review_list[review[i].task_id._id].details;
//         }
//         summary.efficiency_score = (5 * summary.five + 4 * summary.four + 3 * summary.three + 2 * summary.two + 1 * summary.one) / summary.total_review;

//         summary.five = ((summary.five * 100) / summary.total_review).toFixed(2);
//         summary.four = ((summary.four * 100) / summary.total_review).toFixed(2);
//         summary.three = ((summary.three * 100) / summary.total_review).toFixed(2);
//         summary.two = ((summary.two * 100) / summary.total_review).toFixed(2);
//         summary.one = ((summary.one * 100) / summary.total_review).toFixed(2);
//         summary.efficiency_score = ((summary.efficiency_score * 100) / 5).toFixed(2);

//         summary.five = isNaN(summary.five)  ? 0 : summary.five;
//         summary.four = isNaN(summary.four)  ? 0 : summary.four;
//         summary.three = isNaN(summary.three)  ? 0 : summary.three;
//         summary.two = isNaN(summary.two)  ? 0 : summary.two;
//         summary.one = isNaN(summary.one)  ? 0 : summary.one;
//         summary.efficiency_score = isNaN(summary.efficiency_score)  ? 0 : summary.efficiency_score;

//         res.status(200).json({ success: true, review, summary, incomplete: incomplete.length });
//     } catch (e) {
//         res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
//     }
// });

router.get('/get_task_review', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    var query = {};
    query.created_for = { $in: [decode.id] };
    try {
        var result = await Review.find(query).sort({last_updated_at: -1}).populate({
            path: 'task_id',
            model: 'task'
        }).lean();
        var incomplete = await Task.find({ status: { $ne: 'Completed' }, $or: [{ created_by: decode.id }, { assign_to: { $in: [decode.id] } }, { observers: { $in: [decode.id] } }] });

        let summary = {
            five: 0, four: 0, three: 0, two: 0, one: 0, total_review: 0, efficiency_score: 0
        }
        let task_ids = [];
        for (let i = 0; i < result.length; i++) {
            if (task_ids.indexOf(result[i].task_id?._id.toString()) === -1) task_ids.push(result[i].task_id._id.toString());
        }
        let user_review = await Review.find({ task_id: { $in: task_ids } }).lean();

        for (let i = 0; i < result.length; i++) {
            let uobj = await get_user_obj(result[i].created_by);
            result[i].history = Array.isArray(result[i].history) ? result[i].history : [];
            result[i].created_by_name = uobj.firstname + ' ' + uobj.lastname;
            // if(result[i].user_rating){
            //     result[i].rate = result[i].user_rating.hasOwnProperty(decode.id) ? result[i].user_rating[decode.id] : result[i].rate
            // }
            
            switch (result[i].rate) {
                // case 0: summary.zero ++; break;
                case 1: summary.one++; break;
                case 2: summary.two++; break;
                case 3: summary.three++; break;
                case 4: summary.four++; break;
                case 5: summary.five++; break;
            }
            if (result[i].rate > 0) summary.total_review++;
            result[i].other_history = [];
            for (let j = 0; j < user_review.length; j++) {
                if (user_review[j].task_id === result[i].task_id?._id.toString()) {
                    uobj = await get_user_obj(user_review[j].created_for);
                    result[i].other_history.push({
                        rate: user_review[j].rate,
                        review_body: user_review[j].review_body,
                        created_for: user_review[j].created_for,
                        created_for_name: uobj.firstname + ' ' + uobj.lastname,
                        data: user_review[j].last_updated_at
                    });
                }
            }
        }
        summary.efficiency_score = (5 * summary.five + 4 * summary.four + 3 * summary.three + 2 * summary.two + 1 * summary.one) / summary.total_review;

        summary.five = ((summary.five * 100) / summary.total_review).toFixed(2);
        summary.four = ((summary.four * 100) / summary.total_review).toFixed(2);
        summary.three = ((summary.three * 100) / summary.total_review).toFixed(2);
        summary.two = ((summary.two * 100) / summary.total_review).toFixed(2);
        summary.one = ((summary.one * 100) / summary.total_review).toFixed(2);
        summary.efficiency_score = ((summary.efficiency_score * 100) / 5).toFixed(2);

        summary.five = isNaN(summary.five) ? 0 : summary.five;
        summary.four = isNaN(summary.four) ? 0 : summary.four;
        summary.three = isNaN(summary.three) ? 0 : summary.three;
        summary.two = isNaN(summary.two) ? 0 : summary.two;
        summary.one = isNaN(summary.one) ? 0 : summary.one;
        summary.efficiency_score = isNaN(summary.efficiency_score) ? 0 : summary.efficiency_score;

        res.status(200).json({ success: true, result, summary, incomplete: incomplete.length });
    } catch (e) {
        res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
    }
});

/**
 * This route use for task review update
 * 
 */
router.put('/review_update', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    try {
        req.body.last_updated_at = new Date();
        req.body.last_reviewed_by = decode.firstname + " " + decode.lastname;
        if(req.body.review_body){
            req.body.last_commented_by = decode.firstname + " " + decode.lastname;
            req.body.last_comment = req.body.review_body;

        }
        
        
        Review.findOne({ _id: req.body._id }, async function (e, doc) {
            if (e) console.log(768, e);
            if (doc && doc.rate > 0)
                req.body.history = [...doc.history, { date: doc.last_updated_at, rate: doc.rate, review_body: doc.review_body }];

            console.log("11111111111111111111111111111111111111");
            req.body["$set"] = { ["user_rating."+decode.id]: req.body.rate };
            let review = await Review.findOneAndUpdate({ _id: req.body._id }, req.body);
            await Task.findOneAndUpdate({ _id: review.task_id }, { $pull: { review_status: decode.id } });

            review = await Review.find({ _id: req.body._id }).lean();

            var uobj = await get_user_obj(review[0].created_by);
            save_notification({
                company_id: decode.company_id,
                receiver_id: [review[0].created_by, review[0].created_for],
                title: `Review has been updated`,
                type: "Task",
                body: JSON.stringify(review[0]),
                user_id: review[0].created_by,
                user_name: uobj.firstname + ' ' + uobj.lastname,
                user_img: uobj.img,
                task_id: req.body._id,
                tab: "property",
                decode_id: decode.id
            }, req);

            res.status(200).json({ success: true, review: review[0], data: req.body });
        });
    } catch (e) {
        res.status(400).json({ success: false, error: e, msg: 'Unexpected error.' });
    }
});

/**
 * Create new task
 * and update full task document 
 * 
 */
router.post('/discussion_save', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let check_body_data = isValidJson(JSON.stringify(req.body));
    const requiredKeys = ["task_id", "conversation_id", "msg_id", "msg_body"];
    let missingKeys = validateJSON(req.body, requiredKeys);
    if (missingKeys.length === 0) {
        if (check_body_data) {

            let data = {
                ...req.body,
                company_id: decode.company_id
            };

            var bytes = CryptoJS.AES.decrypt(data.msg_body, process.env.CRYPTO_SECRET);
            data.msg_body = bytes.toString(CryptoJS.enc.Utf8);

            data.msg_text = data.msg_text ? data.msg_text : data.msg_body.toString();
            // data.msg_status = (data.participants).filter((a) => { return a != data.sender; });
            data.msg_type = data.msg_type ? data.msg_type : 'text';

            try {
                let newTaskDiscussion = new Messages(data);
                await newTaskDiscussion.save();
                newTaskDiscussion.msg_body = CryptoJS.AES.encrypt(newTaskDiscussion.msg_body, process.env.CRYPTO_SECRET).toString();
                return res.status(200).json({ success: true, TaskDiscussion: newTaskDiscussion, msg: "Create Task Discussion Successfully." });
            } catch (error) {
                if (error instanceof mongoose.Error.StrictModeError) {
                    console.log('Validation error:', error.message);
                } else {
                    console.log('Unexpected error:', error.message);
                }
                return res.status(400).json({ success: false, error });
            }
        } else {
            return res.status(400).json({ success: false, msg: "Data is not valid" })
        }
    } else {
        return res.status(400).json({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}` });
    }
});

/**
 * This route use for task dashboard
 * 
 * members = optional
 */
router.post('/discussion_list', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    // var query = { campaign_id: new ObjectId(campaign._id) };
    let list = await Messages.find({ task_id: new ObjectId(req.body.task_id) }).exec();
    let discussion_list = [];
    list.forEach((v) => {
        let item = new Messages(v)
        item["msg_body"] = CryptoJS.AES.encrypt(v.msg_body, process.env.CRYPTO_SECRET).toString();
        discussion_list.push(item)
    })

    return res.status(200).json({ success: true, discussion_list: discussion_list });
});

/**
 * This route use for get task notification
 * 
 */
router.get('/notification', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    try {
        let notification = await get_notification({ tab: 'global', user_id: decode.id, company_id: decode.company_id, page: req.query.page || 1 });
        return res.status(200).json({ success: true, notification });
    } catch (e) {
        return res.status(400).json({ success: false, error: e });
    }
});

router.post('/read_all_discussion', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);

    Messages.updateMany({ task_id: req.body._id }, { $pull: { msg_status: decode.id } },{ multi: true })
    .then(async function (status) {
        // xmpp_send_server(decode.id, 'read_discussion_msg', { task_id: req.body._id }, req);
        return res.status(200).json({ success: true, message: "All discussion msg read" });

    }).catch((e) => {
        console.log(3454, e);
        callback({ status: true, message: "no reply msg read" });
    });

   
});
router.post('/read_notification', async (req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);

    var update_data = { $pull: { [req.body.type]: decode.id } };
    await Task.findOneAndUpdate({ _id: req.body._id }, update_data );

    var tasks = await Task.find({ _id: req.body._id }).lean();
    if(!tasks[0].view_cost?.includes(decode.id) && 
    !tasks[0].view_hour?.includes(decode.id) && 
    !tasks[0].view_description?.includes(decode.id) &&
    // !tasks[0].view_note?.includes(decode.id) &&
    !tasks[0].view_checklist?.includes(decode.id)
    ){
        update_data.$pull = {view_update: decode.id}
    }
    await Task.findOneAndUpdate({ _id: req.body._id }, update_data );

    return res.status(200).json({ success: true, message: "All discussion msg read" });

});

module.exports = router;
