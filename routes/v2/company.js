const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const isUuid = require('uuid-validate');
const isEmpty = require("is-empty");

// var { models } = require('../../config/db/express-cassandra');
var { xmpp_send_server } = require('../../v2_utils/voip_util');
var { update_user_obj } = require('../../v2_utils/user');

const { validateCompanyInput, validateGet, validateUpdateInput } = require('../../validation/company');
// const { default: isEmail } = require('validator/lib/isemail');
var { bucket_size } = require('../../v2_utils/bucket');
var { count_company_users, update_company, billing_address, billing_company, payment_method } = require('../../v2_utils/company');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var Conversation = require('mongoose').model('conversation')
var Company = require('mongoose').model('company')
var Address = require('mongoose').model('address')
var Users = require('mongoose').model('user')
const { v4: uuidv4 } = require('uuid');
router.get('/usage', passport.authenticate('jwt', { session: false }), async(req, res) => {
    const token = extractToken(req);
    try {
        var decode = jwt.verify(token, process.env.SECRET);
        // console.log(19, all_company[decode.company_id]);
        // var storage = await bucket_size(decode);
        var active_users = await count_company_users(decode.company_id, 1);
        var inactive_users = await count_company_users(decode.company_id, 0);
        var address = await billing_address(decode);
        var payment = await payment_method(decode);
        var company = await billing_company(decode);
        

        return res.status(200).json({
            plan_name: all_company[decode.company_id].plan_name,
            plan_user_limit: all_company[decode.company_id].plan_user_limit,
            plan_storage_limit: all_company[decode.company_id].plan_storage_limit,
            storage: 0,
            active_users,
            inactive_users,
            payment,
            company,
            address: address.length > 0 ? address[address.length - 1] : {}
        });
    } catch (e) {
        return res.status(400).json({ error: 'Error', message: e });
    }
});

/**
 * Add new compnay
 * company_name
 * domain_name = created by user email address domain_name
 * 
 */
router.post('/add', (req, res) => {
    // console.log('add company ', req.body);
    const { errors, isValid } = validateCompanyInput(req.body);
    if (!isValid) {
        return res.status(400).json({ error: 'Validation error', message: errors });
    }
    Company.findOne({ company_name: req.body.company_name }, function(error, company) {
        if (error) {
            return res.status(400).json({ error: 'Find error', message: error });
        } else if (company) {
            return res.status(400).json({ error: 'Duplicate error', message: 'Company already exists.' });
        } else {
            const company = new Company({
                company_id: uuidv4(),
                company_name: req.body.company_name
            });
            company.save().then(function() {
                return res.status(200).json({ message: 'Company added successfully.', company });
            }).catch(function(err) {
                return res.status(400).json({ error: err.name, message: err.message });
            });
        }
    });
});

router.post('/update_plan', passport.authenticate('jwt', { session: false }), (req, res) => {
    Company.findOne({ company_id: req.body.company_id }, async function(error, company) {
        if (error) {
            return res.status(400).json({ error: 'Find error', message: error });
        } else if (company) {
            try {
                var result = await update_company(req.body);
                if (req.body.plan_name) company.plan_name = req.body.plan_name;
                if (req.body.plan_user_limit) company.plan_user_limit = req.body.plan_user_limit;
                if (req.body.plan_storage_limit) company.plan_storage_limit = req.body.plan_storage_limit;

                return res.status(200).json({ message: 'Company plan update successfully.', company });
            } catch (e) {
                return res.status(400).json({ error: 'Error', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error', message: 'Company not found.' });
        }
    });
});

router.post('/update_address', passport.authenticate('jwt', { session: false }), (req, res) => {
    const token = extractToken(req);
    try {
        var decode = jwt.verify(token, process.env.SECRET);
        var address = {
            company_id: decode.company_id,
            first_name: req.body.first_name ? req.body.first_name : '',
            last_name: req.body.last_name ? req.body.last_name : '',
            email: req.body.email ? req.body.email : decode.email,
            street_address: req.body.street_address ? req.body.street_address : '',
            apt_number: req.body.apt_number ? req.body.apt_number : '',
            province: req.body.province ? req.body.province : '',
            zip: req.body.zip ? req.body.zip : '',
            created_by: req.body.created_by ? req.body.created_by : '',
            country: req.body.countryName ? req.body.countryName : '',
            company: req.body.companyName ? req.body.companyName : '',
            city: req.body.cityName ? req.body.cityName : '',
            

        };
        if (req.body.id) {
            address.id = req.body.id;
            address.updated_at = new Date();
            address.updated_by = decode.id;
        } else {
            address.id = uuidv4();
            address.created_by = decode.id;
        }
        // console.log(105, (new Date().getTime()).toString(), address);
        new Address(address).save().then(() => {
            return res.status(200).json({ status: true, address });
        }).catch((e) => {
            return res.status(400).json({ error: e.name, message: e.message });
        })
    } catch (e) {
        return res.status(400).json({ error: 'Error', message: e });
    }
});

router.post('/company_check', (req, res) => {
    // console.log('company_check ', req.body);
    const { errors, isValid } = validateCompanyInput(req.body);
    if (!isValid) {
        return res.status(400).json({ error: 'Validation error', message: errors });
    }
    Company.findOne({ company_name: req.body.company_name, domain_name: req.body.domain_name }, function(error, company) {
        if (error) return res.status(400).json({ error: 'Find error', message: error });
        else if (company) return res.status(400).json({ error: 'Duplicate error', message: 'Company already exists.' });
        else return res.status(200).json({ message: 'Temp Company added successfully.' });
    });
});

router.post('/get', (req, res) => {
    console.log(req.query, req.params, req.body);
    const { query, errors, isValid } = validateGet(req.body);
    if (!isValid)
        return res.status(400).json({ error: 'Validation error', message: errors });

    Company.find(query, { raw: true, allow_filtering: true }).then(function(error, companies) {
        // if (error) return res.status(400).json({ error: 'Find error', message: error });
        // else {
            return res.status(200).send(companies);
        // }
    });
});

/**
 * Get My All Company
 * 
 */
router.get('/get_all', passport.authenticate('jwt', { session: false }), async(req, res) => {
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    let query = { email: decode.email };
    Users.find(query, function(e, users) {
        if (e) return res.status(400).json({ error: 'Find error', message: e });

        var companies = [];
        users.forEach(function(v, k) {
            if (all_company[v.company_id.toString()] && all_company[v.company_id.toString()].created_by !== null && all_company[v.company_id.toString()].created_by.toString() === v.id.toString() && all_company[v.company_id.toString()].is_deactivate < 2) {
                let me = all_users[v.company_id.toString()][all_company[v.company_id.toString()].created_by];
                all_company[v.company_id.toString()].created_by_name = me.firstname + " " + me.lastname;
                all_company[v.company_id.toString()].user_role = 'Admin';
                companies.push(all_company[v.company_id.toString()]);
            }
        });

        return res.status(200).send(companies);
    });
});

/**
 * Get Company Participants
 * 
 */
router.get('/get_participants/:company_id?', passport.authenticate('jwt', { session: false }), async(req, res) => {
    console.log(req.query.company_id);
    const token = extractToken(req);
    let decode = await token_verify_decode(token);
    req.params = req.query.company_id ? req.query : req.params;
    if (all_users.hasOwnProperty(req.params.company_id)) {
        return res.status(200).send(Object.values(all_users[req.params.company_id]));
    } else {
        return res.status(400).json({ error: 'Error', message: 'company not found' });
    }
});

/**
 * Update Company
 * company_id
 * company_name
 * 
 */
router.post('/update', passport.authenticate('jwt', { session: false }), async(req, res) => {
    const token = extractToken(req);
    const { query, errors, isValid } = validateUpdateInput(req.body);
    if (!isValid)
        return res.status(400).json({ error: 'Validation error', message: errors });

    let decode = await token_verify_decode(token);
    let domain = decode.email.split("@");
    Company.findOne({ company_name: req.body.company_name, domain_name: domain[1] }, function(error, company) {
        if (error) return res.status(400).json({ error: 'Find error', message: error });
        else if (company) return res.status(400).json({ error: 'Duplicate error', message: 'Company already exists.' });
        else {
            // console.log(104, query);
            Company.updateOne(query, { company_name: req.body.company_name },  function(e) {
                if (e) return res.status(400).json({ error: 'Update error', message: e });
                else {
                    all_company[req.body.company_id].company_name = req.body.company_name;
                }
                return res.status(200).send(all_company[req.body.company_id]);
            });
        }
    });
});

/**
 * Delete Company
 * company_id
 * 
 */
router.delete('/delete/:company_id?', passport.authenticate('jwt', { session: false }), async(req, res) => {
    const token = extractToken(req);
    req.params = req.query.company_id ? req.query : req.params;
    if (!isUuid(req.params.company_id))
        return res.status(400).json({ error: 'Validation error', message: 'Company id invalid.' });

    let decode = await token_verify_decode(token);

    Conversation.find({ company_id: req.params.company_id }, function(error, convs) {
        if (error) return res.status(400).json({ error: 'Find error', message: error });
        else if (convs.length > 0) return res.status(400).json({ error: 'Used error', message: 'Company already used.' });
        else {
            // console.log(149, all_company[req.params.company_id]);
            if (all_company[req.params.company_id].member == 0) {
                Company.delete({ company_id: req.params.company_id }, function(e) {
                    if (e) return res.status(400).json({ error: 'Company delete error', message: e });
                    else {
                        delete all_company[req.body.company_id];
                        return res.status(200).send({ status: true, message: "Company delete successfully.", company_id: req.params.company_id });
                    }
                });
            } else if (all_company[req.params.company_id].member == 1) {
                let user = Object.values(all_users[req.params.company_id]);
                Users.delete({ id: user[0].id }, function(err) {
                    if (err) return res.status(400).json({ error: 'User delete error', message: err });

                    Company.delete({ company_id: req.params.company_id }, function(e) {
                        if (e) return res.status(400).json({ error: 'Company delete error', message: e });
                        else {
                            delete all_company[req.body.company_id];
                            return res.status(200).send({ status: true, message: "Company delete successfully.", company_id: req.params.company_id });
                        }
                    });
                });
            } else {
                return res.status(400).json({ error: 'Used error', message: 'Please remove all user then try again.' });
            }
        }
    });
});

/**
 * this route use for deactivate a company
 * company_id
 * do_active = yes/ no/ permanent_delete
 * 
 * is_deactivate = 0 = company active
 * is_deactivate = 1 = company deactive
 * is_deactivate = 2 = company permanent delete
 */
router.patch('/deactivate/:company_id?/:do_active?', passport.authenticate('jwt', { session: false }), async(req, res) => {
    const token = extractToken(req);
    req.params = req.query.company_id ? req.query : req.params;
    if (!isUuid(req.params.company_id))
        return res.status(400).json({ error: 'Validation error', message: 'Company id invalid.' });
    try {
        var decode = await token_verify_decode(token);
    } catch (e) {
        console.log(241, e);
    }
    // console.log(240, req.params);
    var queries = [];
    var usersQ = [];
    let is_active = req.params.do_active && req.params.do_active == 'yes' ? 1 : 0;
    if (all_company[req.params.company_id].created_by == decode.id) {
        // console.log(244, is_active);
        let this_company_user = Object.values(all_users[req.params.company_id]);
        if (req.params.do_active === 'permanent_delete' && all_company[req.params.company_id].is_deactivate == 1) {
            usersQ.push({ updateOne: { filter:{ id: decode.id }, update:{ is_active: 0 } } });

            queries.push({updateOne:{filter:{ company_id: req.params.company_id }, update:{ is_deactivate: 2 }}});

            try{
                await Promise.all([
                    Users.bulkWrite(usersQ),
                    Company.bulkWrite(queries)
                  ]);
                all_company[req.params.company_id].is_deactivate = 2;
                all_users[req.params.company_id][decode.id].is_active = 0;
                xmpp_send_server(decode.id, 'logout_from_all', { device_id: decode.device_id });
                return res.status(200).send({ status: true, message: "Company permanent delete." });
            }catch(err){
                return res.status(400).send({ error: "Update error", message: err });
            }
        } else {
            if (is_active === 0) {
                for (let i = 0; i < this_company_user.length; i++) {
                    if (this_company_user[i].id != decode.id) {
                        all_users[req.params.company_id][this_company_user[i].id].is_active = 0;
                        this_company_user[i].is_active = 0;
                        try {
                            usersQ.push({filter:{ id: this_company_user[i].id }, update:{ is_active }});
                        } catch (e) {
                            console.log(245, e);
                        }
                    }
                }
            } else if (is_active === 1) {
                // console.log(260);
                Users.find({ company_id: req.params.company_id }, async function(e, users) {
                    for (let i = 0; i < users.length; i++) {
                        // console.log(263);
                        if (users[i].id.toString() != decode.id) {
                            // console.log(265);
                            users[i].is_active = is_active;
                            try {
                                // console.log(268);
                                update_user_obj(users[i]);
                                usersQ.push({filter:{ id: users[i].id }, update:{ is_active }});
                            } catch (e) {
                                console.log(260, e);
                            }
                        }
                    }

                    try{
                        await Promise.all([
                            Users.bulkWrite(usersQ),
                            Company.bulkWrite(queries)
                          ]);
                        // console.log(284, "user update")
                    }catch(err){
                        return res.status(400).send({ error: "Update error", message: err });
                    }
                })
            }
            // console.log(278);
            try {
                queries.push({filter:{ company_id: req.params.company_id }, update:{ is_deactivate: is_active === 0 ? 1 : 0 }});
                
                // console.log(243, queries);
            } catch (e) {
                console.log(243, e);
            }

            if (queries.length > 0) {
                // console.log(287);

                try{
                    await Promise.all([
                        Users.bulkWrite(usersQ),
                        Company.bulkWrite(queries)
                      ]);
                      all_company[req.params.company_id].is_deactivate = is_active === 0 ? 1 : 0;
                      if (is_active === 0) {
                          for (let i = 0; i < this_company_user.length; i++) {
                              if (this_company_user[i].id != decode.id) {
                                //   console.log(252, this_company_user[i].id);
                                  xmpp_send_server(this_company_user[i].id, 'logout_from_all', { device_id: this_company_user[i].device_id });
                              }
                          }
                          return res.status(200).send({ status: true, message: "All users are now deactivated." });
                      } else {
                        //   console.log(299);
                          return res.status(200).send({ status: true, message: "All users are now reactivated." });
                      }
                }catch(err){
                    return res.status(400).send({ error: "Update error", message: err });
                }
            }
        }
    } else {
        return res.status(401).json({ error: 'Ownership error', message: 'You are not the company admin.' });
    }
});


module.exports = router;