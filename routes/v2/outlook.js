const router = require("express").Router();
// const { ConfidentialClientApplication } = require("@azure/msal-node");
const graph = require('../graph');
const dateFns = require('date-fns');
// const zonedTimeToUtc = require('date-fns-tz/zonedTimeToUtc');
const iana = require('windows-iana');
// const { body, validationResult } = require('express-validator');
// const validator = require('validator');
var moment = require('moment-timezone');
const { htmlToText } = require('html-to-text');
var Users = require('mongoose').model('user');
const jwt = require('jsonwebtoken');
var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { xmpp_send_server } = require('../../v2_utils/voip_util');

router.get('/signin', async function (req, res) {
  let redirectUri = (new URL('/v2/outlook/callback', new URL(process.env.API_SERVER_URL).origin)).href;
  console.log('signin:redirectUri', redirectUri);
  const scopes = process.env.OAUTH_SCOPES || 'https://graph.microsoft.com/.default';
  const urlParameters = {
    scopes: scopes.split(','),
    redirectUri: redirectUri,
    state: req.query.userId,
  };

  try {
    const authUrl = await req.app.locals.msalClient.getAuthCodeUrl(urlParameters);
    res.redirect(authUrl);
  }
  catch (error) {
    res.status(404).json({ error });
    // req.flash('error_msg', {
    //   message: 'Error getting auth URL',
    //   debug: JSON.stringify(error, Object.getOwnPropertyNames(error))
    // });
    // res.redirect('/');
  }
});
router.get('/callback', async function (req, res) {
  const scopes = process.env.OAUTH_SCOPES || 'https://graph.microsoft.com/.default';
  let redirectUri = (new URL('/v2/outlook/callback', new URL(process.env.API_SERVER_URL).origin)).href;
  const tokenRequest = {
    code: req.query.code,
    scopes: scopes.split(','),
    redirectUri: redirectUri
  };

  try {
    const userId = req.query.state;
    const response = await req.app.locals.msalClient.acquireTokenByCode(tokenRequest);
    // const refreshToken = response.refreshToken;
    // Save the user's homeAccountId in their session
    console.log('outlook:account', response.account)
    req.session.userId = response.account.homeAccountId;
    const user = await graph.getUserDetails(req.app.locals.msalClient, req.session.userId);
    if (response.account) {
      await Users.updateOne({ id: userId }, {
        outlookAccount: response.account,
        outlook_id: response.account.homeAccountId,
        outlookTokens: user
      });
    }
    xmpp_send_server(userId, 'calendar_outlook', {outlook_id: response.account.homeAccountId}, req);
    
  } catch (error) {
    console.log('outlook:api:error', error);
    // req.flash('error_msg', {
    //   message: 'Error completing authentication',
    //   debug: JSON.stringify(error, Object.getOwnPropertyNames(error))
    // });
  }
  res.send('Outlook Authentication successful! You can now close this window.');
  // res.redirect('/v2/outlook');
});
router.get('/token', (req, res) => {
  const token = extractToken(req);
  let decode = jwt.verify(token, process.env.SECRET);
  Users.findOne({ id: decode.id }, function (e, user) {
    if (e) return res.status(400).json({ error: 'Find error', message: e });
    res.status(200).json({ outlook_id: user.outlook_id || null });
  });
});
router.get('/events', async function (req, res) {
  const token = extractToken(req);
  let decode = jwt.verify(token, process.env.SECRET);
  var dbuser = await Users.findOne({ id: decode.id }).lean();
  if (!dbuser || !dbuser.outlook_id) {
    console.log('User or token not found.');
    return;
  }

  req.session.userId = dbuser.outlook_id;
  var timeStart = moment().startOf('year').toISOString(true);
  var timeEnd = moment().endOf("year").toISOString(true);
  let event_list = [];

  try {
    // Get the events
    const events = await graph.getCalendarView(
      req.app.locals.msalClient,
      req.session.userId,
      timeStart,
      timeEnd,
      req.query.timeZone || "Greenwich Standard Time",
      dbuser);

    if (events?.value?.length) {
      event_list.push(...events?.value?.map(v => ({
        _id: String(v.id),
        title: v.subject,
        start: moment.tz(v.start.dateTime, req.query.timeZone).format(), 
        allDay: v?.isAllDay,
        ...(v?.end?.dateTime && { end: moment.tz(v.end.dateTime, req.query.timeZone).format() }),
        description: htmlToText(v.body.content),
        type: "outlook",
        calendarId: "",
      })));
    } else {
      console.log(`No upcoming events found`);
    }
  } catch (err) {
    res.status(404).json({ err });
  }
  res.status(200).json({ events: event_list });
  
});
router.post('/create', async function (req, res) {
  const token = extractToken(req);
  let decode = jwt.verify(token, process.env.SECRET);
  var dbuser = await Users.findOne({ id: decode.id }).lean();
  if (!dbuser || !dbuser.outlook_id) { console.log('User or token not found.'); return; }
  req.session.userId = dbuser.outlook_id;
  let timeZone = req.body.timeZone || "Greenwich Standard Time";
  
  const formData = {
    subject: req.body.title,
  };
  if (req.body.description) {
    formData.body = {
      contentType: 'text', // or 'Text' depending on your needs
      content: req.body.description
    };
  }
  let start_time = req.body.allDay ? moment.tz(req.body.start, timeZone).startOf('day').format() : moment.tz(req.body.start, timeZone).format() ;
  let end_time = req.body.allDay ? moment.tz(req.body.end, timeZone).endOf('day').add(1, 'seconds').format() :  moment.tz(req.body.end, timeZone).format();
  if (req.body.start) {
    formData.start = {
      dateTime: req.body.allDay? start_time.split('T')[0] : start_time,
      "timeZone": timeZone
    }
  }
  if (req.body.end) {
    formData.end = {
      dateTime: req.body.allDay ? end_time.split('T')[0]: end_time,
      "timeZone": timeZone
    }
  }
  formData.isAllDay = req.body.allDay;
  // Create the event
  var newEvent=null;
  try {
    newEvent = await graph.createEvent(
      req.app.locals.msalClient,
      req.session.userId,
      formData,
      dbuser
    );
  } catch (error) {
    res.status(404).json({ error });
  }
  res.status(200).json({ status: true, newEvent });
});
router.post('/update', async function (req, res) {
  const token = extractToken(req);
  let decode = jwt.verify(token, process.env.SECRET);
  var dbuser = await Users.findOne({ id: decode.id }).lean();
  if (!dbuser || !dbuser.outlook_id) { console.log('User or token not found.'); return;}
  req.session.userId = dbuser.outlook_id;
  let timeZone = req.body.timeZone || "Greenwich Standard Time";
  const formData = {
    subject: req.body.title,
  };
  if (req.body.description) {
    formData.body = {
      contentType: 'text', // or 'Text' depending on your needs
      content: req.body.description
    };
  }
  let start_time = req.body.allDay ? moment.tz(req.body.start, timeZone).startOf('day').format() : moment.tz(req.body.start, timeZone).format() ;
  let end_time = req.body.allDay ? moment.tz(req.body.end, timeZone).endOf('day').add(1, 'seconds').format() :  moment.tz(req.body.end, timeZone).format();
  if (req.body.start) {
    formData.start = {
      dateTime: req.body.allDay? start_time.split('T')[0] : start_time,
      "timeZone": timeZone
    }
  }
  if (req.body.end) {
    formData.end = {
      dateTime: req.body.allDay ? end_time.split('T')[0]: end_time,
      "timeZone": timeZone
    }
  }
  formData.isAllDay = req.body.allDay;
  var event = null
  try {
    event = await graph.updateEvent(
      req.app.locals.msalClient,
      req.session.userId,
      req.body._id,
      formData,
      dbuser);

  } catch (err) {
    res.status(404).json({ err });
    // req.flash('error_msg', {
    //   message: 'Could not fetch events',
    //   debug: JSON.stringify(err, Object.getOwnPropertyNames(err))
    // });
  }

  res.status(200).json({ status: true, event });
  // }
});
router.post('/delete', async function (req, res) {
  const token = extractToken(req);
  let decode = jwt.verify(token, process.env.SECRET);
  var dbuser = await Users.findOne({ id: decode.id }).lean();
  if (!dbuser || !dbuser.outlook_id) {
    console.log('User or token not found.');
    return;
  }

  req.session.userId = dbuser.outlook_id;
  const user = dbuser.outlookTokens;

  try {
    const events = await graph.deleteEvent(
      req.app.locals.msalClient,
      req.session.userId,
      req.body._id,
      dbuser);

  } catch (err) {
    res.status(404).json({ err });
    // req.flash('error_msg', {
    //   message: 'Could not fetch events',
    //   debug: JSON.stringify(err, Object.getOwnPropertyNames(err))
    // });
  }

  res.status(200).json({ status: true });
  // }
});

router.get('/signout',
  async function (req, res) {
    // Sign out
    if (req.session.userId) {
      // Look up the user's account in the cache
      const accounts = await req.app.locals.msalClient
        .getTokenCache()
        .getAllAccounts();

      const userAccount = accounts.find(a => a.homeAccountId === req.session.userId);

      // Remove the account
      if (userAccount) {
        req.app.locals.msalClient
          .getTokenCache()
          .removeAccount(userAccount);
      }
    }

    // Destroy the user's session
    req.session.destroy(function () {
      res.redirect('/');
    });
  }
);

module.exports = router;
