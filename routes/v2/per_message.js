const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const _ = require('lodash');
const isUuid = require('uuid-validate');
var moment = require('moment');
const short = require('short-uuid');
var CryptoJS = require("crypto-js");

// const Conversation = require('../../mongo-models/conversation');
const Conversation = require('mongoose').model('conversation');
const Users = require('mongoose').model('user');
const File = require('../../mongo-models/file');
var Messages = require('../../mongo-models/message');
var Link = require('../../mongo-models/link');
const Task_discussion = require('../../mongo-models/task_discussion');
// const Users = require('../../mongo-models/user');

var { extractToken, token_verify_decode } = require('../../v2_utils/jwt_helper');
var { znMsgUpdate,znPrivetMsgUpdate,znReadMsgUpdateCounter } = require('../../v2_utils/zookeeper');

var {
    get_message,
    delete_msg,
    remove_this_line,
    forward_msg,
    edit_msg,
    file_share,
    check_reac_emoji_list,
    add_reac_emoji,
    delete_reac_emoji,
    update_reac_emoji,
    view_reac_emoji_list,
    flag_unflag,
    msgs_seen,
    url_title_update,
    edit_private_participants,
    msg_title_update,
    update_one_link_title,
    has_reply_attach_update,
    update_task,
    get_discussion,
    is_the_last_msg_of_this_conv,
    update_conversation_for_last_msg,
    get_the_last_msg_of_this_conv,
    get_rep_message
} = require('../../v2_utils/message');
var { get_user_obj } = require('../../v2_utils/user');
var { update_tag_count } = require('../../v2_utils/tag');
// var { models } = require('../../config/db/express-cassandra');
var { validateEmojiArg, validateFlagArg, validateMsgTaskArg, validateSeenArg, validateTaskArg } = require('../../validation/message');
var { save_notification } = require('../../v2_utils/notification');
var { valdateDateArg, valdateForwardeArg, valdateFileShareArg, valdateEditArg, valdateSecretUserArg } = require('../../validation/home');
var {
    xmpp_send_server,
    xmpp_send_broadcast,
    send_msg_firebase
} = require('../../v2_utils/voip_util');
var { get_msgs_checklist, get_msgs_task } = require('../../v2_utils/message_checklist');
var { get_conversation } = require('../../v2_utils/conversation');
const { mode } = require('crypto-js');
// const { vary } = require('express/lib/response');

function parseJSONSafely(str) {
    if (str === null || str === 'null') return [];
    try {
        return JSON.parse(str);
    }
    catch (e) {
        if (typeof str == 'string') return [str];
        else return [];
    }
}

function getAlink(id){
    return new Promise((resolve)=>{
        Link.find({url_id: String(id)}, function(e, l){
            if(e || !l) {
                console.log(1171, e);
                resolve([]);
            }
            else{
                resolve(l);
            }
        }).lean();
    })
}


async function routes (fastify, options) {
    /**
     * This route is used for delete a message and reply msg
     * conversation_id
     * msg_id
     * participants
     * delete_type = for_me, for_all
     * is_reply_msg = yes, no
     */
    
    fastify.post('/delete_msg', async (req, res) => {
    
        const token = extractToken(req);
        // console.log('token is:', token);
        if (token) {
            var decode = await token_verify_decode(token);
    
            const { errors, isValid } = valdateDateArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            // console.log('delete_msg_api:', 'api is valid');
            try {
                req.body.company_id = decode.company_id;
                req.body.user_id = decode.id.toString();
    
                delete_msg(req.body, async function (response) {
                    if (response.status == true) {
                        if (req.body.delete_type == 'for_all') {
                            var uobj = await get_user_obj(decode.id);
                            save_notification({
                                company_id: decode.company_id,
                                receiver_id: req.body.participants,
                                title: uobj.firstname + " " + uobj.lastname + " delete a message.",
                                type: "message",
                                body: JSON.stringify(req.body),
                                user_id: decode.id,
                                user_name: uobj.firstname + " " + uobj.lastname,
                                user_img: uobj.img
                            }, req);
                            let islm = false;
                            let lm = {};
                            if(response.hasOwnProperty('msg')){
                                islm = await is_the_last_msg_of_this_conv(response["msg"]);
                                if(islm){
                                    lm = await get_the_last_msg_of_this_conv(response["msg"]);
                                    if(lm)
                                        update_conversation_for_last_msg(lm);
                                }
                            }
                            
                            req.body.participants.forEach(function (v, k) {
                                xmpp_send_server(v, 'delete_msg', req.body, req);
                                send_msg_firebase(v, 'delete_msg', req.body, req);
                                if(islm){
                                    xmpp_send_server(v, 'delete_msg_update_conversation_last_msg', lm, req);
                                    send_msg_firebase(v, 'delete_msg_update_conversation_last_msg', lm, req);
                                }
                            });
                        } else if (req.body.delete_type == 'for_me') {
                            xmpp_send_server(decode.id, 'delete_msg', req.body, req);
                        }
                        return res.status(200).json({ status: true, messages: 'Message delete successfully.' });
                    } else {
                        return res.status(400).json({ error: 'Error', message: response.error });
                    }
                });
    
            } catch (e) {
                return res.status(400).json({ error: 'Error', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error', message: "Token missing." });
        }
    });
    /**
     * This router use for add or remove flag
     * 
     * conversation_id
     * msg_id
     * is_add = no/ yes
     * 
     */
    fastify.post('/flag_unflag', async function (req, res) {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            const { errors, isValid } = validateFlagArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                flag_unflag(req.body.msg_id, decode.id.toString(), req.body.is_add, req.body.conversation_id, (result) => {
                    if (result.status) {
                        result.data = req.body;
                        // io.to(decode.id.toString()).emit('flag_unflag', result);
                        xmpp_send_server(decode.id.toString(), 'flag_unflag', result, req);
                        return res.status(200).json(result);
                    } else {
                        return res.status(400).json({ error: 'Flag Error 1', message: result });
                    }
                });
            } catch (e) {
                return res.status(400).json({ error: 'Flag Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Flag Error 3', message: "Token missing." });
        }
    });
    
    /**
     * This router use for view user who given this emoji
     * 
     * msg_id
     * emoji
     * 
     */
    fastify.post('/get_emoji_user_list', function (req, res) {
        if (isUuid(req.body.msg_id)) {
            try {
                view_reac_emoji_list(req.body.msg_id, req.body.emoji, (result) => {
                    return res.status(200).json({ status: true, result: result.result });
                });
            } catch (e) {
                return res.status(400).json({ error: 'Emoji Error', message: e });
            }
        } else {
            return res.status(400).json({ status: false, error: 'Validation error', message: { errors: 'Message id invalid.' } });
        }
    });
    
    /**
     * This router use for add, delete or update an emoji
     * Emoji lists: grinning, joy, open_mouth, disappointed_relieved, rage, thumbsup, thumbsdown, heart, folded_hands, check_mark
     * 
     * conversation_id
     * msg_id
     * emoji
     * participants
     * is_reply_msg = yes/ no
     * root_msg_id = (is_reply_msg == yes) ? root_msg_id : "";
     * 
     */
    fastify.post('/add_reac_emoji', async function (req, res) {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            const { errors, isValid } = validateEmojiArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                check_reac_emoji_list(req.body.msg_id, decode.id.toString(), async (result) => {
                    if (result.status) {
                        if (result.result.length == 0) {
                            // add first time like/reaction
                            add_reac_emoji(req.body.conversation_id, req.body.msg_id, decode.id.toString(), decode.firstname + ' ' + decode.lastname, req.body.emoji, decode.company_id, req.body.participants, async (result1) => {
                                var uobj = await get_user_obj(decode.id);
                                save_notification({
                                    company_id: decode.company_id,
                                    receiver_id: req.body.participants,
                                    title: uobj.firstname + " " + uobj.lastname + " add a reaction into message.",
                                    type: "message",
                                    body: JSON.stringify(req.body),
                                    user_id: decode.id,
                                    user_name: uobj.firstname + " " + uobj.lastname,
                                    user_img: uobj.img
                                }, req);
    
                                req.body.result = result1;
                                req.body.message = 'Add emoji successfully.';
                                // io.emit('add_reac_emoji', req.body);
                                // xmpp_send_broadcast('add_reac_emoji', req.body);
                                req.body.participants.forEach(function (v, k) {
                                    xmpp_send_server(v, 'add_reac_emoji', req.body, req);
                                });
                                return res.status(200).json({ status: true, message: 'Add emoji successfully.', data: result1 });
                            });
                        } else {
                            if (result.result[0].emoji_name == req.body.emoji) {
                                // delete same user same type reaction
                                delete_reac_emoji(req.body.conversation_id, req.body.msg_id, decode.id.toString(), req.body.emoji, req.body.participants, async (result2) => {
                                    var uobj = await get_user_obj(decode.id);
                                    save_notification({
                                        company_id: decode.company_id,
                                        receiver_id: req.body.participants,
                                        title: uobj.firstname + " " + uobj.lastname + " remove reaction from a message.",
                                        type: "message",
                                        body: JSON.stringify(req.body),
                                        user_id: decode.id,
                                        user_name: uobj.firstname + " " + uobj.lastname,
                                        user_img: uobj.img
                                    }, req);
    
                                    req.body.result = result2;
                                    req.body.message = 'Remove emoji successfully.';
                                    // io.emit('add_reac_emoji', req.body);
                                    // xmpp_send_broadcast('add_reac_emoji', req.body);
                                    req.body.participants.forEach(function (v, k) {
                                        xmpp_send_server(v, 'add_reac_emoji', req.body, req);
                                    });
                                    return res.status(200).json({ status: true, messages: 'Remove emoji successfully.', data: result2 });
                                });
                            } else {
                                update_reac_emoji(req.body.conversation_id, req.body.msg_id, decode.id.toString(), req.body.emoji, req.body.participants, async (result3) => {
                                    var uobj = await get_user_obj(decode.id);
                                    save_notification({
                                        company_id: decode.company_id,
                                        receiver_id: req.body.participants,
                                        title: uobj.firstname + " " + uobj.lastname + " update a reaction into message.",
                                        type: "message",
                                        body: JSON.stringify(req.body),
                                        user_id: decode.id,
                                        user_name: uobj.firstname + " " + uobj.lastname,
                                        user_img: uobj.img
                                    }, req);
    
                                    req.body.result = result3;
                                    req.body.message = 'Update emoji successfully.';
                                    // io.emit('add_reac_emoji', req.body);
                                    // xmpp_send_broadcast('add_reac_emoji', req.body);
                                    req.body.participants.forEach(function (v, k) {
                                        xmpp_send_server(v, 'add_reac_emoji', req.body, req);
                                    });
                                    return res.status(200).json({ status: true, messages: 'Update emoji successfully.', data: result3 });
                                });
                            }
                        }
                    } else {
                        return res.status(400).json({ error: 'Check emoji Error 1', message: result });
                    }
                });
            } catch (e) {
                return res.status(400).json({ error: 'Add remove emoji Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Add remove emoji Error 3', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'remove this line' a message and reply msg
     * conversation_id
     * msg_id
     * is_reply_msg = yes, no
     * participants
     */
    fastify.post('/remove_this_line', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            req.body.delete_type = 'remove_this_line';
            const { errors, isValid } = valdateDateArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.user_id = decode.id.toString();
                remove_this_line(req.body, function(response) {
                    if (response.status == true) {
                        io.emit('remove_this_line', response);
                        return res.status(200).json({ status: true, messages: 'Remove this line done.' });
                    } else {
                        return res.status(400).json({ error: 'Error', message: response.error });
                    }
                });
            } catch (e) {
                return res.status(400).json({ error: 'Error', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'forward' a message
     * conversation_id
     * msg_id
     * is_reply_msg = yes, no
     * conversation_lists
     * sender_img
     */
    fastify.post('/forward', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
    
            const { errors, isValid } = valdateForwardeArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.user_id = decode.id.toString();
                req.body.sender_name = decode.firstname + ' ' + decode.lastname ? decode.lastname : '';
                req.body.sender_img = decode.img;
                req.body.company_id = decode.company_id;
                // console.log(109, req.body);
                var response = await forward_msg(req.body, req);
                // console.log(response);
                if (response.status == true) {
                    // if(req.body.is_reply_msg == 'yes')
                    // 	io.emit('new_reply_message', response.msg);
                    // else
                    // 	io.emit('new_message', response.msg);
                    return res.status(200).json({ status: true, messages: 'message forwarded successfully.', data: response });
                } else {
                    return res.status(400).json({ error: 'Error', message: response.error });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'file(s) share'
     * conversation_lists
     * file_lists
     */
     fastify.post('/file_share', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            // console.log(177, req.body);
            const { errors, isValid } = valdateFileShareArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.sender = decode.id.toString();
                req.body.sender_name = decode.firstname + ' ' + decode.lastname ? decode.lastname : '';
                req.body.sender_img = decode.img;
                
                var response = await file_share(req.body, req);
                // console.log(response);
                if (response.status == true) {
                    // response.msg.msg_status.forEach(function(v, k){
                    // 	io.to(v).emit('new_message', response.msg);
                    // });
                    // io.to(decode.id.toString()).emit('new_message', response.msg);
                    return res.status(200).json(response);
                } else {
                    return res.status(400).json({ error: 'Error0', message: response.error });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error1', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error2', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'edit' a message
     * conversation_id
     * msg_id
     * new_msg_body
     * participants = array
     */
    fastify.post('/edit_msg', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
    
            const { errors, isValid } = valdateEditArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.user_id = decode.id.toString();
                req.body.company_id = decode.company_id;
                var response = await edit_msg(req.body);
                // console.log(response);
                if (response.status == true) {
                    var uobj = await get_user_obj(decode.id);
                    save_notification({
                        company_id: decode.company_id,
                        receiver_id: req.body.participants,
                        title: uobj.firstname + " " + uobj.lastname + " edit a message.",
                        type: "message",
                        body: JSON.stringify(response.msg),
                        user_id: decode.id,
                        user_name: uobj.firstname + " " + uobj.lastname,
                        user_img: uobj.img
                    }, req);
    
                    let islm = await is_the_last_msg_of_this_conv(response.msg);
                    if(islm){
                        update_conversation_for_last_msg(response.msg);
                    }
    
                    _.each(req.body.participants, function (v, k) {
                        if (response.msg.is_reply_msg == 'yes') {
                            // io.to(v).emit('edit_reply_message', response.msg);
                            xmpp_send_server(v, 'edit_reply_message', response.msg, req);
                            send_msg_firebase(v, 'edit_reply_message', response.msg, req);
                        } else {
                            // io.to(v).emit('edit_message', response.msg);
                            xmpp_send_server(v, 'edit_message', response.msg, req);
                            send_msg_firebase(v, 'edit_message', response.msg, req);
                            if(islm){
                                xmpp_send_server(v, 'edit_update_conversation_last_msg', response.msg, req);
                                send_msg_firebase(v, 'edit_update_conversation_last_msg', response.msg, req);
                            }
                        }
                    });
                    await znMsgUpdate(response.msg, req.body.participants, 'msg_update_edit');
                    return res.status(200).json({ status: true, messages: 'Message edit successfully.', data: response.msg });
                } else {
                    return res.status(400).json({ error: 'Error 1', message: response.error });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error 3', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'star file'
     * file_id
     */
    fastify.patch('/star_file', async (req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            if (!isUuid(req.body.file_id)) {
                return res.status(400).json({ error: 'Validation error', message: "File id is invalid" });
            }
            try {
                // console.log(218, req.body.file_id);
                var file = await File.findOne({ id: String(req.body.file_id), is_delete: 0 });
                if (file) {
                    file.star = Array.isArray(file.star) ? file.star : [];
                    let star = file.star.indexOf(decode.id) > -1 ? file.star.filter(item => item !== decode.id) : [...file.star, decode.id];
                   var file_update = await File.updateOne({ id: file.id, company_id: decode.company_id }, { star: star });
                   console.log(515,file.msg_id);
                    var fs = await File.find({ msg_id: file.msg_id, star: { $in: [decode.id] } });
                    console.log(514,fs);
                    if (fs.length) {
                        await Messages.updateOne({ conversation_id: file.conversation_id, msg_id: file.msg_id }, { $addToSet: { has_star: decode.id } });
                        var result = {
                            conversation_id: file.conversation_id,
                            msg_id: file.msg_id,
                            file_id: file.id,
                            file_bucket: file.bucket,
                            file_key: file.key,
                            star: star,
                            is_reply_msg: req.body.is_reply_msg,
                            task_id: file.task_id,
                            cost_id: file.cost_id
                        }
                        file.participants = Array.isArray(file.participants) ? file.participants : [];
                        file.participants.forEach(function (v, k) {
                            xmpp_send_server(v, 'star_file', result, req);
                            send_msg_firebase(v, 'star_file', result, req);
                        });
    
                        var mainmsg = (await get_message({ convid: file.conversation_id, msgid: file.msg_id, company_id: file.company_id, type: 'one_msg', limit: 1, user_id: decode.id })).msgs;
    
                      //  if(mainmsg.length) await znMsgUpdate(mainmsg[0], file.participants, 'msg_update_star_file');
    
                        return res.status(200).json(result);
    
                    } else {
                        await Messages.updateOne({ conversation_id: file.conversation_id, msg_id: file.msg_id }, { $pull: { has_star: decode.id } });
                        var result = {
                            conversation_id: file.conversation_id,
                            msg_id: file.msg_id,
                            file_id: file.id,
                            file_bucket: file.bucket,
                            file_key: file.key,
                            star: star,
                            is_reply_msg: req.body.is_reply_msg,
                            task_id: file.task_id,
                            cost_id: file.cost_id
    
                        }
                        file.participants = Array.isArray(file.participants) ? file.participants : [];
                        file.participants.forEach(function (v, k) {
                            xmpp_send_server(v, 'star_file', result, req);
                            send_msg_firebase(v, 'star_file', result, req);
                        });
                        
                        var mainmsg = (await get_message({ convid: file.conversation_id, msgid: file.msg_id, company_id: file.company_id, type: 'one_msg', limit: 1, user_id: decode.id })).msgs;
    
                      //  if(mainmsg.length) await znMsgUpdate(mainmsg[0], file.participants, 'msg_update_star_file');
    
                        return res.status(200).json(result);
    
                    }
                }
                
            } catch (e) {
                return res.status(400).json({ error: 'Error0', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error00', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'file(s) delete'
     * file_id
     * participants
     */
    fastify.delete('/file', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            if (!isUuid(req.body.file_id)) {
                return res.status(400).json({ error: 'Validation error', message: "File id is invalid" });
            }
            if (!Array.isArray(req.body.participants)) {
                return res.status(400).json({ error: 'Validation error', message: "Participants is not array" });
            }
            try {
                // console.log(218, req.body.file_id);
                var file = await File.find({ id: String(req.body.file_id) });
                file[0].tag_list = Array.isArray(file[0].tag_list) && file[0].tag_list.length > 0 ? file[0].tag_list : [];
    
                try {
                    if (file.length > 0 && file[0].user_id.toString() == decode.id && file[0].is_delete == 0) {
                        if(file[0].task_id){
                            var mainmsg = (await get_discussion({ task_id: file[0].task_id, convid: file[0].conversation_id.toString(), company_id: file[0].company_id.toString(), msgid: file[0].msg_id.toString(), limit: 1, is_reply: req.body.is_reply_msg, user_id: decode.id})).msgs;
                        }else{
                            var mainmsg = (await get_message({ convid: file[0].conversation_id.toString(), msgid: file[0].msg_id.toString(), company_id: file[0].company_id, is_reply: req.body.is_reply_msg, limit: 1, user_id: decode.id })).msgs;
                            if(mainmsg.length > 0){
                                var bytes = CryptoJS.AES.decrypt(mainmsg[0].msg_body, process.env.CRYPTO_SECRET);
                                var msg_body = bytes.toString(CryptoJS.enc.Utf8);
                                if (msg_body.indexOf('"') == 0)
                                    msg_body = (msg_body).substring(1, msg_body.length - 1);
                                /**
                                 * this block required for deleting a msg
                                 * when user delete a single file, but the msg contain only 1 file and have msg body = "No Comments!",
                                 * then delete full msg.
                                 */
                                if(mainmsg[0].all_attachment.length === 1 && msg_body === "No Comments!"){
                                    let dmsg_data = {
                                        user_id: decode.id,
                                        sender_img: mainmsg[0].sender_img,
                                        company_id: mainmsg[0].company_id,
                                        conversation_id : mainmsg[0].conversation_id,
                                        msg_id: mainmsg[0].msg_id,
                                        participants: req.body.participants,
                                        delete_type: "for_all",
                                        file_id: req.body.file_id,
                                        is_reply_msg: mainmsg[0].is_reply_msg
                                    };
    
                                    delete_msg(dmsg_data, async function (response) {
                                        if (response.status == true) {
                                            if (dmsg_data.delete_type == 'for_all') {
                                                var uobj = await get_user_obj(decode.id);
                                                save_notification({
                                                    company_id: decode.company_id,
                                                    receiver_id: dmsg_data.participants,
                                                    title: uobj.firstname + " " + uobj.lastname + " delete a message.",
                                                    type: "message",
                                                    body: JSON.stringify(dmsg_data),
                                                    user_id: decode.id,
                                                    user_name: uobj.firstname + " " + uobj.lastname,
                                                    user_img: uobj.img
                                                }, req);
                                                dmsg_data.participants.forEach(function (v, k) {
                                                    xmpp_send_server(v, 'delete_msg', dmsg_data, req);
                                                    send_msg_firebase(v, 'delete_msg', dmsg_data, req);
                                                });
                                            } else if (dmsg_data.delete_type == 'for_me') {
                                                xmpp_send_server(decode.id, 'delete_msg', dmsg_data, req);
                                            }
                                            return res.status(200).json({ status: true, messages: 'Message delete successfully.' });
                                        } else {
                                            return res.status(400).json({ error: 'Error', message: response.error });
                                        }
                                    });
                                }
                            }
                        }
                        if(mainmsg.length === 0){
                            let b = await File.deleteOne({ _id: file[0]._id });
                            if(file[0].tag_list.length>0){
                                await update_tag_count({tag_id: file[0].tag_list, company_id: decode.company_id, val: -1, conversation_id: file[0].conversation_id.toString(), msg_id: file[0].msg_id.toString(), participants: req.body.participants, user_id: decode.id, file_ids: [file[0].id], recursive: false});
                            }
    
                            var result = {
                                conversation_id: file[0].conversation_id.toString(),
                                msg_id: file[0].msg_id.toString(),
                                is_reply_msg: req.body.is_reply_msg,
                                reply_for_msgid: '',
                                file_id: file[0].id,
                                file_bucket: file[0].bucket,
                                file_key: file[0].key,
                                file_type: file[0].file_category == 'image' || file[0].file_category == 'audio' || file[0].file_category == 'video' || file[0].file_category == 'voice' ? file[0].file_category : 'other',
                                is_share: file[0].url_short_id !== null && file[0].url_short_id !== "" && file[0].user_id.toString() === decode.id ? true : false,
                                task_id: file[0].task_id,
                                cost_id: file[0].cost_id
                            }
                            req.body.participants.forEach(function(v, k) {
                                xmpp_send_server(v, 'delete_one_file', result, req);
                                send_msg_firebase(v, 'delete_one_file', result, req);
                            });
                            return res.status(200).json(result);
                        }
                        if(file[0].task_id || msg_body !== "No Comments!" || (file[0].task_id === undefined && mainmsg[0].all_attachment.length > 1)){
                            var filepath = file[0].bucket + '/' + file[0].key;
                            var update_data = {};
                            if(mainmsg.length>0){
                                if (Array.isArray(mainmsg[0].attch_audiofile) && mainmsg[0].attch_audiofile.indexOf(filepath) > -1) {
                                    update_data.attch_audiofile = mainmsg[0].attch_audiofile.filter((e) => { return e !== filepath });
                                    if (update_data.attch_audiofile.length == 0){
                                        update_data.msg_type = 'text';
                                        update_data.attch_audiofile = [];
                                    }else{
                                        update_data.attch_audiofile = update_data.attch_audiofile[0];
                                    }
                                        
                                } else if (Array.isArray(mainmsg[0].attch_imgfile) && mainmsg[0].attch_imgfile.indexOf(filepath) > -1) {
                                    // console.log(240, mainmsg[0].attch_imgfile.indexOf(filepath))
                                    update_data.attch_imgfile = mainmsg[0].attch_imgfile.filter((e) => { return e !== filepath })
                                    if (update_data.attch_imgfile.length == 0){
                                        update_data.msg_type = 'text';
                                        update_data.attch_imgfile = [];
                                    }
                                    else{
                                        update_data.attch_imgfile = update_data.attch_imgfile[0];
                                    }
                                } else if (Array.isArray(mainmsg[0].attch_otherfile) && mainmsg[0].attch_otherfile.indexOf(filepath) > -1) {
                                    update_data.attch_otherfile = mainmsg[0].attch_otherfile.filter((e) => { return e !== filepath })
                                    if (update_data.attch_otherfile.length == 0){
                                        update_data.msg_type = 'text';
                                        update_data.attch_otherfile = [];
                                    }else{
                                        update_data.attch_otherfile = update_data.attch_otherfile[0];
                                    }
                                } else if (Array.isArray(mainmsg[0].attch_videofile) && mainmsg[0].attch_videofile.indexOf(filepath) > -1) {
                                    update_data.attch_videofile = mainmsg[0].attch_videofile.filter((e) => { return e !== filepath })
                                    if (update_data.attch_videofile.length == 0){
                                        update_data.msg_type = 'text';
                                        update_data.attch_videofile = [];
                                    }else{
                                        update_data.attch_videofile = update_data.attch_videofile[0];
                                    }
                                }
                            }
                            // console.log(248, file[0], update_data);
                            try{
                                if(mainmsg.length>0){
                                    if(file[0].task_id){
                                        let a = await Messages.updateOne({ conversation_id: file[0].conversation_id.toString(), msg_id: file[0].msg_id.toString() }, update_data);
                                    }else{
                                        let a = await Messages.updateOne({ conversation_id: file[0].conversation_id.toString(), msg_id: file[0].msg_id.toString() }, update_data);
            
                                    }
    
                                    
                                }
                                let b = await File.updateOne({ _id: file[0]._id }, {is_delete: 1});
                                // console.log(603, b);
                            }catch(e){
                                console.log(605, e);
                            }
    
                            if(file[0].tag_list.length>0){
                                await update_tag_count({tag_id: file[0].tag_list, company_id: decode.company_id, val: -1, conversation_id: file[0].conversation_id.toString(), msg_id: file[0].msg_id.toString(), participants: req.body.participants, user_id: decode.id, file_ids: [file[0].id], recursive: true});
                            }
                            if(req.body.is_reply_msg === 'yes' && mainmsg.length>0)
                                has_reply_attach_update(mainmsg[0]);
                            var result = {
                                conversation_id: mainmsg.length>0 ? mainmsg[0].conversation_id : '',
                                msg_id: mainmsg.length>0 ? mainmsg[0].msg_id : '',
                                is_reply_msg: req.body.is_reply_msg,
                                reply_for_msgid: mainmsg.length>0 ? mainmsg[0].reply_for_msgid : '',
                                file_id: file[0].id,
                                file_bucket: file[0].bucket,
                                file_key: file[0].key,
                                file_type: file[0].file_category == 'image' || file[0].file_category == 'audio' || file[0].file_category == 'video' || file[0].file_category == 'voice' ? file[0].file_category : 'other',
                                is_share: file[0].url_short_id !== null && file[0].url_short_id !== "" && file[0].user_id.toString() === decode.id ? true : false,
                                task_id: file[0].task_id,
                                cost_id: file[0].cost_id
                            }
                            var mainmsg = (await get_message({ convid: file[0].conversation_id.toString(), msgid: file[0].msg_id.toString(), company_id: file[0].company_id, is_reply: req.body.is_reply_msg, limit: 1, user_id: decode.id })).msgs;
                            req.body.participants.forEach(function(v, k) {
                                xmpp_send_server(v, 'delete_one_file', result, req);
                                send_msg_firebase(v, 'delete_one_file', result, req);
                            });
                            await znMsgUpdate(mainmsg[0], req.body.participants, 'msg_update_edit');
                            return res.status(200).json(result);
                        }
                    } else {
                        return res.status(400).json({ error: 'Not found', message: "File not found" });
                    }
                } catch (e) {
                    return res.status(400).json({ error: 'Error000', message: e });
                }
    
                // models.instance.File.find({ id: models.timeuuidFromString(req.body.file_id) }, { raw: true, allow_filtering: true }, async function(e, file) {
                    // if (e) return res.status(400).json({ error: 'Error1', message: e });
                    // file[0].tag_list = Array.isArray(file[0].tag_list) && file[0].tag_list.length > 0 ? file[0].tag_list : [];
                    // console.log(221, file[0].tag_list, file[0].conversation_id.toString(), file[0].msg_id.toString())
                    
                // });
            } catch (e) {
                return res.status(400).json({ error: 'Error0', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error00', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for update link title for a message
     * conversation_id
     * msg_id
     * url_title
     * participants
     * is_reply_msg
     */
     fastify.post('/url_title_update', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            const { errors, isValid } = valdateEditArg(req.body);
            if (!isValid) {
                return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.user_id = decode.id.toString();
                req.body.company_id = decode.company_id;
                if(req.body.url_title !== undefined)
                    var response = await url_title_update(req.body);
                else if(req.body.msg_title !== undefined)
                    var response = await msg_title_update(req.body);
    
                // console.log(response);
                if (response.status == true) {
                    
                    if(response.msg.sender.toString() == decode.id){
                        var uobj = await get_user_obj(decode.id);
                        save_notification({
                            company_id: decode.company_id,
                            receiver_id: req.body.participants,
                            title: uobj.firstname + " " + uobj.lastname + " add/ update "+ req.body.url_title ? "link" : "message" +" title in a message.",
                            type: "message",
                            body: JSON.stringify(response.msg),
                            user_id: decode.id,
                            user_name: uobj.firstname + " " + uobj.lastname,
                            user_img: uobj.img
                        }, req);
                    }
    
                    _.each(req.body.participants, function(v, k) {
                        if (response.msg.is_reply_msg == 'yes') {
                            // io.to(v).emit('edit_reply_message', response.msg);
                            xmpp_send_server(v, 'edit_reply_message', response.msg, req);
                            send_msg_firebase(v, 'edit_reply_message', response.msg, req);
                        } else {
                            // io.to(v).emit('edit_message', response.msg);
                            xmpp_send_server(v, 'edit_message', response.msg, req);
                            send_msg_firebase(v, 'edit_message', response.msg, req);
                        }
                    });
                    
                    await znMsgUpdate(response.msg, response.msg.participants, 'msg_update_edit');
    
                    return res.status(200).json({ status: true, messages: 'Message link update successfully.', data: response.msg });
                } else {
                    return res.status(400).json({ error: 'Error 1', message: response.error });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error 3', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for update link title for a message
     * conversation_id
     * msg_id
     * url_title
     * participants
     * is_reply_msg
     */
    fastify.post('/link_title', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            let decode = jwt.verify(token, process.env.SECRET);
            if (!isUuid(req.body.url_id)) {
                return res.status(400).json({ error: 'Validation error', message: "Link id invalide" });
            }
            try {
                var old_link = await getAlink(req.body.url_id);
                var result = await update_one_link_title(old_link[0], decode, req.body.title);
                if(result.status){
                    var old_title_obj = {};
                    // link owner add a title into link
                    if(result.link.user_id == decode.id){
                        var conversation = await get_conversation({user_id: decode.id, company_id: decode.company_id, conversation_id: result.link.conversation_id});
                        var uobj = await get_user_obj(decode.id);
                        save_notification({
                            company_id: decode.company_id,
                            receiver_id: conversation[0].participants,
                            title: uobj.firstname + " " + uobj.lastname + " add/ update link title in a link.",
                            type: "message",
                            body: JSON.stringify(result.link),
                            user_id: decode.id,
                            user_name: uobj.firstname + " " + uobj.lastname,
                            user_img: uobj.img
                        }, req);
                        for(let i=0; i<result.link.title; i++){
                            old_title_obj[result.link.title[i].user_id] = result.link.title[i].title;
                        }
                        _.each(conversation[0].participants, function(v, k) {
                            result.link.title = old_title_obj[v] ? old_title_obj[v] : old_title_obj[decode.id];
                            xmpp_send_server(v, 'link_title', result.link, req);
                            send_msg_firebase(v, 'link_title', result.link, req);
                        });
                        result.link.title = old_title_obj[decode.id];
                    }
                    else{
                        result.link.title = old_title_obj[decode.id] ? old_title_obj[decode.id] : req.body.title;
                    }
                    return res.status(200).json({ status: true, messages: 'Link title update successfully.', data: result.link });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error 3', message: "Token missing." });
        }
    });
    
    /**
     * This route is used for 'modify private msg participants'
     * conversation_id
     * msg_id
     * add_secret_user = array
     * remove_secret_user = array
     */
    fastify.post('/edit_private_participants', async(req, res) => {
        const token = extractToken(req);
        if (token) {
            var decode = await token_verify_decode(token);
            const { errors, isValid } = valdateSecretUserArg(req.body);
            if (!isValid) {
                if(errors.msg_id = "Either add_secret_user or remove_secret_user is required.")
                    return res.status(200).json({ status: true, error: 'Validation error', message: errors });
                else
                    return res.status(400).json({ error: 'Validation error', message: errors });
            }
            try {
                req.body.user_id = decode.id.toString();
                req.body.company_id = decode.company_id;
                // console.log(1089, req.body);
                var response = await edit_private_participants(req.body);
                // console.log(1091, response);
                if (response.status == true) {
                    var allpar = [...response.msg.secret_user, ...req.body.add_secret_user, ...req.body.remove_secret_user, response.msg.sender.toString()];
                    var uniqp = [...new Set(allpar)];
                    var uobj = await get_user_obj(decode.id);
                    save_notification({
                        company_id: decode.company_id,
                        receiver_id: uniqp,
                        title: uobj.firstname + " " + uobj.lastname + " edit secret user.",
                        type: "message",
                        body: JSON.stringify(response.msg),
                        user_id: decode.id,
                        user_name: uobj.firstname + " " + uobj.lastname,
                        user_img: uobj.img
                    }, req);
    
                    // console.log(1106, response.msg);
                    _.each(uniqp, async function(v, k) {
                        xmpp_send_server(v, 'edit_private_participants', response.msg, req);
                        send_msg_firebase(v, 'edit_private_participants', response.msg, req);
                        //await znReadMsgUpdateCounter({user_id: v, conversation_id: response.msg.conversation_id, is_reply_msg: 'no', root_msg_id: response.msg.root_msg_id, read: response.msg.secret_user.length, reply_mention:0});
                    });
                    let read =0;
                    let result_un = {};
                    result_un = await get_rep_message({msg_id: response.msg.msg_id, user_id: decode.id});
                    read = result_un.reply_msgs.length;
                    await znPrivetMsgUpdate(response.msg,uniqp,req.body.remove_secret_user,read)
                    //await znMsgUpdate(response.msg, uniqp, 'msg_update_edit');
                    
                    return res.status(200).json({ status: true, messages: 'Secret Message user update successfully.', data: response.msg });
                } else {
                    return res.status(400).json({ error: 'Error 1', message: response.error });
                }
            } catch (e) {
                return res.status(400).json({ error: 'Error 2', message: e });
            }
        } else {
            return res.status(400).json({ error: 'Error 3', message: "Token missing." });
        }
    });
}

module.exports = routes;