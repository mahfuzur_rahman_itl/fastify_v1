const express = require('express');
const router = express.Router();
const isUuid = require('uuid-validate');
const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require('uuid');

var {validateCreateTeamInput} = require('../../validation/team');
var {extractToken, token_verify_decode} = require('../../v2_utils/jwt_helper');
var {get_team} = require('../../v2_utils/team');
var { get_user_obj } = require('../../v2_utils/user');
var { xmpp_send_server } = require('../../v2_utils/voip_util');
var { get_conversation } = require('../../v2_utils/message');
var { save_notification } = require('../../v2_utils/notification');
var {znConNodeUpdate} = require('../../v2_utils/zookeeper');
// var {models} = require('../../config/db/express-cassandra');
var Conversation = require('../../mongo-models/conversation');
const BusinessUnit = require('../../mongo-models/business_unit');
var Team = require('../../mongo-models/team');

/**
 * Get team list
 */
router.get('/get_team_lists/:id?', async (req, res) => {
    // console.log(req.params)
  const token = extractToken(req);
  let decode = await token_verify_decode(token);
  try{
      req.params = req.query.id ? req.query : req.params;
      all_teams[decode.company_id] = await get_team({...decode, team_id: req.params.id});
      var teams = [];
    //   console.log(24, decode.firstname)
      for(let v of all_teams[decode.company_id]){
        v.participants_details = [];
        v.admin_name = [];
        for (let i = 0; i < v.participants.length; i++) {
            let this_user = await get_user_obj(v.participants[i]);
            // console.log(v.participants[i], this_user.firstname, typeof this_user);
            if(typeof this_user == 'object' && this_user.firstname !== ''){
                v.participants_details.push({
                    id: v.participants[i],
                    firstname: this_user.firstname,
                    lastname: this_user.lastname,
                    img: this_user.img,
                    dept: this_user.dept,
                    designation: this_user.designation,
                    email: this_user.email,
                    phone: this_user.phone,
                    access: this_user.access,
                    company_id: this_user.company_id.toString(),
                    company_name: this_user.company_name,
                    conference_id: this_user.conference_id,
                    role: this_user.role,
                    email_otp: this_user.email_otp,
                    is_active: this_user.is_active,
                    fnln: this_user.fnln,
                    createdat: this_user.createdat
                });

                if(v.admin && v.admin.indexOf(v.participants[i]) > -1){
                    v.admin_name.push(this_user.firstname + ' ' + this_user.lastname);
                }
            }
        }
        if(req.params.id && v.team_id.toString() == req.params.id){
            teams.push(v);
        }
      }
    //   console.log(53, isUuid(req.params.id))
      
      return res.status(200).json({status: true, teams: req.params.id? teams : all_teams[decode.company_id] });
  }catch(e){
    console.log(e)
    return res.status(400).json(e);
  }
});

/**
 * Get team list by user id
 */
 router.get('/get_team_lists_by_uid', async (req, res) => {
    // console.log(68, req.query);
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    try{
        all_teams[decode.company_id] = await get_team(decode);
        var teams = [];
        for(let v of all_teams[decode.company_id]){
            // console.log(27, v.participants.length)
            if(v.participants.indexOf(req.query.id) > -1) {
                teams.push(v);
            }
        }
      
        return res.status(200).json({status: true, teams });
    }catch(e){
        return res.status(400).json(e);
    }
});

/**
 * Create or update team
 * title
 * participants
 * type = create/ update/ update_conv_also
 * remove_user = only need when type send update conversation also
 */
router.post('/create_new_team', (req, res) => {
    const { errors, isValid } = validateCreateTeamInput(req.body);
    if (!isValid) {
		    return res.status(400).json({ error: 'Validation error', message: errors });
    }
    try{
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        var team_id = uuidv4();
        // console.log(36, req.body);

        if(isUuid(req.body.team_id) && (req.body.type === 'update' || req.body.type === 'update_conv_also')){
            var update_data = {
                team_title: req.body.title.trim().toString(),
                participants: req.body.participants,
                updated_by: decode.id,
                updated_at: Date.now()
            };
            if(Array.isArray(req.body.admin) && req.body.admin.length>0){
                update_data.admin = req.body.admin;
            }
            try{
                Team.updateOne({team_id: req.body.team_id, admin: {$in: decode.id}}, update_data,async function(err,result){
                    if(err) console.log(err);
                    if(result.modifiedCount > 0){
                        if(req.body.type === 'update_conv_also' && req.body.remove_user){
                            var queries = [];
                            var conversation_ids = [];
                            
                            Conversation.updateMany({ team_id: req.body.team_id }, {$pull: {participants: req.body.remove_user}}, function(err){
                                if (err)
                                    console.log('Error updating conversations:', err);
                                else {
                                    for(let i=0; i<req.body.participants.length; i++)
                                        xmpp_send_server(req.body.participants[i], "team_member_remove", {team_id: req.body.team_id, conversation_ids, remove_user: req.body.remove_user}, req);
                                    xmpp_send_server(req.body.remove_user, "team_member_remove", {team_id: req.body.team_id, conversation_ids, remove_user: req.body.remove_user}, req);
                                  }
                            });
                        }
    
                        all_teams[decode.company_id] = await get_team(decode);
    
                        for(let i=0; i<req.body.participants.length; i++)
                            xmpp_send_server(req.body.participants[i], "update_team", all_teams[decode.company_id], req);
                        let thisteam = all_teams[decode.company_id].filter(e => e.team_id === req.body.team_id);
                        update_data.created_by_name = thisteam[0].created_by_name;
                        update_data.updated_by_name = thisteam[0].updated_by_name;
                        return res.status(200).json({status:true, data:all_teams[decode.company_id], update_data});
                    }else{
                        return res.status(200).json({status:false, data:all_teams[decode.company_id], message: "You are not an admin of this team."});
                    }
                });
            }catch(e){
                return res.status(400).json({ error: e });
            }
        }else{
            console.log('new teeam')
            var newteam = {
                team_id: team_id,
                team_title: req.body.title.trim().toString(),
                participants: req.body.participants,
                created_by: decode.id,
                updated_by: decode.id,
                admin: [decode.id],
                company_id: decode.company_id
            };

            new Team(newteam).save().then(async ()=>{
                Create_Conversations_by_title_team(req,req.body,team_id);
                all_teams[decode.company_id] = await get_team(decode);
                for(let i=0; i<req.body.participants.length; i++)
                    xmpp_send_server(req.body.participants[i], "create_new_team", all_teams[decode.company_id], req);
                return res.status(200).json({status:true, data:all_teams[decode.company_id]});
            }).catch((err)=>{
                //console.log(175,err)
                return res.status(400).json({ error: err.name, message: err.message });
            });
        }
    }catch(e){
        console.log(e)
        return res.status(400).json({ error: 'create_new_team Error 2', message: e });
    }
});

/*
coversation_create
*/
async function Create_Conversations_by_title_team(req,str,team_id){
    const token = extractToken(req);
    let decode = jwt.verify(token, process.env.SECRET);
    let list = await BusinessUnit.findOne({ company_id: decode.company_id });
    const conv = new Conversation({
        conversation_id: team_id,
        created_by: new Date().getTime(),
        sender_id: decode.id,
        participants: str.participants,
        participants_admin: decode.id,
        participants_guest: [],
        title: str.title.trim().toString(),
        conv_img: 'feelix.jpg',
        privacy: 'public',
        single: 'no',
        group: 'yes',
        system_conversation: 'team_'+team_id,
        company_id: decode.company_id,
        team_id: '',
        b_unit_id: list ? list.unit_id : '',
        tag_list: [],
        topic_type: null
    });
     console.log(217, req.body);
    conv.save().then(async () => {
        var newconv = await get_conversation({conversation_id: team_id, user_id: decode.id});
        save_notification({
            company_id: decode.company_id,
            receiver_id: req.body.participants,
            title: "New conversation '" + req.body.title + "' created",
            type: "conversation",
            body: JSON.stringify(newconv[0]),
            user_id: decode.id,
            user_name: decode.firstname + ' ' + decode.lastname,
            user_img: decode.img
        }, req);

       // req.body.name = decode.firstname + " " + decode.lastname;
       // req.body.company_name = decode.company_name;
       // check_send_invite_email(req);
        // io broadcast to all that a new room created
        for(let v of req.body.participants) {
           /*  if(newconv[0].group === 'no'){
                let fid = newconv[0].participants.join().replace(v, '').replace(',', '');
                console.log(166, v);
                let tp = all_users[req.body.company_id][fid];
                newconv[0].title = tp.firstname + ' ' + tp.lastname;
                newconv[0].fnln = tp.fnln;
                console.log(170, fid, newconv[0].title);
            } */
            xmpp_send_server(v, 'new_room', newconv[0], req);
            await znConNodeUpdate({uid: v, type: 'new_room', conv: newconv[0]});
            
        }
         console.log(248, conv);
    }).catch((err) => {
        console.log(377, "Conversation added not successfully.", err);
    });


    /* return new Promise(async (resolve,reject)=>{
		try{
            let team = await Team.findOne({ team_title: str });
            resolve({ status: true, team });
        }catch(error){
            reject({status: false, error: 'Find error in has_this_title_conversation', message: error });
        }
    }); */
}

/*
Delete team
*/
router.post('/delete_team', (req, res) => {
    if (!isUuid(req.body.team_id)) {
		    return res.status(400).json({ error: 'Validation error', message: "Team id invalid" });
    }
    try{
        const token = extractToken(req);
        let decode = jwt.verify(token, process.env.SECRET);
        // console.log(93, req.body);
        Conversation.find({team_id: req.body.team_id}, function(err, convs){
            if(err) return res.status(400).json({ error: 'Find error', message: err });
            if(convs.length == 0){
                Team.deleteOne({team_id: req.body.team_id, admin: {$in: [decode.id]}}, async function(error, result){
                    if(error) return res.status(400).json({ error: 'Find error', message: err });
                    else if(result.deletedCount > 0) {
                        all_teams[decode.company_id] = await get_team(decode);
                        return res.status(200).json({ status: true, message: 'Team delete successfully', team_id: req.body.team_id });
                    }
                    else{
                        return res.status(200).json({ status: false, message: 'You are not an admin of this team.' });
                    }
                })
            }
            else{
                console.log(convs);
                return res.status(400).json({ status: true, message: 'Team already used', team_id: req.body.team_id });
            }
        });
    }catch(e){
        return res.status(400).json({ error: 'delete_team Error 2', message: e });
    }
});



module.exports = router;
