const fs = require('fs');
var csv = require('csv-parser');
const isUuid = require('uuid-validate');
var express = require('express');
var router = express.Router();
const bcrypt = require('bcryptjs');
const short = require('short-uuid');
var cron = require('node-cron');
var moment = require('moment');
const _ = require('lodash');
// var { sg_email } = require('../utils/sg_email');
const nodemailer = require("nodemailer");

var passwordToHass = (pass) => {
    var salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(pass, salt);
};
// var { models } = require('../config/db/express-cassandra');
// var {
//     xmpp_send_server,
//     xmpp_send_broadcast,
// } = require('../utils/voip_util');
// var { count_asofday } = require('../utils/notification');
const logger = require('../logger');
const { mode } = require('crypto-js');
const { rejects } = require('assert');
const { resolve } = require('path');

var zkclient = require('../config/zookeeper');
var {setZK, getZK} = require('../v2_utils/zookeeper');

/* GET home page. */
router.get('/', async function (req, res, next) {
    // zkclient.remove('/workfeeli/cache/company', -1, function (error) {
    //     if (error) {
    //         console.log(error.stack);
    //         return;
    //     }
    
    //     console.log('/workfeeli/cache/company Node is deleted.');
    // });
    // let company = await getZK('/workfeeli/'+ process.env.ZKNODE.replace(/\//g, '') +'/cache/company');
    // console.log(`Company length ${company.length}`);
    res.render('index', { title: "Welcome" });
    // res.render('fileupload');
});

// function work_update_email() {
//     let st = new Date().toLocaleString();
//     let u = [];
//     models.instance.Users.stream({ company_id: models.timeuuidFromString("f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0") }, { raw: true, allow_filtering: true }, function (reader) {
//         var row;
//         while (row = reader.readRow()) {
//             if ((row.last_login !== null || row.last_login != '') && row.login_total > 9) {
//                 // console.log(moment(row.last_login).format("YYYY-MM-DD"), moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD'))
//                 // console.log(44, moment(moment(row.last_login).format("YYYY-MM-DD")).isAfter(moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD')))
//                 // if(moment(moment(row.last_login).format("YYYY-MM-DD")).isAfter(moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD'))){
//                 u.push(row);
//                 // }
//             }
//             // u.push(await count_unread_reply({ user_id: row.id.toString() }));
//         }
//     }, async function (err) {
//         if (err) console.log(84, err);
//         for (let i = 0; i < u.length; i++) {
//             let result = await count_asofday({ user_id: u[i].id.toString() });
//             console.log(58, result);
//             var emaildata = {
//                 to: u[i].email,
//                 // bcc: 'mahfuzak08@gmail.com',
//                 subject: 'Workfreeli Update',
//                 text: 'Workfreeli Update',
//                 html: 'Hi ' + u[i].firstname + ',<br><br>You have:<br><br>' + result.msgs + ' unread messages<br>' + result.reply_msgs + ' unread reply messages<br>' + result.miscall + ' missed call<br>' + result.mention_msgs + ' messages mention to you<br><br>To catch up on what you’ve missed in Workfreeli, please click sign-in button below. If for any reason, you are unable to log in, please use “Forget Password” option on Sign-in Page to receive one time password (OTP) to login and reset your password.<br><br><a clicktracking=off href="' + process.env.CLIENT_BASE_URL + '" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
//             }

//             // console.log(emaildata);
//             try {
//                 // sg_email.send(emaildata, (result) => {
//                 //     if (result.msg == 'success') {
//                 //         // res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
//                 //     } else {
//                 //         console.log('forgot password ', result);
//                 //         // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
//                 //     }
//                 // });
//             } catch (e) {
//                 console.log(e);
//                 // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
//             }
//         }
//         console.log("All email send done at " + new Date().toLocaleString());
//         console.log({ status: true, msg: u.length + " users loaded.", start_time: st, end_time: new Date().toLocaleString() });
//     });
// }
// cron.schedule("*/10 * * * * *", function() {
//     console.log("running a task every 10 second");
// });

// cron.schedule("*/2 * * * *", function() {
//     console.log("running a task every 2 min" + new Date().toLocaleString());
// });
// cron.schedule("30 15 14 * * *", function() {
//     console.log("running a task at 14:15:30" + new Date().toLocaleString());
// });

// cron.schedule("*/15 * * * *", function() {
//     work_update_email();
//     console.log("running a task every 15 min " + new Date().toLocaleString());
// });
// if(process.env.TEST_CHECK === 'yes'){
//     cron.schedule("0 0 2 * * *", function() {
//         work_update_email();
//         console.log("run every midnight at " + new Date().toLocaleString());
//         logger.info("run every midnight at " + new Date().toLocaleString());
//     });
// }
// router.get('/version_info', function(req, res, next) {
//     models.instance.Version.find({ $limit: 1 }, { raw: true }, function(err, vs) {
//         res.json({ status: true, version: vs[0].version, build_version: vs[0].build, last_update: vs[0].created_at });
//     });
// });

// router.get('/one-day-report', function (req, res, next) {
//     let st = new Date().toLocaleString();
//     let u = [];
//     models.instance.Users.stream({ company_id: models.timeuuidFromString("f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0") }, { raw: true, allow_filtering: true }, function (reader) {
//         var row;
//         while (row = reader.readRow()) {
//             if (row.last_login !== null || row.last_login != '') {
//                 // console.log(moment(row.last_login).format("YYYY-MM-DD"), moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD'))
//                 // console.log(44, moment(moment(row.last_login).format("YYYY-MM-DD")).isAfter(moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD')))
//                 // if(moment(moment(row.last_login).format("YYYY-MM-DD")).isAfter(moment(new Date()).subtract(1, 'days').format('YYYY-MM-DD'))){
//                 u.push(row);
//                 // }
//             }
//             // u.push(await count_unread_reply({ user_id: row.id.toString() }));
//         }
//     }, async function (err) {
//         if (err) console.log(84, err);
//         for (let i = 0; i < u.length; i++) {
//             let result = await count_asofday({ user_id: u[i].id.toString() });
//             console.log(58, result);
//             var emaildata = {
//                 to: u[i].email,
//                 // bcc: 'mahfuzak08@gmail.com',
//                 subject: 'Workfreeli Update',
//                 text: 'Workfreeli Update',
//                 html: 'Hi ' + u[i].firstname + ',<br><br>You have:<br><br>' + result.msgs + ' unread messages<br>' + result.reply_msgs + ' unread reply messages<br>' + result.miscall + ' missed call<br>' + result.mention_msgs + ' messages mention to you<br><br>To catch up on what you’ve missed in Workfreeli, please click sign-in button below. If for any reason, you are unable to log in, please use “Forget Password” option on Sign-in Page to receive one time password (OTP) to login and reset your password.<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="color: #FFF;background: #002e98;padding: 5px 10px;text-decoration: none;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
//             }

//             // console.log(emaildata);
//             try {
//                 sg_email.send(emaildata, (result) => {
//                     if (result.msg == 'success') {
//                         // res.status(200).json({ status: true, message: 'Workfreeli password reset code send successfully' });
//                     } else {
//                         console.log('forgot password ', result);
//                         // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: result });
//                     }
//                 });
//             } catch (e) {
//                 console.log(e);
//                 // res.status(200).json({ status: false, message: 'Workfreeli password reset but email not send', error: e });
//             }
//         }
//         console.log("All email send done at " + new Date().toLocaleString());
//         res.json({ status: true, msg: u.length + " users loaded.", start_time: st, end_time: new Date().toLocaleString() });
//     });
// });

// router.get('/send_email', async function(req, res, next){
//     // let transporter = nodemailer.createTransport({
//     //     host: 'smtp.gmail.com',
//     //     port: 587,
//     //     secure: false,
//     //     requireTLS: true,
//     //     auth: {
//     //         user: 'locationa73@gmail.com',
//     //         pass: 'Ma592321'
//     //     }
//     // });
//     // let transporter = nodemailer.createTransport({
//     //     host: "smtp.aol.com",
//     //     port: 587,
//     //     secure: false, 
//     //     requireTLS: true,
//     //     auth: {
//     //       user: 'mahfuzitl@aol.com',
//     //       pass: 'ma592321',
//     //     },
//     // });
//     let transporter = nodemailer.createTransport({
//         host: "mail.eibull.com",
//         port: 465,
//         secure: true,
//         auth: {
//           user: 'noreply@eibull.com',
//           pass: 'b9aca165bb33259dcb6048942b96e56b',
//         },
//     });
//     try{
//         let info = await transporter.sendMail({
//             from: '"Fred Foo" <noreply@eibull.com>', // sender address
//             to: "fajleh_rabbi@imaginebd.com", // list of receivers
//             cc: "mahfuzak08@gmail.com", // list of receivers
//             subject: "Hello ✔", // Subject line
//             text: "Hello world?", // plain text body
//             html: "<b>Hello world?</b>", // html body
//         });

//         console.log("Message sent: %s", info.messageId);
//         res.json({ status: true, info});
//     }catch(e){
//         console.log(e);
//         res.json({ status: false, e});
//     }
// });

// router.get('/company_is_activate', function(req, res, next) {
//     models.instance.Company.find({}, { raw: true }, function(err, com) {
//         var queries = [];
//         for (let i = 0; i < com.length; i++) {
//             if (com[i].company_name.indexOf("@") > -1) continue;
//             if (com[i].is_deactivate === null) {
//                 queries.push(models.instance.Company.update({ company_id: com[i].company_id }, { is_deactivate: 0 }, { return_query: true }));
//             }
//         }
//         if (queries.length > 0) {
//             models.doBatch(queries, function(err) {
//                 if (err) return res.status(400).send({ error: "Update error", message: err });

//                 console.log({ status: true, message: "Company is_deactivate set to 0 done" });
//             });
//         }
//         return res.status(200).send({ status: true, message: "Company is_deactivate done" });
//     });
// });
// router.get('/company', function(req, res, next) {
// 	var data = [];
// 	var str = "";
// 	fs.createReadStream('/Playground/file/input/v24_company.csv')
// 		.pipe(csv()).on('data', function (row) {
// 			data.push(row);
// 		}).on('end', function () {
// 			var filetime = new Date().getTime();
// 			console.log(318, data.length);
// 			data.forEach(function(v, k){
// 				v.campus = v.campus != "" ? v.campus.replace(/\"/g, "'") : null;
// 				v.class = v.class != "" ? v.class.replace(/\"/g, "'") : null;
// 				v.section = v.section != "" ? v.section.replace(/\"/g, "'") : null;

// 				str += "INSERT INTO company (company_id,company_name,campus,class,company_img,created_at,created_by,section,updated_at,updated_by) VALUES ("+ v.company_id +",'"+ v.company_name +"', "+ v.campus +", "+ v.class +",'"+ v.company_img +"', '"+ v.created_at +"', '"+ v.created_by +"', "+ v.section +", '"+ v.updated_at +"', '"+ v.updated_by +"');\r\n";

// 				if(k+1 == data.length){
// 					fs.appendFile('./output/company_'+filetime+'.sql', str, function (err) {
// 						if (err) console.log(err);
// 						console.log("All company data manipulation done save into file done.");
// 					});
// 				}
// 			});
// 		});
// 	res.json({ title: 'Company table.' });
// });

// router.get('/role', function(req, res, next) {
// 	var data = [];
// 	var str = "";
// 	fs.createReadStream('/Playground/file/input/v24_role.csv')
// 		.pipe(csv()).on('data', function (row) {
// 			data.push(row);
// 		}).on('end', function () {
// 			var filetime = new Date().getTime();
// 			console.log(318, data.length);
// 			data.forEach(function(v, k){
// 				v.role_access = v.role_access != "" ? v.role_access.replace(/\"/g, "") : null;
// 				if(v.user_id == "") return;

// 				str += "INSERT INTO role (role_id,company_id,created_at,created_by,role_access,role_title) VALUES ("+ v.role_id +",'"+ v.company_id +"', '"+ v.created_at +"', '"+ v.created_by +"', "+ v.role_access +", '"+ v.role_title +"');\r\n";

// 				if(k+1 == data.length){
// 					fs.appendFile('./output/role_'+filetime+'.sql', str, function (err) {
// 						if (err) console.log(err);
// 						console.log("All role data manipulation done save into file done.");
// 					});
// 				}
// 			});
// 		});
// 	res.json({ title: 'File table.' });
// });

// router.get('/businessunit', function(req, res, next) {
// 	var data = [];
// 	var str = "";
// 	fs.createReadStream('/Playground/file/input/v24_businessunit.csv')
// 		.pipe(csv()).on('data', function (row) {
// 			data.push(row);
// 		}).on('end', function () {
// 			var filetime = new Date().getTime();
// 			console.log(318, data.length);
// 			data.forEach(function(v, k){
// 				str += "INSERT INTO businessunit (company_id,unit_id,user_id,unit_name,industry_name,industry_id,created_by,updated_at,created_at) VALUES ('79df4d50-ce4a-11e9-90f6-e9974a7dde09', "+ v.unit_id +", '"+ v.user_id +"', '"+ v.unit_name +"', '"+ v.industry_name +"', '"+ v.industry_id +"', '"+ v.created_by +"', '"+ v.updated_at +"', '"+ v.created_at +"');\r\n";

// 				if(k+1 == data.length){
// 					fs.appendFile('./output/businessunit_'+filetime+'.sql', str, function (err) {
// 						if (err) console.log(err);
// 						console.log("All businessunit data manipulation done save into file done.");
// 					});
// 				}
// 			});
// 		});
// 	res.json({ title: 'Businessunit table.' });
// });

// router.get('/industry', function(req, res, next) {
// 	var data = [];
// 	var str = "";
// 	fs.createReadStream('/Playground/file/input/v24_industry.csv')
// 		.pipe(csv()).on('data', function (row) {
// 			data.push(row);
// 		}).on('end', function () {
// 			var filetime = new Date().getTime();
// 			console.log(318, data.length);
// 			data.forEach(function(v, k){
// 				str += "INSERT INTO industry (company_id,industry_id,industry_name,created_at) VALUES ('79df4d50-ce4a-11e9-90f6-e9974a7dde09', "+ v.industry_id +", '"+ v.industry_name +"', '"+ v.created_at +"');\r\n";

// 				if(k+1 == data.length){
// 					fs.appendFile('./output/industry_'+filetime+'.sql', str, function (err) {
// 						if (err) console.log(err);
// 						console.log("All role data manipulation done save into file done.");
// 					});
// 				}
// 			});
// 		});
// 	res.json({ title: 'Industry table.' });
// });

// router.get('/team', function(req, res, next) {
// 	var data = [];
// 	var str = "";
// 	fs.createReadStream('/Playground/file/input/v24_team.csv')
// 		.pipe(csv()).on('data', function (row) {
// 			data.push(row);
// 		}).on('end', function () {
// 			var filetime = new Date().getTime();
// 			console.log(318, data.length);
// 			data.forEach(function(v, k){
// 				v.participants = v.participants != "" ? v.participants.replace(/\"/g, "'") : null;
// 				str += "INSERT INTO team (team_id,created_at,updated_at,created_by,updated_by,company_id,team_title,participants) VALUES ("+ v.team_id +", '"+ v.created_at +"', '"+ v.updated_at +"', "+ v.created_by +", "+ v.updated_by +", "+ v.company_id +", '"+ v.team_title +"', "+ v.participants +");\r\n";

// 				if(k+1 == data.length){
// 					fs.appendFile('./output/team_'+filetime+'.sql', str, function (err) {
// 						if (err) console.log(err);
// 						console.log("All team data manipulation done save into file done.");
// 					});
// 				}
// 			});
// 		});
// 	res.json({ title: 'Team table.' });
// });

// router.get('/conversation', function(req, res, next) {
// var conv_data = [];
// var pin_data = [];
// var str = "";
// fs.createReadStream('/Office Work/freeli_server_v1/server/temp/v24_pin.csv')
// .pipe(csv()).on('data', function (row) {
// if(pin_data.find(n => n.convid === row.block_id) === undefined)
// pin_data.push({convid: row.block_id, uids: ["'"+row.user_id+"'"] });
// else{
// pin_data.find(function(n){
// if(n.convid === row.block_id){
// n.uids.push("'"+row.user_id+"'");
// }
// });
// }
// }).on('end', function () {
// console.log(153, pin_data.length);
// fs.createReadStream('/Office Work/freeli_server_v1/server/temp/live22_conversation.csv')
// .pipe(csv()).on('data', function (row) {
// conv_data.push(row);
// }).on('end', function () {
// console.log(conv_data.length);
// var filename = "conv_"+new Date().getTime();
// conv_data.forEach(function(v, k){
// if(k>0 && k % 1000 == 0){
// fs.appendFile('/Office Work/freeli_server_v1/server/temp/'+filename+'.sql', str, function (err) {
// if (err) console.log(err);
// console.log("1st 1000 done.");
// });
// str = "";
// }
// v.close_for = v.close_for != "" ? v.close_for.replace(/\"/g, "'") : null;
// v.is_active = v.is_active != "" ? v.is_active.replace(/\"/g, "'") : null;
// v.is_pinned_users = v.is_pinned_users != "" ? v.is_pinned_users.replace(/\"/g, "'") : null;
// v.participants = v.participants != "" ? v.participants.replace(/\"/g, "'") : null;
// v.participants_admin = v.participants_admin != "" ? v.participants_admin.replace(/\"/g, "'") : null;
// v.participants_guest = v.participants_guest != "" ? v.participants_guest.replace(/\"/g, "'") : null;
// v.participants_sub = v.participants_sub != "" ? v.participants_sub.replace(/\"/g, "'") : null;
// v.service_provider = v.service_provider != "" ? v.service_provider.replace(/\"/g, "'") : null;
// v.tag_list = v.tag_list != "" ? v.tag_list.replace(/\"/g, "'") : null;
// v.last_msg = v.last_msg.indexOf("'") > -1 ? v.last_msg.replace(/'/g, "&apos;") : v.last_msg;
// v.sender_id = v.sender_id != "" ? v.sender_id : null;
// if(v.created_by == "") return;
// v.pin = null;
// pin_data.find(function(n){
// if(n.convid === v.conversation_id){
// // console.log(284, n.uids.toString());
// v.pin = "{" + n.uids + "}";
// }
// });


// str += "INSERT INTO conversation (conversation_id,company_id,archive,b_unit_id,close_for,conference_id,conv_img,created_at,created_by,group,team_id,guests,is_active,is_busy,is_pinned_users,last_msg,last_msg_time,msg_status,participants,participants_admin,participants_guest,participants_sub,privacy,reset_id,room_id,root_conv_id,sender_id,service_provider,single,status,tag_list,title,topic_type,pin,has_mute,mute) VALUES ("+ v.conversation_id +", "+ v.company_id +", '"+ v.archive +"', '"+ v.b_unit_id +"', "+ v.close_for +", '"+ v.conference_id +"', '"+ v.conv_img +"', '"+ v.created_at +"', "+ v.created_by +", '"+ v.group +"', '"+ v.team_id +"', '"+ v.guests +"', "+ v.is_active +", "+ v.is_busy +", "+ v.is_pinned_users +", '"+ v.last_msg +"', '"+ v.last_msg_time +"', '"+ v.msg_status +"', "+ v.participants +", "+ v.participants_admin +", "+ v.participants_guest +", "+ v.participants_sub +", '"+ v.privacy +"', '"+ v.reset_id +"', '"+ v.room_id +"', '"+ v.root_conv_id +"', "+ v.sender_id +", "+ v.service_provider +", '"+ v.single +"', '"+ v.status +"', "+ v.tag_list +", '"+ v.title +"', '"+ v.topic_type +"', "+ v.pin +", null, null);\r\n";
// if(k+1 == conv_data.length){
// fs.appendFile('/Office Work/freeli_server_v1/server/temp/'+filename+'.sql', str, function (err) {
// if (err) console.log(err);
// console.log("All data manipulation done save into file done.");
// });
// }
// });
// });
// });


// res.json({ title: 'Conversation table.' });
// });

// checklist_status = 0 (only open)
//                  = 1 (completed)
//                  = 2 (self assign)
//                  = 3 (assign to)
//                  = 4 (alternative assign to)
//                  = 5 (accept)
//                  = 7 (decline)
//                  = 8 (request)
//                  = 9 (review)
//                  = 10 (reopen)

// accept korle assign_status = 2
// decline korle assign_status = 1
// decline cancel korle assign_status = assign user id

// function checklistCounterLenthString(data) {
//         if (v.checklist_status == null) {
//             v.checklist_status = 0
//         }
//         if (v.request_ttl_approved_date != null) {
//             v.end_due_date = (moment(v.request_ttl_approved_date, "YYYY-MM-DD HH:mm").unix() * 1000);
//         }
//         var assusr = v.alternative_assign_to == null ? v.assign_to : v.alternative_assign_to;
//         if (v.checklist_status == 0) {
//             if (assusr != user_id) {
//                 if (v.assign_status == 1 && v.assignedby == user_id) {
// 					// review required
//                     response_req++;
//                     need_due_check = 'no';
//                 } else if (v.Request_ttl_by != null && v.assignedby == user_id) {
//                     response_req++;
//                     need_due_check = 'no';
//                 }
//                 if (v.assign_status == user_id && v.assignedby == user_id && v.Request_ttl_by == null && v.end_due_date != null) {
//                 }
//             } else {
//                 if (v.assign_status != null && v.assign_status != 2 && v.assign_status != user_id && assusr == user_id) {
//                     if ((v.assignee_change_reason != null || v.assign_decline_note != null) && v.assign_status != 1 && v.Request_ttl_by == null) {
//                         response_req++;
//                         need_due_check = 'no';
//                     } else if (v.assign_status == 1 && assusr == user_id) {

//                     } else if (v.Request_ttl_by == user_id) {

//                     } else {
//                         response_req++;
//                         need_due_check = 'no';
//                     }
//                 }
//             }

//             if (need_due_check == 'yes' && due_date_is_today && assusr == user_id && v.assignedby != user_id) {
//                 if (v.assign_status == 2 && v.Request_ttl_by == user_id) {} else {
//                     if (v.Request_ttl_by == null) {
//                         response_req++;
//                     }
//                 }
//             }
//         }

//         t++;
//         if (v.checklist_status == 1) {
//             c++;
//         } else {
//             i++;
//         }
//     });
//     return (t == c) ? -1 : response_req;
// }



// function checklistCounterLenthString(data) {
//     if (data == undefined || data.length == 0) return "";
//     if ($("#conv" + data[0].convid).length != 1) return "";
//     var c = 0;
//     var i = 0;
//     var t = 0;
//     var response_req = 0;
//     var str = '';
//     $.each(data, function(k, v) {
//         var due_date_is_today = false;
//         var need_due_check = 'yes';
//         if (checklist_item_due_date_check(v) === true) {
//             due_date_is_today = true;
//         }

//         if (v.checklist_status == null) {
//             v.checklist_status = 0
//         }
//         if (v.request_ttl_approved_date != null) {
//             v.end_due_date = (moment(v.request_ttl_approved_date, "YYYY-MM-DD HH:mm").unix() * 1000);
//         }
//         var assusr = v.alternative_assign_to == null ? v.assign_to : v.alternative_assign_to;
//         if (v.checklist_status == 0) {
//             if (assusr != user_id) {
//                 if (v.assign_status == 1 && v.assignedby == user_id) {
//                     response_req++;
//                     need_due_check = 'no';
//                 } else if (v.Request_ttl_by != null && v.assignedby == user_id) {
//                     response_req++;
//                     need_due_check = 'no';
//                 }
//                 if (v.assign_status == user_id && v.assignedby == user_id && v.Request_ttl_by == null && v.end_due_date != null) {
//                 }
//             } else {
//                 if (v.assign_status != null && v.assign_status != 2 && v.assign_status != user_id && assusr == user_id) {
//                     if ((v.assignee_change_reason != null || v.assign_decline_note != null) && v.assign_status != 1 && v.Request_ttl_by == null) {
//                         response_req++;
//                         need_due_check = 'no';
//                     } else if (v.assign_status == 1 && assusr == user_id) {

//                     } else if (v.Request_ttl_by == user_id) {

//                     } else {
//                         response_req++;
//                         need_due_check = 'no';
//                     }
//                 }
//             }

//             if (need_due_check == 'yes' && due_date_is_today && assusr == user_id && v.assignedby != user_id) {
//                 if (v.assign_status == 2 && v.Request_ttl_by == user_id) {} else {
//                     if (v.Request_ttl_by == null) {
//                         response_req++;
//                     }
//                 }
//             }
//         }

//         t++;
//         if (v.checklist_status == 1) {
//             c++;
//         } else {
//             i++;
//         }
//     });
//     return (t == c) ? -1 : response_req;
// }


// router.get('/msgs', function(req, res, next) {
// var rep_data = [];
// var rep_id = [];
// var linkrow_data = {};
// var linkmsg_id = [];
// var success = 0;
// var error = [];
// var key = 0;
// var start = 0; 
// var end = 500;
// console.log(663, "msgs hit");
// fs.createReadStream('/Office Work/freeli_server_v1/server/temp/live22_reply.csv')
// .pipe(csv()).on('data', function (row) {
// rep_data.push(row);
// rep_id.push(row.rep_id.toString());
// }).on('end', function () {

// fs.createReadStream('/Office Work/freeli_server_v1/server/temp/live22_link.csv')
// .pipe(csv()).on('data', function (linkrow) {
// if(isUuid(linkrow.conversation_id) && isUuid(linkrow.msg_id)){
// linkmsg_id.push(linkrow.msg_id.toString());
// linkrow_data[linkrow.msg_id.toString()] = linkrow;
// }
// }).on('end', function () {

// console.log(16, rep_data.length);			
// var query = [];
// var count = 1;
// fs.createReadStream('/Office Work/freeli_server_v1/server/temp/messages625.csv')
// .pipe(csv()).on('data', function (v) {
// if(key>= start && start <= end){
// start++;
// // old reply table data manipulation for new structure
// var ridkey = rep_id.indexOf(v.conversation_id.toString());
// if(ridkey > -1){
// v.conversation_id = rep_data[ridkey].conversation_id;
// v.reply_for_msgid = rep_data[ridkey].msg_id;
// v.is_reply_msg = 'yes';
// }else{
// v.is_reply_msg = 'no';
// v.reply_for_msgid = null;
// }

// if(linkmsg_id.indexOf(v.msg_id.toString()) > -1){
// linkrow_data[v.msg_id.toString()].title = linkrow_data[v.msg_id.toString()].title ? linkrow_data[v.msg_id.toString()].title : '';
// linkrow_data[v.msg_id.toString()].title = linkrow_data[v.msg_id.toString()].title.indexOf("'") > -1 ? linkrow_data[v.msg_id.toString()].title.replace(/'/g, "&apos;") : linkrow_data[v.msg_id.toString()].title;

// v.msg_type = 'text link';
// v.url_base_title = linkrow_data[v.msg_id.toString()].title;
// v.url_body = linkrow_data[v.msg_id.toString()].url;
// }

// if(isUuid(v.conversation_id) && v.created_at != null && v.created_at != ""){
// try{
// // if(v.msg_status != ""){
// // 	v.msg_status = v.msg_status.substring(1, v.msg_status.length-1);
// // 	v.msg_status = v.msg_status.replace(/\"/g, "");
// // 	v.msg_status = v.msg_status.split(",");
// // 	// console.log(74, v.msg_status);
// // }
// if(v.assign_to != ""){
// v.assign_to = v.assign_to.substring(1, v.assign_to.length-1);
// v.assign_to = v.assign_to.replace(/\"/g, "");
// v.assign_to = v.assign_to.split(",");
// }
// if(v.attch_audiofile != ""){
// v.attch_audiofile = v.attch_audiofile.substring(1, v.attch_audiofile.length-1);
// v.attch_audiofile = v.attch_audiofile.replace(/\"/g, "");
// v.attch_audiofile = v.attch_audiofile.split(",");
// }
// if(v.attch_imgfile != ""){
// v.attch_imgfile = v.attch_imgfile.substring(1, v.attch_imgfile.length-1);
// v.attch_imgfile = v.attch_imgfile.replace(/\"/g, "");
// v.attch_imgfile = v.attch_imgfile.split(",");
// }
// if(v.attch_otherfile != ""){
// v.attch_otherfile = v.attch_otherfile.substring(1, v.attch_otherfile.length-1);
// v.attch_otherfile = v.attch_otherfile.replace(/\"/g, "");
// v.attch_otherfile = v.attch_otherfile.split(",");
// }
// if(v.attch_videofile != ""){
// v.attch_videofile = v.attch_videofile.substring(1, v.attch_videofile.length-1);
// v.attch_videofile = v.attch_videofile.replace(/\"/g, "");
// v.attch_videofile = v.attch_videofile.split(",");
// }
// if(v.call_participants != ""){
// v.call_participants = v.call_participants.substring(1, v.call_participants.length-1);
// v.call_participants = v.call_participants.replace(/\"/g, "");
// v.call_participants = v.call_participants.split(",");
// }
// if(v.edit_seen != ""){
// v.edit_seen = v.edit_seen.substring(1, v.edit_seen.length-1);
// v.edit_seen = v.edit_seen.replace(/\"/g, "");
// v.edit_seen = v.edit_seen.split(",");
// }
// if(v.has_delete != ""){
// v.has_delete = v.has_delete.substring(1, v.has_delete.length-1);
// v.has_delete = v.has_delete.replace(/\"/g, "");
// v.has_delete = v.has_delete.split(",");
// }
// if(v.has_flagged != ""){
// v.has_flagged = v.has_flagged.substring(1, v.has_flagged.length-1);
// v.has_flagged = v.has_flagged.replace(/\"/g, "");
// v.has_flagged = v.has_flagged.split(",");
// }
// if(v.has_hide != ""){
// v.has_hide = v.has_hide.substring(1, v.has_hide.length-1);
// v.has_hide = v.has_hide.replace(/\"/g, "");
// v.has_hide = v.has_hide.split(",");
// }
// if(v.has_tag_text != ""){
// v.has_tag_text = v.has_tag_text.substring(1, v.has_tag_text.length-1);
// v.has_tag_text = v.has_tag_text.replace(/\"/g, "");
// v.has_tag_text = v.has_tag_text.split(",");
// }
// if(v.issue_accept_user != ""){
// v.issue_accept_user = v.issue_accept_user.substring(1, v.issue_accept_user.length-1);
// v.issue_accept_user = v.issue_accept_user.replace(/\"/g, "");
// v.issue_accept_user = v.issue_accept_user.split(",");
// }
// if(v.service_provider != ""){
// v.service_provider = v.service_provider.substring(1, v.service_provider.length-1);
// v.service_provider = v.service_provider.replace(/\"/g, "");
// v.service_provider = v.service_provider.split(",");
// }
// if(v.tag_list != ""){
// v.tag_list = v.tag_list.substring(1, v.tag_list.length-1);
// v.tag_list = v.tag_list.replace(/\"/g, "");
// v.tag_list = v.tag_list.split(",");
// }
// if(v.secret_user != ""){
// v.secret_user = v.secret_user.substring(1, v.secret_user.length-1);
// v.secret_user = v.secret_user.replace(/\"/g, "");
// v.secret_user = v.secret_user.split(",");
// }
// if(v.mention_user != ""){
// v.mention_user = v.mention_user.substring(1, v.mention_user.length-1);
// v.mention_user = v.mention_user.replace(/\"/g, "");
// v.mention_user = v.mention_user.split(",");
// }

// v.msg_body = v.msg_body.replace(/'/g, "");
// v.msg_body = v.msg_body.replace(/â€™/g, "&apos;");
// v.msg_body = v.msg_body.replace(/’/g, "&apos;");
// // v.msg_body = v.msg_body.replace(/<\/?[^>]+(>|$)/g, ""); // replace all tag
// v.msg_body = '"'+ v.msg_body +'"';

// v.msg_text = v.msg_text.replace(/'/g, "");
// v.msg_text = v.msg_text.replace(/â€™/g, "&apos;");
// v.msg_text = v.msg_text.replace(/’/g, "&apos;");
// v.msg_text = v.msg_text.replace(/\\'/g, "'");

// v.edit_history = v.edit_history.replace(/'/g, "");
// v.edit_history = v.edit_history.replace(/â€™/g, "&apos;");
// v.edit_history = v.edit_history.replace(/’/g, "&apos;");

// // v.attch_audiofile = v.attch_audiofile.replace(/'/g, "");
// // v.attch_imgfile = v.attch_imgfile.replace(/'/g, "");
// // v.attch_otherfile = v.attch_otherfile.replace(/'/g, "");
// // v.attch_videofile = v.attch_videofile.replace(/'/g, "");

// // v.msg_status = v.msg_status.replace(/\"/g, "'");

// var msg = new models.instance.Messages({
// conversation_id: models.uuidFromString(v.conversation_id),
// msg_id: models.timeuuidFromString(v.msg_id),
// msg_body: v.msg_body == "" ? models.datatypes.unset : v.msg_body,
// msg_text: v.msg_text == "" ? models.datatypes.unset : v.msg_text,
// edit_history: v.edit_history == "" ? models.datatypes.unset : v.edit_history,
// activity_id: v.activity_id == "" ? models.datatypes.unset : v.activity_id,
// call_duration: v.call_duration == "" ? models.datatypes.unset : v.call_duration,
// call_msg: v.call_msg == "" ? models.datatypes.unset : v.call_msg,
// call_receiver_device: v.call_receiver_device == "" ? models.datatypes.unset : v.call_receiver_device,
// call_receiver_ip: v.call_receiver_ip == "" ? models.datatypes.unset : v.call_receiver_ip,
// call_sender_device: v.call_sender_device == "" ? models.datatypes.unset : v.call_sender_device,
// call_sender_ip: v.call_sender_ip == "" ? models.datatypes.unset : v.call_sender_ip,
// call_server_addr: v.call_server_addr == "" ? models.datatypes.unset : v.call_server_addr,
// call_status: v.call_status == "" ? models.datatypes.unset : v.call_status,
// call_type: v.call_type == "" ? models.datatypes.unset : v.call_type,
// conference_id: v.conference_id == "" ? models.datatypes.unset : v.conference_id,
// created_at: v.created_at == "" ? models.datatypes.unset : v.created_at,
// edit_status: v.edit_status == "" ? models.datatypes.unset : v.edit_status,
// forward_at: v.forward_at == "" ? models.datatypes.unset : v.forward_at,
// has_delivered: v.has_delivered == "" ? models.datatypes.unset : parseInt(v.has_delivered),
// has_reply: v.has_reply == "" ? models.datatypes.unset : parseInt(v.has_reply),
// has_timer: v.has_timer == "" ? models.datatypes.unset : v.has_timer,
// is_reply_msg: v.is_reply_msg == "" ? models.datatypes.unset : v.is_reply_msg,
// last_reply_name: v.last_reply_name == "" ? models.datatypes.unset : v.last_reply_name,
// msg_type: v.msg_type == "" ? models.datatypes.unset : v.msg_type,
// reply_for_msgid: v.reply_for_msgid == "" ? models.datatypes.unset : v.reply_for_msgid,
// root_conv_id: v.root_conv_id == "" ? models.datatypes.unset : v.root_conv_id,
// sender: v.sender == "" ? models.datatypes.unset : models.uuidFromString(v.sender),
// sender_img: v.sender_img == "" ? models.datatypes.unset : v.sender_img,
// sender_name: v.sender_name == "" ? models.datatypes.unset : v.sender_name,
// url_base_title: v.url_base_title == "" ? models.datatypes.unset : v.url_base_title,
// url_body: v.url_body == "" ? models.datatypes.unset : v.url_body,
// url_favicon: v.url_favicon == "" ? models.datatypes.unset : v.url_favicon,
// url_image: v.url_image == "" ? models.datatypes.unset : v.url_image,
// url_title: v.url_title == "" ? models.datatypes.unset : v.url_title,
// user_tag_string: v.user_tag_string == "" ? models.datatypes.unset : v.user_tag_string,						
// assign_to: v.assign_to == "" ? models.datatypes.unset : v.assign_to,
// attch_audiofile: v.attch_audiofile != "" ? v.attch_audiofile : models.datatypes.unset,
// attch_imgfile: v.attch_imgfile != "" ? v.attch_imgfile : models.datatypes.unset,
// attch_otherfile: v.attch_otherfile != "" ? v.attch_otherfile : models.datatypes.unset,
// attch_videofile: v.attch_videofile != "" ? v.attch_videofile : models.datatypes.unset,
// call_participants: v.call_participants != "" ? v.call_participants : models.datatypes.unset,
// edit_seen: v.edit_seen != "" ? v.edit_seen : models.datatypes.unset,
// has_delete: v.has_delete != "" ? v.has_delete : models.datatypes.unset,
// has_flagged: v.has_flagged != "" ? v.has_flagged : models.datatypes.unset,
// has_hide: v.has_hide != "" ? v.has_hide : models.datatypes.unset,
// has_tag_text: v.has_tag_text != "" ? v.has_tag_text : models.datatypes.unset,
// issue_accept_user: v.issue_accept_user != "" ? v.issue_accept_user : models.datatypes.unset,
// last_update_user: isUuid(v.last_update_user) ? models.uuidFromString(v.last_update_user) : models.datatypes.unset,
// service_provider: v.service_provider != "" ? v.service_provider : models.datatypes.unset,
// updatedmsgid: isUuid(v.updatedmsgid) ? models.uuidFromString(v.updatedmsgid) : models.datatypes.unset,
// forward_by: isUuid(v.forward_by) ? models.uuidFromString(v.forward_by) : models.datatypes.unset,
// tag_list: v.tag_list != "" ? v.tag_list : models.datatypes.unset,
// secret_user: v.secret_user != "" ? v.secret_user : models.datatypes.unset,
// msg_status: models.datatypes.unset,
// mention_user: v.mention_user != "" ? v.mention_user : models.datatypes.unset,
// last_reply_time: v.last_reply_time != '' ? v.last_reply_time : v.last_update_time,
// old_created_time: v.old_created_time != '' ? v.old_created_time : v.last_update_time,
// call_running: v.call_running === true ? v.call_running : false,
// call_server_switch: v.call_server_switch === true ? v.call_server_switch : false,
// is_secret: v.is_secret === true || v.is_secret === 'true' ? true : false,
// company_id: v.company_id ? v.company_id : models.datatypes.unset,
// root_msg_id: v.root_msg_id ? v.root_msg_id : models.datatypes.unset,
// has_emoji: {'disappointed_relieved':0,'folded_hands':0,'grinning':0,'heart':0,'joy':0,'open_mouth':0,'rage':0,'thumbsdown':0,'thumbsup':0}
// });

// // msg.save(function(err){
// // 	if(err) {
// // 		error.push(v.msg_id);
// // 	}
// // 	success++;
// // });

// query.push(msg.save({ return_query: true }));
// if(query.length == 100){
// var temp = query;
// setTimeout(function(){
// models.doBatch(temp, function(err) {
// if (err) { error.push(v.msg_id);}
// else success++;
// });
// }, Math.floor(Math.random() * 9999) + 100);
// query = [];
// }
// } catch(e) {
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log(e);
// }
// }
// }
// key++;
// }).on('end', async function () {
// if(query.length > 0){
// models.doBatch(query, function(err) {
// if (err) console.log(err);
// console.log(query.length + ' items saved');
// });
// query = [];
// }
// console.log(success + ' msgs import done.');
// console.log('msgs import error '+ error.length, error);
// });
// });
// });

// res.render('index', {title: "Welcome to Message"});
// });


// router.get('/file', function(req, res, next) {
//     console.log(1012, "msgs hit");
//     var query = [];
//     var count = 1;
//     fs.createReadStream('/Office Work/freeli_server_v1/server/temp/temp.csv')
//         .pipe(csv()).on('data', function(v) {
//             if (isUuid(v.user_id)) {
//                 try {
//                     if (v.mention_user != "") {
//                         v.mention_user = v.mention_user.substring(1, v.mention_user.length - 1);
//                         v.mention_user = v.mention_user.replace(/\"/g, "");
//                         v.mention_user = v.mention_user.split(",");
//                     }
//                     if (v.tag_list != "") {
//                         v.tag_list = v.tag_list.substring(1, v.tag_list.length - 1);
//                         v.tag_list = v.tag_list.replace(/\"/g, "");
//                         v.tag_list = v.tag_list.split(",");
//                     }
//                     if (v.secret_user != "") {
//                         v.secret_user = v.secret_user.substring(1, v.secret_user.length - 1);
//                         v.secret_user = v.secret_user.replace(/\"/g, "");
//                         v.secret_user = v.secret_user.split(",");
//                     }
//                     v.has_tag = v.tag_list != null ? 'tag' : 'no';
//                     // v.tag_list_with_user = v.tag_list_with_user ? v.tag_list_with_user : null;
//                     v.key = v.key.indexOf("'") > -1 ? v.key.replace(/'/g, "&apos;") : v.key;
//                     v.location = v.location.indexOf("'") > -1 ? v.location.replace(/'/g, "&apos;") : v.location;
//                     v.originalname = v.originalname.indexOf("'") > -1 ? v.originalname.replace(/'/g, "&apos;") : v.originalname;

//                     var file = new models.instance.TempFile({
//                         id: models.timeuuidFromString(v.id),
//                         conversation_id: models.uuidFromString(v.conversation_id),
//                         user_id: models.uuidFromString(v.user_id),
//                         msg_id: models.timeuuidFromString(v.msg_id),
//                         acl: v.acl == "" ? models.datatypes.unset : v.acl,
//                         bucket: v.bucket == "" ? models.datatypes.unset : v.bucket,
//                         file_type: v.file_type == "" ? models.datatypes.unset : v.file_type,
//                         file_type: v.file_type == "" ? models.datatypes.unset : v.file_type,
//                         key: v.key == "" ? models.datatypes.unset : v.key,
//                         location: v.location == "" ? models.datatypes.unset : v.location,
//                         originalname: v.originalname == "" ? models.datatypes.unset : v.originalname,
//                         file_size: v.file_size == "" ? models.datatypes.unset : v.file_size,
//                         is_delete: v.is_delete == "" ? models.datatypes.unset : parseInt(v.is_delete),
//                         has_tag: v.has_tag == "" ? models.datatypes.unset : v.has_tag,
//                         tag_list_with_user: v.tag_list_with_user == "" ? models.datatypes.unset : v.tag_list_with_user,
//                         mention_user: v.mention_user != "" ? v.mention_user : models.datatypes.unset,
//                         tag_list: v.tag_list != "" ? v.tag_list : models.datatypes.unset,
//                         root_conv_id: v.root_conv_id == "" ? models.datatypes.unset : v.root_conv_id,
//                         created_at: v.created_at == "" ? models.datatypes.unset : v.created_at,
//                         secret_user: v.secret_user != "" ? v.secret_user : models.datatypes.unset,
//                         is_secret: v.is_secret === true ? true : false
//                     });

//                     // msg.save(function(err){
//                     // 	if(err) {
//                     // 		error.push(v.msg_id);
//                     // 	}
//                     // 	success++;
//                     // });

//                     query.push(file.save({ return_query: true }));
//                     if (query.length == 50) {
//                         var temp = query;
//                         setTimeout(function() {
//                             models.doBatch(temp, function(err) {
//                                 if (err) { error.push(v.msg_id); }
//                             });
//                         }, Math.floor(Math.random() * 9999) + 100);
//                         query = [];
//                     }
//                 } catch (e) {
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log(e);
//                 }
//             }
//         }).on('end', async function() {
//             if (query.length > 0) {
//                 models.doBatch(query, function(err) {
//                     if (err) console.log(err);
//                     console.log(query.length + ' items saved');
//                 });
//                 query = [];
//             }
//         });

//     res.render('index', { title: "Welcome to File" });
// });

// router.get('/files', function (req, res, next) {
//     var update_query = [];
//     var n = 1;
//     models.instance.File.eachRow({}, { fetchSize: 100, raw: true }, function (n, row) {
//         var cat = 'docs';
//         if (row.file_type === null) cat = 'docs';
//         if (row.file_type.indexOf('image') > -1) cat = 'image';
//         if (row.file_type.indexOf('audio') > -1) cat = 'audio';
//         if (row.file_type.indexOf('video') > -1) cat = 'video';
//         update_query.push(models.instance.File.update({ id: row.id, company_id: row.company_id }, { has_tag: null, tag_list: null, file_category: cat }, { return_query: true }));
//     }, async function (err, result) {
//         if (err) throw err;
//         await new Promise((resolve) => {
//             if (update_query.length > 0) {
//                 var temp = update_query;
//                 setTimeout(function () {
//                     models.doBatch(temp, function (err) {
//                         if (err) console.log(err);
//                         console.log(1044, n++);
//                         update_query = [];
//                         resolve();
//                     });
//                 }, Math.floor(Math.random() * 9999) + 100);
//             }
//         });
//         if (result.nextPage) {
//             result.nextPage();
//         }
//     });
//     res.render('index', { title: "Update to Files" });
// });

// router.get('/files_other', function (req, res, next) {
//     var update_query = [];
//     var no = 1;
//     models.instance.Conversation.find({}, { raw: true }, function (e, convs) {
//         if (e) console.log(1061, e);
//         var c = {};
//         for (let v of convs) {
//             c[v.conversation_id.toString()] = v.participants;
//         }
//         models.instance.File.eachRow({}, { fetchSize: 100, raw: true }, function (n, row) {
//             if (c.hasOwnProperty(row.conversation_id.toString())) {
//                 var update_data = {
//                     other_user: (c[row.conversation_id.toString()]).filter((a) => { return a != row.user_id.toString(); }),
//                     participants: c[row.conversation_id.toString()]
//                 };
//                 update_query.push(models.instance.File.update({ id: row.id, company_id: row.company_id }, update_data, { return_query: true }));
//             }
//         }, async function (err, result) {
//             if (err) throw err;
//             await new Promise((resolve) => {
//                 if (update_query.length > 0) {
//                     var temp = update_query;
//                     setTimeout(function () {
//                         models.doBatch(temp, function (err) {
//                             if (err) console.log(err);
//                             console.log(1080, no++);
//                             update_query = [];
//                             resolve();
//                         });
//                     }, Math.floor(Math.random() * 999) + 100);
//                 }
//             });
//             if (result.nextPage) {
//                 result.nextPage();
//             }
//         });
//     });
//     res.render('index', { title: "Update to File" });
// });

// router.get('/update_msg_par', function(req, res, next) {
//     var update_query = [];
//     var no = 1;
//     models.instance.Conversation.find({}, {raw: true}, function(e, convs){
//         if(e) console.log(1102, e);
//         var c = {};
//         for(let v of convs){
//             c[v.conversation_id.toString()] = v.participants;
//         }
//         models.instance.Messages.eachRow({}, { fetchSize: 100, raw: true }, function(n, row) {
//             if(c.hasOwnProperty(row.conversation_id.toString())){
//                 var update_data = {
//                     participants: c[row.conversation_id.toString()]
//                 };
//                 update_query.push(models.instance.Messages.update({id: row.id, conversation_id: row.conversation_id}, update_data, { return_query: true } ));
//             }
//         }, async function(err, result) {
//             if (err) throw err;
//             await new Promise((resolve) => {
//                 if (update_query.length > 0) {
//                     models.doBatch(update_query, function(err) {
//                         if (err) console.log(err);
//                         console.log(1120, no++);
//                         update_query = [];
//                         resolve();
//                     });
//                 }
//             });
//             if (result.nextPage) {
//                 result.nextPage();
//             }
//         });
//     });
//     res.render('index', { title: "Update to Messages" });
// });

// router.get('/files2file', function (req, res, next) {
//     var update_query = [];
//     var no = 1;
//     models.instance.Conversation.find({}, { raw: true }, function (e, convs) {
//         if (e) console.log(1061, e);
//         var c = {};
//         console.log(1140, convs.length);
//         for (let v of convs) {
//             c[v.conversation_id.toString()] = v;
//         }
//         models.instance.Files.eachRow({}, { fetchSize: 100, raw: true }, function (n, row) {
//             if (c.hasOwnProperty(row.conversation_id.toString())) {
//                 row.company_id = c[row.conversation_id.toString()].company_id.toString();
//                 let newrow = new models.instance.File(row);
//                 update_query.push(newrow.save({ return_query: true }));
//             }
//         }, async function (err, result) {
//             if (err) throw err;
//             await new Promise((resolve) => {
//                 if (update_query.length > 0) {
//                     models.doBatch(update_query, function (err) {
//                         if (err) console.log(err);
//                         console.log(1080, no++);
//                         update_query = [];
//                         resolve();
//                     });
//                 }
//             });
//             if (result.nextPage) {
//                 result.nextPage();
//             }
//         });
//     });
//     res.render('index', { title: "Files 2 File" });
// });

// router.get('/file_tag_null', function (req, res, next) {
//     var update_query = [];
//     var no = 1;
//     models.instance.File.eachRow({}, { fetchSize: 100, raw: true }, function (n, row) {
//         // if(! Array.isArray(row.tag_list)){
//         update_query.push(models.instance.File.update({ id: row.id, company_id: row.company_id }, { has_tag: null, tag_list: null }, { return_query: true }));
//         // }
//     }, async function (err, result) {
//         if (err) throw err;
//         await new Promise((resolve) => {
//             if (update_query.length > 0) {
//                 models.doBatch(update_query, function (err) {
//                     if (err) console.log(err);
//                     console.log(1183, no++);
//                     update_query = [];
//                     resolve();
//                 });
//             } else {
//                 resolve();
//             }
//         });
//         if (result.nextPage) {
//             result.nextPage();
//         }
//     });
//     res.render('index', { title: "File tag null" });
// });


// router.get('/cus_msg_del', function (req, res, next) {
//     models.instance.Messages.delete({ conversation_id: models.uuidFromString(req.body.conversation_id), msg_id: models.timeuuidFromString(req.body.msg_id) }, function (err) {
//         if (err) console.log(1201, err);
//         res.render('index', { title: "Custom messages delete done." });
//     });
// });



// router.get('/checklist', function(req, res, next) {
//     console.log(1106, "checklist hit");
// 	var query = [];
// 	var count = 1;
// 	fs.createReadStream('/Office/freeli_react_1/server/temp/v24_checklist.csv')
// 	.pipe(csv()).on('data', function (v) {
// 			if(isUuid(v.created_by)){
// 				try{
// 					if(v.participant_id != ""){
// 						v.participant_id = v.participant_id.substring(1, v.participant_id.length-1);
// 						v.participant_id = v.participant_id.replace(/\"/g, "");
// 						v.participant_id = v.participant_id.split(",");
// 					}

// 					v.msg_title = v.msg_title.indexOf("'") > -1 ? v.msg_title.replace(/'/g, "&apos;") : v.msg_title;
// 					v.checklist_title = v.checklist_title.indexOf("'") > -1 ? v.checklist_title.replace(/'/g, "&apos;") : v.checklist_title;	

// 						var checklist = new models.instance.MessageChecklist({
// 							checklist_id: models.timeuuidFromString(v.checklist_id),
// 							msg_id: models.timeuuidFromString(v.msg_id),
// 							msg_title: v.msg_title == "" ? models.datatypes.unset : v.msg_title,
// 							convid: v.convid == "" ? models.datatypes.unset : v.convid,
// 							original_ttl: v.original_ttl == "" ? models.datatypes.unset : v.original_ttl,
// 							assignedby: v.assignedby == "" ? models.datatypes.unset : v.assignedby,
// 							assignby_role: v.assignby_role == "" ? models.datatypes.unset : v.assignby_role,
// 							alternative_assign_to: v.alternative_assign_to == "" ? models.datatypes.unset : v.alternative_assign_to,
// 							assignee_change_reason: v.assignee_change_reason == "" ? models.datatypes.unset : v.assignee_change_reason,
// 							Completed_status_updated_by: v.Completed_status_updated_by == "" ? models.datatypes.unset : v.Completed_status_updated_by,
// 							Request_ttl_by: v.Request_ttl_by == "" ? models.datatypes.unset : v.Request_ttl_by,
// 							request_ttl_message: v.request_ttl_message == "" ? models.datatypes.unset : v.request_ttl_message,
// 							request_ttl_approved_by: v.request_ttl_approved_by == "" ? models.datatypes.unset : v.request_ttl_approved_by,
// 							request_ttl_time: v.request_ttl_time != "" ? v.request_ttl_time : models.datatypes.unset,
// 							Is_direct_group: v.Is_direct_group != "" ? v.Is_direct_group : models.datatypes.unset,
// 							participant_id: v.participant_id != "" ? v.participant_id : models.datatypes.unset,
// 							request_repetition: v.request_repetition == "" ? models.datatypes.unset : parseInt(v.request_repetition),
// 							from_user_counter: v.from_user_counter == "" ? models.datatypes.unset : parseInt(v.from_user_counter),
// 							to_user_counter: v.to_user_counter == "" ? models.datatypes.unset : parseInt(v.to_user_counter),
// 							to_user_counter: v.to_user_counter == "" ? models.datatypes.unset : parseInt(v.to_user_counter),
// 							request_ttl_approved_date: v.request_ttl_approved_date == "" ? models.datatypes.unset : v.request_ttl_approved_date,
// 							request_ttl_date: v.request_ttl_date != "" ? v.request_ttl_date : models.datatypes.unset,
// 							created_at: v.created_at != "" ? v.created_at : models.datatypes.unset,
// 							checklist_title: v.checklist_title != "" ? v.checklist_title : models.datatypes.unset,
// 							last_updated_by: v.last_updated_by != "" ? v.last_updated_by : models.datatypes.unset,
// 							last_action: v.last_action != "" ? v.last_action : models.datatypes.unset,
// 							last_edited_by: v.last_edited_by != "" ? v.last_edited_by : models.datatypes.unset,
// 							start_due_date: v.start_due_date != "" ? v.start_due_date : models.datatypes.unset,
// 							end_due_date: v.end_due_date != "" ? v.end_due_date : models.datatypes.unset,
// 							assign_to: v.assign_to != "" ? v.assign_to : models.datatypes.unset,
// 							privacy: v.privacy != "" ? v.privacy : models.datatypes.unset,
// 							assign_status: v.assign_status != "" ? v.assign_status : models.datatypes.unset,
// 							assign_decline_note: v.assign_decline_note != "" ? v.assign_decline_note : models.datatypes.unset,
// 							last_edited_at: v.last_edited_at != "" ? v.last_edited_at : models.datatypes.unset,
// 							checklist_status: v.checklist_status != "" ? parseInt(v.checklist_status) : models.datatypes.unset,
// 							root_conv_id: v.root_conv_id != "" ? v.root_conv_id : models.datatypes.unset,
// 							review_status: v.review_status != "" ? parseInt(v.review_status) : models.datatypes.unset,
// 							created_by: isUuid(v.created_by) ? models.uuidFromString(v.created_by) : models.datatypes.unset,
// 							is_secret: v.is_secret === true ? true : false
// 						});

// 						query.push(checklist.save({ return_query: true }));
// 						if(query.length == 100){
// 							var temp = query;
// 							setTimeout(function(){
// 								models.doBatch(temp, function(err) {
// 									if (err) { error.push(v.msg_id);}
// 								});
// 							}, Math.floor(Math.random() * 9999) + 100);
// 							query = [];
// 						}
// 				} catch(e) {
// 					console.log("");
// 					console.log("");
// 					console.log("");
// 					console.log("");
// 					console.log("");
// 					console.log("");
// 					console.log("");
// 					console.log(e);
// 				}
// 			}
// 	}).on('end', async function () {
// 		if(query.length > 0){
// 			models.doBatch(query, function(err) {
// 				if (err) console.log(err);
// 				console.log(query.length + ' items saved');
// 			});
// 			query = [];
// 		}
// 	});

// 	res.render('index', {title: "Welcome to Checklist"});
// });
row = null;

// router.get('/user_update', function(req, res, next) {
// console.log(1094, "user_update");
// var count = 0;
// models.instance.Users.eachRow({}, { fetchSize: 1, raw: true }, function(n, _row) {
// row = _row;
// count++;
// console.log(count)

// // invoked per each row in all the pages
// }, async function(err, result) {
// // called once the page has been retrieved.
// if (err) throw err;
// await new Promise((resolve) => {
// models.instance.Users.update({ id: row.id }, { password: passwordToHass('a123456') }, update_if_exists, function(e) {
// if (e) console.log(e);
// else {
// // count++;
// console.log(row.email + ' user update successfully...');
// resolve()

// }
// });
// });
// if (result.nextPage) {

// result.nextPage();

// // retrieve the following pages
// // the same row handler from above will be used
// // if (row.email !== null && row.email != "") {
// //     models.instance.Users.find({ email: row.email }, { raw: true, allow_filtering: true }, async function(e, user) {
// //         if (e) console.log(1098, e);
// //         if (user.length > 1) {
// //             var more_use_pass = user[0].password;
// //             var most_login = parseInt(user[0].login_total);
// //             var uids = [];
// //             user.forEach(function(v, k) {
// //                 uids.push(v.id);
// //                 if (parseInt(v.login_total) > most_login) {
// //                     most_login = parseInt(v.login_total);
// //                     more_use_pass = v.password;
// //                 }
// //             });
// //             if (more_use_pass == '' || more_use_pass == "a123456") more_use_pass = passwordToHass('a123456');
// //             for (const uid of uids) {
// //                 await new Promise((resolve) => {
// //                     models.instance.Users.update({ id: uid }, { password: (more_use_pass) }, update_if_exists, function(e) {
// //                         if (e) console.log(e, user[0].email);
// //                         else {
// //                             // count++;
// //                             console.log(user[0].email + ' user update successfully...');
// //                             resolve()

// //                         }
// //                     });
// //                 })

// //             };
// //             result.nextPage();

// //         } else {
// //             // var more_use_pass = user[0].password;
// //             // if (more_use_pass == '' || more_use_pass=="a123456") more_use_pass = passwordToHass('a123456');
// //             // await new Promise((resolve) => {
// //             //     models.instance.Users.update({ id: user[0].id }, { password: (more_use_pass) }, update_if_exists, function(e) {
// //             //         if (e) console.log(e, user[0].email);
// //             //         else {
// //             //             // count++;
// //             //             console.log(user[0].email + ' user update successfully...');
// //             //             resolve()

// //             //         }
// //             //     });
// //             // })
// //             result.nextPage();
// //         }
// //     });
// // } else {
// //     // var more_use_pass = row.password;
// //     // if (more_use_pass == '' || more_use_pass=="a123456") more_use_pass = passwordToHass('a123456');
// //     // await new Promise((resolve) => {
// //     //     models.instance.Users.update({ id: row.id }, { password: (more_use_pass) }, update_if_exists, function(e) {
// //     //         if (e) console.log(e, row.email);
// //     //         else {
// //     //             // count++;
// //     //             console.log(row.email + ' user update successfully...');
// //     //             resolve()

// //     //         }
// //     //     });
// //     // })
// //     result.nextPage();
// // }


// }
// });

// // var count = 0;
// models.instance.Users.stream({}, { raw: true, allow_filtering: true }, function(reader) {
// while (row = reader.readRow()) {
// // console.log(1098, row);
// if (row.email !== null && row.email != "") {
// models.instance.Users.find({ email: row.email }, { raw: true, allow_filtering: true }, function(e, user) {
// if (e) console.log(1098, e);
// if (user.length > 1) {
// var more_use_pass = user[0].password;
// var most_login = parseInt(user[0].login_total);
// var uids = [];
// user.forEach(function(v, k) {
// uids.push(v.id);
// if (parseInt(v.login_total) > most_login) {
// most_login = parseInt(v.login_total);
// more_use_pass = v.password;
// }
// });

// models.instance.Users.update({ id: { '$in': uids } }, { password: more_use_pass }, update_if_exists, function(e) {
// if (e) console.log(e, user[0].email);
// else { count++;
// console.log(user[0].email + ' user update successfully...'); }
// });
// }
// });
// }
// }
// // }, function(err) {
// //     if (err) console.log(43, err);
// //     console.log('Total ' + count + ' users updated.');
// // });

// res.render('index', { title: "User update" });
// });

// router.get('/msg_link', function(req, res, next) {
// console.log(1234, "msgs link hit");
// var count = 3500;
// var key = 0;
// fs.createReadStream('/Office Work/freeli_react_1/server/temp/v24_link.csv')
// .pipe(csv()).on('data', function (v) {
// if(key>count && count < 4000){
// if(isUuid(v.conversation_id) && isUuid(v.msg_id)){
// try{
// v.title = v.title ? v.title : '';
// v.title = v.title.indexOf("'") > -1 ? v.title.replace(/'/g, "&apos;") : v.title;

// var update_data = {
// msg_type: 'text link',
// url_base_title: v.title,
// url_body: v.url
// };
// models.instance.Messages.update({msg_id: models.timeuuidFromString(v.msg_id), conversation_id: models.uuidFromString(v.conversation_id) }, update_data, update_if_exists, function(e){
// if (e) throw e;
// console.log(key, count);
// })

// } catch(e) {
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log("");
// console.log(e);
// }
// }
// count++
// }
// key++;
// }).on('end', function () {
// console.log(count + ' messages update');
// });

// res.render('index', {title: " Messages Update for link"});
// });

// router.post('/getCallingUrl', function (req, res, next) {
//     let d1 = Date.now();
//     if (req.body.type == 'user') {
//         models.instance.Users.findOne({ short_id: req.body.short_id }, { raw: true, allow_filtering: true }, async function (error, user) {
//             console.log('getCallingUrl:user:', Date.now() - d1);
//             if (user) {
//                 let user_id = user.id.toString();
//                 let url = "https://wfvs001.freeli.io/" + user_id.replace(/-/g, "");

//                 // let url = new URL( "/call/"+ convs.conversation_id.toString(),req.headers.origin);
//                 return res.json({
//                     url: url,
//                     participants: [user_id],
//                     conversation_id: user_id,
//                     is_running: (voip_conv_store.hasOwnProperty(user_id) && voip_conv_store[user_id].database_update == false),
//                     // title: convs.title,
//                     conversation_type: 'personal'
//                 });
//             } else {
//                 return res.json({ url: false });
//             }
//         });

//     } else {
//         models.instance.Conversation.findOne({ short_id: req.body.short_id }, { raw: true, allow_filtering: true }, async function (error, convs) {
//             console.log('getCallingUrl:conv:', Date.now() - d1);
//             if (convs) {
//                 let conv_id = convs.conversation_id.toString();
//                 let url = "https://wfvs001.freeli.io/" + conv_id.replace(/-/g, "");
//                 // let url = new URL( "/call/"+ conv_id,req.headers.origin);
//                 return res.json({
//                     url: url,
//                     participants: convs.participants,
//                     conversation_id: conv_id,
//                     is_running: (voip_conv_store.hasOwnProperty(conv_id) && voip_conv_store[conv_id].database_update == false),
//                     title: convs.title,
//                     conversation_type: convs.group == 'yes' ? 'group' : 'personal'
//                 });
//             } else {
//                 return res.json({ url: false });
//             }
//         });

//     }

// });
// router.post('/setCallingUrl', function (req, res, next) {
//     if (req.body.type == 'group') {
//         var query = {
//             conversation_id: models.uuidFromString(req.body.conversation_id),
//             company_id: models.timeuuidFromString(req.body.company_id)
//         }
//         if (req.body.action == 'get') {
//             models.instance.Conversation.findOne(query, function (error, conv) {
//                 if (conv && conv.short_id) {
//                     return res.json({ status: true, short_id: conv.short_id });
//                 } else {
//                     let short_id_new = short.generate();
//                     models.instance.Conversation.update(query, { short_id: short_id_new }, update_if_exists, function (err) {
//                         if (err) {
//                             console.log(err);
//                             return res.json({ status: false });
//                         } else {
//                             return res.json({ status: true, short_id: short_id_new });
//                         }
//                     });
//                 }
//             });

//         } else if (req.body.action == 'reset') {
//             let short_id_new = short.generate();
//             models.instance.Conversation.update(query, { short_id: short_id_new }, update_if_exists, function (err) {
//                 if (err) {
//                     console.log(err);
//                     return res.json({ status: false });
//                 } else {
//                     return res.json({ status: true, short_id: short_id_new });
//                 }
//             });

//         }


//     } else if (req.body.type == 'user') {
//         var query = { id: models.uuidFromString(req.body.user_id) };
//         if (req.body.action == 'get') {

//             models.instance.Users.findOne(query, function (error, user) {
//                 if (user && user.short_id) {
//                     return res.json({ status: true, short_id: user.short_id });
//                 } else {
//                     let short_id_new = short.generate();
//                     models.instance.Users.update(query, { short_id: short_id_new }, update_if_exists, function (err) {
//                         if (err) {
//                             console.log(err);
//                             return res.json({ status: false });
//                         } else {
//                             all_users[req.body.company_id][req.body.user_id].short_id = short_id_new;
//                             return res.json({ status: true, short_id: short_id_new });
//                         }
//                     });
//                 }
//             });

//         } else if (req.body.action == 'reset') {
//             let short_id_new = short.generate();
//             models.instance.Users.update(query, { short_id: short_id_new }, update_if_exists, function (err) {
//                 if (err) {
//                     console.log(err);
//                     return res.json({ status: false });
//                 } else {
//                     all_users[req.body.company_id][req.body.user_id].short_id = short_id_new;
//                     return res.json({ status: true, short_id: short_id_new });
//                 }
//             });

//         }

//     }

// });



// router.get('/usertag', function (req, res, next) {
//     console.log(1012, "usertag hit");
//     var query = [];
//     var count = 1;
//     fs.createReadStream('/Office Work/freeli_server_v1/server/temp/live22_usertag.csv')
//         .pipe(csv()).on('data', function (v) {
//             if (isUuid(v.tagged_by) && isUuid(v.company_id)) {
//                 try {
//                     v.title = v.title.indexOf("'") > -1 ? v.title.replace(/'/g, "&apos;") : v.title;
//                     if (v.mention_users != "") {
//                         v.mention_users = v.mention_users.substring(1, v.mention_users.length - 1);
//                         v.mention_users = v.mention_users.replace(/\"/g, "");
//                         v.mention_users = v.mention_users.split(",");
//                     }

//                     if (v.team_list != "") {
//                         v.team_list = v.team_list.substring(1, v.team_list.length - 1);
//                         v.team_list = v.team_list.replace(/\"/g, "");
//                         v.team_list = v.team_list.split(",");
//                     }

//                     var tag = new models.instance.UserTag({
//                         tag_id: models.timeuuidFromString(v.tag_id),
//                         tagged_by: models.uuidFromString(v.tagged_by),
//                         title: v.title == "" ? models.datatypes.unset : v.title,
//                         company_id: v.company_id == "" ? models.datatypes.unset : v.company_id,
//                         type: v.type == "" ? models.datatypes.unset : v.type,
//                         shared_tag: v.shared_tag == "" ? models.datatypes.unset : v.shared_tag,
//                         visibility: v.visibility == "" ? models.datatypes.unset : v.visibility,
//                         tag_type: v.tag_type == "" ? models.datatypes.unset : v.tag_type,
//                         tag_color: v.tag_color == "" ? models.datatypes.unset : v.tag_color,
//                         mention_users: v.mention_users == "" ? models.datatypes.unset : v.mention_users,
//                         team_list: v.team_list == "" ? models.datatypes.unset : v.team_list,
//                         created_at: v.created_at == "" ? models.datatypes.unset : v.created_at,
//                         update_at: v.update_at == "" ? models.datatypes.unset : v.update_at,
//                         update_by: v.update_by != "" ? v.update_by : models.datatypes.unset
//                     });

//                     query.push(tag.save({ return_query: true }));
//                     if (query.length == 50) {
//                         var temp = query;
//                         setTimeout(function () {
//                             models.doBatch(temp, function (err) {
//                                 if (err) { error.push(v.msg_id); }
//                             });
//                         }, Math.floor(Math.random() * 9999) + 100);
//                         query = [];
//                     }
//                 } catch (e) {
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log("");
//                     console.log(e);
//                 }
//             }
//         }).on('end', async function () {
//             if (query.length > 0) {
//                 models.doBatch(query, function (err) {
//                     if (err) console.log(err);
//                     console.log(query.length + ' items saved');
//                 });
//                 query = [];
//             }
//         });

//     res.render('index', { title: "Welcome to User tag" });
// });

// router.get('/msg_type_update', function(req, res, next) {
//     console.log(1299, "msg_type_update");
//     var count = 0;
// 	var msg_file;
// 	models.instance.Messages.eachRow({msg_type : { $like: 'media_imgfile%' } }, { fetchSize: 1, raw: true }, function(n, _row) {
//         msg_file = _row;
//     }, async function(err, result) {
//         // called once the page has been retrieved.
//         if (err) throw err;
//         await new Promise((resolve) => {
//             models.instance.Messages.update({ msg_id: msg_file.msg_id, conversation_id: msg_file.conversation_id }, { msg_type: 'media_attachment' }, update_if_exists, function(e) {
//                 if (e) console.log(e);
//                 else {
//                     count++;
//                     console.log(count + ' done');
//                     resolve()

//                 }
//             });
//         });
//         if (result.nextPage) {
//             result.nextPage();
//         }
//     });


//     res.render('index', { title: "User update" });
// });

// router.get('/msg_type_update', function (req, res, next) {
//     console.log(1299, "msg_type_update");
//     var count = 0;
//     var c = 0;
//     var msg_file;
//     models.instance.Messages.eachRow({ msg_type: { $like: 'text link' } }, { fetchSize: 1, raw: true }, function (n, _row) {
//         msg_file = _row;
//         console.log('read ', c++);
//     }, async function (err, result) {
//         // called once the page has been retrieved.
//         if (err) throw err;
//         await new Promise((resolve) => {
//             if (msg_file.attch_audiofile == null && msg_file.attch_imgfile == null && msg_file.attch_videofile == null && msg_file.attch_otherfile == null) resolve()
//             else {
//                 models.instance.Messages.update({ msg_id: msg_file.msg_id, conversation_id: msg_file.conversation_id }, { msg_type: 'text link media_attachment' }, update_if_exists, function (e) {
//                     if (e) console.log(e);
//                     else {
//                         console.log('change ', count++);
//                         resolve()

//                     }
//                 });
//             }
//         });
//         if (result.nextPage) {
//             result.nextPage();
//         }
//     });


//     res.render('index', { title: "User update" });
// });

// router.get('/msg_star_update', function (req, res, next) {
//     console.log(1299, "msg_star_update");
//     var count = 0;
//     var c = 0;
//     var queries = [];
//     models.instance.File.eachRow({}, { fetchSize: 300, raw: true }, function (n, row) {
//         if (Array.isArray(row.star)) {
//             console.log('update query ', c++);
//             queries.push(models.instance.Messages.update({ conversation_id: row.conversation_id, msg_id: row.msg_id }, { has_star: { $add: [...row.star] } }, { return_query: true }));
//         }
//     }, function (err, result) {
//         // called once the page has been retrieved.
//         if (err) console.log(1730, err);

//         if (queries.length > 0) {
//             var temp = queries;
//             queries = [];
//             models.doBatch(temp, function (e) {
//                 if (e) console.log(1734, e);
//                 else
//                     console.log('change ', temp.length);
//             });
//         }

//         if (result.nextPage) {
//             queries = [];
//             console.log('file read ', count++);
//             result.nextPage();
//         }
//     });
//     res.render('index', { title: "msg_star_update" });
// });

const endpointSecret = "whsec_c692f0e118ca6e78ebbbe78f48bfc9e07d1c51e7eab8a837beaff7e4872162fe";
router.post('/webhook', express.json({ type: 'application/json' }), async (req, res) => {
    const sig = req.headers['stripe-signature'];
    // let event;

    // try {
    //   event = stripe.webhooks.constructEvent(req.rawBody, sig, endpointSecret);
    // } catch (err) {
    //   res.status(400).send(`Webhook Error: ${err.message}`);
    //   return;
    // }
    const event = req.body;
    console.log('stripe:webhook:', event.type);

    // Handle the event
    switch (event.type) {
        case 'payment_intent.succeeded':
            var paymentIntent = event.data.object;
            // Then define and call a method to handle the successful payment intent.
            // handlePaymentIntentSucceeded(paymentIntent);
            break;
        case 'payment_method.attached':
            var paymentMethod = event.data.object;
            // Then define and call a method to handle the successful attachment of a PaymentMethod.
            // handlePaymentMethodAttached(paymentMethod);
            break;
        case 'invoice.payment_succeeded':
            var customer = await stripe.customers.retrieve(event.data.object.customer, { expand: ['subscriptions'] });
            // var customer = await stripe.customers.retrieve('cus_LpHD6IYF80wk9D', {expand: ['subscriptions']});

            var paymentCard = null;
            if (customer.invoice_settings.default_payment_method) {
                paymentCard = await stripe.paymentMethods.retrieve(customer.invoice_settings.default_payment_method);
            }
            var amount = customer.subscriptions.data[0].plan.amount / 100;
            var next_date = moment().add(3, 'days').format("LL");


            var eHtml = `Hi ${customer.name},<br><br>
            Your subscription plan has successfully been renewed for this billing cycle. Your invoice/receipt is attached herewith. We hope you and your team enjoy using Workfreeli to connect and collaborate with each other.<br><br>
            <b>ESSENTIAL PLAN</b><br>
            <b>Account Name: ${customer.description} workspace</b><br>
            <b>Subscribed with email ID: ${customer.email}</b><br>
            Charged \$${amount} on ${moment().format("LL")} <br>
            Payment method: Credit Card (last 4 digits: ${paymentCard.card.last4})<br>
            This plan includes ${customer.metadata.plan_user_limit} users and ${customer.metadata.plan_storage_limit} GB of storage.<br><br>
            By subscribing, you have authorized us to charge you the subscription fee automatically until cancelled. Please keep this invoice for your records.<br><br>
            To upgrade or cancel your subscription or to buy additional storage, please go to Admin Panel -> Subscription & Billing. Admin Panel can only be accessed from an administrator’s account.<br><br>
            Regards,<br><br>
            <img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br>
            WORKFREELI Team<br>
            W: workfreeli.com<br>
            E: support@workfreeli.com<br>
            `;

            sg_email.send({
                to: customer.email,
                // to: 'sujon.xpress@gmail.com',
                // bcc: 'mahfuzak08@gmail.com',
                subject: 'Invoice for Your Workfreeli Monthly Subscription',
                text: 'RECURRING SUBSCRIPTION INVOICES',
                html: eHtml,

            }, (result) => {
                if (result.msg == 'success') {
                    res.status(200);
                } else {
                    res.status(500);
                }
            });

            break;
        case 'invoice.payment_failed':
            // var customer = await stripe.customers.retrieve(event.data.object.customer);
            var customer = await stripe.customers.retrieve(event.data.object.customer, { expand: ['subscriptions'] });

            var paymentCard = null;
            if (customer.invoice_settings.default_payment_method) {
                paymentCard = await stripe.paymentMethods.retrieve(customer.invoice_settings.default_payment_method);
            }
            var amount = customer.subscriptions.data[0].plan.amount / 100;
            var next_date = moment().add(3, 'days').format("LL");

            var eHtml = `Hi ${customer.name},<br><br>
            We tried to charge the monthly subscription fee against your current plan for your account and unfortunately the payment has been declined. We’ll try to process the payment again on ${next_date}. Please make sure that your credit card is active and updated with sufficient balance.<br><br>
            Please find below the charge details:<br><br>
            <b>ESSENTIAL PLAN - \$${amount}/month</b><br>
            <b>Account Name: ${customer.description} workspace</b><br>
            <b>Subscribed with email ID: ${customer.email}</b><br>
            <b>Tried to charge \$${amount} on ${moment().format("LL")} </b><br>
            <b>Payment method: Credit Card (last 4 digits: ${paymentCard.card.last4}) </b><br>
            <b>Payment Processor Gateway: Stripe </b><br>
            <b>Payment Process Status: Declined </b><br>
            <b>Reason: [stripe api decline feedback]</b><br><br>
            To add, edit or update your card details or subscription plan, please go to Admin Panel -> Subscription & Billing. Admin Panel can only be accessed from an administrator’s account.<br><br>
            Regards,<br><br>
            <img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br>
            WORKFREELI Team<br>
            W: workfreeli.com<br>
            E: support@workfreeli.com<br>
            `;

            sg_email.send({
                to: customer.email,
                // to: 'sujon.xpress@gmail.com',
                // bcc: 'mahfuzak08@gmail.com',
                subject: 'Payment has been declined and Your Workfreeli subscription plan could not be renewed!',
                text: 'PAYMENT DECLINED!',
                html: eHtml,
                // attachments: [{
                //     content: fs.readFileSync(file_name).toString("base64"),
                //     filename: "attachment.pdf",
                //     type: "application/pdf",
                //     disposition: "attachment"
                // }]
            }, (result) => {
                if (result.msg == 'success') {
                    res.status(200);
                } else {
                    res.status(500);
                }
            });
            break;
        // ... handle other event types
        default:
            console.log(`Stripe event type ${event.type}`);
    }

    // Return a response to acknowledge receipt of the event
    res.status(200).json({ received: true });
});

// router.get('/user_role_update', function(req, res, next) {
//     console.log(1536, "user_role_update");
//     var count = 0;
//     var c = 0;
//     // var rows;
//     models.instance.Users.eachRow({},  { fetchSize: 300, raw: true }, async function(n, row) {
//         console.log('read ', c++);
//         if(row.role == 'System Admin' || row.role == 'Normal User'){
//             await new Promise((resolve) => {
//                 if(row.role == 'System Admin'){
//                     models.instance.Users.update({ id: row.id }, { role: 'Admin' }, update_if_exists, function(e) {
//                         if (e) console.log(e);
//                         else {
//                             console.log('change ', count++);
//                             resolve()
//                         }
//                     });
//                 }
//                 else if(row.role == 'Normal User'){
//                     models.instance.Users.update({ id: row.id }, { role: 'Member' }, update_if_exists, function(e) {
//                         if (e) console.log(e);
//                         else {
//                             console.log('change ', count++);
//                             resolve()
//                         }
//                     });
//                 }
//                 else{
//                     resolve()
//                 }
//             });
//         }
//     }, async function(err, result) {
//         // called once the page has been retrieved.
//         if (err) throw err;
//         if (result.nextPage) {
//             result.nextPage();
//         }
//     });


//     res.render('index', { title: "User update" });
// });

// router.get('/file_tag_with_user_update', function (req, res, next) {
//     console.log(2135, "file_tag_with_user_update");
//     var c = 0;
//     var s = 0;
//     var tag = {};
//     var conv = {};
//     models.instance.Conversation.find({}, { raw: true }, function (err1, convs) {
//         if (err1) console.log(2139, err1);
//         console.log("all conversations read done = " + convs.length);
//         for (let i = 0; i < convs.length; i++) {
//             conv[convs[i].conversation_id.toString()] = convs[i];
//         }
//         models.instance.Tag.find({}, { raw: true }, function (err2, tags) {
//             if (err2) console.log(2143, err2);
//             console.log("total tag read done = " + tags.length);
//             for (let i = 0; i < tags.length; i++) {
//                 tag[tags[i].tag_id.toString()] = tags[i];
//             }
//             models.instance.File.stream({}, { raw: true }, function (reader) {
//                 var row;
//                 while (row = reader.readRow()) {
//                     if (row.tag_list !== null && Array.isArray(row.tag_list)) {
//                         console.log(++c);
//                         var tag_list_with_user = [];
//                         for (let i = 0; i < row.tag_list.length; i++) {
//                             if (tag.hasOwnProperty(row.tag_list[i]) && conv.hasOwnProperty(row.conversation_id.toString())) {
//                                 if (tag[row.tag_list[i]].tag_type === 'public') {
//                                     tag_list_with_user.push({ "tag_id": tag[row.tag_list[i]].tag_id.toString(), "created_by": conv[row.conversation_id.toString()].created_by.toString() });
//                                 } else {
//                                     tag_list_with_user.push({ "tag_id": tag[row.tag_list[i]].tag_id.toString(), "created_by": tag[row.tag_list[i]].tagged_by.toString() });
//                                 }
//                             }
//                         }

//                         models.instance.File.update({ company_id: row.company_id, id: row.id }, { tag_list_with_user: tag_list_with_user.length > 0 ? JSON.stringify(tag_list_with_user) : null }, function (eu) {
//                             if (eu) console.log(2170, eu);
//                             else console.log("success = ", ++s);
//                         });

//                     }
//                 }
//             }, function (err5) {
//                 if (err5) console.log(2175, err5);
//                 res.send({ msg: 'done ' + s });
//             });
//         });
//     });
// });

// router.get('/tag_with_user_update', function(req, res, next) {
//     console.log(2185, "tag_with_user_update");
//     var c = 0;
//     // var n = 0;
//     var tag = {};
//     var conv = {};
//     var file = [];
//     models.instance.Conversation.find({},  {raw: true}, function(err1, convs) {
//         if (err1) console.log(2139, err1);
//         console.log("all conversations read done = " + convs.length);
//         for(let i=0; i<convs.length; i++){
//             conv[convs[i].conversation_id.toString()] = convs[i];
//         }
//         models.instance.Tag.find({},  {raw: true}, function(err2, tags) {
//             if (err2) console.log(2143, err2);
//             console.log("total tag read done = " + tags.length);
//             for(let i=0; i<tags.length; i++){
//                 tag[tags[i].tag_id.toString()] = tags[i];
//             }

//             console.log({msg: 'read done convs = ' + convs.length + ' tags = ' + tags.length });
//             var connected_user = [];
//             var user_use_count = {};
//             var my_use_count = {};
//             var use_count = 0;
//             var conversation_ids = "";
//             var q = [];
//             // all tags
//             for(let i=0; i<tags.length; i++){
//                 // jodi tag use kora hoy
//                 if(tags[i].use_count && tags[i].use_count > 0){
//                     models.instance.File.find({tag_list: {$contains: (tags[i].tag_id.toString())}, is_delete: 0}, {raw: true, allow_filtering: true}, function(e, files){
//                         if(e) console.log(2214, e);
//                         else{
//                             connected_user = [];
//                             user_use_count = {};
//                             my_use_count = {};
//                             use_count = 0;
//                             conversation_ids = "";
//                             console.log("==================", files.length);
//                             // for(let j=0; j<files.length; j++){
//                             //     console.log(files[j].is_delete);
//                             // }
//                             if(files.length>0){
//                                 if(tags[i].tag_type === 'public' ){
//                                     use_count = files.length;
//                                     for(let j=0; j<files.length; j++){
//                                         if(conv.hasOwnProperty(files[j].conversation_id.toString()) && conv[files[j].conversation_id.toString()].participants){
//                                             for(let k=0; k<conv[files[j].conversation_id.toString()].participants.length; k++){
//                                                 if(connected_user.indexOf(conv[files[j].conversation_id.toString()].participants[k]) == -1)
//                                                     connected_user.push(conv[files[j].conversation_id.toString()].participants[k])
//                                             }
//                                         }

//                                         var twu = JSON.parse(files[j].tag_list_with_user);
//                                         if(twu && Array.isArray(twu)){
//                                             for(let k=0; k<twu.length; k++){
//                                                 if(twu[k].tag_id === tags[i].tag_id.toString()){
//                                                     my_use_count[twu[k].created_by] = my_use_count[twu[k].created_by] ? my_use_count[twu[k].created_by] + 1 : 1;
//                                                 }
//                                             }
//                                         }
//                                     }
//                                 }else{
//                                     use_count = files.length;
//                                     var pt = '';
//                                     for(let j=0; j<files.length; j++){
//                                         var twu = JSON.parse(files[j].tag_list_with_user);
//                                         if(twu && Array.isArray(twu)){
//                                             for(let k=0; k<twu.length; k++){
//                                                 if(twu[k].tag_id === tags[i].tag_id.toString()){
//                                                     my_use_count[twu[k].created_by] = my_use_count[twu[k].created_by] ? my_use_count[twu[k].created_by] + 1 : 1;
//                                                     pt = twu[k].created_by;
//                                                 }
//                                             }
//                                         }
//                                         connected_user.push(pt);
//                                     }
//                                     for(let j=0; j<connected_user.length; j++){
//                                         user_use_count[connected_user[j]] = use_count;
//                                     }
//                                 }
//                             }
//                             // console.log( 2248, use_count, connected_user, my_use_count, user_use_count);
//                             if(connected_user.length>0){
//                                 models.instance.Tag.update({company_id: tags[i].company_id, tag_id: tags[i].tag_id}, {use_count, connected_user, user_use_count, my_use_count}, function(eee){
//                                     if(eee) console.log(2251, eee);
//                                     else console.log( "done === ", i, " of ", tags.length);
//                                 })
//                             }
//                         }
//                     });
//                 }
//             } 
//             res.send({msg: 'done convs = ' + convs.length + ' tags = ' + tags.length });                   
//         });
//     });
// });

// router.get('/tag_i_connected_update', function (req, res, next) {
//     console.log(2280, "tag_i_connected_update");
//     var c = 0;
//     // var n = 0;
//     var tag = {};
//     var conv = {};
//     var file = [];
//     models.instance.Conversation.find({}, { raw: true }, function (err1, convs) {
//         if (err1) console.log(2284, err1);
//         console.log("all conversations read done = " + convs.length);
//         for (let i = 0; i < convs.length; i++) {
//             conv[convs[i].conversation_id.toString()] = convs[i];
//         }
//         // models.instance.Tag.find({tag_id: models.timeuuidFromString('440b4170-9904-11ec-8f6d-7e9eb44a7fa8')},  {raw: true, allow_filtering: true}, async function(err2, tags) {
//         models.instance.Tag.find({}, { raw: true }, async function (err2, tags) {
//             if (err2) console.log(2143, err2);
//             console.log("total tag read done = " + tags.length);
//             for (let i = 0; i < tags.length; i++) {
//                 tag[tags[i].tag_id.toString()] = tags[i];
//             }

//             console.log({ msg: 'read done convs = ' + convs.length + ' tags = ' + tags.length });
//             var queries = [];
//             var connected_user = [];
//             var user_use_count = {};
//             var my_use_count = {};
//             var use_count = 0;
//             var pt_use_count = 0;
//             var conversation_ids = "";
//             var connected_user_ids = "";
//             var count = 0;
//             // all tags
//             for (let i = 0; i < tags.length; i++) {
//                 // jodi tag use kora hoy
//                 // if(tags[i].use_count && tags[i].use_count > 0 && Array.isArray(tags[i].connected_user)){
//                 queries = [];
//                 user_use_count = {};
//                 connected_user = [];
//                 my_use_count = {};
//                 use_count = 0;
//                 pt_use_count = 0;
//                 conversation_ids = "";
//                 connected_user_ids = "";

//                 var file = await new Promise((resolve) => {
//                     models.instance.File.find({ is_delete: 0, tag_list: { $contains: tags[i].tag_id.toString() } }, { raw: true, allow_filtering: true }, function (ef, f) {
//                         if (ef) { console.log(2316, 'file find error'); }
//                         resolve(f);
//                     });
//                 });
//                 console.log(2322, file.length);
//                 if (file.length > 0) {
//                     use_count = pt_use_count = file.length;
//                     for (let n = 0; n < file.length; n++) {
//                         if (file[n].conversation_id.toString() === file[n].user_id.toString() && !conv.hasOwnProperty(file[n].conversation_id.toString())) {
//                             conv[file[n].conversation_id.toString()] = {
//                                 conversation_id: file[n].conversation_id.toString(),
//                                 participants: [file[n].conversation_id.toString()]
//                             }
//                         }
//                         if (file[n].conversation_id !== null && conv.hasOwnProperty(file[n].conversation_id.toString()) && conv[file[n].conversation_id.toString()].participants !== undefined) {
//                             file[n].has_delete = file[n].has_delete === null ? [] : file[n].has_delete;

//                             // conversation_ids
//                             conversation_ids += "@" + file[n].conversation_id.toString() + "@";

//                             for (let m = 0; m < conv[file[n].conversation_id.toString()].participants.length; m++) {
//                                 if (tags[i].tag_type !== 'public' && file[n].user_id.toString() !== conv[file[n].conversation_id.toString()].participants[m]) continue;
//                                 if (tags[i].tag_type === 'public') {
//                                     var tc = new models.instance.Tagcount({
//                                         id: models.timeuuid(),
//                                         company_id: file[n].company_id.toString(),
//                                         conversation_id: file[n].conversation_id.toString(),
//                                         msg_id: file[n].msg_id.toString(),
//                                         tag_id: tags[i].tag_id.toString(),
//                                         user_id: conv[file[n].conversation_id.toString()].participants[m],
//                                         used_by: file[n].user_id.toString() === conv[file[n].conversation_id.toString()].participants[m] ? 'me' : 'others'
//                                     });
//                                 } else if (tags[i].tag_type === 'private') {
//                                     var tc = new models.instance.Tagcount({
//                                         id: models.timeuuid(),
//                                         company_id: file[n].company_id.toString(),
//                                         conversation_id: file[n].conversation_id.toString(),
//                                         msg_id: file[n].msg_id.toString(),
//                                         tag_id: tags[i].tag_id.toString(),
//                                         user_id: tags[i].tagged_by.toString(),
//                                         used_by: 'me'
//                                     });
//                                 }
//                                 queries.push(tc.save({ return_query: true }));
//                                 connected_user_ids += "@" + conv[file[n].conversation_id.toString()].participants[m] + "@";
//                             }
//                         }
//                     }
//                     queries.push(models.instance.Tag.update({ company_id: tags[i].company_id, tag_id: tags[i].tag_id }, { conversation_ids, connected_user_ids }, { return_query: true }));

//                     if (queries.length > 0) {
//                         await new Promise((resolve) => {
//                             models.doBatch(queries, function (err) {
//                                 if (err) console.log(2367, err);
//                                 else console.log(2368, 'Tag update successfully... ', i);
//                                 resolve();
//                             });
//                         })
//                     }
//                 }

//                 if (tags.length == i + 1) {
//                     console.log("******************* Done *************** convs = " + convs.length + " tags = " + tags.length)
//                 }
//             }
//             res.send({ msg: 'done convs = ' + convs.length + ' tags = ' + tags.length });
//         });
//     });
// });

// router.get('/link_bug_fix', function (req, res, next) {
//     console.log(2351, "link_bug_fix");
//     var q = "SELECT * FROM messages WHERE msg_type LIKE '%link%' AND url_body = '' ALLOW FILTERING ;";
//     console.log(2358, q);
//     models.instance.Messages.execute_query(q, {}, function (err, msgs) {
//         if (err) { console.log(2360, err); res.status(400).send(err); }
//         for (let v of msgs.rows) {
//             var t = v.msg_type.replace(' link', '');
//             models.instance.Messages.update({ conversation_id: v.conversation_id, msg_id: v.msg_id }, { msg_type: t }, function (e) {
//                 console.log(2364, e);
//             });
//         }
//         res.send({ msg: msgs.rows + ' done' });
//     });
// });

// function parseJSONSafely(str) {
//     try {
//         return JSON.parse(str);
//     }
//     catch (e) {
//         console.log("Error in JSON parse");
//         if (typeof str == 'string') return [str];
//         else return [];
//     }
// }

// router.get('/old_link_data_added', function (req, res, next) {
//     console.log(2367, "old_link_data_added");
//     // var q = "SELECT * FROM messages WHERE msg_type LIKE '%link%' ALLOW FILTERING ;";
//     var almsgs = [];
//     models.instance.Messages.stream({ msg_type: { $like: '%link%' } }, { raw: true, allow_filtering: true }, function (reader) {
//         var row;
//         while (row = reader.readRow()) {
//             if (row.msg_id && row.conversation_id && row.company_id && row.sender) {
//                 console.log(2385);
//                 almsgs.push(row);
//             }
//         }
//     }, async function (err) {
//         console.log(almsgs.length);
//         almsgs = _.orderBy(almsgs, ['created_at'], ['asc']);
//         var c = 0;
//         var q = [];

//         for (let i = 0; i < almsgs.length; i++) {
//             console.log("after = ", almsgs[i].created_at);
//             var urls = parseJSONSafely(almsgs[i].url_body);
//             if (urls) {
//                 c++;
//                 var title = [];
//                 if (almsgs[i].url_base_title) {
//                     title.push({ user_id: almsgs[i].sender.toString(), title: almsgs[i].url_base_title });
//                 }
//                 if (!_.isEmpty(almsgs[i].url_title)) {
//                     console.log(2407, almsgs[i].url_title)
//                     var urldata = parseJSONSafely(almsgs[i].url_title);
//                     for (let p; p < urldata.length; p++) {
//                         title.push({ user_id: urldata[p].user_id, title: urldata[p].title });
//                     }
//                 }

//                 for (let j = 0; j < urls.length; j++) {
//                     if (urls[j]) {
//                         var link_data = {
//                             url_id: models.timeuuid(),
//                             msg_id: almsgs[i].msg_id.toString(),
//                             conversation_id: almsgs[i].conversation_id.toString(),
//                             company_id: almsgs[i].company_id.toString(),
//                             user_id: almsgs[i].sender.toString(),
//                             title: title.length > 0 ? JSON.stringify(title) : null,
//                             url: urls[j].toString(),
//                             created_at: almsgs[i].created_at,
//                             has_hide: almsgs[i].has_hide,
//                             has_delete: almsgs[i].has_delete
//                         }
//                         // console.log(2412, link_data);
//                         var linkd = new models.instance.Link(link_data);
//                         q.push(linkd.save({ return_query: true }));
//                     }
//                 }
//                 if (q.length > 20) {
//                     await new Promise((resolve) => {
//                         models.doBatch(q, function (err) {
//                             q = [];
//                             if (err) { console.log(err); resolve(); }
//                             else {
//                                 console.log(c, q.length, 'Yuppiie!');
//                                 resolve();
//                             }
//                         });
//                     });
//                 }
//             }
//         }
//         if (q.length > 0) {
//             await new Promise((resolve) => {
//                 models.doBatch(q, function (err) {
//                     q = [];
//                     if (err) { console.log(err); resolve(); }
//                     else {
//                         console.log(c, q.length, 'Yuppiie!');
//                         resolve();
//                     }
//                 });
//             });
//         }
//         return res.send({ msg: almsgs.length + ' done' });
//     });
// });

// router.get('/delete_msg_update_into_file', function (req, res, next) {
//     console.log(2515, "delete_msg_update_into_file");
//     var queries = [];
//     var c = 0;
//     var p = 0;
//     models.instance.File.eachRow({ is_delete: 0 }, { fetchSize: 300, raw: true, allow_filtering: true }, function (n, row) {
//         models.instance.Messages.findOne({ msg_id: row.msg_id }, { raw: true, allow_filtering: true }, function (me, m) {
//             if (me) { console.log(2328, 'msg find error'); }
//             if (m) {
//                 if (m.has_delete !== undefined && m.has_delete !== null && Array.isArray(m.has_delete) && m.company_id) {
//                     queries.push(models.instance.File.update({ company_id: m.company_id.toString(), id: row.msg_id }, { has_delete: { $add: m.has_delete } }, { return_query: true }));
//                 }
//             }
//         });
//     }, async function (err, result) {
//         // called once the page has been retrieved.
//         if (err) { console.log(2520, 'file find error'); }
//         if (queries.length) {
//             await new Promise((resolve) => {
//                 models.doBatch(queries, function (e) {
//                     if (e) { console.log(e); resolve(); }
//                     else {
//                         c += queries.length;
//                         console.log(c, 'Yuppiie!');
//                         queries = [];
//                         resolve();
//                     }
//                 });
//             });
//         }
//         if (result.nextPage) {
//             console.log("file read done ", ++p * 300);
//             result.nextPage();
//         } else {
//             console.log("All done =========== ", c);
//             return res.send({ msg: c + ' done' });
//         }
//     });
// });


// router.get('/delete_all_task_msg', function (req, res, next) {
//     console.log(2574, "delete_all_task_msg");
//     var queries = [];
//     models.instance.Messages.find({ msg_type: 'task', company_id: "f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0" }, { raw: true, allow_filtering: true }, function (n, row) {
//         if (n) { console.log(2577, 'find error'); }
//         else {
//             for(let i=0; i<row.length; i++){
//                 queries.push(models.instance.Messages.delete({ conversation_id: row[i].conversation_id, msg_id: row[i].msg_id }, { return_query: true }));
//             }
//         }
//         if(queries.length>0){
//             models.doBatch(queries, function (e) {
//                 if (e) { console.log(e); }
//                 else {
//                     console.log(`Yuppiie! ${queries.length} task messages deleted successfully`);
//                 }
//             });
//         }
//         else
//             console.log(`No task messages found`);
//     });
// });


// router.get('/test_query', function (req, res, next) {
//     console.log(2598, "test_query");
//     var queries = [];
//     models.instance.Users.get_cql_client(function(err, client){
//         client.eachRow(`Select * from users WHERE company_id = f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0 ALLOW FILTERING;`, [], { autoPage : true }, function(n, row) {
//             console.log(2601, row.email);
//         }, function(err, result){
//             if (err) { console.log(2603, err); }
//             console.log(2604,result);    
//         });
//     });
// });


module.exports = router;