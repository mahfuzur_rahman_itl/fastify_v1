const fs = require('fs');
var mongoose = require('mongoose');

main().catch(err => console.log(3, err));

async function main() {
    // await mongoose.connect('mongodb://localhost:27017/freeli_v1');
    // console.log("MongoDB Connected Successfully...");
    
    // const Project = require('./mongo-models/project');
    // const Task = require('./mongo-models/task');
    
    // // Drop unwanted collection
    // await new Promise(resolve =>{
    //     Project.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Project collection');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     Task.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task collection');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     // disconnect from the database
    //     mongoose.disconnect();
    //     console.log('Database disconnect successfully...');
    //     resolve();
    // });

    // fs.stat('new_mongoose.js', async (err, stats) => {
    //     if (err) {
    //         if (err.code === 'ENOENT') {
    //             console.log('File does not exist');
    //             return;
    //         }
    //         throw err;
    //     }
    //     console.log('File exists');

    //     // Read the contents of mongoose.js and make a copy named old_mongoose.js
    //     await new Promise(r=>{
    //         fs.readFile('./config/db/mongoose.js', 'utf8', (err, data) => {
    //             if (err) {
    //                 console.error(err);
    //                 return;
    //             }
        
    //             // Write the contents of mongoose.js to old_mongoose.js
    //             fs.writeFile('./config/db/old_mongoose.js', data, 'utf8', (err) => {
    //                 if (err) {
    //                     console.error(err);
    //                     return;
    //                 }
    //                 console.log('mongoose.js successfully copied to old_mongoose.js');
    //             });
    //         });
    //         r();
    //     });
        
    //     // then replace the contents of mongoose.js from new_mongoose.js
    //     await new Promise(r=>{
    //         fs.readFile('new_mongoose.js', 'utf8', (err, data) => {
    //             if (err) {
    //                 console.error(err);
    //                 return;
    //             }
        
    //             // Write the contents of new_mongoose.js to mongoose.js
    //             fs.writeFile('./config/db/mongoose.js', data, 'utf8', (err) => {
    //                 if (err) {
    //                     console.error(err);
    //                     return;
    //                 }
    //                 console.log('new_mongoose.js successfully copied to mongoose.js');
    //             });
    //         });
    //         r();
    //     });
        
    //     await new Promise(r=>{
    //         fs.unlink('new_mongoose.js', (err) => {
    //             if (err) {
    //             console.error(err);
    //             return;
    //             }
    //             console.log('new_mongoose.js successfully deleted');
    //         });
    //         r();
    //     });
    // });
}
