var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const task_custom = new schema({
    label: { type: String, default: ""},
    value: { type: String, default: ""},
    custom_type: { type: String, default: "Status" },
    company_id: {type: String, default: ""},
    // task_ids:  {type: Array, default: [] },
    // project_id: { type: String, default: "" },
    created_by: { type: String, default: "" },
    // created_at: { type: Date, default: Date.now },
    // updated_at: {type: Date, default: Date.now},
}, { strict: 'throw' });

module.exports = mongoose.model('task_custom', task_custom);