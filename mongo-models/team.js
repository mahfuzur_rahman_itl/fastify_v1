var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var team = new schema({
    team_id: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    created_by: {type: String, default: uuidv4() },
    updated_by: {type: String, default: uuidv4() },
    company_id: {type: String, default: uuidv4() },
    team_title: { type: String, default: "NOT NULL"},
    //company_id: {type: String, default: uuidv4(),index: {unique: true, dropDups: true} },
    //team_title: { type: String, default: "NOT NULL",index: {unique: true, dropDups: true}},
    participants: { type: Array, default: [] },
    admin: { type: Array, default: [] },
   // team.index({ name: 1, user: 1 }, { unique: true })
});
team.index({ team_title: 1, company_id: 1}, { unique: true });
module.exports = mongoose.model('team', team);

