var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var conversation = new schema({
    conversation_id: { type: String, unique : true, default: uuidv4() },
    created_by: { type: String, default: uuidv4() },
    participants: { type: Array, default: [] },
    participants_admin: { type: Array, default: [] },
    participants_guest: { type: Array, default: [] },
    title: { type: String, default: "" },
    group: { type: String, default: "no" },
    group_keyspace: { type: String, default: uuidv4() },
    team_id: { type: String, default: uuidv4() },
    privacy: { type: String, default: "public" },
    archive: { type: String, default: "no" },
    is_active: { type: Array, default: [] },
    status: { type: String, default: "active" },
    close_for: { type: Array, default: [] },
    conv_img: { type: String, default: "" },
    is_pinned_users: { type: Array, default: [] },
    topic_type: { type: String, default: "" },
    b_unit_id: { type: String, default: "" },
    room_id: { type: String, default: "" },
    created_at: { type: Date, default: Date.now },
    is_busy: { type: Number, default: 0 },
    company_id: { type: String, default: uuidv4() },
    last_msg: { type: String, default: "New messages" },
    last_msg_time: { type: Date, default: Date.now },
    sender_id: { type: String, default: uuidv4() },
    tag_list: { type: Array, default: [] },
    msg_status: { type: String, default: "" },
    conference_id: { type: String, default: "" },
    root_conv_id: { type: String, default: "" },
    reset_id: { type: String, default: "" },
    system_conversation: { type: String, default: 'No' },
    system_conversation_active: { type: String, default: 'No' },
    system_conversatio_is_active: { type: Array, default: [] },
    system_conversatio_send_sms: { type: Array, default: [] },
    pin: { type: Array, default: [] },
    has_mute: { type: Array, default: [] },
    mute: { type: Array, default: [] },
    short_id: { type: String, default: "" },

});
module.exports = mongoose.model('conversation', conversation);

