var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var calendar = new schema({
    // id: {type: String,default: uuidv4()},
    title: {type: String, default: ""},
    start_date: {type: Date},
    end_date: {type: Date},
    description: {type: String, default: ""},
    created_by: {type: String, default: ""},
    company_id: {type: String, default: ""},
    created_at: {type: Date, default: Date.now},
    
    updated_at: {type: Date},
    updated_by: {type: String, default: ""},
    
});
module.exports = mongoose.model('calendar', calendar);

