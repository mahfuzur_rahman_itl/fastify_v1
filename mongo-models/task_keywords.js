var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const task_keywords = new schema({
    keywords_title: { type: String, default: "New Key"},
    description: { type: String, default: ""},
    task_ids: { type: Array, default: []},
    created_by: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    last_updated_at: {type: Date, default: Date.now},
    company_id: { type: String, default: uuidv4() },
});

module.exports = mongoose.model('task_keywords', task_keywords);