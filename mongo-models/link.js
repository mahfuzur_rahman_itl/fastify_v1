var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var link = new schema({
    url_id: {
        type: String,
        default: uuidv4()
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    msg_id: { type: String, default: "" },
    conversation_id: { type: String, default: "" },
    company_id: { type: String, default: "" },
    user_id: { type: String, default: "" },
    url: { type: String, default: "" },
    title: { type: String, default: "" },
    has_hide: { type:Array, default: []},
    has_delete: { type:Array, default: []},
    root_conv_id: { type: String, default: "" },
    is_delete: { type: String, default: "0" },
    secret_user: { type:Array, default: []},
    other_user: { type:Array, default: []},
    participants: { type:Array, default: []},
});
module.exports = mongoose.model('link', link);