var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var company = new schema({
    company_id: {
        type: String,
        default: uuidv4()
    },
    company_name: {
        type: String,
        default: ""
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    created_by: { type: String, default: "" },
    updated_by: { type: String, default: "" },
    class: { type: Array, default: [] },
    campus: { type: Array, default: [] },
    section: { type: Array, default: [] },
    company_img: { type: String, default: "" },
    industry: { type: String, default: "" },
    domain_name: { type: String, default: "" },
    plan_name: { type: String, default: "" },
    plan_user_limit: { type: String, default: "" },
    plan_storage_limit: { type: String, default: "" },
    is_deactivate: { type: Number, default: 0 },
    plan_access: {type: Array, default: []},
    plan_id: { type: String, default: "" },
    subscription_id: { type: String, default: "" },
    product_id: { type: String, default: "" },
    price_id: { type: String, default: "" }
});
module.exports = mongoose.model('company', company);

