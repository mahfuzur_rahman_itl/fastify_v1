var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const checklist_item = new schema({
    task_id: { type: mongoose.Schema.Types.ObjectId, ref: 'task' },
    checklist_id: { type: mongoose.Schema.Types.ObjectId, ref: 'task_checklist' },
    item_title: { type: String, default: "" },
    description: { type: String, default: "" },
    start_date: { type: Date, default: Date.now },
    end_date: { type: Date, default: Date.now },
    created_by: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    last_updated_at: {type: Date, default: Date.now},
    status: { type: String, default: "incomplete" },
    company_id: { type: String, default: uuidv4() },
}, { strict: 'throw' });
module.exports = mongoose.model('checklist_item', checklist_item);