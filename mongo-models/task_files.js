var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var task_files = new schema({
    id: { type: String, default: uuidv4() },
    conversation_id: { type: String, default: uuidv4() },
    user_id: { type: String, default: uuidv4() },
    msg_id: { type: String, default: uuidv4() },
    acl: { type: String, default: "public-read" },
    bucket: { type: String, default: "no bucket" },
    file_type: { type: String, default: "not found" },
    key: { type: String, default: "not found" },
    location: { type: String, default: "not found" },
    originalname: { type: String, default: "not found" },
    file_size: { type: String, default: "not found" },
    is_delete: { type: Number, default: 0 },
    has_delete: { type: Array, default: [] },
    has_tag: { type: String, default: "" },
    tag_list: { type: Array, default: [] },
    tag_list_with_user: { type: Array, default: [] },
    mention_user: { type: Array, default: [] },
    root_conv_id: { type: String, default: "" },
    created_at: { type: Date, default: Date.now },
    secret_user: { type: Array, default: [] },
    is_secret: { type: Boolean, default: false },
    url_short_id: { type: String, default: "" },
    file_category: { type: String, default: "" },
    main_msg_id: { type: String, default: "" },
    participants: { type: Array, default: [] },
    star: { type: Array, default: [] },
    company_id: { type: String, default: uuidv4() },
    task_id: { type: mongoose.Schema.Types.ObjectId, ref: 'task' }

});
module.exports = mongoose.model('task_files', task_files);

