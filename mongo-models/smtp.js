var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var smtp = new schema({
    id: {
        type: String,
        default: uuidv4(),
    },
    company_id: { type: String, default: "" },
    user_id: { type: String, default: "" },
    from_email: { type: String, default: "" },
    from_name: { type: String, default: "" },
    smtp_host: { type: String, default: "" },
    smtp_port: { type: String, default: "" },
    smtp_auth: { type: String, default: "" },
    smtp_user: { type: String, default: "" },
    smtp_pass: { type: String, default: "" },
    created_at: {
        type: Date,
        default: Date.now
    },
});
module.exports = mongoose.model('smtp', smtp);

