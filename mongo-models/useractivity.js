var mongoose = require('mongoose');
var schema = mongoose.Schema;

var useractivity = new schema({
    created_at: { type: Date, default: Date.now },
    type: {type: String, default: ""},
    title: {type: String, default: ""},
    body: {type: String, default: ""},
    created_by_id: {type: String, default: ""},
    created_by_name: {type: String, default: ""},
    created_by_img: {type: String, default: ""},
    company_id: {type: String, default: ""},

});
module.exports = mongoose.model('useractivity', useractivity);

