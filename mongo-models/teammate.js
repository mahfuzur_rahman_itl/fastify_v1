var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var teammate = new schema({
    id: {
        type: String,
        default: uuidv4()
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    created_by_id: { type: String, default: "" },
    created_by_name: { type: String, default: "" },
    company_id: { type: String, default: "" },
    company_name: { type: String, default: "" },
    invite_email: { type: String, default: "" },
    invite_name: { type: String, default: "" },
    invite_role: { type: String, default: "" },
    invite_team: { type:Array, default: null },
    invite_room: { type: Array, default: null }
});
module.exports = mongoose.model('teammate', teammate);

