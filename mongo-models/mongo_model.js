require('./user');
require('./file');
require('./conversation');
require('./message');

require('./notification');
require('./tag');
require('./tagcount');
require('./team'); 
require('./useractivity');
require('./version');

require('./address');
require('./business_unit');
require('./industry');
require('./link');
require('./message_checklist');
require('./message_emoji');
require('./payment');
require('./role');
require('./teammate');
require('./company');
require('./project');
require('./task');
require('./task_keywords');
require('./task_checklist');
require('./checklist_item');
require('./task_review');
require('./task_discussion');
require('./task_files');
require('./task_custom');
require('./calendar');

async function db_drop() {   
    // await new Promise(resolve =>{
    //     const Tagcount = require('./tagcount');
    //     const Conversation = require('./conversation');
    //     const File = require('./file');
    //     Tagcount.deleteMany({}, async function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Tagcount');
    //             let convs = await Conversation.find({}).lean();
    //             let convsobj = {};
    //             for(let i=0; i<convs.length; i++){
    //                 convsobj[convs[i].conversation_id.toString()] = convs[i].participants;
    //             }
    //             console.log('All Conversation load');
    //             let files = await File.find({}).lean();
    //             console.log('All File load');
    //             let c = 1;
    //             for(let i=0; i<files.length; i++){
    //                 if(files[i].is_delete === 0){
    //                     for(let j=0; j<files[i].tag_list.length; j++){
    //                         let members = files[i].is_secret === true && files[i].secret_user.length > 0 ? files[i].secret_user : convsobj[files[i].conversation_id.toString()];
    //                         if(files[i].has_delete.length > 0)
    //                             members = members.filter((e)=>{return files[i].has_delete.indexOf(e) === -1});
    //                         for(let k=0; k<members.length; k++){
    //                             let tc = new Tagcount({
    //                                 company_id: files[i].company_id.toString(),
    //                                 conversation_id: files[i].conversation_id.toString(),
    //                                 msg_id: files[i].msg_id.toString(),
    //                                 tag_id: files[i].tag_list[j].toString(),
    //                                 user_id: members[k].toString(),
    //                                 file_id: files[i].id.toString(),
    //                                 used_by: members[k].toString() === files[i].user_id.toString() ? 'me' : 'others'
    //                             });
    //                             tc.save((err)=>{
    //                                 if(err) console.log(134, err);
    //                                 else console.log(c++);
    //                             });
    //                         }
    //                     }
    //                 }
    //             }
    //             resolve();
    //         }
    //     });
    // }); 
    // await new Promise(resolve =>{
    //     const BU = require('./business_unit');
    //     BU.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from business_unit collection');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Project = require('./project');
    //     Project.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Project collection');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Task = require('./task');
    //     Task.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task collection');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Task_checklist = require('./task_checklist');
    //     Task_checklist.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task checklist');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Task_discussion = require('./task_discussion');
    //     Task_discussion.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task discussion');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Task_review = require('./task_review');
    //     Task_review.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task review');
    //             resolve();
    //         }
    //     });
    // });
    // await new Promise(resolve =>{
    //     const Task_keywords = require('./task_keywords');
    //     Task_keywords.deleteMany({}, function(err) {
    //         if (err) {
    //             console.log(err);
    //         } else {
    //             console.log('All data removed from Task keywords');
    //             resolve();
    //         }
    //     });
    // });
}
db_drop();