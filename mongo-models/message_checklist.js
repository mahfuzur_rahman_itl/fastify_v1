var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var message_checklist = new schema({
    checklist_id: {
        type: String,
        default: uuidv4()
    },
    msg_id: {
        type: String
    },
    msg_title: {
        type:String,
        default: ""
    },
    convid: {
        type:String,
        default: ""
    },
    original_ttl: {
        type:String,
        default: ""
    },
    assignedby: {
        type:String,
        default: ""
    },
    assignby_role: {
        type:String,
        default: ""
    },
    alternative_assign_to: {
        type:String,
        default: ""
    },
    assignee_change_reason: {
        type:String,
        default: ""
    },
    Completed_status_updated_by: {
        type:String,
        default: ""
    },
    Request_ttl_by: {
        type:String,
        default: ""
    },
    request_ttl_message: {
        type:String,
        default: ""
    },
    request_ttl_approved_by: {
        type:String,
        default: ""
    },
    request_ttl_time: {
        type:String,
        default: ""
    },
    Is_direct_group: {
        type:String,
        default: ""
    },
    participant_id: {
        type: Array, 
        default: null
    },
    request_repetition: {
        type: Number,
        default: null
    },
    from_user_counter: {
        type: Number,
        default: null
    },
    to_user_counter: {
        type: Number,
        default: null
    },
    request_ttl_approved_date: {
        type: Date,
        default: null
    },
    request_ttl_date: {
        type: Date,
        default: null
    },
    created_by: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    checklist_title: {
        type: String
    },
    last_updated_by: {
        type:String,
        default: ""
    },
    last_updated_at: {
        type: Date,
        default: Date.now
    },
    last_action: {
        type:String,
        default: ""
    },
    last_edited_by: {
        type:String,
        default: ""
    },
    start_due_date: {
        type:String,
        default: ""
    },
    end_due_date: {
        type:String,
        default: ""
    },
    assign_to: {
        type:String,
        default: ""
    },
    privacy: {
        type:String,
        default: ""
    },
    assign_status: {
        type:String,
        default: ""
    },
    assign_decline_note: {
        type:String,
        default: ""
    },
    last_edited_at: {
        type: Date,
        default: null
    },
    checklist_status: {
        type: Number,
        default: 0
    },
    root_conv_id: { type: String, default: null },
    review_status: {
        type: Number,
        default: 0
    },

});
module.exports = mongoose.model('message_checklist', message_checklist);