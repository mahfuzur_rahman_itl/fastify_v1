var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var address = new schema({
    id: {
        type: String,
        default: uuidv4()
    },
    company_id: {type: String, default: ""},
    first_name: {type: String, default: ""},
    last_name: {type: String, default: ""},
    email: {type: String, default: ""},
    street_address: {type: String, default: ""},
    apt_number: {type: String, default: ""},
    province: {type: String, default: ""},
    zip: {type: String, default: ""},
    created_at: {
        type: Date,
        default: Date.now
    },
    created_by: {type: String, default: ""},
    updated_at: { type: Date },
    updated_by: {type: String, default: ""},
    country: {type: String, default: ""},
    company: {type: String, default: ""},
    city: {type: String, default: ""}
});
module.exports = mongoose.model('address', address);

