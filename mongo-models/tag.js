var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var tag = new schema({
    tag_id: { type: String, unique : true, default: uuidv4() },
    tagged_by: {type: String, default: uuidv4() },
    title: { type: String, default: "" },
    company_id: {type: String, default: uuidv4(), index: true },
    type: { type: String, default: "" },
    tag_type: { type: String, default: "" },
    tag_color: { type: String, default: "" },
    team_list: { type: Array, default: [] },
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    updated_by: {type: String, default: uuidv4() },
    use_count: {type: Number, default: 0 },
    connected_user: { type: Array, default: [] },
    user_use_count: { type: Map, of: Number },
    my_use_count: { type: Map, of: Number },
    conversation_ids: { type: String, default: "" },
    connected_user_ids: { type: String, default: "" },
    favourite: { type: Array, default: [] },
});
module.exports = mongoose.model('tag', tag);

