var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const task_checklist = new schema({
    checklist_title: { type: String, default: "New Checklist"},
    checklist_status: { type: String, default: "" },
    task_id: { type: mongoose.Schema.Types.ObjectId, ref: 'task' },
    created_by: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    last_updated_at: {type: Date, default: Date.now},
    company_id: { type: String, default: uuidv4() },
}, { strict: 'throw' });
module.exports = mongoose.model('task_checklist', task_checklist);