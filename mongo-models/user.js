var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var user = new schema({
    id: {
        type: String,
        default: uuidv4(),
    },
    createdat: {
        type: Date,
        default:Date.now
    },
    email: {
        type: String,
        default: ""
    },
    firstname: {
        type: String,
        default: ""
    },
    lastname: {
        type: String,
        default: ""
    },
    dept: { type: String, default: "" },
    designation: { type: String, default: "" },
    account_type: { type: String, default: "" },
    phone: { type: Array, default: [] },
    device: { type: Array, default: [] },
    lat: { type: String, default: "" },
    log: { type: String, default: "" },
    gcm_id: { type: String, default: "" },
    fcm_id: { type: Array, default: [] },
    img: { type: String, default: "img.png" },
    is_active: { type: Number, default: 1 },
    is_delete: { type: Number, default: 0 },
    is_busy: { type: Number, default: 0 },
    login_total: { type: Number, default: 0 },
    last_login: {
        type: Date,
        default:Date.now
    },
    password: { type: String, default: "" },
    role: { type: String, default: "" },
    access: { type: Array, default: [] },
    company_id: {
        type: String,
        default: uuidv4()
    },
    reset_id: { type: String, default: "" },
    conference_id: { type: String, default: "" },
    created_by: { type: String, default: "" },
    updated_by: { type: String, default: "" },
    updated_at: { type: String, default: "" },
    class: {
        type: String,
        default: ""
    },
    section: { type: String, default: "" },
    campus: { type: String, default: "" },
    company: { type: String, default: "" },
    parent_id: { type: String, default: "" },
    student_id: { type: String, default: "" },
    student_list: { type: Array, default: [] },
    parent_list: { type: Array, default: [] },
    relationship: { type: String, default: "" },
    login_id: { type: String, default: "" },
    relationship_map: { type: Array,default:[] },
    roll_number: { type: String, default: "" },
    //  company_img: { type: String, default: null },
    birth_day: { type: Date,default:'' },
    mobile_otp: { type: String, default: "" },
    email_otp: { type: String, default: "" },
    verified: { type: String, default: "" },
    login_attempt: { type: Number, default: 0 },
    last_login_address: { type: String, default: "" },
    user_friend_list: { type: Array, default: [] },
    connected_device: { type: Array, default: [] },
    blocked_list: { type: Array, default: [] },
    gender: { type: String, default: "" },
    blood_group: { type: String, default: "" },
    marital_status: { type: String, default: "" },
    nickname: { type: String, default: "" },
    present_address: { type: String, default: "" },
    permanent_address: { type: String, default: "" },
    employee_id: { type: String, default: "" },
    nid: { type: String, default: "" },
    intro: { type: String, default: "" },
    father_name: { type: String, default: "" },
    mother_name: { type: String, default: "" },
    interests: { type: Array, default: [] },
    do_not_disturb: { type: String, default: "" },
    screen_time_today: { type: String, default: "" },
    face_detection: { type: String, default: "" },
    finger_detection: { type: String, default: "" },
    vacation_mode: { type: String, default: "" },
    in_time_today: { type: String, default: "" },
    out_time_today: { type: String, default: "" },
    company_list: { type: Array, default:[] },
    phone_optional: { type: String, default: null },
    school: { type: String, default: null },
    student_link: { type: Array, default:[] },
    short_id: { type: String, default: null },
    mute_all: { type: Boolean, default: false },
    mute: { type: String, default: "" },
    short_id_guest: {type: String, default: "" },
    customer_id: {type: String, default: "" },
    google_id: {type: String, default: "" },
    googleTokens: {type: Object, default: "" },
    outlook_id: {type: String, default: "" },
    outlookTokens: {type: Object, default: "" },
    outlookAccount: {type: Object, default: "" },
}
);
module.exports = mongoose.model('user', user);

