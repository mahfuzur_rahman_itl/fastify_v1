var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var notification = new schema({
    type: { type: String, default: "" },
    title:{ type: String, default: "" },
    body: { type: String, default: "" },
    created_at: { type: Date, default: Date.now },
    created_by_id: { type: String, default: "" },
    created_by_name: { type: String, default: "" },
    created_by_img: { type: String, default: "" },
    receiver_id: { type: String, default: "" },
    read_status: { type: String, default: "no" },
    company_id: { type: String, default: uuidv4() },
    task_id: { type: mongoose.Schema.Types.ObjectId, ref: 'task' },
    tab: { type: String, default: 'property'}

});
module.exports = mongoose.model('notification', notification);

