var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const task_review = new schema({
    review_title: { type: String, default: ""},
    review_body: { type: String, default: "" },
    review_status: { type: String, default: "pending" },
    rate: { type: Number, default: 0 },
    task_id: { type: String, default: "" },
    project_id: { type: String, default: "" },
    created_by: { type: String, default: uuidv4() },
    created_for: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    last_updated_at: {type: Date, default: Date.now},
    last_reviewed_by: {type: String, default: ""},
    last_commented_by: {type: String, default: ""},
    last_comment: {type: String, default: ""},
    history: {type: Array, default: []},
    company_id: { type: String, default: uuidv4() },
    user_rating: { type: Map }
}, { strict: 'throw' });

module.exports = mongoose.model('task_review', task_review);