var mongoose = require('mongoose');
var schema = mongoose.Schema;

var tagcount = new schema({
    company_id: { type: String, default: "" },
    conversation_id: {type: String, default: "" },
    msg_id: {type: String, default: "" },
    tag_id: {type: String, default: "" },
    user_id: { type: String, default: "" },
    file_id: { type: String, default: "" },
    used_by: { type: String, default: "" },

});
module.exports = mongoose.model('tagcount', tagcount);

