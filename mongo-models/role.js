var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var role = new schema({
    role_id: {
        type: String,
        default: uuidv4()
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    created_by: { type: String, default: "" },
    role_title: { type: String, default: "" },
    company_id: { type: String, default: "" },
    role_access: { type: Array, default: [] }
});
module.exports = mongoose.model('role', role);

