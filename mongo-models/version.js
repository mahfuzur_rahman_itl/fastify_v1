var mongoose = require('mongoose');
var schema = mongoose.Schema;

var version = new schema({
    created_at: { type: Date, default: Date.now },
    major: {type: Number, default: 0},
    minor: {type: Number, default: 0},
    build: {type: Number, default: 0},
    version: {type: String, default: ""},
    version_details: {type: String, default: "Not Specified"},

});
module.exports = mongoose.model('version', version);

