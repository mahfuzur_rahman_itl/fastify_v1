var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var set_tune = new schema({
    id: {
        type: String,
        default: uuidv4()
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    user_id: { type: String, default: "" },
    conversation_id: { type: String, default: "" },
    type: { type: String, default: "" },
    tune_title: { type: String, default: "" },
    company_id: { type: String, default: "" },
    status: { type: Number, default: 1 },

});
module.exports = mongoose.model('set_tune', set_tune);

