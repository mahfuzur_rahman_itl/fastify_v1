var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var payment = new schema({
    id: {
        type: String,
        default: uuidv4()
    },
    company_id: { type: String, default: "" },
    card_no: { type: String, default: "" },
    card_holder_name: { type: String, default: "" },
    transection_id: { type: String, default: "" },
    payment_by: { type: String, default: "" },
    amount: { type: String, default: "" },
    transection_date: {
        type: Date,
        default: Date.now
    },
    created_by: { type: String, default: "" },
    plan_name: { type: String, default: "" },
    plan_user_limit: { type: String, default: "" },
    plan_storage_limit: { type: String, default: "" },
    customer_id: { type: String, default: "" },
    additional_storage: { type: String, default: "" },
    

}

);
module.exports = mongoose.model('payment', payment);

