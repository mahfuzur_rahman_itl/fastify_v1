var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var message_emoji = new schema({
    msg_id: { type: String, default: "" },
    user_id: { type: String, default: "" },
    company_id: { type: String, default: "" },
    user_fullname: { type: String, default: "" },
    emoji_name: { type: String, default: "" },
    created_at: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('message_emoji', message_emoji);