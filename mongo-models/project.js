var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
const project = new schema({
    project_title: { type: String, default: "New Project" },
    created_by: { type: String, default: uuidv4() },
    created_at: { type: Date, default: Date.now },
    start_date: { type: Date, default: Date.now },
    end_date: { type: Date, default: Date.now },
    forecasted_cost: { type: Number, default: 0},
    actual_cost: { type: Number, default: 0},
    cost_variance: { type: Number, default: 0},
    forecasted_hours: { type: Number, default: 0},
    actual_hours: { type: Number, default: 0 },
    hours_variance: { type: Number, default: 0 },
    description: { type: String, default: "" },
    status: { type: String, default: "inprogress" },
    projecct_img: { type: String, default: "" },
    company_id: { type: String, default: uuidv4() },
    manager_id: { type: String, default: uuidv4() },
    participants: { type: Array, default: [] },
    task_ids: { type: Array, default: [] },
    is_archive: { type: Boolean, default: false},
    pin: { type: Array, default: [] },
});

module.exports = mongoose.model('project', project);