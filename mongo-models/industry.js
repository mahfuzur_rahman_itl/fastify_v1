var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var industry = new schema({
    industry_id: {
        type: String,
        default: uuidv4()
    },
    industry_name: { type: String, default: null },
    company_id: {type: String, default: ""},
    created_at: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('industry', industry);

