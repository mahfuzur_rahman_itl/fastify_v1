var mongoose = require('mongoose');
var schema = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');
var business_unit = new schema({
    company_id: {type: String, default: ""},
    unit_id: {
        type: String,
        default: uuidv4()
    },
    user_id: {type: String, default: ""},
    unit_name: {type: String, default: ""},
    industry_name: {type: String, default: ""},
    industry_id: {type: String, default: ""},
    created_by: {type: String, default: ""},
    updated_at: {
        type: Date,
        default: Date.now
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('business_unit', business_unit);

