var Conversation = require('mongoose').model('conversation');
var Users = require('mongoose').model('user');
const isUuid = require('uuid-validate');
const _ = require('lodash');
const { get_user_obj, fnln } = require('./message');

var { sg_email } = require('./sg_email');
var { save_activity } = require('./user_activity');

function has_conversation(data) {
    return new Promise(async (resolve, reject) => {
        // console.log(12, data.participants)
        let conversation = await Conversation.findOne({ participants: { $all: data.participants }, group: 'no', company_id: data.company_id }).lean();
        // console.log(14, conversation.participants)
        if (conversation && conversation.hasOwnProperty('conversation_id'))
            resolve({ status: true, conversation });
        else
            resolve({ status: false });
        // var fid = data.participants[data.participants.indexOf(data.created_by) == 0 ? 1 : 0];
        // Conversation.find({ participants: { $in: data.created_by }, single: 'yes' }, function(error, conversations){
        //     if(error) reject({status: false, error: 'Find error ib has_conversation', message: error });
        //     // console.log(8, conversations.length);
        //     if(conversations.length > 0){
        //         conversations.forEach(function(v, k){
        //             if(v.participants.indexOf(fid) > -1){
        //                 console.log(8, v.single, v.participants.length, v.conversation_id.toString());
        //                 resolve({status: true, conversation: v });
        //             }
        //         });
        //     }

        //     resolve({status: false });
        // });
    });
}

function has_this_title_conversation(str) {
    return new Promise(async (resolve, reject) => {
        try {
            let conversation = await Conversation.findOne({ title: str });
            resolve({ status: true, conversation });
        } catch (error) {
            reject({ status: false, error: 'Find error in has_this_title_conversation', message: error });
        }
    });
}

function get_conversation(arg) {
    // console.log(36, arg);
    // arg[conversation_id, user_id, company_id]
    return new Promise(async (resolve) => {
        var query = {};
        if (isUuid(arg.conversation_id))
            query.conversation_id = arg.conversation_id;
        else if (isUuid(arg.user_id)) {
            query = {
                $or: [{ participants: arg.user_id }, { temp_user: arg.user_id }],
                $and: [{ status: 'active' }]
            };
        }
        // var convs = await Conversation.find(query).exec();
        var convs = await Conversation.aggregate([
            { $match: query },
            // {$lookup:{ 
            //     from: 'users', 
            //     localField: 'participants', 
            //     foreignField:'id',
            //     as:'participants_details' 
            // }},
            {$lookup:{ 
                from: 'business_units', 
                localField: 'b_unit_id', 
                foreignField:'unit_id',
                as:'b_unit_id_info' 
            }},
            {$lookup:{ 
                from: 'teams', 
                localField: 'team_id', 
                foreignField:'team_id',
                as:'team_id_info' 
            }},
            {
                "$project": {
                "_id": 1,
                "id": 1,
                "conversation_id": 1,
                "created_by": 1,
                "participants": 1,
                "participants_admin": 1,
                "participants_guest": 1,
                "group": 1,
                "team_id": 1,
                "team_id_name": 1,
                "privacy": 1,
                "archive": 1,
                "is_active": 1,
                "status": 1,
                "close_for": 1,
                "conv_img": 1,
                "is_pinned_users": 1,
                "b_unit_id": 1,
                "b_unit_id_name": {
                    $map: {
                        input: '$b_unit_id_info',
                        as: 'b_unit_id_info',
                        in: '$$b_unit_id_info.unit_name'
                    }
                },
                "room_id": 1,
                "created_at": 1,
                "is_busy": 1,
                "company_id": 1,
                "last_msg": 1,
                "last_msg_time": 1,
                "sender_id": 1,
                "tag_list": 1,
                "msg_status": 1,
                "conference_id": 1,
                "root_conv_id": 1,
                "reset_id": 1,
                "pin": 1,
                "has_mute": 1,
                "mute": 1,
                "short_id": 1,
                "title": 1,
                // "participants_details.id": 1,
                // "participants_details.email": 1,
                // "participants_details.firstname": 1,
                // "participants_details.lastname": 1,
                // "participants_details.is_active": 1,
                // "participants_details.img": 1,
                // "participants_details.role": 1,
                // "participants_details.createdat": 1,
                }
            }
        ]).exec();
        if (convs.length > 0) {
            // let allusers = await Users.find({company_id: convs[0].company_id}).select('id email firstname lastname is_active img role createdat').exec();

            for (let key in convs) {
                let conv = convs[key];
                conv.friend_id = "";
                conv.conv_is_active = 1;
                conv.close_for = conv.close_for === null ? 'no' : 'yes';
                conv.temp_user = conv.temp_user === null ? [] : conv.temp_user;
                conv.participants_details = [];
                for (let i = 0; i < conv.participants.length; i++) {
                    let this_user = await get_user_obj(conv.participants[i]);
                    if (this_user.firstname !== '' && this_user.firstname !== undefined) {
                        conv.participants_details.push({
                            id: this_user.id,
                            email: this_user.email,
                            firstname: this_user.firstname,
                            lastname: this_user.lastname,
                            is_active: this_user.is_active,
                            img: this_user.img,
                            role: this_user.role,
                            fnln: this_user.fnln,
                            createdat: this_user.createdat
                        });
                    }
                }
                // Direct conversation
                if (conv.group === 'no') {
                    if (conv.conversation_id === conv.participants.join())
                        var fid = conv.conversation_id;
                    else
                        var fid = conv.participants.join().replace(arg.user_id, '').replace(',', '');
                    let this_user = await get_user_obj(fid);
                    if (this_user.firstname !== '' && this_user.firstname !== undefined) {
                        conv.title = this_user.fullname;
                        conv.conv_img = this_user.img;
                        conv.friend_id = fid;
                        conv.fnln = fnln(this_user);
                        conv.conv_is_active = this_user.is_active;
                    }
                    else continue;
                }
                // Group conversation
                else {
                    conv.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + conv.conv_img;
                    // for(let i=0; i<conv.participants_details.length; i++){
                    //     conv.participants_details[i].fnln = fnln(conv.participants_details[i]);
                    //     conv.participants_details[i].img = process.env.FILE_SERVER + 'profile-pic/Photos/' + conv.participants_details[i].img;
                    // }
                }
            }
        }

        // console.log(122, convs.length);
        resolve(convs);
    });
}

async function check_send_invite_email(req) {
    // console.log(167, req.headers.origin)
    req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
    req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
    // console.log(169, req.headers.origin, req.body)
    for (let i = 0; i < req.body.participants.length; i++) {
        var this_user = await get_user_obj(req.body.participants[i]);
        // console.log(165, this_user.role)
        if (this_user.role === 'Guest') {
            var title = req.body.title ? req.body.title + '</b> room of <b>' : '';
            var convid = req.body.convid ? '/' + req.body.convid : '';
            var passtxt = '<br><b>One-Time Password:</b> ' + this_user.email_otp;
            var subject = ' invited you to join Workfreeli.';
            var html = 'Hi ' + this_user.firstname + '!<br><br>' + req.body.name + ' has invited you as a guest to the <b>' + title + req.body.company_name + '</b> company in Workfreeli.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + this_user.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + this_user.id.toString() + convid + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';

            if (this_user.role === 'Guest') {
                subject = ' invited you to a Workfreeli room.';
            }
            // console.log(176, html);
            var emaildata = {
                to: this_user.email,
                subject: req.body.name + subject,
                text: req.body.name + subject,
                html: html
            }

            try {
                sg_email.send(emaildata, (result) => {
                    if (result.msg == 'success') {
                        save_activity({
                            company_id: this_user.company_id,
                            user_id: this_user.id,
                            title: this_user.firstname + " email send successfully.",
                            type: "success",
                            body: req.body.name + ' added a new user named ' + this_user.firstname + " as guest and email send successfully at " + new Date(),
                            user_name: req.body.name,
                            user_img: 'img.png',
                        });
                    } else {
                        console.log('email not send');
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }
    }
}

module.exports = { has_conversation, has_this_title_conversation, get_conversation, check_send_invite_email };