const isUuid = require('uuid-validate');
const bcrypt = require('bcryptjs');
const { createBucket } = require('./bucket');
var { save_activity } = require('./user_activity');
var { sg_email } = require('./sg_email');
// var { models } = require('../config/db/express-cassandra');
const Users = require('mongoose').model('user');
const Company = require('../mongo-models/company');
const Conversation = require('../mongo-models/conversation');
const BusinessUnit = require('../mongo-models/business_unit');
const Team = require('../mongo-models/team');
const { mode } = require('crypto-js');
var { xmpp_send_server, xmpp_send_broadcast, send_msg_firebase } = require('./voip_util');
var { create_new_personal_tag } = require('./tag');
var { fnln } = require('./message');
const isEmpty = require("is-empty");
const { v4: uuidv4 } = require('uuid');

function passwordToHass(str) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(str, salt, (err, hash) => {
                if (err) reject(err);
                resolve(hash);
            });
        });
    });
}

async function has_this_company_user(email, company_id) {
    var result = false;
    const user = await Users.find({ email, company_id }).exec();
    if (user.length > 0)
        return user[0];
    return result;
}

function has_other_company_user(email) {
    return new Promise((resolve) => {
        Users.find({ email }).then(function (u) {
            resolve(u);

        }).catch((e) => {
            resolve([]);
        });
    });
}

function get_user(query) {
    return new Promise((resolve, reject) => {
        Users.find(query, 'id email company_id firstname lastname dept designation access conference_id  phone is_active img role login_total createdat created_by').then(function (users) {
            if (users) {
                users.forEach(function (v, k) {
                    v.lastname = v.lastname ? v.lastname : "";
                    v.fullname = v.firstname + " " + v.lastname;
                    v.img = process.env.FILE_SERVER + 'profile-pic/Photos/' + v.img;
                    v.fnln = fnln(v);
                    v.role = v.role === '' ? 'Member' : v.role;
                    // OHS Global Inc.
                    if (v.company_id.toString() == '79df4d50-ce4a-11e9-90f6-e9974a7dde09' && !isUuid(v.created_by))
                        v.created_by = "0979ae3a-1fdc-4859-a702-cba13a61e471";
                    // ITL
                    else if (v.company_id.toString() == 'f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0' && !isUuid(v.created_by))
                        v.created_by = "02138189-ca7b-4627-8ad8-a84bb73b3f76";
                    else if (!isUuid(v.created_by))
                        v.created_by = v.id;
                });
                resolve(users);
            }
            else {
                resolve([]);
            }
        }).catch((error) => {
            reject({ status: false, error });
        });
    });
}

async function get_user_obj(id) {
    return new Promise((resolve, reject) => {
        for (let value of Object.entries(all_users)) {
            for (let v of value) {
                if (typeof v === 'object' && v.hasOwnProperty(id)) {
                    return resolve(v[id]);
                }
            }
        }
        if (cache_user) {
            return resolve({
                id: id,
                firstname: "",
                lastname: "",
                fullname: "",
                img: "",
                dept: "",
                designation: "",
                email: "",
                phone: "",
                access: [],
                company_id: "",
                company_name: "",
                conference_id: "",
                role: "",
                email_otp: "",
                is_active: 0,
                mute_all: "",
                mute: "",
                login_total: 0,
                fnln: "",
                createdat: ""
            });
        };
        console.log('get_user_obj', id)
        Users.findOne({ id: String(id) }, function (error, user) {
            if (user) {
                return resolve({
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    fullname: user.firstname + ' ' + user.lastname,
                    img: process.env.FILE_SERVER + user.img,
                    dept: user.dept,
                    designation: user.designation,
                    email: user.email,
                    phone: Array.isArray(user.phone) ? user.phone.join("") : "",
                    access: user.access,
                    company_id: user.company_id,
                    company_name: user.company,
                    conference_id: user.conference_id,
                    role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
                    email_otp: user.email_otp,
                    is_active: user.is_active,
                    mute_all: user.mute_all,
                    mute: user.mute ? JSON.parse(user.mute) : '',
                    login_total: user.login_total,
                    fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
                    device: user.device ? user.device : [],
                    short_id: user.short_id,
                    createdat: user.createdat ? user.createdat : new Date(),
                    fcm_id: user.fcm_id
                });

            } else {
                return resolve({
                    id: id,
                    firstname: "",
                    lastname: "",
                    fullname: "",
                    img: "",
                    dept: "",
                    designation: "",
                    email: "",
                    phone: "",
                    access: [],
                    company_id: "",
                    company_name: "",
                    conference_id: "",
                    role: "",
                    email_otp: "",
                    is_active: 0,
                    mute_all: "",
                    mute: "",
                    login_total: 0,
                    fnln: "",
                    createdat: ""
                });
            }

        });

    })

}


async function update_user_obj(user) {
    // console.log(55, all_users.hasOwnProperty(user.company_id.toString()));
    if (all_users.hasOwnProperty(user.company_id.toString()) && all_company.hasOwnProperty(user.company_id.toString())) {
        // console.log(57)
        user.img = user.img.indexOf('profile-pic/Photos/') > -1 ? user.img : process.env.FILE_SERVER + 'profile-pic/Photos/' + user.img;
        user.lastname = user.lastname === null ? '' : user.lastname;
        user.phone = Array.isArray(user.phone) ? user.phone.join("") : "";
        all_users[user.company_id.toString()][user.id.toString()] = {
            id: user.id.toString(),
            firstname: user.firstname,
            lastname: user.lastname,
            img: user.img,
            dept: user.dept,
            designation: user.designation,
            email: user.email,
            email: user.email,
            phone: user.phone,
            company_id: user.company_id.toString(),
            company_name: all_company[user.company_id.toString()].company_name,
            conference_id: user.conference_id,
            role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
            email_otp: user.email_otp,
            is_active: user.is_active,
            mute_all: user.mute_all,
            mute: user.mute ? JSON.parse(user.mute) : '',
            login_total: user.login_total,
            fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
            createdat: user.createdat
        }
        if (process.send) process.send({ type: 'all_users', all_users: all_users });
    }
    else if ((all_users.hasOwnProperty(user.company_id.toString()) && !all_company.hasOwnProperty(user.company_id.toString())) || user.temp_count === undefined) {
        // console.log(84)
        // models.instance.Company.find({ company_id: models.timeuuidFromString(user.company_id.toString()) }, { raw: true, allow_filtering: true }, function(err1, company) {
        //     if (err1) console.log("company find error in utils user js ", err1);
        //     if (company.length == 1) {
        //         all_company[company[0].company_id.toString()] = company[0];
        //         all_users[company[0].company_id.toString()] = {};

        //         models.instance.Team.find({company_id: models.timeuuidFromString(user.company_id.toString())}, { raw: true, allow_filtering: true }, function(err2, teams) {
        //             if (err2) console.log("Team find error in utils user js ", err2 );
        //             else {
        //                 if(teams.length > 0){
        //                     for (let i = 0; i < teams.length; i++) {
        //                         if (all_teams[teams[i].company_id.toString()] === undefined)
        //                             all_teams[teams[i].company_id.toString()] = [];

        //                         all_teams[teams[i].company_id.toString()].push(teams[i]);
        //                     }
        //                 }

        //                 models.instance.BusinessUnit.find({company_id: user.company_id.toString()}, { raw: true, allow_filtering: true }, function(err3, bu) {
        //                     if (err3) console.log("Room category find error in utils js ", err3);
        //                     else {
        //                         if(bu.length>0){
        //                             for (let i = 0; i < bu.length; i++) {
        //                                 if (all_bunit[bu[i].company_id.toString()] === undefined)
        //                                     all_bunit[bu[i].company_id.toString()] = [];
        //                                 all_bunit[bu[i].company_id.toString()].push(bu[i]);
        //                             }
        //                         }
        //                     }
        //                 });
        //             }
        //         });

        //         user.temp_count = 1;
        //         update_user_obj(user);
        //     }
        // });
        var r = await find_and_update(user);
    } else return false;

    return true;
}

function find_and_update(user) {
    return new Promise((resolve) => {
        Company.find({ company_id: user.company_id.toString() }, function (err1, company) {
            if (err1) console.log("company find error in utils user js ", err1);
            if (company.length == 1) {
                // console.log(130, company[0]);
                all_company[company[0].company_id.toString()] = company[0];
                // console.log(132, all_company[company[0].company_id.toString()]);
                all_users[company[0].company_id.toString()] = {};
                // console.log(137, all_company.hasOwnProperty(user.company_id.toString()));
                // console.log(138, all_company[user.company_id.toString()].hasOwnProperty("company_name"));

                Team.find({ company_id: user.company_id.toString() }, function (err2, teams) {
                    if (err2) console.log("Team find error in utils user js ", err2);
                    else {
                        if (teams.length > 0) {
                            for (let i = 0; i < teams.length; i++) {
                                if (all_teams[teams[i].company_id.toString()] === undefined)
                                    all_teams[teams[i].company_id.toString()] = [];

                                all_teams[teams[i].company_id.toString()].push(teams[i]);
                            }
                        }

                        BusinessUnit.find({ company_id: user.company_id.toString() }, function (err3, bu) {
                            if (err3) console.log("Room category find error in utils js ", err3);
                            else {
                                if (bu.length > 0) {
                                    for (let i = 0; i < bu.length; i++) {
                                        if (all_bunit[bu[i].company_id.toString()] === undefined)
                                            all_bunit[bu[i].company_id.toString()] = [];
                                        all_bunit[bu[i].company_id.toString()].push(bu[i]);
                                    }
                                }
                                user.temp_count = 1;
                                // console.log(195, user);
                                update_user_obj(user);
                                resolve(true);
                            }
                        });
                    }
                });
            }
        });
    });
}

function get_smtp(req) {
    // return new Promise((resolve, reject) => {
    //     var q = {};
    //     if(isUuid(req.query.id))
    //         q.id = models.timeuuidFromString(req.query.id);
    //     if(isUuid(req.query.user_id))
    //         q.user_id = req.query.user_id;
    //     models.instance.Smtp.find(q, {raw: true, allow_filtering: true}, function(e, smtp){
    //         if(e) reject({ error: 'Find error', message: e });
    //         resolve({ status: true, smtp });
    //     });
    // });
}

async function add_user(data, req = null) {
    return new Promise(async (allresolve) => {
        req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
        req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length - 1) : req.headers.origin;
        var promises = [];
        var queries = [];
        var teaminfo = {};
        req.body.team_list = Array.isArray(req.body.team_list) ? req.body.team_list : Array.isArray(req.body.team_id) ? req.body.team_id : [];
        // if(all_teams.hasOwnProperty(req.body.company_id)){
        //     (all_teams[req.body.company_id]).forEach(function(v, k) {
        //         if (v.team_id.toString() == req.body.team_id) {
        //             teaminfo = v;
        //             return false;
        //         }
        //     });
        // }
        // console.log(234, data, req); 
        for (let i = 0; i < data.length; i++) {
            if (await has_this_company_user(data[i].email, req.body.company_id) === false) {
                var ou = await has_other_company_user(data[i].email);
                promises.push(
                    new Promise(async (resolve, reject) => {
                        var new_uid = uuidv4();
                        var code = 0;
                        if (ou.length > 0) {
                            var password = ou[0].password;
                            data[i].firstname = ou[0].firstname;
                            data[i].lastname = ou[0].lastname;
                        } else {
                            if (data[i].password_req && data[i].password_req === "yes") {
                                var password = data[i].password;
                            } else {
                                code = Math.floor(100000 + Math.random() * 900000);
                                // var password = data[i].password ? data[i].password : 'a'+code;
                                var password = 'a' + code;
                            }

                        }
                        //  console.log(351,data);
                        var newUser = {
                            id: new_uid,
                            firstname: data[i].firstname ? data[i].firstname : '',
                            lastname: data[i].lastname ? data[i].lastname : '',
                            email: data[i].email,
                            password: ou.length == 0 ? await passwordToHass(password) : password,
                            email_otp: ou.length == 0 ? code.toString() : ou[0].email_otp,
                            role: req.body.role ? req.body.role : 'Member',
                            phone: data[i].phone ? [data[i].phone] : [],
                            company_id: req.body.company_id.toString(),
                            created_by: isUuid(req.body.user_id) ? req.body.user_id : new_uid.toString()
                        };

                        Users.create(newUser).then(async (addeduser) => {
                            console.log(209, addeduser);
                            let bucket_name = data[i].email.replace(/\./g, '-').replace(/\_/g, '-').replace('@', '-');
                            createBucket(bucket_name);
                            create_new_personal_tag(new_uid, req.body.company_id);
                            update_user_obj(addeduser);
                            var au = await Users.find({ company_id: req.body.company_id }).lean();
                             let con_e = await Conversation.findOne({ company_id: req.body.company_id, title: 'Everyone Channel' })
                            // console.log(231, con_e);
                              console.log(231, con_e.conversation_id);
                           var new_con= await Conversation.updateOne({conversation_id:con_e.conversation_id}, { $push: { participants: new_uid.toString() } });
                             console.log("all_users[req.body.company_id].length", new_con.conversation_id);
                            for (let i = 0; au.length > 1 && i < au.length; i++) {
                                xmpp_send_server(au[i].id, 'new_user', addeduser, req);
                                send_msg_firebase(au[i].id, 'new_user', addeduser, req);
                            }

                            if (req.body.team_list.length > 0 && isUuid(req.body.team_list[0])) {
                                for (let ti = 0; ti < req.body.team_list.length; ti++) {
                                    Team.updateOne({ team_id: req.body.team_list[ti] }, { $push: { participants: new_uid.toString() } }).then(function () { }).catch((e) => {
                                        console.log(389, e)
                                    });
                                    Conversation.updateOne({ company_id: req.body.company_id, system_conversation: 'team_' +req.body.team_list[ti]}, { $push: { participants: new_uid.toString() } });
                                }
                            } else {
                                var team_list = await Team.find({ company_id: req.body.company_id }).lean();
                                //console.log(382, team_list.length);
                                for (let ti = 0; ti < team_list.length; ti++) {
                                    Team.updateOne({ team_id: team_list[ti].team_id }, { $push: { participants: new_uid.toString() } }).then(function () { }).catch((e) => {
                                        console.log(385, e)
                                    });
                                    Conversation.updateOne({ company_id: req.body.company_id, system_conversation: 'team_' +team_list[ti].team_id}, { $push: { participants: new_uid.toString() } });
                                   // Conversation.updateOne({ conversation_id: team_list[ti].team_id }, { $push: { participants: new_uid.toString() } });
                                }
                            }

                            var title = req.body.title ? req.body.title + '</b> room of <b>' : '';
                            var convid = req.body.convid ? '/' + req.body.convid : '';
                            var passtxt = !isEmpty(addeduser.email_otp) ? '<br><b>One-Time Password:</b> a' + addeduser.email_otp : '';
                            var subject = ' invited you to join Workfreeli.';
                            var html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you as a guest to the <b>' + title + req.body.company_name + '</b> company in Workfreeli.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + convid + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                            // var html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you as a guest to the <b>'+ title + req.body.company_name +'</b> company in Workfreeli.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + addeduser.id.toString() + convid + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';

                            if (addeduser.role === 'Guest') {
                                subject = ' invited you to a Workfreeli room.';
                            } else if (addeduser.role === 'Admin') {
                                html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has setup a new Workfreeli business account <b>' + req.body.company_name + '</b> and has invited you as an administrator.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                                // html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has setup a new Workfreeli business account <b>'+ req.body.company_name +'</b> and has invited you as an administrator.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + addeduser.id.toString() + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                            } else if (addeduser.role === 'Member') {
                                var invitation_msg = req.body.msg ? '<br><br>' + req.body.name + ' sent you a message while inviting you: <br>' + req.body.msg : '';
                                if (!isEmpty(passtxt)) {
                                    if (addeduser.password_req && addeduser.password_req === "yes") {
                                        html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you to join a new business account on Workfreeli - <b>' + req.body.company_name + '</b>.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br>' + invitation_msg + 'To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + '<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                                    } else {
                                        html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you to join a new business account on Workfreeli - <b>' + req.body.company_name + '</b>.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br>' + invitation_msg + 'To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                                    }

                                    // html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you to join a new business account on Workfreeli - <b>'+ req.body.company_name +'</b>.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br>'+ invitation_msg +'To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + addeduser.id.toString() + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                                }
                                else {
                                    html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you to join a new business account on Workfreeli - <b>' + req.body.company_name + '</b>.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.<br><br>' + invitation_msg + 'To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + '<br><br><a clicktracking=off href="' + req.headers.origin + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                                }
                            }
                            // console.log(278, html);
                            var emaildata = {
                                to: addeduser.email,
                                subject: req.body.name + subject,
                                text: req.body.name + subject,
                                html: html
                            }

                            try {
                                if (req.originalUrl != '/conversation/update_room' && req.originalUrl != '/conversation/create_room' && (req.send_mail === undefined || req.send_mail === "" || req.send_mail === 'yes')) {
                                    sg_email.send(emaildata, (result) => {
                                        if (result.msg == 'success') {
                                            save_activity({
                                                company_id: addeduser.company_id,
                                                user_id: addeduser.id,
                                                title: addeduser.firstname + " email send successfully.",
                                                type: "success",
                                                body: req.body.name + ' added a new user named ' + addeduser.firstname + " as guest and email send successfully at " + new Date(),
                                                user_name: req.body.name,
                                                user_img: 'img.png',
                                            });
                                        } else {
                                            console.log('email not send');
                                        }
                                    });
                                }
                            } catch (e) {
                                console.log(e);
                            }

                            resolve(addeduser);
                        }).catch((e) => {
                            console.log(283, err);
                            reject(e);
                        });
                    })
                );
            } else {
                // console.log(367,req.body)
                // console.log(318, "************************************************", req.headers.origin);
                var addeduser = await has_this_company_user(data[i].email, req.body.company_id);
                // console.log(addeduser);
                var title = req.body.title ? req.body.title + '</b> room of <b>' : '';
                var convid = req.body.convid ? '/' + req.body.convid : '';
                var passtxt = '<br><b>One-Time Password:</b> a' + addeduser.email_otp;
                var subject = ' invited you to join Workfreeli.';
                var html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you as a guest to the <b>' + title + req.body.company_name + '</b> company in Workfreeli.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + convid + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';
                // var html = 'Hi ' + addeduser.firstname + '!<br><br>' + req.body.name + ' has invited you as a guest to the <b>'+ title + req.body.company_name +'</b> company in Workfreeli.<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + addeduser.email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + addeduser.id.toString() + convid + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com';

                if (addeduser.role === 'Guest') {
                    subject = ' invited you to a Workfreeli room.';
                }
                // console.log(328, html);
                var emaildata = {
                    to: addeduser.email,
                    subject: req.body.name + subject,
                    text: req.body.name + subject,
                    html: html
                }

                try {
                    if ((req.send_mail === undefined || req.send_mail === "" || req.send_mail === 'yes')) {
                        sg_email.send(emaildata, (result) => {
                            if (result.msg == 'success') {
                                save_activity({
                                    company_id: addeduser.company_id,
                                    user_id: addeduser.id,
                                    title: addeduser.firstname + " email send successfully.",
                                    type: "success",
                                    body: req.body.name + ' added a new user named ' + addeduser.firstname + " as guest and email send successfully at " + new Date(),
                                    user_name: req.body.name,
                                    user_img: 'img.png',
                                });
                            } else {
                                console.log('email not send');
                            }
                        });
                    }
                } catch (e) {
                    console.log(e);
                }
                promises.push(addeduser);
            }
        }
        Promise.all(promises).then(values => {
            // console.log(values);
            allresolve(values);
        }).catch(error => {
            console.error(error.message)
            allresolve([]);
        });
    });
}
function self_conversation_create_if_not_found(result) {
    // console.log(951, result)
    Conversation.findOne({ conversation_id: result.id.toString() }).then(function (sc) {

        if (sc === null || sc === undefined) {
            let InsertRows = {
                conversation_id: String(result.id),
                created_by: String(result.created_by),
                participants: [String(result.id)],
                participants_admin: [String(result.id)],
                participants_guest: [],
                title: "Personal",
                group: "no",
                team_id: "",
                privacy: "private",
                archive: "no",
                is_active: [],
                status: "active",
                close_for: [],
                conv_img: String(result.img),
                is_pinned_users: [],
                topic_type: "",
                b_unit_id: "",
                room_id: "",
                created_at: result.createdat,
                is_busy: result.is_busy,
                company_id: String(result.company_id),
                last_msg: "",
                last_msg_time: "",
                sender_id: String(result.id),
                tag_list: [],
                msg_status: "",
                conference_id: String(result.conference_id),
                root_conv_id: "",
                reset_id: "",
                pin: [String(result.id)],
                has_mute: [],
                mute: [],
                short_id: "",

            };
            Conversation.create(InsertRows).then(function () {
                console.log("Self conversation create done")
            }).catch((e) => {
                console.log(528, e)
            });
            /*  Conversation.create(InsertRows, function(err, c){
                 if(err) console.log(528, err);
                 else console.log("Self conversation create done"); 
             }); */
        } else {
            console.log(532, sc);
        }
    }).catch((e) => {
        console.log(949, e);
    });
}
module.exports = { get_user, get_user_obj, update_user_obj, get_smtp, add_user, self_conversation_create_if_not_found };