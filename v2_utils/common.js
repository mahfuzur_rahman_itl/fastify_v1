exports.isValidJson = function (json) {
    try {
      // Parse the JSON object
      const obj = JSON.parse(json);
      
      // Check for null or undefined
      if (obj === null || obj === undefined) {
        return false;
      }
      
      // Check for empty string, array, or object
      if ((typeof obj === "string" || Array.isArray(obj)) && obj.length === 0) {
        return false;
      }
      
      if (typeof obj === "object" && Object.keys(obj).length === 0) {
        return false;
      }
      
      // Check for non-alphanumeric string
      if (typeof obj === "string" && !/^[a-zA-Z0-9]+$/.test(obj)) {
        return false;
      }
      
      // Check for NaN or Infinity
      if (typeof obj === "number" && !isFinite(obj)) {
        return false;
      }
      
      // If all checks pass, return true
      return true;
    } catch (e) {
      // Catch any parsing errors and return false
      return false;
    }
}

exports.validateJSON = function (jsonObj, requiredKeys) {
    const missingKeys = [];
  
    // Check if jsonObj is not null or undefined
    if (!jsonObj) {
      missingKeys.push(...requiredKeys);
      return missingKeys;
    }
  
    // Loop through each required key
    for (let i = 0; i < requiredKeys.length; i++) {
      const key = requiredKeys[i];
  
      // Check if the key exists in the JSON object
      if (!(key in jsonObj) || ! jsonObj[key]) {
        missingKeys.push(key);
      }
    }
  
    // Return an array of missing keys
    return missingKeys;
  }
  