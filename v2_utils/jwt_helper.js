const jwt = require('jsonwebtoken');
const User = require('../mongo-models/user');

function extractToken(req){
	const bearerHeader = req.headers["authorization"];
	if(typeof bearerHeader !== 'undefined'){
		const bearer = bearerHeader.split(" ");
		if(bearer[0] == "Bearer"){
			const bearerToken = bearer[1];
			return bearerToken;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
}

function token_verify_decode(token){
    return new Promise(async (resolve,reject)=>{
        let decode = jwt.verify(token, process.env.SECRET);
        // console.log(24, decode.id);
        var userinfo = await User.aggregate([
                                {$match: {id: decode.payload.id}},
                                {$lookup:{ 
                                    from: 'companies', 
                                    localField: 'company_id', 
                                    foreignField:'company_id',
                                    as:'company_info' 
                                }},
                                {$project: {
                                    mergedData: { $mergeObjects: [ '$$ROOT', { company_name: { $arrayElemAt: ['$company_info.company_name', 0] } } ] }
                                }}
                                ]).exec();
        // console.log(34, userinfo);
        if(userinfo) {
            let u = userinfo[0].mergedData;
            u.xmpp_user = decode.payload.xmpp_user;
            u.device_id = decode.payload.device_id;
            resolve(u);
        }
        else reject({ status: false, error: "Device is not listed. Please Login." });
    });
}

function verify_refresh(token){
	return new Promise((resolve)=>{
        var decode = {};
        var refresh_token = "";
        try{
            decode = jwt.verify(token, process.env.SECRET);

            const payload = {
                id: decode.id,
                firstname: decode.firstname,
                lastname: decode.lastname,
                email: decode.email,
                company_id: decode.company_id,
                company_name: all_company[decode.company_id.toString()].company_name,
                multi_company: decode.multi_company,
                device_id: decode.device_id,
                device: decode.device
            };

            jwt.sign(
                payload,
                process.env.SECRET, {
                    expiresIn: 30 //31556926 // 1 year in seconds
                },
                async(err, new_token) => {
                    resolve({decode, refresh_token: "Bearer "+new_token});
                }
            );

        }catch(e){
            resolve({decode, refresh_token});
        }
    });
}

module.exports = { extractToken, token_verify_decode, verify_refresh };