var zkclient = require('../config/zookeeper');
var moment = require('moment');
const _ = require('lodash');

async function get_user_obj(id) {
    return new Promise((resolve, reject) => {
        for (let value of Object.entries(all_users)) {
            for (let v of value) {
                if (typeof v === 'object' && v.hasOwnProperty(id)) {
                    return resolve(v[id]);
                }
            }
        }
        if (cache_user) {
            return resolve({
                id: id,
                firstname: "",
                lastname: "",
                fullname: "",
                img: "",
                dept: "",
                designation: "",
                email: "",
                phone: "",
                access: [],
                company_id: "",
                company_name: "",
                conference_id: "",
                role: "",
                email_otp: "",
                is_active: 0,
                mute_all: "",
                mute: "",
                login_total: 0,
                fnln: "",
                createdat: ""
            });
        };
        // console.log('get_user_obj',id)
        Users.findOne({ id: String(id) }).then(function (user) {
            if (user) {
                return resolve({
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    fullname: user.firstname + ' ' + user.lastname,
                    img: process.env.FILE_SERVER + user.img,
                    dept: user.dept,
                    designation: user.designation,
                    email: user.email,
                    phone: Array.isArray(user.phone) ? user.phone.join("") : "",
                    access: user.access,
                    company_id: user.company_id,
                    company_name: user.company,
                    conference_id: user.conference_id,
                    role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
                    email_otp: user.email_otp,
                    is_active: user.is_active,
                    mute_all: user.mute_all,
                    mute: user.mute ? JSON.parse(user.mute) : '',
                    login_total: user.login_total,
                    fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
                    device: user.device ? user.device : [],
                    short_id: user.short_id,
                    createdat: user.createdat ? user.createdat : new Date(),
                    fcm_id: user.fcm_id
                });

            } else {
                return resolve({
                    id: id,
                    firstname: "",
                    lastname: "",
                    fullname: "",
                    img: "",
                    dept: "",
                    designation: "",
                    email: "",
                    phone: "",
                    access: [],
                    company_id: "",
                    company_name: "",
                    conference_id: "",
                    role: "",
                    email_otp: "",
                    is_active: 0,
                    mute_all: "",
                    mute: "",
                    login_total: 0,
                    fnln: "",
                    createdat: ""
                });
            }

        }).catch((e) => {

        });

    })
}

function fnln(data) {
    let str = "";
    if (data.firstname && data.firstname != "") {
        str += (data.firstname).charAt(0);
    }
    if (data.lastname && data.lastname != "") {
        str += (data.lastname).charAt(0);
    }
    return str;
}
// zookeeper file save documentaton.

/**
 * Node Structure
 * Base Node- workfeeli
 |-workfeeli
 |--cache

 |-workfeeli
 |--connect
 |---{user_id}
 |----{conversation list data}
 
 |-workfeeli
 |--active_conv
 |---{user_id}
 |----{conversation_id}
 |-----{msgs data}
 |-----{details data}
 
 |-workfeeli
 |--reply_msgs
 |---{reply_for_msgid}
 |----conversation
 |----main_msg
 |----reply_msgs
 
 |-workfeeli
 |--all_flag
 |---{user_id}
 |----list_of_conversations
 |----{conversation_id}

 |-workfeeli
 |--all_private_msgs
 |---{user_id}
 |----list_of_conversations
 |----{conversation_id}
 
 |-workfeeli
 |--total_unread_reply
 |---{user_id}
 |----{total_unread_msg}
 |----{total_unread_reply}
 |----{total_unread_notification}
 |----{total_r_mention_user}
 |----{conversations_list}
 |----{conversations}
 |----{conversations_arr}
 
 |-workfeeli
 |--get_reply_msg
 |---{user_id}
 |----{conversation_id}
 |-----msg
 |-----details

 */
// var path = '/workfeeli';
var path = '/workfeeli';

console.log(75, path);
function parseJSONSafely(str) {
    // 182.163.122.133
    // '70.35.204.57:2181
    if (str === null || str === 'null') return [];
    try {
        return JSON.parse(str);
    }
    catch (e) {
        console.log("Error in JSON parse");
        if (typeof str == 'string') return [str];
        else return [];
    }
}
function zNode_init() {
    zkclient.exists(path, function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path, null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path);
                }
            });
        }
    });

    zkclient.exists(path + '/cache', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/cache', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/cache');
                }
            });
        }
    });

    /**
     * User wise left side conversation list node
     */
    zkclient.exists(path + '/connect', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/connect', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/connect');
                }
            });
        }
    });

    /**
     * User wise right side active_conv list node
     */
    zkclient.exists(path + '/active_conv', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/active_conv', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/active_conv');
                }
            });
        }
    });

    /**
     * reply msg node
     */
    zkclient.exists(path + '/reply_msgs', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/reply_msgs', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/reply_msgs');
                }
            });
        }
    });
    /**
     * all_flag node
     */
    zkclient.exists(path + '/all_flag', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/all_flag', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/all_flag');
                }
            });
        }
    });
    /**
     * all_private_msgs node
     */
    zkclient.exists(path + '/all_private_msgs', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/all_private_msgs', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/all_private_msgs');
                }
            });
        }
    });
    /**
     * total_unread_reply node
     */
    zkclient.exists(path + '/total_unread_reply', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/total_unread_reply', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/total_unread_reply');
                }
            });
        }
    });
    zkclient.exists(path + '/get_reply_msg', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/get_reply_msg', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/get_reply_msg');
                }
            });
        }
    });
}
function zNode_init_final() {
    zkclient.exists(path, function (error, stat) {
        if (error) {
            // multi node error check
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path, null, function (error) {
                if (error) {
                    // multi node error check
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path);
                }
            });
        }
    });

    zkclient.exists(path + '/cache', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/cache', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/cache');
                }
            });
        }
    });

    /**
     * User wise left side conversation list node
     */
    zkclient.exists(path + '/connect', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/connect', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/connect');
                }
            });
        }
    });

    /**
     * User wise right side active_conv list node
     */
    zkclient.exists(path + '/active_conv', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/active_conv', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/active_conv');
                }
            });
        }
    });

    /**
     * reply msg node
     */
    zkclient.exists(path + '/reply_msgs', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/reply_msgs', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/reply_msgs');
                }
            });
        }
    });
    /**
     * all_flag node
     */
    zkclient.exists(path + '/all_flag', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/all_flag', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/all_flag');
                }
            });
        }
    });
    /**
     * all_private_msgs node
     */
    zkclient.exists(path + '/all_private_msgs', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/all_private_msgs', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/all_private_msgs');
                }
            });
        }
    });
    /**
     * total_unread_reply node
     */
    zkclient.exists(path + '/total_unread_reply', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/total_unread_reply', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/total_unread_reply');
                }
            });
        }
    });
    zkclient.exists(path + '/get_reply_msg', function (error, stat) {
        if (error) {
            console.log(error.stack);
        }
        if (stat) {
            console.log('Node exists.');
        } else {
            zkclient.create(path + '/get_reply_msg', null, function (error) {
                if (error) {
                    console.log('Failed to create node: %s due to: %s.', path, error);
                } else {
                    console.log('Node: %s is successfully created.', path + '/get_reply_msg');
                }
            });
        }
    });
}
/**
 * before zookeeper init,
 * clear all old zookeeper data
 */
zNodeRecursive(path).then(() => {
    zNode_init();
})

function setZK(path, data) {
    return new Promise((resolve, reject) => {
        if (!zkConnectionStatus) resolve(false);

        zkclient.exists(path, function (error, stat) {
            if (error) {
                console.log(error.stack);
                resolve(false);
            }
            let bufData = data ? Buffer.from(JSON.stringify(data)) : null;
            if (stat) {
                console.log(`${path} node exists. need to delete`);
                // console.log('Stat details: ', stat);
                // zkclient.setData(path, bufData, Date.now(), function (error, stat) {
                //     if (error) {
                //         console.log(error.stack);
                //         resolve(false);
                //     }

                //     console.log('Znode data updated successfully:', stat);
                //     resolve(true);
                // });
                if (zNodeDelete(path)) {
                    zkclient.create(path, bufData, function (error) {
                        if (error) {
                            console.log('Failed to create node: %s due to: %s.', path, error);
                            resolve(false);
                        } else {
                            console.log('Node: %s is successfully created.', path);
                            resolve(true);
                        }
                    });
                }
            } else {
                zkclient.create(path, bufData, function (error) {
                    if (error) {
                        console.log('Failed to create node: %s due to: %s.', path, error);
                        resolve(false);
                    } else {
                        console.log('Node: %s is successfully created.', path);
                        resolve(true);
                    }
                });
            }
        });
    })
}

function getZK(path) {
    return new Promise((resolve, reject) => {
        if (!zkConnectionStatus) resolve(false);
        zkclient.exists(path, function (error, stat) {
            if (error) {
                console.log(error.stack);
                resolve(false);
            }
            // console.log(44, stat);
            if (stat) {
                console.log(`${path} node exists.`);
                zkclient.getData(path, function (event) {
                    console.log('Got event: %s.', event);
                },
                    function (error, data, stat) {
                        if (error) {
                            console.log(error.stack);
                            resolve(false);
                        }
                        if (data) {
                            let val = JSON.parse(data.toString('utf8'));
                            if (val) {
                                // console.log('Got data: %s', val);
                                resolve(val)
                            } else {
                                resolve([]);
                            }
                        }
                        else {
                            resolve([]);
                        }
                    }
                );
            } else {
                zkclient.create(path, null, function (error) {
                    if (error) {
                        console.log('Failed to create node: %s due to: %s.', path, error);
                        resolve(false);
                    } else {
                        console.log('Node: %s is successfully created.', path);
                        resolve([]);
                    }
                });
            }
        });
    });
}


async function updateConversationName(data) {
    let con = `${path}/connect`;
    /**
     * All left side connect, conversation lists
     */
    zkclient.getChildren(con, async (error, children, stat) => {
        if (error) {
            console.log(error);
        }
        for (let child of children) {
            // child = user id
            let datacon = con + '/' + child;
            let convs = await getZK(datacon);
            for (let v of convs) {
                if (v.group === 'no') {
                    if (v.friend_id === data.id || v.conversation_id === data.id) {
                        console.log(377, v.friend_id, data.firstname, v.conversation_id);
                        v.title = data.firstname + ' ' + data.lastname;
                        v.fnln = fnln(data);
                        v.conv_img = data.img.indexOf('profile-pic/Photos/') > -1 ? data.img : process.env.FILE_SERVER + 'profile-pic/Photos/' + data.img;

                        // console.log(351, v);
                    }
                }
            }
            if (await zNodeDelete(datacon)) {
                await delayFun(100);
                await setZK(datacon, convs);
            }
        }

        /**
         * user base find the active conversation
         */
        con = `${path}/active_conv/`;
        for (let child of children) {
            let ccon = con + child;
            zkclient.getChildren(ccon, async (error1, children1, stat1) => {
                if (error1) {
                    console.log(392, error1);
                }
                else {
                    for (let child1 of children1) {
                        let acondata = ccon + '/' + child1;
                        // console.log(396, acondata);
                        let active_conv = await getZK(acondata);
                        if (active_conv['details'].group === 'no') {
                            if (active_conv['details'].friend_id === data.id) {
                                active_conv['details'].title = data.firstname + ' ' + data.lastname;
                                active_conv['details'].fnln = fnln(data);
                                active_conv['details'].conv_img = data.img.indexOf('profile-pic/Photos/') > -1 ? data.img : process.env.FILE_SERVER + 'profile-pic/Photos/' + data.img;
                            }
                        }
                        if (await zNodeDelete(acondata)) {
                            await delayFun(100);
                            await setZK(acondata, active_conv);
                        }
                        await delayFun(100);
                    }
                }
            });
        }
    });

    let acccon = `${path}/active_conv/${data.id}/${data.id}`;
    let hasNode = await checkZNode(acccon);
    if (hasNode) {
        let active_conv = await getZK(acccon);
        active_conv['details'].title = data.firstname + ' ' + data.lastname;
        active_conv['details'].fnln = fnln(data);
        active_conv['details'].conv_img = data.img.indexOf('profile-pic/Photos/') > -1 ? data.img : process.env.FILE_SERVER + 'profile-pic/Photos/' + data.img;

        if (await zNodeDelete(acccon)) {
            await delayFun(100);
            await setZK(acccon, active_conv);
        }
    }
}


function checkZNode(path) {
    return new Promise((resolve, reject) => {
        if (!zkConnectionStatus) resolve(false);

        zkclient.exists(path, function (error, stat) {
            if (error) {
                console.log(error.stack);
                resolve(false);
            }
            if (stat) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
}

function zNodeDelete(n) {
    return new Promise(resolve => {
        zkclient.remove(n, -1, function (error) {
            if (error) {
                console.log(100, error.stack);
                resolve(false);
            }
            else {
                console.log(n, ' Node is deleted.');
                resolve(true);
            }
        });
    });
}

function zNodeRecursive(n) {
    return new Promise(resolve => {
        zkclient.getChildren(n, (error, children, stat) => {
            if (error) {
                resolve(error);
                return;
            }

            if (children.length === 0) {
                zkclient.remove(n, -1, (error) => {
                    resolve(error);
                });
            }
            else {
                // If there are child nodes, recursively delete each child node
                let deletedCount = 0;
                children.forEach((child) => {
                    zNodeRecursive(n + '/' + child).then(() => {
                        if (++deletedCount === children.length) {
                            zkclient.remove(n, -1, (error) => {
                                resolve(error);
                            });
                        }
                    });
                });
            }
        });
    });
}

function delayFun(t) {
    return new Promise(resolve => {
        setTimeout(() => { resolve(true); }, t);
    });
}

async function znMsgUpdate(data, p, type) {
    let cid = data.conversation_id;
    console.log(609, p.length);
    if (type === 'msg_update_delete') {
        if (p.length === 1) {
            data.has_delete.push(p[0]);
            data.has_hide.push(p[0]);
        } else {
            data.has_delete = ['for_all', ...p];
            data.has_hide = ['for_all', ...p];
        }
    }

    if (data.is_reply_msg === 'yes' && (type === 'msg_update_emoji' || type === 'msg_update_edit' || type === 'msg_update_delete' || type === 'msg_update_star_file' || type === 'call_msg_update')) {
        let rnr = `${path}/reply_msgs/${data.reply_for_msgid}`;
        let hasNode = await checkZNode(rnr);
        if (hasNode) {
            let result = {};
            result.reply_msgs = await getZK(`${rnr}/reply_msgs`);
            if (result.reply_msgs && result.reply_msgs.length > 0) {
                for (let i = 0; i < result.reply_msgs.length; i++) {
                    if (result.reply_msgs[i].msg_id === data.msg_id) {
                        data.all_attachment = [...result.reply_msgs[i]['all_attachment']]
                        result.reply_msgs[i] = data;
                        if (await zNodeDelete(`${rnr}/reply_msgs`)) {
                            await delayFun(100);
                            await setZK(`${rnr}/reply_msgs`, result.reply_msgs);
                            break;
                        }
                    }
                }
            }
        }
    }
    else {
        for (let i = 0; i < p.length; i++) {
            /**
             * check if participants have any active conversation
             */
            let acn = `${path}/active_conv/${p[i]}`;
            let hasacNode = await checkZNode(acn);
            if (hasacNode) {
                acn = `${path}/active_conv/${p[i]}/${cid}`;
                let hasNode = await checkZNode(acn);
                if (hasNode) {
                    let active_conv = await getZK(acn);
                    if (type === 'update_main_msg_reply_counter') {
                        for (let row in active_conv['msgs']) {
                            if (active_conv['msgs'][row].msg_id === data.reply_for_msgid) {
                                active_conv['msgs'][row].last_reply_name = data.sender_name;
                                active_conv['msgs'][row].last_reply_time = new Date().getTime();
                                active_conv['msgs'][row].has_reply = Number(active_conv['msgs'][row].has_reply) + 1;
                                active_conv['msgs'][row].has_reply_attach = data.msg_type.indexOf('media_attachment') > -1 ? Number(active_conv['msgs'][row].has_reply_attach) + 1 : Number(active_conv['msgs'][row].has_reply_attach);
                            }
                        }
                    }
                    else if (type === 'msg_update_flag_unflag') {
                        for (let row in active_conv['msgs']) {
                            if (active_conv['msgs'][row].msg_id === data.msg_id) {
                                active_conv['msgs'][row].has_flagged = Array.isArray(active_conv['msgs'][row].has_flagged) ? active_conv['msgs'][row].has_flagged : [];
                                if (data.is_add === 'no')
                                    active_conv['msgs'][row].has_flagged = active_conv['msgs'][row].has_flagged.filter((e) => { return e !== p[i] });
                                else
                                    active_conv['msgs'][row].has_flagged.push(p[i]);
                                // console.log(153, active_conv['msgs'][row]);
                            }
                        }
                    }
                    else if (type === 'msg_update_emoji' || type === 'msg_update_edit' || type === 'msg_update_delete' || type === 'msg_update_star_file' || type === 'call_msg_update') {

                        for (let row in active_conv['msgs']) {
                            //console.log(537, active_conv['msgs'][row]);
                            // console.log(538, data.msg_id);
                            if (active_conv['msgs'][row].msg_id === data.msg_id) {
                                if (type === 'msg_update_edit') {
                                    console.log(547, data.msg_id);
                                    active_conv['msgs'].splice(row, 1);
                                    break;
                                    // active_conv['msgs'].push(data);
                                }
                                // console.log(545, active_conv['msgs'][row].created_at);
                                // console.log(541, active_conv['msgs'][row]);
                                // data.all_attachment = [...active_conv['msgs'][row]['all_attachment']];
                                if(type === 'msg_update_emoji'){
                                    data.all_attachment = active_conv['msgs'][row].all_attachment
                                }
                                
                                active_conv['msgs'][row] = data;
                                // console.log(549, active_conv['msgs'][row].created_at);
                                // delete active_conv['msgs'][row];
                                //active_conv['msgs'][row].push
                                // console.log(544, active_conv['msgs'][row]);
                            }
                            // console.log(153, active_conv['msgs'][row]);
                        }
                        if (type === 'msg_update_edit') {
                            console.log(563, data.msg_id);
                            // active_conv['msgs'].splice(row,1);
                            active_conv['msgs'].push(data);
                        }
                    }
                    if (await zNodeDelete(acn)) {
                        await delayFun(100);
                        await setZK(acn, active_conv);
                    }
                }
            }
            if (type === 'msg_update_flag_unflag') {
                let afmn = `${path}/all_flag/${p[i]}`;
                let hasAFMN = await checkZNode(afmn);
                if (hasAFMN) {
                    //     afmn = `${path}/all_flag/${p[i]}/list_of_conversations`;
                    //     let list_of_conversations = await getZK(afmn);
                    //     for(let c of list_of_conversations){
                    //         let fmn = `${path}/all_flag/${p[i]}/${c.conversation_id}`;
                    //         await zNodeDelete(fmn);
                    //     }
                    //     await zNodeDelete(afmn);
                    //     let afmn = `${path}/all_flag/${p[i]}`;
                    await zNodeRecursive(afmn);
                }
            }
        }
    }
}
async function znPrivetMsgUpdate(data, p, remove_user,read) {
    let cid = data.conversation_id;
    /**
     * check if participants have remove user Privet Msg
     */
    if (remove_user.length > 0) {
        console.log(733, cid);
        for (let i = 0; i < remove_user.length; i++) {
            let con = `${path}/total_unread_reply/${remove_user[i]}`;
            let hasNode = await checkZNode(con);
            if (hasNode) {
                var result = await getZK(con);
                console.log(740, data.is_reply_msg);
                //console.log(741, result);
                result.total_unread_reply = result.total_unread_reply - read;
                if (result.conversations.hasOwnProperty(cid)) {
                    result.conversations[cid].urreply = result.conversations[cid].urreply -read;
                   
                }
                else {
                    result.conversations[data.conversation_id] = {
                        'conversation_id': data.conversation_id,
                        'urmsg': 1,
                        'urreply': 0,
                        'r_mention_user': 0,
                        'mention_user': 0,
                        'msgids': [],
                        'title': "",
                        'conv_img': "",
                        'fnln': '',
                        'friend_id': ''
                    };
                }
                if (await zNodeDelete(con)) {
                    await delayFun(100);
                    await setZK(con, result);
                }
            }
            let acn = `${path}/active_conv/${remove_user[i]}`;
            let hasacNode = await checkZNode(acn);
            if (hasacNode) {
                console.log(738, cid);
                acn = `${path}/active_conv/${remove_user[i]}/${cid}`;
                let hasNode = await checkZNode(acn);
                if (hasNode) {
                    //console.log(592, user_id);
                    let active_conv = await getZK(acn);
                    for (let row in active_conv['msgs']) {
                        //console.log(537, active_conv['msgs'][row]);
                        //  console.log(538, data.msg_id);
                        if (active_conv['msgs'][row].msg_id === data.msg_id) {
                            console.log(748, active_conv['msgs'][row].msg_id);

                            // active_conv['msgs'][row];
                            active_conv['msgs'].splice(row, 1);
                        }
                        // console.log(153, active_conv['msgs'][row]);
                    }
                    if (await zNodeDelete(acn)) {
                        await delayFun(100);
                        await setZK(acn, active_conv);
                    }
                    //console.log(630, user_id);
                }
            }

        }
    } else {
        for (let i = 0; i < p.length; i++) {
            let acn = `${path}/active_conv/${p[i]}`;
            let hasacNode = await checkZNode(acn);
            if (hasacNode) {
                console.log(768, cid);
                acn = `${path}/active_conv/${p[i]}/${cid}`;
                let hasNode = await checkZNode(acn);
                if (hasNode) {
                    // console.log(592, user_id);
                    let active_conv = await getZK(acn);
                    var check = false;
                    for (let row in active_conv['msgs']) {
                        //console.log(537, active_conv['msgs'][row]);
                        // console.log(538, data.msg_id);
                        if (active_conv['msgs'][row].msg_id === data.msg_id) {
                            console.log(779, active_conv['msgs'][row].msg_id);
                            if (data.secret_user.indexOf(p[i]) > -1 || data.sender === p[i]) {
                                // console.log(613, user_id);
                                active_conv['msgs'][row].secret_user = data.secret_user;
                                active_conv['msgs'][row].is_secret = data.is_secret;
                                active_conv['msgs'][row].msg_body = data.msg_body;
                                check = true;
                            }
                        }
                        // console.log(153, active_conv['msgs'][row]);
                    }
                    if (check === false) {
                        active_conv['msgs'].push(data);
                    }
                    if (await zNodeDelete(acn)) {
                        await delayFun(100);
                        await setZK(acn, active_conv);
                    }
                    // console.log(630, user_id);
                }
                else {
                    //  console.log(656,acn)
                }
            }
        }
    }





}
async function znTagMsgUpdate(data) {
    let cid = data.conversation_id;
    let p = data.participants;
    if (data.is_reply) {
        con = `${path}/reply_msgs/${data.root_msg_id}`;
        hasNode = await checkZNode(con);
        if (hasNode) {
            con = `${path}/reply_msgs/${data.root_msg_id}/reply_msgs`;
            hasNode = await checkZNode(con);
            if (hasNode) {
                let reply_msgs = await getZK(con);
                if (reply_msgs.length > 0) {
                    for (let row in reply_msgs) {
                        if (reply_msgs[row].msg_id === data.msg_id) {
                            reply_msgs[row] = data.msg;
                        }
                    }
                    if (await zNodeDelete(con)) {
                        await delayFun(100);
                        await setZK(con, active_conv);
                    }
                }
            }
        }
    } else {
        for (let i = 0; i < p.length; i++) {
            let con = `${path}/active_conv/${p[i]}/${cid}`;
            let hasNode = await checkZNode(con);
            if (hasNode) {
                let active_conv = await getZK(con);
                for (let row in active_conv['msgs']) {
                    if (active_conv['msgs'][row].msg_id === data.msg_id) {
                        active_conv['msgs'][row] = data.msg;
                    }
                }
                if (await zNodeDelete(con)) {
                    await delayFun(100);
                    await setZK(con, active_conv);
                }
            }

            con = `${path}/get_reply_msg/${p[i]}/${cid}`;
            hasNode = await checkZNode(con);
            if (hasNode) {
                let grmmsg = await getZK(con + '/msg');
                for (let i = 0; i < grmmsg.length; i++) {
                    if (grmmsg[i].msg_id === data.msg_id) {
                        grmmsg[i] = data.msg;
                    }
                }
                if (await zNodeDelete(con + '/msg')) {
                    await delayFun(100);
                    await setZK(con + '/msg', grmmsg);
                }
            }
        }
    }
}

async function sendMsgUpdateIntoZnode(data) {
   // console.log(1077,data);
    let cid = data.details.conversation_id;
    let p = data.msgs[0].participants;
    if (data.msgs[0].is_reply_msg === 'no') {
        console.log(724, data.details.last_msg);
        console.log(725, data.msgs[0].msg_body);
        data.details.last_msg = data.msgs[0].msg_body;
        console.log(727, data.details.last_msg);
        for (let i = 0; i < p.length; i++) {
            /**
             * check if any participants have connect znode.
             * if have, then update the node data
             */
            let con = `${path}/connect/${p[i]}`;
            let hasConnect = await checkZNode(con);
            if (hasConnect) {
                let convs = await getZK(con);
                //console.log(737,convs);
                // fix the conversation name
                if (data.details.group === 'no') {
                    if (data.details.conversation_id === p[i])
                        var fid = data.details.conversation_id;
                    else
                        var fid = data.details.participants.join().replace(p[i], '').replace(',', '');
                    let this_user = await get_user_obj(fid);
                    if (this_user.firstname !== '' && this_user.firstname !== undefined) {
                        data.details.title = this_user.fullname ? this_user.fullname : (this_user.firstname + ' ' + this_user.lastname).trim();
                        data.details.conv_img = this_user.img;
                        data.details.friend_id = fid;
                        data.details.fnln = fnln(this_user);
                        data.details.conv_is_active = this_user.is_active;
                    }
                }

                let need2update = convs.filter((e) => { return e.conversation_id === cid });
                //only for new participants/ room
                if (!need2update) {
                    convs = [data.details, ...convs];
                    // console.log(740,convs);
                    if (await zNodeDelete(con)) {
                        await delayFun(100);
                        await setZK(con, convs);
                    }
                }
                //old room
                else {
                    // console.log(750,convs[0]);
                    console.log(751, cid);
                    //if(convs[0].conversation_id !== cid){
                    let other = convs.filter((e) => { return e.conversation_id !== cid });
                    convs = [data.details, ...other];
                    // console.log(750,convs);
                    if (await zNodeDelete(con)) {
                        await delayFun(100);
                        await setZK(con, convs);
                    }
                    // }
                }
            }

            /**
             * check if participants have any active conversation
             */
            let acn = `${path}/active_conv/${p[i]}`;
            let hasacNode = await checkZNode(acn);
            if (hasacNode) {
                acn = `${path}/active_conv/${p[i]}/${cid}`;
                let hasNode = await checkZNode(acn);
                if (hasNode) {
                    let active_conv = await getZK(acn);
                    active_conv['msgs'].push(data.msgs[0]);
                    if (await zNodeDelete(acn)) {
                        await delayFun(100);
                        await setZK(acn, active_conv);
                    }
                }
            }

            /**
             * check msg is private msg or not
             * if private, then check all private msg node
             * if have data in all private msg, then update it aslo
             */
            if (data.msgs[0].is_secret === true && Array.isArray(data.msgs[0].secret_user)) {
                let pmn = `${path}/all_private_msgs/${p[i]}`;
                let hasacNode = await checkZNode(pmn);
                if (hasacNode) {
                    pmn = `${path}/all_private_msgs/${p[i]}/list_of_conversations`;
                    let list_of_conversations = await getZK(pmn);
                    let need2update = list_of_conversations.filter((e) => { return e.conversation_id === cid });
                    if (!need2update) {
                        list_of_conversations = [data.details, ...list_of_conversations];
                        if (await zNodeDelete(pmn)) {
                            await delayFun(100);
                            await setZK(pmn, list_of_conversations);
                        }
                        pmn = `${path}/all_private_msgs/${p[i]}/${cid}`;
                        await setZK(pmn, { msgs: data.msgs, pagination: data.pagination });
                    }
                    else {
                        pmn = `${path}/all_private_msgs/${p[i]}/${cid}`;
                        let apm = await getZK(pmn);
                        if (await zNodeDelete(pmn)) {
                            await delayFun(100);
                            await setZK(pmn, { msgs: [...data.msgs, ...apm.msgs], pagination: apm.pagination });
                        }
                    }
                }
            }

            /**
             * increase the counter,
             * except sender
             */
            let turn = `${path}/total_unread_reply/${p[i]}`;
            let hasNode = await checkZNode(turn);
            if (hasNode) {
                var tur = await getZK(turn);
                if (data.msgs[0].sender !== p[i]) {
                    if (tur.conversations_list.indexOf(cid) === -1)
                        tur.conversations_list.push(cid);
                    // tur.total_unread_msg = tur.total_unread_msg ? Number(tur.total_unread_msg) + 1 : 0;
                }

                if (tur.conversations.hasOwnProperty(cid)) {
                    if (data.msgs[0].sender !== p[i])
                        tur.conversations[cid].urmsg = Number(tur.conversations[cid].urmsg) + 1;
                }
                else {
                    if (data.msgs[0].sender !== p[i]) {
                        tur.conversations[cid] = {
                            'conversation_id': cid,
                            'urmsg': 1,
                            'urreply': 0,
                            'r_mention_user': 0,
                            'mention_user': 0,
                            'msgids': [],
                            'title': data.details.title,
                            'conv_img': data.details.conv_img,
                            'fnln': data.details.fnln,
                            'friend_id': data.details.friend_id
                        };
                    }
                }
                tur.total_unread_msg = 0;
                for (let m in tur.conversations) {
                    tur.total_unread_msg += tur.conversations[m].urmsg;
                }

                console.log(720, tur.total_unread_msg);

                if (await zNodeDelete(turn)) {
                    await delayFun(100);
                    await setZK(turn, tur);
                }
            }
        }
    }
    else if (data.msgs[0].is_reply_msg === 'yes') {
        let rnr = `${path}/reply_msgs/${data.msgs[0].reply_for_msgid}`; // reply node root = rnr
        let hasNode = await checkZNode(rnr);
        if (hasNode) {
            let result = {};
            result.reply_msgs = await getZK(`${rnr}/reply_msgs`);
            if (result.reply_msgs && result.reply_msgs.length > 0) {
                result.reply_msgs.push(data.msgs[0]);
            } else {
                result.reply_msgs = [data.msgs[0]];
            }
            console.log(1040, data.msgs[0]);
            if (await zNodeDelete(`${rnr}/reply_msgs`)) {
                await delayFun(100);
                await setZK(`${rnr}/reply_msgs`, result.reply_msgs.length > 0 ? result.reply_msgs : null);
                await znMsgUpdate(data.msgs[0], p, 'update_main_msg_reply_counter');
            }
        }

        for (let i = 0; i < p.length; i++) {
            let turn = `${path}/total_unread_reply/${p[i]}`;
            let hasNode = await checkZNode(turn);
            if (hasNode) {
                var tur = await getZK(turn);
                if (data.msgs[0].sender !== p[i]) {
                    if (tur.conversations_list.indexOf(cid) === -1)
                        tur.conversations_list.push(cid);
                    // tur.total_unread_reply = tur.total_unread_reply ? Number(tur.total_unread_reply) + 1 : 0;
                }

                if (tur.conversations.hasOwnProperty(cid)) {
                    if (data.msgs[0].sender !== p[i])
                        tur.conversations[cid].urreply = Number(tur.conversations[cid].urreply) + 1;
                }
                else {
                    if (data.msgs[0].sender !== p[i]) {
                        tur.conversations_list.push(cid);
                        tur.conversations[cid] = {
                            'conversation_id': cid,
                            'urmsg': 0,
                            'urreply': 1,
                            'r_mention_user': 0,
                            'mention_user': 0,
                            'msgids': [
                                {
                                    'msgid': data.msgs[0].reply_for_msgid,
                                    'unread': 1
                                }
                            ],
                            'title': data.details.title,
                            'conv_img': data.details.conv_img,
                            'fnln': data.details.fnln,
                            'friend_id': data.details.friend_id
                        };
                    }
                }

                tur.total_unread_reply = 0;
                for (let m in tur.conversations) {
                    tur.total_unread_reply += tur.conversations[m].urreply;
                }
                console.log(787, tur.total_unread_reply);
                if (await zNodeDelete(turn)) {
                    await delayFun(100);
                    await setZK(turn, tur);
                }
            }

            /**
             * unread reply msg counter add into active conversation
             */
            let acn = `${path}/active_conv/${p[i]}/${cid}`;
            let hasacNode = await checkZNode(acn);
            if (hasacNode) {
                let reply_for_msgid = data.msgs[0].reply_for_msgid;

                let active_conv = await getZK(acn);
                // console.log(1106,reply_for_msgid);
                for (let j = 0; j < active_conv['msgs'].length; j++) {
                    if (active_conv['msgs'][j].msg_id === reply_for_msgid && p[i] !== data.msgs[0].sender) {
                        active_conv['msgs'][j].unread_reply = Number(active_conv['msgs'][j].unread_reply) + 1;
                        console.log(1165, p[i], data.msgs[0].sender, active_conv['msgs'][j].unread_reply);
                    }
                }

                // for(let i=0; i<active_conv['msgs'].length; i++){
                //    // console.log(1108,active_conv['msgs'][i].msg_id);
                //     if(active_conv['msgs'][i].msg_id === reply_for_msgid){
                //         console.log(1109,data.msgs[0]);
                //         active_conv['msgs'][i].has_reply = active_conv['msgs'][i].has_reply + 1;
                //         if(data.msgs[0].sender !== p[i]){
                //             active_conv['msgs'][i].unread_reply = active_conv['msgs'][i].unread_reply +1;
                //         }
                //     }
                // }
                if (await zNodeDelete(acn)) {
                    await delayFun(100);
                    await setZK(acn, active_conv);
                }
            }

            con = `${path}/get_reply_msg/${p[i]}/${cid}`;
            hasNode = await checkZNode(con);
            if (hasNode) {
                let grmmsg = await getZK(con + '/msg');
                let reply_for_msgid = data.msgs[0].reply_for_msgid;
                let has_previouly = false;
                for (let j = 0; i < grmmsg.length; j++) {
                    if (grmmsg[j].msg_id === reply_for_msgid) {
                        grmmsg[j].has_reply = grmmsg[j].has_reply + 1;
                        has_previouly = true;
                        if (data.msgs[0].sender !== p[i]) {
                            grmmsg[j].unread_reply = grmmsg[j].unread_reply + 1;
                            console.log(1197, grmmsg[j].unread_reply);
                        }
                    }
                }
                if (has_previouly === false) {
                    await zNodeDelete(`${con}/msg`);
                    await zNodeDelete(`${con}/details`);
                    await zNodeDelete(con);
                } else {
                    if (await zNodeDelete(`${con}/msg`)) {
                        await delayFun(100);
                        await setZK(`${con}/msg`, grmmsg);
                    }
                }
            }

        }
    }
}

async function znReadReply(msgs, uid) {
    let cid = msgs[0].conversation_id;
    let con = `${path}/total_unread_reply/${uid}`;
    let hasNode = await checkZNode(con);
    if (hasNode) {
        let tur = await getZK(con);
        if (tur.conversations[cid].urreply > 0) {
            tur.total_unread_reply = tur.total_unread_reply && Number(tur.total_unread_reply) >= 0 ? Number(tur.total_unread_reply) - msgs.length : tur.total_unread_reply;

            tur.conversations[cid].urreply = Number(tur.conversations[cid].urreply) - msgs.length;

            if (tur.conversations[cid].urreply === 0 && tur.conversations[cid].urmsg == 0) {
                delete tur.conversations[cid];
                tur.conversations_list.splice(tur.conversations_list.indexOf(cid), 1);
            }

            if (await zNodeDelete(con)) {
                await delayFun(100);
                await setZK(con, tur);
            }
        }
    }

    con = `${path}/get_reply_msg/${uid}/${cid}`;
    hasNode = await checkZNode(con);
    if (hasNode) {
        let grmmsg = await getZK(con + '/msg');
        let reply_for_msgid = msgs[0].reply_for_msgid;
        for (let i = 0; i < grmmsg.length; i++) {
            if (grmmsg[i].msg_id === reply_for_msgid) {
                grmmsg[i].unread_reply = Number(grmmsg[i].unread_reply) - msgs.length;
                if (grmmsg[i].unread_reply <= 0) {
                    grmmsg.splice(i, 1);
                }

                if (grmmsg.length >= 1) {
                    if (await zNodeDelete(`${con}/msg`)) {
                        await delayFun(100);
                        await setZK(`${con}/msg`, grmmsg);
                        break;
                    }
                } else {
                    await zNodeDelete(`${con}/msg`);
                    await zNodeDelete(`${con}/details`);
                    await zNodeDelete(con);
                    break;
                }
            }
        }
    }
}

async function znReadMsgUpdateCounter(data) {
    //console.log(673, data);
    let con = `${path}/total_unread_reply/${data.user_id}`;
    let hasNode = await checkZNode(con);
    if (hasNode) {
        var result = await getZK(con);
        if (data.is_reply_msg === 'yes') {
            result.total_unread_reply = result.total_unread_reply - data.read;
            // console.log(680, result.total_unread_reply);
        }
        else
            result.total_unread_msg = result.total_unread_msg - data.read;

        result.total_unread_reply = result.total_unread_reply < 0 ? 0 : result.total_unread_reply;
        result.total_unread_msg = result.total_unread_msg < 0 ? 0 : result.total_unread_msg;
        if (result.conversations.hasOwnProperty(data.conversation_id)) {
            result.conversations[data.conversation_id].urmsg = data.is_reply_msg !== 'yes' ? result.conversations[data.conversation_id].urmsg - data.read : result.conversations[data.conversation_id].urmsg;

            result.conversations[data.conversation_id].urreply = data.is_reply_msg === 'yes' ? result.conversations[data.conversation_id].urreply - data.read : result.conversations[data.conversation_id].urreply;

            result.conversations[data.conversation_id].urmsg = result.conversations[data.conversation_id].urmsg < 0 ? 0 : result.conversations[data.conversation_id].urmsg;
            result.conversations[data.conversation_id].urreply = result.conversations[data.conversation_id].urreply < 0 ? 0 : result.conversations[data.conversation_id].urreply;

            result.conversations[data.conversation_id].msgids = result.conversations[data.conversation_id].msgids.map((e) => { return e.msgid === data.root_msg_id ? asdf(e) : e });
            function asdf(e) {
                e.unread -= data.read;
                e.unread = e.unread < 0 ? 0 : e.unread;
                return e;
            }


            if (result.conversations[data.conversation_id].urmsg <= 0 && result.conversations[data.conversation_id].urreply <= 0) {
                result.conversations_list.splice(result.conversations_list.indexOf(data.conversation_id), 1);
            }
            /* if(result.conversations[data.conversation_id].hasOwnProperty('urmsg')){

            }else{

            } */
        }
        else {
            result.conversations[data.conversation_id] = {
                'conversation_id': data.conversation_id,
                'urmsg': 1,
                'urreply': 0,
                'r_mention_user': 0,
                'mention_user': 0,
                'msgids': [],
                'title': "",
                'conv_img': "",
                'fnln': '',
                'friend_id': ''
            };
        }


        if (await zNodeDelete(con)) {
            await delayFun(100);
            await setZK(con, result);
        }

        con = `${path}/get_reply_msg/${data.user_id}/${data.conversation_id}/msg`;
        hasNode = await checkZNode(con);
        if (hasNode) {
            var msgs = await getZK(con);
            msgs['msgs'] = msgs['msgs'].map((e) => {
                if (e.msg_id === data.root_msg_id) {
                    e.unread_reply -= data.read;
                    e.unread_reply = e.unread_reply < 0 ? 0 : e.unread_reply;
                }
                return e;
            });

            if (msgs.length <= 0) {
                con = `${path}/get_reply_msg/${data.user_id}/${data.conversation_id}`;
                await zNodeRecursive(con);
            } else {
                if (await zNodeDelete(con)) {
                    await delayFun(100);
                    await setZK(con, msgs);
                }
            }
        }

        /**
         * unread reply msg counter add into active conversation
         */
        let acn = `${path}/active_conv/${data.user_id}/${data.conversation_id}`;
        let hasacNode = await checkZNode(acn);
        console.log(825, hasacNode);
        if (hasacNode) {
            let active_conv = await getZK(acn);
            for (let i = 0; i < active_conv['msgs'].length; i++) {
                if (active_conv['msgs'][i].msg_id === data.root_msg_id) {
                    active_conv['msgs'][i].unread_reply = 0;
                    console.log(831, active_conv['msgs'][i].unread_reply);
                }
            }
            if (await zNodeDelete(acn)) {
                await delayFun(100);
                await setZK(acn, active_conv);

                //console.log(838, active_conv);
            }
        }
    }
}

async function znConNodeUpdate(data) {
    // console.log(972,"ddg");
    let con = `${path}/connect/${data.uid}`;
    let hasConnect = await checkZNode(con);
    if (hasConnect) {
        //console.log(978,"asf");
        var convs = await getZK(con);
        if (data.type === 'new_room') {
            convs.unshift(data.conv);
            // console.log(982,"asf");
        }
        else if (data.type === 'update_room') {
            convs = convs.map((e) => { return e.conversation_id === data.conv.conversation_id ? data.conv : e; });
        }
        else if (data.type === 'kick_out') {
            convs = convs.filter((e) => { return e.conversation_id !== data.conv.conversation_id });
        }
        else if (data.type === 'room_archive') {
            convs = convs.map((e) => {
                if (e.conversation_id === data.conv.conversation_id) {
                    e.archive = data.conv.archive;
                    console.log(1010, data.conv.archive);
                }
                return e;
            });
        }
        else if (data.type === 'close_room') {
            convs = convs.map((e) => {
                if (e.conversation_id === data.conv.conversation_id) {
                    // console.log(1018,e);
                    e.close_for = data.close_room;

                    // console.log(1021,data.close_room);

                }
                return e;
            });
        }
        else if (data.type === 'mute_conversation') {

            convs = convs.map((e) => {

                if (e.conversation_id === data.conv) {
                    //console.log(1032,e);
                    var new_mute_data = [];
                    const muteid = [];
                    if (e.has_mute.indexOf(data.uid) > -1) {
                        //console.log(1048,data.mute_data.mute_duration);
                        //muteid == e.has_mute;
                        var nowUnix = moment(data.mute_data.mute_start_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();
                        var endUnix = moment(data.mute_data.mute_end_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();


                        var update_mute_data = JSON.stringify({
                            conversation_id: data.conv,
                            mute_by: data.uid,
                            mute_duration: data.mute_data.mute_duration,
                            mute_start_time: data.mute_data.mute_start_time,
                            mute_end_time: data.mute_data.mute_end_time,
                            mute_day: data.mute_data.mute_day,
                            mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                            mute_unix_end: endUnix ? endUnix.toString() : "0",
                            mute_timezone: data.mute_data.mute_timezone ? data.mute_timezone : '',
                        });
                        _.each(e.mute, function (v, k) {
                            var mute_data_new = parseJSONSafely(v);
                            // console.log(1072,mute_data_new.mute_by);
                            // console.log(1073,data.uid);
                            if (mute_data_new.mute_by == data.uid) {
                                new_mute_data.push(update_mute_data);
                            } else {
                                new_mute_data.push(v);
                            }
                        });
                        e.mute = new_mute_data;
                        if (data.mute_data.type === 'delete') {
                            e.has_mute = [];
                        } else {
                            e.has_mute = e.has_mute;
                        }


                    }
                    else {
                        // console.log(1048,data.mute_data.mute_duration);
                        var nowUnix = moment(data.mute_data.mute_start_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();
                        var endUnix = moment(data.mute_data.mute_end_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();


                        var update_mute_data = JSON.stringify({
                            conversation_id: data.conv,
                            mute_by: data.uid,
                            mute_duration: data.mute_data.mute_duration,
                            mute_start_time: data.mute_data.mute_start_time,
                            mute_end_time: data.mute_data.mute_end_time,
                            mute_day: data.mute_data.mute_day,
                            mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                            mute_unix_end: endUnix ? endUnix.toString() : "0",
                            mute_timezone: data.mute_data.mute_timezone ? data.mute_timezone : '',
                        })
                        new_mute_data.push(update_mute_data);
                        muteid.push(data.uid);
                        e.mute = new_mute_data;
                        e.has_mute = muteid;
                    }
                    //console.log(1040,e);
                }
                return e;
            });
        }
        // console.log(1009,convs.mute);
        if (await zNodeDelete(con)) {
            await delayFun(100);
            await setZK(con, convs);
        }

        await delayFun(1000);
        if (data.type === 'mute_conversation') {
            con = `${path}/active_conv/${data.uid}/${data.conv}`;
        } else {
            con = `${path}/active_conv/${data.uid}/${data.conv.conversation_id}`;
        }

        let hasNode = await checkZNode(con);
        if (hasNode) {
            // console.log(1057,active_conv.details.close_room)
            let active_conv = await getZK(con);
            if (data.type === 'room_archive') {
                active_conv['details'].archive = data.archive;
            }
            else if (data.type === 'close_room') {
                //console.log(1061,active_conv['details'])
                active_conv['details'].close_for = data.close_room;
                //console.log(1063,data.close_room);
            }
            else if (data.type === 'mute_conversation') {

                // console.log(978,active_conv);
                var new_mute_data = [];
                const muteid = [];
                if (active_conv['details'].has_mute.indexOf(data.uid) > -1) {
                    var nowUnix = moment(data.mute_data.mute_start_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();
                    var endUnix = moment(data.mute_data.mute_end_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();

                    var update_mute_data = JSON.stringify({
                        conversation_id: data.conv,
                        mute_by: data.uid,
                        mute_duration: data.mute_data.mute_duration,
                        mute_start_time: data.mute_data.mute_start_time,
                        mute_end_time: data.mute_data.mute_end_time,
                        mute_day: data.mute_data.mute_day,
                        mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                        mute_unix_end: endUnix ? endUnix.toString() : "0",
                        mute_timezone: data.mute_data.mute_timezone ? data.mute_timezone : '',
                    });
                    _.each(active_conv['details'].mute, function (v, k) {
                        var mute_data_new = parseJSONSafely(v);
                        // console.log(1145,mute_data_new);
                        if (mute_data_new.mute_by == data.uid) {
                            new_mute_data.push(update_mute_data);
                        } else {
                            new_mute_data.push(v);
                        }
                    });
                    active_conv['details'].mute = new_mute_data;
                    if (data.mute_data.type === 'delete') {
                        active_conv['details'].has_mute = [];
                    } else {
                        active_conv['details'].has_mute = active_conv['details'].has_mute;
                    }


                } else {
                    var nowUnix = moment(data.mute_data.mute_start_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();
                    var endUnix = moment(data.mute_data.mute_end_time + ' ' + data.mute_data.mute_timezone, 'llll Z').unix();

                    var update_mute_data = JSON.stringify({
                        conversation_id: data.conv,
                        mute_by: data.uid,
                        mute_duration: data.mute_data.mute_duration,
                        mute_start_time: data.mute_data.mute_start_time,
                        mute_end_time: data.mute_data.mute_end_time,
                        mute_day: data.mute_data.mute_day,
                        mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                        mute_unix_end: endUnix ? endUnix.toString() : "0",
                        mute_timezone: data.mute_data.mute_timezone ? data.mute_timezone : '',
                    });
                    new_mute_data.push(update_mute_data);
                    muteid.push(data.uid);
                    active_conv['details'].mute = new_mute_data;
                    active_conv['details'].has_mute = muteid;

                }
                // console.log(1081,active_conv['details']);
            } else {
                active_conv['details'] = data.conv;
            }

            if (await zNodeDelete(con)) {
                await delayFun(100);
                await setZK(con, active_conv);
            }
        }
    }
}

module.exports = {
    zNode_init,
    setZK,
    getZK,
    checkZNode,
    sendMsgUpdateIntoZnode,
    znMsgUpdate,
    znReadReply,
    zNodeDelete,
    zNodeRecursive,
    delayFun,
    znReadMsgUpdateCounter,
    znConNodeUpdate,
    updateConversationName,
    znTagMsgUpdate,
    znPrivetMsgUpdate
};