const fs = require('fs');
const { resolve } = require('path');
const { v4: uuidv4 } = require('uuid');
const Company = require('../mongo-models/company');
const File = require('../mongo-models/file');
const Conversation = require('../mongo-models/conversation');
const BusinessUnit = require('../mongo-models/business_unit');
const Tagcount = require('../mongo-models/tagcount');
const Tag = require('../mongo-models/tag');
const Team = require('../mongo-models/team');
const Users = require('../mongo-models/user');
const teammate = require('../mongo-models/teammate');
const Version = require('../mongo-models/version');
const TaskCustom = require('../mongo-models/task_custom');
const Project = require('../mongo-models/project');
const Task = require('../mongo-models/task');
var { has_this_title_conversation } = require('./conversation');
// var zkclient = require('../config/zookeeper');

function load_company() {
    return new Promise(async (resolve, reject) => {
        var companies = await Company.find({}).lean();
        for (let i = 0; i < companies.length; i++) {
            companies[i].member = 0;
            companies[i].keyspace = (companies[i].company_name.replace(/ /g, "_")).replace(/[^\w\s]/gi, "").toLowerCase();
            companies[i].plan_name = companies[i].plan_name ? companies[i].plan_name : 'starter';
            companies[i].plan_user_limit = companies[i].plan_user_limit ? companies[i].plan_user_limit : '100';
            companies[i].plan_storage_limit = companies[i].plan_storage_limit ? companies[i].plan_storage_limit : '5';
            all_company[companies[i].company_id.toString()] = companies[i];
        }
        console.log("All company are loaded");
        // let path = '/workfeeli/cache/company';
        // zkclient.exists(path, function (error, stat) {
        //     if (error) {
        //         console.log(error.stack);
        //     }
        //     if (stat) {
        //         console.log('Node exists.');
        //     } else {
        //         zkclient.create(path, Buffer.from(JSON.stringify(companies)), function (error) {
        //             if (error) {
        //                 console.log('Failed to create node: %s due to: %s.', path, error);
        //             } else {
        //                 console.log('Node: %s is successfully created.', path);
        //             }
        //         });
        //     }
        // });
        resolve();
    });
}
load_company();

// function team_admin_add(){
//     return new Promise(async (resolve, reject)=>{
//         var teams = await Team.find({}).lean();
//         for (let t of teams) {
//             await Team.updateOne({_id: t._id}, {admin: [t.created_by]});
//         }
//         console.log('All team_admin_add are done');
//         resolve();
//     });
// }
// team_admin_add();
function load_team() {
    return new Promise(async (resolve, reject) => {
        var teams = await Team.find({}).lean();
        for (let i = 0; i < teams.length; i++) {
            if (all_teams[teams[i].company_id.toString()] === undefined)
                all_teams[teams[i].company_id.toString()] = [];

            all_teams[teams[i].company_id.toString()].push(teams[i]);

            // if( teams[i].team_id === "960e78d0-5532-11ed-8e33-9c8d42e8d745") console.log("====> FOUND", teams[i]);
        }
        console.log('All teams are loaded');
        // team_admin_room_admin();
        resolve();
    });
}
load_team();

function load_users() {
    var { xmpp_send_all, xmpp_send_broadcast } = require('./voip_util');

    return new Promise(async (resolve, reject) => {
        var result = await Users.find().lean();
        for (let i = 0; i < result.length; i++) {
            if (result[i].company_id && all_company.hasOwnProperty(result[i].company_id.toString())) {
                if (all_users[result[i].company_id.toString()] === undefined)
                    all_users[result[i].company_id.toString()] = {};

                result[i].img = process.env.FILE_SERVER + 'profile-pic/Photos/' + result[i].img;
                result[i].lastname = result[i].lastname ? result[i].lastname : '';
                result[i].phone = Array.isArray(result[i].phone) ? result[i].phone.join("") : "";
                all_company[result[i].company_id.toString()].member++;

                // if (result[i].is_active === 0) {
                //     result[i].lastname += " [Inactive]";
                //     result[i].lastname = result[i].lastname.trim();
                // }

                // console.log(42, result[i].company_id.toString());
                // console.log(43, all_users[result[i].company_id.toString()].length);
                all_users[result[i].company_id.toString()][result[i].id.toString()] = {
                    id: result[i].id.toString(),
                    firstname: result[i].firstname,
                    lastname: result[i].lastname,
                    fullname: result[i].firstname + ' ' + result[i].lastname,
                    img: result[i].img,
                    dept: result[i].dept,
                    designation: result[i].designation,
                    email: result[i].email,
                    phone: result[i].phone,
                    access: result[i].access,
                    company_id: result[i].company_id.toString(),
                    company_name: all_company[result[i].company_id.toString()].company_name,
                    conference_id: result[i].conference_id,
                    role: ((result[i].role !== null) && (result[i].role !== '')) ? result[i].role === 'User' ? 'Member' : result[i].role : 'Member',
                    is_active: result[i].is_active,
                    mute_all: result[i].mute_all,
                    mute: result[i].mute ? JSON.parse(result[i].mute) : '',
                    login_total: result[i].login_total,
                    email_otp: result[i].email_otp,
                    fnln: result[i].firstname.charAt(0) + result[i].lastname.charAt(0),
                    device: result[i].device ? result[i].device : [],
                    short_id: result[i].short_id,
                    createdat: result[i].createdat ? result[i].createdat : new Date(),
                    fcm_id: result[i].fcm_id
                };
                var user_id = result[i].id.toString();
                if (!all_devices[user_id]) all_devices[user_id] = [];
                if (result[i].device) {
                    for (let device_id of result[i].device) {
                        if (all_devices[user_id].indexOf(device_id) === -1)
                            all_devices[user_id].push(device_id);

                    }
                }
            }
        }
        console.log("All users are loaded");
        
        let project_update = await Project.updateMany({ $or: [{ project_title: "Undefined" }, { project_title: "Untitled" },{ project_title: "undefined" }, { project_title: "null" },{ project_title: "" }, { project_title: undefined }, { project_title: null } ] }, { $set: { project_title: "Not Defined" } });
        let task_update = await Task.updateMany({ $or: [{ project_title: "Undefined" }, { project_title: "Untitled" },{ project_title: "undefined" }, { project_title: "null" },{ project_title: "" }, { project_title: undefined }, { project_title: null } ] }, { $set: { project_title: "Not Defined" } });

        console.log("project title updated:",project_update,task_update);

        // try{
        //     [
        //         { id: "Not Started 2", label: "Not Started 2" },
        //         // { id: "In Progress", label: "In Progress" },
        //         // { id: "On Hold", label: "On Hold" },
        //         // { id: "Completed", label: "Completed" },
        //         // { id: "Canceled", label: "Canceled" }
        //     ].map(async v => {
        //         await new TaskCustom({
        //             "custom_type": "Status",
        //             label: v.label,
        //             value: v.label,
        //             company_id: "57620630-552b-11ed-bc02-3d08415c9794",
        //             created_by: "62adaa8c-6686-428d-b958-1fe75b946222"
        //         }).save();
        //         // console.log(reviewInstant)
            
        //     })
        
        // }catch(e){
        //     console.log(e)
        // }

        // new Tag({
        //     tag_id          : uuidv4(),
        //     tagged_by       : "",
        //     title           : "#TASK",
        //     type            : 'tag',
        //     tag_type        : 'task',
        //     tag_color       : '#023d67',
        //     company_id      : "",
        //     use_count       : 0
        // }).save().then(()=>{
        //     console.log("done");

        // });
        // remove_garbage_company();
        cache_user = true;
        // if (process.env.API_SERVER_IP == 'localhost' || process.env.API_SERVER_IP.includes('192')) return;

        try {
            xmpp_send_all("update_client_version", { restart_time });
        } catch (e) {
            console.log(112, e);
        }
        resolve();
    });
}
setTimeout(load_users, 2000);

function load_untag() {
    return new Promise((resolve, reject) => {
        Tag.find({ title: 'UNTAGGED FILES' }).then(async function (untags) {
            // if(e) reject(e);
            for (let i = 0; i < untags.length; i++) {

                all_untags[untags[i].company_id] = untags[i];
            }
            let insertTags = [];
            for (let [key, value] of Object.entries(all_company)) {
                if (!all_untags[value.company_id]) {
                    var newtag = {
                        tag_id: uuidv4(),
                        tagged_by: value.created_by,
                        title: "UNTAGGED FILES",
                        type: 'tag',
                        tag_type: 'public',
                        tag_color: '#023d67',
                        company_id: value.company_id.toString(),
                        use_count: 0
                    };
                    insertTags.push(newtag);
                    console.log(newtag);
                }
            }
            Tag.insertMany(insertTags);
            console.log("All untagged are loaded");
            resolve();
        });
    });
}

async function remove_garbage_company() {
    for (let [key, value] of Object.entries(all_company)) {
        console.log(`${value.company_name} = ${value.member}`);
        if (value.member < 5) {
            await Users.deleteMany({ company_id: key }).exec();
            await Company.deleteOne({ company_id: key }).exec();
        }
    }
}
setTimeout(load_untag, 2000);

function tagcount() {
    return new Promise(resolve => {
        Tagcount.deleteMany({}, async function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log('All data removed from Tagcount');
                let convs = await Conversation.find({}).lean();
                let convsobj = {};
                for (let i = 0; i < convs.length; i++) {
                    convsobj[convs[i].conversation_id.toString()] = convs[i].participants;
                }
                console.log('All Conversation load');
                let files = await File.find({}).lean();
                console.log('All File load ', files.length);
                let c = 1;
                for (let i = 0; i < files.length; i++) {
                    if (files[i].is_delete === 0) {
                        files[i].tag_list = Array.isArray(files[i].tag_list) ? files[i].tag_list : [];
                        let data = [];
                        for (let j = 0; j < files[i].tag_list.length; j++) {
                            let members = files[i].is_secret === true && files[i].secret_user.length > 0 ? files[i].secret_user : convsobj[files[i].conversation_id.toString()];
                            if (files[i].has_delete.length > 0)
                                members = members.filter((e) => { return files[i].has_delete.indexOf(e) === -1 });
                            for (let k = 0; k < members.length; k++) {
                                let tc = new Tagcount({
                                    company_id: files[i].company_id.toString(),
                                    conversation_id: files[i].conversation_id.toString(),
                                    msg_id: files[i].msg_id.toString(),
                                    tag_id: files[i].tag_list[j].toString(),
                                    user_id: members[k].toString(),
                                    file_id: files[i].id.toString(),
                                    used_by: members[k].toString() === files[i].user_id.toString() ? 'me' : 'others'
                                });
                                data.push(tc);
                            }
                        }
                        try {
                            await Tagcount.insertMany(data);
                        } catch (e) {
                            console.log(213, e);
                            resolve();
                        }
                        console.log(`${i} file done`);
                    }
                }
                console.log('Tagcount script completed');
                resolve();
            }
        });
    });
}

// tagcount();


// function get_team_admin(company_id, team_id){
//     return new Promise(resolve =>{
//         console.log(232, company_id, team_id);
//         let ct = all_teams[company_id];
//         if(ct && ct.length !== undefined && ct.length !== null){
//             console.log(234, ct.length)
//             for(let i=0; i<ct.length; i++){
//                 console.log(236, ct[i].team_id.toString(), team_id.toString())
//                 console.log(237, ct[i].team_id.toString() === team_id.toString())
//                 if(ct[i].team_id.toString() === team_id.toString()){
//                     console.log("ct[i].admin", ct[i].admin);
//                     resolve(ct[i].admin);
//                 }
//             }
//         }
//         resolve([])
//     });
// }
// function team_admin_room_admin(){
//     return new Promise(async resolve =>{
//         let convs = await Conversation.find({}).lean();
//         console.log('All Conversation load');
//         for(let i=0; i<convs.length; i++){
//             console.log("loop run ", 1);
//             await new Promise(async update_resolve => {
//                 console.log(246, convs[i].company_id, convs[i].team_id);
//                 let team_admin = await get_team_admin(convs[i].company_id, convs[i].team_id);
//                 console.log('team_admin:db:',team_admin)
//                 let participants = [...new Set([...convs[i].participants, ...team_admin])];
//                 let participants_admin = [...new Set([...convs[i].participants_admin, ...team_admin])];
//                 await Conversation.updateOne({_id: convs[i]._id}, {participants, participants_admin});
//                 update_resolve();
//             });
//             console.log(i + ' conversation update done');
//         }
//         console.log('team_admin_room_admin script completed');
//         resolve();
//     });
// }

/**
 * Database change if the version number not match
 */
async function database_update_req() {
    let database_update_req = false;
    var vinfo = await Version.findOne({}).sort({ "_id": "desc" }).exec();

    // in major version 2, clear all the mute data, because mute data have some gurbage info.
    if (vinfo.major < 2) {
        database_update_req = true;
        console.log("mute data clear script running.....")
        let c = await Conversation.updateMany({}, { has_mute: [], mute: [] });
        console.log("mute data clear script end.....")
    }

    // conversation member update, but the file table participants not updated
    // so in major version 3, manually all file participants update required
    if (vinfo.major < 3) {
        console.log("File participants update script running.....")
        database_update_req = true;
        let convs = await Conversation.find({}).lean();
        let c = {};
        let cids = [];
        for (let v of convs) {
            cids.push(v.conversation_id.toString());
            c[v.conversation_id.toString()] = v.participants;
        }
        let all_files = await File.find({ conversation_id: { $in: cids }, is_secret: false, is_delete: 0 }).lean();
        let update_convid = [];
        for (let f of all_files) {
            if (update_convid.indexOf(f.conversation_id) === -1 && c[f.conversation_id].length != f.participants.length) {
                let r = await File.updateMany({ conversation_id: f.conversation_id }, { participants: c[f.conversation_id] });
                console.log(f.conversation_id, r.matchedCount, r.modifiedCount);
                update_convid.push(f.conversation_id);
            }
        }
        console.log("File participants update script end.....")
    }
    if (vinfo.major < 6) {
        console.log("Everyone Channel update script runing.....")

        Conversation.find({ title: 'Everyone Channel' }).remove().exec();
        // Conversation.find({ system_conversation: 'team' }).remove().exec();
        // Conversation.find({sender_id:"f979329b-c0bc-4155-aa60-4421a29bfa4e"}).remove().exec();
        database_update_req = true;

        try {
            var companies = await Company.find({}).lean();
            //console.log(339, companies.length);
            for (let i = 0; i < companies.length; i++) {
                new Promise(async (resolve, reject) => {
                    Conversation.updateMany({ company_id: companies[i].company_id }, { system_conversation: "No", system_conversation_active: "No", system_conversatio_is_active: [], system_conversatio_send_sms: [] }, async function (err) {
                        if (err) {
                            console.log(334, 'Conversation update not successfully.');
                        } else {
                            // console.log(334, 'Conversation update  successfully.');
                        }

                        // return res.status(200).json({ status: true, message: 'Conversation pin successfully.' });
                    });

                    var convid = uuidv4();
                    var cur_time = new Date().getTime();
                    let participants_all = [];
                    let participants_admin_v = [];
                    let participants_guest_v = [];
                    participants_admin_v.push(companies[i].created_by);
                    // let teams = await Team.find({company_id: decode.company_id}).lean();
                    let list = await BusinessUnit.findOne({ company_id: companies[i].company_id });
                    var au = await Users.find({ company_id: companies[i].company_id, is_active: 1 }).lean();
                    //var au = await Users.find({ company_id: companies[i].company_id}).lean();
                    //  console.log("all_users[req.body.company_id].length", au.length);
                    // console.log(361,companies[i].company_name)
                    for (let k = 0; k < au.length; k++) {
                        participants_all.push(au[k].id);

                    }
                    const conv = new Conversation({
                        conversation_id: convid,
                        created_by: companies[i].created_by,
                        sender_id: companies[i].created_by,
                        created_at: cur_time,
                        participants: participants_all,
                        participants_admin: participants_admin_v,
                        participants_guest: participants_guest_v,
                        title: 'Everyone Channel',
                        conv_img: 'feelix.jpg',
                        privacy: 'public',
                        single: 'no',
                        group: 'yes',
                        system_conversation: 'Everyone',
                        system_conversation_active: "No",
                        system_conversatio_is_active: [],
                        system_conversatio_send_sms: [],
                        company_id: companies[i].company_id,
                        team_id: '',
                        b_unit_id: list ? list.unit_id : '',
                        tag_list: [],
                        topic_type: null
                    });
                    // console.log(112, conv);
                    conv.save().then(async () => {
                        // console.log(375, "Conversation added successfully.");
                    }).catch((err) => {
                        console.log(377, "Conversation added not successfully.", err);
                    });

                    // }
                    resolve();
                });

            }

        } catch (e) {
            console.log(190, e);
        }

        console.log("Everyone Channel update script end.....")
    }
    if (vinfo.major < 6) {
        console.log("team update script runing.....")
        // Conversation.find({ system_conversation: 'team'}).remove().exec();
        // Conversation.find({ conversation_id: "23d324b0-9591-11ed-83cf-ada3cdc52558"}).remove().exec();
        database_update_req = true;
        try {
            var companies = await Company.find({}).lean();
            console.log(339, companies.length);
            for (let i = 0; i < companies.length; i++) {
                new Promise(async (resolve, reject) => {
                    let participants_all_team = [];
                    let participants_admin_team = [];
                    let team_title = '';
                    let team_id = '';
                    let participants_guest_v = [];
                    var allteams = await Team.find({ company_id: companies[i].company_id }).lean();
                    let list = await BusinessUnit.findOne({ company_id: companies[i].company_id });
                    // console.log(421, allteams.length);
                    for (let j = 0; j < allteams.length; j++) {
                        var convid = uuidv4();
                        var cur_time = new Date().getTime();
                        team_title = allteams[j].team_title;
                        participants_all_team = allteams[j].participants;
                        participants_admin_team = allteams[j].admin;
                        //console.log(445,team_title)
                        team_id = allteams[j].team_id;
                        var delete_con_team = await Conversation.find({ system_conversation: 'team_' + team_id }).remove().exec();
                        // console.log(448,delete_con_team);
                        const conv = new Conversation({
                            conversation_id: convid,
                            created_at: cur_time,
                            sender_id: companies[i].created_by,
                            created_by: companies[i].created_by,
                            participants: participants_all_team,
                            participants_admin: participants_admin_team,
                            participants_guest: participants_guest_v,
                            title: team_title,
                            conv_img: 'feelix.jpg',
                            privacy: 'public',
                            single: 'no',
                            group: 'yes',
                            system_conversation: 'team_' + team_id,
                            system_conversation_active: "No",
                            system_conversatio_is_active: [],
                            system_conversatio_send_sms: [],
                            company_id: companies[i].company_id,
                            team_id: companies[i].company_id,
                            b_unit_id: list ? list.unit_id : '',
                            tag_list: [],
                            topic_type: null
                        });
                        // console.log(112, conv);
                        conv.save().then(async () => {
                            // console.log(471,team_title+ "Conversation added successfully.");
                        }).catch((err) => {
                            console.log(454, "team Conversation added not successfully.", err);
                        });


                    }
                    const conv = new Conversation({
                        conversation_id: convid,
                        created_by: cur_time,
                        sender_id: companies[i].created_by,
                        participants: participants_all_team,
                        participants_admin: participants_admin_team,
                        participants_guest: participants_guest_v,
                        title: team_title,
                        conv_img: 'feelix.jpg',
                        privacy: 'public',
                        single: 'no',
                        group: 'yes',
                        system_conversation: 'team',
                        company_id: companies[i].company_id,
                        team_id: '',
                        b_unit_id: list ? list.unit_id : '',
                        tag_list: [],
                        topic_type: null
                    });
                    // console.log(112, conv);
                    conv.save().then(async () => {
                        // console.log(375, "Conversation added successfully.");
                    }).catch((err) => {
                        console.log(377, "Conversation added not successfully.", err);
                    });

                    // }
                    resolve();
                });

            }

        } catch (e) {
            console.log(190, e);
        }

        console.log("team update script end.....")
    }

    if(vinfo.major <= 7){
        database_update_req = true;
        Conversation.updateMany(
            { }, // Match all documents
            { $rename: { "group_keyspace": "team_id" } }, // Rename the field
            { multi: true } // Update multiple documents
          )
          .then(result => {
            console.log("group_keyspace => team_id field name updated successfully");
          })
          .catch(error => {
            console.error("group_keyspace => team_id error updating field name:", error);
        });
    }
    
    if(vinfo.major < 9){
        database_update_req = true;
        try{
            let c = await Conversation.find({}).lean();
            for(let r of c){
                if(r.group_keyspace && r.group_keyspace !== null && r.group_keyspace !== ''){
                    await Conversation.updateOne({_id: r._id}, {team_id: r.group_keyspace});
                    console.log(r.conversation_id, " update successfully ");
                }else{
                    console.log("no update need ");
                }
            }
            console.log("group_keyspace => team_id field copy done");
        }catch(e){
            console.error("group_keyspace => team_id copy field error:", e);
        }
        
    }

    if (database_update_req) {
        var v = new Version({
            build: 0,
            minor: 1,
            major: version_major,
            version: version_major + ".1.1"
        });
        v.save((err, info) => {
            if (err) {
                console.error(err);
            } else {
                console.log("Version info: ", info);
            }
        });
    }
}

setTimeout(database_update_req, 3000)