const isUuid = require('uuid-validate');
// var {models} = require('../config/db/express-cassandra');
const { v4: uuidv4 } = require('uuid');
const File = require('../mongo-models/file');
const Tag = require('../mongo-models/tag');
const Tagcount = require('../mongo-models/tagcount');

function get_one_tag(id){
    return new Promise((resolve, reject)=>{
        Tag.findOne({tag_id: id}).then(function(tag) {
            
            if(tag) resolve(tag);
            else reject(0);
        }).catch((e)=>{
            reject(e);
        });
    });
}

function get_all_tags(ids){
    return new Promise((resolve, reject)=>{
        Tag.find({tag_id: {$in: ids}}).then((tags)=>{
            if(tags) resolve(tags);
            else reject(0);
        }).catch((e)=>{
            if(e) reject(e);
        })
    });
}

function findAFile(data){
    return new Promise((resolve, reject)=>{
        File.findOne({company_id: data.company_id, id: data.file_id.toString()}).then(function(f) {

            if(f) resolve(f);
            else resolve({});
        }).catch((e)=>{
             console.log(256, e);
        });
    });
}

function tag_counting(data){
    // console.log(26, data);
    return new Promise(async (resolve)=>{
        if(data.val > 0){
            for(let i=0; i<data.participants.length; i++){
                let used_by = data.user_id.toString() === data.participants[i] ? 'me' : 'others';

                if(data.tag_type !== 'public' && data.user_id.toString() !== data.participants[i]) continue;
                if(data.file_ids.length>0){
                    for(let j=0; j<data.file_ids.length; j++){
                        var tc = new Tagcount({
                            company_id: data.company_id.toString(),
                            conversation_id: data.conversation_id.toString(),
                            msg_id: data.msg_id.toString(),
                            tag_id: data.tagid.toString(),
                            user_id: data.participants[i].toString(),
                            file_id: data.file_ids[j].toString(),
                            used_by: used_by
                        });
                        
                        tc.save().then({}).catch((e)=>{
                            console.log(47, e);
                        });
                    }
                }else{
                    var tc = new Tagcount({
                        company_id: data.company_id.toString(),
                        conversation_id: data.conversation_id.toString(),
                        msg_id: data.msg_id.toString(),
                        tag_id: data.tagid.toString(),
                        user_id: data.participants[i].toString(),
                        used_by: used_by
                    });
                    tc.save((err)=>{
                        if(err) console.log(47, err);
                    });
                }
            }
        }else{
            for(let i=0; i<data.participants.length; i++){
                let used_by = data.user_id.toString() === data.participants[i] ? 'me' : 'others';

                if(data.tag_type !== 'public' && data.user_id.toString() !== data.participants[i]) continue;
                for(let j=0; j<data.file_ids.length; j++){
                    var tc = {
                        company_id: data.company_id.toString(),
                        conversation_id: data.conversation_id.toString(),
                        msg_id: data.msg_id.toString(),
                        tag_id: data.tagid.toString(),
                        user_id: data.participants[i].toString(),
                        file_id: data.file_ids[j].toString(),
                        used_by: used_by
                    };
                    await new Promise((r)=>{
                        Tagcount.findOne(tc).then(async function(t) {
                            if(t){
                                await Tagcount.deleteOne({_id: t._id});
                            }
                            else console.log('no data found');
                            r();
                        }).catch((e)=>{
                             console.log(53, e);
                        });
                    });
                }
            }
        }
        
        console.log(44, 'Counter update successfully...');
        resolve();
    });
}

function check_update_untag(data){
    return new Promise(async (resolve)=>{
        if(Array.isArray(data.file_ids)){
            var untag = all_untags[data.company_id.toString()];
            for(let i=0; i<data.file_ids.length; i++){
                var fileinfo = await findAFile({company_id: data.company_id.toString(), file_id: data.file_ids[i]});
                fileinfo.tag_list = Array.isArray(fileinfo.tag_list) ? fileinfo.tag_list : [];
                
                if(fileinfo.tag_list.length >= 2 && fileinfo.tag_list.indexOf(untag.tag_id.toString()) > -1){
                    // remove untag, due to another tag added by user
                    var tag_list_with_user = [];
                    if(fileinfo.tag_list_with_user !== null){
                        var db_tag_list_with_user = JSON.parse(fileinfo.tag_list_with_user);
                        // console.log(97, db_tag_list_with_user);
                        for(let j = 0; j<db_tag_list_with_user.length; j++){
                            if([untag.tag_id.toString()].indexOf(db_tag_list_with_user[j].tag_id) > -1 && db_tag_list_with_user[j].created_by === data.user_id){
                                db_tag_list_with_user.splice(j, 1);
                                break;
                            }
                        }
                        tag_list_with_user = db_tag_list_with_user;
                    }
                    await new Promise((resolvei)=>{
                        File.updateOne({id: fileinfo.id, company_id: fileinfo.company_id}, {$pull: {tag_list: untag.tag_id.toString()}, tag_list_with_user: JSON.stringify(tag_list_with_user)}, async function(err){
                            if(err) console.log(108, err);
                            await update_tag_count({tag_id: untag.tag_id.toString(), company_id: data.company_id, val: -1, conversation_id: data.conversation_id, msg_id: data.msg_id, participants: data.participants, user_id: data.user_id, file_ids: [data.file_ids[i]], recursive: false });
                            resolvei();
                        });
                    });
                }
                else if(fileinfo.tag_list.length == 0){
                    // add untag, due to no tag found in this file
                    var tag_list_with_user = [];
                    tag_list_with_user.push({"tag_id": untag.tag_id.toString(), "created_by": data.user_id});
                    await new Promise((resolvei)=>{
                        File.updateOne({id: fileinfo.id, company_id: fileinfo.company_id}, {$push: {tag_list: untag.tag_id.toString()}, tag_list_with_user: JSON.stringify(tag_list_with_user)}, async function(err){
                            if(err) console.log(115, err);
                            await update_tag_count({tag_id: untag.tag_id.toString(), company_id: data.company_id, val: 1, conversation_id: data.conversation_id, msg_id: data.msg_id, participants: data.participants, user_id: data.user_id, file_ids: [data.file_ids[i]], recursive: false });
                        });
                        resolvei();
                    });
                }
            }
            console.log(102, "Untag check and update successfully...");
            resolve();
        }
        else resolve();
    });
}

/**
 * tag_id string
 * company_id string
 * val = 1/ -1
 * participants
 * user_id
 */
 function update_tag_count(data){
    return new Promise(async (resolve)=>{
        data.tag_id = Array.isArray(data.tag_id) ? data.tag_id : [data.tag_id];
        var queries = [];
        // var user_use_count = {};
        // var my_use_count = {};
        for(let i=0; i<data.tag_id.length; i++){
            if(data.tag_id[i] === null || data.tag_id[i] === undefined) continue;
            data.tagid = data.tag_id[i];
            try{
                var taginfo = await get_one_tag(data.tag_id[i].toString());
            }catch(e){
                console.log(153, e)
                continue;
            }
            data.tag_type = taginfo.tag_type;
            data.tag_title = taginfo.title;
            await tag_counting(data);
            // var upst = await Tag.updateOne({tag_id: data.tag_id[i].toString(), company_id: data.company_id.toString()}, {connected_user_ids, conversation_ids});
            // console.log(upst);
        }
        console.log("Tag use count update");
        // if(data.recursive === true){
        //     try{
        //         let untag_update = await check_update_untag(data);
        //     }catch(e){
        //         console.log(104, "untag problem", e);
        //     }
        // }
        resolve();
    });
}

function create_new_personal_tag(id, company_id){
    Tag.create({
        tag_id          : uuidv4(),
        tagged_by       : id.toString(),
        title           : 'PERSONAL FILES',
        type            : 'tag',
        tag_type        : 'private',
        tag_color       : '#e82a87',
        company_id      : company_id.toString(),
        shared_tag      : id.toString(),
        use_count       : 0
    }).then(function(pt){
       console.log(id.toString() , " personal file tag created successfully.");
    }).catch((e)=>{
      console.log("personal tag create error", e);
    });
}

function get_company_untag_id(comid){
    // return new Promise((resolve)=>{
    //     models.instance.Tag.findOne({title: 'UNTAGGED FILES', company_id: comid.toString()}, {raw: true, allow_filtering: true}, function(e, t){
    //         if(e) resolve(false);
    //         else resolve(t);
    //     })
    // })
}
module.exports = {get_one_tag, get_all_tags, update_tag_count, findAFile, create_new_personal_tag, get_company_untag_id };