var { sg_email } = require('./sg_email');
const isUuid = require('uuid-validate');
const Validator = require("validator");
var { models } = require('../config/db/express-cassandra');

function maketmdata(team_users, v, req, email_id, toname){
    var query = [];
    var return_data = [];
    var teammate_uid = [];

    if (team_users.find(e => e.invite_email === v) === undefined) {
        if (all_users[req.body.company_id][req.body.user_id].email != v) {
            // var toname = req.body.firstname ? req.body.firstname + ' ' + req.body.lastname : v.split("@")[0];
            var uid = models.timeuuid();
            var invite_user = new models.instance.Teammates({
                id: uid,
                created_by_id: req.body.user_id,
                created_by_name: req.body.name,
                company_id: req.body.company_id,
                company_name: req.body.company_name,
                invite_email: v,
                invite_name: toname,
                invite_role: req.body.role,
                invite_team: req.body.team_id ? [req.body.team_id] : [],
                invite_room: req.body.convid ? [req.body.convid.toString()] : []
            });
            email_id[v] = uid;
            return_data.push(invite_user);
            teammate_uid.push(uid);
            // 86400 sec * 30 = 2592000 sec = 1 month
            var invite_user_query = invite_user.save({ ttl: 2592000, return_query: true });
            query.push(invite_user_query);
        }
    } else {
        console.log(v, " already invited");
        if(req.body.team_id){
            for(let i=0; i<team_users.length; i++){
                if(team_users[i].invite_email === v){
                    query.push(models.instance.Teammates.update({company_id: team_users[i].company_id, created_by_id: team_users[i].created_by_id, id: team_users[i].id}, { invite_team: {$add: [req.body.team_id]}, invite_room: { $add: [req.body.convid.toString()] } }, { return_query: true } ));
                    
                    team_users[i].invite_team = [...team_users[i].invite_team, req.body.team_id];
                    team_users[i].invite_room = [...team_users[i].invite_room, req.body.convid.toString()];
                    
                    email_id[v] = team_users[i].id;
                    return_data.push(team_users[i]);
                    teammate_uid.push(team_users[i].id);
                    break;
                }
            }
        }
    }
    return ({query, return_data, teammate_uid});
}

function create_teammate(req){
    return new Promise((resolve, reject) => {
        var allquery = [];
        var allreturn_data = [];
        var email_id = {};
        var allteammate_uid = [];
        console.log(10, req.body);
        models.instance.Teammates.find({ company_id: req.body.company_id }, { raw: true, allow_filtering: true }, function(e, team_users) {
            if (e) reject("team user find error ", e);

            (req.body.emails).forEach(function(v, k) {
                console.log(66, typeof v);
                if(typeof v === "object"){
                    var toname = v.firstname ? v.firstname + ' ' + v.lastname : (v.email).split("@")[0];
                    var {query, return_data, teammate_uid} = maketmdata(team_users, v.email, req, email_id, toname);
                    console.log(70, {query, return_data, teammate_uid});
                    allquery.push(query[0]);
                    allreturn_data.push(return_data[0]);
                    allteammate_uid.push(teammate_uid[0]);
                }
                else{
                    var toname = req.body.firstname ? req.body.firstname + ' ' + req.body.lastname : v.split("@")[0];
                    var {query, return_data, teammate_uid} = maketmdata(team_users, v, req, email_id, toname);
                    console.log(78, {query, return_data, teammate_uid});
                    allquery.push(query[0]);
                    allreturn_data.push(return_data[0]);
                    allteammate_uid.push(teammate_uid[0]);
                }
                // if (team_users.find(e => e.invite_email === v) === undefined) {
                //     if (all_users[req.body.company_id][req.body.user_id].email != v) {
                //         var toname = req.body.firstname ? req.body.firstname + ' ' + req.body.lastname : v.split("@")[0];
                //         var uid = models.timeuuid();
                //         var invite_user = new models.instance.Teammates({
                //             id: uid,
                //             created_by_id: req.body.user_id,
                //             created_by_name: req.body.name,
                //             company_id: req.body.company_id,
                //             company_name: req.body.company_name,
                //             invite_email: v,
                //             invite_name: toname,
                //             invite_role: req.body.role,
                //             invite_team: req.body.team_id ? [req.body.team_id] : [],
                //             invite_room: req.body.convid ? [req.body.convid.toString()] : []
                //         });
                //         email_id[v] = uid;
                //         return_data.push(invite_user);
                //         teammate_uid.push(uid);
                //         // 86400 sec * 30 = 2592000 sec = 1 month
                //         var invite_user_query = invite_user.save({ ttl: 2592000, return_query: true });
                //         query.push(invite_user_query);
                //     }
                // } else {
                //     console.log(v, " already invited");
                //     if(req.body.team_id){
                //         for(let i=0; i<team_users.length; i++){
                //             if(team_users[i].invite_email === v){
                //                 query.push(models.instance.Teammates.update({company_id: team_users[i].company_id, created_by_id: team_users[i].created_by_id, id: team_users[i].id}, { invite_team: {$add: [req.body.team_id]}, invite_room: { $add: [req.body.convid.toString()] } }, { return_query: true } ));
                                
                //                 team_users[i].invite_team = [...team_users[i].invite_team, req.body.team_id];
                //                 team_users[i].invite_room = [...team_users[i].invite_room, req.body.convid.toString()];
                                
                //                 email_id[v] = team_users[i].id;
                //                 return_data.push(team_users[i]);
                //                 teammate_uid.push(team_users[i].id);
                //                 break;
                //             }
                //         }
                //     }
                // }
            });
            console.log(125, allquery);
            models.doBatch(allquery, function(err) {
                if (err) reject(err);
                else {
                    console.log({ status: true, msg: 'Save invite user info.' });
                    var success = 0;
                    var fail = 0;
                    var toname = req.body.firstname ? req.body.firstname + ' ' + req.body.lastname : "there";

                    var invitation_msg = req.body.msg ? '<br><br>' + req.body.name + ' sent you a message while inviting you: <br>' + req.body.msg : '';
                    var title = req.body.title ? req.body.title + ' room of ' : '';
                    (req.body.emails).forEach(function(v, k) {
                        var invite_user_email = '';
                        var passtxt = '';
                        if(typeof v === "object"){
                            invite_user_email = v.email;
                            toname = v.firstname;
                            passtxt = '<br><b>One-Time Password:</b> ' + v.code;
                        }
                        else
                            invite_user_email = v;
                        console.log(943, email_id[invite_user_email]);
                        if (toname == "there") { // Signup teammate or Guest user invite from room
                            var rolebasetext = req.body.role == 'Admin' ? req.body.name + ' has setup a new Workfreeli business account <b>' + invite_user_email + '</b> and has invited you as an administrator.' : req.body.name + ' has invited you as a guest to the '+ title + req.body.company_name +' company in Workfreeli.';
                            req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                            req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length-1) : req.headers.origin;
                            var emaildata = {
                                to: invite_user_email,
                                subject: req.body.name + ' invited you to a Workfreeli room',
                                text: req.body.name + ' invited you to a Workfreeli room',
                                html: 'Hi ' + toname + '!<br><br>' + rolebasetext + '<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. As a guest, you can send messages, share files, and join calls in the room you\'re invited to.<br><br>To get started, please find your access details below to sign into your guest account.<br><br><b>Username:</b> ' + invite_user_email + passtxt + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + email_id[invite_user_email] + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                            }
                        } else { // Admin invite teammate
                            var rolebasetext = req.body.role == 'Admin' ? req.body.name + ' has invited you to join a new business account on Workfreeli <b>' + invite_user_email + '</b> and has invited you as an administrator.' : req.body.name + ' has invited you to join a new business account on Workfreeli - <b>' + req.body.company_name + '</b>.';
                            req.headers.origin = req.headers.origin ? req.headers.origin + '/' : process.env.CLIENT_BASE_URL;
                            req.headers.origin = req.headers.origin.slice(-2) === '//' ? req.headers.origin.substring(0, req.headers.origin.length-1) : req.headers.origin;
                            var emaildata = {
                                to: invite_user_email,
                                subject: req.body.name + ' invited you to join Workfreeli',
                                text: req.body.name + ' invited you to join Workfreeli',
                                html: 'Hi ' + toname + '!<br><br>' + rolebasetext + '<br><br>Workfreeli is a team collaboration platform that simplifies the way we work. Workfreeli combines many features and functions that today\'s teams rely on. It is a single platform that combines chat, calls, file, task management and much more. It is a point-to-point encrypted business solution with many new features on the way.' + invitation_msg + '<br><br>To get started, please find your access details below to sign into your account:<br><br><b>Username: ' + invite_user_email + '<br><br><a clicktracking=off href="' + req.headers.origin + 'teammate/' + email_id[invite_user_email] + '" target="_blank" style="text-decoration: none;color: white;background: #002e98;padding: 8px 12px;font-weight: 300;font-size: 14px;">Sign In</a><br><br>Cheers!<br><br><img src="https://wfss001.freeli.io/common/workfreeli_email_logo.png"><br><br>WORKFREELI Team<br>W: workfreeli.com<br>E: support@workfreeli.com'
                            }
                        }

                        try {
                            sg_email.send(emaildata, (result) => {
                                if (result.msg == 'success') {
                                    success++;
                                } else {
                                    fail++;
                                }
                            });
                        } catch (e) {
                            console.log(e);
                            fail++;
                        }
                    });
                    console.log(82, { status: true, success, fail, teammate_uid: allteammate_uid, message: 'Workfreeli teammate invitation send' });
                    resolve({ status: true, teammates: allreturn_data, success, fail, teammate_uid: allteammate_uid, message: 'Workfreeli teammate invitation send' });
                }
            });
        });
    });
}

module.exports = {
    create_teammate
}