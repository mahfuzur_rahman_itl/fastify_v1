const Project = require('../mongo-models/project');
const Task = require('../mongo-models/task');
const Task_keywords = require('../mongo-models/task_keywords');
const Checklist = require('../mongo-models/task_checklist');
const Item = require('../mongo-models/checklist_item');
const Review = require('../mongo-models/task_review');

const {isValidJson, validateJSON} = require('./common');


function get_unread_tasks(data){
    return new Promise(async resolve =>{
        let tasks = await Task.find({view_status: {$in: data.id}}).lean();
        resolve(tasks.length || 0);
    })
}

function get_project(data){
    return new Promise(async (resolve)=>{
        let query = {};
        if(data._id) query._id = data._id;  
        if(data.project_title) query.project_title = new RegExp("\\b" + data.project_title + "\\b", "i");
        if(data.company_id) query.company_id = data.company_id;
        
        const projects = await Project.find(query).sort({created_at: -1}).lean();
        if(projects)
            resolve({success: true, projects});
        else
            resolve({success: false, projects: []});
    });
}

function set_project(data){
    return new Promise(async (resolve)=>{
        let check_data = isValidJson(JSON.stringify(data));
        const requiredKeys = ["project_title"];
        let missingKeys = validateJSON(data, requiredKeys)
        if (missingKeys.length === 0) {
            if(check_data){
                Project.findOne({project_title: data.project_title, company_id: data.company_id}).then(p =>{
                    if(p)
                        // res.status(200).json({success: true, project: p, msg: "Already exists"});
                        resolve({success: true, project: p, msg: "Already exists"});
                    else{
                        Project.create(data, (err, project)=>{
                            if(err) resolve({success: false, error: err});
                            else resolve({success: true, project, msg: "Create project successfully."});
                        });
                    }
                });
            }
            else resolve({success: false, error: "Data is not valid"});
        }
        else resolve({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    });
}

function get_keywords(data){
    return new Promise(async (resolve)=>{
        let query = {};
        if(data._id)
            query._id = data._id;
        else if(data.company_id)
            query.company_id = data.company_id;
        const task_keywords = await Task_keywords.find(query).lean();
        if(task_keywords)
            resolve({success: true, task_keywords});
        else
            resolve({success: false, task_keywords: []});
    });
}

function set_keywords(data){
    return new Promise(async (resolve)=>{
        let check_data = isValidJson(JSON.stringify(data));
        const requiredKeys = ["keywords_title"];
        let missingKeys = validateJSON(data,requiredKeys)
        if (missingKeys.length === 0) {
            if(check_data){
                Task_keywords.findOne({keywords_title: data.keywords_title, company_id: data.company_id }).then(async (tk)=>{
                    if(tk){
                        resolve({success: true, task_keywords: tk, msg: "Already exists"});
                    }else{
                        let task_ids = [];
                        if(data.task_id){
                            task_ids = [data.task_id];
                            delete data.task_id;
                        }
                        Task_keywords.create(data, (err, tk)=>{
                            if(err) resolve({success: false, error: err});
                            else resolve({success: true, task_keywords: tk, msg: "Create keyword successfully."});
                        });
                    }
                });
            }
            else resolve({success: false, error: "Data is not valid"});
        }
        else resolve({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    });
}

function set_task_into_keyword(data){
    return new Promise(async (resolve)=>{
        for(let i=0; i<data.task_keywords.length; i++){
            await Task_keywords.findOneAndUpdate({keywords_title: data.task_keywords[i]}, {$push: {task_ids: data.task_id} });
        }
        resolve({success: true, msg: "Keyword update successfully..."})
    });
}

function set_task_into_review(data){
    return new Promise(async (resolve) =>{
        try{
            Review.findOne({task_id: data.task_id, created_for: data.created_for}, async function(err, doc){
                // Review.findOneAndDelete({task_id: data.task_id, created_by: data.created_by, created_for: data.created_for}, async function(err, doc){
                if(err) throw err;

                if(doc && doc.rate > 0)
                    data.history = [...doc.history, {date: doc.last_updated_at, rate: doc.rate, review_body: doc.review_body}];
                let reviewInstant = new Review(data);
                await reviewInstant.save();
                resolve({success: true, review: reviewInstant});
            });
        }catch(e){
            console.log('Review set error: ', e);
            resolve({success: false});
        }
    });
}

function remove_task_review(data){
    return new Promise(async (resolve) =>{
        try{
            Review.deleteMany({ task_id: data.task_id }, function(err, doc) {
                if (err) throw err;
              
                if (!doc) {
                    console.log('No document found');
                    resolve({success: false});
                }
                console.log('Deleted document:', doc);
                resolve({success: true});
            });
        }catch(e){
            console.log('Review remove error: ', e);
            resolve({success: false});
        }
    });
}

function set_checklist_item(data){
    return new Promise(async (resolve)=>{
        const requiredKeys = ["task_id", "checklist_id", "item_title", "company_id"];
        let missingKeys = validateJSON(data, requiredKeys);
        if (missingKeys.length === 0) {
            try{
                if(data.hasOwnProperty('participants')) delete data.participants;
                if(data._id){
                    if(data.hasOwnProperty('created_by')) delete data.created_by;
                    data.last_updated_at = Date.now();
                    Item.findOneAndUpdate({_id: data._id}, data, {new: true}, (err, updateData) =>{
                        if(err) throw err;
                        resolve({success: true, item: updateData, is_new: false});
                    });
                }else{
                    let newChecklistItem = new Item(data);
                    newChecklistItem.save((err, savedItem) => {
                        if (err) throw err;
                        // console.log('Item saved:', savedItem);
                        resolve({success: true, item: savedItem, is_new: true});
                    });
                }
            }catch(error){
                // if (error instanceof mongoose.Error.StrictModeError) {
                //     console.log('Validation error:', error.message);
                // } else {
                //     console.log('Unexpected error:', error.message);
                // }
                resolve({success: false, error});
            }
        }
        else resolve({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    });
}

function set_checklist(data){
    return new Promise(async (resolve)=>{
        const requiredKeys = ["task_id", "checklist_title"];
        let missingKeys = validateJSON(data.checklist, requiredKeys);
        if (missingKeys.length === 0) {
            try{
                data.checklist.company_id = data.company_id;
                data.checklist.created_by = data.created_by;
                let newChecklist = new Checklist(data.checklist);
                newChecklist.save(async (err, savedChecklist) => {
                    if (err) throw err;
                    // console.log('Item saved:', savedChecklist);
                    Task.updateOne({_id: savedChecklist.task_id}, {checklist_id: savedChecklist._id}, function(e){
                        console.log(e? 'Error : '+e : 'Task update successfully by checklist id');
                    })
                    let items = [];
                    for(let i=0; i<data.items.length; i++){
                        let item = {};
                        item.company_id = data.checklist.company_id;
                        item.created_by = data.checklist.created_by;
                        item.task_id = data.checklist.task_id;
                        item.checklist_id = savedChecklist._id;
                        item.item_title = data.items[i].item_title;
                        let is = await set_checklist_item(item);

                        if(is.success && is.item._id){
                            // let uobj = await get_user_obj(is.item.created_by);
                            // is.item.created_by_name = uobj.firstname + ' ' + uobj.lastname;
                            items.push(is.item);
                        }
                            
                    }
                    resolve({success: true, checklist: savedChecklist, items})
                });
            }catch(error){
                if (error instanceof mongoose.Error.StrictModeError) {
                    console.log('Validation error:', error.message);
                } else {
                    console.log('Unexpected error:', error.message);
                }
                resolve({success: false, error});
            }
        }
        else resolve({ success: false, error: `The JSON object is missing the following required keys: ${missingKeys.join(", ")}`});
    });
}

function item_to_checklist(data){
    return new Promise(async (resolve)=>{
        try{
            let checklist  = {
                company_id: data.company_id,
                created_by: data.created_by,
                task_id: data.task_id,
                checklist_title: data.task_title
            };
            let newChecklist = new Checklist(checklist);
            newChecklist.save(async (err, savedChecklist) => {
                if (err) throw err;
                // console.log('Checklist saved:', savedChecklist);
                Task.updateOne({_id: savedChecklist.task_id}, {checklist_id: savedChecklist._id}, function(e){
                    console.log(e? 'Error : '+e : 'Task update successfully by checklist id');
                })
                let items = [];
                for(let i=0; i<data.items.length; i++){
                    let item = {};
                    item.company_id = data.company_id;
                    item.created_by = data.created_by;
                    item.task_id = data.task_id;
                    item.checklist_id = savedChecklist._id;
                    item.item_title = data.items[i].item_title;
                    item.status = data.items[i].status ? data.items[i].status : 'incomplete';
                    let is = await set_checklist_item(item);
                    if(is.success && is.item._id)
                        items.push(is.item);
                }
                resolve({success: true, checklist: savedChecklist, items})
            });
        }catch(error){
            if (error instanceof mongoose.Error.StrictModeError) {
                console.log('Validation error:', error.message);
            } else {
                console.log('Unexpected error:', error.message);
            }
            resolve({success: false, error});
        }
    });
}

function delete_checklist_item(data){
    return new Promise(async (resolve)=>{
        Item.findOneAndDelete(data, function(err, doc){
            if(err) {
                console.log(err);
                resolve({success: false, message: err});
            }
            resolve({success: true, item: doc});
        })
    });
}

module.exports = {get_unread_tasks, get_project, set_project, get_keywords, set_keywords, set_task_into_keyword, set_task_into_review, remove_task_review, set_checklist_item, set_checklist, item_to_checklist, delete_checklist_item };