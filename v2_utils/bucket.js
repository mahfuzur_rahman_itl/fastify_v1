var Minio = require('minio');
// var {models} = require('../config/db/express-cassandra');
const isUuid = require('uuid-validate');
var File = require('../mongo-models/file');

var minioClient = new Minio.Client({
    endPoint: process.env.FILE_SERVER_ENDPOINT,
    port: 9000,
    useSSL: false,
    accessKey: process.env.FILE_SERVER_ACCESS_KEY,
    secretKey: process.env.FILE_SERVER_SECRET_KEY
});

function createBucket(bucket_name) { 
    
    console.log(24, bucket_name);
    
    var all_error = []; 
    var bucketPolicy = `{
                          "Version": "2012-10-17",
                          "Statement": [
                            {
                              "Action": "s3:GetObject",
                              "Effect": "Allow",
                              "Principal": {"AWS": "*"},
                              "Resource": ["arn:aws:s3:::${bucket_name}/*"],
                              "Sid": "Public"
                            }
                          ]
                        }`;
    minioClient.bucketExists(bucket_name, function(err1, exists) {
        if (err1) console.log({ status: false, error: err1 });
        else {
            if (exists) {
                minioClient.setBucketPolicy(bucket_name, bucketPolicy, function(err3) {
                    if (err3) all_error.push(err3);
                    // console.log('Bucket policy set');
                });
                console.log({ status: true, bucket_name, msg: 'User has bucket.' });
            } else {
                minioClient.makeBucket(bucket_name, 'us-east-1', function(err4) {
                    if (err4) console.log({ status: false, error: err4 });
                    else {
                        minioClient.setBucketPolicy(bucket_name, bucketPolicy, function(err5) {
                            if (err5) all_error.push(err5);
                            // console.log('Bucket policy set');
                        });
                        console.log({ status: true, bucket_name: bucket_name, msg: 'Bucket created successfully' });
                    }
                });
            }
        }
    });
}

function bucket_size(data){
    return new Promise((resolve,reject)=>{
        var total = 0;
        var q = {};
        if(data.company_id) q.company_id = data.company_id;
        if(data.id && data.type === 'user') q.user_id = data.user_id;
        const stream = File.find(q).stream();
        stream.on('data', (reader) => {
            var row;
            while (row = reader.readRow()) {
                total+= Number(row.file_size);
            }
        });
        stream.on('error', (err) => {
            reject(err);
        });
        stream.on('end', () => {
            resolve(total>0 ? (total/1024/1024/1024).toFixed(2) : 0);
        });
    });
}


module.exports = {createBucket, bucket_size};