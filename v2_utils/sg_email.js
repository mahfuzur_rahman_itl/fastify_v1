const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

var sg_email = [];

sg_email["send"] = (data, callback) =>{
  console.log("Sendgrid call for sending mail to : "+data.to);
  data.from = 'no-reply@workfreeli.com';
  // var tophtml = '<!DOCTYPE html><html><head></head><body><br><br><br><div style="width: 700px;height: auto;background-color: #FFF;position: absolute;top:100;bottom: 0;left: 0;right: 0;margin: auto;border:1px solid #000;color:black;font-size: 18px;"><p style="background-color:#023d67; text-align: center;margin: 0px;"><img src="https://wfss001.freeli.io/common/Workfreeli_logo_full_connect.png"></p><div style="padding:10px;">';
  // var foothtml = '</div></div></body></html>';
  // data.html = tophtml + data.html + foothtml;

  var tophtml = '<!DOCTYPE html><html><head></head><body style="background-color: #FFF;"><br><br><br><div style="width: 730px;height: auto;background-color: #FFF;position: absolute;top:100;bottom: 0;left: 0;right: 0;margin: auto;color:black;font-size: 18px;"><div style="padding:10px;">';
  var foothtml = '</div></div></body></html>';
  data.html = tophtml + data.html + foothtml;

  try{
    if(data.to === 'anwar@ohs.global' && (process.env.CLIENT_BASE_URL === 'https://cacdn01.freeli.io/' || process.env.CLIENT_BASE_URL === 'http://localhost:5000/')){
      console.log('Email sent off')
      callback({msg:'success'});
    }else{
      sgMail.send(data).then(() => {
        console.log('Email sent')
        // console.log('Email sent off')
        callback({msg:'success'});
      }).catch((error) => {
          console.log(error)
          callback({msg:'error'});
      });
    }
  }catch(e){
    console.log("send grid error" ,e);
    callback({msg:'error'});
  }
}

module.exports = {sg_email};