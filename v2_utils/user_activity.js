const isUuid = require('uuid-validate');
const Useractivity = require('../mongo-models/useractivity');

async function save_activity(post){
    if(post.company_id.toString() && post.user_id.toString()){
        var data = {};
        data.company_id = post.company_id.toString();
        data.created_by_id = post.user_id.toString();
        data.title = post.title ? post.title : 'Activity saved';
        data.type = post.type ? post.type : 'danger';
        data.body = post.body ? post.body : '';
        // data.created_by_name = post.user_name ? post.user_name : 'System user';
        // data.created_by_img = post.user_img ? post.user_img : 'feelix.jpg';
        
        await new Useractivity(data).save();
    }
    else{
        console.log(new Date().toLocaleString() + " Activity not save, due to user id or company id missing.")
    }
}

module.exports = {save_activity};