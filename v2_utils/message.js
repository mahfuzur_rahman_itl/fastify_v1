const isUuid = require('uuid-validate');
const _ = require('lodash');
var CryptoJS = require("crypto-js");
var moment = require('moment');

// var { models } = require('../config/db/express-cassandra');
var { get_msgs_checklist } = require('./message_checklist');
var { xmpp_send_server, send_msg_firebase } = require('./voip_util');
var { update_tag_count, get_all_tags } = require('./tag');
var { sendMsgUpdateIntoZnode, znMsgUpdate, znReadMsgUpdateCounter } = require('./zookeeper');

var Messages = require('../mongo-models/message');
var MessagesEmojis = require('../mongo-models/message_emoji');
var Users = require('../mongo-models/user');
var Conversation = require('../mongo-models/conversation');
var File = require('../mongo-models/file');
var Link = require('../mongo-models/link');
var Tag = require('../mongo-models/tag');
var Tagcount = require('../mongo-models/tagcount');
var Team = require('../mongo-models/team');
// var TaskFiles = require('../mongo-models/task_files');
const Task_discussion = require('../mongo-models/task_discussion');
var ObjectId = require('mongoose').Types.ObjectId;

const { v4: uuidv4 } = require('uuid');

// process.env.CRYPTO_SECRET = 'D1583ED51EEB8E58F2D3317F4839A';

function parseJSONSafely(str) {
    // console.log(30, typeof str)
    if(str === null || str === 'null') return [];
    try {
       return JSON.parse(str);
    }
    catch (e) {
       console.log("Error in JSON parse");
       if(typeof str == 'string') return [str];
       else return [];
    }
}

function parseMuteSafely(str) {
    if (str === null || str === 'null') return [];
    try {
        if (str.startsWith('[') && str.endsWith(']')) {
            return JSON.parse(str);
        } else {
            return JSON.parse('['+ str +']');
        }
    } catch (e) {
        console.log("Error in JSON parse");
        if (typeof str == 'string') return [str];
        else return [];
    }
}

function fnln(data){
    let str = "";
    if(data.firstname && data.firstname != ""){
        str += (data.firstname).charAt(0);
    }
    if(data.lastname && data.lastname != ""){
        str += (data.lastname).charAt(0);
    }
    return str;
}

async function get_user_obj(id) {
    return new Promise((resolve,reject)=>{
        for (let value of Object.entries(all_users)) {
            for (let v of value) {
                if (typeof v === 'object' && v.hasOwnProperty(id)) {
                    return resolve(v[id]);
                }
            }
        }
        if(cache_user){
            return resolve({
                id: id,
                firstname: "",
                lastname: "",
                fullname: "",
                img: "",
                dept: "",
                designation: "",
                email: "",
                phone: "",
                access: [],
                company_id: "",
                company_name: "",
                conference_id: "",
                role: "",
                email_otp: "",
                is_active: 0,
                mute_all: "",
                mute: "",
                login_total: 0,
                fnln: "",
                createdat: ""
            });
        };  
        // console.log('get_user_obj',id)
        Users.findOne({ id: String(id) }).then(function (user) {
            if(user){
                return resolve({
                    id: user.id,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    fullname: user.firstname + ' ' + user.lastname,
                    img: process.env.FILE_SERVER + user.img,
                    dept: user.dept,
                    designation: user.designation,
                    email: user.email,
                    phone : Array.isArray(user.phone) ? user.phone.join("") : "",
                    access: user.access,
                    company_id:user.company_id,
                    company_name: user.company,
                    conference_id: user.conference_id,
                    role: ((user.role !== null) && (user.role !== '')) ? user.role === 'User' ? 'Member' : user.role : 'Member',
                    email_otp: user.email_otp,
                    is_active: user.is_active,
                    mute_all: user.mute_all,
                    mute: user.mute ? JSON.parse(user.mute) : '',
                    login_total: user.login_total,
                    fnln: user.firstname.charAt(0) + user.lastname.charAt(0),
                    device: user.device ? user.device : [],
                    short_id: user.short_id,
                    createdat: user.createdat ? user.createdat : new Date(),
                    fcm_id: user.fcm_id
                });

            }else{
                return resolve({
                    id: id,
                    firstname: "",
                    lastname: "",
                    fullname: "",
                    img: "",
                    dept: "",
                    designation: "",
                    email: "",
                    phone: "",
                    access: [],
                    company_id: "",
                    company_name: "",
                    conference_id: "",
                    role: "",
                    email_otp: "",
                    is_active: 0,
                    mute_all: "",
                    mute: "",
                    login_total: 0,
                    fnln: "",
                    createdat: ""
                });
            }
            
        }).catch((e)=>{
            
        });

    })
}


function get_conversation(arg){
    // console.log(36, arg);
    // arg[conversation_id, user_id, company_id]
    return new Promise(async (resolve)=>{
        var query = {};
        if(isUuid(arg.conversation_id))
            query.conversation_id = arg.conversation_id;
        else if(Array.isArray(arg.conversation_id) && arg.conversation_id.length > 0)
            query.conversation_id = {$in: arg.conversation_id};
        else if(arg.archive === 'yes' && isUuid(arg.user_id)){
            query = {
                $or: [{participants: {$in: [arg.user_id]}}, {temp_user: {$in: [arg.user_id]}}], 
                $and: [{archive: 'yes'}]
            };
        }
        else if(isUuid(arg.user_id)){
            query = {
                $or: [{participants: {$in: [arg.user_id]}}, {temp_user: {$in: [arg.user_id]}}], 
                $and: [{status: 'active'}]
            };
        }
        let project = {"_id": 1, "id": 1,"conversation_id": 1, "company_id": 1};
        if(!arg.select || arg.select === "*") {
            project = {
                "_id": 1,
                "id": 1,
                "conversation_id": 1,
                "created_by": 1,
                "participants": 1,
                "participants_admin": 1,
                "participants_guest": 1,
                "group": 1,
                "team_id": 1,
                "team_id_name": {
                    $map: {
                        input: '$team_id_info',
                        as: 'team_id_info',
                        in: '$$team_id_info.team_title'
                    }
                },
                "privacy": 1,
                "archive": 1,
                "is_active": 1,
                "status": 1,
                "close_for": 1,
                "conv_img": 1,
                "is_pinned_users": 1,
                "b_unit_id": 1,
                "b_unit_id_name": {
                    $map: {
                        input: '$b_unit_id_info',
                        as: 'b_unit_id_info',
                        in: '$$b_unit_id_info.unit_name'
                    }
                },
                "room_id": 1,
                "created_at": 1,
                "is_busy": 1,
                "company_id": 1,
                "last_msg": 1,
                "last_msg_time": 1,
                "sender_id": 1,
                "tag_list": 1,
                "msg_status": 1,
                "conference_id": 1,
                "root_conv_id": 1,
                "reset_id": 1,
                "pin": 1,
                "has_mute": 1,
                "mute": 1,
                "short_id": 1,
                "title": 1,
                "system_conversation": 1,
                "system_conversation_active": 1,
                
            };
        }
        project.system_conversation_active = 1;
        project.system_conversation = 1;
        if(arg.select && arg.select.indexOf('created_by') > -1) project.created_by = 1;
        if(arg.select && arg.select.indexOf('participants') > -1) project.participants = 1;
        if(arg.select && arg.select.indexOf('participants_admin') > -1) project.participants_admin = 1;
        if(arg.select && arg.select.indexOf('participants_guest') > -1) project.participants_guest = 1;
        if(arg.select && arg.select.indexOf('group') > -1) project.group = 1;
        if(arg.select && arg.select.indexOf('team_id') > -1) project.team_id = 1;
        if(arg.select && arg.select.indexOf('privacy') > -1) project.privacy = 1;
        if(arg.select && arg.select.indexOf('archive') > -1) project.archive = 1;
        if(arg.select && arg.select.indexOf('is_active') > -1) project.is_active = 1;
        if(arg.select && arg.select.indexOf('status') > -1) project.status = 1;
        if(arg.select && arg.select.indexOf('close_for') > -1) project.close_for = 1;
        if(arg.select && arg.select.indexOf('conv_img') > -1) project.conv_img = 1;
        if(arg.select && arg.select.indexOf('is_pinned_users') > -1) project.is_pinned_users = 1;
        if(arg.select && arg.select.indexOf('b_unit_id') > -1) project.b_unit_id = 1;
        if(arg.select && arg.select.indexOf('room_id') > -1) project.room_id = 1;
        if(arg.select && arg.select.indexOf('is_busy') > -1) project.is_busy = 1;
        if(arg.select && arg.select.indexOf('last_msg') > -1) project.last_msg = 1;
        if(arg.select && arg.select.indexOf('last_msg_time') > -1) project.last_msg_time = 1;
        if(arg.select && arg.select.indexOf('sender_id') > -1) project.sender_id = 1;
        if(arg.select && arg.select.indexOf('tag_list') > -1) project.tag_list = 1;
        if(arg.select && arg.select.indexOf('msg_status') > -1) project.msg_status = 1;
        if(arg.select && arg.select.indexOf('conference_id') > -1) project.conference_id = 1;
        if(arg.select && arg.select.indexOf('root_conv_id') > -1) project.root_conv_id = 1;
        if(arg.select && arg.select.indexOf('reset_id') > -1) project.reset_id = 1;
        if(arg.select && arg.select.indexOf('pin') > -1) project.pin = 1;
        if(arg.select && arg.select.indexOf('has_mute') > -1) project.has_mute = 1;
        if(arg.select && arg.select.indexOf('mute') > -1) project.mute = 1;
        if(arg.select && arg.select.indexOf('short_id') > -1) project.short_id = 1;
        if(arg.select && arg.select.indexOf('title') > -1) project.title = 1;
        let conv_arg = [
            {$match: query},
            {$lookup:{ 
                from: 'business_units', 
                localField: 'b_unit_id', 
                foreignField:'unit_id',
                as:'b_unit_id_info' 
            }},
            {$lookup:{ 
                from: 'teams', 
                localField: 'team_id', 
                foreignField:'team_id',
                as:'team_id_info' 
            }}];
        if(arg.select && arg.select.indexOf('team_id_name') > -1) {
            project.team_id_name  = {
                $map: {
                    input: '$team_id_info',
                    as: 'team_id_info',
                    in: '$$team_id_info.team_title'
                }
            };
            // conv_arg.push({$lookup:{ 
            //     from: 'teams', 
            //     localField: 'team_id', 
            //     foreignField:'team_id',
            //     as:'team_id_info' 
            // }});
        }
        if(arg.select && arg.select.indexOf('b_unit_id_name') > -1) {
            project.b_unit_id_name = {
                $map: {
                    input: '$b_unit_id_info',
                    as: 'b_unit_id_info',
                    in: '$$b_unit_id_info.unit_name'
                }
            };
            // conv_arg.push({$lookup:{ 
            //     from: 'business_units', 
            //     localField: 'b_unit_id', 
            //     foreignField:'unit_id',
            //     as:'b_unit_id_info' 
            // }});
        }
        var convs = await Conversation.aggregate([...conv_arg,
            {$sort: { last_msg_time: -1, last_update_time: -1, created_at: -1 } },
            { "$project": project }
        ]).exec();
        
        let conversations = [];
        if(convs.length>0){
            if(convs.length === 1 && convs[0].tag_list.length){
                let taginfo = await get_all_tags(convs[0].tag_list);
                convs[0].tag_list_details = [];
                for(let t of taginfo){
                    convs[0].tag_list_details.push({
                        tag_id: t.tag_id,
                        tagged_by: t.tagged_by,
                        title: t.title,
                        tag_type: t.tag_type,
                        tag_color: t.tag_color
                    });
                }
            }
            // let allusers = await Users.find({company_id: convs[0].company_id}).select('id email firstname lastname is_active img role createdat').exec();
                
            for(let key in convs){
                let conv = convs[key];
                if(conv.has_mute && conv.has_mute.indexOf(arg.user_id) > -1){
                    for(let m of conv.mute){
                        let mute_data = parseMuteSafely(m);
                        for(let n=0; n<mute_data.length; n++){
                            if (mute_data[n].mute_by == arg.user_id) {
                                conv.mute = [mute_data[n]];
                            }
                        }
                    }
                }else{
                    conv.mute = [];
                }
                /* conv.participants_details = [];
                if(arg.select === undefined || (arg.select && arg.select.indexOf('participants_details')>-1)){
                    for(let i=0;i<conv.participants.length; i++){
                        let this_user = await get_user_obj(conv.participants[i]);
                        if(this_user.firstname !== '' && this_user.firstname !== undefined){
                            conv.participants_details.push({
                                id: this_user.id,
                                email: this_user.email,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                is_active: this_user.is_active,
                                img: this_user.img,
                                role: this_user.role,
                                fnln: this_user.fnln,
                                createdat: this_user.createdat
                            });
                        }
                    }
                } */
                conv.friend_id = "";
                conv.conv_is_active = 1;
                conv.close_for = conv.close_for.indexOf('yes') > -1 ? 'yes' : 'no';
                conv.temp_user = [];
                // Direct conversation
                if(conv.group === 'no'){
                    if(conv.conversation_id === arg.user_id)
                        var fid = conv.conversation_id;
                    else
                        var fid = conv.participants.join().replace(arg.user_id, '').replace(',', '');
                    let this_user = await get_user_obj(fid);
                    if(this_user.firstname !== '' && this_user.firstname !== undefined){
                        conv.title = this_user.fullname? this_user.fullname: (this_user.firstname + ' ' + this_user.lastname).trim();
                        conv.conv_img = this_user.img;
                        conv.friend_id = fid;
                        conv.fnln = fnln(this_user);
                        conv.conv_is_active = this_user.is_active;
                    }
                    else continue;
                }
                // Group conversation
                else{
                    conv.conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + conv.conv_img;
                }
               // console.log(392,conv);
                if(conv.system_conversation === 'No'){  
                    
                  //  console.log(397,conv.team_id_name)
                   // conv.team_id = all_company[conv.company_id].company_name;

                }else{
                    conv.team_id_name.push(all_company[conv.company_id].company_name);
                    if(conv.system_conversation === 'Everyone'){

                    }else{
                        console.log(conv.conversation_id)
                        if(conv.system_conversation_active.toLowerCase() === 'no'){
                            console.log(406,conv.system_conversation_active) 
                            continue; 
                        }else{
    
                        }
                    }
                    
                    //else continue;
                  //  console.log(401,conv.team_id_name)  

                }
                
                conversations.push(conv);
            }
        }
            
        // console.log(356, Date.now() - ct);
        resolve(conversations);
    });
}

// convid='', msgid='', limit='', all_users='', type='', is_reply = 'no', has_reply='', user_id
function get_message(arg, req = null) {
    return new Promise( async (resolve, reject) => {
        const PAGE_SIZE = 20;
        const page = Number(arg.page) || 1;
        var convid = arg.convid ? arg.convid : '';
        var msgid = arg.msgid ? arg.msgid : '';
        var limit = arg.limit ? arg.limit : '';
        var type = arg.type ? arg.type : '';
        var is_reply = arg.is_reply ? arg.is_reply : 'no';
        var has_reply = arg.has_reply ? arg.has_reply : '';
        var uid = arg.user_id ? arg.user_id : '';

        // console.log(11, convid, msgid, limit, type, is_reply, has_reply, uid);
        var query = {};
        if (is_reply != '')
            query.is_reply_msg = is_reply;
        if (isUuid(convid.toString())) {
            query.conversation_id = convid.toString();
        }
        if(type === 'one_msg' && isUuid(msgid)){
            query.msg_id = msgid;
        }
        // if (limit != "" && !isNaN(limit)) {
        //     limit = limit > 1 ? 100 : 1;
        //     query.$limit = limit;
        // }
        if (isUuid(msgid) && type == "") {
            query.msg_id = msgid;
        }
        // if (isUuid(msgid) && type == "old") {
        //     query.msg_id = { $lt: models.timeuuidFromString(msgid) };
        // }
        // if (isUuid(msgid) && type == "new") {
        //     limit = 110;
        //     query.$limit = 110;
        //     // query.msg_id = { $gt: models.timeuuidFromString(msgid) };
        //     // query.$orderby = { '$desc': 'msg_id' };
        // }
        // if (has_reply != '' && has_reply == 'yes') {
        //     query.has_reply = { $gt: 0 };
        // }
        if(type !== 'one_msg'){
            query.has_hide = { $nin: [uid.toString()] };
            query.has_delete = { $nin: [uid.toString()] };
            query.$or = [ 
                    {
                        $and: [
                            { is_secret: true }, 
                            { secret_user: { $in: [uid.toString()] }} 
                        ]
                    },
                    { is_secret: false } 
            ];
        }
        // console.log(56, limit, query);
        const count_total = [{$match: query}, {$count: "total"}];
        
        let project = {"_id": 1, "msg_id": 1,"conversation_id": 1, "created_at": 1, "company_id": 1, "sender": 1, "msg_body": 1, "msg_text": 1, "msg_type": 1, "msg_status": 1, "attch_imgfile": 1, "attch_audiofile": 1, "attch_videofile": 1, "attch_otherfile": 1, "has_flagged": 1, "has_emoji": 1, "has_tag_text": 1, "has_delivered": 1, "reply_for_msgid": 1, "has_reply": 1, "has_reply_attach": 1, "last_reply_name": 1, "is_reply_msg": 1, "root_msg_id": 1, "url_base_title": 1, "url_title": 1, "url_body": 1, "url_image": 1, "old_created_time": 1, "last_update_user": 1, "last_update_time": 1, "tag_list": 1, "forward_by": 1, "forward_at": 1, "edit_history": 1, "secret_user": 1, "mention_user": 1, "is_secret": 1, "participants": 1, "tasks": 1, "tasks_history": 1, "assign_to": 1, "task_observers": 1, "unread_reply": 1, "call_duration": 1, "call_msg": 1, "call_participants": 1, "call_running": 1, "call_type": 1, "call_status": 1, "call_sender_ip": 1, "call_sender_device": 1, "call_receiver_ip": 1, "call_receiver_device": 1, "call_server_addr": 1, "call_server_switch": 1, "edit_seen": 1, "has_delete": 1, "has_hide": 1, "issue_accept_user": 1, "conference_id": 1, "user_tag_string": 1, "root_conv_id": 1, "sender_name": 1, "sender_img": 1, "last_reply_time": 1, "activity_id": 1, "url_favicon": 1, "has_timer": 1, "edit_status": 1, "updatedmsgid": 1, "service_provider": 1, "has_star": 1, "referenceId": 1, "reference_type": 1, "all_attachment": 1, "task_id": 1};
        // if(!arg.select || arg.select === "*") {
        // }
        const dataAggregate = [
                                {$match: query}, 
                                {$lookup:{ 
                                    from: 'files', 
                                    localField: 'msg_id', 
                                    foreignField:'msg_id',
                                    as:'all_attachment',
                                    pipeline: [
                                        {$match: {
                                            is_delete: 0
                                        }}
                                    ]
                                }},
                                { $sort: { created_at: -1 } },
                                { $skip: (page - 1) * PAGE_SIZE },
                                { $limit: PAGE_SIZE },
                                { "$project": project }
                            ];

        var [msgs, convs] = await Promise.all([
            Messages.aggregate(dataAggregate),
            convid ? get_conversation({ conversation_id: convid, user_id: uid }) : []
        ]);
        var total = 0;
        var totalPages = 0;
        if(type !== 'one_msg'){
            /////////////////////////////////////////////////////////
            let has_reply_msg_id = [];
            for(let i=0; i<msgs.length; i++){
                if(msgs[i].has_reply > 0)
                    has_reply_msg_id.push(msgs[i].msg_id);
            }
            var rq = { msg_status: { $in: [uid] } };
            rq.conversation_id = convid;
            rq.has_hide = { $nin: [uid] };
            rq.has_delete = { $nin: [uid] };
            rq.is_reply_msg = 'yes';
            rq.reply_for_msgid = {$in: has_reply_msg_id};
            const ur_reply_msgs = [ {$match: rq}, {$group: { _id: "$reply_for_msgid", unread_reply: { $sum: 1 } } } ];
            const rmsgs = await Messages.aggregate(ur_reply_msgs);
            msgs = msgs.map(obj => {
                let matchingObj = rmsgs.find(item => item._id === obj.msg_id) || { unread_reply: 0 };
                return { ...obj, ...matchingObj };
            });
            /////////////////////////////////////////////////////////
            const countResult = await Messages.aggregate(count_total);
            total = countResult[0] ? countResult[0].total : 0;
            totalPages = Math.ceil(total / PAGE_SIZE);
        }
        let tag_ids = [];
        let garbage_remove = [];
        let unique_msgid = [];
        for(let msg of msgs){
            if(unique_msgid.indexOf(msg.msg_id.toString()) === -1){
                unique_msgid.push(msg.msg_id.toString());

                // msg.msg_body = CryptoJS.AES.encrypt(msg.msg_body, process.env.CRYPTO_SECRET).toString();
                if(type !== 'one_msg')
                    delete msg.msg_text;
                let this_user = await get_user_obj(msg.sender);
                if(this_user.firstname !== undefined){
                    msg.sendername = this_user.firstname + ' ' + this_user.lastname;
                    msg.fnln = this_user.fnln;
                    msg.senderimg = this_user.img;
                    msg.senderemail = this_user.email;
                    msg.sender_is_active = this_user.is_active;

                    if(this_user.fullname === 'User Not Found' && this_user.company_id === ''){
                        garbage_remove.push(msg.msg_id);
                    }
                }

                if(msg.all_attachment.length>0){
                    for(let i=0; i<msg.all_attachment.length; i++){
                        msg.all_attachment[i].location = process.env.FILE_SERVER + msg.all_attachment[i].location;
                        msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                        if(msg.all_attachment[i].tag_list.length>0){
                            for(let j=0; j<msg.all_attachment[i].tag_list.length; j++)
                                if(tag_ids.indexOf(msg.all_attachment[i].tag_list[j]) === -1)
                                    if(Array.isArray(msg.all_attachment[i].tag_list[j])){
                                        tag_ids.push(msg.all_attachment[i].tag_list[j][0]);
                                    }else{
                                        tag_ids.push(msg.all_attachment[i].tag_list[j]);

                                    }
                                    
                        }
                    }
                }
                // msg.secret_user_name = [];
                // msg.secret_user_details = [];
                // if(msg.is_secret === true){
                //     for(let sv of msg.secret_user){
                //         if (isUuid(sv.toString())) {
                //             let this_user = await get_user_obj(sv);
                //             if(this_user.firstname !== undefined && msg.secret_user_name.indexOf(this_user.firstname + ' ' + this_user.lastname) == -1) {
                //                 msg.secret_user_name.push(this_user.firstname + ' ' + this_user.lastname);
                //                 msg.secret_user_details.push({
                //                     id: sv,
                //                     firstname: this_user.firstname,
                //                     lastname: this_user.lastname,
                //                     email: this_user.email,
                //                     img: this_user.img,
                //                     fnln: this_user.fnln
                //                 });
                //             }
                //         }
                //     }
                //     // msg.secret_user.forEach(async function (sv, sk) {
                        
                //     // });
                // }
            }
            else{
                Messages.deleteOne({_id: msg._id}, function(e){
                    console.log(`Garbage msg remove ${msg.msg_id}`);
                });
            }
        }

        if(garbage_remove.length>0){
            Messages.updateMany({msg_id: {$in: garbage_remove}}, {$push: {has_hide: uid.toString()}}, { multi: true })
            .then(result => {console.log(381, 'garbage remove')})
            .catch(error => {console.log(382, error);})
        }

        if(tag_ids.length>0){
            let taginfo = await Tag.find({tag_id: {$in: tag_ids}}, 'tag_id tagged_by title company_id type shared_tag visibility tag_type tag_color team_list created_at update_at').lean();
            
            for(let msg of msgs){
                if(msg.all_attachment.length>0){
                    for(let i=0; i<msg.all_attachment.length; i++){
                        msg.all_attachment[i].tag_list_details = [];
                        msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                        if(msg.all_attachment[i].tag_list.length>0){
                            for(let j=0; j<msg.all_attachment[i].tag_list.length; j++){
                                for(let k=0; k<taginfo.length; k++){
                                    if(taginfo[k].tag_id === msg.all_attachment[i].tag_list[j])
                                        msg.all_attachment[i].tag_list_details.push(taginfo[k]);
                                }
                            }
                        }
                    }
                }
            }
        }

        resolve({
            details: convs.length > 0 ? convs[0] : {},
            msgs: msgs.reverse(), 
            pagination: {
                page: parseInt(page),
                totalPages,
                total
            }
        });
    });
}

function get_discussion(arg, req = null) {
    return new Promise( async (resolve, reject) => {
        const PAGE_SIZE = 20;
        const page = Number(arg.page) || 1;
        var convid = arg.convid ? arg.convid : '';
        var task_id = arg.task_id ? arg.task_id : '';
        var limit = arg.limit ? arg.limit : '';
        var type = arg.type ? arg.type : '';
        var is_reply = arg.is_reply ? arg.is_reply : 'no';
        var has_reply = arg.has_reply ? arg.has_reply : '';
        var uid = arg.user_id ? arg.user_id : '';

        // console.log(11, convid, task_id, limit, type, is_reply, has_reply, uid);
        var query = {task_id: new ObjectId(task_id), msg_id: arg.msgid}; 
        // if (is_reply != '')
        //     query.is_reply_msg = is_reply;
        // if (isUuid(convid.toString())) {
        //     query.conversation_id = convid.toString();
        // }
        // if(type === 'one_msg' && task_id){
        //     query.task_id = task_id;
        // }
        
        // if ((task_id) && type == "") {
        //     query.task_id = task_id;
        // }
        
        // query.has_hide = { $nin: [uid.toString()] };
        // query.has_delete = { $nin: [uid.toString()] };
        // query.$or = [ 
        //             {
        //                 $and: [
        //                     { is_secret: true }, 
        //                     { secret_user: { $in: [uid.toString()] }} 
        //                 ]
        //             },
        //             { is_secret: false } 
        //         ];

        // console.log(56, limit, query);
        const count_total = [{$match: query}, {$count: "total"}];
          
        const dataAggregate = [
                                {$match: query}, 
                                {$lookup:{ 
                                    from: 'files', 
                                    localField: 'msg_id', 
                                    foreignField:'msg_id',
                                    as:'all_attachment',
                                    pipeline: [
                                        {$match: {
                                            is_delete: 0
                                        }}
                                    ]
                                }},
                                { $sort: { created_at: -1 } },
                                { $skip: (page - 1) * PAGE_SIZE },
                                { $limit: PAGE_SIZE }
                            ];
        
        const [msgs, convs] = await Promise.all([
            Messages.aggregate(dataAggregate),
            convid ? get_conversation({ conversation_id: convid, user_id: uid }) : []
        ]);
        var total = 0;
        var totalPages = 0;
        if(type !== 'one_msg'){
            const countResult = await Messages.aggregate(count_total);
            total = countResult[0] ? countResult[0].total : 0;
            totalPages = Math.ceil(total / PAGE_SIZE);
        }
        let tag_ids = [];
        for(let msg of msgs){
            // msg.msg_body = CryptoJS.AES.encrypt(msg.msg_body, process.env.CRYPTO_SECRET).toString();
            let this_user = await get_user_obj(msg.sender);
            if(this_user.firstname !== undefined){
                msg.sendername = this_user.firstname + ' ' + this_user.lastname;
                msg.fnln = this_user.fnln;
                msg.senderimg = this_user.img;
                msg.senderemail = this_user.email;
                msg.sender_is_active = this_user.is_active;
            }

            if(msg.all_attachment.length>0){
                for(let i=0; i<msg.all_attachment.length; i++){
                    msg.all_attachment[i].location = process.env.FILE_SERVER + msg.all_attachment[i].location;
                    msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                    if(msg.all_attachment[i].tag_list.length>0){
                        for(let j=0; j<msg.all_attachment[i].tag_list.length; j++)
                            if(tag_ids.indexOf(msg.all_attachment[i].tag_list[j]) === -1)
                                tag_ids.push(msg.all_attachment[i].tag_list[j]);
                    }
                }
            }
            msg.secret_user_name = [];
            msg.secret_user_details = [];
            if(msg.is_secret === true){
                msg.secret_user.forEach(async function (sv, sk) {
                    if (isUuid(sv.toString())) {
                        let this_user = await get_user_obj(sv);
                        if(this_user.firstname !== undefined && msg.secret_user_name.indexOf(this_user.firstname + ' ' + this_user.lastname) == -1) {
                            msg.secret_user_name.push(this_user.firstname + ' ' + this_user.lastname);
                            msg.secret_user_details.push({
                                id: sv,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                email: this_user.email,
                                img: this_user.img,
                                fnln: this_user.fnln
                            });
                        }
                    }
                });
            }
        }

        if(tag_ids.length>0){
            let taginfo = await Tag.find({tag_id: {$in: tag_ids}}, 'tag_id tagged_by title company_id type shared_tag visibility tag_type tag_color team_list created_at update_at').lean();
            
            for(let msg of msgs){
                if(msg.all_attachment.length>0){
                    for(let i=0; i<msg.all_attachment.length; i++){
                        msg.all_attachment[i].tag_list_details = [];
                        msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                        if(msg.all_attachment[i].tag_list.length>0){
                            for(let j=0; j<msg.all_attachment[i].tag_list.length; j++){
                                for(let k=0; k<taginfo.length; k++){
                                    if(taginfo[k].tag_id === msg.all_attachment[i].tag_list[j])
                                        msg.all_attachment[i].tag_list_details.push(taginfo[k]);
                                }
                            }
                        }
                    }
                }
            }
        }

        resolve({
            details: convs.length > 0 ? convs[0] : {},
            msgs: msgs.reverse(), 
            pagination: {
                page: parseInt(page),
                totalPages,
                total
            }
        });
    });
}

function get_filter_msg(data) {
    return new Promise(async (resolve, reject) => {
        var PAGE_SIZE = 20;
        const page = Number(data.page) || 1;
        var query = {};
        var rmsgs = [];
        if (data.name == "unread") {
            query.msg_status = { $in: [data.user_id] };
        } else if (data.name == "all_reply") {
            query.has_reply = { '$gt': 0 };
            query.is_reply_msg = 'no';

            if(data.unread_reply === 'yes'){
                var rq = { msg_status: { $in: [data.user_id] } };
                rq.has_hide = { $nin: [data.user_id] };
                rq.has_delete = { $nin: [data.user_id] };
                rq.is_reply_msg = 'yes';
                rq.conversation_id = data.conversation_id;
                const ur_reply_msgs = [ {$match: rq}, {$group: { _id: "$reply_for_msgid", unread_reply: { $sum: 1 } } }, { "$project": {reply_for_msgid: 1, unread_reply: 1} } ];
                rmsgs = await Messages.aggregate(ur_reply_msgs);
                // console.log(784, rmsgs);
                let mids = [];
                query.msg_id = {$in: rmsgs.map((e)=>{if(mids.indexOf(e._id) === -1) return e._id;}) };
                // console.log(787, query.msg_id);
            }
        } else if (data.name == "call") {
            query.msg_type = 'call';
        } else if (data.name == "checklist") {
            query.msg_type = 'checklist';
        } else if (data.name == "task") {
            query.msg_type = 'task';
        } else if (data.name == "media") {
            query.msg_type = new RegExp('media_attachment', 'i');
        } else if (data.name == "voice") {
            query.msg_type = new RegExp('has_voice_msg', 'i');
        } else if (data.name == "link") {
            query.msg_type = new RegExp('link', 'i');
            // if (!isUuid(data.conversation_id)) {
            //     query.company_id = data.company_id;
            //     query.$groupby = ['conversation_id'];
            // }
        } else if (data.name == "msg_title") {
            query.msg_type = new RegExp('has_msg_title', 'i');
            // if (!isUuid(data.conversation_id)) {
            //     query.company_id = data.company_id;
            //     query.$groupby = ['conversation_id'];
            // }
        } else if (data.name == "tag") {
            query.msg_type = new RegExp('tag', 'i');
        } else if (data.name == "flag") {
            query.has_flagged = { $in: [data.user_id] };
        } else if (data.name == "star") {
            query.has_star = { $in: [data.user_id] };
        } else if (data.name == 'private') {
            query.is_secret = true;
            if (data.user_id)
                query.secret_user = { $in: [data.user_id] };
        } else if (data.name == "str") {
            query.msg_text = new RegExp(data.str.toString(), 'i');
        } else if (data.name == "referenceId") {
            query.referenceId = { $gt: '' };
        }

        if (data.is_reply) {
            query.is_reply_msg = data.is_reply;
        }
        if (isUuid(data.conversation_id)) {
            query.conversation_id = data.conversation_id;
        }
        if (isUuid(data.msg_id) && data.type == "one") {
            query.msg_id = data.msg_id;
        }
        // if (isUuid(data.msg_id) && data.type == "old") {
        //     query.msg_id = { $lt: models.timeuuidFromString(data.msg_id) };
        //     // query.$orderby = { '$desc': 'msg_id' };
        // }
        // if (isUuid(data.msg_id) && data.type == "new") {
        //     query.msg_id = { $gt: models.timeuuidFromString(data.msg_id) };
        // }
        if (data.reply_for_msgid && isUuid(data.reply_for_msgid) && data.is_reply === 'yes') {
            query.reply_for_msgid = data.reply_for_msgid;
        }
        // if (data.limit && !isNaN(data.limit)) {
        //     query.$limit = data.limit == 20 ? 40 : 100;
        // }

        query.has_hide = { $nin: [data.user_id] };
        query.has_delete = { $nin: [data.user_id] };
        query.$or = [ 
                    {
                        $and: [
                            { is_secret: true }, 
                            { secret_user: { $in: [data.user_id] }} 
                        ]
                    },
                    { is_secret: false } 
                ];
        // console.log(861, JSON.stringify(query));
        const count_total = [{$match: query}, {$count: "total"}];
          
        const dataAggregate = [
                                {$match: query}, 
                                {$lookup:{ 
                                    from: 'files', 
                                    localField: 'msg_id', 
                                    foreignField:'msg_id',
                                    as:'all_attachment',
                                    pipeline: [
                                        {$match: {
                                            is_delete: 0
                                        }}
                                    ]
                                }}
                            ];
        dataAggregate.push({ $sort: { created_at: -1 } });
        if(data.page_size !== 'unlimited'){
            dataAggregate.push({$skip: (page - 1) * PAGE_SIZE});
            dataAggregate.push({$limit: PAGE_SIZE});
        }
        // console.log(883, JSON.stringify(dataAggregate));
        var [countResult, msgs] = await Promise.all([
            Messages.aggregate(count_total),
            Messages.aggregate(dataAggregate)
        ]);
        
        const total = countResult[0] ? countResult[0].total : 0;
        const totalPages = Math.ceil(total / PAGE_SIZE);

        let has_reply_msg_id = [];
        for(let i=0; i<msgs.length; i++){
            if(msgs[i].has_reply > 0 && has_reply_msg_id.indexOf(msgs[i].msg_id) === -1)
                has_reply_msg_id.push(msgs[i].msg_id);
        }
        console.log(897, has_reply_msg_id)
        // if(data.unread_reply !== 'yes'){
            var rq = { msg_status: { $in: [data.user_id] } };
            rq.has_hide = { $nin: [data.user_id] };
            rq.has_delete = { $nin: [data.user_id] };
            rq.is_reply_msg = 'yes';
            rq.reply_for_msgid = {$in: has_reply_msg_id};
            const ur_reply_msgs = [ {$match: rq}, {$group: { _id: "$reply_for_msgid", unread_reply: { $sum: 1 } } } ];
            rmsgs = await Messages.aggregate(ur_reply_msgs);
        // }
        // console.log(907, rmsgs)
        msgs = msgs.map(obj => {
            let matchingObj = rmsgs.find(item => item._id === obj.msg_id) || { unread_reply: 0 };
            return { ...obj, ...matchingObj };
        });

        for(let msg of msgs){
            // msg.msg_body = CryptoJS.AES.encrypt(msg.msg_body, process.env.CRYPTO_SECRET).toString();
            let this_user = await get_user_obj(msg.sender);
            if(this_user.firstname !== undefined){
                msg.sendername = this_user.firstname + ' ' + this_user.lastname;
                msg.fnln = this_user.fnln;
                msg.senderimg = this_user.img;
                msg.senderemail = this_user.email;
                msg.sender_is_active = this_user.is_active;
            }
            if(msg.all_attachment.length>0){
                for(let i=0; i<msg.all_attachment.length; i++){
                    msg.all_attachment[i].location = process.env.FILE_SERVER + msg.all_attachment[i].location;
                }
            }
            
            msg.secret_user_name = [];
            msg.secret_user_details = [];
            if(msg.is_secret === true){
                for(let sv of msg.secret_user){
                    if (isUuid(sv.toString())) {
                        let this_user = await get_user_obj(sv);
                        if(this_user.firstname !== undefined && msg.secret_user_name.indexOf(this_user.firstname + ' ' + this_user.lastname) == -1) {
                            msg.secret_user_name.push(this_user.firstname + ' ' + this_user.lastname);
                            msg.secret_user_details.push({
                                id: sv,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                email: this_user.email,
                                img: this_user.img,
                                fnln: this_user.fnln
                            });
                        }
                    }
                }
                // msg.secret_user.forEach(async function (sv, sk) {
                    
                // });
            }
        }
        
        resolve({
            msgs, 
            pagination: {
                page: parseInt(page),
                totalPages,
                total
            }
        });
    });
}

function getModelData(model_object, data) {
    for (let field in model_object.fields) {
        if (!data.hasOwnProperty(field)) {
            if (model_object.fields[field].hasOwnProperty('type') && model_object.fields[field]['type']) {
                var field_type = model_object.fields[field]['type'];
                var field_default = model_object.fields[field]['default'];
            } else {
                var field_type = model_object.fields[field];
                var field_default = undefined;
            }

            if (field_type == 'timestamp') {
                if (field_default) data[field] = new Date();
            } else if (field_type == 'text') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = '';
            } else if (field_type == 'set') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = [];
            } else if (field_type == 'map') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = {};
            } else if (field_type == 'int') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = 0;
            } else if (field_type == 'tinyint') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = 0;
            } else if (field_type == 'boolean') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = false;
            } else if (field_type == 'uuid') {
                if (field_default != undefined) data[field] = field_default;
                else data[field] = null;
            }
        }
    }
}

async function getFullData(data, post, callback) {
    return new Promise(async (resolve, reject) => {
        // data = { ...data, ...post};
        // getModelData(models.instance.Messages._properties.schema, data);
        data.checklist = [];
        let sobj = await get_user_obj(data.sender.toString());
        data.sendername = sobj.firstname + ' ' + sobj.lastname;
        data.fnln = sobj.fnln;
        data.senderimg = sobj.img;
        data.senderemail = sobj.email;
        data.sender_is_active = sobj.is_active;

        data.has_hide = Array.isArray(data.has_hide) ? data.has_hide : [];
        data.has_delete = Array.isArray(data.has_delete) ? data.has_delete : [];
        data.secret_user = Array.isArray(data.secret_user) ? data.secret_user : [];
        data.has_tag_text = Array.isArray(data.has_tag_text) ? data.has_tag_text : [];
        // data.msg_body_db = data.msg_body;
        data.msg_body = CryptoJS.AES.encrypt(data.msg_body, process.env.CRYPTO_SECRET).toString();
        data.msg_status = data.msg_status == null ? [] : data.msg_status;
        data.has_hide = data.has_hide == null ? [] : data.has_hide;
        data.has_mute = data.has_mute == null ? [] : data.has_mute;
        data.attch_audiofile = data.attch_audiofile == null ? [] : data.attch_audiofile;
        data.attch_imgfile = data.attch_imgfile == null ? [] : data.attch_imgfile;
        data.attch_otherfile = data.attch_otherfile == null ? [] : data.attch_otherfile;
        data.attch_videofile = data.attch_videofile == null ? [] : data.attch_videofile;
        data.all_attachment = [];
        data.secret_user_name = [];
        data.secret_user_details = [];
        data.unread_reply = 0;
        if (data.is_secret === true) {
            data.secret_user.forEach(async function (sv, sk) {
                if (isUuid(sv.toString())) {
                    var this_user = await get_user_obj(sv.toString());
                    if (data.secret_user_name.indexOf(this_user.firstname + ' ' + this_user.lastname) == -1) {
                        data.secret_user_name.push(this_user.firstname + ' ' + this_user.lastname);
                        data.secret_user_details.push({
                            id: sv.toString(),
                            firstname: this_user.firstname,
                            lastname: this_user.lastname,
                            email: this_user.email,
                            img: this_user.img,
                            fnln: this_user.fnln
                        });
                    }
                }
            });
        }
        // ==============
        if (typeof post.attach_files !== 'undefined' && typeof post.attach_files.allfiles !== 'undefined') {

            var fdata = {
                allfiles: post.attach_files.allfiles,
                user_id: data.sender,
                msgid: data.msg_id,
                conversation_id: data.conversation_id,
                tempAttachmentTag: [],
                mention_user: post.mention_user && post.mention_user.length > 0 ? post.mention_user : [],
                root_conv_id: null,
                secret_user: post.secret_user && post.secret_user.length > 0 ? post.secret_user : [],
                reply_for_msgid: post.is_reply_msg === 'yes' ? post.reply_for_msgid : null,
                participants: post.participants,
                other_user: data.msg_status,
                company_id: post.company_id,
                tag_list: post.newtag
            }
            // ===============

            if (!Array.isArray(post.attach_files.allfiles)) {
                post.attach_files.allfiles = JSON.parse(post.attach_files.allfiles);
            }
            var tag_list_with_user = [];
            if (post.attach_files.allfiles.length > 0) {
                for await (const v of post.attach_files.allfiles) {
                    // _.forEach(post.attach_files.allfiles, async function(v, k) {
                    v.mimetype = (v.mimetype.indexOf("text") > -1) ? "application/" + v.mimetype : v.mimetype;
                    var cat = 'docs';
                    if (v.mimetype.indexOf('image') > -1) cat = 'image';
                    if (v.mimetype.indexOf('audio') > -1) cat = 'audio';
                    if (v.mimetype.indexOf('video') > -1) cat = 'video';

                    var sdata = {
                        id: uuidv4(),
                        user_id: fdata.user_id,
                        msg_id: fdata.msgid,
                        conversation_id: fdata.conversation_id,
                        acl: v.acl,
                        bucket: v.bucket,
                        file_type: v.mimetype,
                        key: v.key,
                        location: v.bucket + '/' + v.key,
                        originalname: (v.voriginalName === undefined) ? v.originalname : v.voriginalName,
                        file_size: v.size.toString(),
                        tag_list: null,
                        mention_user: fdata.mention_user,
                        root_conv_id: (fdata.root_conv_id != undefined) ? fdata.root_conv_id : null,
                        secret_user: fdata.secret_user,
                        is_secret: fdata.secret_user && fdata.secret_user.length ? true : false,
                        file_category: post.voice_set_type ? 'voice' : cat,
                        main_msg_id: fdata.reply_for_msgid,
                        other_user: fdata.other_user,
                        participants: fdata.participants,
                        company_id: fdata.company_id,
                        tag_list_with_user: null,
                        referenceId: data.referenceId,
                        reference_type: data.reference_type
                    }
                    // console.log('xmpp:file_set',sdata.originalname,sdata.id.toString());
                    if (fdata.tag_list && Array.isArray(fdata.tag_list) && fdata.tag_list.length > 0) {
                        sdata.tag_list = fdata.tag_list;
                        sdata.has_tag = 'tag';
                        for (let i = 0; i < fdata.tag_list.length; i++) {
                            tag_list_with_user.push({ "tag_id": fdata.tag_list[i], "created_by": fdata.user_id });
                        }
                        sdata.tag_list_with_user = JSON.stringify(tag_list_with_user);
                    }
                    // getModelData(models.instance.File._properties.schema, sdata);

                    // var file_data = new models.instance.File(sdata);

                    sdata.tag_list = Array.isArray(sdata.tag_list) ? sdata.tag_list : [];
                    sdata.star = Array.isArray(sdata.star) ? sdata.star : [];
                    sdata.url_short_id = sdata.url_short_id === null ? "" : sdata.url_short_id;
                    sdata.secret_user = Array.isArray(sdata.secret_user) ? sdata.secret_user : [];
                    try {
                        var tlwu = JSON.parse(sdata.tag_list_with_user);
                    } catch (e) {
                        var tlwu = [];
                    }
                    sdata.tag_list_with_user = Array.isArray(tlwu) ? tlwu : [];
                    sdata.tag_list_details = [];
                    
                    if (sdata.tag_list != null && sdata.tag_list.length > 0) {
                        if (data.company_id && isUuid(data.company_id.toString())){
                            try{
                                tags = await get_tags({ company_id: data.company_id, id: data.sender });
                            }catch(e){
                                console.log(758, e);
                            }
                        }
                        _.each(sdata.tag_list, function (tv, tk) {
                            _.each(tags.result, function (vv, kk) {
                                if (tv.toString() == vv.tag_id.toString()) {
                                    sdata.tag_list_details.push(vv);
                                }
                            });
                        });
                    }
                    
                    sdata.location = process.env.FILE_SERVER + sdata.location;
                    data.all_attachment.push(sdata);

                };

            }
        }

        // callback({ status: true, msg: data });
        // console.log('getFullData',data, post);
        var bytes = CryptoJS.AES.decrypt(data.msg_body, process.env.CRYPTO_SECRET);
        data.msg_body = bytes.toString(CryptoJS.enc.Utf8);
        resolve();
    })




}
async function cassandraSaveBatch(queries){
    return new Promise((resolve,reject)=>{
        models.doBatch(queries, function (err) {
            if (err){
                setTimeout(()=>{
                    cassandraSaveBatch(queries);
                },1000);
            }else{
                resolve({ status: true});
            } 
        });
    })

}
async function set_message(post, callback) {
    var diff_start = Date.now();
    var data = {};
    // console.log(505, post);
    data.conversation_id = post.conversation_id;
    data.company_id = post.company_id;
    data.sender = post.sender;
    data.sender_name = post.sender_name;
    data.sender_img = post.sender_img;
    data.msg_body = post.msg_body;
    var bytes = CryptoJS.AES.decrypt(post.msg_body, process.env.CRYPTO_SECRET);
    let msgbody = bytes.toString(CryptoJS.enc.Utf8);
    data.msg_text = post.msg_text && post.msg_text !== "" ? post.msg_text : msgbody.toString();
    data.msg_status = (post.participants).filter((a) => { return a != post.sender; });
    data.msg_type = post.msg_type ? post.msg_type : 'text';

    post.is_reply_msg = post.is_reply_msg === undefined ? 'no' : post.is_reply_msg;

    if (post.is_reply_msg == 'yes') {
        data.is_reply_msg = post.is_reply_msg;
        data.reply_for_msgid = post.reply_for_msgid;
    }
    if (typeof post.attach_files === 'undefined') {
        data.attch_imgfile = [];
        data.attch_audiofile = [];
        data.attch_videofile = [];
        data.attch_otherfile = [];
    } else {
        if(Array.isArray(post.all_attachment) && post.all_attachment.length > 0){
            for(let i=0; i<post.all_attachment.length; i++){
                post.all_attachment[i].has_tag = post.all_attachment[i].has_tag === 'null' ? null : post.all_attachment[i].has_tag;
                post.all_attachment[i].tag_list = Array.isArray(post.all_attachment[i].tag_list) ? post.all_attachment[i].tag_list : [];
            }
        }
        data.msg_type = 'media_attachment';
        data.attch_imgfile = (typeof post.attach_files.imgfile === 'undefined' || post.attach_files.imgfile == null) ? [] : post.attach_files.imgfile;
        data.attch_audiofile = (typeof post.attach_files.audiofile === 'undefined' || post.attach_files.audiofile == null) ? [] : post.attach_files.audiofile;
        data.attch_videofile = (typeof post.attach_files.videofile === 'undefined' || post.attach_files.videofile == null) ? [] : post.attach_files.videofile;
        data.attch_otherfile = (typeof post.attach_files.otherfile === 'undefined' || post.attach_files.otherfile == null) ? [] : post.attach_files.otherfile;
        // has_tag_text
        if (Array.isArray(post.newtag) && post.newtag.length > 0) {
            data.has_tag_text = post.newtag;
        }
        // else{
        //     var untag = all_untags[post.company_id];
        //     data.has_tag_text = post.newtag = [untag.tag_id.toString()];
        // }
        if (post.attach_files.allfiles !== undefined && post.attach_files.allfiles.length > 0) {
            for (let n = 0; n < post.attach_files.allfiles.length; n++) {
                let tempfn = post.attach_files.allfiles[n].voriginalName === undefined ? post.attach_files.allfiles[n].originalName : post.attach_files.allfiles[n].voriginalName;
                data.msg_text += " " + tempfn;
            }
        }
    }
    if (typeof post.mention_user !== 'undefined') {
        if (Array.isArray(post.mention_user)) {
            data.mention_user = post.mention_user;
        }
    }
    if (typeof post.secret_user !== 'undefined' && typeof post.is_secret !== 'undefined' && post.is_secret == true) {
        data.is_secret = post.is_secret;
        if (Array.isArray(post.secret_user)) {
            data.secret_user = post.secret_user;
        }
    }
    if (typeof post.forward_by !== 'undefined') {
        if (isUuid(post.forward_by)) {
            data.forward_by = String(post.forward_by);
        }
    }
    if (data.msg_type.indexOf('link') > -1) {
        data.url_body = post.url_body !== undefined ? post.url_body : msgbody;
    } else if (post.url_body && post.url_body !== "" && post.url_body !== undefined) {
        data.msg_type = data.msg_type == 'media_attachment' ? 'text link media_attachment' : 'text link';
        data.url_body = post.url_body;
    }

    data.url_base_title = post.msg_title && post.msg_title !== '' ? post.msg_title : post.url_title && post.url_title !== '' ? post.url_title : null;
    if (data.url_base_title !== null && data.url_base_title !== '') {
        data.msg_type += ' has_msg_title';
    }
    if (post.voice_set_type) {
        data.msg_type += ' has_voice_msg';
    }
    if(post.referenceId){
        data.referenceId = post.referenceId;
        data.msg_type += ' referenceId';
    }
    if(post.reference_type){
        data.reference_type = post.reference_type;
        data.msg_type += ' reference_type';
    }

    if (data.msg_type.indexOf('task') > -1){
        data.task_start_date = post.task_start_date ? post.task_start_date : new Date(); //yyyy-MM-dd'T'HH:mm:ssZ
        data.task_due_date = post.task_due_date ? post.task_due_date : new Date(); //yyyy-MM-dd'T'HH:mm:ssZ
        data.task_review = post.task_review ? post.task_review : 'no'; // yes/ no
        data.task_keywords = post.task_keywords ? post.task_keywords.toString() : ''; // yes/ no
        data.task_progress = post.task_progress ? post.task_progress : 'Not Started'; // Not Started/ In Progress/ Completed/ Canceled
        data.task_forecasted_hours = post.task_forecasted_hours ? post.task_forecasted_hours : '0';
        data.task_actual_hours = post.task_actual_hours ? post.task_actual_hours : '0';
        data.task_hours_variance = post.task_hours_variance ? post.task_hours_variance : '0';
        data.task_forecasted_cost = post.task_forecasted_cost ? post.task_forecasted_cost : '0';
        data.task_actual_cost = post.task_actual_cost ? post.task_actual_cost : '0';
        data.task_cost_variance = post.task_cost_variance ? post.task_cost_variance  : '0';
        data.task_description = post.task_description ? post.task_description  : '';
        data.task_notes = post.task_notes ? post.task_notes  : '';
        data.assign_to = Array.isArray(post.assign_to) && post.assign_to.length > 0 ? post.assign_to  : null;
        data.task_observers = Array.isArray(post.task_observers) && post.task_observers.length > 0 ? post.task_observers  : null;
    }
    
    // console.log(112, data);

    var queries = [];
    // console.log(583, post.topic_type);
    try {
        // console.log('reply_msg_id_client:',post.msg_id)
        data.msg_id = post.msg_id ? String(post.msg_id) : uuidv4();
        data.created_at = new Date();
        data.forward_at = new Date();
        data.last_update_time = new Date();
        data.participants = post.participants;
        await getFullData(data, post, callback);
        // callback({ status: true, msg: data });
        // console.log('msg_data',data);
        if(post.task_id){
            data.task_id = post.task_id;
            data.msg_type = post.msg_type;
            // data.is_reply_msg = "yes";
            // data.reply_for_msgid=data.msg_id;
            await Messages.insertMany([data]);
        }else{
            if(post.taskid){
                data.task_id = post.taskid;
                // data.msg_type = "task";
                // data.topic_type = post.topic_type;
            } 
            await Messages.insertMany([data]);
        }
        
        // console.log('xmpp_diff:direct', Date.now() - diff_start);
        if (post.msg_type && post.msg_type.indexOf('link') > -1) {
            await save_link_data(post, data);
        }
        if (typeof post.attach_files !== 'undefined' && typeof post.attach_files.allfiles !== 'undefined') {
            await save_file_info({
                allfiles: post.attach_files.allfiles,
                user_id: data.sender,
                msgid: data.msg_id,
                conversation_id: data.conversation_id,
                tempAttachmentTag: [],
                mention_user: post.mention_user && post.mention_user.length > 0 ? post.mention_user : [],
                root_conv_id: null,
                secret_user: post.secret_user && post.secret_user.length > 0 ? post.secret_user : [],
                reply_for_msgid: post.is_reply_msg === 'yes' ? post.reply_for_msgid : null,
                participants: post.participants,
                other_user: data.msg_status,
                company_id: post.company_id,
                tag_list: post.newtag,
                voice_set_type: post.voice_set_type,
                all_attachment: data.all_attachment,
                referenceId: post.referenceId,
                reference_type: post.reference_type,
                task_id: post.task_id,
                file_group: post.file_group,
                cost_id: post.cost_id
            });
        }
        try {
            if(post.task_id){
                var lastmsg = await get_discussion({ task_id: post.task_id, convid: data.conversation_id, company_id: data.company_id, msgid: data.msg_id, limit: 1, is_reply: data.is_reply_msg, user_id: data.sender, type: 'one_msg' });
            }else{
                var lastmsg = await get_message({ convid: data.conversation_id, company_id: data.company_id, msgid: data.msg_id, limit: 1, is_reply: data.is_reply_msg, user_id: data.sender, type: 'one_msg' });
            }

            
            callback({ status: true, msg: lastmsg.msgs[0] });
            // console.log('xmpp_diff:db', Date.now() - diff_start);
            
            if (post.is_reply_msg == 'yes') {
                update_msg_for_reply_msg(lastmsg.msgs[0]);
            } else {
                update_conversation_for_last_msg(lastmsg.msgs[0]);
            }
            sendMsgUpdateIntoZnode(lastmsg);
        } catch (e) {
            console.log(373, e)
            callback({ status: false, error: e });
        }
    } catch (e) {
        console.log(678, e);
        callback({ status: false });
    }
}

function send_bot_reply(data, msg, req) {
    // console.log(705, data);
    // console.log(706, msg.msg_id, msg.tasks);
    var old_task = JSON.parse(msg.tasks);
    // console.log(709, old_task)
    var body = data.sender_name;
    var flag = false;
    // Assigner] updated the Task Status from Assigned to In-Progress on 02-Jan-2021 at 10:00 a.m.
    if (old_task.task_title != data.list_items[0].title.toString()) {
        flag = true;
        body += ' updated the Task Title ' + old_task.task_title + ' to ' + data.list_items[0].title.toString();
    }

    if (old_task.task_status != data.list_items[0].mark) {
        body += flag ? ' and ' : '';
        flag = true;
        let oldm = '',
            newm = '';
        switch (old_task.task_status.toString()) {
            case '0':
                oldm = '<b>Assigned</b>';
                break;
            case '1':
                oldm = '<b>In-Progress</b>';
                break;
            case '2':
                oldm = '<b>Completed</b>';
                break;
            case '3':
                oldm = '<b>Reopened</b>';
                break;
        }
        switch (data.list_items[0].mark.toString()) {
            case '0':
                newm = '<b>Assigned</b>';
                break;
            case '1':
                newm = '<b>In-Progress</b>';
                break;
            case '2':
                newm = '<b>Completed</b>';
                break;
            case '3':
                newm = '<b>Reopened</b>';
                break;
        }
        body += ' updated the Task Status from ' + oldm + ' to ' + newm;
    }
    if (old_task.private != data.is_secret) {
        body += flag ? ' and ' : '';
        flag = true;
        let oldp = old_task.private == true ? '<b>Private</b>' : '<b>Public</b>';
        let newp = data.is_secret == true ? '<b>Private</b>' : '<b>Public</b>';
        body += ' updated the Task Privacy from ' + oldp + ' to ' + newp;
    }
    if (old_task.working_hour != data.list_items[0].working_hour) {
        body += flag ? ' and ' : '';
        flag = true;
        body += ' updated the Task Working Hour from <b>' + old_task.working_hour + '</b> to <b>' + data.list_items[0].working_hour + '</b>';
    }
    if (old_task.budget != data.list_items[0].budget) {
        body += flag ? ' and ' : '';
        flag = true;
        body += ' updated the Task Budget from <b>' + old_task.budget + '</b> to <b>' + data.list_items[0].budget + '</b>';
    }
    if (old_task.actual_hour != data.list_items[0].actual_hour) {
        body += flag ? ' and ' : '';
        flag = true;
        if (old_task.actual_hour == '0')
            body += ' set the Task Actual Hour <b>' + data.list_items[0].actual_hour + '</b>';
        else
            body += ' updated the Task Actual Hour from <b>' + old_task.actual_hour + '</b> to <b>' + data.list_items[0].actual_hour + '</b>';
    }
    if (old_task.actual_cost != data.list_items[0].actual_cost) {
        body += flag ? ' and ' : '';
        flag = true;
        if (old_task.actual_cost == '0')
            body += ' set the Task Actual Cost <b>' + data.list_items[0].actual_cost + '</b>';
        else
            body += ' updated the Task Actual Cost from <b>' + old_task.actual_cost + '</b> to <b>' + data.list_items[0].actual_cost + '</b>';
    }
    if (old_task.assign_to != data.list_items[0].assign_to) {
        body += flag ? ' and ' : '';
        flag = true;
        if (data.list_items[0].assign_to != '')
            body += ' updated the Task Assignee from <b>' + old_task.assign_to_name + '</b> to <b>' + data.list_items[0].assign_to_name + '</b>';
        else
            body += ' removed the Task Assignee <b>' + old_task.assign_to_name + '</b>';
    }
    if (old_task.due_date != data.list_items[0].due_date) {
        body += flag ? ' and ' : '';
        flag = true;
        body += ' updated the Task Due Date from <b>' + moment(old_task.due_date).format('Do MMMM YYYY, h:mm:ss a') + '</b> to <b>' + moment(data.list_items[0].due_date).format('Do MMMM YYYY, h:mm:ss a') + '</b>';
    }
    if (flag === true) {
        body += ' dated on ' + moment().format('Do MMMM YYYY, h:mm:ss a');
        body = CryptoJS.AES.encrypt(JSON.stringify(body), process.env.CRYPTO_SECRET).toString();
        var arg = {
            conversation_id: data.conversation_id,
            company_id: msg.company_id.toString(),
            sender: data.sender,
            sender_img: data.sender_img,
            msg_body: body,
            participants: data.participants,
            is_reply_msg: 'yes',
            msg_type: 'text log_msg',
            reply_for_msgid: msg.msg_id.toString(),
            CLIENT_BASE_URL: process.env.API_SERVER_IP
        }
        try {
            set_message(arg, function (res) {
                data.participants.forEach(function (v, k) {
                    xmpp_send_server(v, 'new_reply_message', res.msg, req);
                    send_msg_firebase(v, 'new_reply_message', res.msg, req);
                });
            });
        } catch (e) {
            console.log(e);
        }
    } else {
        console.log("No update found for bot msg");
    }
}

function save_file_info(data) {
    return new Promise(async (resolve, reject) => {
        // console.log(213, data);
        var queries = [];
        if (!Array.isArray(data.allfiles)) {
            data.allfiles = JSON.parse(data.allfiles);
        }
        var returnData = [];
        var tag_list_with_user = [];
        var file_ids = [];
        if (data.allfiles.length > 0) {
            _.forEach(data.allfiles, function (v, k) {
                v.mimetype = (v.mimetype.indexOf("text") > -1) ? "application/" + v.mimetype : v.mimetype;
                var cat = 'docs';
                if (v.mimetype.indexOf('image') > -1) cat = 'image';
                if (v.mimetype.indexOf('audio') > -1) cat = 'audio';
                if (v.mimetype.indexOf('video') > -1) cat = 'video';
                // console.log(1102,data);
                // console.log('xmpp:file_get',data.all_attachment[k].originalname,data.all_attachment[k].id.toString());
                file_ids.push(data.all_attachment[k].id);
                var sdata = {
                    id: data.all_attachment[k].id,
                    user_id: data.user_id,
                    msg_id: data.msgid,
                    conversation_id: data.conversation_id,
                    acl: v.acl,
                    bucket: v.bucket,
                    file_type: v.mimetype,
                    key: v.key,
                    location: v.bucket + '/' + v.key,
                    originalname: (v.voriginalName === undefined) ? v.originalname : v.voriginalName,
                    file_size: v.size.toString(),
                    tag_list: null,
                    mention_user: data.mention_user,
                    root_conv_id: (data.root_conv_id != undefined) ? data.root_conv_id : null,
                    secret_user: data.secret_user,
                    is_secret: data.secret_user && data.secret_user.length ? true : false,
                    file_category: data.voice_set_type ? 'voice' : cat,
                    main_msg_id: data.reply_for_msgid,
                    // other_user: data.other_user,
                    participants: data.participants,
                    company_id: data.company_id,
                    tag_list_with_user: null,
                    referenceId: data.referenceId,
                    reference_type: data.reference_type,
                    file_group: data.file_group || ""
                }
                
                if (data.tag_list && Array.isArray(data.tag_list) && data.tag_list.length > 0) {
                    sdata.tag_list = data.tag_list;
                    sdata.has_tag = 'tag';
                    for (let i = 0; i < data.tag_list.length; i++) {
                        tag_list_with_user.push({ "tag_id": data.tag_list[i], "created_by": data.user_id });
                    }
                    sdata.tag_list_with_user = JSON.stringify(tag_list_with_user);
                }
                if(data.task_id) sdata.task_id = data.task_id;
                if(data.cost_id) sdata.cost_id = data.cost_id;
                var file_data = new File(sdata);
                // console.log('file_data',sdata);
                
                file_data.save().then({

                }).catch((e)=>{
                    console.log(256, e);
                });
            });
            // models.doBatch(queries, async function (err) {
                // if (err) reject({ status: false, error: err });
                if (data.tag_list && Array.isArray(data.tag_list) && data.tag_list.length > 0) {
                    try{
                        await update_tag_count({ 
                            tag_id: data.tag_list, 
                            company_id: data.company_id, 
                            val: 1, 
                            participants: data.participants, 
                            conversation_id: data.conversation_id, 
                            msg_id: data.msgid, 
                            user_id: data.user_id,
                            file_ids: file_ids, 
                            recursive: false
                        });
                    }catch(e){
                        console.log(1416, e);
                    }
                }
                resolve({ status: true, msg: 'File info saved', data: returnData });
            // });
        } else {
            reject({ status: true, msg: 'File info saved error' });
        }
    });
}

function get_file_info(data) {
    // console.log(1016, data);
    return new Promise(async (resolve, reject) => {
        var files = await File.find({ conversation_id: data.conversation_id, is_delete: 0 });
        // models.instance.File.find({ conversation_id: data.conversation_id, is_delete: 0 }, { raw: true, allow_filtering: true }, async function (err, files) {
            // if (err) {
            //     console.log(1018, err);
            //     reject({ status: false, error: err });
            // }
            var tags = {};
            tags.result = [];
            // var tags = await get_tags({company_id: data.company_id.toString()});
            try {
                if (data.company_id && isUuid(data.company_id.toString()))
                    tags = await get_tags({ company_id: data.company_id, id: data.user_id });
            } catch (e) {
                console.log(e);
            }
            _.each(files, function (v, k) {
                v.tag_list_details = [];
                v.location = process.env.FILE_SERVER + v.location;
                if (v.tag_list != null && tags.result.length > 0) {
                    _.each(v.tag_list, function (tv, tk) {
                        _.each(tags.result, function (vv, kk) {
                            if (tv.toString() == vv.tag_id.toString()) {
                                v.tag_list_details.push(vv);
                            }
                        });
                    });
                }
            });
            resolve(files);
        // });
    });
}

function update_conversation_for_last_msg(data) {
    // console.log(data.msg_body, Buffer.from(data.msg_body, 'base64').toString());
    var cur_time = new Date().getTime();
    // var bytes = CryptoJS.AES.decrypt(data.msg_body, process.env.CRYPTO_SECRET);
    // var msg_body = bytes.toString(CryptoJS.enc.Utf8);
    // if (msg_body.indexOf('"') == 0)
    //     msg_body = (msg_body).substring(1, msg_body.length - 1);
    // if (data.is_secret === true)
    //     msg_body = "Private message";
    // msg_body = msg_body.replace(/<\/?[^>]+(>|$)/g, "");
    console.log("555555555555555555555555555555");
    Conversation.updateOne({ conversation_id: data.conversation_id, company_id: data.company_id }, { last_msg: data.msg_body, last_msg_time: cur_time, $pull: { is_active: data.sender.toString() } }).then(function (conv) {
        console.log(1610);
    }).catch((ue)=>{
        console.log("in message.js update conversation error ", ue);
    });
}

function is_the_last_msg_of_this_conv(data) {
    return new Promise(resolve =>{
        Messages.findOne({ conversation_id: data.conversation_id, company_id: data.company_id }).sort({"_id": "desc"}).then(function (msg)  {
            
            resolve(msg.msg_id.toString() === data.msg_id.toString() ? true : false);
        }).catch((e)=>{
                console.log("in message.js update conversation error ", e);
                resolve(false);
            
        });
    });
}

function get_the_last_msg_of_this_conv(data) {
    return new Promise(resolve =>{
        Messages.findOne({ conversation_id: data.conversation_id, company_id: data.company_id, has_delete: [] }).sort({"_id": "desc"}).then(function (msg) {
            if(msg)
                resolve(msg.is_secret ? {conversation_id: msg.conversation_id, company_id: msg.company_id, msg_id: msg.msg_id, msg_body: "U2FsdGVkX1+cLmnXvXKLuOqBMSEtG2n6/cQu3cekp4AWo1RFhgq3H7D9/m5m9BkV"} : msg);
            else
                resolve(false);
        }).catch((e)=>{
           
                console.log("in message.js update conversation error ", e);
                resolve(false);
            
        });
    })
}

async function update_msg_for_reply_msg(data) {
    var cur_time = new Date().getTime();
    Messages.findOneAndUpdate(
        {conversation_id: data.conversation_id, msg_id: data.reply_for_msgid}, 
        {
            $inc: {has_reply: 1, has_reply_attach: (data.is_reply_msg === 'yes' && data.msg_type.indexOf('media_attachment')>-1) ? 1 : 0 }, 
            $set: {last_reply_name: data.sender_name, last_reply_time: cur_time}
        }, {new: false}).then((doc)=>{
        }).catch((e)=>{
            if (e) console.log("in message.js update messager for reply error ", e);
        });
}

async function has_reply_attach_update(data) {
    var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.reply_for_msgid, limit: 1, is_reply: 'no', user_id: data.sender });
    // console.log("has_reply_attach = ", mainmsg["msgs"][0].has_reply_attach);
    if(mainmsg["msgs"].length){
        var hra = Number(mainmsg["msgs"][0].has_reply_attach) > 0 ? Number(mainmsg["msgs"][0].has_reply_attach) : 0;
        hra = hra > 1 ? hra - 1 : 0;
        Messages.findOneAndUpdate({ conversation_id: data.conversation_id, msg_id: data.reply_for_msgid }, { has_reply_attach: hra }, {new: false}, function (ue) {
            if (ue) console.log("in message.js has_reply_attach_update error ", ue);
            else console.log("has_reply_attach_update successfully. now = ", hra);
        });

    }
    
}

function get_rep_message(arg) {
    return new Promise(async(resolve, reject) => {
        const PAGE_SIZE = 20;
        const page = Number(arg.page) || 1;
        var query = {};
        query.is_reply_msg = 'yes';
        if (isUuid(arg.msg_id)) {
            query.reply_for_msgid = arg.msg_id;
        }
        
        var main_msg = await get_message({ type: 'one_msg', msgid: arg.msg_id, user_id: arg.user_id });

        const count_total = [{$match: query}, {$count: "total"}];
          
        const dataAggregate = [
                                {$match: query}, 
                                {$lookup:{ 
                                    from: 'files', 
                                    localField: 'msg_id', 
                                    foreignField:'msg_id',
                                    as:'all_attachment',
                                    pipeline: [
                                        {$match: {
                                            is_delete: 0
                                        }}
                                    ]
                                }},
                                {$skip: (page - 1) * PAGE_SIZE},
                                {$limit: PAGE_SIZE}
                            ];
        
        const [countResult, msgs, convs] = await Promise.all([
            Messages.aggregate(count_total),
            Messages.aggregate(dataAggregate),
            get_conversation({ conversation_id: main_msg.msgs[0].conversation_id, user_id: arg.user_id })
        ]);

        const total = countResult[0] ? countResult[0].total : 0;
        const totalPages = Math.ceil(total / PAGE_SIZE);
        let tag_ids = [];
        for(let msg of msgs){
            // msg.msg_body = CryptoJS.AES.encrypt(msg.msg_body, process.env.CRYPTO_SECRET).toString();
            let this_user = await get_user_obj(msg.sender);
            if(this_user.firstname !== undefined){
                msg.sendername = this_user.firstname + ' ' + this_user.lastname;
                msg.fnln = this_user.fnln;
                msg.senderimg = this_user.img;
                msg.senderemail = this_user.email;
                msg.sender_is_active = this_user.is_active;
            }
            if(msg.all_attachment.length>0){
                for(let i=0; i<msg.all_attachment.length; i++){
                    msg.all_attachment[i].location = process.env.FILE_SERVER + msg.all_attachment[i].location;
                    msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                        if(msg.all_attachment[i].tag_list.length>0){
                            for(let j=0; j<msg.all_attachment[i].tag_list.length; j++)
                                if(tag_ids.indexOf(msg.all_attachment[i].tag_list[j]) === -1)
                                    tag_ids.push(msg.all_attachment[i].tag_list[j]);
                        }
                }
            }
            msg.secret_user_name = [];
            msg.secret_user_details = [];
            if(msg.is_secret === true){
                msg.secret_user.forEach(async function (sv, sk) {
                    if (isUuid(sv.toString())) {
                        let this_user = await get_user_obj(sv);
                        if(this_user.firstname !== undefined && msg.secret_user_name.indexOf(this_user.firstname + ' ' + this_user.lastname) == -1) {
                            msg.secret_user_name.push(this_user.firstname + ' ' + this_user.lastname);
                            msg.secret_user_details.push({
                                id: sv,
                                firstname: this_user.firstname,
                                lastname: this_user.lastname,
                                email: this_user.email,
                                img: this_user.img,
                                fnln: this_user.fnln
                            });
                        }
                    }
                });
            }
        }

        if(tag_ids.length>0){
            let taginfo = await Tag.find({tag_id: {$in: tag_ids}}, 'tag_id tagged_by title company_id type shared_tag visibility tag_type tag_color team_list created_at update_at').lean();
            
            for(let msg of msgs){
                if(msg.all_attachment.length>0){
                    for(let i=0; i<msg.all_attachment.length; i++){
                        msg.all_attachment[i].tag_list_details = [];
                        msg.all_attachment[i].tag_list = Array.isArray(msg.all_attachment[i].tag_list) ? msg.all_attachment[i].tag_list : [];
                        if(msg.all_attachment[i].tag_list.length>0){
                            for(let j=0; j<msg.all_attachment[i].tag_list.length; j++){
                                for(let k=0; k<taginfo.length; k++){
                                    if(taginfo[k].tag_id === msg.all_attachment[i].tag_list[j])
                                        msg.all_attachment[i].tag_list_details.push(taginfo[k]);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        resolve({
            conversation: convs.length > 0 ? convs[0] : {},
            main_msg: main_msg.msgs[0],
            reply_msgs: msgs, 
            pagination: {
                page: parseInt(page),
                totalPages,
                total
            }
        });
    });
}

function _delete_link(data, type) {
    Link.find({msg_id: data.msg_id.toString()}).then(async function(l) {
            var queries = [];
            for(let i=0; i<l.length; i++){
                if(type == 'for_all')
                    Link.updateOne({url_id: l[i].url_id, company_id: l[i].company_id}, {is_delete: "1"});
                else
                    Link.updateOne({url_id: l[i].url_id, company_id: l[i].company_id}, {$push: {has_delete: data.req_user_id } });
            }
        
    }).catch((e)=>{
        console.log("Link find error ", e);
    });
}

function _hide_attachment(data) {
    // var queries = [];
    // data.all_attachment.forEach(function (v, k) {
    //     var delete_query = models.instance.File.update({ id: v.id, company_id: data.company_id.toString() }, {has_delete: {$add: [data.req_user_id]}}, { return_query: true });
    //     queries.push(delete_query);
    // });
    // if (queries.length > 0) {
    //     models.doBatch(queries, function (err) {
    //         if (err) console.log(err);
    //         else console.log("file hide successfully");
    //     });
    // }
}

async function _delete_attachment(data) {
    var queries = [];
    data.all_attachment.forEach(function (v, k) {
        queries.push(v.id);
    });
    if (queries.length > 0) {
        try{
            let d = await File.deleteMany({id: {$in: queries}});
            // console.log(d);
        }catch(e){
            console.log(1800, e);
        }
    }
}

function _delete_reply_msg(data) {
    for(let i=0; i<data.conv_participants.length; i++){
        Messages.find({ reply_for_msgid: data.msg_id, msg_status: {$in: data.conv_participants[i]}, has_delete: {$nin: data.conv_participants[i]} },  async function (err, msgs) {
            if (err) console.log("_delete_reply_msg error ", err);
            // console.log(801, msgs.length);
            if (msgs.length > 0) {
                var reply_msgids = [];
                
                for (let i = 0; i < msgs.length; i++) {
                    reply_msgids.push(msgs[i].msg_id);
                }
                // console.log(812, msg_set_del);
                let updatedata = {$push :{ has_delete : data.conv_participants[i], has_hide : data.conv_participants[i] } }
                
                if (data.delete_type == 'for_all')
                    updatedata.msg_status = null;
                else
                    updatedata.$pull = {msg_status: data.conv_participants[i]};

                Messages.updateOne({ conversation_id:data.conversation_id, msg_id: { '$in': reply_msgids } }, updatedata, async function (ue, msg) {
                    if (ue) console.log("_delete_reply_msg 2 error ", ue);
                    
                    xmpp_send_server(data.conv_participants[i], 'read_status_msg', { conversation_id: data.conversation_id, is_reply_msg: 'yes', root_msg_id: data.msg_id, read: msgs.length });

                    await znReadMsgUpdateCounter({user_id: data.conv_participants[i], conversation_id: data.conversation_id, is_reply_msg: 'yes', root_msg_id: data.msg_id, read: msgs.length, reply_mention:0});
                    console.log("Successfully reply msg delete");
                });
            }
        })
    }
}

async function delete_msg(data, callback) {
    var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, limit: 1, is_reply: data.is_reply_msg, company_id: data.company_id, user_id: data.user_id });
    // console.log(3333,mainmsg["msgs"][0],"delete_msg_function")

    try {
        if (mainmsg["msgs"].length == 0){
            // console.log(3333,mainmsg.length,"delete_msg_function")
            return callback({ status: false });
        } 
        mainmsg["msgs"][0].req_user_id = data.user_id;
        File.find({ msg_id: data.msg_id }).then(async function (f)  {
            
             if (f.length > 0) {
                // console.log(1705, f.length)

                for (let i = 0; i < f.length; i++) {
                    f[i].tag_list = Array.isArray(f[i].tag_list) ? f[i].tag_list : [];
                    if (f[i].tag_list.length > 0) {
                        await update_tag_count({ tag_id: f[i].tag_list, company_id: f[i].company_id, val: -1, conversation_id: f[i].conversation_id.toString(), msg_id: f[i].msg_id.toString(), participants: data.participants, user_id: data.user_id, file_ids: [f[i].id.toString()], recursive: false });
                    }
                }
            }
            else{
                // console.log(2033,f.length,f)
            }
        }).catch((e)=>{
            console.log(1903, e);
        });

        if (data.delete_type == 'for_me') {
         Messages.updateOne({ conversation_id:data.conversation_id, msg_id:data.msg_id }, {$push :{ has_delete : data.user_id } }).then(async function (msg ) {
                
                if (mainmsg["msgs"][0].has_reply > 0) {
                    data.conv_participants = data.participants;
                    _delete_reply_msg(data);
                }
                if (Array.isArray(mainmsg["msgs"][0].all_attachment) && mainmsg["msgs"][0].all_attachment.length > 0) {
                    _hide_attachment(mainmsg["msgs"][0]);
                }
                if(mainmsg["msgs"][0].msg_type.indexOf("link") > -1 && mainmsg["msgs"][0].url !== null){
                    _delete_link(mainmsg["msgs"][0], data.delete_type);
                }
                await znMsgUpdate(mainmsg["msgs"][0], [data.user_id], 'msg_update_delete');
                callback({ status: true });
            }).catch((e)=>{
                 console.log("in message.js delete message for me error ", e);
            });
        } else if (data.delete_type == 'for_all') {
            if (mainmsg["msgs"][0].sender.toString() == data.user_id) {
             Conversation.findOne({ conversation_id: data.conversation_id }).then(function (conv)  {
                    
                    if (conv) {
                     
                        var all_participants = conv.participants;
                        // console.log(2222,all_participants)
                        // all_participants.push('for_all');
                      Messages.updateOne({ conversation_id: data.conversation_id, msg_id:data.msg_id }, { has_delete: [...all_participants, 'for_all'] }).then(async function (result)  {
                            
                            // console.log(855, mainmsg[0].has_reply);
                            if (mainmsg["msgs"][0].has_reply > 0) {
                                data.conv_participants = conv.participants;
                                _delete_reply_msg(data);
                            }
                            if (Array.isArray(mainmsg["msgs"][0].all_attachment) && mainmsg["msgs"][0].all_attachment.length > 0) {
                                _delete_attachment(mainmsg["msgs"][0]);
                            }
                            if(mainmsg["msgs"][0].msg_type.indexOf("link") > -1 && mainmsg["msgs"][0].url !== null){
                                _delete_link(mainmsg["msgs"][0], data.delete_type);
                            }
                            for (let i = 0; i < conv.participants.length; i++) {
                                xmpp_send_server(conv.participants[i], 'read_status_msg', { conversation_id: data.conversation_id, is_reply_msg: data.is_reply_msg, root_msg_id: "", read: 1 });
                            }
                            
                            await znMsgUpdate(mainmsg["msgs"][0], conv.participants, 'msg_update_delete');

                            callback({ status: true, participants: conv.participants, msg: mainmsg["msgs"][0] });
                        }).catch((e)=>{
                            console.log("in message.js delete message for all error ", e);
                        });
                    }
                }).catch((e)=>{
                     callback({ status: false, e: e });
                });
            } else {
                callback({ status: false, error: 'You are not the message sender' });
            }
        } else {
            callback({ status: false, error: 'no valid type found' });
        }
    } catch (e) {
        console.log("*********", e);
        callback({ status: false, error: e });
    }
}

function remove_this_line(data, callback) {
    var status = false;
    Messages.updateOne({ conversation_id: data.conversation_id, msg_id: data.msg_id }, { $push: {has_hide: data.user_id } }, async function (ue, msg) {
        if (ue) console.log("in message.js remove this line for me error ", ue);
        status = true;

        /**
         * No message will not be deleted
         */
        /*
        var mainmsg = await get_message({convid: data.conversation_id, msgid: data.msg_id, limit: 1, is_reply: data.is_reply_msg, user_id: data.user_id });

        if(mainmsg[0].has_delete.indexOf('for_all') && (mainmsg[0].has_hide.length + 1) == mainmsg[0].has_delete.length){
        	models.instance.Messages.delete({ conversation_id: models.uuidFromString(data.conversation_id), msg_id: models.timeuuidFromString(data.msg_id) }, function(mde) {
        		if (mde) console.log("in message.js remove this line delete error ", mde);
        		callback({status: true, message: 'Remove for all.', data});
        	});
        }
        else 
        */
        callback({ status: status, message: 'Remove for me.', data });
    });
}

function clear_conversation(data, callback) {
    // console.log(1693, data);
    Messages.find({ conversation_id: data.conversation_id }, function (err, messages) {
        if (err) callback({ status: false, error: err });
        var msgids = [];
        // console.log(1697, messages.length);
        _.each(messages, function (v, k) {
            if (v.has_hide == null || v.has_hide.indexOf(data.user_id) == -1) {
                if (data.delete_flag == 'no' && v.has_flagged.indexOf(data.user_id) > -1) {
                    return;
                }
                if (data.delete_flag == 'yes' && v.has_flagged.indexOf(data.user_id) > -1) {
                    msgids.push(v.msg_id);
                } else if (data.delete_media == 'no' && v.msg_type == 'media_attachment') {
                    return;
                } else if (data.delete_checklist == 'no' && v.msg_type == 'checklist') {
                    return;
                } else if (data.delete_text == 'no' && v.msg_type == 'text') {
                    return;
                } else {
                    msgids.push(v.msg_id);
                }
            }
        });
        Messages.updateMany({msg_id: {$in: msgids}}, {$pull: { msg_status: data.user_id }, $push: { has_hide: data.user_id } }, { multi: true })
        .then(result => {callback({ status: true, result })})
        .catch(error => {console.log(1719, error); callback({ status: false, error: error })});
    });
}



function forward_msg(data, req) {
    return new Promise(async (resolve, reject) => {
        // var mainmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', data.is_reply_msg);
        var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, limit: 1, is_reply: data.is_reply_msg, company_id: data.company_id, user_id: data.user_id });

        _.forEach(data.conversation_lists, function (v, k) {
            Conversation.find({ conversation_id: v }, function (ce, conv) {
                if (ce) reject({ status: false, error: ce });
                // console.log(429, conv);
                mainmsg["msgs"][0].conversation_id = v;
                mainmsg["msgs"][0].sender = data.user_id;
                mainmsg["msgs"][0].sender_name = data.sender_name;
                mainmsg["msgs"][0].sender_img = data.sender_img;
                // mainmsg["msgs"][0].is_reply_msg = data.is_reply_msg;
                mainmsg["msgs"][0].is_reply_msg = 'no'; // forward msg always main msg
                mainmsg["msgs"][0].reply_for_msgid = ''; // forward msg always main msg

                mainmsg["msgs"][0].forward_by = data.user_id;
                mainmsg["msgs"][0].participants = conv[0].participants;

                if (mainmsg["msgs"][0].all_attachment != null && mainmsg["msgs"][0].all_attachment.length > 0) {
                    mainmsg["msgs"][0].attach_files = [];
                    mainmsg["msgs"][0].attach_files.allfiles = [];
                    mainmsg["msgs"][0].attach_files.imgfile = (typeof mainmsg["msgs"][0].attch_imgfile === 'undefined' || mainmsg["msgs"][0].attch_imgfile == null) ? [] : mainmsg["msgs"][0].attch_imgfile;
                    mainmsg["msgs"][0].attach_files.audiofile = (typeof mainmsg["msgs"][0].attch_audiofile === 'undefined' || mainmsg["msgs"][0].attch_audiofile == null) ? [] : mainmsg["msgs"][0].attch_audiofile;
                    mainmsg["msgs"][0].attach_files.videofile = (typeof mainmsg["msgs"][0].attch_videofile === 'undefined' || mainmsg["msgs"][0].attch_videofile == null) ? [] : mainmsg["msgs"][0].attch_videofile;
                    mainmsg["msgs"][0].attach_files.otherfile = (typeof mainmsg["msgs"][0].attch_otherfile === 'undefined' || mainmsg["msgs"][0].attch_otherfile == null) ? [] : mainmsg["msgs"][0].attch_otherfile;

                    _.forEach(mainmsg["msgs"][0].all_attachment, function (fv, fk) {
                        mainmsg["msgs"][0].attach_files.allfiles.push({
                            acl: fv.acl,
                            bucket: fv.bucket,
                            mimetype: fv.file_type,
                            key: fv.key,
                            originalname: fv.originalname,
                            size: fv.file_size
                        });
                    });
                }
                try {
                    delete mainmsg["msgs"][0].msg_id;
                    set_message(mainmsg["msgs"][0], function (res) {
                        res.msg.msg_status.forEach(function (rv, rk) {
                            // io.to(rv).emit('new_message', res.msg);
                            xmpp_send_server(rv, 'new_message', res.msg, req);
                            send_msg_firebase(rv, 'new_message', res.msg, req);
                        });
                        // io.to(data.user_id).emit('new_message', res.msg);
                        xmpp_send_server(data.user_id, 'new_message', res.msg, req);
                        send_msg_firebase(data.user_id, 'new_message', res.msg, req);
                    });
                } catch (e) {
                    reject(e);
                }
            });
        });
        resolve({ status: true });
    });
}

function file_share(data, req) {
    return new Promise(async (resolve, reject) => {
        var filelist = [];
        data.file_lists.forEach(function (v, k) {
            filelist.push(v);
        });
        var convlist = [];
        var myconv = "";
        data.conversation_lists.forEach(function (v, k) {
            if (v == data.sender) myconv = v;
            convlist.push(v);
        });
        let mb = data.msg_body && data.msg_body !== "" ? data.msg_body : "No Comments!";
        var share_msg = {};
        share_msg.sender = data.sender;
        share_msg.forward_by = data.sender; // as per Rajon requirement, file share also make as file forward
        share_msg.sender_name = data.sender_name;
        share_msg.sender_img = data.sender_img;
        share_msg.msg_text = mb.toString();
        share_msg.msg_body = CryptoJS.AES.encrypt(JSON.stringify(mb), process.env.CRYPTO_SECRET).toString();
        share_msg.attach_files = [];
        share_msg.attach_files.allfiles = [];
        share_msg.attach_files.imgfile = [];
        share_msg.attach_files.audiofile = [];
        share_msg.attach_files.videofile = [];
        share_msg.attach_files.otherfile = [];

        File.find({ id: { '$in': filelist } }, function (fe, files) {
            if (fe) reject(fe);
            if (files.length > 0) {
                files.forEach(function (fv, fk) {
                    share_msg.attach_files.allfiles.push({
                        acl: fv.acl,
                        bucket: fv.bucket,
                        mimetype: fv.file_type,
                        key: fv.key,
                        originalname: fv.originalname,
                        size: fv.file_size
                    })
                    if (fv.file_type.indexOf('image') > -1) {
                        share_msg.attach_files.imgfile.push(fv.location);
                    } else if (fv.file_type.indexOf('audio') > -1) {
                        share_msg.attach_files.audiofile.push(fv.location);
                    } else if (fv.file_type.indexOf('video') > -1) {
                        share_msg.attach_files.videofile.push(fv.location);
                    } else {
                        share_msg.attach_files.otherfile.push(fv.location);
                    }
                });

                Conversation.find({ conversation_id: { '$in': convlist } }, function (ce, convs) {
                    if (ce) reject(ce);
                    if (myconv === data.sender) {
                        convs.push({
                            conversation_id: myconv,
                            company_id: files[0].company_id,
                            participants: [myconv]
                        })
                    }
                    if (convs.length > 0) {
                        convs.forEach(function (cv, cf) {
                            share_msg.conversation_id = cv.conversation_id.toString();
                            share_msg.company_id = cv.company_id.toString();
                            share_msg.participants = cv.participants;
                            // console.log(838, share_msg);
                            try {
                                set_message(share_msg, function (res) {
                                    res.msg.msg_status.forEach(function (v, k) {
                                        // io.to(v).emit('new_message', res.msg);
                                        xmpp_send_server(v, 'new_message', res.msg, req);
                                        send_msg_firebase(v, 'new_message', res.msg, req);
                                    });
                                    // io.to(data.sender).emit('new_message', res.msg);
                                    xmpp_send_server(data.sender, 'new_message', res.msg, req);
                                    send_msg_firebase(data.sender, 'new_message', res.msg, req);
                                });
                            } catch (e) {
                                console.log(e);
                                reject(e);
                            }
                        });
                        resolve({ status: true });
                    }
                });
            }
        });
    });
}

function edit_msg(data) {
    return new Promise(async (resolve, reject) => {
        try {
            // var mainmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', '');
            var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, limit: 1, user_id: data.user_id });
            // console.log(4444,mainmsg["msgs"][0].msg_id)
            var cur_time = new Date().getTime();
            if (data.user_id == mainmsg["msgs"][0].sender.toString()) {
                var edit_history = mainmsg["msgs"][0].edit_history;
                let bytes = CryptoJS.AES.decrypt(mainmsg["msgs"][0].msg_body, process.env.CRYPTO_SECRET);
                let oldmsgbody = bytes.toString(CryptoJS.enc.Utf8);
                if (oldmsgbody.indexOf('"') == 0)
                    oldmsgbody = (oldmsgbody).substring(1, oldmsgbody.length - 1);
                oldmsgbody = oldmsgbody.trim();
                var jsondata = JSON.stringify({ msg_body: oldmsgbody, update_at: cur_time });
                if (edit_history == null || edit_history == "") {
                    edit_history = jsondata.toString();
                } else {
                    edit_history = edit_history + '@_$cUsJs' + jsondata.toString();
                }

                mainmsg["msgs"][0].edit_history = edit_history;
                mainmsg["msgs"][0].edit_status = cur_time;
                mainmsg["msgs"][0].msg_body = data.new_msg_body;
                mainmsg["msgs"][0].msg_text = mainmsg["msgs"][0].msg_text ? mainmsg["msgs"][0].msg_text + ' ' + oldmsgbody : oldmsgbody;

                Messages.updateOne({ conversation_id: mainmsg["msgs"][0].conversation_id, msg_id: mainmsg["msgs"][0].msg_id }, {
                    msg_body: mainmsg["msgs"][0].msg_body,
                    edit_history: mainmsg["msgs"][0].edit_history,
                    edit_status: mainmsg["msgs"][0].edit_status.toString(),
                    msg_text: mainmsg["msgs"][0].msg_text,
                    last_update_time: cur_time,
                    created_at:cur_time
                }, async function (ue, msg) {
                    if (ue) console.log("in message.js update messager for edit_msg error ", ue);
                    // var newmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', '');
                    var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, limit: 1, user_id: data.user_id });                    
                    resolve({ status: true, msg: newmsg["msgs"][0] });
                });
            } else {
                reject({ status: false, error: 'You are not authorize to edit this message.' });
            }
        } catch (e) {
            reject({ status: false, error: e });
        }

    });
}

function update_task(data) {
    // return new Promise(async (resolve, reject) => {
    //     try {
    //         // var mainmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', '');
    //         var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, limit: 1, user_id: data.user_id });
    //         var um = {};
    //         var cur_time = new Date().getTime();
    //         um.msg_type = data.msg_type.indexOf('task') > -1 ? data.msg_type : mainmsg[0].msg_type;

    //         if (um.msg_type.indexOf('task') > -1){
    //             um.task_start_date = data.task_start_date ? data.task_start_date : mainmsg[0].task_start_date;
    //             um.task_due_date = data.task_due_date ? data.task_due_date : mainmsg[0].task_due_date;
    //             um.task_review = data.task_review ? data.task_review : mainmsg[0].task_review;
    //             um.task_keywords = data.task_keywords ? data.task_keywords : mainmsg[0].task_keywords;
    //             um.task_progress = data.task_progress ? data.task_progress : mainmsg[0].task_progress;
    //             um.task_forecasted_hours = data.task_forecasted_hours ? data.task_forecasted_hours : mainmsg[0].task_forecasted_hours;
    //             um.task_actual_hours = data.task_actual_hours ? data.task_actual_hours : mainmsg[0].task_actual_hours;
    //             um.task_hours_variance = data.task_hours_variance ? data.task_hours_variance : mainmsg[0].task_hours_variance;
    //             um.task_forecasted_cost = data.task_forecasted_cost ? data.task_forecasted_cost : mainmsg[0].task_forecasted_cost;
    //             um.task_actual_cost = data.task_actual_cost ? data.task_actual_cost : mainmsg[0].task_actual_cost;
    //             um.task_cost_variance = data.task_cost_variance ? data.task_cost_variance  : mainmsg[0].task_cost_variance;
    //             um.task_description = data.task_description ? data.task_description  : mainmsg[0].task_description;
    //             um.task_notes = data.task_notes ? data.task_notes  : mainmsg[0].task_notes;
    //             um.assign_to = Array.isArray(data.assign_to) && data.assign_to.length > 0 ? data.assign_to  : mainmsg[0].assign_to;
    //             um.task_observers = Array.isArray(data.task_observers) && data.task_observers.length > 0 ? data.task_observers  : mainmsg[0].task_observers;
    //             // um.msg_body = data.msg_body ? data.msg_body : mainmsg[0].msg_body;
    //             um.last_update_time = cur_time;
                
    //             var bytes = CryptoJS.AES.decrypt(data.msg_body, process.env.CRYPTO_SECRET);
    //             um.msg_body = bytes.toString(CryptoJS.enc.Utf8);
    //             um.msg_text = um.msg_body;
    //             console.log(um.msg_body, um.msg_text);

    //             models.instance.Messages.update({ conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id }, um, update_if_exists, async function (ue, msg) {
    //                 if (ue) console.log("in message.js update messager for edit_msg error ", ue);
    //                 // var newmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', '');
    //                 var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, limit: 1, user_id: data.user_id });

    //                 resolve({ status: true, msg: newmsg[0] });
    //             });
    //         } else {
    //             reject({ status: false, error: 'This is not a task messages.' });
    //         }
    //     } catch (e) {
    //         reject({ status: false, error: e });
    //     }

    // });
}

var check_reac_emoji_list = (msg_id, uid, callback) => {

    MessagesEmojis.find({msg_id, user_id: uid }, function (err,emoji_user) {
        if (err) callback({ status: false, err: err });
        else{
            // console.log(66666,emoji_user)
            callback({ status: true, result: emoji_user });
            
        
        }
    });
};




var add_reac_emoji = (conversation_id, msg_id, user_id, user_fullname, emoji, company_id, p, callback) => {
    Messages.find({ conversation_id, msg_id }, function (err, message) {
        if (err) {
            console.log(error);
        } else {
            var messageEmoji = new  MessagesEmojis({
                msg_id: msg_id,
                user_id: user_id,
                user_fullname: user_fullname,
                emoji_name: emoji,
                company_id: company_id
            });
            // console.log(545, {msg_id,user_id,user_fullname,emoji,company_id});
            messageEmoji.save().then(function () {
                console.log('Ok');
            }).catch(function (err) {
                console.log(549, err);
            });
            if (message[0].has_emoji['folded_hands'] === undefined) message[0].has_emoji['folded_hands'] = 0;
            
            if (message[0].has_emoji['check_mark'] === undefined)   message[0].has_emoji['check_mark'] = 0;
            
            if (message[0].has_emoji['grinning'] === undefined)  message[0].has_emoji['grinning'] = 0;
            
            if (message[0].has_emoji['joy'] === undefined)  message[0].has_emoji['joy'] = 0;
            
            if (message[0].has_emoji['open_mouth'] === undefined)  message[0].has_emoji['open_mouth'] = 0;
            
            if (message[0].has_emoji['disappointed_relieved'] === undefined)  message[0].has_emoji['disappointed_relieved'] = 0;
            
            if (message[0].has_emoji['rage'] === undefined)  message[0].has_emoji['rage'] = 0;
            
            if (message[0].has_emoji['thumbsup'] === undefined)  message[0].has_emoji['thumbsup'] = 0;
            
            if (message[0].has_emoji['heart'] === undefined)  message[0].has_emoji['heart'] = 0;
            
            if (message[0].has_emoji['thumbsdown'] === undefined)  message[0].has_emoji['thumbsdown'] = 0;
            
            message[0].has_emoji[emoji] = message[0].has_emoji[emoji] + 1;
            
            Messages.updateOne({conversation_id,msg_id}, { $set: { has_emoji: message[0].has_emoji }}, function (err) {
                if (err) callback({ status: false, err: err });
                else {
                    Messages.find({ conversation_id, msg_id }, async function (err, message) {
                        let this_user = await get_user_obj(message[0].sender);
                        if(this_user.firstname !== undefined){
                            message[0].sendername = this_user.firstname + ' ' + this_user.lastname;
                            message[0].fnln = this_user.fnln;
                            message[0].senderimg = this_user.img;
                            message[0].senderemail = this_user.email;
                            message[0].sender_is_active = this_user.is_active;
                        }

                        await znMsgUpdate(message[0], p, 'msg_update_emoji');
                        callback({ status: true, rep: 'add', is_reply_msg: message[0].is_reply_msg, has_emoji: message[0].has_emoji });
                    }).lean();
                }
            });
        }
    }).lean();
};
var delete_reac_emoji = (conversation_id, msg_id, uid, emoji, p, callback) => {
    MessagesEmojis.deleteOne({ msg_id: msg_id, user_id: uid }, function (err) {
        if (err) {
            callback({ status: false, result: err });
        } else {

            Messages.find({ conversation_id, msg_id } , function (err, message) {
                if (err) {
                    console.log(error);
                } else {
                    message[0].has_emoji[emoji] = message[0].has_emoji[emoji] - 1;

                    Messages.updateOne({conversation_id,msg_id}, { $set: { has_emoji: message[0].has_emoji }}, function (err) {
                        if (err) callback({ status: false, err: err });
                        else {
                            Messages.find({ conversation_id, msg_id }, async function (err, message) {
                                let this_user = await get_user_obj(message[0].sender);
                                if(this_user.firstname !== undefined){
                                    message[0].sendername = this_user.firstname + ' ' + this_user.lastname;
                                    message[0].fnln = this_user.fnln;
                                    message[0].senderimg = this_user.img;
                                    message[0].senderemail = this_user.email;
                                    message[0].sender_is_active = this_user.is_active;
                                }
                                await znMsgUpdate(message[0], p, 'msg_update_emoji');
                                callback({ status: true, rep: 'delete', is_reply_msg: message[0].is_reply_msg, has_emoji: message[0].has_emoji });
                            }).lean();
                       }
                   });

             
                }
            }).lean();
        }
    });
};
var update_reac_emoji = (conversation_id, msg_id, uid, emoji, p, callback) => {
    
    MessagesEmojis.find({msg_id, user_id: uid }, function (err,emoji_user) {
        if (err) callback({ status: false, err: err });
        else {
            var old_rec = emoji_user[0].emoji_name;
            MessagesEmojis.updateOne({ msg_id: msg_id, user_id: uid }, { emoji_name: emoji }, function (err2) {
                if (err2) callback({ status: false, err: err2 });
                else {
                    Messages.find({ conversation_id, msg_id},  function (err3, message) {
                        if (message[0].has_emoji['folded_hands'] === undefined) message[0].has_emoji['folded_hands'] = 0;
            
                        if (message[0].has_emoji['check_mark'] === undefined)   message[0].has_emoji['check_mark'] = 0;
                        
                        if (message[0].has_emoji['grinning'] === undefined)  message[0].has_emoji['grinning'] = 0;
                        
                        if (message[0].has_emoji['joy'] === undefined)  message[0].has_emoji['joy'] = 0;
                        
                        if (message[0].has_emoji['open_mouth'] === undefined)  message[0].has_emoji['open_mouth'] = 0;
                        
                        if (message[0].has_emoji['disappointed_relieved'] === undefined)  message[0].has_emoji['disappointed_relieved'] = 0;
                        
                        if  (message[0].has_emoji['rage'] === undefined)  message[0].has_emoji['rage'] = 0;
                        
                        if (message[0].has_emoji['thumbsup'] === undefined)  message[0].has_emoji['thumbsup'] = 0;
                        
                        if (message[0].has_emoji['heart'] === undefined)  message[0].has_emoji['heart'] = 0;
                        
                        if (message[0].has_emoji['thumbsdown'] === undefined)  message[0].has_emoji['thumbsdown'] = 0;

                        message[0].has_emoji[emoji] = message[0].has_emoji[emoji] + 1;
                        message[0].has_emoji[old_rec] = message[0].has_emoji[old_rec] > 0 ? message[0].has_emoji[old_rec] - 1 : 0;
                        
                        Messages.updateOne({conversation_id,msg_id}, { $set: { has_emoji: message[0].has_emoji }}, function (err) {
                            if (err) callback({ status: false, err: err });
                            else {
                                Messages.find({ conversation_id, msg_id }, async function (err, message) {
                                    let this_user = await get_user_obj(message[0].sender);
                                    if(this_user.firstname !== undefined){
                                        message[0].sendername = this_user.firstname + ' ' + this_user.lastname;
                                        message[0].fnln = this_user.fnln;
                                        message[0].senderimg = this_user.img;
                                        message[0].senderemail = this_user.email;
                                        message[0].sender_is_active = this_user.is_active;
                                    }
                                    await znMsgUpdate(message[0], p, 'msg_update_emoji');
                                   callback({ status: true, rep: 'update', is_reply_msg: message[0].is_reply_msg, has_emoji: message[0].has_emoji });
                               }).lean();
                            }
                        });
                    }).lean();
                }
            });
        }
    });
}

var view_reac_emoji_list = (msg_id, emoji, callback) => {
    MessagesEmojis.find({ msg_id: msg_id, emoji_name: emoji }, function (err, emoji_user_list) {
        if (err) {
            callback({ status: false, result: err });
        } else {
            callback({ status: true, result: emoji_user_list });
        }
    });
};

var flag_unflag = (msg_id, uid, is_add, conversation_id, callback) => {
    // console.log(55555,callback,msg_id,is_add);

    if(is_add == 'no') {
        Messages.updateOne({conversation_id,msg_id}, { $pull: { has_flagged: uid }},async function (err) {
            if (err) callback({ status: false, err: err });
            else{
                await znMsgUpdate({conversation_id, msg_id, is_add}, [uid], 'msg_update_flag_unflag');
                callback({ status: true, result: 'Remove flag successfully.' });
            }
        });

  
    } else if (is_add == 'yes') {
        Messages.updateOne({conversation_id,msg_id}, { $push: { has_flagged: uid }}).then(async function ()  {
                await znMsgUpdate({conversation_id, msg_id, is_add}, [uid], 'msg_update_flag_unflag');
                callback({ status: true, result: 'add flag successfully.' });
            
        }).catch((e)=>{
             callback({ status: false, err: e });
        });
    }
};

// function get_user_obj(query) {
//     return new Promise((resolve, reject) => {
//         Users.find(query, 'id email company_id firstname lastname dept designation access conference_id  phone is_active img role createdat created_by', function(error, users) {
//             if(error) reject({status: false, error});
//             var users_obj = {};
//             if (users) {
//                 users.forEach(function(v, k) {
//                     v.lastname = v.lastname ? v.lastname : "";
//                     v.fullname = v.firstname + " " + v.lastname;
//                     v.img = process.env.FILE_SERVER + 'profile-pic/Photos/' + v.img;
//                     v.fnln = fnln(v);
//                     v.role = v.role === '' ? 'Member' : v.role;
//                     // OHS Global Inc.
//                     if (v.company_id.toString() == '79df4d50-ce4a-11e9-90f6-e9974a7dde09' && !isUuid(v.created_by))
//                         v.created_by = "0979ae3a-1fdc-4859-a702-cba13a61e471";
//                     // ITL
//                     else if (v.company_id.toString() == 'f4e2ccb0-5a3a-11ec-9799-a4ea5366ede0' && !isUuid(v.created_by))
//                         v.created_by = "02138189-ca7b-4627-8ad8-a84bb73b3f76";
//                     else if (!isUuid(v.created_by))
//                         v.created_by = v.id;

//                     users_obj[v.id] = v;
//                 });
//                 resolve(query.id ? users[0] : users_obj);
//             }
//             else{
//                 resolve({});
//             }
//         });
//     });
// }

function get_tags(data) {
    // console.log(1782, data);
    return new Promise(async (resolve, reject) => {
        var query = { company_id: data.company_id };
        if(isUuid(data.team_id)){
            query.$or = [ 
                {
                    $and: [
                        { tag_type: 'private' },
                        { tagged_by: {$in: [data.id]} },
                    ]
                },
                {
                    $and: [
                        { tag_type: 'public' },
                        { team_list: { $in: [data.team_id] }} 
                    ]
                },
                {
                    $and: [
                        { tag_type: 'public' },
                        { team_list: []} 
                    ]
                },
                {
                    $and: [
                        { tag_type: 'public' },
                        { team_list: null} 
                    ]
                }
            ];
        }
        else {
            query.$or = [ 
                {
                    $and: [
                        { tag_type: 'private' },
                        { tagged_by: {$in: [data.id]} },
                    ]
                },
                { tag_type: 'public' }
            ];
        }
        if (data.conversation_id) {
            query.conversation_ids = new RegExp(`@${data.conversation_id}@`);
        }

        let q = {company_id: data.company_id, user_id: data.id};
        if(isUuid(data.conversation_id))
            q.conversation_id = data.conversation_id;
        let nr = await Tagcount.aggregate([
            {$match: q},
            // {$lookup:{ 
            //     from: 'tags', 
            //     localField: 'tag_id', 
            //     foreignField:'tag_id',
            //     as:'tag_details_info' 
            // }},
            {$group: {
                    _id: "$tag_id",
                    count: { $sum: 1 },
                    // tag_details_info: { $first: "$tag_details_info" }
                }
            }
        ]).exec();

        // console.log(`Start ${new Date().getTime()}`, JSON.stringify(query));
        var result = await Tag.aggregate([
            { $match: query}, 
            { $project: 
                {
                    _id: 1,
                    tag_id: 1,
                    tagged_by: 1,
                    title: 1,
                    company_id: 1,
                    type: 1,
                    tag_color: 1,
                    created_at: 1,
                    updated_at: 1,
                    updated_by: 1,
                    team_list: 1,
                    shared_tag: 1,
                    tag_type: 1
                }
            }
        ]).exec();
        // console.log(`End ${new Date().getTime()}`);
        if (isUuid(data.id)) {
            var public_tag = [];
            var private_tag = [];
            var task_tag = [];
            var teamobj = {};
            if (all_teams.hasOwnProperty(data.company_id)) {
                for (let j = 0; j < all_teams[data.company_id].length; j++) {
                    teamobj[all_teams[data.company_id][j].team_id.toString()] = {
                        title: all_teams[data.company_id][j].team_title,
                        participants: all_teams[data.company_id][j].participants
                    }
                }
                // console.log(1799, teamobj);
                result.forEach(function (v, k) {
                    let uid = v.tagged_by.toString();
                    v.created_by_name = "";
                    if (all_users[data.company_id].hasOwnProperty(uid))
                        v.created_by_name = all_users[data.company_id][uid].firstname + ' ' + all_users[data.company_id][uid].lastname;
                    v.team_list = Array.isArray(v.team_list) ? v.team_list : [];
                    v.team_list_name = [];
                    if (v.team_list.length > 0) {
                        for (let i = 0; i < v.team_list.length; i++) {
                            if (teamobj.hasOwnProperty(v.team_list[i]))
                                v.team_list_name.push(teamobj[v.team_list[i]].title);
                        }
                    }
                    v.use_count = v.my_use_count_int = v.i_connected = v.tag_use_count || 0;
                    for(let i=0; i<nr.length; i++){
                        if(nr[i]._id === v.tag_id) v.use_count = nr[i].count;
                    }
                    v.conversation_ids = '';
                    v.connected_user_ids = '';
                    v.my_use_count = '';
                    v.user_use_count = '';
                    v.disabled = v.shared_tag ? true : false;
                    // if (v.my_use_count)
                    //     v.my_use_count_int = v.my_use_count.hasOwnProperty(data.id) ? v.my_use_count[data.id] : 0;
                    // if (v.user_use_count)
                    //     v.i_connected = v.user_use_count.hasOwnProperty(data.id) ? v.user_use_count[data.id] : 0;

                    if (v.tag_type == 'public') {
                        if (isUuid(data.team_id) && v.team_list.indexOf(data.team_id) > -1) {
                            if (teamobj.hasOwnProperty(data.team_id)) {
                                if (teamobj[data.team_id].participants.indexOf(data.id) > -1) {
                                    public_tag.push(v);
                                }
                            }
                        } else if (isUuid(data.team_id) && v.team_list.length == 0) {
                            public_tag.push(v);
                        } else if (!isUuid(data.team_id)) {
                            public_tag.push(v);
                        }
                    } else if (v.tagged_by.toString() == data.id)
                        private_tag.push(v);
                });
                if(data.upload_type == "task"){
                    task_tag = await Tag.aggregate([
                        { $match: {"tag_type": "task"}}, 
                        { $project: 
                            {
                                _id: 1,
                                tag_id: 1,
                                tagged_by: 1,
                                title: 1,
                                company_id: 1,
                                type: 1,
                                tag_color: 1,
                                created_at: 1,
                                updated_at: 1,
                                updated_by: 1,
                                team_list: 1,
                                shared_tag: 1,
                                tag_type: 1
                            }
                        }
                    ]).exec();

                }
            
                
                resolve({ status: true, public_tag, private_tag,task_tag, result: [...private_tag, ...public_tag,...task_tag] });
            } else {
                resolve({ status: false, public_tag, private_tag, result: [] });
            }
        } else {
            resolve({ status: true, result });
        }
    
    });
}
// function get_tags(data) {
//     // console.log(1782, data);
//     return new Promise(async (resolve, reject) => {
//         // models.instance.UserTag.find({company_id: data.company_id}, {raw: true}, function(err, result){
//         var query = { company_id: data.company_id };
//         if(isUuid(data.team_id)){
//             query.$or = [ 
//                 {
//                     $and: [
//                         { tag_type: 'private' },
//                         { tagged_by: {$in: [data.id]} },
//                     ]
//                 },
//                 {
//                     $and: [
//                         { tag_type: 'public' },
//                         { team_list: { $in: [data.team_id] }} 
//                     ]
//                 },
//                 {
//                     $and: [
//                         { tag_type: 'public' },
//                         { team_list: []} 
//                     ]
//                 }
//             ];
//         }
//         else {
//             query.$or = [ 
//                 {
//                     $and: [
//                         { tag_type: 'private' },
//                         { tagged_by: {$in: [data.id]} },
//                     ]
//                 },
//                 {
//                     $and: [
//                         { tag_type: 'public' },
//                         { team_list: []} 
//                     ]
//                 }
//             ];
//         }
//         if (data.from === 'mytag') {
//             query.connected_user = { $contains: data.id };
//         }
//         if (data.conversation_id) {
//             query.conversation_ids = new RegExp(`@${data.conversation_id}@`);
//         }
//         console.log(`Start ${new Date().getTime()}`, query);
//         var result = await Tag.aggregate([
//             { $match: query}, 
//             { $project: 
//                 {
//                     _id: 1,
//                     tag_id: 1,
//                     tagged_by: 1,
//                     title: 1,
//                     company_id: 1,
//                     type: 1,
//                     tag_color: 1,
//                     created_at: 1,
//                     updated_at: 1,
//                     updated_by: 1,
//                     team_list: 1,
//                     favourite: {
//                         $cond: {
//                             if: { $in: [data.id, "$favourite"] },
//                             then: [data.id],
//                             else: []
//                         }
//                     },
//                     shared_tag: 1,
//                     tag_type: 1,
//                     tag_use_count: 1,
//                     // tag_use_count: {
//                     //     $size: { $split: ["$conversation_ids", "@@"] }
//                     // }
//                 }
//             }
//         ]).exec();
//         console.log(`End ${new Date().getTime()}`);
//         if (isUuid(data.id)) {
//             var public_tag = [];
//             var private_tag = [];
//             var teamobj = {};
//             if (all_teams.hasOwnProperty(data.company_id)) {
//                 for (let j = 0; j < all_teams[data.company_id].length; j++) {
//                     teamobj[all_teams[data.company_id][j].team_id.toString()] = {
//                         title: all_teams[data.company_id][j].team_title,
//                         participants: all_teams[data.company_id][j].participants
//                     }
//                 }
//                 // console.log(1799, teamobj);
//                 result.forEach(function (v, k) {
//                     // let v = Object.assign({}, f.toJSON());
//                     let uid = v.tagged_by.toString();
//                     v.created_by_name = "";
//                     if (all_users[data.company_id].hasOwnProperty(uid))
//                         v.created_by_name = all_users[data.company_id][uid].firstname + ' ' + all_users[data.company_id][uid].lastname;
//                     v.team_list = Array.isArray(v.team_list) ? v.team_list : [];
//                     v.team_list_name = [];
//                     // console.log(1758, v.team_list)
//                     if (v.team_list.length > 0) {
//                         for (let i = 0; i < v.team_list.length; i++) {
//                             if (teamobj.hasOwnProperty(v.team_list[i]))
//                                 v.team_list_name.push(teamobj[v.team_list[i]].title);
//                         }
//                     }

//                     // v.favourite = v.favourite.indexOf(data.id) > -1 ? [data.id] : [];
//                     // if(v.conversation_ids)
//                     //     v.use_count = v.my_use_count_int = v.i_connected = (v.conversation_ids.split("@@")).length;
//                     // else
//                     v.use_count = v.my_use_count_int = v.i_connected = v.tag_use_count || 0;
//                     v.conversation_ids = '';
//                     v.connected_user_ids = '';
//                     v.my_use_count = '';
//                     v.user_use_count = '';
//                     v.disabled = v.shared_tag ? true : false;
//                     // if (v.my_use_count)
//                     //     v.my_use_count_int = v.my_use_count.hasOwnProperty(data.id) ? v.my_use_count[data.id] : 0;
//                     // if (v.user_use_count)
//                     //     v.i_connected = v.user_use_count.hasOwnProperty(data.id) ? v.user_use_count[data.id] : 0;

//                     if (v.tag_type == 'public') {
//                         // console.log(1818)
//                         if (isUuid(data.team_id) && v.team_list.indexOf(data.team_id) > -1) {
//                             // console.log(1820)
//                             if (teamobj.hasOwnProperty(data.team_id)) {
//                                 // console.log(1822)
//                                 if (teamobj[data.team_id].participants.indexOf(data.id) > -1) {
//                                     // console.log(1824)
//                                     public_tag.push(v);
//                                 }
//                             }
//                         } else if (isUuid(data.team_id) && v.team_list.length == 0) {
//                             public_tag.push(v);
//                         } else if (!isUuid(data.team_id)) {
//                             public_tag.push(v);
//                         }
//                     } else if (v.tagged_by.toString() == data.id)
//                         private_tag.push(v);
//                 });
//                 resolve({ status: true, public_tag, private_tag, result: [...private_tag, ...public_tag] });
//             } else {
//                 resolve({ status: false, public_tag, private_tag, result: [] });
//             }
//         } else {
//             resolve({ status: true, result });
//         }
    
//     });
// }

function get_tags2(data) {
    // console.log(1782, data);
    return new Promise(async (resolve, reject) => {
        // let s = Date.now();
        // console.log(`Start count query at ${s}`);
        let q = {company_id: data.company_id, user_id: data.id};
        if(isUuid(data.conversation_id))
            q.conversation_id = data.conversation_id;
        let nr = await Tagcount.aggregate([
            {$match: q},
            {$lookup:{ 
                from: 'tags', 
                localField: 'tag_id', 
                foreignField:'tag_id',
                as:'tag_details_info' 
            }},
            {$group: {
                    _id: "$tag_id",
                    count: { $sum: 1 },
                    tag_details_info: { $first: "$tag_details_info" }
                }
            }
        ]).exec();
        // console.log(`End current query at ${Date.now()} = ${Date.now() - s}ms`);
        // console.log(nr[0]);
        let tagids = [];
        for(let i=0; i<nr.length; i++){
            tagids.push(nr[i]._id);
        }
        let query = {
            company_id: data.company_id,
            $or: [
                {tag_id: {$in: tagids}}, 
                {$and: [
                    { tag_type: 'private' },
                    { tagged_by: data.id }]
                },
                {title: 'UNTAGGED FILES'}
            ]
        };
        if(data.start_date){
            data.start_date = new Date(data.start_date).setHours(00, 00, 00, 000);
            query.created_at = { $gte: new Date(data.start_date) };

            if(data.start_date && data.end_date){
                data.end_date = new Date(data.end_date).setHours(23, 59, 59, 999);
                query.created_at = { $gte: new Date(data.start_date), $lte: new Date(data.end_date) };
            }
        }
        
        let result = await Tag.aggregate([
            {$match: query}
        ]).exec();
        let utq = {participants: {$in: [data.id]}, is_delete: 0, has_delete: {$nin: [data.id]}, $and: [{$or: [{tag_list: []}, {tag_list: null}, {tag_list: 'null'}]}], $or:[ { $and: [ { is_secret: true }, { secret_user: { $in: [data.id] }} ] }, { is_secret: false } ]};
        if(data.conversation_id) utq.conversation_id = data.conversation_id;
        let utfc = await File.countDocuments(utq).exec();
        if (isUuid(data.id)) {
            var public_tag = [];
            var private_tag = [];
            var teamobj = {};
            var all_teams = await Team.find({company_id: data.company_id}).exec();
            if (all_teams.length>0) {
                for (let j = 0; j < all_teams.length; j++) {
                    teamobj[all_teams[j].team_id.toString()] = {
                        title: all_teams[j].team_title,
                        participants: all_teams[j].participants
                    }
                }
                // console.log(2268, {company_id: data.company_id});
                result.forEach(v => {
                    
                        v.conversation_ids = 0;
                        for(let i=0; i<nr.length; i++){
                            if(nr[i]._id.toString() === v.tag_id.toString()){
                                v.conversation_ids = nr[i].count;
                            }
                        }
                        if(v.title === 'UNTAGGED FILES')
                            v.conversation_ids = utfc;

                        let use = v.conversation_ids === "" ? 0 : v.conversation_ids;
                        let uid = v.tagged_by.toString();
                        v.created_by_name = "";
                        v.connected_user_ids = '';
                        if (all_users[data.company_id].hasOwnProperty(uid))
                            v.created_by_name = all_users[data.company_id][uid].fullname;
                        v.team_list_name = [];
                        if (v.team_list && v.team_list.length > 0) {
                            for (let i = 0; i < v.team_list.length; i++) {
                                if (teamobj.hasOwnProperty(v.team_list[i]))
                                    v.team_list_name.push(teamobj[v.team_list[i]].title);
                            }
                        }

                        v.favourite = v.favourite.indexOf(data.id) > -1 ? [data.id] : [];
                        v.team_list = Array.isArray(v.team_list) ? v.team_list : [];
                        v.use_count = v.my_use_count_int = v.i_connected = use;
                        v.conversation_ids = "";
                        v.connected_user = [];
                        v.user_use_count = {};
                        v.my_use_count = {};
                        if (v.tag_type == 'public') {
                            if (isUuid(data.team_id) && v.team_list.indexOf(data.team_id) > -1) {
                                if (teamobj.hasOwnProperty(data.team_id)) {
                                    if (teamobj[data.team_id].participants.indexOf(data.id) > -1) {
                                        public_tag.push(v);
                                    }
                                }
                            } else if (isUuid(data.team_id) && v.team_list.length == 0) {
                                public_tag.push(v);
                            } else if (!isUuid(data.team_id)) {
                                public_tag.push(v);
                            }
                        } else if (v.tagged_by.toString() == data.id)
                            private_tag.push(v);
                    
                });
                // console.log("==============",result[0].i_connected);
                resolve({ status: true, public_tag, private_tag, result: [...private_tag, ...public_tag] });
            } else {
                resolve({ status: false, public_tag, private_tag, result: [] });
            }
        } else {
            resolve({ status: true, result });
        }
    });
}

// function get_all_tags(data) {
//     // console.log(1782, data);
//     return new Promise((resolve, reject) => {
//         // models.instance.UserTag.find({company_id: data.company_id}, {raw: true}, function(err, result){
//         var query = { company_id: data.company_id };
//         // var query = { connected_user_ids: { $like: '%@'+data.id+'@%' } };
//         // query.tagged_by = models.uuidFromString(data.id);
//         // if (data.from === 'mytag') {
//         //     query.connected_user = { $contains: data.id };
//         // }
//         if (data.conversation_id) {
//             query.conversation_ids = { $like: '%@'+data.conversation_id+'@%' };
//         }
//         // models.instance.Tag.find(query, { raw: true, allow_filtering: true }, function (err, result) {
//         //     if (err) {
//         //         reject({ error: err.name, message: err.message });
//         //     } else {
//         //         if (isUuid(data.id)) {
//         //             var public_tag = [];
//         //             var private_tag = [];
//         //             var teamobj = {};
//         //             if (all_teams.hasOwnProperty(data.company_id)) {
//         //                 for (let j = 0; j < all_teams[data.company_id].length; j++) {
//         //                     teamobj[all_teams[data.company_id][j].team_id.toString()] = {
//         //                         title: all_teams[data.company_id][j].team_title,
//         //                         participants: all_teams[data.company_id][j].participants
//         //                     }
//         //                 }
//         //                 console.log(2623, result.length);
//         //                 result.forEach(function (v, k) {
//         //                     v.connected_user_ids = v.connected_user_ids !== null ? v.connected_user_ids : "";
//         //                     if(v.connected_user_ids.indexOf('@'+data.id+'@') > -1 || (v.tagged_by.toString() == data.id && v.tag_type == 'private')){
//         //                         let uid = v.tagged_by.toString();
//         //                         v.created_by_name = "";
//         //                         v.connected_user_ids = '';
//         //                         if (all_users[data.company_id].hasOwnProperty(uid))
//         //                             v.created_by_name = all_users[data.company_id][uid].firstname + ' ' + all_users[data.company_id][uid].lastname;
//         //                         v.team_list = Array.isArray(v.team_list) ? v.team_list : [];
//         //                         v.team_list_name = [];
//         //                         // console.log(1758, v.team_list)
//         //                         if (v.team_list.length > 0) {
//         //                             for (let i = 0; i < v.team_list.length; i++) {
//         //                                 if (teamobj.hasOwnProperty(v.team_list[i]))
//         //                                     v.team_list_name.push(teamobj[v.team_list[i]].title);
//         //                             }
//         //                         }

//         //                         v.favourite = v.favourite === null ? [] : v.favourite;
//         //                         v.favourite = v.favourite.indexOf(data.id) > -1 ? [data.id] : [];
//         //                         if(v.conversation_ids)
//         //                             v.use_count = v.my_use_count_int = v.i_connected = (v.conversation_ids.split("@@")).length;
//         //                         else
//         //                             v.use_count = v.my_use_count_int = v.i_connected = 0;
//         //                         v.conversation_ids = '';
//         //                         // v.use_count = v.use_count === null ? 0 : v.use_count;
//         //                         // v.my_use_count_int = 0;
//         //                         // v.i_connected = 0;
//         //                         // if (v.my_use_count)
//         //                         //     v.my_use_count_int = v.my_use_count.hasOwnProperty(data.id) ? v.my_use_count[data.id] : 0;
//         //                         // if (v.user_use_count)
//         //                         //     v.i_connected = v.user_use_count.hasOwnProperty(data.id) ? v.user_use_count[data.id] : 0;

//         //                         if (v.tag_type == 'public') {
//         //                             // console.log(1818)
//         //                             if (isUuid(data.team_id) && v.team_list.indexOf(data.team_id) > -1) {
//         //                                 console.log(1820)
//         //                                 if (teamobj.hasOwnProperty(data.team_id)) {
//         //                                     console.log(1822)
//         //                                     if (teamobj[data.team_id].participants.indexOf(data.id) > -1) {
//         //                                         console.log(1824)
//         //                                         public_tag.push(v);
//         //                                     }
//         //                                 }
//         //                             } else if (isUuid(data.team_id) && v.team_list.length == 0) {
//         //                                 public_tag.push(v);
//         //                             } else if (!isUuid(data.team_id)) {
//         //                                 public_tag.push(v);
//         //                             }
//         //                         } else if (v.tagged_by.toString() == data.id)
//         //                             private_tag.push(v);
//         //                     }
//         //                 });
//         //                 resolve({ status: true, public_tag, private_tag, result: [...private_tag, ...public_tag] });
//         //             } else {
//         //                 resolve({ status: false, public_tag, private_tag, result: [] });
//         //             }
//         //         } else {
//         //             resolve({ status: true, result });
//         //         }
//         //     }
//         // });
//     });
// }

function mute_conversation(data, callback) {
    //console.log(3031,"dsd")
    var query = { conversation_id: data.conversation_id, company_id: data.company_id };
    if (data.type == 'add') {
       // console.log(3034,"adsd")
        var nowUnix = moment(data.mute_start_time + ' ' + data.mute_timezone, 'llll Z').unix();
        var endUnix = moment(data.mute_end_time + ' ' + data.mute_timezone, 'llll Z').unix();
        var mute_data = JSON.stringify({
            conversation_id: data.conversation_id,
            mute_by: data.user_id,
            mute_duration: data.mute_duration,
            mute_start_time: data.mute_start_time,
            mute_end_time: data.mute_end_time,
            mute_day: data.mute_day,
            mute_unix_start: nowUnix ? nowUnix.toString() : "0",
            mute_unix_end: endUnix ? endUnix.toString() : "0",
            mute_timezone: data.mute_timezone ? data.mute_timezone : '',
        });
        Conversation.updateOne(query, { $push: {has_mute: data.user_id, mute: mute_data.toString()}}).then(function () {
            //if (ue) callback({ error: ue, message: "in message.js mute_conversation error " });
             callback({ status: true, mute_data: JSON.parse(mute_data) });
        }).catch((e)=>{
            callback({ error: ue, message: "in message.js mute_conversation error " });
        });
    } else if (data.type == 'update') {
       // console.log(3055,"aadsd")
        var nowUnix = moment(data.mute_start_time + ' ' + data.mute_timezone, 'llll Z').unix();
        var endUnix = moment(data.mute_end_time + ' ' + data.mute_timezone, 'llll Z').unix();
        Conversation.findOne(query).then(function (conv) {
            
            // console.log(2521, conv.has_mute);
            if (conv && conv.has_mute != null) {
                if (conv.has_mute.indexOf(data.user_id) > -1) {
                    var new_mute_data = [];
                    var update_mute_data = JSON.stringify({
                        conversation_id: data.conversation_id,
                        mute_by: data.user_id,
                        mute_duration: data.mute_duration,
                        mute_start_time: data.mute_start_time,
                        mute_end_time: data.mute_end_time,
                        mute_day: data.mute_day,
                        mute_unix_start: nowUnix ? nowUnix.toString() : "0",
                        mute_unix_end: endUnix ? endUnix.toString() : "0",
                        mute_timezone: data.mute_timezone ? data.mute_timezone : '',
                    });
                    _.each(conv.mute, function (v, k) {
                        var mute_data = parseJSONSafely(v);
                        if (mute_data.mute_by == data.user_id) {
                            new_mute_data.push(update_mute_data);
                        } else {
                            new_mute_data.push(v);
                        }
                    });
                    Conversation.updateOne(query, { $set: {mute: new_mute_data.length>0 ? [new_mute_data.toString()] : []}}).then(function () {
                       // if (ue) callback({ error: ue, message: "in message.js mute_conversation error " });
                         callback({ status: true, mute_data: JSON.parse(update_mute_data) });
                    }).catch((e)=>{
                         callback({ error: e, message: "in message.js mute_conversation error " });
                    });
                } else {
                    callback({ status: true, message: 'You are not mute this conversation.' });
                }
            } else {
                callback({ status: true, message: 'No mute list found.' });
            }
        }).catch((e)=>{
             callback({ status: false, error: e });
        });
    } else if (data.type == 'delete') {
        //console.log(3034,"awwdsd")
        Conversation.findOne(query).then(function (conv) {
            
            if (conv && conv.has_mute != null) {
                if (conv.has_mute.indexOf(data.user_id) > -1) {
                    var new_mute_data = [];
                    _.each(conv.mute, function (v, k) {
                        var mute_data = parseJSONSafely(v);
                        if (mute_data.mute_by != data.user_id) {
                            new_mute_data.push(v);
                        }
                    });
                    // console.log(3016, new_mute_data);
                    Conversation.updateOne(query, { $pull: { has_mute: data.user_id }, $set: {mute: new_mute_data.length>0 ? [new_mute_data.toString()] : []}}).then( function () {
                        //if (ue) callback({ error: ue, message: "in message.js mute_conversation error " });
                         callback({ status: true, new_mute_data });
                    }).catch((e)=>{
                    callback({ error: e, message: "in message.js mute_conversation error " });
                    });
                } else {
                    callback({ status: true, message: 'You are not mute this conversation.' });
                }
            } else {
                callback({ status: true, message: 'No mute list found.' });
            }
        }).catch((e)=>{
             callback({ status: false, error: e });
        });
    }
}

function _set_read(data, req) {
    var msg_ids = [];
    var is_reply_msg = 'no';
    var root_msg_id = '';
    var reply_mention = 0;
    for(let v of data.msgs){
        if (v.msg_status.indexOf(data.user_id) > -1)
            msg_ids.push(v.msg_id);
        if(v.is_reply_msg === 'yes' && v.mention_user.indexOf(data.user_id) > -1)
            reply_mention++;
    }
    // console.log(1500, msg_ids.length);
    if (msg_ids.length > 0) {
        is_reply_msg = data.msgs[0].is_reply_msg;
        // console.log(3154, data.msgs[0].is_reply_msg);
        root_msg_id = data.msgs[0].is_reply_msg === 'yes' ? data.msgs[0].reply_for_msgid : '';
        var query = ({ conversation_id: data.msgs[0].conversation_id, msg_id: { $in: Array.from(msg_ids) } });
        console.log("44444444444444444444444444444444");
        Messages.updateMany(query, { '$pull': { msg_status: data.user_id } }).then(function (result) {
            if(result.modifiedCount >0) {
                
                console.log(3173, result);
                setTimeout(async function () {
                    xmpp_send_server(data.user_id, 'read_status_msg', { conversation_id: data.msgs[0].conversation_id, is_reply_msg, root_msg_id, read: msg_ids.length, reply_mention }, req);
    
                    await znReadMsgUpdateCounter({user_id: data.user_id, conversation_id: data.msgs[0].conversation_id, is_reply_msg, root_msg_id, read: msg_ids.length, reply_mention});
                }, 2000);
            }
            // console.log({ status: true, result: 'Set msg_status read successfully.' });
            
            if(is_reply_msg === 'yes'){
                
            }
        }).catch((e)=>{
            console.log(3140, e)
        })
    } else console.log("No msg found for set to read status");
}

function set_msg_status_read(data, req) {
    // console.log("set_msg_status_read ", data.user_id, data.msgs.length);
    if (data.conversation_id && isUuid(data.conversation_id)) {
        Conversation.find({ conversation_id: data.conversation_id.toString() }, function (err, msgs) {
            if (err) console.log({ status: false, err: err });
            data.msgs = msgs;
            _set_read(data, req);
        });
    } else if (data.msgs.length > 0) {
        _set_read(data, req);
    }
}

function msgs_seen(data) {
    // console.log("msgs_seen ", data);
    // var mids = [];
    // data.msg_ids.forEach(function (v, k) {
    //     mids.push(models.timeuuidFromString(v));
    // });
    // var query = ({ conversation_id: models.uuidFromString(data.conversation_id.toString()), msg_id: { $in: mids } });
    // models.instance.Messages.update(query, { msg_status: { $remove: [data.user_id] } }, function (err) {
    //     if (err) console.log({ status: false, err: err });
    //     console.log({ status: true, result: 'Message seen successfully.' });
    // });
}

function count_unread_reply(data) {
    return new Promise(async (resolve, reject) => {
        var query = { msg_status: { $in: [data.user_id] } };
        if (data.conversation_id && isUuid(data.conversation_id.toString())) {
            query.conversation_id = data.conversation_id.toString();
        }
        query.is_reply_msg = 'no';
        query.has_hide = { $nin: [data.user_id.toString()] };
        query.has_delete = { $nin: [data.user_id.toString()] };
        query.$or = [ 
                    {
                        $and: [
                            { is_secret: true }, 
                            { secret_user: { $in: [data.user_id.toString()] }} 
                        ]
                    },
                    { is_secret: false } 
                ];

        const ur_msgs = [ {$match: query}];
        const msgs = await Messages.aggregate(ur_msgs);
        query.is_reply_msg = 'yes';
        const ur_reply_msgs = [ {$match: query}];
        var rmsgs = await Messages.aggregate(ur_reply_msgs);
        // console.log(3234, rmsgs);
        let reply_for_msgids = [];
        for(let v of rmsgs){
            if(reply_for_msgids.indexOf(v.reply_for_msgid) === -1)
                reply_for_msgids.push(v.reply_for_msgid);
        }
        query.msg_id = {$in: reply_for_msgids};
        // console.log(3241, query.msg_id)
        query.is_reply_msg = 'no';
        delete query.msg_status;
        // console.log(3243, JSON.stringify(query))
        let frmsgs = await Messages.aggregate([ {$match: query}]);
        // console.log(3245, frmsgs);
        let frmsgss = [];
        for(let e of rmsgs){
            for(let n = 0; n<frmsgs.length; n++){
                if(frmsgs[n].msg_id === e.reply_for_msgid)
                    frmsgss.push(e);
            }
        }
        // console.log(3250, frmsgss);
        var total_urmsg = msgs.length;
        var total_urreply = frmsgss.length;
        var total_r_mention_user = 0;
        var total_unread_notification = 0;
        var convid = [];
        var conversation_lists = {};
        // message
        for(let i=0; i<msgs.length; i++){
            if(convid.indexOf(msgs[i].conversation_id) == -1){
                convid.push(msgs[i].conversation_id);
                if(! conversation_lists.hasOwnProperty(msgs[i].conversation_id))
                    conversation_lists[msgs[i].conversation_id] = {conversation_id: msgs[i].conversation_id, urmsg: 0, urreply: 0, r_mention_user: 0, mention_user: 0, msgids: [] };
            }
            conversation_lists[msgs[i].conversation_id].urmsg++;
            if(msgs[i].mention_user.indexOf(data.user_id)>1)
                conversation_lists[msgs[i].conversation_id].mention_user++;
        }
        // reply msg
        for(let i=0; i<frmsgss.length; i++){
            if(convid.indexOf(frmsgss[i].conversation_id) == -1){
                convid.push(frmsgss[i].conversation_id);
                if(! conversation_lists.hasOwnProperty(frmsgss[i].conversation_id))
                    conversation_lists[frmsgss[i].conversation_id] = {conversation_id: frmsgss[i].conversation_id, urmsg: 0, urreply: 0, r_mention_user: 0, mention_user: 0, msgids: [] };
            }
            conversation_lists[frmsgss[i].conversation_id].urreply++;
            if(conversation_lists[frmsgss[i].conversation_id].msgids.length>0){
                let flag = true;
                for(let j=0; j<conversation_lists[frmsgss[i].conversation_id].msgids.length; j++){
                    if(conversation_lists[frmsgss[i].conversation_id].msgids[j].msgid == frmsgss[i].reply_for_msgid){
                        conversation_lists[frmsgss[i].conversation_id].msgids[j].unread++;
                        flag = false;
                    }
                }
                if(flag){
                    conversation_lists[frmsgss[i].conversation_id].msgids.push({
                        msgid: frmsgss[i].reply_for_msgid,
                        unread: 1
                    });
                }
            }else{
                conversation_lists[frmsgss[i].conversation_id].msgids.push({
                    msgid: frmsgss[i].reply_for_msgid,
                    unread: 1
                });
            }
            if(frmsgss[i].mention_user.indexOf(data.user_id)>1){
                total_r_mention_user++;
                conversation_lists[frmsgss[i].conversation_id].r_mention_user++;
            }
        }
        let gurbage_data = [];
        // if(data.type === 'conv_info_required'){
            var cinfo = await get_conversation({conversation_id: convid, user_id: data.user_id});
            if(cinfo.length !== convid.length){
                // gurbage_data = convid.filter(value => !cinfo.some(obj => obj.cinfo === value));
                for(let n=0; n<convid.length; n++){
                    let find = false;
                    for(let m=0; m<cinfo.length; m++){
                        if(convid[n] === cinfo[m].conversation_id){
                            find = true;
                        }
                    }
                    if(find === false)
                        gurbage_data.push(convid[n]);
                }
            }
            for(let i=0; i<cinfo.length; i++){
                if(cinfo[i].participants.indexOf(data.user_id)>-1){
                    if(conversation_lists.hasOwnProperty(cinfo[i].conversation_id)){
                        conversation_lists[cinfo[i].conversation_id]["title"] = cinfo[i].title ? cinfo[i].title : "No Title Found";
                        conversation_lists[cinfo[i].conversation_id]["conv_img"] = cinfo[i].conv_img ? cinfo[i].conv_img : "feelix.jpg";
                        conversation_lists[cinfo[i].conversation_id]["fnln"] = cinfo[i].fnln ? cinfo[i].fnln : "";
                        conversation_lists[cinfo[i].conversation_id]["friend_id"] = cinfo[i].friend_id;
                    }
                }else{
                    gurbage_data.push(cinfo[i].conversation_id.toString());
                }
            }
        // }
        if(gurbage_data.length>0){
            console.log("3333333333333333333333333333333", gurbage_data);
            let gr = await Messages.updateMany({conversation_id: {$in: gurbage_data}, msg_status: {$in: data.user_id}}, {$pull: {msg_status: data.user_id}});
            // console.log(3028, gr);
        }
        resolve({ total_unread_msg: total_urmsg, total_unread_reply: total_urreply, total_unread_notification, total_r_mention_user, conversations_list: convid, conversations: conversation_lists});
    });
}

function _unread_notification_count(data) {
    // return new Promise((resolve) => {
    //     models.instance.Notifications.find({ receiver_id: data.user_id.toString(), read_status: 'no' }, { raw: true, allow_filtering: true }, function (error, notis) {
    //         if (error) console.log(2125, { error: error.name, message: error.message });
    //         resolve(notis);
    //     });
    // });
}

// function count_unread_reply(data) {
//     return new Promise((resolve, reject) => {
//         var query = { msg_status: { $contains: data.user_id } };
//         if (data.conversation_id && isUuid(data.conversation_id.toString())) {
//             query.conversation_id = models.uuidFromString(data.conversation_id.toString());
//         }
//         models.instance.Notifications.find({ receiver_id: data.user_id.toString(), read_status: 'no' }, { raw: true, allow_filtering: true }, function(error, notis) {
//             if (error) reject({ error: error.name, message: error.message });

//             var total_urmsg = 0;
//             var total_urreply = 0;
//             var convid = [];
//             var conversation_lists = [];
//             // if(data.type === 'reply') query.is_reply_msg = 'yes';
//             console.log(1913, query, new Date().getTime());
//             var msgids_need_read = [];
//             models.instance.Messages.stream(query, {raw: true, allow_filtering: true}, function(reader){
//                 var row;
//                 while (row = reader.readRow()) {
//                     console.log(2088, row.is_reply_msg);
//                     row.has_hide = Array.isArray(row.has_hide) ? row.has_hide : [];
//                     row.has_delete = Array.isArray(row.has_delete) ? row.has_delete : [];
//                     row.mention_user = Array.isArray(row.mention_user) ? row.mention_user : [];
//                     if (row.has_hide.indexOf(data.user_id) > -1) {
//                         msgids_need_read.push(models.instance.Messages.update({msg_id: row.msg_id, conversation_id: row.conversation_id}, {msg_status:  { $remove: [data.user_id] } }, {return_query: true}));
//                         continue;
//                     }
//                     if (row.has_delete.indexOf(data.user_id) > -1) {
//                         msgids_need_read.push(models.instance.Messages.update({msg_id: row.msg_id, conversation_id: row.conversation_id}, {msg_status:  { $remove: [data.user_id] }}, {return_query: true}));
//                         continue;
//                     }

//                     if (row.is_secret === true) {
//                         if (row.secret_user.indexOf(data.user_id) == -1) {
//                             msgids_need_read.push(models.instance.Messages.update({msg_id: row.msg_id, conversation_id: row.conversation_id}, {msg_status:  { $remove: [data.user_id] }}, {return_query: true}));
//                             continue;
//                         }
//                     }

//                     // console.log('has_ok', k);
//                     if (row.is_reply_msg == 'yes') {
//                         total_urreply++;
//                         if (convid.indexOf(row.conversation_id.toString()) == -1) {
//                             convid.push(row.conversation_id.toString());
//                             let mention_user = row.mention_user.indexOf(data.user_id) > -1 ? 1 : 0;
//                             conversation_lists.push({ conversation_id: row.conversation_id.toString(), urmsg: 0, r_mention_user: mention_user, mention_user: 0, urreply: 1, msgids: [{ msgid: row.reply_for_msgid.toString(), unread: 1 }] });                                
//                         } else {
//                             conversation_lists.forEach(function(rv, rk) {
//                                 if (rv.conversation_id == row.conversation_id.toString()) {
//                                     // console.log(1951, rv.msgids);
//                                     rv.urreply += 1;
//                                     rv.r_mention_user += row.mention_user.indexOf(data.user_id) > -1 ? ( 1 + rv.r_mention_user) : rv.r_mention_user;
//                                     var flag = true;
//                                     rv.msgids.forEach(function(m, n) {
//                                         if (m.msgid.toString() == row.reply_for_msgid) {
//                                             m.unread++;
//                                             flag = false;
//                                         }
//                                     });
//                                     if (flag) {
//                                         if (Array.isArray(rv.msgids))
//                                             rv.msgids.push({ msgid: row.reply_for_msgid.toString(), unread: 1 });
//                                         else
//                                             rv.msgids = [{ msgid: row.reply_for_msgid.toString(), unread: 1 }];
//                                     }
//                                     return;
//                                 }
//                             });
//                         }
//                     } else {
//                         total_urmsg++;
//                         if (convid.indexOf(row.conversation_id.toString()) == -1) {
//                             convid.push(row.conversation_id.toString());
//                             let mention_user = row.mention_user.indexOf(data.user_id) > -1 ? 1 : 0;
//                             conversation_lists.push({ conversation_id: row.conversation_id.toString(), urmsg: 1, mention_user: mention_user, r_mention_user: 0, urreply: 0, msgids: [] });
//                         } else {
//                             conversation_lists.forEach(function(rv, rk) {
//                                 if (rv.conversation_id == row.conversation_id.toString()) {
//                                     rv.urmsg += 1;
//                                     rv.mention_user += row.mention_user.indexOf(data.user_id) > -1 ? ( 1 + rv.mention_user) : rv.mention_user;
//                                     return;
//                                 }
//                             });
//                         }
//                     }   
//                 }
//             }, function(err){
//                 if(err)
//                     reject({ error: err.name, message: err.message });
//                 if(convid.length>0){
//                     for(let i=0; i<convid.length; i++){
//                         convid[i] = models.uuidFromString(convid[i]);
//                     }
//                     models.instance.Conversation.find({conversation_id: {$in: convid}}, {raw: true, allow_filtering: true}, function(e, convs){
//                         if(e) reject({ error: err.name, message: err.message });
//                         for(let i=0; i<convs.length; i++){
//                             let pos = conversation_lists.findIndex(el => el.conversation_id === convs[i].conversation_id.toString());
//                             if(convs[i].participants.indexOf(data.user_id) == -1){
//                                 total_urmsg -= conversation_lists[pos].urmsg;
//                                 total_urreply -= conversation_lists[pos].urreply;
//                                 conversation_lists.splice(pos, 1);
//                             }
//                             else{
//                                 conversation_lists[pos].title = convs[i].title;
//                                 conversation_lists[pos].conv_img = process.env.FILE_SERVER + 'room-images-uploads/Photos/' + convs[i].conv_img;
//                                 conversation_lists[pos].fnln = "";
//                                 conversation_lists[pos].friend_id = "";
//                                 if(convs[i].single == 'yes' && convs[i].conversation_id.toString() != data.user_id){
//                                     var fid = convs[i].participants.join().replace(data.user_id, '').replace(',', '');
//                                     if(isUuid(fid)){
//                                         conversation_lists[pos].title = all_users[convs[i].company_id][fid].firstname + " " + all_users[convs[i].company_id][fid].lastname;
//                                         conversation_lists[pos].conv_img = all_users[convs[i].company_id][fid].img;
//                                         conversation_lists[pos].fnln = all_users[convs[i].company_id][fid].fnln;
//                                         conversation_lists[pos].friend_id = fid;
//                                     }
//                                 }
//                             }
//                         }
//                         if(msgids_need_read.length>0){
//                             models.doBatch(msgids_need_read, function(err) {
//                                 if (err) console.log({ error: 'hide msg update error', message: err });
//                                 else console.log("hide msg update status set read");
//                             });
//                         }
//                         resolve({ total_urmsg, total_urreply, conversation_lists, notification: _.isEmpty(notis) ? 0 : notis.length });
//                     });
//                 }
//             });
//         });
//     });
// }

function read_all_reply(data, req, callback) {
    var query = { msg_status: { $in: [data.user_id] } };
    if (data.conversation_id && isUuid(data.conversation_id.toString())) {
        query.conversation_id = data.conversation_id.toString();
    }
   // console.log(3427, data);
    Messages.find(query).then(function (msgs) {
        // if (err) {
        //     console.log(3430, err);
        //     callback({ status: false, error: err.name, message: err.message });
        // } else {
            var msg_ids = [];
            // console.log(1263, msgs.length);
            if (msgs.length) {
                msgs.forEach(function (v, k) {
                    if (v.is_reply_msg == 'yes' || data.read_type === 'all') {
                        msg_ids.push(v.msg_id);
                    }
                });

                if (msg_ids.length > 0) {
                    var query2 = {
                        conversation_id: data.conversation_id.toString(),
                        msg_id: { $in: msg_ids }
                    };
                    Messages.updateMany(query2, { msg_status: { $pull: data.user_id } }).then( function () {
                        setTimeout(async function () {
                            console.log(3450, data.user_id)
                            xmpp_send_server(data.user_id, 'read_status_msg', { conversation_id: data.conversation_id.toString(), is_reply_msg: 'yes', root_msg_id: '', read: msg_ids.length }, req);
                            await znReadMsgUpdateCounter({user_id: data.user_id, conversation_id: data.conversation_id.toString(), is_reply_msg: 'yes', root_msg_id: '', read: msg_ids.length, reply_mention:0});
                        }, 2000);
                        callback({ status: true, data: query2, message: "All reply msg read" });
                    }).catch((e)=>{
                        console.log(3454, e);
                        callback({ status: true, message: "no reply msg read" });
                    });
                } else {
                    callback({ status: true, data: { conversation_id: data.conversation_id }, message: "All reply msg read" });
                }
            } else {
                callback({ status: true, message: "no reply msg read" });
            }
        // }
    }).catch((e)=>{
        //console.log(3427, e);
    });
}

function count_unread_reply_for_other_acc(data) {
    // return new Promise((resolve, reject) => {
    //     var query = { msg_status: { $contains: data.user_id } };
    //     if (data.conversation_id && isUuid(data.conversation_id.toString())) {
    //         query.conversation_id = models.uuidFromString(data.conversation_id.toString());
    //     }
    //     models.instance.Notifications.find({ receiver_id: data.user_id.toString(), read_status: 'no' }, { raw: true, allow_filtering: true }, function (error, notis) {
    //         if (error) reject({ error: error.name, message: error.message });

    //         query.$limit = 1000;

    //         var total_urmsg = 0;
    //         var total_urreply = 0;
    //         var allmsgs = [];
    //         var convid = [];
    //         var conversation_lists = [];

    //         models.instance.Messages.find(query, { raw: true, allow_filtering: true }, function (err, msgs) {
    //             if (err) {
    //                 reject({ error: err.name, message: err.message });
    //                 // resolve({total_urmsg, total_urreply, conversation_lists, notification: _.isEmpty(notis) ? 0 : notis.length});
    //             } else {
    //                 if (msgs.length) {
    //                     msgs.forEach(function (v, k) {
    //                         v.has_hide = Array.isArray(v.has_hide) ? v.has_hide : [];
    //                         v.has_delete = Array.isArray(v.has_delete) ? v.has_delete : [];
    //                         if (v.has_hide.indexOf(data.user_id) > -1) {
    //                             // console.log('has_hide', k); 
    //                             return;
    //                         }
    //                         if (v.has_delete.indexOf(data.user_id) > -1) {
    //                             // console.log('has_delete', k); 
    //                             return;
    //                         }
    //                         if (v.is_secret === true) {
    //                             if (v.secret_user.indexOf(data.user_id) == -1) return;
    //                         }
    //                         // console.log('has_ok', k);
    //                         if (v.is_reply_msg == 'yes') {
    //                             total_urreply++;
    //                             v.company_id = data.company_id;
    //                             allmsgs.push(v);
    //                             if (convid.indexOf(v.conversation_id.toString()) == -1) {
    //                                 convid.push(v.conversation_id.toString());
    //                                 conversation_lists.push({ conversation_id: v.conversation_id.toString(), urmsg: 0, urreply: 1, msgids: [{ msgid: v.reply_for_msgid.toString(), unread: 1 }] });
    //                             } else {
    //                                 conversation_lists.forEach(function (rv, rk) {
    //                                     if (rv.conversation_id == v.conversation_id.toString()) {
    //                                         // console.log(1332, rv.msgids);
    //                                         rv.urreply += 1;
    //                                         var flag = true;
    //                                         rv.msgids.forEach(function (m, n) {
    //                                             if (m.msgid.toString() == v.reply_for_msgid) {
    //                                                 m.unread++;
    //                                                 flag = false;
    //                                             }
    //                                         });
    //                                         if (flag) {
    //                                             if (Array.isArray(rv.msgids))
    //                                                 rv.msgids.push({ msgid: v.reply_for_msgid.toString(), unread: 1 });
    //                                             else
    //                                                 rv.msgids = [{ msgid: v.reply_for_msgid.toString(), unread: 1 }];
    //                                         }
    //                                         return;
    //                                     }
    //                                 });
    //                             }
    //                         } else {
    //                             total_urmsg++;
    //                             v.company_id = data.company_id;
    //                             allmsgs.push(v);
    //                             if (convid.indexOf(v.conversation_id.toString()) == -1) {
    //                                 convid.push(v.conversation_id.toString());
    //                                 conversation_lists.push({ conversation_id: v.conversation_id.toString(), urmsg: 1, urreply: 0, msgids: [] });
    //                             } else {
    //                                 conversation_lists.forEach(function (rv, rk) {
    //                                     if (rv.conversation_id == v.conversation_id.toString()) {
    //                                         rv.urmsg += 1;
    //                                         return;
    //                                     }
    //                                 });
    //                             }
    //                         }
    //                     });
    //                 }
    //                 for (let i = 0; i < convid.length; i++)
    //                     convid[i] = models.uuidFromString(convid[i]);

    //                 models.instance.Conversation.find({ conversation_id: { '$in': convid } }, { raw: true }, async function (e, convs) {
    //                     if (e) reject({ error: e.name, message: e.message });
    //                     for (let i = 0; i < allmsgs.length; i++) {
    //                         allmsgs[i].company_name = data.company_name;
    //                         for (let j = 0; j < convs.length; j++) {
    //                             if (convs[j].conversation_id.toString() == allmsgs[i].conversation_id.toString()) {
    //                                 if (convs[j].single == 'yes') {
    //                                     var fid = convs[j].participants.join().replace(data.user_id, '').replace(',', '');
    //                                     var fobj = await get_user_obj(fid);
    //                                     allmsgs[i].conversation_name = fobj.firstname + ' ' + fobj.lastname;
    //                                 } else
    //                                     allmsgs[i].conversation_name = convs[j].title;
    //                             }
    //                         }
    //                     }
    //                 });
    //                 resolve({ company_id: data.company_id, company_name: data.company_name, role: data.role, user_id: data.user_id, img: data.img, total_urmsg, total_urreply, conversation_lists, allmsgs, notification: _.isEmpty(notis) ? 0 : notis.length });
    //             }
    //         });
    //     });
    // });
}

function update_one_link_title(old_link, decode, new_title){
    return new Promise((resolve)=>{
        var old_title = [];
        var old_title_obj = {};
        var is_retitle_by_recevier = false;
        if (!_.isEmpty(old_link.title)) {
            // console.log(2987, old_link.title)
            old_title = parseJSONSafely(old_link.title);
            for (let i = 0; i < old_title.length; i++) {
                if (old_title[i].user_id == decode.id) {
                    old_title[i].title = new_title;
                    is_retitle_by_recevier = true;
                }
                old_title_obj[old_title[i].user_id] = old_title[i].title;
            }
        }
        if (is_retitle_by_recevier === false){
            old_title.push({ user_id: decode.id, title: new_title });
            old_title_obj[decode.id] = new_title;
        }
            
        Link.updateOne({company_id: old_link.company_id, url_id: old_link.url_id}, {title: JSON.stringify(old_title)}, function(e){
            if(e) {
                console.log(3005, { status: false, error: 'Service Unavailable', message: "The server is not ready to handle the request." }, e); 
                resolve({ status: false, error: 'Service Unavailable', message: "The server is not ready to handle the request." });
            }
            else{
                old_link.title = old_title;
                resolve({status: true, link: old_link});
            }
        });
    })
}

function _url_title_update_link_table(data){
    return new Promise((resolve)=>{
        Link.find({msg_id: data.msg_id.toString()}).then(async function(l) {
            
           
                var result = [];
                for(let i=0; i<l.length; i++){
                    result.push( await update_one_link_title(l[i], {id: data.user_id}, data.url_title));
                    if(l.length == i+1){
                        resolve(result);
                    }
                }
            
        }).catch((e)=>{
            
                console.log("Link find error ", e);
                resolve([]);
            
        });
    });
}

function url_title_update(data) {
    return new Promise(async (resolve, reject) => {
        try {
            // var mainmsg = await get_message(data.conversation_id, data.msg_id, 1, '', '', '');
            data.is_reply_msg = data.is_reply_msg ? data.is_reply_msg : 'no';
            var mainmsg = (await get_message({ convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id, type: 'one_msg' })).msgs;

            var cur_time = new Date().getTime();
            if (data.user_id == mainmsg[0].sender.toString()) {
                var msgtxt = '';
                var msgtype = '';
                if (data.old_url_title && data.old_url_title !== "") {
                    msgtxt = mainmsg[0].msg_text.replace(data.old_url_title, data.url_title);
                    if (data.url_title === '')
                        msgtype = mainmsg[0].msg_type.replace(' has_msg_title', '');
                    else
                        msgtype = mainmsg[0].msg_type;
                } else {
                    msgtxt = mainmsg[0].msg_text + ' ' + data.url_title.toString();
                    msgtype = mainmsg[0].msg_type + ' link has_msg_title';
                }
                msgtxt = msgtxt.replace(/undefined/g, '');
                Messages.updateOne(
                    { conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id },
                    {
                        msg_text: msgtxt,
                        url_base_title: data.url_title && data.url_title !== '' ? data.url_title : null,
                        msg_type: msgtype,
                        last_update_time: cur_time
                    }).then(async function (msg)  {  
                        var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id, type: 'one_msg' });
                        var result = await _url_title_update_link_table(data);
                        // console.log(3062, result);
                        resolve({ status: true, msg: newmsg.msgs[0] });
                    }).catch((e)=>{
                        console.log("in message.js update url title messager error 1", e);
                    });

                // models.instance.Messages.update({ conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id }, {
                //     msg_text: msgtxt,
                //     url_base_title: data.url_title && data.url_title !== '' ? data.url_title : null,
                //     msg_type: msgtype,
                //     last_update_time: cur_time
                // }, update_if_exists, async function (ue, msg) {
                //     if (ue) console.log("in message.js update url title messager error 1", ue);
                //     var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id });
                //     var result = await _url_title_update_link_table(data);
                //     console.log(3062, result);
                //     resolve({ status: true, msg: newmsg[0] });
                // });
            } else {
                var urldata = [];
                var is_retitle_by_recevier = false;
                if (!_.isEmpty(mainmsg[0].url_title)) {
                    // console.log(2123, mainmsg[0].url_title)
                    urldata = JSON.parse(mainmsg[0].url_title);
                    for (let i = 0; i < urldata.length; i++) {
                        if (urldata[i].user_id == data.user_id) {
                            urldata[i].title = data.url_title;
                            is_retitle_by_recevier = true;
                        }
                    }
                }
                if (is_retitle_by_recevier === false)
                    urldata.push({ user_id: data.user_id, title: data.url_title });

                Messages.updateOne({ conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id }, {
                    url_title: JSON.stringify(urldata),
                    msg_type: 'text link'
                }, async function (ue, msg) {
                    if (ue) console.log("in message.js update url title messager error 2", ue);
                    var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id, type: 'one_msg' });
                    var result = await _url_title_update_link_table(data);
                    // console.log(3088, result);
                    resolve({ status: true, msg: newmsg[0] });
                });
            }
        } catch (e) {
            reject({ status: false, error: e });
        }

    });
}

function edit_private_participants(data) {
    return new Promise(async (resolve, reject) => {
        try {
            // console.log(3546, {convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, limit: 1, user_id: data.user_id, type: 'one_msg'});
            var mainmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, limit: 1, user_id: data.user_id, type: 'one_msg' });
           // console(3796,mainmsg);
            var cur_time = new Date().getTime();
            if (data.user_id == mainmsg["msgs"][0].sender.toString()) {
                try{
                    if (data.add_secret_user.length > 0) {
                        // console.log(3533)
                        await Messages.updateOne({ conversation_id: mainmsg["msgs"][0].conversation_id, msg_id: mainmsg["msgs"][0].msg_id }, { $push: { secret_user: { $each: data.add_secret_user}, msg_status: { $each: data.add_secret_user}, participants: { $each: data.add_secret_user} },created_at:cur_time } );
                        // console.log(3535)
                    } 
                    if (data.remove_secret_user.length > 0) {
                        // console.log(3538)
                        await Messages.updateOne({ conversation_id: mainmsg["msgs"][0].conversation_id, msg_id: mainmsg["msgs"][0].msg_id }, { $pull: { secret_user: { $in: data.remove_secret_user}, msg_status: { $in: data.remove_secret_user},participants: { $in: data.remove_secret_user} },created_at:cur_time});
                    }
                }catch(e){
                    
                    console.log('3538 edit_private_participants error ', e);
                    resolve({ status: false, error: e });
                }
                var newmsg = (await get_message({ convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, limit: 1, user_id: data.user_id, type: 'one_msg' })).msgs;
                // console.log(3549, newmsg);
                if (newmsg[0].is_secret === true && newmsg[0].secret_user.length === 1) {
                    Messages.updateOne({ conversation_id: data.conversation_id, msg_id: data.msg_id }, { is_secret: false, secret_user: null }, function (e) {
                        if (e) console.log(3552, e);
                        else {
                            newmsg[0].is_secret = false;
                            newmsg[0].secret_user = [];
                            resolve({ status: true, msg: newmsg[0] });
                        }
                    })
                } else
                    resolve({ status: true, msg: newmsg[0] });
            } else {
                resolve({ status: false, error: 'You are not authorize to edit this message.' });
            }
        } catch (e) {
            resolve({ status: false, error: e });
        }

    });
}

function msg_title_update(data) {
    return new Promise(async (resolve, reject) => {
        try {
            data.is_reply_msg = data.is_reply_msg ? data.is_reply_msg : 'no';
            var mainmsg = (await get_message({ convid: data.conversation_id, msgid: data.msg_id, company_id: data.company_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id })).msgs;

            var cur_time = new Date().getTime();
            if (data.user_id == mainmsg[0].sender.toString()) {
                var msgtxt = '';
                var msgtype = '';
                if (data.old_msg_title && data.old_msg_title !== "") {
                    msgtxt = mainmsg[0].msg_text.replace(data.old_msg_title, data.msg_title);
                    if (data.msg_title === '')
                        msgtype = mainmsg[0].msg_type.replace(' has_msg_title', '');
                    else
                        msgtype = mainmsg[0].msg_type;
                } else {
                    msgtxt = mainmsg[0].msg_text + ' ' + data.msg_title.toString();
                    msgtype = mainmsg[0].msg_type + ' has_msg_title';
                    if (data.msg_title === '')
                        msgtype = mainmsg[0].msg_type.replace(' has_msg_title', '');
                }
                Messages.updateOne(
                    { conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id },
                    {
                        msg_text: msgtxt,
                        msg_type: msgtype,
                        url_base_title: data.msg_title && data.msg_title !== '' ? data.msg_title : null,
                        last_update_time: cur_time
                    }).then(async function (msg)  {
                        
                        var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id });

                         resolve({ status: true, msg: newmsg.msgs[0] });
                    }).catch((e)=>{
                        console.log("in message.js update msg title messager error 1", e);
                    })
                // models.instance.Messages.update({ conversation_id: mainmsg[0].conversation_id, msg_id: mainmsg[0].msg_id }, {
                //     msg_text: msgtxt,
                //     msg_type: msgtype,
                //     url_base_title: data.msg_title && data.msg_title !== '' ? data.msg_title : null,
                //     last_update_time: cur_time
                // }, update_if_exists, async function (ue, msg) {
                //     if (ue) console.log("in message.js update msg title messager error 1", ue);
                //     var newmsg = await get_message({ convid: data.conversation_id, msgid: data.msg_id, is_reply: data.is_reply_msg, limit: 1, user_id: data.user_id });

                //     resolve({ status: true, msg: newmsg[0] });
                // });
            } else {
                resolve({ status: false, msg: {}, message: 'You are not the message sender.' });
            }
        } catch (e) {
            reject({ status: false, error: e });
        }

    });
}

function save_link_data(post, data) {
    return new Promise((resolve) => {
        // console.log(3153, post, data);
        try {
            var urls = parseJSONSafely(post.url_body);
            // var queries = [];
            for (let i = 0; i < urls.length; i++) {
                var link_data = {
                    url_id: uuidv4(),
                    msg_id: data.msg_id.toString(),
                    conversation_id: post.conversation_id,
                    company_id: post.company_id,
                    user_id: post.sender,
                    url: urls[i].toString()
                }
                var linkd = new Link(link_data);
                linkd.save(e=>{
                    if(e) throw e;
                });
            }
            resolve(true);
        } catch (e) {
            console.log(3550, e);
            resolve(false);
        }
    });
}

// function edit_link_data(post, data) {
//     return new Promise((resolve) => {
//         console.log(3376, post, data);
//         try {
//             models
//             var urls = JSON.parse(post.url_body);
//             var queries = [];
//             for (let i = 0; i < urls.length; i++) {
//                 var link_data = {
//                     url_id: models.timeuuid(),
//                     msg_id: data.msg_id.toString(),
//                     conversation_id: post.conversation_id,
//                     company_id: post.company_id,
//                     user_id: post.sender,
//                     url: urls[i].toString(),
//                     // participants: post.participants,
//                     // other_user: data.msg_status
//                 }
//                 var linkd = new models.instance.Link(link_data);
//                 queries.push(linkd.save({ return_query: true }));
//             }
//             if (queries.length > 0) {
//                 models.doBatch(queries, function(err) {
//                     if (err) {
//                         console.log(err);
//                         resolve(false);
//                     } else resolve(true);
//                 });
//             }
//         } catch (e) {
//             console.log(3179, e);
//             resolve(false);
//         }
//     });
// }

module.exports = {
    get_conversation,
    fnln,
    get_user_obj,
    get_message,
    set_message,
    get_rep_message,
    get_filter_msg,
    delete_msg,
    remove_this_line,
    forward_msg,
    edit_msg,
    file_share,
    check_reac_emoji_list,
    add_reac_emoji,
    delete_reac_emoji,
    update_reac_emoji,
    view_reac_emoji_list,
    flag_unflag,
    get_tags,
    get_tags2,
    clear_conversation,
    mute_conversation,
    set_msg_status_read,
    count_unread_reply,
    read_all_reply,
    update_conversation_for_last_msg,
    msgs_seen,
    count_unread_reply_for_other_acc,
    url_title_update,
    edit_private_participants,
    msg_title_update,
    update_one_link_title,
    has_reply_attach_update,
    update_task,
    get_discussion,
    is_the_last_msg_of_this_conv,
    get_the_last_msg_of_this_conv,
    parseMuteSafely,
    fnln
};