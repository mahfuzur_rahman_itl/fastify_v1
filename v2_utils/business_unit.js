// var {models} = require('./../config/db/express-cassandra');

var BusinessUnit = require('../mongo-models/business_unit');
var Conversation = require('../mongo-models/conversation');


function count_category(id){
    return new Promise((resolve, reject)=>{
        Conversation.find({b_unit_id: id}, function(err, convs){
            if(err) reject({ error: 'Find error', message: err });
            resolve({ status: true, len: convs.length });
        });
    });
}

function get_bunit(company_id = '', all_users=[]){
    return new Promise((resolve,reject)=>{
        console.log('11111')
        BusinessUnit.aggregate([
            {$match: {company_id: company_id}},
            {$lookup:{
                from: 'users',
                localField: 'created_by',
                foreignField: "id",
                as: 'created_by_name'
            }
            },
            {$lookup:{
                from: 'conversations',
                localField: 'unit_id',
                foreignField: "b_unit_id",
                as: "use"
            }
            },
            {$group:{
                _id: "$_id",
                company_id: {$first:"$company_id"},
                unit_id: {$first:"$unit_id"},
                user_id: {$first:"$user_id"},
                unit_name: {$first:"$unit_name"},
                industry_name: {$first:"$industry_name"},
                industry_id: {$first:"$industry_name"},
                created_by: {$first:"$created_by"},
                updated_at: {$first:"$updated_at"},
                created_at: {$first:"$created_at"},
                total_use:{ $sum: { $size: "$use" } },
                created_by_name:{ $first: { $concat: [
                    { $arrayElemAt: ['$created_by_name.firstname', 0] },
                    ' ',
                    { $arrayElemAt: ['$created_by_name.lastname', 0] }
                ]}}
                
            }},
            
            ]).then(function(results) {
                // if (err) {
                // console.error(35,err);
                // } else {
                    resolve(results)
                // }
            });
        });
}

module.exports = {count_category, get_bunit};