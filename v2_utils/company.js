// var { models } = require('./../config/db/express-cassandra');
const isUuid = require('uuid-validate');
//const {uuidv4} = require('uuid')
const { v4: uuidv4 } = require('uuid');
var Users = require('mongoose').model('user')
var Address = require('../mongo-models/address');// require('mongoose').model('address')
var Payment = require('../mongo-models/payment');//require('mongoose').model('payment')
var Company = require('../mongo-models/company');//require('mongoose').model('company')
var Tag = require('../mongo-models/tag');//require('mongoose').model('tag')
var BusinessUnit = require('../mongo-models/business_unit');//require('mongoose').model('business_unit')
var Team = require('../mongo-models/team');//require('mongoose').model('team')


function check_company_limit(id, len){
    return new Promise((resolve)=>{
        Company.findOne({company_id: id.toString()}).lean().then(function(c) {
            if(c){
                c.plan_user_limit = c.plan_user_limit === null ? 10 : Number(c.plan_user_limit);
                c.plan_user_limit = c.company_name === 'ITL Dev' ? 300 : Number(c.plan_user_limit);
                Users.count({company_id: id.toString(), is_active:1 }).then(function(count)  {
                    if(c.plan_user_limit>(count + len))
                        resolve(true);
                    else
                        resolve(false);
                }).catch((e)=>{
                    reject(e);
                });
            }else{
                resolve(false);
            }
        }).catch((e)=>{
            console.log(8, e);
             resolve(true);
        });
    });
}

function get_company_by_user_email(str) {
    return new Promise((resolve, reject) => {
        Users.find({ email: str }, function (err, users) {
            if (err) {
                reject({ status: false, error: err });
            } else {
                if (users.length) {
                    var comids = [];
                    users.forEach(function (v, k) {
                        comids.push(v.company_id);
                    });
                    Company.find({ company_id: { '$in': comids }, is_deactivate: 0 }).then(function (companies) {
                        // if (cerr) reject({ status: false, error: cerr });
                        // console.log(45, companies);
                        resolve(companies);
                    });
                } else {
                    reject({ status: false, error: 'no company found by this email' })
                }
            }
        });
    });
}

function get_company_name(id) {
    return new Promise((resolve, reject) => {
        Company.findOne({ company_id: id }, { select: { "company_name": 1} }, function (cerr, company) {
            if (cerr) reject({ status: false, error: cerr });
            resolve(company);
        });
    });
}

function get_all_company(id = "") {
    return new Promise((resolve, reject) => {
        var query = {};
        if(id) query = { company_id: id.toString() };
        Company.find(query).then(function (company) {
            // if (cerr) reject({ status: false, error: cerr });
            resolve(company);
        });
    });
}

function count_company_users(id = "", active = "") {
    return new Promise((resolve, reject) => {
        if (active !== "" && (active == 0 || active == 1))
            var q = {is_active:active,company_id:id} 
        else
            var q = {is_active:active,company_id:id}
            
        Users.countDocuments(q, (err, msgs) => {
            if (err) {
                console.error(err);
            } else {
                resolve(msgs);
            }
        });
    });
}


function add_company(data) {
    console.log(96,data.email);
    return new Promise((resolve, reject) => {
        Users.find({ email: data.email }).then(function (users) {
            if (users.length > 9) {
                var count = 0;
                users.forEach(function (v, k) {
                    if (all_company.hasOwnProperty(v.company_id.toString()) && all_company[v.company_id.toString()].hasOwnProperty('created_by') && all_company[v.company_id.toString()].created_by.toString() === v.id.toString())
                        count++;
                });
                if (count > 9)
                    reject({ error: 'Company Limit Error', message: 'You already have 10 company.', company: v });
            }

            // models.instance.Company.findOne({ company_name: data.company_name }, { allow_filtering: true }, function(error, company){
            data.domain_name = data.domain_name ? data.domain_name : "";
            Company.find({ domain_name: data.domain_name }).then(function (company) {
                if (company.length) {
                    var flag = false;
                    for (let i = 0; i < company.length; i++) {
                        if (company[i].company_name.toLowerCase() == data.company_name.toLowerCase()) {
                            flag = true;
                            reject({ error: 'Duplicate error', message: 'Company already exists.', company: company[i], company_id: company[i].company_id });
                        }
                    }
                    // plan name = starter/ essential/ etc
                    // plan_access = [
                    // 1000 = Up to 10 users
                    // ]
                    if (flag === false) {
                        const company = new Company({
                            company_id: uuidv4(),
                            company_name: data.company_name,
                            created_by: data.new_uid ? data.new_uid.toString() : '',
                            industry: data.industry ? data.industry : '',
                            domain_name: data.domain_name ? data.domain_name : '',
                            plan_name: data.plan_name ? data.plan_name : 'starter',
                            plan_user_limit: data.plan_user_limit ? data.plan_user_limit : '10',
                            plan_storage_limit: data.plan_storage_limit ? data.plan_storage_limit : '5',
                            plan_access: data.plan_access ? data.plan_access : null
                        });
                        company.save().then(function () {
                            add_default_data(company);
                            resolve({ message: 'Company added successfully.', company ,company_id:company.company_id});
                        }).catch(function (err) {
                            reject({ error: err.name, message: err.message });
                        });
                    }
                } else {
                    const company = new Company({
                        company_id: uuidv4(),
                        company_name: data.company_name,
                        created_by: data.new_uid ? data.new_uid.toString() : '',
                        industry: data.industry ? data.industry : '',
                        domain_name: data.domain_name ? data.domain_name : ''
                    });
                    company.save().then(function () {
                        add_default_data(company);
                        resolve({ message: 'Company added successfully.', company ,company_id:company.company_id});
                    }).catch(function (err) {
                        reject({ error: err.name, message: err.message });
                    });
                }
            }).catch((error)=>{
                reject({ error: 'Find error', message: error });
            });
        }).catch((e)=>{
            if (e) reject({ error: 'Find error', message: e });
        });
    });
}

function add_default_data(data) {
    var tag_name = ["UNTAGGED FILES", "Draft", "Final", "Proposal", "Policy Document", "Agreement", "Contract", "Invoice", "Marketing Document", "HR Document"];
    var queries = [];
    let insertTags = []
    let insertTeams = []
    let insertBusinessUnit = []
    for (let i = 0; i < tag_name.length; i++) {
        var newtag = {
            tag_id: uuidv4(),
            tagged_by: data.created_by,
            title: tag_name[i].toString(),
            type: 'tag',
            tag_type: 'public',
            tag_color: '#023d67',
            company_id: data.company_id.toString(),
            use_count: 0 
        };
        insertTags.push(newtag);
        
    }
    var team_data = ["Default"];
    for (let i = 0; i < team_data.length; i++) {
        var newteam = {
            team_id: uuidv4(),
            created_by: data.created_by,
            updated_by: data.created_by,
            team_title: team_data[i].toString(),
            type: 'tag',
            company_id: data.company_id.toString(),
            participants: [data.created_by]
        };
        insertTeams.push(newteam);
    }
    var room_category_data = ["ADMIN", "FINANCE", "GENERAL", "HR", "IT", "LEGAL", "MARKETING", "PROJECT", "SALES"];
    for (let i = 0; i < room_category_data.length; i++) {
        var newcat = {
            unit_id: uuidv4(),
            company_id: data.company_id.toString(),
            created_by: data.created_by,
            user_id: data.created_by,
            unit_name: room_category_data[i].toString()
        };
        insertBusinessUnit.push(newcat)
    }
    Tag.insertMany(insertTags)
    BusinessUnit.insertMany(insertBusinessUnit)
    Team.insertMany(insertTeams)
}

function update_company(data) {
    return new Promise((resolve, reject) => {
        var update_data = {};
        if (data.plan_name) update_data.plan_name = String(data.plan_name);
        if (data.plan_user_limit) update_data.plan_user_limit = String(data.plan_user_limit);
        if (data.plan_storage_limit) update_data.plan_storage_limit = String(data.plan_storage_limit);
        Company.updateOne({ company_id: data.company_id }, update_data, function (err) {
            if (err) reject({ status: false, error: 'Update error', message: err });
            resolve({ status: true, message: 'Company plan update successfully.', data });
        });
    });
}

function billing_address(data) {
    return new Promise((resolve, reject) => {
        Address.find({ company_id: data.company_id }, function (e, address) {
            if (e) reject({ status: false, error: e });
            resolve(address);
        })
    });
}

function billing_company(data) {
    return new Promise((resolve, reject) => {
        Company.findOne({ company_id: data.company_id }, function (cerr, company) {
            if (cerr) reject({ status: false, error: cerr });
            resolve(company);
        });
    });
}

function payment_method(data) {
    return new Promise(async (resolve, reject) => {
        try {
            var all_pay = await new Promise((resolvePay, rejectPay) => {
                Payment.find({ company_id: data.company_id}).sort({id:-1}).exec((err,pay)=>{
                    if (pay.length) {
                        resolvePay(pay);
                    } else {
                        resolvePay([]);
                    }
                });
            });
            var cardObj = {};
            var storage_purchase_history = [];
            if (all_pay.length) {
                for (let i = 0; i < all_pay.length; i++) {
                    if (Number(all_pay[i].plan_storage_limit) > 0)
                        storage_purchase_history.push({
                            transection_date: all_pay[i].transection_date,
                            plan_storage_limit: all_pay[i].plan_storage_limit,
                            additional_storage: all_pay[i].additional_storage ? Number(all_pay[i].additional_storage) : 0
                        });
                }
                var last_pay = all_pay[0];
                if (last_pay.customer_id) {
                    const customer = await stripe.customers.retrieve(last_pay.customer_id);
                    if (customer.invoice_settings && customer.invoice_settings.default_payment_method) {
                        var paymentMethod = await stripe.paymentMethods.retrieve(
                            customer.invoice_settings.default_payment_method
                        );
                        cardObj.exp_year = paymentMethod.card.exp_year;
                        cardObj.exp_month = paymentMethod.card.exp_month;
                        cardObj.last4 = paymentMethod.card.last4;
                        cardObj.brand = paymentMethod.card.brand;
                        cardObj.fullname = paymentMethod.billing_details.name;
                        cardObj.amount = last_pay.amount;
                        

                    }


                    // if (customer.default_source) {
                    //     const card = await stripe.customers.retrieveSource(last_pay.customer_id, customer.default_source);
                    //     console.log(customer, card);
                    //     cardObj.exp_year = card.exp_year;
                    //     cardObj.exp_month = card.exp_month;
                    //     cardObj.last4 = card.last4;
                    //     cardObj.fullname = customer.name;
                    //     cardObj.amount = last_pay.amount;

                    // }

                }
            }
            resolve({ all_pay, storage_purchase_history, cardObj });

        } catch (e) {
            console.log(e);
            resolve({ all_pay: [], storage_purchase_history: [], cardObj: {} });
        }
    });


}

module.exports = { check_company_limit, get_company_by_user_email, get_company_name, add_company, get_all_company, count_company_users, update_company, billing_address, billing_company, payment_method };