const isUuid = require('uuid-validate');
// var {models} = require('./../config/db/express-cassandra');
var { xmpp_send_server } = require('./voip_util');
const Notification = require('../mongo-models/notification');
const { v4: uuidv4 } = require('uuid');
var ObjectId = require('mongoose').Types.ObjectId;

function save_notification(post,req){
    return new Promise(async (resolve, reject)=>{
        if(isUuid(post.company_id.toString()) && isUuid(post.user_id.toString()) && Array.isArray(post.receiver_id) && post.user_name ){
            var query = [];
            var data = {};
            // No notification for my conversation
            // if(post.receiver_id.length == 1 && post.receiver_id[0].toString() == post.user_id.toString()) return false;

            data.notification_id = uuidv4();
            data.title = post.title ? post.title : 'Untitle Notification';
            data.type = post.type ? post.type : 'danger';
            data.body = post.body ? post.body : '';
            data.created_by_id = post.user_id.toString();
            data.created_by_name = post.user_name ? post.user_name : 'System user';
            data.created_by_img = post.user_img ? post.user_img : 'img.png';
            data.company_id = post.company_id.toString();
            data.task_id = post.task_id;
            data.tab = post.tab;
            (post.receiver_id).forEach(async function(v, k){
                if(isUuid(v.toString())){
                    data.receiver_id = v.toString();
                    var noti = new Notification(data);
                    try{
                        await noti.save();
                        var noti_data = noti.toObject();
                        if(post.decode_id) noti_data.decode_id = post.decode_id;
                        if(post.decode_id !== v.toString()){
                            xmpp_send_server(v.toString(), 'new_notification', noti_data, req);
                        }
                        resolve(noti_data);
                    }catch(err){
                        if(err) console.log(30, err);
                        reject(err);
                    };
                }
            });
            // console.log({ status: true, msg:'Save notification.' });
        }
        else{
            console.log(new Date().toLocaleString() + " Notification not save, due to user id or company id or receiver id missing.")
        }
    })
}

function get_notification(data){
    return new Promise(async (resolve, reject)=>{
        const PAGE_SIZE = 20;
        let page = data.page || 1;
        let query = {
            company_id: data.company_id, 
            type: 'Task'
        };
        if(data.user_id) query.receiver_id = data.user_id;
        if(data.task_id) query.task_id = new ObjectId(data.task_id);
        if(data.tab) query.tab = data.tab;

        const count_total = [{$match: query}, {$count: "total"}];
        const dataAggregate = [
                    {$match: query}, 
                    { $sort: { created_at: -1 } },
                    { $skip: (page - 1) * PAGE_SIZE },
                    { $limit: PAGE_SIZE }
                ];

        const [notifications, countResult] = await Promise.all([
            Notification.aggregate(dataAggregate),
            Notification.aggregate(count_total)
        ]);

        Notification.updateMany(query, {read_status: 'yes'}, { multi: true })
        .then(result => {console.log(64, 'notification update done')})
        // .then(result => {console.log(64, 'notification update done', result)})
        .catch(error => {console.log(65, error);});

        total = countResult[0] ? countResult[0].total : 0;
        totalPages = Math.ceil(total / PAGE_SIZE);

        resolve({
            notifications: notifications, 
            pagination: {
                page: parseInt(page),
                totalPages,
                total
            }
        });
    })
}

function get_notification_count(data){
    return new Promise(async (resolve, reject)=>{
        let query = {company_id: data.company_id, type: 'Task', read_status: 'no'};
        if(data.id)
            query.receiver_id = data.id;

        const count_total = [{$match: query}, {$count: "total"}];
        const countResult = await Notification.aggregate(count_total);

        total = countResult[0] ? countResult[0].total : 0;
        resolve(total);
    })
}

function count_asofday(data) {
    // return new Promise((resolve, reject) => {
    //     var query = { msg_status: { $contains: data.user_id } };
    //     query.$limit = 1000;
    //     var msgs = reply_msgs = mention_msgs = miscall = 0;
    //     models.instance.Messages.find(query, {raw: true, allow_filtering: true}, function(err, rows){
    //         if(err) reject({ error: err.name, message: err.message });
    //         for(let i=0; i<rows.length; i++){
    //             var row = rows[i];
    //             // console.log(2009, row.is_reply_msg);
    //             row.has_hide = Array.isArray(row.has_hide) ? row.has_hide : [];
    //             row.has_delete = Array.isArray(row.has_delete) ? row.has_delete : [];
    //             row.secret_user = Array.isArray(row.secret_user) ? row.secret_user : [];
    //             row.mention_user = Array.isArray(row.mention_user) ? row.mention_user : [];
                
    //             if (row.has_hide.indexOf(data.user_id) == -1 && row.has_delete.indexOf(data.user_id) == -1 && row.secret_user.indexOf(data.user_id) == -1){
    //                 if(row.is_reply_msg == 'yes')
    //                     reply_msgs++;
    //                 else if(row.is_reply_msg == 'no'){
    //                     if(row.msg_type == 'call')
    //                         miscall++;
    //                     msgs++;
    //                 }
    //                 else if(row.mention_user.indexOf(data.user_id) > -1)
    //                     msgs++;
    //             }
    //         }
    //         resolve({ msgs, reply_msgs, mention_msgs, miscall });
    //     });
    // });
}

module.exports = {save_notification, get_notification, get_notification_count, count_asofday};