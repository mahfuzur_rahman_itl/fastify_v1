const isUuid = require('uuid-validate');
// var {models} = require('./../config/db/express-cassandra');
var { get_user_obj } = require('./user');
const Team = require('../mongo-models/team');
var Conversation = require('../mongo-models/conversation');
const _ = require('lodash');
var { fnln } = require('./message');

function get_team(decode){
    return new Promise(async(resolve,reject)=>{
		var query = {};
        // console.log(decode)
		query.company_id = decode.company_id;
        console.log(12,decode.company_id)
		if(decode.role && decode.role !== 'Admin'){
			query.participants = { $in: decode.id };
		}
        if(decode.team_id){
            query.team_id = decode.team_id;
        }
        // console.log(15,query)
        var allteams = await Team.find(query).lean();
       // console.log(20,allteams[0]);
        allteams = _.orderBy(allteams, ['updated_at'],['desc']);
        var teams = [];
        // console.log(23, allteams[0].company_id,all_users.hasOwnProperty(allteams[0].company_id))
        for(let i=0; i<allteams.length; i++){
            // const filteredUser = Object.assign({}, user.toJSON());
            if(! all_users.hasOwnProperty(allteams[i].company_id)) continue;
            if(! all_users[allteams[i].company_id].hasOwnProperty(allteams[i].created_by)) continue;
            var team_system_conversation_active ='';
            var team_system_conversation_off_sms =[];
            
            let con_e = await Conversation.findOne({ company_id: decode.company_id, system_conversation: 'team_' + allteams[i].team_id })

            team_system_conversation_active = con_e.system_conversation_active;
            team_system_conversation_off_sms =con_e.system_conversatio_send_sms;
            allteams[i].team_system_conversation_active = team_system_conversation_active;
            allteams[i].created_by_name = all_users[allteams[i].company_id][allteams[i].created_by].fullname;
            allteams[i].updated_by_name = all_users[allteams[i].company_id][allteams[i].updated_by].fullname;
            allteams[i].participants_details = [];
            allteams[i].team_system_conversation_off_sms = team_system_conversation_off_sms;
            allteams[i].participants = allteams[i].participants !== null ? allteams[i].participants : [];
            for (let j = 0; j < allteams[i].participants.length; j++) {
                // let this_user = get_user_obj(allteams[i].participants[j]);
                // console.log(v.participants[i], this_user.firstname, typeof this_user);
                if(all_users[allteams[i].company_id].hasOwnProperty(allteams[i].participants[j])){
                    allteams[i].participants_details.push({
                        id: allteams[i].participants[j],
                        firstname: all_users[allteams[i].company_id][allteams[i].participants[j]].firstname,
                        lastname: all_users[allteams[i].company_id][allteams[i].participants[j]].lastname,
                        img: all_users[allteams[i].company_id][allteams[i].participants[j]].img,
                        dept: all_users[allteams[i].company_id][allteams[i].participants[j]].dept,
                        designation: all_users[allteams[i].company_id][allteams[i].participants[j]].designation,
                        email: all_users[allteams[i].company_id][allteams[i].participants[j]].email,
                        access: all_users[allteams[i].company_id][allteams[i].participants[j]].access,
                        company_id: all_users[allteams[i].company_id][allteams[i].participants[j]].company_id.toString(),
                        company_name: decode.company_name,
                        conference_id: all_users[allteams[i].company_id][allteams[i].participants[j]].conference_id,
                        role: all_users[allteams[i].company_id][allteams[i].participants[j]].role,
                        is_active: all_users[allteams[i].company_id][allteams[i].participants[j]].is_active,
                        createdat: all_users[allteams[i].company_id][allteams[i].participants[j]].createdat,
                        fnln: fnln(all_users[allteams[i].company_id][allteams[i].participants[j]]),
                        
                    });
                }
            }
            teams.push(allteams[i]);
        }
        // console.log(64,teams);
        resolve(teams);
    });
}

function get_team_admin(company_id, team_id){
    
    Team.find({team_id: team_id}, function(err2, teams) {
        // console.log('get_team_admin',company_id, team_id, teams);
    }).lean();
    let ct = all_teams[company_id];
    for(let i=0; i<ct.length; i++){
        if(ct[i].team_id === team_id)
            return ct[i].admin ? ct[i].admin: [];
    }
    return []
}

module.exports = {get_team, get_team_admin};