const isUuid = require('uuid-validate');
var CryptoJS = require("crypto-js");
var moment = require('moment');
var memored = require('memored');
const sanitizeHtml = require('sanitize-html');
// var { models } = require('../config/db/express-cassandra');
const _ = require('lodash');
const { token } = require('morgan');
const jwt = require('jsonwebtoken');
var { extractToken, verify_refresh } = require('./jwt_helper');
const { v4: uuidv4 } = require('uuid');
var Messages = require('../mongo-models/message');
const Conversation = require('../mongo-models/conversation');
const Users = require('../mongo-models/user');
var { sendMsgUpdateIntoZnode, znMsgUpdate, znReadMsgUpdateCounter } = require('./zookeeper');

var _get_message;
var _parseMuteSafely;
setTimeout(() => {
    var { set_message, get_message, parseMuteSafely, update_conversation_for_last_msg } = require('./message');
    _get_message = get_message;
    _parseMuteSafely = parseMuteSafely;
    _update_conversation_for_last_msg = update_conversation_for_last_msg
}, 1000);



voip_conv_store = {};
voip_busy_user = {};
voip_busy_conv = {};
busy_conv_list = [];
voip_pending_store = {};
voip_timer_ring = {};
voip_timer_process = {};

// call_timer_self = {};
// call_stats_user = {};
// calltimer_ring = {};
// call_timer_connect = {};
// call_socket_store = {};
// call_socket_user = {};
log = console.log;

CALL_RING_TIME = 60000;
// CALL_WAIT_TIME = 20000;
// CALL_CLOSE_TIME = 30000;

CALL_RING_INTERVAL = CALL_RING_TIME / 12;

function convertMS(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    h += d * 24;
    return h + ':' + m + ':' + s;
}

function update_conversation(conversation_id, msg) {
    // models.instance.Conversation.findOne({ conversation_id }, { raw: true, allow_filtering: true }, function (err, conv) {
    //     if (err) { console.log('in message.js file find conversation error ', err); }
    //     if (conv) {
    //         var cur_time = new Date().getTime();
    //         // console.log(1060, msg);
    //         models.instance.Conversation.update({ conversation_id: conv.conversation_id, company_id: conv.company_id }, { last_msg: msg.msg_body, last_msg_time: cur_time, is_active: { '$remove': [msg.sender.toString()] } },
    //             update_if_exists,
    //             function (ue, data) {
    //                 if (ue) console.log("in message.js update conversation error ", ue);
    //             }
    //         )
    //     }
    // });
}

function delRingInterval(conv_id, user_id) {
    if (voip_timer_ring[conv_id] && voip_timer_ring[conv_id][user_id]) {
        if (voip_timer_process[conv_id] && voip_timer_process[conv_id][user_id] && voip_timer_process[conv_id][user_id]['timer']) {
            // console.log('RingInterval:del:this:', process.pid, conv_id, user_id);
            clearTimeout(voip_timer_process[conv_id][user_id]['timer']);
            delete voip_timer_ring[conv_id][user_id];
            delete voip_timer_process[conv_id][user_id];
            if (process.send) process.send({ type: 'voip_timer_ring', voip_timer_ring });

        } else {
            // console.log('RingInterval:del:sendto:', process.pid, conv_id, user_id);
            if (process.send) process.send({
                type: 'voip_timer_clear',
                pid: voip_timer_ring[conv_id][user_id]['pid'],
                conv_id,
                user_id

            });

        }



    }

}


function updateJitsiMemberStatus(conv_id, user_id, status, msg_code, req) {
    if (voip_conv_store.hasOwnProperty(conv_id)) {
        if (msg_code == -3) {
            voip_conv_store[conv_id].participants_status[user_id] = 'Connecting...';
        } else if (msg_code == -2) {
            voip_conv_store[conv_id].participants_status[user_id] = '';
        } else if (msg_code == -1) {
            voip_conv_store[conv_id].participants_status[user_id] = 'Missed Call';
        } else if (msg_code == 0) {
            voip_conv_store[conv_id].participants_status[user_id] = 'Rejected';
        } else if (msg_code == 1) {
            voip_conv_store[conv_id].participants_status[user_id] = "I'll call you later.";
        } else if (msg_code == 2) {
            voip_conv_store[conv_id].participants_status[user_id] = "Can't talk now. Call me later?";
        } else if (msg_code == 3) {
            voip_conv_store[conv_id].participants_status[user_id] = "I'm in a meeting. I'll call you back.";
        }
        // console.log('part_status', user_id, voip_conv_store[conv_id].participants_status[user_id]);

        for (let active_id of voip_conv_store[conv_id].active_participants) {
            xmpp_send_server(active_id, 'jitsi_ring_status', {
                status: status,
                conversation_id: conv_id,
                ring_id: user_id,
                active_participants: voip_conv_store[conv_id].active_participants,
                participants_status: voip_conv_store[conv_id].participants_status
            }, req);

        }
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
    }


}

function add2buffer(data) {
    // data.msg.ack_id = String(models.timeuuid());
    // var io = __io;
    // ignore current socket
    // if (data.emitter == 'jitsi_send_accept') {
    //     if (io.sockets.adapter.rooms.hasOwnProperty(data.user_id)) {
    //         if (Object.keys(io.sockets.adapter.rooms[data.user_id].sockets).length > 0) {
    //             var socket_clients = io.sockets.adapter.rooms[data.user_id].sockets;
    //             for (var sId in socket_clients) {
    //                 if (sId != data.msg.socket_id) {
    //                     var clientSocket = io.sockets.connected[sId];
    //                     clientSocket.emit(data.emitter, data.msg)
    //                 }
    //             }
    //         }
    //     }
    // } else {
    //     io.to(data.user_id).emit(data.emitter, data.msg);


    // }
    // xmpp_send_server(data.user_id, data.emitter, data.msg);

    // if (user_session[data.user_id] && user_session[data.user_id].length > 0) {
    //     if (!free_buffer.hasOwnProperty(data.user_id)) { free_buffer[data.user_id] = []; }

    //     free_buffer[data.user_id].push({
    //         ack_id: data.msg.ack_id,
    //         session_id: user_session[data.user_id],
    //         msg: data.msg,
    //         emitter: data.emitter
    //     });

    // }

}

function remove_buffer(data) {
    // console.log(40, free_buffer);
    if (free_buffer.hasOwnProperty(data.user_id)) {
        var _buffer = free_buffer[data.user_id];
        _.each(_buffer, function (v, k) {
            if (v.ack_id == data.message.ack_id) {
                if (v.session_id.length == 1) {
                    free_buffer[data.user_id] = [];
                    // console.log('rem_buffer_clear', data.user_id);

                } else {
                    v.session_id.splice(v.session_id.indexOf(data.session_id), 1);
                    // console.log('rem_buffer_splice', v.session_id);
                }
                return false;
            }
        })
    }
    // console.log(52, free_buffer);
}
var getConvUsers = (data, callback) => {
    // if (userCompany_id.hasOwnProperty(uid)) {
    Conversation.findOne({ conversation_id: data.conversation_id }, function (err, conv) {
        if (conv) {
            var arr_uid = conv.participants.map((uid) => { return uid; });
            Users.find({ id: { '$in': arr_uid } }, function (err, users) {
                if (err) {
                    callback({ status: false, err: err });
                } else {
                    callback({ status: true, users: users });
                }
            });

        }

    });
    // }

};

function addUserBusy(conv_id, user_id, req) {
    // console.log('voip:busy:add:', user_id, conv_id);
    if (!voip_busy_user[user_id]) voip_busy_user[user_id] = [];
    if (voip_busy_user[user_id].indexOf(conv_id) === -1) voip_busy_user[user_id].push(conv_id);
    if (process.send) process.send({ type: 'voip_busy_user', voip_busy_user });

    if (voip_conv_store.hasOwnProperty(conv_id)) {
        if (voip_conv_store[conv_id].active_participants.indexOf(user_id) === -1) voip_conv_store[conv_id].active_participants.push(user_id);
        // voip_conv_store[conv_id].participants_status[user_id] = 'Connecting';
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

    }

}

function deleteUserBusy(conv_id, user_id, req) {
    // console.log('voip:busy:del:', user_id, conv_id);
    if (voip_busy_user[user_id] && voip_busy_user[user_id].length) {
        if (voip_busy_user[user_id].indexOf(conv_id) > -1) voip_busy_user[user_id].splice(voip_busy_user[user_id].indexOf(conv_id), 1);
        if (voip_busy_user[user_id].length == 0) delete voip_busy_user[user_id];
        if (process.send) process.send({ type: 'voip_busy_user', voip_busy_user });
    }

    if (voip_conv_store[conv_id]) {
        if (voip_conv_store[conv_id].active_participants.indexOf(user_id) !== -1) {
            voip_conv_store[conv_id].active_participants.splice(voip_conv_store[conv_id].active_participants.indexOf(user_id), 1);
        }
        if (voip_conv_store[conv_id].admin_users.indexOf(user_id) !== -1) {
            voip_conv_store[conv_id].admin_users.splice(voip_conv_store[conv_id].admin_users.indexOf(user_id), 1);
        }

        voip_conv_store[conv_id].participants_status[user_id] = '';

        // console.log('part_status', user_id, voip_conv_store[conv_id].participants_status[user_id]);

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
    }
}

async function hangup_conv_exe(conv_id, msg_code = -1, hold_status, req) {

    if (voip_conv_store.hasOwnProperty(conv_id)) {
        delRingInterval(conv_id, conv_id);
        var ring_data = voip_conv_store[conv_id];
        if (!ring_data) return;
        // console.log('voip::hangup_conv_exe::chk:', Date.now() - voip_conv_store[conv_id].init_time,ring_data.database_update);
        if (ring_data.database_update == false) {
            ring_data.database_update = true;
            // console.log('voip:::::::::::::hangup_conv_exe::ok:', Date.now() - voip_conv_store[conv_id].init_time, ring_data.database_update);
            if (voip_timer_ring[conv_id]) {
                for (const user_id in voip_timer_ring[conv_id]) {
                    delRingInterval(conv_id, user_id);
                    updateJitsiMemberStatus(conv_id, user_id, 'hangup', msg_code, req);
                };
            }
            delete voip_busy_conv[conv_id];
            if (process.send) process.send({ type: 'voip_busy_conv', voip_busy_conv });

            // var msg_body = 'Missed Call';
            var msg_popup = {
                msg_body: 'Missed Call',
                conversation_id: conv_id,
                company_id: ring_data.company_id,
                participants: ring_data.participants_all
            }
            if (msg_code == 0) {
                // msg_popup.msg_body = ring_data.hangup_name + ' rejected your call.';
                msg_popup.msg_body = 'Your request to join the call has been declined by ' + ring_data.hangup_name;
                xmpp_send_server(ring_data.root_id, 'jitsi_send_popup', msg_popup, req);
            } else if (msg_code == -1) {
                msg_popup.msg_body = 'Missed Call';
                xmpp_send_server(ring_data.root_id, 'jitsi_send_popup', msg_popup, req);
            } else if (msg_code == 1) {
                msg_popup.msg_body = `${ring_data.hangup_name} couldn't attend call from ${ring_data.user_fullname} and replied back "I'll call you later."`;
                xmpp_send_server(ring_data.root_id, 'jitsi_send_popup', msg_popup, req);
            } else if (msg_code == 2) {
                msg_popup.msg_body = `${ring_data.hangup_name} couldn't attend call from ${ring_data.user_fullname} and replied back "Can't talk now. Call me later?"`;;
                xmpp_send_server(ring_data.root_id, 'jitsi_send_popup', msg_popup, req);
            } else if (msg_code == 3) {
                msg_popup.msg_body = `${ring_data.hangup_name} couldn't attend call from ${ring_data.user_fullname} and replied back "I'm in a meeting. I'll call you back."`;;
                xmpp_send_server(ring_data.root_id, 'jitsi_send_popup', msg_popup, req);
            }

            for (const [i, v] of ring_data.participants_all.entries()) {
                let msg = {
                    conversation_id: conv_id,
                    msgid: ring_data.msgid,
                    user_id: v,
                    // msg_body: msg_popup.msg_body,
                    // ring_index: ring_data.ring_index_list[v], // issue
                    // origin: req.headers.origin
                };
                xmpp_send_server(v, 'jitsi_send_hangup', msg, req);
                send_msg_firebase(v, 'jitsi_send_hangup', msg, req);

                deleteUserBusy(conv_id, v, req);
                xmpp_send_server(v, 'jitsi_busy_status', {
                    // conversation_id: conv_id,
                    // user_busy: voip_busy_user.hasOwnProperty(v),
                    voip_busy_conv: voip_busy_conv,
                    busy_conv_list: Object.keys(voip_busy_conv),
                }, req);
            };

            if (ring_data.start_time == 0) {
                ring_data.call_duration = '';
            } else {
                ring_data.call_duration = convertMS(Math.abs(Date.now() - ring_data.start_time));
            }
            if (ring_data.msgid) {
                var msg_info = await _get_message({ convid: conv_id, msgid: ring_data.msgid, limit: 1, is_reply: 'no', user_id: ring_data.user_id, type: 'one_msg' });
                if (msg_info.msgs[0].last_reply_name) {
                    msg_popup.msg_body = ring_data.call_duration ? 'In a call for ' + ring_data.call_duration + ' . See thread for call log and notes.' : msg_popup.msg_body;

                } else {
                    msg_popup.msg_body = ring_data.call_duration ? 'In a call for ' + ring_data.call_duration : msg_popup.msg_body;

                }

                msg_popup.msg_body = '<p>' + msg_popup.msg_body + '</p>';


                Messages.updateOne(
                    { conversation_id: conv_id, msg_id: ring_data.msgid },
                    {
                        created_at: new Date().getTime(),
                        msg_body: msg_popup.msg_body,
                        call_status: msg_code < 0 ? 'ended' : 'message',
                        call_duration: ring_data.call_duration,
                        call_running: false,
                        has_reply: msg_info.msgs[0].has_reply,
                        last_reply_time: msg_info.msgs[0].last_reply_time,
                        last_reply_name: msg_info.msgs[0].last_reply_name,
                    }, async function (e, result) {
                        // if (ue) {
                        var lastmsg = await _get_message({ convid: conv_id, msgid: ring_data.msgid, limit: 1, is_reply: 'no', user_id: ring_data.user_id, company_id: ring_data.company_id, type: 'one_msg' });
                        //sendMsgUpdateIntoZnode(lastmsg,ring_data.participants_all,"call_msg_update");
                        await znMsgUpdate(lastmsg.msgs[0],ring_data.participants_all,'call_msg_update');
                    
                        (ring_data.participants_all).forEach(function (v, k) {
                            xmpp_send_server(v, 'edit_message', lastmsg.msgs[0], req);
                            send_msg_firebase(v, 'edit_message', lastmsg.msgs[0], req);
                        });
                        // console.log("call message.js update", result);
                        // }
                    })

                // models.instance.Messages.findOne({
                //     conversation_id: models.uuidFromString(conv_id),
                //     msg_id: models.timeuuidFromString(ring_data.msgid)
                // }, { raw: true, allow_filtering: true }, async function (error2, msg_info) {
                //     if (error2) { console.error(error2); } else {
                //         if (msg_info.last_reply_name) {
                //             msg_popup.msg_body = ring_data.call_duration ? 'In a call for ' + ring_data.call_duration + ' . See thread for call log and notes.' : msg_popup.msg_body;

                //         } else {
                //             msg_popup.msg_body = ring_data.call_duration ? 'In a call for ' + ring_data.call_duration : msg_popup.msg_body;

                //         }

                //         msg_popup.msg_body = '<p>' + msg_popup.msg_body + '</p>';



                //         // models.instance.Messages.update({
                //         //     conversation_id: models.uuidFromString(conv_id),
                //         //     msg_id: models.timeuuidFromString(ring_data.msgid)
                //         // }, {
                //         //     created_at: new Date().getTime(),
                //         //     msg_body: msg_popup.msg_body,
                //         //     call_status: msg_code < 0 ? 'ended' : 'message',
                //         //     call_duration: ring_data.call_duration,
                //         //     call_running: false,
                //         //     has_reply: msg_info.has_reply,
                //         //     last_reply_time: msg_info.last_reply_time,
                //         //     last_reply_name: msg_info.last_reply_name,
                //         // }, update_if_exists, async function (error4) {
                //         //     if (error4) { console.error(error4); } else {
                //         //         msg_info.msg_body = msg_popup.msg_body;
                //         //         update_conversation(models.uuidFromString(conv_id), msg_info);

                //         //         var lastmsg = await _get_message({ convid: conv_id, msgid: ring_data.msgid, limit: 1, is_reply: 'no', user_id: msg_info.sender });
                //         //         (ring_data.participants_all).forEach(function (v, k) {
                //         //             xmpp_send_server(v, 'edit_message', lastmsg[0], req);
                //         //             send_msg_firebase(v, 'edit_message', lastmsg[0], req);
                //         //         });

                //         //     }
                //         // });
                //     }
                // });

            }



        }

        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
    }
}

// var reply_Id = (msg_id, conversation_id) => {
//     return new Promise((resolve, reject) => {
//         models.instance.Messages.findOne({ msg_id: models.timeuuidFromString(msg_id), conversation_id: models.uuidFromString(conversation_id) }, function(error, msg) {
//             if (error) resolve({ status: false, result: error });
//             else {
//                 models.instance.ReplayConv.find({ msg_id: models.timeuuidFromString(msg_id), conversation_id: models.uuidFromString(conversation_id) }, { raw: true, allow_filtering: true }, function(err, reply_info) {
//                     if (err) {
//                         resolve({ status: false, result: err });
//                     } else {
//                         if (reply_info.length == 0) {
//                             var reply_uuid = models.uuid();
//                             var replyData = new models.instance.ReplayConv({
//                                 rep_id: reply_uuid,
//                                 msg_id: models.timeuuidFromString(msg_id),
//                                 conversation_id: models.uuidFromString(conversation_id)
//                             });
//                             replyData.saveAsync().then(function(res) {
//                                 resolve({ status: true, result: reply_uuid, msgdata: msg });
//                             }).catch(function(err) {
//                                 resolve({ status: false, err: err });
//                             });
//                         } else {
//                             resolve({ status: true, result: reply_info[0].rep_id, msgdata: msg });
//                         }
//                     }
//                 });
//             }
//         });

//     })

// };
var get_one_msg = (data, callback) => {
    // var query = {
    //     conversation_id: models.uuidFromString(data.conversation_id),
    //     msg_id: models.timeuuidFromString(data.msg_id)
    // }
    // models.instance.Messages.find(query, { raw: true, allow_filtering: true }, function (error, msg) {
    //     if (error) {
    //         callback({ status: false, error: error });
    //     } else {
    //         callback({ status: true, msg: msg[0] });
    //     }
    // });
};
// async function xmpp_send_msg(to_user, data) {
//     var jabberid = to_user + '@' + xmpp_domain;
//     const message = xmpp.xml(
//         "message", { type: "chat", to: jabberid },
//         xmpp.xml("body", {}, JSON.stringify(data)),
//     );
//     await xmpp_server.send(message); // admin to admin
//     console.log('ok');

// }

const xmpp_send_server = async (user_id, emit, data2, req = false, condition = null) => {
    // console.log('xmpp_send_server:init:',user_id,emit);
    var data = JSON.parse(JSON.stringify(data2));
    data.xmpp_type = emit;
    data.xmpp_unix = Date.now();
    data.CLIENT_BASE_URL = new URL(process.env.CLIENT_BASE_URL).hostname;
    data.API_SERVER_URL = new URL(process.env.API_SERVER_URL);
    if (req) data.origin = req.headers.origin;
    if (!data.origin) data.origin = '*';
    if (user_id && user_id.includes('@')) {
        // if (data.xmpp_type.indexOf('online') === -1) console.log(`xmpp:send:jid:start: ${user_id} : ${data.xmpp_type}`);
        xmpp_server.send(xmpp.xml(
            "message", { type: "chat", to: user_id },
            xmpp.xml("body", {}, JSON.stringify(data)),
        ))
            .catch((error) => {
                console.error('xmpp_server:send:error', error);
                // setTimeout(() => {
                //     xmpp_send_server(user_id, emit, data, req, condition);
                // }, 1000);

            })
            .then((done) => {
                // console.log(`xmpp:send:jid:success: ${user_id} : ${data.xmpp_type}`);
            });
    } else {
        if (user_id && all_devices[user_id] && all_devices[user_id].length) {
            // console.log(`xmpp:send:user:start:  ${user_id} : ${data.xmpp_type} : ${all_devices[user_id]}`);
            all_devices[user_id].forEach((device_id) => {
                if (device_id === data.device_id && emit === 'logout_from_all' && data.still_login) return;
                if (condition) {
                    if (condition.filter) {
                        if (condition.filter == device_id) {
                            return;
                        }
                    } else if (condition.equal) {
                        if (condition.equal != device_id) {
                            return;
                        }
                    }

                }
                var jabberid = user_id + '$$$' + device_id + '@' + xmpp_domain;
                // console.log('jabberid:::::======>',jabberid)
                // data = CryptoJS.AES.encrypt(JSON.stringify(data), process.env.CRYPTO_SECRET).toString();
                // console.log('xmpp_data', data);
                const message = xmpp.xml(
                    "message", { type: "chat", to: jabberid },
                    xmpp.xml("body", {}, JSON.stringify(data)),
                );
                xmpp_server.send(message)
                    .then((done) => {
                        // console.log(`xmpp_server:done: ${Date.now()} : ${data.xmpp_type} : ${jabberid}`);
                    })
                    .catch((error) => {
                        console.error('xmpp_server:send:error', error);
                        // setTimeout(() => {
                        //     xmpp_send_server(jabberid, emit, data, req, condition);
                        // }, 1000);
                    });
            })
        }
    }

}

const xmpp_send_room = async (user_id, conversation_id, emit, data, req = false, condition = null) => {
    // console.log('xmpp_send_server:init:',user_id,emit);
    var data = _.cloneDeep(data);
    data.xmpp_type = emit;
    data.xmpp_unix = Date.now();
    data.CLIENT_BASE_URL = new URL(process.env.CLIENT_BASE_URL).hostname;
    data.API_SERVER_URL = new URL(process.env.API_SERVER_URL).hostname;
    if (req) data.origin = req.headers.origin;
    if (!data.origin) data.origin = '*';

    xmpp_api_client.sendMessage('groupchat', user_id, conversation_id, 'subject', JSON.stringify(data))
        .then(res => {
            // console.log('groupchat:done:', res);
            // resolve(true);
        })
        .catch((err) => {
            console.log('groupchat:error', err);
            // resolve(true);
        });

}

function xmpp_send_all(emit, data) {
    if (xmpp_server.status == 'online') {
        // console.log('xmpp_send_all');
        for (let user_id in all_devices) {
            all_devices[user_id].forEach((device_id) => {
                var jabberid = user_id + '$$$' + device_id + '@' + xmpp_domain;
                xmpp_send_server(jabberid, emit, data);
            })

        }
    } else {
        setTimeout(() => {
            xmpp_send_all(emit, data);
        }, 1000);

    }


}

function xmpp_send_broadcast(emit, data, generate = true) {
    if (!xmpp_api_client) return;
    // xmpp_api_client.registeredUsers('caquecdn02.freeli.io').then((total_users) => {
    //     console.log('xmpp:total_users:', total_users);
    //     var fill = total_users.filter((user)=>{
    //         return user.includes('94508809-7f1d-454b-8dc2-ecece18db2e5');

    //     });
    //     console.log('xmpp:total_users:', fill);

    // });

    xmpp_api_client.connectedUsers().then((online_users) => {

        for (user of online_users) {
            xmpp_send_server(user, emit, data);

            if (user.includes('$$$') && generate) {
                let user_id = user.split('$$$')[0];
                let xmpp_user = user.split('@')[0];
                let xmpp_token = xmpp_user.split('$$$')[1];
                if (!online_user_lists[user_id]) online_user_lists[user_id] = [];
                if (!all_devices[user_id]) all_devices[user_id] = [];

                if (online_user_lists[user_id].indexOf(xmpp_user) === -1) online_user_lists[user_id].push(xmpp_user);
                if (all_devices[user_id].indexOf(xmpp_token) === -1) all_devices[user_id].push(xmpp_token);
            }
        }
        // console.log('==================> xmpp:broadcast:', emit);
        if (process.send) process.send({ type: 'all_devices', all_devices });


    }).catch((err) => {
        console.log(err);
        // resolve(false);
    });

}
async function send_msg_firebase(_user_id, fcm_type, _data, req = null) {
    return new Promise(async (resolveMain, rejectMain) => {
        if (req)
            var data = _.cloneDeep(_data);
        // if (data.all_attachment) delete data.all_attachment;
        var decode = null;
        if (req) {
            data.origin = req.headers.origin;
            let token = extractToken(req);
            if (token) decode = jwt.verify(token, process.env.SECRET);
        }
        if (data.sender) {
            if (data.sender.toString() == _user_id) return;
        }
        if (data.msg_text) {
            data.msg_text = sanitizeHtml(data.msg_text, {
                allowedTags: [],
                allowedAttributes: {},
                allowedIframeHostnames: []
            });
        }
        if (data.msg_body) {
            let bytes = CryptoJS.AES.decrypt(data.msg_body, process.env.CRYPTO_SECRET);
            data.msg_body = bytes.toString(CryptoJS.enc.Utf8);
            data.msg_body = sanitizeHtml(data.msg_body, {
                allowedTags: [],
                allowedAttributes: {},
                allowedIframeHostnames: []
            });
            data.msg_body = data.msg_body.trim().replace(/"/g, '').replace(/\\n/g, '');

            const aesEncrypted = CryptoJS.AES.encrypt(data.msg_body, process.env.CRYPTO_SECRET);
            data.msg_body_str = aesEncrypted.toString();
            data.msg_body_enc = {
                key: aesEncrypted.key.toString(),
                iv: aesEncrypted.iv.toString(),
                content: aesEncrypted.toString()
            };
        }
        let user_id = _user_id;
        // console.log(`firebase:start: user=> ${user_id} , fcm_type=> ${fcm_type}`);

        if (user_id && data) {
            var mute_status = false;
            if (data.conversation_id) {
                let conv_id = String(data.conversation_id);

                let conv = await Conversation.findOne({ conversation_id: conv_id }).lean();
                if (conv) {
                    if (fcm_type == 'new_message') {
                        if (conv.group && conv.title) {
                            if (conv.group == 'yes') {
                                data.msg_title = `${data.sendername ? data.sendername.trim() : ''} wrote in ${conv.title} room on Workfreeli:`;
                            } else {
                                data.msg_title = `${data.sendername ? data.sendername.trim() : ''} wrote in the direct room with you on Workfreeli:`;
                            }
                        }
                    } else if (fcm_type == 'new_reply_message') {
                        if (conv.group && conv.title) {
                            if (conv.group == 'yes') {
                                data.msg_title = `Threaded reply from ${data.sendername ? data.sendername.trim() : ""} in ${conv.title} room on Workfreeli:`;
                            } else {
                                data.msg_title = `Threaded reply from ${data.sendername ? data.sendername.trim() : ""} in the direct room with you on Workfreeli:`;
                            }
                        }
                    } else if (fcm_type == 'edit_message') {
                        if (conv.group && conv.title) {
                            if (conv.group == 'yes') {
                                data.msg_title = `${data.sendername.trim()} edited a message in ${conv.title} room on Workfreeli:`;
                            } else {
                                data.msg_title = `${data.sendername.trim()} edited a message in the direct room with you on Workfreeli:`;
                            }
                        }
                    }

                }
                if (conv && conv.has_mute != null) {
                    if (conv.has_mute.indexOf(user_id) > -1) {
                        var mute_conv = null;
                        for (const v of conv.mute) {
                            // _.each(conv.mute, async function(v, k) {
                            var mute_data = _parseMuteSafely(v);
                            if (mute_data.mute_by == user_id) {
                                mute_conv = mute_data;
                                if (mute_conv) {
                                    // for (let i = 0; i < mute_conv.length; i++) {
                                    if (mute_conv.mute_day && mute_conv.mute_day.length) { // recursive pattern
                                        // var db_tz = mute_conv[i].mute_timezone;
                                        // if (db_tz) {
                                        //     var user_moment = moment().utcOffset(db_tz);
                                        //     var user_day = user_moment.format('ddd');
                                        //     if (mute_conv[i].mute_day.indexOf(user_day) > -1) { // today match
                                        //         var currentDate = user_moment.format('DD-MM-YYYY');
                                        //         var db_mute_start = mute_conv[i].mute_start_time;
                                        //         var db_mute_end = mute_conv[i].mute_end_time;
                                        //         var st = moment(currentDate + ' ' + db_mute_start + ' ' + db_tz, 'DD-MM-YYYY LT Z');
                                        //         var et = moment(currentDate + ' ' + db_mute_end + ' ' + db_tz, 'DD-MM-YYYY LT Z');
                                        //         if ((st.hour() >= 12 && et.hour() <= 12) || et.isBefore(st)) { et.add(1, "days"); }

                                        //         if (user_moment.isBetween(st, et)) {
                                        //             // if (mute_conv[i].show_notification == null) mute_conv[i].show_notification = false;
                                        //             // if (mute_conv[i].show_notification == false) 
                                        //             mute_status = true;
                                        //         }
                                        //     }
                                        // }
                                    } else { // normal pattern
                                        var now_unix = moment().unix();
                                        var mute_start = parseInt(mute_conv.mute_unix_start);
                                        var mute_end = parseInt(mute_conv.mute_unix_end);
                                        if (now_unix > mute_start && now_unix < mute_end) {
                                            mute_status = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // if (decode && all_users && all_users[decode.company_id] && all_users[decode.company_id][user_id]) {
            //     var user = all_users[decode.company_id][user_id];
            //     console.log('firebase:user:cache',user);
            // } else {

            let user = await Users.findOne({ id: String(user_id) }).lean();

            if (user) {
                if (user.mute) {
                    var mute_data = JSON.parse(user.mute);
                    if (mute_data) {
                        var now_unix = moment().unix();
                        var mute_start = parseInt(mute_data.mute_unix_start);
                        var mute_end = parseInt(mute_data.mute_unix_end);
                        if (now_unix > mute_start && now_unix < mute_end) {
                            mute_status = true;
                        }
                    }
                }

                if (fcm_type == 'jitsi_ring_send' || fcm_type == 'jitsi_send_hangup' || fcm_type == 'logout_from_all') {
                    mute_status = false;
                }
                if (mute_status == false) {
                    var messages = [];
                    let data2send = {};
                    Object.keys(data).forEach(ky => {
                        if (data[ky] != null) {
                            if (_.isString(data[ky])) data2send[ky] = data[ky]; else data2send[ky] = JSON.stringify(data[ky]);
                        }
                    });
                    data2send['fcm_type'] = fcm_type;
                    data2send.file_server = process.env.FILE_SERVER;
                    data2send.CLIENT_BASE_URL = process.env.CLIENT_BASE_URL;
                    data2send.xmpp_unix = String(Date.now());
                    // console.log('data2send.xmpp_unix', data2send.xmpp_unix / 1000)
                    if (!user.fcm_id) user.fcm_id = [];
                    if (data.fcm_optional) {
                        if (user.fcm_id.indexOf(data.fcm_optional) === -1) user.fcm_id.push(data.fcm_optional);
                    }
                    if (user.fcm_id.length) {
                        for (let fid of user.fcm_id) {
                            if (fcm_type === 'logout_from_all' && fid === data.firebase_id && data.still_login) continue;
                            
                            if (typeof fid === 'object') fid = Object.values(fid)[0];
                            else if (typeof fid !== 'string') continue;

                            if (fid.includes('@@@')) { // device check
                                var device = fid.split('@@@')[0];
                                var fcm_id = fid.split('@@@')[1];

                                if (fcm_id != 'null') {
                                    if (device == 'android') {
                                        let msg = {
                                            data: data2send,
                                            token: fcm_id,
                                            "android": {
                                                "priority": "high",
                                                // "ttl": 60000,
                                            },
                                        };
                                        if (fcm_type == 'jitsi_ring_send') {
                                            msg.android.ttl = 60000;
                                        }
                                        messages.push(msg);
                                    } else if (device == 'web') {
                                        let msg = {
                                            data: data2send,
                                            token: fcm_id,
                                            "webpush": {
                                                "headers": {
                                                    "Urgency": "high",
                                                    // "TTL": "60" 
                                                },
                                            }
                                        };
                                        if (fcm_type == 'jitsi_ring_send') {
                                            // msg.webpush.TTL = "60";
                                        }
                                        messages.push(msg);
                                    } else { // iOS
                                        // if (fcm_id != 'null') {
                                        messages.push({
                                            // notification: notification_obj,
                                            data: data2send,
                                            token: fcm_id,
                                            "apns": {
                                                "headers": {
                                                    "apns-priority": "10",
                                                    "apns-expiration": "0"
                                                },
                                                "payload": {
                                                    "aps": {
                                                        "mutable-content": 1
                                                    }
                                                },
                                            },
                                        });
                                        // }
                                    }
                                }
                            } else { // fcm direct token
                                var fcm_id = fid;
                                if (fcm_id != 'null') {
                                    // var message = { data: data2send };
                                    messages.push({
                                        // notification: notification_obj,
                                        data: data2send,
                                        token: fcm_id
                                    });
                                }
                            }
                        }
                    }
                    if (messages.length) {
                        sendFirebaseMsg(user_id, messages, fcm_type)
                    }
                }
            }
        }
        resolveMain();
    });

}



function sendFirebaseMsg(user_id, messages, fcm_type) {
    // console.log(`firebase:send: user=> ${user_id} , fcm_type=> ${fcm_type} , count=> ${messages.length}`);
    // console.log('sendFirebaseMsg', messages)
    firebase_admin.messaging().sendAll(messages).then((response) => {
        // console.log('firebase:done:::::::::::::', fcm_type, user_id, response.successCount,response.failureCount);
        // response.responses.forEach((e) => {
        //     console.log('firebase:responses:::::::::::::', fcm_type, user_id, e.error);

        // })
    }).catch(function (error) {
        console.trace("Firebase:Error:", error);
    });

}

function deleteRingIndex(conv_id, user_id) {
    var ring_data = voip_conv_store[conv_id];
    if (ring_data.ring_index_list && ring_data.ring_index_list.hasOwnProperty(user_id)) {
        delete ring_data.ring_index_list[user_id];
    }
    if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });

}

function checkRingStatus(user_id) {
    let status = false;
    for (let conv_id in voip_conv_store) {
        if (voip_conv_store[conv_id].participants_status[user_id] &&
            voip_conv_store[conv_id].participants_status[user_id] == 'Calling...') {
            status = true;
            break;
        }

    }
    return status;
}

function call_ringing_loop(conv_id, receiver_id, data, req) {
    if (checkRingStatus(receiver_id) == true) return;
    data.user_busy = voip_busy_user[receiver_id] ? voip_busy_user[receiver_id] : [];
    if (data.user_busy.length) data.user_busy = data.user_busy[data.user_busy.length - 1];
    else data.user_busy = '';

    data.call_merge = {};
    if (voip_busy_user[receiver_id] && voip_busy_user[receiver_id].length) {
        let active_current = voip_conv_store[conv_id].active_participants.length;
        let active_type = voip_conv_store[conv_id].conversation_type;
        voip_busy_user[receiver_id].forEach(function (conv_old, k) {
            var active_old = voip_conv_store[conv_old].active_participants.length;
            if (active_type != 'group' && (active_current < 2 || active_old < 2)) {
                data.call_merge[conv_old] = voip_conv_store[conv_old].convname;
            }
        });
    }
    data.call_merge_icon = Object.keys(data.call_merge).length ? true : false;

    if (voip_conv_store.hasOwnProperty(conv_id)) {
        if (voip_conv_store[conv_id].ring_index_list[receiver_id]) return;
        if (voip_conv_store[conv_id].active_participants.indexOf(receiver_id) > -1) return;
        voip_conv_store[conv_id].ring_index_list[receiver_id] = data.ring_index;
        voip_conv_store[conv_id].participants_status[receiver_id] = 'Calling...';

        // console.log('part_status', receiver_id, voip_conv_store[conv_id].participants_status[receiver_id]);
        if (process.send) process.send({ type: 'voip_conv_store', voip_conv_store });
        // console.log('voip:call_ringing__loop:', voip_conv_store[conv_id].init_time - Date.now());
        xmpp_send_server(receiver_id, 'jitsi_ring_send', data, req);
        send_msg_firebase(receiver_id, 'jitsi_ring_send', data, req);

        // let cc = 0;
        // console.log('RingInterval:set:this', process.pid, conv_id, receiver_id)
        voip_timer_ring[conv_id][receiver_id] = { pid: process.pid };
        if (process.send) process.send({ type: 'voip_timer_ring', voip_timer_ring: voip_timer_ring });
        if (!voip_timer_process[conv_id]) voip_timer_process[conv_id] = {};
        voip_timer_process[conv_id][receiver_id] = {
            timer: setTimeout(() => {
                // cc++;
                // console.log('RingInterval:timeout:user:', process.pid, conv_id, receiver_id);
                // if (cc >= 12) {
                delRingInterval(conv_id, receiver_id);
                deleteUserBusy(conv_id, receiver_id);
                if (voip_conv_store[conv_id]) {
                    voip_conv_store[conv_id].participants_status[receiver_id] = '';
                    // console.log('part_status', receiver_id, voip_conv_store[conv_id].participants_status[receiver_id]);
                }
                updateJitsiMemberStatus(conv_id, receiver_id, 'timeout', -1, req);
                deleteRingIndex(conv_id, receiver_id);


            }, CALL_RING_TIME)
        }
    }
}
var isRealString = (str) => {
    return typeof str === 'string' && str.trim().length > 0;
}
var sendCallMsgDB = async (from, sender_img, sender_name, conversation_id, msg_body, msgduration, set_calltype, replycount, arr_participants, conference_id, call_running, call_status, last_reply_time, last_reply_name, company_id) => {
    return new Promise(async (resolve, reject) => {
        // if (isRealString(msg_body) && from != null && sender_img != null && sender_name != null && conversation_id != null && msg_body != null && set_calltype != null && replycount != null && arr_participants != null) {
        var msgid = uuidv4();
        var bytes = CryptoJS.AES.decrypt(msg_body, process.env.CRYPTO_SECRET);
        msg_body = bytes.toString(CryptoJS.enc.Utf8);
        var totalData = {
            msg_id: msgid,
            conversation_id: String(conversation_id),
            company_id: company_id,

            sender: String(from),
            sender_name: sender_name,
            sender_img: sender_img,

            msg_body: msg_body,
            msg_type: 'call',
            msg_text: '',
            msg_status: (arr_participants).filter((a) => { return a != from; }),

            call_type: set_calltype,
            call_status: call_status,
            call_duration: msgduration,
            call_running: call_running,
            call_participants: arr_participants,
            participants:arr_participants,
            conference_id: conference_id,

            attch_imgfile: [],
            attch_audiofile: [],
            attch_videofile: [],
            attch_otherfile: [],

            has_reply: replycount,
            is_reply_msg: 'no'
        }
        if (last_reply_time != null) { totalData['last_reply_time'] = last_reply_time; }
        if (last_reply_name != null) { totalData['last_reply_name'] = last_reply_name; }
        await Messages.insertMany([totalData]);

        resolve({ status: true, res: msgid, msg: totalData });

        var lastmsg = await _get_message({ convid: String(conversation_id), company_id: company_id, msgid: msgid, limit: 1, is_reply: 'no', user_id: String(from), type: 'one_msg' });
        //console.log(1005,lastmsg);
        _update_conversation_for_last_msg(lastmsg.msgs[0]);
        sendMsgUpdateIntoZnode(lastmsg);

        // var msg_info = await _get_message({ convid: conversation_id, company_id: company_id, msgid: msgid, limit: 1, is_reply: 'no', user_id: ring_data.user_id, type: 'one_msg' });

        // var message = new models.instance.Messages(totalData);
        // message.saveAsync()
        //     .then(function (res) {
        //         resolve({ status: true, res: msgid, msg: message });
        //     })
        //     .catch(function (err) {
        //         resolve({ status: false, err: err });

        //     });
        // } else {
        //     resolve({ status: false, err: 'Message formate not supported.' });
        // }

    })

};
async function create_account_xmpp(user) {
    if (!xmpp_api_client) return;
    return new Promise((resolve, reject) => {
        xmpp_api_client.checkAccount(user, xmpp_domain).then((res) => {
            if (res == 1) {
                xmpp_api_client.register(user, xmpp_domain, 'a123456').then((res) => {
                    // console.log(res);
                    resolve(true);

                }).catch((r) => {
                    console.log(r);
                    resolve(false);
                });

            } else {
                resolve(true);
            }
        }).catch((r) => {
            console.log(r);
            resolve(false);
        });

    })
}
async function register_firebase_token(user_id, device, firebase_token) {
    return new Promise(async (resolveit, rejectit) => {
        // models.instance.Users.find({ 'fcm_id': { '$contains': device + '@@@' + firebase_token } }, 
        // { allow_filtering: true }, async function (error, user_fcm) {
        // if (error) console.error(error);
        // var proArr = [];
        // if (user_fcm && user_fcm.length) {
        //     for (let fcm of user_fcm) {
        //         proArr.push(
        //             new Promise((resolve, reject) => {
        //                 models.instance.Users.update({ id: fcm.id }, { fcm_id: { '$remove': [device + '@@@' + firebase_token] } },
        //                     update_if_exists, async function (err) {
        //                         if (err) console.trace(err);
        //                         resolve();
        //                     });
        //             })
        //         )
        //     }
        // }
        // Promise.all(proArr).then(results => {
        // console.log('register_firebase_token::', user_id, device + '@@@' + firebase_token)
        await Users.updateOne({ id: String(user_id) }, { $addToSet: { fcm_id: device + '@@@' + firebase_token } });
        resolveit();
        // });
        // });

    });
}

async function xmppUserAdd(xmpp_user, req, xmpp_emit = false) {
    if (!xmpp_user) return;
    let user_id = xmpp_user.split('$$$')[0];
    let xmpp_token = xmpp_user.split('$$$')[1];

    if (!online_user_lists[user_id]) online_user_lists[user_id] = [];
    if (!all_devices[user_id]) all_devices[user_id] = [];

    if (all_devices[user_id].indexOf(xmpp_token) === -1) all_devices[user_id].push(xmpp_token);

    if (online_user_lists[user_id].indexOf(xmpp_user) === -1) {
        online_user_lists[user_id].push(xmpp_user);
        if (process.send) process.send({ type: 'online_user_lists', online_user_lists });
        if (process.send) process.send({ type: 'all_devices', all_devices });

        Users.updateOne({ id: String(user_id) }, { $addToSet: { device: xmpp_token } });
    }
    if (xmpp_emit) xmpp_send_broadcast('online_user_lists', { list: Object.keys(online_user_lists), online_user_lists, origin: req ? req.headers.origin : '*' });
}

async function xmppUserRemove(user_id, xmpp_user, req) {

    if (!online_user_lists[user_id]) online_user_lists[user_id] = [];
    if (!all_devices[user_id]) all_devices[user_id] = [];

    let xmpp_token = xmpp_user.split('$$$')[1];
    if (xmpp_user == 'all') {
        delete online_user_lists[user_id];
        await Users.updateOne({ id: String(user_id) }, { $set: { 'device': [] } });

        // if (database_loaded && models && models.instance && models.instance.Users) {
        //     models.instance.Users.update({ id: models.uuidFromString(user_id) }, { device: null }, update_if_exists, function (error) {
        //         if (error) console.log("Last login info update error");
        //     });
        // }
    }
    else if (xmpp_user == 'except') {
        online_user_lists[user_id] = [xmpp_user];
        await Users.updateOne({ id: String(user_id) }, { $set: { 'device': [xmpp_token] } });

        // if (database_loaded && models && models.instance && models.instance.Users) {
        //     models.instance.Users.update({ id: models.uuidFromString(user_id) }, { device: [xmpp_token] }, update_if_exists, function (error) {
        //         if (error)
        //             console.log("Last login info update error");
        //     });
        // }
    }
    else {
        let idx = online_user_lists[user_id].indexOf(xmpp_user);
        if (idx !== -1) { online_user_lists[user_id].splice(idx, 1); }
        if (online_user_lists[user_id].length == 0) delete online_user_lists[user_id];

        if(req.body.remove_device !== 'no'){ 
            console.log("11111111111111111111111111");
            await Users.updateOne({ id: String(user_id) }, { '$pull': { 'device': xmpp_token } });
        }

        // if (database_loaded && models && models.instance && models.instance.Users) {
        //     models.instance.Users.update({ id: models.uuidFromString(user_id) }, { device: { $remove: [xmpp_token] } }, update_if_exists, function (error) {
        //         if (error)
        //             console.log("Last login info update error");
        //     });
        // }

    }

    // let idx2 = all_devices[user_id].indexOf(xmpp_token);
    // if (idx2 !== -1) { all_devices[user_id].splice(idx2, 1); }

    if (process.send) process.send({ type: 'online_user_lists', online_user_lists });
    if (process.send) process.send({ type: 'all_devices', all_devices });
    // console.log('=========================>>> xmppUserRemove', user_id, xmpp_user, online_user_lists);
    xmpp_send_broadcast('online_user_lists', { list: Object.keys(online_user_lists), online_user_lists, origin: req ? req.headers.origin : '*' }, false);



}
async function cache_write(key, value) {
    return new Promise((resolve, reject) => {
        memored.store(key, value, function () {
            // console.log('cluster_cache__write:', process.pid, key, value);
            resolve();
        });

    })
}
async function cache_read(key) {
    return new Promise((resolve, reject) => {
        memored.read(key, function (err, value) {
            if (!value) {
                if (key == 'online_user_lists') value = {};
                else if (key == 'all_users') value = {};
                else if (key == 'all_company') value = {};
                else if (key == 'all_teams') value = {};
                else if (key == 'all_bunit') value = {};

            }
            // console.log('cluster_cache__read:', process.pid, key, value);
            resolve(value);
        });

    })
}
module.exports = {
    convertMS,
    update_conversation,
    delRingInterval,
    send_msg_firebase,
    updateJitsiMemberStatus,

    add2buffer,
    remove_buffer,
    getConvUsers,
    addUserBusy,
    deleteUserBusy,
    hangup_conv_exe,
    get_one_msg,
    call_ringing_loop,
    isRealString,
    sendCallMsgDB,
    xmpp_send_server,
    xmpp_send_broadcast,
    xmpp_send_all,
    deleteRingIndex,
    create_account_xmpp,
    register_firebase_token,
    xmppUserAdd,
    xmppUserRemove,
    cache_write,
    cache_read,
    checkRingStatus,
    xmpp_send_room
};
