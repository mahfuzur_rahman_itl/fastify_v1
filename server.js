require('dotenv').config();
require('./config/mongoose');

restart_time = Date.now();
update_if_exists = { if_exists: true };
all_user_lists = [];
online_user_lists = {};
online_user_details = [];
free_buffer = {};
user_session = {};
user_token_store = {};
userCompany_id = {}
io = "";
xmpp_api_client = null;
zkConnectionStatus = false;

// as buffer
all_company = {};
all_users = {};
all_teams = {};
all_untags = {};
all_bunit = {};
all_devices = {};

setTimeout(function(){
    const fastify = require('fastify')({logger: true})
    const jwt = require('fastify-jwt');
    fastify.register(jwt, {
        secret: process.env.SECRET
    });

    // require('./v2_utils/jwt_helper')
    require('./v2_utils/load_cache')
    
    fastify.register(require('./routes/v2/users'))
    fastify.register(require('./routes/v2/category'))
    fastify.register(require('./routes/v2/home'))
    fastify.register(require('./routes/v2/conversation'))
    fastify.register(require('./routes/v2/per_message'))

    fastify.listen({ port: 3000 }, function (err, address) {
        if (err) {
            fastify.log.error(err)
            process.exit(1)
        }

        xmpp_domain = process.env.xmpp_domain;
        xmpp_admin_name = process.env.xmpp_admin_name;
        xmpp_admin_pass = process.env.xmpp_admin_pass;
        // ===================================================
        xmpp = require("@xmpp/client");
        xmpp_debug = require("@xmpp/debug");
        xmpp_api = require('@appunto/ejabberd-api-client');
        // xmpp_simple = require('simple-xmpp');
        var { xmpp_send_broadcast } = require('./v2_utils/voip_util');
        // setTimeout(() => {
        // ================= xmpp.js ===========================================

        // // wss://192.168.0.108:5443/ws
        // // xmpp://192.168.0.108:5222

        xmpp_server = xmpp.client({
            service: "wss://" + xmpp_domain + ":5443/ws",
            domain: xmpp_domain,
            // resource: "example",
            username: xmpp_admin_name,
            password: xmpp_admin_pass,
        });

        // // // debug(xmpp_server, true);

        xmpp_server.on("error", (err) => {
            console.error('>>> XMPP SERVER ONLINE: ', err);
            // throw 'XMPP SERVER CONNECTION FAILED.';
        });

        xmpp_server.on("offline", () => {
        console.log("offline");
        });

        xmpp_server.on("stanza", async(stanza) => {
            if (stanza.is("message")) {
                // var ss = JSON.parse(stanza.children[0].children);
                
                // await xmpp_server.send(xml("presence", { type: "unavailable" }));

                let bb = stanza.getChild("body").text();
            //  console.log('xmpp:body1',bb);
                var data = JSON.parse(bb);
            //  console.log('xmpp_msg:recive:', stanza.attrs.from + '=>' + stanza.attrs.to,data);

            } else if (stanza.is("presence")) {
            //  console.log('xmpp_msg:presence:', stanza.attrs.from + '=>' + stanza.attrs.to);

            }
        });

        xmpp_server.on("online", async(address) => {
            console.log('>>> XMPP SERVER ONLINE: ', address);
                // Makes itself available
                // await xmpp_server.send(xmpp.xml("presence"));

            xmpp_api_client = new xmpp_api(xmpp_domain, 5443);
            xmpp_api_client.status().then(
                result => {
                    console.log('>>> XMPP SERVER ADMIN API:', result, xmpp_domain);
                    xmpp_send_broadcast('xmpp_reconnect_client', { xmpp_domain, CLIENT_BASE_URL: process.env.CLIENT_BASE_URL, origin: '*' });
                    // xmpp_send_broadcast("update_client_version", {restart_time});
                }
            ).catch((err) => {
                console.log(err);
            });
        });


        xmpp_server.start().catch(console.error);
    })
}, 3000);